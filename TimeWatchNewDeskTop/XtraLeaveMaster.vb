﻿Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Base

Public Class XtraLeaveMaster
    Dim ulf As UserLookAndFeel
    Public Shared LEAVEFIELD As String
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            ''ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            Me.TblLeaveMaster1TableAdapter1.Fill(Me.SSSDBDataSet.tblLeaveMaster1)
            GridControl1.DataSource = SSSDBDataSet.tblLeaveMaster1
        Else
            ''ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            TblLeaveMasterTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.TblLeaveMasterTableAdapter.Fill(Me.SSSDBDataSet.tblLeaveMaster)
            GridControl1.DataSource = SSSDBDataSet.tblLeaveMaster
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraLeaveMaster_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Me.Width = My.Computer.Screen.WorkingArea.Width
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.Width = SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = (SplitContainerControl1.Parent.Width) * 85 / 100

        'not to do in all pages
        Common.splitforMasterMenuWidth = SplitContainerControl1.Width
        Common.SplitterPosition = SplitContainerControl1.SplitterPosition

        'Common.res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraCompany).Assembly)
        'Common.cul = CultureInfo.CreateSpecificCulture("en")

        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub GridView1_InitNewRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles GridView1.InitNewRow
        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            view.SetRowCellValue(e.RowHandle, "LEAVETYPE", "Leave")

            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet
            ds = New DataSet
            Dim sSql As String = "SELECT max(LEAVEFIELD) as LEAVE from tblLeaveMaster "
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            'MsgBox(ds.Tables(0).Rows(0).Item("LEAVE").ToString)
            If ds.Tables(0).Rows(0).Item("LEAVE").ToString.Trim = "" Then
                view.SetRowCellValue(e.RowHandle, "LEAVEFIELD", "L01")
            Else
                view.SetRowCellValue(e.RowHandle, "LEAVEFIELD", "L" & (ds.Tables(0).Rows(0).Item("LEAVE").ToString.Substring(1, 2) + 1).ToString("00"))
            End If
            view.SetRowCellValue(e.RowHandle, "ISOFFINCLUDE", "N")
            view.SetRowCellValue(e.RowHandle, "ISHOLIDAYINCLUDE", "N")
            view.SetRowCellValue(e.RowHandle, "ISLEAVEACCRUAL", "N")
            view.SetRowCellValue(e.RowHandle, "isMonthly", "N")
            view.SetRowCellValue(e.RowHandle, "isCompOffType", "N")
            view.SetRowCellValue(e.RowHandle, "FIXED", "N")

            view.SetRowCellValue(e.RowHandle, "SMIN", 0)
            view.SetRowCellValue(e.RowHandle, "ExpiryDays", 0)
            view.SetRowCellValue(e.RowHandle, "SMAX", 0)
            view.SetRowCellValue(e.RowHandle, "PRESENT", 0)
            view.SetRowCellValue(e.RowHandle, "LEAVE", 0)
            view.SetRowCellValue(e.RowHandle, "LEAVELIMIT", 0)
        End If
    End Sub
    Private Sub GridView1_RowDeleted(sender As System.Object, e As DevExpress.Data.RowDeletedEventArgs) Handles GridView1.RowDeleted
        Me.TblLeaveMasterTableAdapter.Update(Me.SSSDBDataSet.tblLeaveMaster)
        Me.TblLeaveMaster1TableAdapter1.Update(Me.SSSDBDataSet.tblLeaveMaster1)
        XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
    End Sub
    Private Sub GridView1_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles GridView1.RowUpdated
        Me.TblLeaveMasterTableAdapter.Update(Me.SSSDBDataSet.tblLeaveMaster)
        Me.TblLeaveMaster1TableAdapter1.Update(Me.SSSDBDataSet.tblLeaveMaster1)
    End Sub
    Private Sub GridView1_ValidateRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles GridView1.ValidateRow
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim CellId As String = row(1).ToString.Trim
        If CellId = "" Then
            e.Valid = False
            e.ErrorText = "<size=10>Leave Code " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
        End If
        Dim cellname As String = row(2).ToString.Trim
        If cellname = "" Then
            e.Valid = False
            e.ErrorText = "<size=10>Leave Description " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
        End If

        Dim LEAVETYPE = row("LEAVETYPE").ToString.Trim
        If LEAVETYPE = "Leave" Then
            row("LEAVETYPE") = "L"
        ElseIf LEAVETYPE = "Absent" Then
            row("LEAVETYPE") = "A"
        ElseIf LEAVETYPE = "Present" Then
            row("LEAVETYPE") = "P"
        ElseIf LEAVETYPE = "" Then
            e.Valid = False
            e.ErrorText = "<size=10>Leave Type " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
        Else
            e.Valid = False
            e.ErrorText = "<size=10>Leave Type " & Common.res_man.GetString("is_invalid", Common.cul) & ","
        End If
        'MsgBox(row("LEAVETYPE"))
        'MsgBox(row("ISOFFINCLUDE").ToString)

        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet
            ds = New DataSet
            Dim sSql As String = "select LEAVECODE from tblLeaveMaster where LEAVECODE = '" & row("LEAVECODE").ToString & "'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                e.Valid = False
                e.ErrorText = "<size=10>Duplicate Leave code ,"
            End If
        End If

        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
    End Sub
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Try
            LEAVEFIELD = row("LEAVEFIELD").ToString.Trim
        Catch ex As Exception
            LEAVEFIELD = ""
        End Try
        e.Allow = False

        XtraLeaveMasterEdit.ShowDialog()
        If Common.servername = "Access" Then   'to refresh the grid
            Me.TblLeaveMaster1TableAdapter1.Fill(Me.SSSDBDataSet.tblLeaveMaster1)
        Else
            Me.TblLeaveMasterTableAdapter.Fill(Me.SSSDBDataSet.tblLeaveMaster)
        End If

        'Try
        '    Dim LEAVETYPE As String = row("LEAVETYPE").ToString.Trim
        '    If LEAVETYPE = "L" Then
        '        row("LEAVETYPE") = "Leave"
        '    ElseIf LEAVETYPE = "A" Then
        '        row("LEAVETYPE") = "Absent"
        '    ElseIf LEAVETYPE = "P" Then
        '        row("LEAVETYPE") = "Present"
        '    End If
        'Catch
        'End Try
    End Sub
    Private Sub GridView1_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        Try
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "LEAVETYPE" Then
                Dim LEAVETYPE As String = view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LEAVETYPE")
                If LEAVETYPE = "L" Then
                    e.DisplayText = "Leave"
                ElseIf LEAVETYPE = "A" Then
                    e.DisplayText = "Absent"
                ElseIf LEAVETYPE = "P" Then
                    e.DisplayText = "Present"
                End If
            End If
        Catch
        End Try
    End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                'Me.Validate(False)
                'e.Handled = False
                Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
                Dim CellId As String = row("LEAVECODE").ToString.Trim

                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet = New DataSet
                Dim sSql As String = "select count(*) from LeaveApplication where LeaveCode = '" & CellId & "'"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows(0).Item(0).ToString > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Leave already applied for Employee. Cannot delete.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Me.Validate()
                    e.Handled = True
                    Exit Sub
                Else
                    Common.LogPost("Leave Master Delete; LeaveCode:" & CellId)
                End If
            End If
        End If
    End Sub
    Private Sub GridView1_EditFormPrepared(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventArgs) Handles GridView1.EditFormPrepared
        For Each control As Control In e.Panel.Controls
            For Each button As Control In control.Controls
                If (button.Text = "Update") Then
                    button.Text = Common.res_man.GetString("save", Common.cul)
                End If
                If (button.Text = "Cancel") Then
                    button.Text = Common.res_man.GetString("cancel", Common.cul)
                End If
            Next
        Next
        e.BindableControls(colLEAVEFIELD).Enabled = False 'e.RowHandle Mod 2 = 0
    End Sub
    Private Sub GridView1_ShowingPopupEditForm(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.ShowingPopupEditFormEventArgs) Handles GridView1.ShowingPopupEditForm
        For Each control As Control In e.EditForm.Controls
            Common.SetFont(control, 9)
        Next control
        e.EditForm.StartPosition = FormStartPosition.CenterParent
    End Sub
End Class
