﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraPayArrear
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraPayArrear))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.SidePanel2 = New DevExpress.XtraEditors.SidePanel()
        Me.mskPaidDays = New DevExpress.XtraEditors.TextEdit()
        Me.Label9 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.optPayable2 = New DevExpress.XtraEditors.CheckEdit()
        Me.optPayable1 = New DevExpress.XtraEditors.CheckEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.lbl8 = New DevExpress.XtraEditors.LabelControl()
        Me.lbl10 = New DevExpress.XtraEditors.LabelControl()
        Me.lbl6 = New DevExpress.XtraEditors.LabelControl()
        Me.lbl9 = New DevExpress.XtraEditors.LabelControl()
        Me.lbl7 = New DevExpress.XtraEditors.LabelControl()
        Me.lbl3 = New DevExpress.XtraEditors.LabelControl()
        Me.lbl5 = New DevExpress.XtraEditors.LabelControl()
        Me.lbl1 = New DevExpress.XtraEditors.LabelControl()
        Me.lbl4 = New DevExpress.XtraEditors.LabelControl()
        Me.lbl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TXTPAYdAYS = New DevExpress.XtraEditors.TextEdit()
        Me.txtAmtonESI = New DevExpress.XtraEditors.TextEdit()
        Me.txtAmtonVPf = New DevExpress.XtraEditors.TextEdit()
        Me.txtAmtonPf = New DevExpress.XtraEditors.TextEdit()
        Me.txtcalculatedFpf = New DevExpress.XtraEditors.TextEdit()
        Me.txtcalculatedEpf = New DevExpress.XtraEditors.TextEdit()
        Me.txtCalculatedPF = New DevExpress.XtraEditors.TextEdit()
        Me.txtESIAmount = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.txtCalculatedVPF = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.txtEarningEarn10 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarningEarn9 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarningEarn8 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarningEarn7 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarningEarn6 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarningEarn5 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarningEarn1 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarningEarn4 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarningEarn3 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarningEarn2 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarningBasic = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarningHRA = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarningMed = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarningConv = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarningDA = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarn10 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarn9 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarn8 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarn7 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarn6 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarn5 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarn1 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarn4 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarn3 = New DevExpress.XtraEditors.TextEdit()
        Me.txtEarn2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.txtBasic = New DevExpress.XtraEditors.TextEdit()
        Me.txtHRA = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.txtMed = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.txtConv = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.txtDA = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.btnApply = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.txtapplicableto = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.txtapplicablefrom = New DevExpress.XtraEditors.DateEdit()
        Me.ComboNepaliYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNEpaliMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.txtPayableMonth = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.lblName = New DevExpress.XtraEditors.LabelControl()
        Me.lblDept = New DevExpress.XtraEditors.LabelControl()
        Me.lblCardNum = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.SSSDBDataSet = New ULtra.SSSDBDataSet()
        Me.TblEmployeeTableAdapter = New ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblEmployee1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        Me.SidePanel2.SuspendLayout()
        CType(Me.mskPaidDays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.optPayable2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optPayable1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.TXTPAYdAYS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAmtonESI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAmtonVPf.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAmtonPf.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtcalculatedFpf.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtcalculatedEpf.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCalculatedPF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtESIAmount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCalculatedVPF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningEarn2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningBasic.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningHRA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningMed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningConv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarningDA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarn10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarn9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarn8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarn7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarn6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarn5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarn1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarn4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarn3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarn2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBasic.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtHRA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtConv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtapplicableto.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtapplicableto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtapplicablefrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtapplicablefrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPayableMonth.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPayableMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1250, 568)
        Me.SplitContainerControl1.SplitterPosition = 1165
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'SidePanel2
        '
        Me.SidePanel2.Controls.Add(Me.mskPaidDays)
        Me.SidePanel2.Controls.Add(Me.Label9)
        Me.SidePanel2.Controls.Add(Me.GroupControl3)
        Me.SidePanel2.Controls.Add(Me.PanelControl1)
        Me.SidePanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanel2.Location = New System.Drawing.Point(0, 100)
        Me.SidePanel2.Name = "SidePanel2"
        Me.SidePanel2.Size = New System.Drawing.Size(1165, 434)
        Me.SidePanel2.TabIndex = 42
        Me.SidePanel2.Text = "d"
        '
        'mskPaidDays
        '
        Me.mskPaidDays.Location = New System.Drawing.Point(751, 109)
        Me.mskPaidDays.Name = "mskPaidDays"
        Me.mskPaidDays.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.mskPaidDays.Properties.Appearance.Options.UseFont = True
        Me.mskPaidDays.Properties.Mask.EditMask = "##.##"
        Me.mskPaidDays.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.mskPaidDays.Properties.MaxLength = 5
        Me.mskPaidDays.Size = New System.Drawing.Size(84, 20)
        Me.mskPaidDays.TabIndex = 55
        '
        'Label9
        '
        Me.Label9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.Label9.Appearance.Options.UseFont = True
        Me.Label9.Location = New System.Drawing.Point(641, 112)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(85, 14)
        Me.Label9.TabIndex = 54
        Me.Label9.Text = "No.of Paid Days"
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GroupControl3.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl3.Controls.Add(Me.optPayable2)
        Me.GroupControl3.Controls.Add(Me.optPayable1)
        Me.GroupControl3.Location = New System.Drawing.Point(622, 6)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(239, 97)
        Me.GroupControl3.TabIndex = 1
        Me.GroupControl3.Text = "Payble Days Selection"
        '
        'optPayable2
        '
        Me.optPayable2.Location = New System.Drawing.Point(16, 62)
        Me.optPayable2.Name = "optPayable2"
        Me.optPayable2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.optPayable2.Properties.Appearance.Options.UseFont = True
        Me.optPayable2.Properties.Caption = "Paid Days by User"
        Me.optPayable2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.optPayable2.Properties.RadioGroupIndex = 0
        Me.optPayable2.Size = New System.Drawing.Size(154, 19)
        Me.optPayable2.TabIndex = 61
        Me.optPayable2.TabStop = False
        '
        'optPayable1
        '
        Me.optPayable1.EditValue = True
        Me.optPayable1.Location = New System.Drawing.Point(16, 37)
        Me.optPayable1.Name = "optPayable1"
        Me.optPayable1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.optPayable1.Properties.Appearance.Options.UseFont = True
        Me.optPayable1.Properties.Caption = "As Per Actual Paid Days"
        Me.optPayable1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.optPayable1.Properties.RadioGroupIndex = 0
        Me.optPayable1.Size = New System.Drawing.Size(177, 19)
        Me.optPayable1.TabIndex = 60
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.lbl8)
        Me.PanelControl1.Controls.Add(Me.lbl10)
        Me.PanelControl1.Controls.Add(Me.lbl6)
        Me.PanelControl1.Controls.Add(Me.lbl9)
        Me.PanelControl1.Controls.Add(Me.lbl7)
        Me.PanelControl1.Controls.Add(Me.lbl3)
        Me.PanelControl1.Controls.Add(Me.lbl5)
        Me.PanelControl1.Controls.Add(Me.lbl1)
        Me.PanelControl1.Controls.Add(Me.lbl4)
        Me.PanelControl1.Controls.Add(Me.lbl2)
        Me.PanelControl1.Controls.Add(Me.TXTPAYdAYS)
        Me.PanelControl1.Controls.Add(Me.txtAmtonESI)
        Me.PanelControl1.Controls.Add(Me.txtAmtonVPf)
        Me.PanelControl1.Controls.Add(Me.txtAmtonPf)
        Me.PanelControl1.Controls.Add(Me.txtcalculatedFpf)
        Me.PanelControl1.Controls.Add(Me.txtcalculatedEpf)
        Me.PanelControl1.Controls.Add(Me.txtCalculatedPF)
        Me.PanelControl1.Controls.Add(Me.txtESIAmount)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.txtCalculatedVPF)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.txtEarningEarn10)
        Me.PanelControl1.Controls.Add(Me.txtEarningEarn9)
        Me.PanelControl1.Controls.Add(Me.txtEarningEarn8)
        Me.PanelControl1.Controls.Add(Me.txtEarningEarn7)
        Me.PanelControl1.Controls.Add(Me.txtEarningEarn6)
        Me.PanelControl1.Controls.Add(Me.txtEarningEarn5)
        Me.PanelControl1.Controls.Add(Me.txtEarningEarn1)
        Me.PanelControl1.Controls.Add(Me.txtEarningEarn4)
        Me.PanelControl1.Controls.Add(Me.txtEarningEarn3)
        Me.PanelControl1.Controls.Add(Me.txtEarningEarn2)
        Me.PanelControl1.Controls.Add(Me.txtEarningBasic)
        Me.PanelControl1.Controls.Add(Me.txtEarningHRA)
        Me.PanelControl1.Controls.Add(Me.txtEarningMed)
        Me.PanelControl1.Controls.Add(Me.txtEarningConv)
        Me.PanelControl1.Controls.Add(Me.txtEarningDA)
        Me.PanelControl1.Controls.Add(Me.txtEarn10)
        Me.PanelControl1.Controls.Add(Me.txtEarn9)
        Me.PanelControl1.Controls.Add(Me.txtEarn8)
        Me.PanelControl1.Controls.Add(Me.txtEarn7)
        Me.PanelControl1.Controls.Add(Me.txtEarn6)
        Me.PanelControl1.Controls.Add(Me.txtEarn5)
        Me.PanelControl1.Controls.Add(Me.txtEarn1)
        Me.PanelControl1.Controls.Add(Me.txtEarn4)
        Me.PanelControl1.Controls.Add(Me.txtEarn3)
        Me.PanelControl1.Controls.Add(Me.txtEarn2)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.txtBasic)
        Me.PanelControl1.Controls.Add(Me.txtHRA)
        Me.PanelControl1.Controls.Add(Me.LabelControl16)
        Me.PanelControl1.Controls.Add(Me.txtMed)
        Me.PanelControl1.Controls.Add(Me.LabelControl13)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.txtConv)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.txtDA)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(616, 434)
        Me.PanelControl1.TabIndex = 0
        '
        'lbl8
        '
        Me.lbl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl8.Appearance.Options.UseFont = True
        Me.lbl8.Location = New System.Drawing.Point(29, 346)
        Me.lbl8.Name = "lbl8"
        Me.lbl8.Size = New System.Drawing.Size(20, 14)
        Me.lbl8.TabIndex = 110
        Me.lbl8.Text = "     "
        '
        'lbl10
        '
        Me.lbl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl10.Appearance.Options.UseFont = True
        Me.lbl10.Location = New System.Drawing.Point(27, 398)
        Me.lbl10.Name = "lbl10"
        Me.lbl10.Size = New System.Drawing.Size(20, 14)
        Me.lbl10.TabIndex = 109
        Me.lbl10.Text = "     "
        '
        'lbl6
        '
        Me.lbl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl6.Appearance.Options.UseFont = True
        Me.lbl6.Location = New System.Drawing.Point(27, 294)
        Me.lbl6.Name = "lbl6"
        Me.lbl6.Size = New System.Drawing.Size(20, 14)
        Me.lbl6.TabIndex = 106
        Me.lbl6.Text = "     "
        '
        'lbl9
        '
        Me.lbl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl9.Appearance.Options.UseFont = True
        Me.lbl9.Location = New System.Drawing.Point(27, 372)
        Me.lbl9.Name = "lbl9"
        Me.lbl9.Size = New System.Drawing.Size(20, 14)
        Me.lbl9.TabIndex = 108
        Me.lbl9.Text = "     "
        '
        'lbl7
        '
        Me.lbl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl7.Appearance.Options.UseFont = True
        Me.lbl7.Location = New System.Drawing.Point(27, 320)
        Me.lbl7.Name = "lbl7"
        Me.lbl7.Size = New System.Drawing.Size(20, 14)
        Me.lbl7.TabIndex = 107
        Me.lbl7.Text = "     "
        '
        'lbl3
        '
        Me.lbl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl3.Appearance.Options.UseFont = True
        Me.lbl3.Location = New System.Drawing.Point(27, 216)
        Me.lbl3.Name = "lbl3"
        Me.lbl3.Size = New System.Drawing.Size(20, 14)
        Me.lbl3.TabIndex = 105
        Me.lbl3.Text = "     "
        '
        'lbl5
        '
        Me.lbl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl5.Appearance.Options.UseFont = True
        Me.lbl5.Location = New System.Drawing.Point(27, 268)
        Me.lbl5.Name = "lbl5"
        Me.lbl5.Size = New System.Drawing.Size(20, 14)
        Me.lbl5.TabIndex = 104
        Me.lbl5.Text = "     "
        '
        'lbl1
        '
        Me.lbl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl1.Appearance.Options.UseFont = True
        Me.lbl1.Location = New System.Drawing.Point(27, 164)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(20, 14)
        Me.lbl1.TabIndex = 101
        Me.lbl1.Text = "     "
        '
        'lbl4
        '
        Me.lbl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl4.Appearance.Options.UseFont = True
        Me.lbl4.Location = New System.Drawing.Point(27, 242)
        Me.lbl4.Name = "lbl4"
        Me.lbl4.Size = New System.Drawing.Size(20, 14)
        Me.lbl4.TabIndex = 103
        Me.lbl4.Text = "     "
        '
        'lbl2
        '
        Me.lbl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbl2.Appearance.Options.UseFont = True
        Me.lbl2.Location = New System.Drawing.Point(27, 190)
        Me.lbl2.Name = "lbl2"
        Me.lbl2.Size = New System.Drawing.Size(20, 14)
        Me.lbl2.TabIndex = 102
        Me.lbl2.Text = "     "
        '
        'TXTPAYdAYS
        '
        Me.TXTPAYdAYS.Enabled = False
        Me.TXTPAYdAYS.Location = New System.Drawing.Point(467, 317)
        Me.TXTPAYdAYS.Name = "TXTPAYdAYS"
        Me.TXTPAYdAYS.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TXTPAYdAYS.Properties.Appearance.Options.UseFont = True
        Me.TXTPAYdAYS.Properties.Mask.EditMask = "######.##"
        Me.TXTPAYdAYS.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TXTPAYdAYS.Properties.MaxLength = 9
        Me.TXTPAYdAYS.Size = New System.Drawing.Size(100, 20)
        Me.TXTPAYdAYS.TabIndex = 100
        Me.TXTPAYdAYS.Visible = False
        '
        'txtAmtonESI
        '
        Me.txtAmtonESI.Enabled = False
        Me.txtAmtonESI.Location = New System.Drawing.Point(467, 291)
        Me.txtAmtonESI.Name = "txtAmtonESI"
        Me.txtAmtonESI.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtAmtonESI.Properties.Appearance.Options.UseFont = True
        Me.txtAmtonESI.Properties.Mask.EditMask = "######.##"
        Me.txtAmtonESI.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtAmtonESI.Properties.MaxLength = 9
        Me.txtAmtonESI.Size = New System.Drawing.Size(100, 20)
        Me.txtAmtonESI.TabIndex = 99
        Me.txtAmtonESI.Visible = False
        '
        'txtAmtonVPf
        '
        Me.txtAmtonVPf.Enabled = False
        Me.txtAmtonVPf.Location = New System.Drawing.Point(467, 265)
        Me.txtAmtonVPf.Name = "txtAmtonVPf"
        Me.txtAmtonVPf.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtAmtonVPf.Properties.Appearance.Options.UseFont = True
        Me.txtAmtonVPf.Properties.Mask.EditMask = "######.##"
        Me.txtAmtonVPf.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtAmtonVPf.Properties.MaxLength = 9
        Me.txtAmtonVPf.Size = New System.Drawing.Size(100, 20)
        Me.txtAmtonVPf.TabIndex = 98
        Me.txtAmtonVPf.Visible = False
        '
        'txtAmtonPf
        '
        Me.txtAmtonPf.Enabled = False
        Me.txtAmtonPf.Location = New System.Drawing.Point(467, 239)
        Me.txtAmtonPf.Name = "txtAmtonPf"
        Me.txtAmtonPf.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtAmtonPf.Properties.Appearance.Options.UseFont = True
        Me.txtAmtonPf.Properties.Mask.EditMask = "######.##"
        Me.txtAmtonPf.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtAmtonPf.Properties.MaxLength = 9
        Me.txtAmtonPf.Size = New System.Drawing.Size(100, 20)
        Me.txtAmtonPf.TabIndex = 97
        Me.txtAmtonPf.Visible = False
        '
        'txtcalculatedFpf
        '
        Me.txtcalculatedFpf.Enabled = False
        Me.txtcalculatedFpf.Location = New System.Drawing.Point(467, 213)
        Me.txtcalculatedFpf.Name = "txtcalculatedFpf"
        Me.txtcalculatedFpf.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtcalculatedFpf.Properties.Appearance.Options.UseFont = True
        Me.txtcalculatedFpf.Properties.Mask.EditMask = "######.##"
        Me.txtcalculatedFpf.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtcalculatedFpf.Properties.MaxLength = 9
        Me.txtcalculatedFpf.Size = New System.Drawing.Size(100, 20)
        Me.txtcalculatedFpf.TabIndex = 96
        Me.txtcalculatedFpf.Visible = False
        '
        'txtcalculatedEpf
        '
        Me.txtcalculatedEpf.Enabled = False
        Me.txtcalculatedEpf.Location = New System.Drawing.Point(467, 187)
        Me.txtcalculatedEpf.Name = "txtcalculatedEpf"
        Me.txtcalculatedEpf.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtcalculatedEpf.Properties.Appearance.Options.UseFont = True
        Me.txtcalculatedEpf.Properties.Mask.EditMask = "######.##"
        Me.txtcalculatedEpf.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtcalculatedEpf.Properties.MaxLength = 9
        Me.txtcalculatedEpf.Size = New System.Drawing.Size(100, 20)
        Me.txtcalculatedEpf.TabIndex = 95
        Me.txtcalculatedEpf.Visible = False
        '
        'txtCalculatedPF
        '
        Me.txtCalculatedPF.Location = New System.Drawing.Point(467, 109)
        Me.txtCalculatedPF.Name = "txtCalculatedPF"
        Me.txtCalculatedPF.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtCalculatedPF.Properties.Appearance.Options.UseFont = True
        Me.txtCalculatedPF.Properties.Mask.EditMask = "######.##"
        Me.txtCalculatedPF.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtCalculatedPF.Properties.MaxLength = 9
        Me.txtCalculatedPF.Size = New System.Drawing.Size(100, 20)
        Me.txtCalculatedPF.TabIndex = 90
        '
        'txtESIAmount
        '
        Me.txtESIAmount.Location = New System.Drawing.Point(467, 161)
        Me.txtESIAmount.Name = "txtESIAmount"
        Me.txtESIAmount.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtESIAmount.Properties.Appearance.Options.UseFont = True
        Me.txtESIAmount.Properties.Mask.EditMask = "######.##"
        Me.txtESIAmount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtESIAmount.Properties.MaxLength = 9
        Me.txtESIAmount.Size = New System.Drawing.Size(100, 20)
        Me.txtESIAmount.TabIndex = 94
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(410, 164)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(30, 14)
        Me.LabelControl7.TabIndex = 93
        Me.LabelControl7.Text = "E.S.I."
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(410, 112)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(21, 14)
        Me.LabelControl10.TabIndex = 89
        Me.LabelControl10.Text = "P.F."
        '
        'txtCalculatedVPF
        '
        Me.txtCalculatedVPF.Location = New System.Drawing.Point(467, 135)
        Me.txtCalculatedVPF.Name = "txtCalculatedVPF"
        Me.txtCalculatedVPF.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtCalculatedVPF.Properties.Appearance.Options.UseFont = True
        Me.txtCalculatedVPF.Properties.Mask.EditMask = "######.##"
        Me.txtCalculatedVPF.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtCalculatedVPF.Properties.MaxLength = 9
        Me.txtCalculatedVPF.Size = New System.Drawing.Size(100, 20)
        Me.txtCalculatedVPF.TabIndex = 92
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(410, 138)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(33, 14)
        Me.LabelControl14.TabIndex = 91
        Me.LabelControl14.Text = "V.P.F."
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(254, 11)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(44, 14)
        Me.LabelControl4.TabIndex = 88
        Me.LabelControl4.Text = "Amount"
        '
        'txtEarningEarn10
        '
        Me.txtEarningEarn10.Enabled = False
        Me.txtEarningEarn10.Location = New System.Drawing.Point(227, 395)
        Me.txtEarningEarn10.Name = "txtEarningEarn10"
        Me.txtEarningEarn10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn10.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn10.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn10.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn10.Properties.MaxLength = 9
        Me.txtEarningEarn10.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn10.TabIndex = 87
        '
        'txtEarningEarn9
        '
        Me.txtEarningEarn9.Enabled = False
        Me.txtEarningEarn9.Location = New System.Drawing.Point(227, 369)
        Me.txtEarningEarn9.Name = "txtEarningEarn9"
        Me.txtEarningEarn9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn9.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn9.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn9.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn9.Properties.MaxLength = 9
        Me.txtEarningEarn9.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn9.TabIndex = 86
        '
        'txtEarningEarn8
        '
        Me.txtEarningEarn8.Enabled = False
        Me.txtEarningEarn8.Location = New System.Drawing.Point(227, 343)
        Me.txtEarningEarn8.Name = "txtEarningEarn8"
        Me.txtEarningEarn8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn8.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn8.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn8.Properties.MaxLength = 9
        Me.txtEarningEarn8.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn8.TabIndex = 85
        '
        'txtEarningEarn7
        '
        Me.txtEarningEarn7.Enabled = False
        Me.txtEarningEarn7.Location = New System.Drawing.Point(227, 317)
        Me.txtEarningEarn7.Name = "txtEarningEarn7"
        Me.txtEarningEarn7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn7.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn7.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn7.Properties.MaxLength = 9
        Me.txtEarningEarn7.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn7.TabIndex = 84
        '
        'txtEarningEarn6
        '
        Me.txtEarningEarn6.Enabled = False
        Me.txtEarningEarn6.Location = New System.Drawing.Point(227, 291)
        Me.txtEarningEarn6.Name = "txtEarningEarn6"
        Me.txtEarningEarn6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn6.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn6.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn6.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn6.Properties.MaxLength = 9
        Me.txtEarningEarn6.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn6.TabIndex = 83
        '
        'txtEarningEarn5
        '
        Me.txtEarningEarn5.Enabled = False
        Me.txtEarningEarn5.Location = New System.Drawing.Point(227, 265)
        Me.txtEarningEarn5.Name = "txtEarningEarn5"
        Me.txtEarningEarn5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn5.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn5.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn5.Properties.MaxLength = 9
        Me.txtEarningEarn5.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn5.TabIndex = 82
        '
        'txtEarningEarn1
        '
        Me.txtEarningEarn1.Enabled = False
        Me.txtEarningEarn1.Location = New System.Drawing.Point(227, 161)
        Me.txtEarningEarn1.Name = "txtEarningEarn1"
        Me.txtEarningEarn1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn1.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn1.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn1.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.txtEarningEarn1.Properties.MaxLength = 9
        Me.txtEarningEarn1.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn1.TabIndex = 78
        '
        'txtEarningEarn4
        '
        Me.txtEarningEarn4.Enabled = False
        Me.txtEarningEarn4.Location = New System.Drawing.Point(227, 239)
        Me.txtEarningEarn4.Name = "txtEarningEarn4"
        Me.txtEarningEarn4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn4.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn4.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn4.Properties.MaxLength = 9
        Me.txtEarningEarn4.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn4.TabIndex = 81
        '
        'txtEarningEarn3
        '
        Me.txtEarningEarn3.Enabled = False
        Me.txtEarningEarn3.Location = New System.Drawing.Point(227, 213)
        Me.txtEarningEarn3.Name = "txtEarningEarn3"
        Me.txtEarningEarn3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn3.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn3.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn3.Properties.MaxLength = 9
        Me.txtEarningEarn3.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn3.TabIndex = 80
        '
        'txtEarningEarn2
        '
        Me.txtEarningEarn2.Enabled = False
        Me.txtEarningEarn2.Location = New System.Drawing.Point(227, 187)
        Me.txtEarningEarn2.Name = "txtEarningEarn2"
        Me.txtEarningEarn2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningEarn2.Properties.Appearance.Options.UseFont = True
        Me.txtEarningEarn2.Properties.Mask.EditMask = "######.##"
        Me.txtEarningEarn2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningEarn2.Properties.MaxLength = 9
        Me.txtEarningEarn2.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningEarn2.TabIndex = 79
        '
        'txtEarningBasic
        '
        Me.txtEarningBasic.Enabled = False
        Me.txtEarningBasic.Location = New System.Drawing.Point(227, 31)
        Me.txtEarningBasic.Name = "txtEarningBasic"
        Me.txtEarningBasic.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningBasic.Properties.Appearance.Options.UseFont = True
        Me.txtEarningBasic.Properties.Mask.EditMask = "######.##"
        Me.txtEarningBasic.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningBasic.Properties.MaxLength = 9
        Me.txtEarningBasic.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningBasic.TabIndex = 73
        '
        'txtEarningHRA
        '
        Me.txtEarningHRA.Enabled = False
        Me.txtEarningHRA.Location = New System.Drawing.Point(227, 83)
        Me.txtEarningHRA.Name = "txtEarningHRA"
        Me.txtEarningHRA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningHRA.Properties.Appearance.Options.UseFont = True
        Me.txtEarningHRA.Properties.Mask.EditMask = "######.##"
        Me.txtEarningHRA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningHRA.Properties.MaxLength = 9
        Me.txtEarningHRA.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningHRA.TabIndex = 77
        '
        'txtEarningMed
        '
        Me.txtEarningMed.Enabled = False
        Me.txtEarningMed.Location = New System.Drawing.Point(227, 135)
        Me.txtEarningMed.Name = "txtEarningMed"
        Me.txtEarningMed.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningMed.Properties.Appearance.Options.UseFont = True
        Me.txtEarningMed.Properties.Mask.EditMask = "######.##"
        Me.txtEarningMed.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningMed.Properties.MaxLength = 9
        Me.txtEarningMed.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningMed.TabIndex = 76
        '
        'txtEarningConv
        '
        Me.txtEarningConv.Enabled = False
        Me.txtEarningConv.Location = New System.Drawing.Point(227, 109)
        Me.txtEarningConv.Name = "txtEarningConv"
        Me.txtEarningConv.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningConv.Properties.Appearance.Options.UseFont = True
        Me.txtEarningConv.Properties.Mask.EditMask = "######.##"
        Me.txtEarningConv.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningConv.Properties.MaxLength = 9
        Me.txtEarningConv.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningConv.TabIndex = 75
        '
        'txtEarningDA
        '
        Me.txtEarningDA.Enabled = False
        Me.txtEarningDA.Location = New System.Drawing.Point(227, 57)
        Me.txtEarningDA.Name = "txtEarningDA"
        Me.txtEarningDA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarningDA.Properties.Appearance.Options.UseFont = True
        Me.txtEarningDA.Properties.Mask.EditMask = "######.##"
        Me.txtEarningDA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarningDA.Properties.MaxLength = 9
        Me.txtEarningDA.Size = New System.Drawing.Size(100, 20)
        Me.txtEarningDA.TabIndex = 74
        '
        'txtEarn10
        '
        Me.txtEarn10.Location = New System.Drawing.Point(121, 395)
        Me.txtEarn10.Name = "txtEarn10"
        Me.txtEarn10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarn10.Properties.Appearance.Options.UseFont = True
        Me.txtEarn10.Properties.Mask.EditMask = "######.##"
        Me.txtEarn10.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarn10.Properties.MaxLength = 9
        Me.txtEarn10.Size = New System.Drawing.Size(100, 20)
        Me.txtEarn10.TabIndex = 72
        '
        'txtEarn9
        '
        Me.txtEarn9.Location = New System.Drawing.Point(121, 369)
        Me.txtEarn9.Name = "txtEarn9"
        Me.txtEarn9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarn9.Properties.Appearance.Options.UseFont = True
        Me.txtEarn9.Properties.Mask.EditMask = "######.##"
        Me.txtEarn9.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarn9.Properties.MaxLength = 9
        Me.txtEarn9.Size = New System.Drawing.Size(100, 20)
        Me.txtEarn9.TabIndex = 71
        '
        'txtEarn8
        '
        Me.txtEarn8.Location = New System.Drawing.Point(121, 343)
        Me.txtEarn8.Name = "txtEarn8"
        Me.txtEarn8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarn8.Properties.Appearance.Options.UseFont = True
        Me.txtEarn8.Properties.Mask.EditMask = "######.##"
        Me.txtEarn8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarn8.Properties.MaxLength = 9
        Me.txtEarn8.Size = New System.Drawing.Size(100, 20)
        Me.txtEarn8.TabIndex = 70
        '
        'txtEarn7
        '
        Me.txtEarn7.Location = New System.Drawing.Point(121, 317)
        Me.txtEarn7.Name = "txtEarn7"
        Me.txtEarn7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarn7.Properties.Appearance.Options.UseFont = True
        Me.txtEarn7.Properties.Mask.EditMask = "######.##"
        Me.txtEarn7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarn7.Properties.MaxLength = 9
        Me.txtEarn7.Size = New System.Drawing.Size(100, 20)
        Me.txtEarn7.TabIndex = 69
        '
        'txtEarn6
        '
        Me.txtEarn6.Location = New System.Drawing.Point(121, 291)
        Me.txtEarn6.Name = "txtEarn6"
        Me.txtEarn6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarn6.Properties.Appearance.Options.UseFont = True
        Me.txtEarn6.Properties.Mask.EditMask = "######.##"
        Me.txtEarn6.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarn6.Properties.MaxLength = 9
        Me.txtEarn6.Size = New System.Drawing.Size(100, 20)
        Me.txtEarn6.TabIndex = 68
        '
        'txtEarn5
        '
        Me.txtEarn5.Location = New System.Drawing.Point(121, 265)
        Me.txtEarn5.Name = "txtEarn5"
        Me.txtEarn5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarn5.Properties.Appearance.Options.UseFont = True
        Me.txtEarn5.Properties.Mask.EditMask = "######.##"
        Me.txtEarn5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarn5.Properties.MaxLength = 9
        Me.txtEarn5.Size = New System.Drawing.Size(100, 20)
        Me.txtEarn5.TabIndex = 67
        '
        'txtEarn1
        '
        Me.txtEarn1.Location = New System.Drawing.Point(121, 161)
        Me.txtEarn1.Name = "txtEarn1"
        Me.txtEarn1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarn1.Properties.Appearance.Options.UseFont = True
        Me.txtEarn1.Properties.Mask.EditMask = "######.##"
        Me.txtEarn1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarn1.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.txtEarn1.Properties.MaxLength = 9
        Me.txtEarn1.Size = New System.Drawing.Size(100, 20)
        Me.txtEarn1.TabIndex = 63
        '
        'txtEarn4
        '
        Me.txtEarn4.Location = New System.Drawing.Point(121, 239)
        Me.txtEarn4.Name = "txtEarn4"
        Me.txtEarn4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarn4.Properties.Appearance.Options.UseFont = True
        Me.txtEarn4.Properties.Mask.EditMask = "######.##"
        Me.txtEarn4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarn4.Properties.MaxLength = 9
        Me.txtEarn4.Size = New System.Drawing.Size(100, 20)
        Me.txtEarn4.TabIndex = 66
        '
        'txtEarn3
        '
        Me.txtEarn3.Location = New System.Drawing.Point(121, 213)
        Me.txtEarn3.Name = "txtEarn3"
        Me.txtEarn3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarn3.Properties.Appearance.Options.UseFont = True
        Me.txtEarn3.Properties.Mask.EditMask = "######.##"
        Me.txtEarn3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarn3.Properties.MaxLength = 9
        Me.txtEarn3.Size = New System.Drawing.Size(100, 20)
        Me.txtEarn3.TabIndex = 65
        '
        'txtEarn2
        '
        Me.txtEarn2.Location = New System.Drawing.Point(121, 187)
        Me.txtEarn2.Name = "txtEarn2"
        Me.txtEarn2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarn2.Properties.Appearance.Options.UseFont = True
        Me.txtEarn2.Properties.Mask.EditMask = "######.##"
        Me.txtEarn2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarn2.Properties.MaxLength = 9
        Me.txtEarn2.Size = New System.Drawing.Size(100, 20)
        Me.txtEarn2.TabIndex = 64
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(158, 11)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(30, 14)
        Me.LabelControl3.TabIndex = 62
        Me.LabelControl3.Text = "Rates"
        '
        'txtBasic
        '
        Me.txtBasic.EditValue = ""
        Me.txtBasic.Location = New System.Drawing.Point(121, 31)
        Me.txtBasic.Name = "txtBasic"
        Me.txtBasic.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtBasic.Properties.Appearance.Options.UseFont = True
        Me.txtBasic.Properties.Mask.EditMask = "######.##"
        Me.txtBasic.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtBasic.Properties.MaxLength = 9
        Me.txtBasic.Size = New System.Drawing.Size(100, 20)
        Me.txtBasic.TabIndex = 53
        '
        'txtHRA
        '
        Me.txtHRA.Location = New System.Drawing.Point(121, 83)
        Me.txtHRA.Name = "txtHRA"
        Me.txtHRA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtHRA.Properties.Appearance.Options.UseFont = True
        Me.txtHRA.Properties.Mask.EditMask = "######.##"
        Me.txtHRA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtHRA.Properties.MaxLength = 9
        Me.txtHRA.Size = New System.Drawing.Size(100, 20)
        Me.txtHRA.TabIndex = 61
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(27, 86)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(23, 14)
        Me.LabelControl16.TabIndex = 60
        Me.LabelControl16.Text = "HRA"
        '
        'txtMed
        '
        Me.txtMed.Location = New System.Drawing.Point(121, 135)
        Me.txtMed.Name = "txtMed"
        Me.txtMed.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtMed.Properties.Appearance.Options.UseFont = True
        Me.txtMed.Properties.Mask.EditMask = "######.##"
        Me.txtMed.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtMed.Properties.MaxLength = 9
        Me.txtMed.Size = New System.Drawing.Size(100, 20)
        Me.txtMed.TabIndex = 59
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(27, 138)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(39, 14)
        Me.LabelControl13.TabIndex = 58
        Me.LabelControl13.Text = "Medical"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(27, 34)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(26, 14)
        Me.LabelControl8.TabIndex = 52
        Me.LabelControl8.Text = "Basic"
        '
        'txtConv
        '
        Me.txtConv.Location = New System.Drawing.Point(121, 109)
        Me.txtConv.Name = "txtConv"
        Me.txtConv.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtConv.Properties.Appearance.Options.UseFont = True
        Me.txtConv.Properties.Mask.EditMask = "######.##"
        Me.txtConv.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtConv.Properties.MaxLength = 9
        Me.txtConv.Size = New System.Drawing.Size(100, 20)
        Me.txtConv.TabIndex = 57
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(27, 112)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl12.TabIndex = 56
        Me.LabelControl12.Text = "Conveyance"
        '
        'txtDA
        '
        Me.txtDA.Location = New System.Drawing.Point(121, 57)
        Me.txtDA.Name = "txtDA"
        Me.txtDA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtDA.Properties.Appearance.Options.UseFont = True
        Me.txtDA.Properties.Mask.EditMask = "######.##"
        Me.txtDA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtDA.Properties.MaxLength = 9
        Me.txtDA.Size = New System.Drawing.Size(100, 20)
        Me.txtDA.TabIndex = 55
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(27, 60)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(16, 14)
        Me.LabelControl11.TabIndex = 54
        Me.LabelControl11.Text = "DA"
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.btnApply)
        Me.SidePanel1.Controls.Add(Me.SimpleButtonSave)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel1.Location = New System.Drawing.Point(0, 534)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1165, 34)
        Me.SidePanel1.TabIndex = 41
        Me.SidePanel1.Text = "SidePanel1"
        '
        'btnApply
        '
        Me.btnApply.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.btnApply.Appearance.Options.UseFont = True
        Me.btnApply.Enabled = False
        Me.btnApply.Location = New System.Drawing.Point(7, 6)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(75, 23)
        Me.btnApply.TabIndex = 17
        Me.btnApply.Text = "Apply"
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Enabled = False
        Me.SimpleButtonSave.Location = New System.Drawing.Point(88, 6)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 16
        Me.SimpleButtonSave.Text = "Save"
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.GroupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl1.Controls.Add(Me.LabelControl20)
        Me.GroupControl1.Controls.Add(Me.txtapplicableto)
        Me.GroupControl1.Controls.Add(Me.LabelControl19)
        Me.GroupControl1.Controls.Add(Me.txtapplicablefrom)
        Me.GroupControl1.Controls.Add(Me.ComboNepaliYear)
        Me.GroupControl1.Controls.Add(Me.ComboNEpaliMonth)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.LookUpEdit1)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.txtPayableMonth)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.lblName)
        Me.GroupControl1.Controls.Add(Me.lblDept)
        Me.GroupControl1.Controls.Add(Me.lblCardNum)
        Me.GroupControl1.Controls.Add(Me.LabelControl9)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(1165, 100)
        Me.GroupControl1.TabIndex = 39
        Me.GroupControl1.Text = "Employee"
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl20.Appearance.Options.UseFont = True
        Me.LabelControl20.Location = New System.Drawing.Point(848, 42)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(54, 14)
        Me.LabelControl20.TabIndex = 43
        Me.LabelControl20.Text = "To Month"
        '
        'txtapplicableto
        '
        Me.txtapplicableto.EditValue = New Date(2018, 1, 1, 16, 13, 14, 0)
        Me.txtapplicableto.Location = New System.Drawing.Point(908, 39)
        Me.txtapplicableto.Name = "txtapplicableto"
        Me.txtapplicableto.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtapplicableto.Properties.Appearance.Options.UseFont = True
        Me.txtapplicableto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtapplicableto.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtapplicableto.Properties.Mask.EditMask = "MM/yyyy"
        Me.txtapplicableto.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtapplicableto.Size = New System.Drawing.Size(100, 20)
        Me.txtapplicableto.TabIndex = 42
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Location = New System.Drawing.Point(638, 42)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl19.TabIndex = 41
        Me.LabelControl19.Text = "From Month"
        '
        'txtapplicablefrom
        '
        Me.txtapplicablefrom.EditValue = New Date(2018, 1, 1, 16, 13, 14, 0)
        Me.txtapplicablefrom.Location = New System.Drawing.Point(715, 39)
        Me.txtapplicablefrom.Name = "txtapplicablefrom"
        Me.txtapplicablefrom.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtapplicablefrom.Properties.Appearance.Options.UseFont = True
        Me.txtapplicablefrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtapplicablefrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtapplicablefrom.Properties.Mask.EditMask = "MM/yyyy"
        Me.txtapplicablefrom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtapplicablefrom.Size = New System.Drawing.Size(100, 20)
        Me.txtapplicablefrom.TabIndex = 40
        '
        'ComboNepaliYear
        '
        Me.ComboNepaliYear.Location = New System.Drawing.Point(527, 39)
        Me.ComboNepaliYear.Name = "ComboNepaliYear"
        Me.ComboNepaliYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYear.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYear.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliYear.TabIndex = 39
        '
        'ComboNEpaliMonth
        '
        Me.ComboNEpaliMonth.Location = New System.Drawing.Point(455, 39)
        Me.ComboNEpaliMonth.Name = "ComboNEpaliMonth"
        Me.ComboNEpaliMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonth.Size = New System.Drawing.Size(66, 20)
        Me.ComboNEpaliMonth.TabIndex = 38
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(21, 42)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(84, 14)
        Me.LabelControl5.TabIndex = 21
        Me.LabelControl5.Text = "Select Paycode"
        '
        'LookUpEdit1
        '
        Me.LookUpEdit1.Location = New System.Drawing.Point(127, 40)
        Me.LookUpEdit1.Name = "LookUpEdit1"
        Me.LookUpEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.Appearance.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceFocused.Options.UseFont = True
        Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PAYCODE", "PAYCODE", 30, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EMPNAME", "Name")})
        Me.LookUpEdit1.Properties.DisplayMember = "PAYCODE"
        Me.LookUpEdit1.Properties.MaxLength = 12
        Me.LookUpEdit1.Properties.NullText = ""
        Me.LookUpEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.LookUpEdit1.Properties.ValueMember = "PAYCODE"
        Me.LookUpEdit1.Size = New System.Drawing.Size(129, 20)
        Me.LookUpEdit1.TabIndex = 1
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(311, 42)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(134, 14)
        Me.LabelControl6.TabIndex = 16
        Me.LabelControl6.Text = "Payable In the Month of"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(21, 74)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(31, 14)
        Me.LabelControl1.TabIndex = 32
        Me.LabelControl1.Text = "Name"
        '
        'txtPayableMonth
        '
        Me.txtPayableMonth.EditValue = New Date(2018, 1, 1, 16, 13, 14, 0)
        Me.txtPayableMonth.Location = New System.Drawing.Point(455, 39)
        Me.txtPayableMonth.Name = "txtPayableMonth"
        Me.txtPayableMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtPayableMonth.Properties.Appearance.Options.UseFont = True
        Me.txtPayableMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtPayableMonth.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtPayableMonth.Properties.Mask.EditMask = "MM/yyyy"
        Me.txtPayableMonth.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPayableMonth.Size = New System.Drawing.Size(100, 20)
        Me.txtPayableMonth.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(431, 74)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(47, 14)
        Me.LabelControl2.TabIndex = 33
        Me.LabelControl2.Text = "Card No."
        '
        'lblName
        '
        Me.lblName.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblName.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblName.Appearance.Options.UseFont = True
        Me.lblName.Appearance.Options.UseForeColor = True
        Me.lblName.Location = New System.Drawing.Point(127, 74)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(20, 14)
        Me.lblName.TabIndex = 35
        Me.lblName.Text = "     "
        '
        'lblDept
        '
        Me.lblDept.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblDept.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblDept.Appearance.Options.UseFont = True
        Me.lblDept.Appearance.Options.UseForeColor = True
        Me.lblDept.Location = New System.Drawing.Point(811, 74)
        Me.lblDept.Name = "lblDept"
        Me.lblDept.Size = New System.Drawing.Size(24, 14)
        Me.lblDept.TabIndex = 37
        Me.lblDept.Text = "      "
        '
        'lblCardNum
        '
        Me.lblCardNum.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblCardNum.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblCardNum.Appearance.Options.UseFont = True
        Me.lblCardNum.Appearance.Options.UseForeColor = True
        Me.lblCardNum.Location = New System.Drawing.Point(543, 74)
        Me.lblCardNum.Name = "lblCardNum"
        Me.lblCardNum.Size = New System.Drawing.Size(24, 14)
        Me.lblCardNum.TabIndex = 36
        Me.lblCardNum.Text = "      "
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(699, 74)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl9.TabIndex = 34
        Me.LabelControl9.Text = "Department"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(77, 568)
        Me.MemoEdit1.TabIndex = 4
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'XtraPayArrear
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraPayArrear"
        Me.Size = New System.Drawing.Size(1250, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        Me.SidePanel2.ResumeLayout(False)
        Me.SidePanel2.PerformLayout()
        CType(Me.mskPaidDays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.optPayable2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optPayable1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.TXTPAYdAYS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAmtonESI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAmtonVPf.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAmtonPf.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtcalculatedFpf.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtcalculatedEpf.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCalculatedPF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtESIAmount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCalculatedVPF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningEarn2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningBasic.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningHRA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningMed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningConv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarningDA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarn10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarn9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarn8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarn7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarn6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarn5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarn1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarn4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarn3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarn2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBasic.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtHRA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtConv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtapplicableto.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtapplicableto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtapplicablefrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtapplicablefrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPayableMonth.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPayableMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents txtPayableMonth As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents SSSDBDataSet As ULtra.SSSDBDataSet
    Friend WithEvents TblEmployeeTableAdapter As ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblEmployee1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblDept As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCardNum As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ComboNepaliYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonth As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtapplicablefrom As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtapplicableto As DevExpress.XtraEditors.DateEdit
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanel2 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents txtBasic As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtHRA As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtMed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtConv As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtDA As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtEarn10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarn9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarn8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarn7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarn6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarn5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarn1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarn4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarn3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarn2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtEarningEarn10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarningEarn9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarningEarn8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarningEarn7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarningEarn6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarningEarn5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarningEarn1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarningEarn4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarningEarn3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarningEarn2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarningBasic As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarningHRA As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarningMed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarningConv As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEarningDA As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCalculatedPF As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtESIAmount As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtCalculatedVPF As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TXTPAYdAYS As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtAmtonESI As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtAmtonVPf As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtAmtonPf As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtcalculatedFpf As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtcalculatedEpf As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents optPayable2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents optPayable1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents mskPaidDays As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnApply As DevExpress.XtraEditors.SimpleButton

End Class
