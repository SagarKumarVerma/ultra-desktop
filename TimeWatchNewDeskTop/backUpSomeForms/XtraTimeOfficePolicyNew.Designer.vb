﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class XtraTimeOfficePolicyNew
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraTimeOfficePolicyNew))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.ToggleSwitchCanteen = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleSwitchIsNepali = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl56 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchVisitor = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl55 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl54 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.ToggleOnlineEvents = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleDownloadAtStartUp = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl58 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditTimerDur = New DevExpress.XtraEditors.TextEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleCompl = New DevExpress.XtraEditors.ToggleSwitch()
        Me.PanelControlIOCL = New DevExpress.XtraEditors.PanelControl()
        Me.TextLink = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.TextIoclInterval = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleIsIOCL = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.TextBio1EcoPort = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextInActiveDays = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditAutoDwnDur = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl60 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleRealTime = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl57 = New DevExpress.XtraEditors.LabelControl()
        Me.TextTWIR102Port = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl59 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditZKPort = New DevExpress.XtraEditors.TextEdit()
        Me.ToggleLeaveAsPerFinancialYear = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl53 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditBioPort = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblSetupBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New ULtra.SSSDBDataSet()
        Me.TblSetupTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblSetupTableAdapter()
        Me.TblSetup1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblSetup1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.ToggleSwitchCanteen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchIsNepali.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchVisitor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.ToggleOnlineEvents.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleDownloadAtStartUp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditTimerDur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.ToggleCompl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControlIOCL, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControlIOCL.SuspendLayout()
        CType(Me.TextLink.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextIoclInterval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleIsIOCL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBio1EcoPort.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextInActiveDays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditAutoDwnDur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleRealTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextTWIR102Port.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditZKPort.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleLeaveAsPerFinancialYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditBioPort.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblSetupBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PanelControl4)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PanelControl3)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PanelControl2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PanelControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.SimpleButtonSave)
        Me.PanelControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl4.Location = New System.Drawing.Point(0, 502)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(1036, 66)
        Me.PanelControl4.TabIndex = 4
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(14, 20)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 1
        Me.SimpleButtonSave.Text = "Save"
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.ToggleSwitchCanteen)
        Me.PanelControl3.Controls.Add(Me.ToggleSwitchIsNepali)
        Me.PanelControl3.Controls.Add(Me.LabelControl56)
        Me.PanelControl3.Controls.Add(Me.ToggleSwitchVisitor)
        Me.PanelControl3.Controls.Add(Me.LabelControl55)
        Me.PanelControl3.Controls.Add(Me.LabelControl54)
        Me.PanelControl3.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl3.Location = New System.Drawing.Point(0, 369)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(1036, 133)
        Me.PanelControl3.TabIndex = 3
        '
        'ToggleSwitchCanteen
        '
        Me.ToggleSwitchCanteen.Location = New System.Drawing.Point(240, 20)
        Me.ToggleSwitchCanteen.Name = "ToggleSwitchCanteen"
        Me.ToggleSwitchCanteen.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchCanteen.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchCanteen.Properties.OffText = "No"
        Me.ToggleSwitchCanteen.Properties.OnText = "Yes"
        Me.ToggleSwitchCanteen.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchCanteen.TabIndex = 1
        '
        'ToggleSwitchIsNepali
        '
        Me.ToggleSwitchIsNepali.Location = New System.Drawing.Point(240, 82)
        Me.ToggleSwitchIsNepali.Name = "ToggleSwitchIsNepali"
        Me.ToggleSwitchIsNepali.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchIsNepali.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchIsNepali.Properties.OffText = "No"
        Me.ToggleSwitchIsNepali.Properties.OnText = "Yes"
        Me.ToggleSwitchIsNepali.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchIsNepali.TabIndex = 3
        Me.ToggleSwitchIsNepali.Visible = False
        '
        'LabelControl56
        '
        Me.LabelControl56.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl56.Appearance.Options.UseFont = True
        Me.LabelControl56.Location = New System.Drawing.Point(18, 87)
        Me.LabelControl56.Name = "LabelControl56"
        Me.LabelControl56.Size = New System.Drawing.Size(106, 14)
        Me.LabelControl56.TabIndex = 91
        Me.LabelControl56.Text = "Use Nepali Calendar"
        Me.LabelControl56.Visible = False
        '
        'ToggleSwitchVisitor
        '
        Me.ToggleSwitchVisitor.Location = New System.Drawing.Point(240, 51)
        Me.ToggleSwitchVisitor.Name = "ToggleSwitchVisitor"
        Me.ToggleSwitchVisitor.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchVisitor.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchVisitor.Properties.OffText = "No"
        Me.ToggleSwitchVisitor.Properties.OnText = "Yes"
        Me.ToggleSwitchVisitor.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchVisitor.TabIndex = 2
        '
        'LabelControl55
        '
        Me.LabelControl55.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl55.Appearance.Options.UseFont = True
        Me.LabelControl55.Location = New System.Drawing.Point(18, 25)
        Me.LabelControl55.Name = "LabelControl55"
        Me.LabelControl55.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl55.TabIndex = 89
        Me.LabelControl55.Text = "Enable Canteen"
        '
        'LabelControl54
        '
        Me.LabelControl54.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl54.Appearance.Options.UseFont = True
        Me.LabelControl54.Location = New System.Drawing.Point(18, 56)
        Me.LabelControl54.Name = "LabelControl54"
        Me.LabelControl54.Size = New System.Drawing.Size(73, 14)
        Me.LabelControl54.TabIndex = 90
        Me.LabelControl54.Text = "Enable Visitor"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.ToggleOnlineEvents)
        Me.PanelControl2.Controls.Add(Me.LabelControl39)
        Me.PanelControl2.Controls.Add(Me.LabelControl40)
        Me.PanelControl2.Controls.Add(Me.ToggleDownloadAtStartUp)
        Me.PanelControl2.Controls.Add(Me.LabelControl58)
        Me.PanelControl2.Controls.Add(Me.TextEditTimerDur)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl2.Location = New System.Drawing.Point(0, 246)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(1036, 123)
        Me.PanelControl2.TabIndex = 2
        Me.PanelControl2.Visible = False
        '
        'ToggleOnlineEvents
        '
        Me.ToggleOnlineEvents.Location = New System.Drawing.Point(240, 17)
        Me.ToggleOnlineEvents.Name = "ToggleOnlineEvents"
        Me.ToggleOnlineEvents.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleOnlineEvents.Properties.Appearance.Options.UseFont = True
        Me.ToggleOnlineEvents.Properties.OffText = "No"
        Me.ToggleOnlineEvents.Properties.OnText = "Yes"
        Me.ToggleOnlineEvents.Size = New System.Drawing.Size(95, 25)
        Me.ToggleOnlineEvents.TabIndex = 1
        '
        'LabelControl39
        '
        Me.LabelControl39.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl39.Appearance.Options.UseFont = True
        Me.LabelControl39.Location = New System.Drawing.Point(18, 22)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(71, 14)
        Me.LabelControl39.TabIndex = 84
        Me.LabelControl39.Text = "Cloud Events"
        '
        'LabelControl40
        '
        Me.LabelControl40.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl40.Appearance.Options.UseFont = True
        Me.LabelControl40.Location = New System.Drawing.Point(18, 71)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(114, 14)
        Me.LabelControl40.TabIndex = 85
        Me.LabelControl40.Text = "Download at Startup"
        '
        'ToggleDownloadAtStartUp
        '
        Me.ToggleDownloadAtStartUp.Location = New System.Drawing.Point(240, 66)
        Me.ToggleDownloadAtStartUp.Name = "ToggleDownloadAtStartUp"
        Me.ToggleDownloadAtStartUp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleDownloadAtStartUp.Properties.Appearance.Options.UseFont = True
        Me.ToggleDownloadAtStartUp.Properties.OffText = "No"
        Me.ToggleDownloadAtStartUp.Properties.OnText = "Yes"
        Me.ToggleDownloadAtStartUp.Size = New System.Drawing.Size(95, 25)
        Me.ToggleDownloadAtStartUp.TabIndex = 3
        '
        'LabelControl58
        '
        Me.LabelControl58.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl58.Appearance.Options.UseFont = True
        Me.LabelControl58.Location = New System.Drawing.Point(19, 47)
        Me.LabelControl58.Name = "LabelControl58"
        Me.LabelControl58.Size = New System.Drawing.Size(158, 14)
        Me.LabelControl58.TabIndex = 92
        Me.LabelControl58.Text = "Cloud Events Timer(Minutes)"
        '
        'TextEditTimerDur
        '
        Me.TextEditTimerDur.EditValue = "10"
        Me.TextEditTimerDur.Location = New System.Drawing.Point(240, 44)
        Me.TextEditTimerDur.Name = "TextEditTimerDur"
        Me.TextEditTimerDur.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextEditTimerDur.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditTimerDur.Properties.Appearance.Options.UseFont = True
        Me.TextEditTimerDur.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditTimerDur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditTimerDur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditTimerDur.Properties.MaxLength = 4
        Me.TextEditTimerDur.Size = New System.Drawing.Size(72, 20)
        Me.TextEditTimerDur.TabIndex = 2
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.ToggleCompl)
        Me.PanelControl1.Controls.Add(Me.PanelControlIOCL)
        Me.PanelControl1.Controls.Add(Me.ToggleIsIOCL)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.TextBio1EcoPort)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.TextInActiveDays)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.TextEditAutoDwnDur)
        Me.PanelControl1.Controls.Add(Me.LabelControl60)
        Me.PanelControl1.Controls.Add(Me.ToggleRealTime)
        Me.PanelControl1.Controls.Add(Me.LabelControl57)
        Me.PanelControl1.Controls.Add(Me.TextTWIR102Port)
        Me.PanelControl1.Controls.Add(Me.LabelControl59)
        Me.PanelControl1.Controls.Add(Me.TextEditZKPort)
        Me.PanelControl1.Controls.Add(Me.ToggleLeaveAsPerFinancialYear)
        Me.PanelControl1.Controls.Add(Me.LabelControl41)
        Me.PanelControl1.Controls.Add(Me.LabelControl53)
        Me.PanelControl1.Controls.Add(Me.TextEditBioPort)
        Me.PanelControl1.Controls.Add(Me.LabelControl30)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(1036, 246)
        Me.PanelControl1.TabIndex = 1
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(18, 208)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(74, 14)
        Me.LabelControl10.TabIndex = 107
        Me.LabelControl10.Text = "Is Compliance"
        Me.LabelControl10.Visible = False
        '
        'ToggleCompl
        '
        Me.ToggleCompl.Location = New System.Drawing.Point(240, 203)
        Me.ToggleCompl.Name = "ToggleCompl"
        Me.ToggleCompl.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleCompl.Properties.Appearance.Options.UseFont = True
        Me.ToggleCompl.Properties.OffText = "No"
        Me.ToggleCompl.Properties.OnText = "Yes"
        Me.ToggleCompl.Size = New System.Drawing.Size(95, 25)
        Me.ToggleCompl.TabIndex = 106
        Me.ToggleCompl.Visible = False
        '
        'PanelControlIOCL
        '
        Me.PanelControlIOCL.Controls.Add(Me.TextLink)
        Me.PanelControlIOCL.Controls.Add(Me.LabelControl6)
        Me.PanelControlIOCL.Controls.Add(Me.TextIoclInterval)
        Me.PanelControlIOCL.Controls.Add(Me.LabelControl5)
        Me.PanelControlIOCL.Location = New System.Drawing.Point(513, 76)
        Me.PanelControlIOCL.Name = "PanelControlIOCL"
        Me.PanelControlIOCL.Size = New System.Drawing.Size(381, 121)
        Me.PanelControlIOCL.TabIndex = 105
        Me.PanelControlIOCL.Visible = False
        '
        'TextLink
        '
        Me.TextLink.Location = New System.Drawing.Point(114, 38)
        Me.TextLink.Name = "TextLink"
        Me.TextLink.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextLink.Properties.Appearance.Options.UseFont = True
        Me.TextLink.Size = New System.Drawing.Size(251, 69)
        Me.TextLink.TabIndex = 27
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(17, 41)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(63, 14)
        Me.LabelControl6.TabIndex = 8
        Me.LabelControl6.Text = "Upload Link"
        '
        'TextIoclInterval
        '
        Me.TextIoclInterval.EditValue = "00:05"
        Me.TextIoclInterval.Location = New System.Drawing.Point(222, 12)
        Me.TextIoclInterval.Name = "TextIoclInterval"
        Me.TextIoclInterval.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextIoclInterval.Properties.Appearance.Options.UseFont = True
        Me.TextIoclInterval.Properties.Mask.EditMask = "HH:mm"
        Me.TextIoclInterval.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextIoclInterval.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextIoclInterval.Properties.MaxLength = 5
        Me.TextIoclInterval.Size = New System.Drawing.Size(72, 20)
        Me.TextIoclInterval.TabIndex = 5
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(17, 15)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(82, 14)
        Me.LabelControl5.TabIndex = 6
        Me.LabelControl5.Text = "Upload Interval"
        '
        'ToggleIsIOCL
        '
        Me.ToggleIsIOCL.Location = New System.Drawing.Point(743, 45)
        Me.ToggleIsIOCL.Name = "ToggleIsIOCL"
        Me.ToggleIsIOCL.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleIsIOCL.Properties.Appearance.Options.UseFont = True
        Me.ToggleIsIOCL.Properties.OffText = "No"
        Me.ToggleIsIOCL.Properties.OnText = "Yes"
        Me.ToggleIsIOCL.Size = New System.Drawing.Size(95, 25)
        Me.ToggleIsIOCL.TabIndex = 103
        Me.ToggleIsIOCL.Visible = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(521, 50)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(39, 14)
        Me.LabelControl4.TabIndex = 104
        Me.LabelControl4.Text = "Is IOCL"
        Me.LabelControl4.Visible = False
        '
        'TextBio1EcoPort
        '
        Me.TextBio1EcoPort.EditValue = "9000"
        Me.TextBio1EcoPort.Location = New System.Drawing.Point(240, 151)
        Me.TextBio1EcoPort.Name = "TextBio1EcoPort"
        Me.TextBio1EcoPort.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextBio1EcoPort.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextBio1EcoPort.Properties.Appearance.Options.UseFont = True
        Me.TextBio1EcoPort.Properties.Mask.EditMask = "[0-9]*"
        Me.TextBio1EcoPort.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextBio1EcoPort.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextBio1EcoPort.Properties.MaxLength = 5
        Me.TextBio1EcoPort.Size = New System.Drawing.Size(72, 20)
        Me.TextBio1EcoPort.TabIndex = 6
        Me.TextBio1EcoPort.Visible = False
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(17, 154)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(185, 14)
        Me.LabelControl3.TabIndex = 102
        Me.LabelControl3.Text = "Bio1Eco Real Time Download Port"
        Me.LabelControl3.Visible = False
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(327, 180)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(134, 14)
        Me.LabelControl2.TabIndex = 100
        Me.LabelControl2.Text = "Days continuous absent "
        Me.LabelControl2.Visible = False
        '
        'TextInActiveDays
        '
        Me.TextInActiveDays.EditValue = "0"
        Me.TextInActiveDays.Location = New System.Drawing.Point(240, 177)
        Me.TextInActiveDays.Name = "TextInActiveDays"
        Me.TextInActiveDays.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextInActiveDays.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextInActiveDays.Properties.Appearance.Options.UseFont = True
        Me.TextInActiveDays.Properties.Mask.EditMask = "[0-9]*"
        Me.TextInActiveDays.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextInActiveDays.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextInActiveDays.Properties.MaxLength = 3
        Me.TextInActiveDays.Size = New System.Drawing.Size(72, 20)
        Me.TextInActiveDays.TabIndex = 7
        Me.TextInActiveDays.Visible = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(17, 180)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(167, 14)
        Me.LabelControl1.TabIndex = 99
        Me.LabelControl1.Text = "Employee Mark Inactive  after "
        Me.LabelControl1.Visible = False
        '
        'TextEditAutoDwnDur
        '
        Me.TextEditAutoDwnDur.EditValue = "240"
        Me.TextEditAutoDwnDur.Location = New System.Drawing.Point(240, 47)
        Me.TextEditAutoDwnDur.Name = "TextEditAutoDwnDur"
        Me.TextEditAutoDwnDur.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextEditAutoDwnDur.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditAutoDwnDur.Properties.Appearance.Options.UseFont = True
        Me.TextEditAutoDwnDur.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditAutoDwnDur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditAutoDwnDur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditAutoDwnDur.Properties.MaxLength = 3
        Me.TextEditAutoDwnDur.Size = New System.Drawing.Size(72, 20)
        Me.TextEditAutoDwnDur.TabIndex = 2
        '
        'LabelControl60
        '
        Me.LabelControl60.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl60.Appearance.Options.UseFont = True
        Me.LabelControl60.Location = New System.Drawing.Point(18, 50)
        Me.LabelControl60.Name = "LabelControl60"
        Me.LabelControl60.Size = New System.Drawing.Size(172, 14)
        Me.LabelControl60.TabIndex = 97
        Me.LabelControl60.Text = "Auto Download Duration (Mins)"
        '
        'ToggleRealTime
        '
        Me.ToggleRealTime.Location = New System.Drawing.Point(240, 16)
        Me.ToggleRealTime.Name = "ToggleRealTime"
        Me.ToggleRealTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleRealTime.Properties.Appearance.Options.UseFont = True
        Me.ToggleRealTime.Properties.OffText = "No"
        Me.ToggleRealTime.Properties.OnText = "Yes"
        Me.ToggleRealTime.Size = New System.Drawing.Size(95, 25)
        Me.ToggleRealTime.TabIndex = 1
        '
        'LabelControl57
        '
        Me.LabelControl57.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl57.Appearance.Options.UseFont = True
        Me.LabelControl57.Location = New System.Drawing.Point(18, 21)
        Me.LabelControl57.Name = "LabelControl57"
        Me.LabelControl57.Size = New System.Drawing.Size(150, 14)
        Me.LabelControl57.TabIndex = 96
        Me.LabelControl57.Text = "Start Real Time On StartUp"
        '
        'TextTWIR102Port
        '
        Me.TextTWIR102Port.EditValue = "15001"
        Me.TextTWIR102Port.Location = New System.Drawing.Point(240, 125)
        Me.TextTWIR102Port.Name = "TextTWIR102Port"
        Me.TextTWIR102Port.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextTWIR102Port.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextTWIR102Port.Properties.Appearance.Options.UseFont = True
        Me.TextTWIR102Port.Properties.Mask.EditMask = "[0-9]*"
        Me.TextTWIR102Port.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextTWIR102Port.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextTWIR102Port.Properties.MaxLength = 5
        Me.TextTWIR102Port.Size = New System.Drawing.Size(72, 20)
        Me.TextTWIR102Port.TabIndex = 5
        Me.TextTWIR102Port.Visible = False
        '
        'LabelControl59
        '
        Me.LabelControl59.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl59.Appearance.Options.UseFont = True
        Me.LabelControl59.Location = New System.Drawing.Point(17, 128)
        Me.LabelControl59.Name = "LabelControl59"
        Me.LabelControl59.Size = New System.Drawing.Size(194, 14)
        Me.LabelControl59.TabIndex = 93
        Me.LabelControl59.Text = "TWIR102 Real Time Download Port"
        Me.LabelControl59.Visible = False
        '
        'TextEditZKPort
        '
        Me.TextEditZKPort.EditValue = "8081"
        Me.TextEditZKPort.Location = New System.Drawing.Point(240, 99)
        Me.TextEditZKPort.Name = "TextEditZKPort"
        Me.TextEditZKPort.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextEditZKPort.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditZKPort.Properties.Appearance.Options.UseFont = True
        Me.TextEditZKPort.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditZKPort.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditZKPort.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditZKPort.Properties.MaxLength = 4
        Me.TextEditZKPort.Size = New System.Drawing.Size(72, 20)
        Me.TextEditZKPort.TabIndex = 4
        Me.TextEditZKPort.Visible = False
        '
        'ToggleLeaveAsPerFinancialYear
        '
        Me.ToggleLeaveAsPerFinancialYear.Location = New System.Drawing.Point(743, 10)
        Me.ToggleLeaveAsPerFinancialYear.Name = "ToggleLeaveAsPerFinancialYear"
        Me.ToggleLeaveAsPerFinancialYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleLeaveAsPerFinancialYear.Properties.Appearance.Options.UseFont = True
        Me.ToggleLeaveAsPerFinancialYear.Properties.OffText = "No"
        Me.ToggleLeaveAsPerFinancialYear.Properties.OnText = "Yes"
        Me.ToggleLeaveAsPerFinancialYear.Size = New System.Drawing.Size(95, 25)
        Me.ToggleLeaveAsPerFinancialYear.TabIndex = 77
        '
        'LabelControl41
        '
        Me.LabelControl41.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl41.Appearance.Options.UseFont = True
        Me.LabelControl41.Location = New System.Drawing.Point(521, 15)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(146, 14)
        Me.LabelControl41.TabIndex = 86
        Me.LabelControl41.Text = "Leave as per Financial Year"
        '
        'LabelControl53
        '
        Me.LabelControl53.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl53.Appearance.Options.UseFont = True
        Me.LabelControl53.Location = New System.Drawing.Point(17, 102)
        Me.LabelControl53.Name = "LabelControl53"
        Me.LabelControl53.Size = New System.Drawing.Size(199, 14)
        Me.LabelControl53.TabIndex = 88
        Me.LabelControl53.Text = "ZK/Bio Pro Real Time Download Port"
        Me.LabelControl53.Visible = False
        '
        'TextEditBioPort
        '
        Me.TextEditBioPort.EditValue = "7005"
        Me.TextEditBioPort.Location = New System.Drawing.Point(240, 73)
        Me.TextEditBioPort.Name = "TextEditBioPort"
        Me.TextEditBioPort.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextEditBioPort.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditBioPort.Properties.Appearance.Options.UseFont = True
        Me.TextEditBioPort.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditBioPort.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditBioPort.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditBioPort.Properties.MaxLength = 4
        Me.TextEditBioPort.Size = New System.Drawing.Size(72, 20)
        Me.TextEditBioPort.TabIndex = 3
        Me.TextEditBioPort.Visible = False
        '
        'LabelControl30
        '
        Me.LabelControl30.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl30.Appearance.Options.UseFont = True
        Me.LabelControl30.Location = New System.Drawing.Point(18, 76)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(212, 14)
        Me.LabelControl30.TabIndex = 87
        Me.LabelControl30.Text = "Bio Series/F9 Real Time Download Port"
        Me.LabelControl30.Visible = False
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(104, 568)
        Me.MemoEdit1.TabIndex = 1
        '
        'TblSetupBindingSource
        '
        Me.TblSetupBindingSource.DataMember = "tblSetup"
        Me.TblSetupBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblSetupTableAdapter
        '
        Me.TblSetupTableAdapter.ClearBeforeFill = True
        '
        'TblSetup1TableAdapter1
        '
        Me.TblSetup1TableAdapter1.ClearBeforeFill = True
        '
        'XtraTimeOfficePolicyNew
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "XtraTimeOfficePolicyNew"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.ToggleSwitchCanteen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchIsNepali.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchVisitor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.ToggleOnlineEvents.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleDownloadAtStartUp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditTimerDur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.ToggleCompl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControlIOCL, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControlIOCL.ResumeLayout(False)
        Me.PanelControlIOCL.PerformLayout()
        CType(Me.TextLink.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextIoclInterval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleIsIOCL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBio1EcoPort.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextInActiveDays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditAutoDwnDur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleRealTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextTWIR102Port.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditZKPort.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleLeaveAsPerFinancialYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditBioPort.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblSetupBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TblSetupBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As ULtra.SSSDBDataSet
    Friend WithEvents TblSetupTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblSetupTableAdapter
    Friend WithEvents TblSetup1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblSetup1TableAdapter
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents TextTWIR102Port As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl59 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditTimerDur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl58 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchIsNepali As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl56 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchVisitor As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleSwitchCanteen As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl54 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl55 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditZKPort As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl53 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditBioPort As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleLeaveAsPerFinancialYear As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleDownloadAtStartUp As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleOnlineEvents As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents TextEditAutoDwnDur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl60 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleRealTime As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl57 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextInActiveDays As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextBio1EcoPort As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleIsIOCL As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControlIOCL As DevExpress.XtraEditors.PanelControl
    Friend WithEvents TextIoclInterval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextLink As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleCompl As DevExpress.XtraEditors.ToggleSwitch
End Class
