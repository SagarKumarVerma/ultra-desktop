﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraEditors.Repository
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports DevExpress.XtraGrid

Public Class XtraTimeOfficePolicyNew
    Dim ulf As UserLookAndFeel
    Public Sub New()
        InitializeComponent()      
    End Sub
    Private Sub XtraTimeOfficePolicy_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Me.Width = My.Computer.Screen.WorkingArea.Width
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.Width = SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = (SplitContainerControl1.Parent.Width) * 85 / 100

        'not to do in all pages
        Common.splitforMasterMenuWidth = SplitContainerControl1.Width
        Common.SplitterPosition = SplitContainerControl1.SplitterPosition

        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        setDefault()
    End Sub
    Private Sub setDefault()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        'Dim sSql As String = "select * from tblSetup where SETUPID= (select MAX(SETUPID) from tblSetup)"
        Dim sSql As String = "Select * from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblSetup )"
        If Common.servername = "Access" Then
            sSql = "Select * from tblSetUp where setupid =(Select MAX(val(Setupid)) from tblSetup )"
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If

        If ds.Tables(0).Rows(0).Item("LeaveFinancialYear").ToString.Trim = "Y" Then
            ToggleLeaveAsPerFinancialYear.IsOn = True
        Else
            ToggleLeaveAsPerFinancialYear.IsOn = False
        End If

        If ds.Tables(0).Rows(0).Item("Online").ToString.Trim = "Y" Then
            ToggleOnlineEvents.IsOn = True
        Else
            ToggleOnlineEvents.IsOn = False
        End If

        If ds.Tables(0).Rows(0).Item("AutoDownload").ToString.Trim = "Y" Then
            ToggleDownloadAtStartUp.IsOn = True
        Else
            ToggleDownloadAtStartUp.IsOn = False
        End If

        'CreateEmpPunchDwnld
        If ds.Tables(0).Rows(0).Item("StartRealTime").ToString.Trim = "Y" Then
            ToggleRealTime.IsOn = True
        Else
            ToggleRealTime.IsOn = False
        End If

        TextEditBioPort.Text = ds.Tables(0).Rows(0).Item("BioPort").ToString.Trim
        TextEditZKPort.Text = ds.Tables(0).Rows(0).Item("ZKPort").ToString.Trim
        TextTWIR102Port.Text = ds.Tables(0).Rows(0).Item("TWIR102Port").ToString.Trim
        TextEditTimerDur.Text = ds.Tables(0).Rows(0).Item("TimerDur").ToString.Trim
        If ds.Tables(0).Rows(0).Item("Canteen").ToString.Trim = "Y" Then
            ToggleSwitchCanteen.IsOn = True
        ElseIf ds.Tables(0).Rows(0).Item("Canteen").ToString.Trim = "N" Then
            ToggleSwitchCanteen.IsOn = False
        End If
        If ds.Tables(0).Rows(0).Item("Visitor").ToString.Trim = "Y" Then
            ToggleSwitchVisitor.IsOn = True
        ElseIf ds.Tables(0).Rows(0).Item("Visitor").ToString.Trim = "N" Then
            ToggleSwitchVisitor.IsOn = False
        End If

        If ds.Tables(0).Rows(0).Item("IsNepali").ToString.Trim = "Y" Then
            ToggleSwitchIsNepali.IsOn = True
        ElseIf ds.Tables(0).Rows(0).Item("IsNepali").ToString.Trim = "N" Then
            ToggleSwitchIsNepali.IsOn = False
        End If
        TextEditAutoDwnDur.Text = ds.Tables(0).Rows(0).Item("AutoDwnDur").ToString.Trim
        TextInActiveDays.Text = ds.Tables(0).Rows(0).Item("MarkInactivDurDay").ToString.Trim
        TextBio1EcoPort.Text = ds.Tables(0).Rows(0).Item("Bio1EcoPort").ToString.Trim
        
        If ds.Tables(0).Rows(0).Item("IsIOCL").ToString.Trim = "Y" Then
            PanelControlIOCL.Visible = True
            ToggleIsIOCL.IsOn = True
            TextIoclInterval.Text = ds.Tables(0).Rows(0).Item("IOCLInterval").ToString.Trim ' (ds.Tables(0).Rows(0).Item("IOCLInterval").ToString.Trim \ 60) & ":" & ds.Tables(0).Rows(0).Item("IOCLInterval").ToString.Trim Mod 60
            TextLink.Text = ds.Tables(0).Rows(0).Item("IOCLLink").ToString.Trim
        Else 'If ds.Tables(0).Rows(0).Item("IsIOCL").ToString.Trim = "N" Then
            ToggleIsIOCL.IsOn = False
            PanelControlIOCL.Visible = False

        End If
        ToggleCompl.EditValue = ds.Tables(0).Rows(0).Item("IsCompliance").ToString.Trim
        If Common.IsComplianceFromLic = True Then
            LabelControl10.Visible = True
            ToggleCompl.Visible = True
        Else
            LabelControl10.Visible = False
            ToggleCompl.Visible = False
        End If
    End Sub
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        Me.Cursor = Cursors.WaitCursor
        Dim cmd As New SqlCommand
        Dim cmd1 As New OleDbCommand

        Dim StartRealTime As String
        If ToggleRealTime.IsOn = True Then
            StartRealTime = "Y"
        Else
            StartRealTime = "N"
        End If

        Dim LeaveFinancialYear As String
        If ToggleLeaveAsPerFinancialYear.IsOn = True Then
            LeaveFinancialYear = "Y"
        Else
            LeaveFinancialYear = "N"
        End If
        Dim Online As String
        If ToggleOnlineEvents.IsOn = True Then
            Online = "Y"
        Else
            Online = "N"
        End If
        Dim AutoDownload As String
        If ToggleDownloadAtStartUp.IsOn = True Then
            AutoDownload = "Y"
        Else
            AutoDownload = "N"
        End If

        Dim BioPort As String = TextEditBioPort.Text.Trim
        Dim ZKPort As String = TextEditZKPort.Text.Trim
        Dim TWIR102Port As String = TextTWIR102Port.Text.Trim
        Dim Canteen As String
        Dim Visitor As String
        If ToggleSwitchCanteen.IsOn = True Then
            Canteen = "Y"
        Else
            Canteen = "N"
        End If
        If ToggleSwitchVisitor.IsOn = True Then
            Visitor = "Y"
        Else
            Visitor = "N"
        End If
        Dim IsNepali As String = "N"
        If ToggleSwitchIsNepali.IsOn = True Then
            IsNepali = "Y"
        Else
            IsNepali = "N"
        End If
        Dim TimerDur As String = TextEditTimerDur.Text.Trim
        If TextEditTimerDur.Text.Trim = "" Then
            TimerDur = 10
        End If
        Dim AutoDwnDur As String = TextEditAutoDwnDur.Text.Trim
        If AutoDwnDur = "" Then
            AutoDwnDur = "0"
        End If
        Dim MarkInactivDurDay As String = TextInActiveDays.Text.Trim
        Dim Bio1EcoPort As String = TextBio1EcoPort.Text.Trim

        Dim IsIOCL As String = "N"
        Dim IOCLInterval As Integer = 0
        Dim IOCLLink As String = ""
        If ToggleIsIOCL.IsOn = True Then
            Dim preMarkDur() As String = TextIoclInterval.Text.Trim.Split(":")
            IOCLInterval = preMarkDur(0) * 60 + preMarkDur(1)
            IsIOCL = "Y"
            IOCLLink = TextLink.Text.Trim
        End If
        Dim IsCompliance As String
        If ToggleCompl.IsOn = True Then
            IsCompliance = "Y"
        Else
            IsCompliance = "N"
        End If


        Dim sSql As String = "Update tblSetup set StartRealTime='" & StartRealTime & "', LeaveFinancialYear='" & LeaveFinancialYear & "', Online='" & Online & "', " &
            "AutoDownload ='" & AutoDownload & "', BioPort='" & BioPort & "', ZKPort='" & ZKPort & "', TWIR102Port='" & TWIR102Port & "', Canteen='" & Canteen & "', " &
            "Visitor='" & Visitor & "', IsNepali='" & IsNepali & "', TimerDur='" & TimerDur & "', AutoDwnDur='" & AutoDwnDur & "' , MarkInactivDurDay='" & MarkInactivDurDay & "', " &
            "Bio1EcoPort='" & Bio1EcoPort & "', IsIOCL ='" & IsIOCL & "', IOCLInterval ='" & IOCLInterval & "', IOCLLink ='" & IOCLLink & "', IsCompliance='" & IsCompliance & "'"
        If Common.servername = "Access" Then

            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()

            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()

            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        Common.IsNepali = IsNepali
        Common.TimerDur = TimerDur
        Common.online = Online
        Common.Load_Corporate_PolicySql()
        Me.Cursor = Cursors.Default
        XtraMessageBox.Show(ulf, "<size=10>Saved Successfully</size>", "<size=9>ULtra</size>")
    End Sub
    Private Sub ToggleIsIOCL_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleIsIOCL.Toggled
        If ToggleIsIOCL.IsOn = True Then
            PanelControlIOCL.Visible = True
        ElseIf ToggleIsIOCL.IsOn = False Then
            PanelControlIOCL.Visible = False
        End If
    End Sub
End Class
