﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Base
Imports iAS.AscDemo
Imports System.Runtime.InteropServices
Imports System.Threading


Public Class XtraFringerDataMgmt
    Public Delegate Sub invokeDelegate()
    Dim lUserID As Integer = -1
    Dim serialNo As String = ""
    Dim m_lGetCardCfgHandle As Int32 = -1 'Hikvision
    Dim g_fGetGatewayCardCallback As CHCNetSDK.RemoteConfigCallback = Nothing 'Hikvision
    Dim m_lGetFingerPrintCfgHandle As Integer = -1
    Dim HCardNos As List(Of String) = Nothing
    Dim iMaxCardNum As Integer = 1000
    Dim m_struFingerPrintOne As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50
    Dim m_struDelFingerPrint As CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_CTRL_BYCARD_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_CTRL_BYCARD_V50
    Dim m_struRecordCardCfg() As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50((iMaxCardNum) - 1) {}
    Dim m_struCardInfo() As CHCNetSDK.NET_DVR_CARD_CFG_V50 = New CHCNetSDK.NET_DVR_CARD_CFG_V50((iMaxCardNum) - 1) {}
    Dim m_lUserID As Integer = -1
    Dim g_fSetFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fGetFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fDelFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fSetGatewayCardCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim m_lSetCardCfgHandle As Int32 = -1
    Dim m_bSendOne As Boolean = False
    Dim m_struSelSendCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50
    Dim m_struNowSendCard As CHCNetSDK.NET_DVR_CARD_CFG_V50
    Dim m_BSendSel As Boolean = False
    Dim m_dwCardNum As UInteger = 0
    Dim m_dwSendIndex As UInteger = 0
    Dim m_lSetFingerPrintCfgHandle As Integer = -1
    Dim m_iSendIndex As Integer = -1

    Dim Downloading As Boolean = False

    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand

    Const PWD_DATA_SIZE = 40
    Const FP_DATA_SIZE = 1680
    Const FACE_DATA_SIZE = 20080
    Const VEIN_DATA_SIZE = 3080
    Private mbytEnrollData(FACE_DATA_SIZE - 1) As Byte
    Private mbytPhotoImage() As Byte

    Private mnCommHandleIndex As Long
    Private mbGetState As Boolean
    Private mlngPasswordData As Long

    Dim vpszIPAddress As String
    Dim vpszNetPort As Long
    Dim vpszNetPassword As Long
    Dim vnTimeOut As Long
    Dim vnProtocolType As Long
    Dim strDateTime As String
    Dim vnResult As Long
    Dim vnLicense As Long


    '//=============== Backup Number Constant ===============//
    'Public Const BACKUP_FP_0 = 0                  ' Finger 0
    'Public Const BACKUP_FP_1 = 1                  ' Finger 1
    'Public Const BACKUP_FP_2 = 2                  ' Finger 2
    'Public Const BACKUP_FP_3 = 3                  ' Finger 3
    'Public Const BACKUP_FP_4 = 4                  ' Finger 4
    'Public Const BACKUP_FP_5 = 5                  ' Finger 5
    'Public Const BACKUP_FP_6 = 6                  ' Finger 6
    'Public Const BACKUP_FP_7 = 7                  ' Finger 7
    'Public Const BACKUP_FP_8 = 8                  ' Finger 8
    'Public Const BACKUP_FP_9 = 9                  ' Finger 9
    'Public Const BACKUP_PSW = 10                  ' Password
    Public Const BACKUP_CARD = 11                 ' Card
    Public Const BACKUP_FACE = 12                 ' Face
    Public Const BACKUP_VEIN_0 = 20               ' Vein 0
    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
        Common.SetGridFont(GridView2, New Font("Tahoma", 10))
        m_struFingerPrintOne.Init()
        m_struDelFingerPrint.struByCard.byFingerPrintID = New Byte((CHCNetSDK.MAX_FINGER_PRINT_NUM) - 1) {}
    End Sub
    Private Sub setDefault()
        ToggleDownloadAll.IsOn = True
        If ToggleDownloadAll.IsOn = True Then
            LabelControl1.Visible = False
            TextSelectUser.Visible = False
        Else
            LabelControl1.Visible = True
            TextSelectUser.Visible = True
        End If
        TextSelectUser.Text = ""
        ToggleCreateEmp.IsOn = False
        GridView1.ClearSelection()
        GridView2.ClearSelection()
        TextPassword.Text = ""
        For Each c As Control In Me.Controls
            c.Enabled = True
        Next
    End Sub
    Private Sub XtraFringerDataMgmt_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        If Common.servername = "Access" Then
            Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine1
            Me.Fptable1TableAdapter1.Fill(Me.SSSDBDataSet.fptable1)
            GridControl2.DataSource = SSSDBDataSet.fptable1
        Else
            TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
            FptableTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine
            Me.FptableTableAdapter.Fill(Me.SSSDBDataSet.fptable)
            GridControl2.DataSource = SSSDBDataSet.fptable
        End If
        GridControl1.DataSource = Common.MachineNonAdmin
        setDefault()
    End Sub
    Private Sub GridView2_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView2.CustomColumnDisplayText
        Try
            Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "FingerNumber" Then
                If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim >= 0 And _
                    view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim < 10 Then
                    e.DisplayText = "FP-" & view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 12 Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 50 Then
                    e.DisplayText = "FACE"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 11 Then
                    e.DisplayText = "CARD"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 10 Then
                    e.DisplayText = "PASSWORD"
                End If
                e.DisplayText = (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim \ 60).ToString("00") & ":" & (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim Mod 60).ToString("00")
            End If
        Catch
        End Try
        Try
            Dim adapA As OleDbDataAdapter
            Dim adap As SqlDataAdapter
            Dim ds As DataSet
            Dim sSql As String
            Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "UserName" Then
                sSql = "SELECT EMPNAME from TblEmployee where PRESENTCARDNO = '" & view.GetListSourceRowCellValue(e.ListSourceRowIndex, "EnrollNumber").ToString().Trim & "'"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    e.DisplayText = ds.Tables(0).Rows(0).Item(0).ToString
                Else
                    e.DisplayText = ""
                End If
            End If

            If e.Column.Caption = "Device Type" Then
                'If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Template").ToString().Trim = "" Then
                '    If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Cardnumber").ToString().Trim <> "" Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Password").ToString().Trim <> "" Then
                '        e.DisplayText = "ZK/Bio"
                '    Else
                '        e.DisplayText = "Bio"
                '    End If
                'ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Template_Tw").ToString().Trim = "" Then
                '    e.DisplayText = "ZK"
                'End If
                e.DisplayText = "HIKVISION"
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GridView1_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        Try
            Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "DeviceType" Then
                If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DeviceType").ToString().Trim = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                    e.DisplayText = "Bio"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DeviceType").ToString().Trim = "ZK(TFT)" Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DeviceType").ToString().Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                    e.DisplayText = "ZK"
                End If
                e.DisplayText = (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim \ 60).ToString("00") & ":" & (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim Mod 60).ToString("00")
            End If
        Catch
        End Try
    End Sub
    Private Sub btnDownloadFinger_Click(sender As System.Object, e As System.EventArgs) Handles btnDownloadFinger.Click
        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Device</size>", "<size=9>Error</size>")
            Exit Sub
        Else
            If ToggleDownloadAll.IsOn = False Then
                If TextSelectUser.Text = "" Then
                    XtraMessageBox.Show(ulf, "<size=10>Please Enter User</size>", "<size=9>Error</size>")
                    Exit Sub
                End If
            End If
            Me.Cursor = Cursors.WaitCursor
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next
            Dim cn As Common = New Common
            Dim paycodelist As New List(Of String)()
            Dim paycodeArray '= paycodelist.Distinct.ToArray ' paycodelist.ToArray

            Dim commkey As Integer
            Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            For i As Integer = 0 To selectedRows.Length - 1
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridView1.IsGroupRow(rowHandle) Then
                    result(i) = GridView1.GetRowCellValue(rowHandle, "ID_NO")
                    vpszIPAddress = Trim(GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim)

                    Dim Emachinenumber As Integer = result(i)
                    Dim A_R As String = GridView1.GetRowCellValue(rowHandle, "A_R") 'GridView1.GetRowCellValue(rowHandle, GridView1.Columns("A_R"))
                    If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                        
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                        
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "HIKVISION" Then
                        Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                        Dim DeviceAdd As String = vpszIPAddress, userName As String = Trim(GridView1.GetRowCellValue(rowHandle, "HLogin").ToString.Trim), pwd As String = Trim(GridView1.GetRowCellValue(rowHandle, "HPassword").ToString.Trim)                        
                        Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID)
                        If logistatus = False Then
                            XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                            Me.Cursor = Cursors.Default

                            For Each c As Control In Me.Controls
                                c.Enabled = True
                            Next
                            Exit Sub
                        Else
                            Downloading = True
                            m_lUserID = lUserID
                            If (-1 <> m_lGetCardCfgHandle) Then
                                If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetCardCfgHandle) Then
                                    m_lGetCardCfgHandle = -1
                                End If
                            End If
                            Dim struCond As CHCNetSDK.NET_DVR_CARD_CFG_COND = New CHCNetSDK.NET_DVR_CARD_CFG_COND
                            struCond.dwSize = CType(Marshal.SizeOf(struCond), UInteger)
                            struCond.wLocalControllerID = 0
                            struCond.dwCardNum = 4294967295
                            struCond.byCheckCardNo = 1
                            Dim dwSize As Integer = Marshal.SizeOf(struCond)
                            Dim ptrStruCond As IntPtr = Marshal.AllocHGlobal(dwSize)
                            Marshal.StructureToPtr(struCond, ptrStruCond, False)
                            g_fGetGatewayCardCallback = New CHCNetSDK.RemoteConfigCallback(AddressOf ProcessGetGatewayCardCallback)
                            Thread.Sleep(200)
                            m_lGetCardCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_GET_CARD_CFG_V50, ptrStruCond, dwSize, g_fGetGatewayCardCallback, Me.Handle)
                            Thread.Sleep(200)
                            If (m_lGetCardCfgHandle = -1) Then
                                Dim listItem As ListViewItem = New ListViewItem
                                listItem.Text = "FAIL"
                                Dim strTemp As String = String.Format("NET_DVR_GET_CARD_CFG_V50 FAIL, ERROR CODE {0}", CHCNetSDK.NET_DVR_GetLastError)
                                listItem.SubItems.Add(strTemp)
                                'Me.AddList(listViewMessage, listItem, True)
                                Marshal.FreeHGlobal(ptrStruCond)
                                Return
                            Else
                                serialNo = System.Text.Encoding.UTF8.GetString(struDeviceInfoV40.struDeviceV30.sSerialNumber).TrimEnd(Microsoft.VisualBasic.ChrW(92))
                                Dim listItem As ListViewItem = New ListViewItem
                                listItem.Text = "SUCC"
                                listItem.SubItems.Add("NET_DVR_GET_CARD_CFG_V50")
                                'Me.AddList(listViewMessage, listItem, True)
                            End If

                            Marshal.FreeHGlobal(ptrStruCond)

                            'logistatus = cn.HikvisionLogOut(lUserID)
                        End If
                    End If
                Else
                    result(0) = -1
                End If
            Next

            If ToggleCreateEmp.IsOn = True Then
                paycodeArray = paycodelist.Distinct.ToArray ' paycodelist.ToArray
                Common.CreateEmployee(paycodeArray)
                Common.loadEmp()
            End If

            For Each c As Control In Me.Controls
                c.Enabled = True
            Next

            'Dim msg As New Message("Downloading", "Download Started...")
            'XtraMaster.AlertControl1.Show(XtraMaster, msg.Caption, msg.Text, "", msg.Image, msg)
            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()

            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()

            Me.Cursor = Cursors.Default
            'If Common.servername = "Access" Then
            '    Me.Fptable1TableAdapter1.Fill(Me.SSSDBDataSet.fptable1)
            '    GridControl2.DataSource = SSSDBDataSet.fptable1
            'Else
            '    FptableTableAdapter.Connection.ConnectionString = Common.ConnectionString
            '    Me.FptableTableAdapter.Fill(Me.SSSDBDataSet.fptable)
            '    GridControl2.DataSource = SSSDBDataSet.fptable
            'End If
            'XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
        End If
    End Sub

    Private Sub ProcessGetGatewayCardCallback(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
        If (pUserData = Nothing) Then
            Return
        End If
        If (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_DATA, UInteger)) Then
            Dim struCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50 = New CHCNetSDK.NET_DVR_CARD_CFG_V50
            struCardCfg = CType(Marshal.PtrToStructure(lpBuffer, GetType(CHCNetSDK.NET_DVR_CARD_CFG_V50)), CHCNetSDK.NET_DVR_CARD_CFG_V50)
            Dim strCardNo As String = struCardCfg.dwEmployeeNo ' System.Text.Encoding.UTF8.GetString(struCardCfg.byCardNo)
            Dim pCardInfo As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(struCardCfg))
            Marshal.StructureToPtr(struCardCfg, pCardInfo, True)
            CHCNetSDK.PostMessage(pUserData, 1003, CType(pCardInfo, Integer), 0)
            'AddToCardList(struCardCfg, strCardNo);
        ElseIf (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_STATUS, UInteger)) Then
            Dim dwStatus As UInteger = CType(Marshal.ReadInt32(lpBuffer), UInteger)
            If (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_SUCCESS, UInteger)) Then
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "SUCC"
                listItem.SubItems.Add("NET_DVR_GET_CARD_CFG_V50 Get finish")
                'Me.AddList(listViewMessage, listItem, True)
                CHCNetSDK.PostMessage(pUserData, 1002, 0, 0)
            ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_FAILED, UInteger)) Then
                Dim dwErrorCode As UInteger = CType(Marshal.ReadInt32((lpBuffer + 1)), UInteger)
                Dim cardNumber As String = Marshal.PtrToStringAnsi((lpBuffer + 2))
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "FAIL"
                listItem.SubItems.Add(String.Format("NET_DVR_GET_CARD_CFG_V50 Get Failed,ErrorCode:{0},CardNo:{1}", dwErrorCode, cardNumber))
                'Me.AddList(listViewMessage, listItem, True)
                CHCNetSDK.PostMessage(pUserData, 1002, 0, 0)
            End If
        End If
        Return
    End Sub
    Public Delegate Sub DefWndProcCallback(ByRef m As System.Windows.Forms.Message)
    Protected Overrides Sub DefWndProc(ByRef m As System.Windows.Forms.Message)
        If InvokeRequired = True Then
            Dim d As DefWndProcCallback = New DefWndProcCallback(AddressOf Me.DefWndProc)
            Me.Invoke(d, New Object() {m})
            'Dim tmp = IN_OUT
        Else
            Select Case (m.Msg)
                Case 1001
                    Dim iErrorMsg As Integer = m.WParam.ToInt32
                    If (-1 <> m_lSetCardCfgHandle) Then
                        If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetCardCfgHandle) Then
                            m_lSetCardCfgHandle = -1
                        End If
                    End If
                    If (-1 <> m_lGetCardCfgHandle) Then
                        If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetCardCfgHandle) Then
                            m_lGetCardCfgHandle = -1

                        End If
                    End If
                Case 1002
                    If Downloading = True Then
                        Downloading = False
                        'Dim msg As New Message("Success", "Download Finished")
                        'XtraMaster.AlertControl1.Show(XtraMaster, msg.Caption, msg.Text, "", msg.Image, msg)
                        'XtraMasterTest.LabelControlStatus.Text = ""
                        'Application.DoEvents()
                        If Common.servername = "Access" Then
                            Me.Fptable1TableAdapter1.Fill(Me.SSSDBDataSet.fptable1)
                            GridControl2.DataSource = SSSDBDataSet.fptable1
                        Else
                            FptableTableAdapter.Connection.ConnectionString = Common.ConnectionString
                            Me.FptableTableAdapter.Fill(Me.SSSDBDataSet.fptable)
                            GridControl2.DataSource = SSSDBDataSet.fptable
                        End If
                        GridView2.RefreshData()
                        Application.DoEvents()
                        XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
                    End If

                    'SendNextCard()
                Case 1003
                    Dim pCardInfo As IntPtr = CType(m.WParam.ToInt32, IntPtr)
                    Dim struCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50 = New CHCNetSDK.NET_DVR_CARD_CFG_V50
                    struCardCfg = CType(Marshal.PtrToStructure(pCardInfo, GetType(CHCNetSDK.NET_DVR_CARD_CFG_V50)), CHCNetSDK.NET_DVR_CARD_CFG_V50)
                    Dim strCardNo As String = System.Text.Encoding.UTF8.GetString(struCardCfg.byCardNo)
                    'AddToCardList(struCardCfg, strCardNo)
                    'Thread.Sleep(200)
                    saveHikvision(struCardCfg)
                Case Else
                    MyBase.DefWndProc(m)
            End Select
        End If
    End Sub
    Private Sub AddToCardList(ByVal struCardInfo As CHCNetSDK.NET_DVR_CARD_CFG_V50, ByVal strCardNo As String)
        Dim iItemIndex As Integer = 0 'GetExistItem(struCardInfo)
        If (-1 = iItemIndex) Then
            iItemIndex = 0 ' listViewGataManage.Items.Count
        End If
        'UpdateList(iItemIndex, strCardNo, struCardInfo)
        m_struCardInfo(iItemIndex) = struCardInfo
    End Sub
    Public Delegate Sub SetTextCallbacksaveHikvision(ByVal struCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50)
    Private Sub saveHikvision(ByVal struCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50)
        If InvokeRequired = True Then
            Dim d As SetTextCallbacksaveHikvision = New SetTextCallbacksaveHikvision(AddressOf Me.saveHikvision)
            Me.Invoke(d, New Object() {struCardCfg})
            'Dim tmp = IN_OUT
        Else
            Dim cn As Common = New Common
            Dim con1 As OleDbConnection
            Dim con As SqlConnection
            If Common.servername = "Access" Then
                con1 = New OleDbConnection(Common.ConnectionString)
            Else
                con = New SqlConnection(Common.ConnectionString)
            End If

            Dim CardNumber As String = System.Text.Encoding.UTF8.GetString(struCardCfg.byCardNo)
            ''HCardNos.Add(EnrollNumber)
            'GetHFp(CardNumber)
            Dim EnrollNumber = struCardCfg.dwEmployeeNo
            GetHFp(EnrollNumber)
            If IsNumeric(EnrollNumber) Then
                EnrollNumber = Convert.ToDouble(EnrollNumber).ToString("000000000000")
            End If
            Dim DeviceType As String = "HIKVISION"
            Dim Password As String = System.Text.Encoding.UTF8.GetString(struCardCfg.byCardPassword)
            Dim UserName As String = System.Text.Encoding.UTF8.GetString(struCardCfg.byName)

            Dim del As invokeDelegate = Function()
                                            XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & EnrollNumber
                                        End Function
            Invoke(del)
            'XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & EnrollNumber
            'Application.DoEvents()
            Dim x As String = serialNo.ToString.Trim 'Replace("-", "")
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim sSql As String = "select * from fptable where [EMachineNumber]='" & x & "' and EnrollNumber ='" & EnrollNumber & "' and FingerNumber=11 "
            Dim dsRecord As DataSet = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1)
                adapA.Fill(dsRecord)
            Else
                adap = New SqlDataAdapter(sSql, con)
                adap.Fill(dsRecord)
            End If
            If dsRecord.Tables(0).Rows.Count = 0 Then
                sSql = "insert into fptable (Emachinenumber, EnrollNumber, UserName, CardNumber, FingerNumber, Privilege) values ('" & serialNo.Trim(vbNullChar) & "','" & EnrollNumber & "', '" & UserName.Trim(vbNullChar) & "','" & CardNumber.Trim(vbNullChar) & "', '11', '0')"
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    Dim cmd As OleDbCommand
                    cmd = New OleDbCommand(sSql, con1)
                    cmd.ExecuteNonQuery()
                    If con1.State <> ConnectionState.Closed Then
                        con1.Close()
                    End If
                Else
                    If con.State <> ConnectionState.Open Then
                        con.Open()
                    End If
                    Dim cmd As SqlCommand
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                End If

                ''GridView2.RefreshData()
                'If Common.servername = "Access" Then
                '    Me.Fptable1TableAdapter1.Fill(Me.SSSDBDataSet.fptable1)
                '    GridControl2.DataSource = SSSDBDataSet.fptable1
                'Else
                '    FptableTableAdapter.Connection.ConnectionString = Common.ConnectionString
                '    Me.FptableTableAdapter.Fill(Me.SSSDBDataSet.fptable)
                '    GridControl2.DataSource = SSSDBDataSet.fptable
                'End If
                'Application.DoEvents()
            End If

            'If IsNumeric(EnrollNumber) Then
            '    EnrollNumber = Convert.ToDouble(EnrollNumber)
            'End If
            'GetHFp(EnrollNumber)
        End If
    End Sub
    Private Function SendNextCard() As Boolean
        If (-1 = m_lSetCardCfgHandle) Then
            Return False
        End If
        m_dwSendIndex = (m_dwSendIndex + 1)
        If (m_dwSendIndex >= m_dwCardNum) Then
            'CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetCardCfgHandle);
            'this.StopRemoteCfg(ref m_lSetCardCfgHandle);
            'm_lSetCardCfgHandle = -1;
            Dim listItem2 As ListViewItem = New ListViewItem
            listItem2.Text = "SUCC"
            Dim strTemp2 As String = Nothing
            strTemp2 = String.Format("Send {0} card(s) over", m_dwCardNum)
            listItem2.SubItems.Add(strTemp2)
            'Me.AddList(listViewMessage, listItem2, True)
            Return True
        End If

        m_struNowSendCard = m_struCardInfo(m_dwSendIndex)
        Dim dwSize As UInteger = CType(Marshal.SizeOf(m_struNowSendCard), UInteger)
        Dim ptrSendCard As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Integer))
        Marshal.StructureToPtr(m_struNowSendCard, ptrSendCard, False)
        If Not CHCNetSDK.NET_DVR_SendRemoteConfig(m_lSetCardCfgHandle, 3, ptrSendCard, dwSize) Then
            Marshal.FreeHGlobal(ptrSendCard)
            Dim listItem3 As ListViewItem = New ListViewItem
            listItem3.Text = "FAIL"
            Dim strTemp3 As String = Nothing
            strTemp3 = String.Format("Send Fail,CardNO:{0}", System.Text.Encoding.UTF8.GetString(m_struNowSendCard.byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
            listItem3.SubItems.Add(strTemp3)
            'Me.AddList(listViewMessage, listItem3, True)           
            Return False
        End If

        Marshal.FreeHGlobal(ptrSendCard)
        Return True
    End Function
    Private Sub GetHFp(ByVal CardNo As String)
        Thread.Sleep(500)
        If (m_lGetFingerPrintCfgHandle <> -1) Then
            CHCNetSDK.NET_DVR_StopRemoteConfig(CType(m_lGetFingerPrintCfgHandle, Integer))
        End If
        Dim struCond As CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_COND_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_COND_V50
        struCond.byCardNo = New Byte((32) - 1) {}
        struCond.byEmployeeNo = New Byte((32) - 1) {}
        struCond.dwSize = CType(Marshal.SizeOf(struCond), UInteger)
        'UInteger.TryParse(textBoxNumber.Text, struCond.dwFingerPrintNum)
        struCond.dwFingerPrintNum = 0
        Dim byTempEmployeeNo() As Byte = System.Text.Encoding.UTF8.GetBytes("")
        Dim i As Integer = 0
        Do While (i < byTempEmployeeNo.Length)
            struCond.byEmployeeNo(i) = byTempEmployeeNo(i)
            i = (i + 1)
        Loop
        Byte.TryParse("1", struCond.byFingerPrintID)
        Dim byTemp() As Byte = System.Text.Encoding.UTF8.GetBytes(CardNo)
        i = 0
        'Do While (i < byTemp.Length)
        '    struCond.byCardNo(i) = byTemp(i)
        '    i = (i + 1)
        'Loop
        Do While (i < byTemp.Length)
            struCond.byEmployeeNo(i) = byTemp(i)
            i = (i + 1)
        Loop
        GetTreeSel()
        struCond.byEnableCardReader = m_struFingerPrintOne.byEnableCardReader
        Dim dwSize As Integer = Marshal.SizeOf(struCond)
        Dim ptrStruCond As IntPtr = Marshal.AllocHGlobal(dwSize)
        Marshal.StructureToPtr(struCond, ptrStruCond, False)
        'Thread.Sleep(200)  'test
        g_fGetFingerPrintCallback = New CHCNetSDK.RemoteConfigCallback(AddressOf ProcessGetFingerPrintCfgCallbackData)
        m_lGetFingerPrintCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(m_lUserID, CHCNetSDK.NET_DVR_GET_FINGERPRINT_CFG_V50, ptrStruCond, dwSize, g_fGetFingerPrintCallback, Me.Handle)
        If (-1 = m_lGetFingerPrintCfgHandle) Then
            Dim listItem As ListViewItem = New ListViewItem
            listItem.Text = "FAIL"
            Dim strTemp As String = String.Format("NET_DVR_GET_FINGERPRINT_CFG_V50 FAIL, ERROR CODE {0}", CHCNetSDK.NET_DVR_GetLastError)
            listItem.SubItems.Add(strTemp)
            'Me.AddList(listViewMessage, listItem)
            Marshal.FreeHGlobal(ptrStruCond)
            Return
        Else
            Dim listItem As ListViewItem = New ListViewItem
            listItem.Text = "SUCC"
            listItem.SubItems.Add("NET_DVR_GET_FINGERPRINT_CFG_V50")
            'Me.AddList(listViewMessage, listItem)
            Marshal.FreeHGlobal(ptrStruCond)
        End If

    End Sub
    'Public Delegate Sub ProcessGetFingerPrintCfgCallbackDataCallback(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
    Private Sub ProcessGetFingerPrintCfgCallbackData(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
        'If InvokeRequired = True Then
        '    Dim d As ProcessGetFingerPrintCfgCallbackDataCallback = New ProcessGetFingerPrintCfgCallbackDataCallback(AddressOf Me.ProcessGetFingerPrintCfgCallbackData)
        '    Me.Invoke(d, New Object() {dwType, lpBuffer, dwBufLen, pUserData})
        '    'Dim tmp = IN_OUT
        'Else

        Thread.Sleep(50)
        If (pUserData = Nothing) Then
            Return
        End If
        If (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_DATA, UInteger)) Then
            Dim strFingerPrintCfg As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50
            strFingerPrintCfg = CType(Marshal.PtrToStructure(lpBuffer, GetType(CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50)), CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50)
            'Marshal.PtrToStructure(lpBuffer, strCardCfg);
            Dim bSendOK As Boolean = False
            Dim i As Integer = 0
            Do While (i < strFingerPrintCfg.byEnableCardReader.Length)
                If (1 = strFingerPrintCfg.byEnableCardReader(i)) Then
                    bSendOK = True
                End If
                i = (i + 1)
            Loop

            If Not bSendOK Then
            End If
            bSendOK = False
            i = 0
            Do While (i < strFingerPrintCfg.byLeaderFP.Length)
                If (1 = strFingerPrintCfg.byLeaderFP(i)) Then
                    bSendOK = True
                End If
                i = (i + 1)
            Loop
            If Not bSendOK Then
            End If
            If (0 = strFingerPrintCfg.dwSize) Then
                Return
            End If
            'AddToFingerPrintList(strFingerPrintCfg, False)
            Dim con1 As OleDbConnection
            Dim con As SqlConnection
            If Common.servername = "Access" Then
                con1 = New OleDbConnection(Common.ConnectionString)
            Else
                con = New SqlConnection(Common.ConnectionString)
            End If
            Dim ds As DataSet = New DataSet
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim Template As String = Encoding.ASCII.GetString(strFingerPrintCfg.byFingerData)
            Dim EnrollNumber As String = System.Text.Encoding.UTF8.GetString(strFingerPrintCfg.byEmployeeNo) 'Convert.ToDouble(System.Text.Encoding.UTF8.GetString(strFingerPrintCfg.byCardNo))

            XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & EnrollNumber
            Application.DoEvents()

            Dim FingerNumber As String = strFingerPrintCfg.byFingerPrintID.ToString
            serialNo = serialNo.Trim
            Dim strPath As String = System.Environment.CurrentDirectory & "\FTemplate\" & EnrollNumber.ToString.Trim(vbNullChar) & "_" & serialNo.Trim.Substring(0, 5) & "fingerprint.dat"
            Dim fs As FileStream = New FileStream(strPath, FileMode.OpenOrCreate)
            If Not File.Exists(strPath) Then
                MessageBox.Show("Fingerprint storage file creat failed")
            End If
            Dim objBinaryWrite As BinaryWriter = New BinaryWriter(fs)
            fs.Write(strFingerPrintCfg.byFingerData, 0, CType(strFingerPrintCfg.dwFingerPrintLen, Integer))
            fs.Close()

            'Dim EMachineNumber As String = serialNo
            If IsNumeric(EnrollNumber) Then
                EnrollNumber = Convert.ToDouble(EnrollNumber).ToString("000000000000")
            End If
            'serialNo = "DS-K1T804MF-120170911V010100ENC13360348"
            Dim SrTmp As String = serialNo.Trim
            Dim Sql1 As String = "select * from fptable Where  [EMachineNumber] = '" & SrTmp.Trim & "' and EnrollNumber ='" & EnrollNumber & "' and FingerNumber=" & FingerNumber & ""
            Dim HTemplate As String
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(Sql1, con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(Sql1, con)
                adap.Fill(ds)
            End If


            If ds.Tables(0).Rows.Count > 0 Then
                Sql1 = "update fptable set HTemplatePath='" & strPath & "' where EMachineNumber = '" & serialNo.Trim & "' and EnrollNumber ='" & EnrollNumber & "' and FingerNumber='" & FingerNumber & "' "
            Else
                Sql1 = "insert into fptable (Emachinenumber, EnrollNumber, UserName, CardNumber, FingerNumber, HTemplatePath) values ('" & serialNo.Trim & "','" & EnrollNumber & "', '','', '" & FingerNumber & "', '" & strPath & "')"
            End If
            If Common.servername = "Access" Then
                If con1.State <> ConnectionState.Open Then
                    con1.Open()
                End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(Sql1, con1)
                cmd.ExecuteNonQuery()

                If con1.State <> ConnectionState.Closed Then
                    con1.Close()
                End If
            Else
                If con.State <> ConnectionState.Open Then
                    con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(Sql1, con)
                cmd.ExecuteNonQuery()
                If con.State <> ConnectionState.Closed Then
                    con.Close()
                End If
            End If
        ElseIf (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_STATUS, UInteger)) Then
            Dim dwStatus As UInteger = 0
            dwStatus = CType(Marshal.ReadInt32(lpBuffer), UInteger)
            If (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_SUCCESS, UInteger)) Then
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "SUCC"
                listItem.SubItems.Add("NET_DVR_GET_FINGERPRINT_CFG_V50 Get finish")
                'Dim msg As New Message("Success", "Download Finished")
                'XtraMaster.AlertControl1.Show(XtraMaster, msg.Caption, msg.Text, "", msg.Image, msg)
                'XtraMasterTest.LabelControlStatus.Text = ""
                'Application.DoEvents()
                'Me.AddList(listViewMessage, listItem)
            ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_FAILED, UInteger)) Then
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "FAIL"
                listItem.SubItems.Add("NET_DVR_GET_FINGERPRINT_CFG_V50 Get FAIL")
                'Me.AddList(listViewMessage, listItem)
            End If
        End If
        'End If
    End Sub
    Private Sub GetTreeSel()
        Dim i As Integer = 0
        Do While (i < 512)
            If i = 0 Then
                m_struFingerPrintOne.byEnableCardReader(i) = 1
            Else
                m_struFingerPrintOne.byEnableCardReader(i) = 0
            End If
            i = (i + 1)
        Loop
        i = 0
        Do While (i < 256)
            m_struFingerPrintOne.byLeaderFP(i) = 0
            i = (i + 1)
        Loop
        i = 0
        Do While (i < 10)
            m_struDelFingerPrint.struByCard.byFingerPrintID(i) = 0
            i = (i + 1)
        Loop
    End Sub
    Private Sub saveTemplate(iMachineNumber As Integer, sdwEnrollNumber As String, sName As String, sPassword As String, iPrivilege As Integer, bEnabled As Boolean, axCZKEM1 As zkemkeeper.CZKEM, lvItem As ListViewItem, lvDownload As System.Windows.Forms.ListView, Emachinenumber As Integer)
        XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & sdwEnrollNumber
        Application.DoEvents()

        Dim sSql As String = ""
        Dim idwFingerIndex As Integer
        Dim sTmpData As String = ""
        Dim iTmpLength As Integer
        Dim iFlag As Integer
        Dim flagenabled As String = ""
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim dsRecord As DataSet = New DataSet

        Dim sEnrollNumber As String = ""
        Dim iFaceIndex As Integer = 50  'the only possible parameter value
        Dim sTmpData1 As String = ""
        Dim iLength As String = 0
        'For idwFingerIndex = 0 To 20
        idwFingerIndex = 50
        iTmpLength = 0
        ''axCZKEM1.GetUserFaceStr(iMachineNumber, sEnrollNumber, iFaceIndex, sTmpData, iLength)
        Dim strCardno As String = ""
        'strCardno = axCZKEM1.CardNumber(0)
        axCZKEM1.GetStrCardNumber(strCardno)
        If IsNumeric(sdwEnrollNumber) = True Then
            sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber).ToString("000000000000")
        End If
        If strCardno <> 0 Then
            sSql = "select * from fptable where Enrollnumber='" & sdwEnrollNumber & "' and CardNumber='" & strCardno & "'"
            dsRecord = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(dsRecord)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(dsRecord)
            End If
            If dsRecord.Tables(0).Rows.Count > 0 Then
                sSql = "update FPTABLE set Enrollnumber='" & sdwEnrollNumber & "' where CardNumber= '" & strCardno & "'"
                'sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & sdwEnrollNumber.ToString("000000000000") & "' and CardNumber=" & strCardno
                If Common.servername = "Access" Then
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                Else
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                End If
            Else
                Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName, CardNumber, FingerNumber, Privilege) values ('" & Emachinenumber & "','" & sdwEnrollNumber & "', '" & sName.ToString() & "','" & strCardno & "', '11', '" & iPrivilege.ToString() & "')"
                If Common.servername = "Access" Then
                    cmd1 = New OleDbCommand(sqlinsert, Common.con1)
                    cmd1.ExecuteNonQuery()
                Else
                    cmd = New SqlCommand(sqlinsert, Common.con)
                    cmd.ExecuteNonQuery()
                End If
                'Strsql = "insert into tblDownloadData(UserID,EmpName,Finger,TmpData,Previlege,Password,Enabled,Flag)values('" + sdwEnrollNumber.ToString("00000000") + "','" + Name.ToString() + "','" + idwFingerIndex.ToString() + "','" + sTmpData.ToString() + "','" + iPrivilege.ToString() + "','" + sPassword.ToString() + "','" + flagenabled.ToString() + "','" + iFlag.ToString() + "')"
            End If
        End If
        'for password
        If sPassword <> "" Then
            sSql = "select * from fptable where Enrollnumber='" & sdwEnrollNumber & "' and Password='" & sPassword & "'"
            dsRecord = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(dsRecord)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(dsRecord)
            End If
            If dsRecord.Tables(0).Rows.Count > 0 Then
                sSql = "update FPTABLE set Enrollnumber='" & sdwEnrollNumber & "' where Password= '" & sPassword & "'"
                'sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & sdwEnrollNumber.ToString("000000000000") & "' and CardNumber=" & strCardno
                If Common.servername = "Access" Then
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                Else
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                End If
            Else
                Dim sqlinsert As String = "insert into fptable ([Emachinenumber], [EnrollNumber], [UserName], [Password], [FingerNumber], [Privilege]) values (" & Emachinenumber & ", '" & sdwEnrollNumber & "', '" & sName.ToString() & "', '" & sPassword & "', 10, " & iPrivilege & ")"
                If Common.servername = "Access" Then
                    cmd1 = New OleDbCommand(sqlinsert, Common.con1)
                    cmd1.ExecuteNonQuery()
                Else
                    cmd = New SqlCommand(sqlinsert, Common.con)
                    cmd.ExecuteNonQuery()
                End If
                'Strsql = "insert into tblDownloadData(UserID,EmpName,Finger,TmpData,Previlege,Password,Enabled,Flag)values('" + sdwEnrollNumber.ToString("00000000") + "','" + Name.ToString() + "','" + idwFingerIndex.ToString() + "','" + sTmpData.ToString() + "','" + iPrivilege.ToString() + "','" + sPassword.ToString() + "','" + flagenabled.ToString() + "','" + iFlag.ToString() + "')"
            End If
        End If
        'Next


        'finger
        iTmpLength = 0
        sEnrollNumber = ""
        iFaceIndex = 50  'the only possible parameter value
        sTmpData1 = ""
        iLength = 0
        If IsNumeric(sdwEnrollNumber) = True Then
            sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber)
        End If
        For idwFingerIndex = 0 To 10
            If axCZKEM1.GetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iFlag, sTmpData, iTmpLength) Then 'get the corresponding templates string and length from the memory
                lvItem = lvDownload.Items.Add(sdwEnrollNumber.ToString())
                lvItem.SubItems.Add(sName)
                lvItem.SubItems.Add(idwFingerIndex.ToString())
                lvItem.SubItems.Add(sTmpData)
                lvItem.SubItems.Add(iPrivilege.ToString())
                lvItem.SubItems.Add(sPassword)
                If sTmpData.ToString.Trim = "" Then
                    Continue For
                End If
                Dim adap1 As SqlDataAdapter
                Dim adapA1 As OleDbDataAdapter
                Dim rsEmp As DataSet = New DataSet
                If IsNumeric(sdwEnrollNumber) = True Then
                    sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber).ToString("000000000000")
                End If
                sSql = "select empname from tblemployee where presentcardno='" & sdwEnrollNumber & "'"
                'rsEmp = Cn.Execute(sSql)
                If Common.servername = "Access" Then
                    adapA1 = New OleDbDataAdapter(sSql, Common.con1)
                    adapA1.Fill(rsEmp)
                Else
                    adap1 = New SqlDataAdapter(sSql, Common.con)
                    adap1.Fill(rsEmp)
                End If
                If rsEmp.Tables(0).Rows.Count > 0 Then
                    sName = rsEmp.Tables(0).Rows(0).Item("empname").ToString.Trim
                Else
                    sName = ""
                End If

                If bEnabled = True Then
                    lvItem.SubItems.Add("true")
                    flagenabled = "true"
                Else
                    lvItem.SubItems.Add("false")
                    flagenabled = "false"
                End If
                lvItem.SubItems.Add(iFlag.ToString())

                'sSql = "select * from tblDownloadData where UserID='" & sdwEnrollNumber.ToString().Trim() & "' and  Finger='" & idwFingerIndex.ToString() & "'"
                sSql = "select * from fptable where Enrollnumber='" & sdwEnrollNumber & "' and FingerNumber=" & idwFingerIndex.ToString()
                dsRecord = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(dsRecord)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(dsRecord)
                End If
                If dsRecord.Tables(0).Rows.Count > 0 Then
                    sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & sdwEnrollNumber & "' and FingerNumber=" & idwFingerIndex.ToString()
                    If Common.servername = "Access" Then
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                    Else
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                    End If
                    Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template, FingerNumber, Privilege) values ('" & Emachinenumber & "','" & sdwEnrollNumber & "', '" & sName.ToString() & "','" & sTmpData.ToString() & "', '" & idwFingerIndex & "', '" & iPrivilege.ToString() & "')"
                    If Common.servername = "Access" Then
                        cmd1 = New OleDbCommand(sqlinsert, Common.con1)
                        cmd1.ExecuteNonQuery()
                    Else
                        cmd = New SqlCommand(sqlinsert, Common.con)
                        cmd.ExecuteNonQuery()
                    End If
                Else
                    Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template, FingerNumber, Privilege) values ('" & Emachinenumber & "','" & sdwEnrollNumber & "', '" & sName.ToString() & "','" & sTmpData.ToString() & "', '" & idwFingerIndex & "', '" & iPrivilege.ToString() & "')"
                    If Common.servername = "Access" Then
                        cmd1 = New OleDbCommand(sqlinsert, Common.con1)
                        cmd1.ExecuteNonQuery()
                    Else
                        cmd = New SqlCommand(sqlinsert, Common.con)
                        cmd.ExecuteNonQuery()
                    End If
                    'Strsql = "insert into tblDownloadData(UserID,EmpName,Finger,TmpData,Previlege,Password,Enabled,Flag)values('" + sdwEnrollNumber.ToString("00000000") + "','" + Name.ToString() + "','" + idwFingerIndex.ToString() + "','" + sTmpData.ToString() + "','" + iPrivilege.ToString() + "','" + sPassword.ToString() + "','" + flagenabled.ToString() + "','" + iFlag.ToString() + "')"
                End If
            End If
            Try
                sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber)
            Catch ex As Exception
                sdwEnrollNumber = sdwEnrollNumber
            End Try
        Next


        'face
        sEnrollNumber = ""
        iFaceIndex = 50  'the only possible parameter value
        idwFingerIndex = iFaceIndex
        sTmpData1 = ""
        iLength = 0
        'axCZKEM1.GetUserFaceStr(iMachineNumber, sEnrollNumber, iFaceIndex, sTmpData, iLength)
        If IsNumeric(sdwEnrollNumber) = True Then
            sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber)
        End If
        If axCZKEM1.GetUserFaceStr(iMachineNumber, sdwEnrollNumber, iFaceIndex, sTmpData1, iLength) Then
            lvItem = lvDownload.Items.Add(sdwEnrollNumber.ToString())
            lvItem.SubItems.Add(sName)
            lvItem.SubItems.Add(idwFingerIndex.ToString())
            lvItem.SubItems.Add(sTmpData)
            lvItem.SubItems.Add(iPrivilege.ToString())
            lvItem.SubItems.Add(sPassword)

            Dim adap1 As SqlDataAdapter
            Dim adapA1 As OleDbDataAdapter
            Dim rsEmp As DataSet = New DataSet
            If IsNumeric(sdwEnrollNumber) = True Then
                sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber).ToString("000000000000")
            End If
            sSql = "select empname from tblemployee where presentcardno='" & sdwEnrollNumber & "'"
            'rsEmp = Cn.Execute(sSql)
            If Common.servername = "Access" Then
                adapA1 = New OleDbDataAdapter(sSql, Common.con1)
                adapA1.Fill(rsEmp)
            Else
                adap1 = New SqlDataAdapter(sSql, Common.con)
                adap1.Fill(rsEmp)
            End If
            If rsEmp.Tables(0).Rows.Count > 0 Then
                sName = rsEmp.Tables(0).Rows(0).Item("empname").ToString.Trim
            Else
                sName = ""
            End If

            If bEnabled = True Then
                lvItem.SubItems.Add("true")
                flagenabled = "true"
            Else
                lvItem.SubItems.Add("false")
                flagenabled = "false"
            End If
            lvItem.SubItems.Add(iFlag.ToString())

            'sSql = "select * from tblDownloadData where UserID='" & sdwEnrollNumber.ToString().Trim() & "' and  Finger='" & idwFingerIndex.ToString() & "'"
            sSql = "select * from fptable where Enrollnumber='" & sdwEnrollNumber & "' and FingerNumber=" & idwFingerIndex.ToString()
            dsRecord = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(dsRecord)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(dsRecord)
            End If
            If dsRecord.Tables(0).Rows.Count > 0 Then
                'sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & sdwEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & idwFingerIndex.ToString()
                sSql = "Update FPTABLE set Template = '" & sTmpData1.ToString() & "', Emachinenumber= '" & Emachinenumber & "', Privilege=" & iPrivilege & " where Enrollnumber = '" & sdwEnrollNumber & "' and FingerNumber=" & idwFingerIndex.ToString()
                If Common.servername = "Access" Then
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                Else
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                End If
            Else
                Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template, FingerNumber, Privilege) values ('" & Emachinenumber & "','" & sdwEnrollNumber & "', '" & sName.ToString() & "','" & sTmpData1.ToString() & "', '" & idwFingerIndex & "', '" & iPrivilege.ToString() & "')"
                If Common.servername = "Access" Then
                    cmd1 = New OleDbCommand(sqlinsert, Common.con1)
                    cmd1.ExecuteNonQuery()
                Else
                    cmd = New SqlCommand(sqlinsert, Common.con)
                    cmd.ExecuteNonQuery()
                End If
                'Strsql = "insert into tblDownloadData(UserID,EmpName,Finger,TmpData,Previlege,Password,Enabled,Flag)values('" + sdwEnrollNumber.ToString("00000000") + "','" + Name.ToString() + "','" + idwFingerIndex.ToString() + "','" + sTmpData.ToString() + "','" + iPrivilege.ToString() + "','" + sPassword.ToString() + "','" + flagenabled.ToString() + "','" + iFlag.ToString() + "')"
            End If
        End If
    End Sub
    'Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Byte, Source As Byte, ByVal Length As Long)
    Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (ByVal Destination As Long, ByVal Source As Long, ByVal Length As Long)
    Public Declare Sub ZeroMemory Lib "kernel32" Alias "RtlZeroMemory" (Destination As Byte, ByVal Length As Long)
    Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
    Public Const gstrNoDevice = "No Device"

    Private Sub ConvFpDataToSaveInDbForCompatibility(ByRef abytSrc() As Byte, ByRef abytDest() As Byte)
        Dim nTempLen As Long, lenConvFpData As Long
        Dim bytConvFpData() As Byte
        Dim k As Int32, m As Int32
        Dim bytTemp() As Byte

        nTempLen = abytSrc.Length / 4
        lenConvFpData = nTempLen * 5
        ReDim bytConvFpData(lenConvFpData)

        For k = 0 To nTempLen - 1
            ReDim bytTemp(3)
            bytTemp(0) = abytSrc(k * 4)
            bytTemp(1) = abytSrc(k * 4 + 1)
            bytTemp(2) = abytSrc(k * 4 + 2)
            bytTemp(3) = abytSrc(k * 4 + 3)

            m = BitConverter.ToInt32(bytTemp, 0)
            bytConvFpData(k * 5) = 1
            If m < 0 Then
                If m = -2147483648 Then
                    bytConvFpData(k * 5) = 2
                    m = 2147483647
                Else
                    bytConvFpData(k * 5) = 0
                    m = -m
                End If
            End If
            bytTemp = BitConverter.GetBytes(m)
            bytConvFpData(k * 5 + 1) = bytTemp(3)
            bytConvFpData(k * 5 + 2) = bytTemp(2)
            bytConvFpData(k * 5 + 3) = bytTemp(1)
            bytConvFpData(k * 5 + 4) = bytTemp(0)
        Next

        abytDest = bytConvFpData
    End Sub
    Private Sub ToggleSwitch1_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleDownloadAll.Toggled
        If ToggleDownloadAll.IsOn = True Then
            LabelControl1.Visible = False
            TextSelectUser.Visible = False
        Else
            LabelControl1.Visible = True
            TextSelectUser.Visible = True
        End If
    End Sub
    Private Sub btnDeleteFrmDB_Click(sender As System.Object, e As System.EventArgs) Handles btnDeleteFrmDB.Click
        If GridView2.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select User from Fring print table</size>", "<size=9>Error</size>")
            Exit Sub
        Else
            If TextPassword.Text.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Please Enter Login Password</size>", "<size=9>Error</size>")
                TextPassword.Select()
                Exit Sub
            Else
                If TextPassword.Text <> Common.PASSWORD Then
                    XtraMessageBox.Show(ulf, "<size=10>Login Password Incorrect</size>", "<size=9>Error</size>")
                    TextPassword.Select()
                    Exit Sub
                End If
            End If
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Me.Cursor = Cursors.WaitCursor
                For Each c As Control In Me.Controls
                    c.Enabled = False
                Next
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                End If
                Dim sSql As String = ""
                Dim selectedRows As Integer() = GridView2.GetSelectedRows()
                Dim result As Object() = New Object(selectedRows.Length - 1) {}
                Dim FingerNumber As String
                Dim EnrollNumber As String
                For i As Integer = 0 To selectedRows.Length - 1
                    Dim rowHandle As Integer = selectedRows(i)
                    If Not GridView2.IsGroupRow(rowHandle) Then
                        EnrollNumber = GridView2.GetRowCellValue(rowHandle, "EnrollNumber").ToString.Trim

                        XtraMasterTest.LabelControlStatus.Text = "Deleting Template " & EnrollNumber
                        Application.DoEvents()

                        FingerNumber = GridView2.GetRowCellValue(rowHandle, "FingerNumber").ToString.Trim
                        sSql = "delete from fptable where EnrollNumber='" & EnrollNumber & "' and FingerNumber='" & FingerNumber & "'"
                        If Common.servername = "Access" Then
                            sSql = "delete from fptable where EnrollNumber='" & EnrollNumber & "' and FingerNumber=" & FingerNumber & ""
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                        Else
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                        End If
                    End If
                Next
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                For Each c As Control In Me.Controls
                    c.Enabled = True
                Next
                Me.Cursor = Cursors.Default
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
                If Common.servername = "Access" Then
                    Me.Fptable1TableAdapter1.Fill(Me.SSSDBDataSet.fptable1)
                    GridControl2.DataSource = SSSDBDataSet.fptable1
                Else
                    FptableTableAdapter.Connection.ConnectionString = Common.ConnectionString
                    Me.FptableTableAdapter.Fill(Me.SSSDBDataSet.fptable)
                    GridControl2.DataSource = SSSDBDataSet.fptable
                End If
                TextPassword.Text = ""
                XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
            End If
        End If
    End Sub    
    Private Sub btnUploadFinger_Click(sender As System.Object, e As System.EventArgs) Handles btnUploadFinger.Click
        Dim m As Integer
        'Dim e As Integer
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim mKey As String
        Dim vnMachineNumber As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long
        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim strDateTime As String
        Dim vRet As Long
        Dim vPrivilege As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If

        If GridView2.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Employee</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Dim sSql As String = ""
        'Me.Cursor = Cursors.WaitCursor

        'For Each c As Control In Me.Controls
        '    c.Enabled = False
        'Next

        Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim LstMachineId As String
        For i = 0 To selectedRows.Length - 1  'for device
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridView1.IsGroupRow(rowHandle) Then
                LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
       
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then

                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "HIKVISION" Then
                    Dim cn As Common = New Common
                    Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                    Dim DeviceAdd As String = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim, userName As String = Trim(GridView1.GetRowCellValue(rowHandle, "HLogin").ToString.Trim), pwd As String = Trim(GridView1.GetRowCellValue(rowHandle, "HPassword").ToString.Trim)
                    Dim lUserID As Integer = -1
                    Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID)
                    If logistatus = False Then
                        XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                        Me.Cursor = Cursors.Default
                        Exit Sub
                    Else
                        'Dim msg As New Message("Uploading", "Upload Started...")
                        'XtraMaster.AlertControl1.Show(XtraMaster, msg.Caption, msg.Text, "", msg.Image, msg)
                        XtraMasterTest.LabelControlStatus.Text = ""
                        Application.DoEvents()

                        If (-1 <> m_lSetCardCfgHandle) Then
                            If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetCardCfgHandle) Then
                                m_lSetCardCfgHandle = -1
                            End If
                        End If
                        m_bSendOne = True
                        'loop for fingerprint
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        'If axCZKEM1.BeginBatchUpdate(iMachineNumber, 1) Then
                        For x As Integer = 0 To selectedRowsEmp.Length - 1  'for user
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim adap As SqlDataAdapter
                                Dim adapA As OleDbDataAdapter
                                Dim RsFp As DataSet = New DataSet
                                Dim mUser_ID As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()
                                XtraMasterTest.LabelControlStatus.Text = "Uploading User " & mUser_ID
                                Application.DoEvents()
                                'If mUser_ID = "000075000104" Then
                                '    Dim tmp = mUser_ID
                                'End If
                                If GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim() = "11" Then
                                    'for user copy only
                                    Dim s As String = "select EMPNAME, ValidityStartdate, ValidityEnddate from TblEmployee where PRESENTCARDNO = '" & mUser_ID & "'"
                                    Dim ValidityStartdate As DateTime
                                    Dim ValidityEnddate As DateTime
                                    Dim adapZ As SqlDataAdapter
                                    Dim adapAZ As OleDbDataAdapter
                                    Dim dstmp As DataSet = New DataSet
                                    If Common.servername = "Access" Then
                                        adapAZ = New OleDbDataAdapter(s, Common.con1)
                                        adapAZ.Fill(dstmp)
                                    Else
                                        adapZ = New SqlDataAdapter(s, Common.con)
                                        adapZ.Fill(dstmp)
                                    End If
                                    Dim sName As String
                                    Dim FpNo As String = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim()
                                    Dim sdwEnrollNumber As String
                                    If IsNumeric(mUser_ID) = True Then
                                        sdwEnrollNumber = Convert.ToDouble(mUser_ID)
                                    Else
                                        sdwEnrollNumber = mUser_ID
                                    End If
                                    If dstmp.Tables(0).Rows.Count > 0 Then
                                        sName = dstmp.Tables(0).Rows(0)("EMPNAME").ToString().Trim()
                                        ValidityStartdate = Convert.ToDateTime(dstmp.Tables(0).Rows(0)("ValidityStartdate").ToString().Trim())
                                        ValidityEnddate = Convert.ToDateTime(dstmp.Tables(0).Rows(0)("ValidityEnddate").ToString().Trim())
                                    Else
                                        sName = ""
                                        ValidityStartdate = Convert.ToDateTime("2018-01-01")
                                        ValidityEnddate = Convert.ToDateTime("2030-12-31")
                                    End If

                                    'all values in m_struSelSendCardCfg
                                    m_struSelSendCardCfg = GetSelItem(rowHandleEmp, ValidityStartdate, ValidityEnddate, sName)


                                    Dim struCond As CHCNetSDK.NET_DVR_CARD_CFG_COND = New CHCNetSDK.NET_DVR_CARD_CFG_COND
                                    struCond.dwSize = CType(Marshal.SizeOf(struCond), UInteger)
                                    struCond.dwCardNum = 1
                                    struCond.wLocalControllerID = 0
                                    struCond.byCheckCardNo = 1
                                    m_BSendSel = True
                                    Dim dwSize As Integer = Marshal.SizeOf(struCond)
                                    Dim ptrStruCond As IntPtr = Marshal.AllocHGlobal(dwSize)
                                    Marshal.StructureToPtr(struCond, ptrStruCond, False)
                                    g_fSetGatewayCardCallback = New CHCNetSDK.RemoteConfigCallback(AddressOf ProcessSetGatewayCardCallback)
                                    Thread.Sleep(200)
                                    m_lSetCardCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_SET_CARD_CFG_V50, ptrStruCond, dwSize, g_fSetGatewayCardCallback, Me.Handle)
                                    Thread.Sleep(200)
                                    If (-1 = m_lSetCardCfgHandle) Then
                                        Dim listItem As ListViewItem = New ListViewItem
                                        listItem.Text = "FAIL"
                                        Dim strTemp As String = String.Format("NET_DVR_SET_CARD_CFG_V50 FAIL, ERROR CODE {0}", CHCNetSDK.NET_DVR_GetLastError)
                                        listItem.SubItems.Add(strTemp)
                                        'Me.AddList(listViewMessage, listItem, True)
                                        Marshal.FreeHGlobal(ptrStruCond)
                                        'Return
                                        Continue For
                                    Else
                                        Dim listItem As ListViewItem = New ListViewItem
                                        listItem.Text = "SUCC"
                                        listItem.SubItems.Add("NET_DVR_SET_CARD_CFG_V50")
                                        'Me.AddList(listViewMessage, listItem, True)
                                    End If
                                    Marshal.FreeHGlobal(ptrStruCond)


                                    If m_BSendSel Then
                                        m_dwCardNum = 1
                                        m_dwSendIndex = 0
                                        SendCardData(m_struSelSendCardCfg)

                                        m_BSendSel = False
                                        m_struSelSendCardCfg = New CHCNetSDK.NET_DVR_CARD_CFG_V50
                                        'Return
                                        Continue For
                                    End If
                                End If
                            End If
                        next
                        For x As Integer = 0 To selectedRowsEmp.Length - 1  'for template
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim adap As SqlDataAdapter
                                Dim adapA As OleDbDataAdapter
                                Dim RsFp As DataSet = New DataSet
                                Dim mUser_ID As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()
                                XtraMasterTest.LabelControlStatus.Text = "Uploading Template " & mUser_ID
                                Application.DoEvents()
                                If GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim() < "11" And GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim() > "0" Then
                                    'for FP
                                    'If (m_lSetFingerPrintCfgHandle <> -1) Then
                                    '    CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetFingerPrintCfgHandle)
                                    'End If

                                    Dim struCond As CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_COND_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_COND_V50
                                    struCond.byCardNo = New Byte((32) - 1) {}
                                    struCond.byEmployeeNo = New Byte((32) - 1) {}
                                    struCond.byEnableCardReader = New Byte((512) - 1) {}
                                    struCond.dwSize = CType(Marshal.SizeOf(struCond), UInteger)
                                    struCond.dwFingerPrintNum = 1
                                    struCond.byCallbackMode = 0
                                    Dim dwSize As Integer = Marshal.SizeOf(struCond)
                                    Dim ptrStruCond As IntPtr = Marshal.AllocHGlobal(dwSize)

                                    Dim byTempEmployeeNo() As Byte = System.Text.Encoding.UTF8.GetBytes("")
                                    Dim y As Integer = 0
                                    Do While (y < byTempEmployeeNo.Length)
                                        struCond.byEmployeeNo(y) = byTempEmployeeNo(y)
                                        y = (y + 1)
                                    Loop
                                    struCond.byFingerPrintID = 1
                                    If IsNumeric(mUser_ID) = True Then
                                        mUser_ID = Convert.ToDouble(mUser_ID)
                                    Else
                                        mUser_ID = mUser_ID
                                    End If
                                    Dim byTemp() As Byte = System.Text.Encoding.UTF8.GetBytes(mUser_ID)
                                    y = 0
                                    Do While (y < byTemp.Length)
                                        struCond.byCardNo(y) = byTemp(y)
                                        y = (y + 1)
                                    Loop
                                    m_struFingerPrintOne.byEnableCardReader(0) = 1
                                    m_struFingerPrintOne.byLeaderFP(0) = 1
                                    struCond.byEnableCardReader = m_struFingerPrintOne.byEnableCardReader
                                    Marshal.StructureToPtr(struCond, ptrStruCond, False)
                                    g_fSetFingerPrintCallback = New CHCNetSDK.RemoteConfigCallback(AddressOf ProcessSetFingerPrintCfgCallbackData)
                                    Thread.Sleep(200)
                                    m_lSetFingerPrintCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_SET_FINGERPRINT_CFG_V50, ptrStruCond, dwSize, g_fSetFingerPrintCallback, Me.Handle)
                                    Thread.Sleep(200)
                                    If (-1 = m_lSetFingerPrintCfgHandle) Then
                                        Dim listItem As ListViewItem = New ListViewItem
                                        listItem.Text = "FAIL"
                                        Dim strTemp As String = String.Format("NET_DVR_SET_FINGERPRINT_CFG_V50 FAIL, ERROR CODE {0}", CHCNetSDK.NET_DVR_GetLastError)
                                        listItem.SubItems.Add(strTemp)
                                        'Me.AddList(listViewMessage, listItem)
                                        Marshal.FreeHGlobal(ptrStruCond)
                                        Return
                                    Else
                                        Dim listItem As ListViewItem = New ListViewItem
                                        listItem.Text = "SUCC"
                                        listItem.SubItems.Add("NET_DVR_SET_FINGERPRINT_CFG_V50")
                                        'Me.AddList(listViewMessage, listItem)
                                    End If
                                    m_iSendIndex = 0
                                    'If Not SendFirstCard Then
                                    Dim struFingerPrintCfg As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50
                                    struFingerPrintCfg.byCardNo = New Byte((32) - 1) {}
                                    struFingerPrintCfg.byEmployeeNo = New Byte((32) - 1) {}
                                    struFingerPrintCfg.byEnableCardReader = New Byte((512) - 1) {}
                                    struFingerPrintCfg.byLeaderFP = New Byte((256) - 1) {}
                                    struFingerPrintCfg.byFingerData = New Byte((CHCNetSDK.MAX_FINGER_PRINT_LEN) - 1) {}
                                    'Dim cardNumber As String
                                    Dim FpNo As String = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim()
                                    Dim FPath As String = GridView2.GetRowCellValue(rowHandleEmp, "HTemplatePath").ToString.Trim()

                                    'If IsNumeric(mUser_ID) = True Then
                                    '    cardNumber = Convert.ToDouble(mUser_ID)
                                    'Else
                                    '    cardNumber = mUser_ID
                                    'End If
                                    UpdateFingerPrintCfg(struFingerPrintCfg, mUser_ID, FpNo, FPath)

                                    Dim dwSize1 As UInteger = CType(Marshal.SizeOf(struFingerPrintCfg), UInteger)
                                    Dim ptrNowSendCard As IntPtr = Marshal.AllocHGlobal(CType(dwSize1, Integer))
                                    Marshal.StructureToPtr(struFingerPrintCfg, ptrNowSendCard, False)
                                    If Not CHCNetSDK.NET_DVR_SendRemoteConfig(m_lSetFingerPrintCfgHandle, 3, ptrNowSendCard, dwSize1) Then
                                        Dim listItem As ListViewItem = New ListViewItem
                                        listItem.Text = "FAIL"
                                        Dim strTemp As String = String.Format("Send Fail,CardNo:{0},EmployeeNo:{1}", System.Text.Encoding.UTF8.GetString(struFingerPrintCfg.byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                                        listItem.SubItems.Add(strTemp)
                                        'Me.AddList(listViewMessage, listItem)
                                        Marshal.FreeHGlobal(ptrNowSendCard)
                                        'Return False
                                    End If

                                    Marshal.FreeHGlobal(ptrNowSendCard)
                                    'End If
                                    Marshal.FreeHGlobal(ptrStruCond)
                                End If
                            End If
                        Next
                    End If
                End If
            End If
        Next i
        For Each c As Control In Me.Controls
            c.Enabled = True
        Next
        Me.Cursor = Cursors.Default
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        ' Screen.MousePointer = vbNormal
        'MsgBox("Task Completed", vbOKOnly)
        XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
    End Sub
    Private Function GetSelItem(ByVal rowHandleEmp As Integer, ValidityStartdate As DateTime, ValidityEnddate As DateTime, sName As String) As CHCNetSDK.NET_DVR_CARD_CFG_V50
        Dim iPos As Integer = 0
        'If (rowHandleEmp >= 0) Then
        '    iPos = rowHandleEmp
        'End If

        Dim struCardInfo As CHCNetSDK.NET_DVR_CARD_CFG_V50 = New CHCNetSDK.NET_DVR_CARD_CFG_V50
        'If (iPos < 0) Then
        '    Return struCardInfo
        'End If

        struCardInfo.byCardNo = New Byte((32) - 1) {}
        struCardInfo.byName = New Byte((32) - 1) {}

        struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H80 'card password
        struCardInfo.byCardPassword = New Byte((8) - 1) {}

        struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H40 'Belongs to group parameters 
        struCardInfo.byBelongGroup = New Byte((128) - 1) {}

        struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H8  'DoorLimitParameter
        struCardInfo.byDoorRight = New Byte((256) - 1) {}
        struCardInfo.byLockCode = New Byte((8) - 1) {}
        struCardInfo.byRes2 = New Byte((3) - 1) {}
        struCardInfo.byRes3 = New Byte((83) - 1) {}
        struCardInfo.byRoomCode = New Byte((8) - 1) {}

        struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H100 'Card authority scheme
        struCardInfo.wCardRightPlan = New UShort((1024) - 1) {}
        struCardInfo.wCardRightPlan(0) = 1


        struCardInfo.struValid.byEnable = 1
        struCardInfo.struValid.byRes1 = New Byte((3) - 1) {}
        struCardInfo.struValid.byRes2 = New Byte((32) - 1) {}

        struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H2 'for validity
        struCardInfo.struValid.struBeginTime.wYear = ValidityStartdate.Year
        struCardInfo.struValid.struBeginTime.byMonth = ValidityStartdate.Month
        struCardInfo.struValid.struBeginTime.byDay = ValidityStartdate.Day
        struCardInfo.struValid.struBeginTime.byHour = ValidityStartdate.Hour
        struCardInfo.struValid.struBeginTime.byMinute = ValidityStartdate.Minute
        struCardInfo.struValid.struBeginTime.bySecond = ValidityStartdate.Second

        struCardInfo.struValid.struEndTime.wYear = ValidityEnddate.Year
        struCardInfo.struValid.struEndTime.byMonth = ValidityEnddate.Month
        struCardInfo.struValid.struEndTime.byDay = ValidityEnddate.Day
        struCardInfo.struValid.struEndTime.byHour = ValidityEnddate.Hour
        struCardInfo.struValid.struEndTime.byMinute = ValidityEnddate.Minute
        struCardInfo.struValid.struEndTime.bySecond = ValidityEnddate.Second


        struCardInfo.dwSize = CType(Marshal.SizeOf(struCardInfo), UInteger)
        Dim password As String = GridView2.GetRowCellValue(rowHandleEmp, "Password").ToString.Trim()
        Dim User_ID As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()
        Dim sdwEnrollNumber As String
        If IsNumeric(User_ID) = True Then
            sdwEnrollNumber = Convert.ToDouble(User_ID)
        Else
            sdwEnrollNumber = User_ID
        End If
        Dim byTemp() As Byte = System.Text.Encoding.UTF8.GetBytes(sdwEnrollNumber)
        Dim y As Integer = 0
        Do While (y < byTemp.Length)
            struCardInfo.byCardNo(y) = byTemp(y)
            y = (y + 1)
        Loop

        byTemp = System.Text.Encoding.UTF8.GetBytes(sName)
        y = 0
        Do While (y < byTemp.Length)
            struCardInfo.byName(y) = byTemp(y)
            y = (y + 1)
        Loop

        byTemp = System.Text.Encoding.UTF8.GetBytes(password)
        y = 0
        Do While (y < byTemp.Length)
            struCardInfo.byCardPassword(y) = byTemp(y)
            y = (y + 1)
        Loop
        'struCardInfo.byCardType = 1
        struCardInfo.byCardValid = 1
        struCardInfo.byLeaderCard = 0
        struCardInfo.bySchedulePlanType = 2
        struCardInfo.dwEmployeeNo = sdwEnrollNumber
        struCardInfo.wDepartmentNo = 1
        struCardInfo.wSchedulePlanNo = 1
        struCardInfo.byDoorRight(0) = 1
        'new values hardcoded
        struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H4  'for card type
        struCardInfo.byCardType = CByte((1))

        Short.TryParse(0, struCardInfo.wFloorNumber)

        'struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H10 'FirstCardParameters
        'struCardInfo.byLeaderCard = 1

        'struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H20 'The maximum swiping card number
        'UInteger.TryParse(textBoxMaximumCreditCard.Text, m_struCardCfg.dwMaxSwipeTime)

        'struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H200  'Swiped number
        'UInteger.TryParse(textBoxreditCard.Text, m_struCardCfg.dwSwipeTime)

        m_struCardInfo(0) = struCardInfo
        Return m_struCardInfo(0)
    End Function
    Private Function UpdateFingerPrintCfg(ByRef struFingerPrintCfg As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50, ByVal cardNumber As String, ByVal FNumber As String, ByVal FPath As String) As Boolean
        struFingerPrintCfg.dwSize = CType(Marshal.SizeOf(struFingerPrintCfg), UInteger)
        Dim byTempCard() As Byte = System.Text.Encoding.UTF8.GetBytes(cardNumber)
        Dim i As Integer = 0
        Do While (i < byTempCard.Length)
            If (i > struFingerPrintCfg.byCardNo.Length) Then
                Return False
            End If

            struFingerPrintCfg.byCardNo(i) = byTempCard(i)
            i = (i + 1)
        Loop

        struFingerPrintCfg.dwFingerPrintLen = 512
        struFingerPrintCfg.byEnableCardReader = m_struFingerPrintOne.byEnableCardReader
        struFingerPrintCfg.byLeaderFP = m_struFingerPrintOne.byLeaderFP

        Byte.TryParse(FNumber, struFingerPrintCfg.byFingerPrintID)
        'struFingerPrintCfg.byFingerType = CType(comboBoxFingerprintType.SelectedIndex, Byte)
        struFingerPrintCfg.byFingerType = CType(0, Byte)
        Dim byTempEmployeeNo() As Byte = System.Text.Encoding.UTF8.GetBytes("")
        i = 0
        Do While (i < byTempEmployeeNo.Length)
            If (i > struFingerPrintCfg.byEmployeeNo.Length) Then
                Return False
            End If

            struFingerPrintCfg.byEmployeeNo(i) = byTempEmployeeNo(i)
            i = (i + 1)
        Loop
        Dim fs As FileStream = New FileStream(FPath, FileMode.OpenOrCreate)
        If (0 = fs.Length) Then
            struFingerPrintCfg.byFingerData(0) = 0
            fs.Close()
            Return True
        End If
        Dim objBinaryReader As BinaryReader = New BinaryReader(fs)
        If (struFingerPrintCfg.dwFingerPrintLen > CHCNetSDK.MAX_FINGER_PRINT_LEN) Then
            Return False
        End If
        i = 0
        Do While (i < struFingerPrintCfg.dwFingerPrintLen)
            If (i >= fs.Length) Then
                Exit Do
            End If
            struFingerPrintCfg.byFingerData(i) = objBinaryReader.ReadByte
            i = (i + 1)
        Loop
        fs.Close()
        Return True
    End Function
    Private Sub SendCardData(ByVal struCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50, Optional ByVal dwDiffTime As UInteger = 0)
        If (-1 = m_lSetCardCfgHandle) Then
            Return
        End If
        Dim dwSize As UInteger = CType(Marshal.SizeOf(struCardCfg), UInteger)
        Dim ptrStruCard As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Integer))
        Marshal.StructureToPtr(struCardCfg, ptrStruCard, False)
        If Not CHCNetSDK.NET_DVR_SendRemoteConfig(m_lSetCardCfgHandle, 3, ptrStruCard, dwSize) Then
            Marshal.FreeHGlobal(ptrStruCard)
            Return
        End If
        Marshal.FreeHGlobal(ptrStruCard)
        Return
    End Sub
    Private Sub ProcessSetGatewayCardCallback(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
        If (pUserData = Nothing) Then
            Return
        End If

        Dim dwStatus As UInteger = CType(Marshal.ReadInt32(lpBuffer), UInteger)
        If (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_PROCESSING, UInteger)) Then
            ' just example need refinement
            Dim listItem As ListViewItem = New ListViewItem
            listItem.Text = "SUCC"
            Dim strTemp As String = Nothing
            If m_bSendOne Then
                'strTemp = String.Format("Send SUCC,CardNO:{0}", System.Text.Encoding.UTF8.GetString(m_struCardInfo(m_iSelectIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                m_bSendOne = False
            Else
                'strTemp = String.Format("Send SUCC,CardNO:{0}", System.Text.Encoding.UTF8.GetString(m_struCardInfo(m_dwSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
            End If

            'listItem.SubItems.Add(strTemp)
            'Me.AddList(listViewMessage, listItem, True)
            'next
            CHCNetSDK.PostMessage(pUserData, 1002, 0, 0)
        ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_FAILED, UInteger)) Then
            Dim listItem As ListViewItem = New ListViewItem
            listItem.Text = "FAIL"
            Dim strTemp As String = Nothing
            Dim errorCode As UInteger = CType(Marshal.ReadInt32((lpBuffer + 4)), UInteger)
            Dim errorUserID As UInteger = CType(Marshal.ReadInt32((lpBuffer + 40)), UInteger)
            'strTemp = String.Format("Send FAILED,CardNO:{0},Error code{1},USER ID{2}", System.Text.Encoding.UTF8.GetString(m_struSelSendCardCfg.byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), errorCode, errorUserID)
            'listItem.SubItems.Add(strTemp)
            'Me.AddList(listViewMessage, listItem, True)
            'next
            Dim x As Integer = CHCNetSDK.NET_DVR_GetLastError
            Dim tmp As String = m_struSelSendCardCfg.dwEmployeeNo
            CHCNetSDK.PostMessage(pUserData, 1002, 0, 0)
        ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_SUCCESS, UInteger)) Then
            Dim listItem As ListViewItem = New ListViewItem
            listItem.Text = "SUCC"
            listItem.SubItems.Add("NET_DVR_SET_CARD_CFG_V50 Set finish")
            'Me.AddList(listViewMessage, listItem, True)
            CHCNetSDK.PostMessage(pUserData, 1001, 0, 0)
        ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_EXCEPTION, UInteger)) Then
            Dim listItem As ListViewItem = New ListViewItem
            listItem.Text = "FAIL"
            listItem.SubItems.Add("NET_DVR_SET_CARD_CFG_V50 Set FAIL")
            'Me.AddList(listViewMessage, listItem, True)
            CHCNetSDK.PostMessage(pUserData, 1001, 0, 0)
        End If

        Return
    End Sub
    Private Sub ConvFpDataAfterReadFromDbForCompatibility(ByRef abytSrc() As Byte, ByRef abytDest() As Byte)
        Dim nTempLen As Long, lenConvFpData As Long
        Dim bytConvFpData() As Byte
        Dim k As Long, m As Long
        Dim bytTemp() As Byte

        nTempLen = abytSrc.Length / 5
        lenConvFpData = nTempLen * 4

        If lenConvFpData < FP_DATA_SIZE Then lenConvFpData = FP_DATA_SIZE
        ReDim bytConvFpData(lenConvFpData)

        For k = 0 To nTempLen - 1
            ReDim bytTemp(3)
            bytTemp(0) = abytSrc(k * 5 + 4)
            bytTemp(1) = abytSrc(k * 5 + 3)
            bytTemp(2) = abytSrc(k * 5 + 2)
            bytTemp(3) = abytSrc(k * 5 + 1)

            m = BitConverter.ToInt32(bytTemp, 0)
            If abytSrc(k * 5) = 0 Then
                m = -m
            ElseIf abytSrc(k * 5) = 2 Then
                m = -2147483648
            End If
            bytTemp = BitConverter.GetBytes(m)

            bytConvFpData(k * 4 + 3) = bytTemp(3)
            bytConvFpData(k * 4 + 2) = bytTemp(2)
            bytConvFpData(k * 4 + 1) = bytTemp(1)
            bytConvFpData(k * 4 + 0) = bytTemp(0)
        Next

        abytDest = bytConvFpData
    End Sub
    Private Sub ProcessSetFingerPrintCfgCallbackData(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
        If (pUserData = Nothing) Then
            Return
        End If

        If (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_STATUS, UInteger)) Then
            Dim dwStatus As UInteger = CType(Marshal.ReadInt32(lpBuffer), UInteger)
            If (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_PROCESSING, UInteger)) Then
                'some problem
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "SUCC"
                Dim strTemp As String = Nothing
                strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1}", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                listItem.SubItems.Add(strTemp)
                'Me.AddList(listViewMessage, listItem)
                SendNextFingerPrint()
            ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_FAILED, UInteger)) Then
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "FAIL"
                listItem.SubItems.Add("SetFingerPrint Err:NET_SDK_CALLBACK_STATUS_FAILED")
                'Me.AddList(listViewMessage, listItem)
                SendNextFingerPrint()
            ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_SUCCESS, UInteger)) Then
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "SUCC"
                listItem.SubItems.Add("SetFingerPrint SUCCESS")
                'Me.AddList(listViewMessage, listItem)
                Dim listItem2 As ListViewItem = New ListViewItem
                listItem2.Text = "SUCC"
                listItem2.SubItems.Add("NET_DVR_SET_FINGERPRINT_CFG_V50 Set finish")
                'Me.AddList(listViewMessage, listItem2)
                CHCNetSDK.PostMessage(pUserData, 1001, 0, 0)
            ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_EXCEPTION, UInteger)) Then
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "FAIL"
                listItem.SubItems.Add("SetFingerPrint EXCEPTION")
                'Me.AddList(listViewMessage, listItem)
                Dim listItem2 As ListViewItem = New ListViewItem
                listItem2.Text = "SUCC"
                listItem2.SubItems.Add("NET_DVR_SET_FINGERPRINT_CFG_V50 Set finish")
                'Me.AddList(listViewMessage, listItem2)
            Else
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "SUCC"
                listItem.SubItems.Add("SetFingerPrint SUCCESS")
                'Me.AddList(listViewMessage, listItem)
                Dim listItem2 As ListViewItem = New ListViewItem
                listItem2.Text = "SUCC"
                listItem2.SubItems.Add("NET_DVR_SET_FINGERPRINT_CFG_V50 Set finish")
                'Me.AddList(listViewMessage, listItem2)
            End If

        ElseIf (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_DATA, UInteger)) Then
            Dim struCfg As CHCNetSDK.NET_DVR_FINGER_PRINT_STATUS_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_STATUS_V50
            struCfg = CType(Marshal.PtrToStructure(lpBuffer, GetType(CHCNetSDK.NET_DVR_FINGER_PRINT_STATUS_V50)), CHCNetSDK.NET_DVR_FINGER_PRINT_STATUS_V50)
            Dim bSendOK As Boolean = False
            If (struCfg.byRecvStatus = 0) Then
                Dim i As Integer = 0
                Do While (i < struCfg.byCardReaderRecvStatus.Length)
                    If (1 = struCfg.byCardReaderRecvStatus(i)) Then
                        bSendOK = True
                        Dim listItem As ListViewItem = New ListViewItem
                        listItem.Text = "SUCC"
                        Dim strTemp As String = Nothing
                        'strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1},CardReader:{2}", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), (i + 1))
                        listItem.SubItems.Add(strTemp)
                        'Me.AddList(listViewMessage, listItem)
                    ElseIf (8 = struCfg.byCardReaderRecvStatus(i)) Then
                        'd:8�h�MnE�Sp�U�o

                    Else
                        bSendOK = False
                        Dim listItem As ListViewItem = New ListViewItem
                        listItem.Text = "FAIL"
                        Dim strTemp As String = Nothing
                        'strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1},CardReader:{2},Error Code:{3}", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), (i + 1), struCfg.byCardReaderRecvStatus(i))
                        listItem.SubItems.Add(strTemp)
                        'Me.AddList(listViewMessage, listItem)
                    End If

                    i = (i + 1)
                Loop

            Else
                bSendOK = False
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "FAIL"
                Dim strTemp As String = Nothing
                If (struCfg.byRecvStatus = 1) Then
                    strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1},Error Finger ID", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                ElseIf (struCfg.byRecvStatus = 2) Then
                    strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1},Error Finger Print Type", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                ElseIf (struCfg.byRecvStatus = 3) Then
                    strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1},Illegal Card No", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                ElseIf (struCfg.byRecvStatus = 4) Then
                    strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1},Finger Print Not Link Employee No Or Card No", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                ElseIf (struCfg.byRecvStatus = 5) Then
                    strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1},Employee No Not Exist", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                Else
                    strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1},Error Code:{2}", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), struCfg.byRecvStatus)
                End If

                listItem.SubItems.Add(strTemp)
                'Me.AddList(listViewMessage, listItem)
            End If

            If Not bSendOK Then
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "FAIL"
                Dim strTemp As String = Nothing
                strTemp = String.Format("SetFingerPrint Failed,CardNo:{0},EmployeeNo:{1}", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                listItem.SubItems.Add(strTemp)
                'Me.AddList(listViewMessage, listItem)
            End If

            'if (0 == struCfg.byTotalStatus)
            '{
            '}
            SendNextFingerPrint()
        End If

    End Sub
    Private Function SendNextFingerPrint() As Boolean
        'IntPtr ihWnd = this.Handle;
        If (-1 = m_lSetFingerPrintCfgHandle) Then
            Return False
        End If

        m_iSendIndex = (m_iSendIndex + 1)
        Dim strTempText As String = "0" 'GetTextBoxText(textBoxNumber)
        Dim indexTmp As Integer = 0
        Integer.TryParse(strTempText, indexTmp)
        If (m_iSendIndex >= indexTmp) Then
            Return True
        End If

        Dim dwSize As UInteger = CType(Marshal.SizeOf(m_struRecordCardCfg(m_iSendIndex)), UInteger)
        Dim ptrNowSendCard As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Integer))
        Marshal.StructureToPtr(m_struRecordCardCfg(m_iSendIndex), ptrNowSendCard, False)
        If Not CHCNetSDK.NET_DVR_SendRemoteConfig(m_lSetFingerPrintCfgHandle, 3, ptrNowSendCard, dwSize) Then
            Dim listItem As ListViewItem = New ListViewItem
            listItem.Text = "FAIL"
            Dim strTemp As String = String.Format("Send Fail,CardNo:{0},EmployeeNo:{1}", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
            listItem.SubItems.Add(strTemp)
            'Me.AddList(listViewMessage, listItem)
            Marshal.FreeHGlobal(ptrNowSendCard)
            Return False
        End If

        Marshal.FreeHGlobal(ptrNowSendCard)
        Return True
    End Function
    Private Sub btnDeleteFrmDevice_Click(sender As System.Object, e As System.EventArgs) Handles btnDeleteFrmDevice.Click
        Dim m As Integer
        'Dim e As Integer
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim mKey As String
        Dim vnMachineNumber As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long
        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim strDateTime As String
        Dim vRet As Long
        Dim vPrivilege As Long
        Dim mEmp As Integer
        Dim vFingerNumber As Long
        Dim vnResultCode As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If GridView2.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Employee</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If TextPassword.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please Enter Login Password</size>", "<size=9>Error</size>")
            TextPassword.Select()
            Exit Sub
        Else
            If TextPassword.Text <> Common.PASSWORD Then
                XtraMessageBox.Show(ulf, "<size=10>Login Password Incorrect</size>", "<size=9>Error</size>")
                TextPassword.Select()
                Exit Sub
            End If
        End If

        If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next
            Me.Cursor = Cursors.WaitCursor
            Dim sSql As String = ""
            Dim commkey As Integer
            Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            Dim LstMachineId As String
            For i = 0 To selectedRows.Length - 1
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridView1.IsGroupRow(rowHandle) Then
                    LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                    'sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
                    Dim bConn As Boolean = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)

                    vpszIPAddress = Trim(lpszIPAddress)
                    If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0 'PROTOCOL_TCPIP
                        'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            vRet = FK_ConnectUSB(vnMachineNumber, vnLicense)
                            '    vstrTelNumber = ""
                            'vnWaitDialTime = 0
                            'vnCommPort = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Val(Trim(rstm!Location))
                            'vnCommBaudrate = 38400
                            ''*************************
                        Else
                            vpszIPAddress = Trim(lpszIPAddress)
                            vpszNetPort = CLng("5005")
                            vpszNetPassword = CLng("0")
                            vnTimeOut = CLng("5000")
                            vnProtocolType = 0
                            vRet = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        End If
                        If vRet > 0 Then
                            bConn = True
                        Else
                            bConn = False
                        End If


                        If vRet > 0 Then
                            mnCommHandleIndex = 1
                            Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                            Dim resultemp As Object() = New Object(selectedRowsEmp.Length - 1) {}

                            For x As Integer = 0 To selectedRowsEmp.Length - 1
                                Dim rowHandleemp As Integer = selectedRowsEmp(x)
                                If Not GridView2.IsGroupRow(rowHandleemp) Then
                                    mEmp = GridView2.GetRowCellValue(rowHandleemp, "EnrollNumber").ToString.Trim ' CInt(Val(Left(LstEmployeesTarget.Text, 8)))
                                    XtraMasterTest.LabelControlStatus.Text = "Deleting Template " & mEmp
                                    Application.DoEvents()
                                    'If GridView2.GetRowCellValue(rowHandleemp, "FingerNumber").ToString.Trim = "11" Then
                                    '    vFingerNumber = 11
                                    '    'ElseIf Trim(Right(LstEmployeesTarget.Text, 3)) = "PWD" Or Trim(Right(LstEmployeesTarget.Text, 4)) = "FACE" Then
                                    'ElseIf GridView2.GetRowCellValue(rowHandleemp, "FingerNumber").ToString.Trim = "10" Then
                                    '    vFingerNumber = 10
                                    'ElseIf GridView2.GetRowCellValue(rowHandleemp, "FingerNumber").ToString.Trim = "12" Then
                                    '    vFingerNumber = 12
                                    'Else
                                    '    'vFingerNumber = Trim(Right(LstEmployeesTarget.Text, 1)) - 1
                                    '    vFingerNumber = 0
                                    'End If
                                    vFingerNumber = GridView2.GetRowCellValue(rowHandleemp, "FingerNumber").ToString.Trim
                                    vnResultCode = FK_DeleteEnrollData(vnResultCode, _
                                                        mEmp, _
                                                        vFingerNumber)

                                    'bdel = FK623Attend.DeleteEnrollData(CInt(Val(Left(LstEmployeesTarget.Text, 8))), vFingerNumber)
                                    'If vnResultCode = 1 And Pdel = False Then
                                    '    Pdel = True
                                    'End If
                                    'Next k
                                    If vnResultCode = 1 Then
                                        ' MsgBox "User has been deleted successfully", vbOKOnly
                                    Else
                                        'MsgBox("Unable to delete the user " & mEmp & " from device " & vnMachineNumber & "", vbOKOnly)                                       
                                        XtraMessageBox.Show(ulf, "<size=10>Unable to delete the user " & mEmp & " from device " & vnMachineNumber & "</size>", "<size=9>Information</size>")
                                    End If
                                End If
                            Next
                        Else
                            'MsgBox("Device No: " & vnMachineNumber & " Not connected..")
                            XtraMessageBox.Show(ulf, "<size=10>Device No: " & vnMachineNumber & " Not connected..</size>", "<size=9>Information</size>")
                        End If
                        'FK623Attend.Disconnect
                        FK_DisConnect(1)
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                        Dim sdwEnrollNumber As Integer
                        Dim idwVerifyMode As Integer
                        Dim idwWorkcode As Integer

                        Dim bIsConnected = False
                        Dim iMachineNumber As Integer = result(i)
                        Dim idwErrorCode As Integer
                        Dim com As Common = New Common
                        Dim axCZKEM1 As New zkemkeeper.CZKEM
                        commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                        Dim sName As String = ""
                        Dim sPassword As String = ""
                        Dim iPrivilege As Integer
                        Dim bEnabled As Boolean = False
                        Dim idwFingerIndex As Integer
                        Dim sTmpData As String = ""
                        Dim iTmpLength As Integer
                        Dim iFlag As Integer
                        Dim flagenabled As String = ""
                        Dim adap As SqlDataAdapter
                        Dim adapA As OleDbDataAdapter
                        Dim dsRecord As DataSet = New DataSet
                        axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                        bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                        If bIsConnected = True Then
                            Dim lvDownload As System.Windows.Forms.ListView = New System.Windows.Forms.ListView
                            lvDownload.Items.Clear()
                            lvDownload.BeginUpdate()
                            axCZKEM1.EnableDevice(iMachineNumber, False)
                            Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                            Dim resultemp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                            Dim fingerNum As Integer
                            Dim tmp As Boolean
                            For x As Integer = 0 To selectedRowsEmp.Length - 1
                                Dim rowHandleemp As Integer = selectedRowsEmp(x)
                                If Not GridView2.IsGroupRow(rowHandleemp) Then
                                    mEmp = GridView2.GetRowCellValue(rowHandleemp, "EnrollNumber").ToString.Trim
                                    XtraMasterTest.LabelControlStatus.Text = "Deleting Template " & mEmp
                                    Application.DoEvents()
                                    fingerNum = 12 'GridView2.GetRowCellValue(rowHandleemp, "FingerNumber").ToString.Trim
                                    tmp = axCZKEM1.SSR_DeleteEnrollData(vnMachineNumber, mEmp, fingerNum)
                                    'If fingerNum = 11 Or fingerNum = 10 Then
                                    '    'fingerNum = 12
                                    'End If
                                    'If fingerNum = 50 Then    'ful delete
                                    '    tmp = axCZKEM1.DelUserTmpExt(vnMachineNumber, mEmp, vnMachineNumber, fingerNum)
                                    'ElseIf fingerNum = 10 Then
                                    '    tmp = axCZKEM1.SSR_DeleteEnrollData(vnMachineNumber, mEmp, fingerNum)
                                    'Else   'indexed value delete
                                    '    tmp = axCZKEM1.SSR_DelUserTmpExt(vnMachineNumber, mEmp, fingerNum)
                                    'End If
                                End If
                            Next
                        End If
                        axCZKEM1.EnableDevice(iMachineNumber, True)
                        axCZKEM1.Disconnect()
                    End If
                End If
            Next
            For Each c As Control In Me.Controls
                c.Enabled = True
            Next
            TextPassword.Text = ""
            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
        End If
    End Sub
    Private Sub btnClearAdmin_Click(sender As System.Object, e As System.EventArgs) Handles btnClearAdmin.Click
        Dim bConn As Boolean
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim vEMachineNumber As Long
        Dim vEnrollNumber As Long
        Dim vFingerNumber As Long
        Dim vPrivilege As Long
        Dim vEnable As Long
        Dim vRet As Boolean
        Dim mKey As String
        Dim vnResultCode As Long
        Dim RsDevice As New DataSet 'ADODB.Recordset
        vnResultCode = 0
        Dim vnMachineNumber As Long

        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If TextPassword.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please Enter Login Password</size>", "<size=9>Error</size>")
            TextPassword.Select()
            Exit Sub
        Else
            If TextPassword.Text <> Common.PASSWORD Then
                XtraMessageBox.Show(ulf, "<size=10>Login Password Incorrect</size>", "<size=9>Error</size>")
                TextPassword.Select()
                Exit Sub
            End If
        End If
        If XtraMessageBox.Show(ulf, "<size=10>Are you sure to clear admin for selected machine</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then

            Me.Cursor = Cursors.WaitCursor
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next
            Dim sSql As String = ""
            Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            Dim LstMachineId As String
            Dim mMachineNumber As Long
            Dim commkey As Integer
            For i = 0 To selectedRows.Length - 1
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridView1.IsGroupRow(rowHandle) Then
                    LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim

                    'sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"

                    bConn = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)

                    If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then

                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0 'PROTOCOL_TCPIP
                        'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            vnResultCode = FK_ConnectUSB(vnMachineNumber, vnLicense)
                            '    vstrTelNumber = ""
                            'vnWaitDialTime = 0
                            'vnCommPort = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Val(Trim(rstm!Location))
                            'vnCommBaudrate = 38400
                        Else
                            vpszIPAddress = Trim(lpszIPAddress)
                            vpszNetPort = CLng("5005")
                            vpszNetPassword = CLng("0")
                            vnTimeOut = CLng("5000")
                            vnProtocolType = 0
                            vnResultCode = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                            'vnResultCode = FK_ConnectNet(mMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        End If
                        If vnResultCode > 0 Then
                            bConn = True
                        Else
                            bConn = False
                        End If
                        If vnResultCode > 0 Then
                            vnResultCode = FK_EnableDevice(vnResultCode, 0)
                            'vnResultCode = FK623Attend.BenumbAllManager()
                            vnResultCode = FK_BenumbAllManager(1)
                            If vnResultCode <> RUN_SUCCESS Then
                                'MsgBox("Unable to clear Admin")
                                XtraMessageBox.Show(ulf, "<size=10>Unable to clear Admin</size>", "<size=9>Information</size>")
                            End If
                        Else
                            'MsgBox("Unable to connect Device No - " & mMachineNumber)
                            XtraMessageBox.Show(ulf, "<size=10>Unable to connect Device No - " & mMachineNumber & "</size>", "<size=9>Information</size>")
                        End If
                        'FK623Attend.Disconnect
                        vnResultCode = FK_EnableDevice(vnResultCode, 1)
                        FK_DisConnect(vnResultCode)
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                        Dim sdwEnrollNumber As Integer
                        Dim idwVerifyMode As Integer
                        Dim idwWorkcode As Integer

                        Dim bIsConnected = False
                        Dim iMachineNumber As Integer = result(i)
                        Dim idwErrorCode As Integer
                        Dim com As Common = New Common
                        Dim axCZKEM1 As New zkemkeeper.CZKEM
                        commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                        Dim sName As String = ""
                        Dim sPassword As String = ""
                        Dim iPrivilege As Integer
                        Dim bEnabled As Boolean = False
                        Dim idwFingerIndex As Integer
                        Dim sTmpData As String = ""
                        Dim iTmpLength As Integer
                        Dim iFlag As Integer
                        Dim flagenabled As String = ""
                        Dim adap As SqlDataAdapter
                        Dim adapA As OleDbDataAdapter
                        Dim dsRecord As DataSet = New DataSet
                        axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                        bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                        If bIsConnected = True Then
                            Dim lvDownload As System.Windows.Forms.ListView = New System.Windows.Forms.ListView
                            lvDownload.Items.Clear()
                            lvDownload.BeginUpdate()
                            axCZKEM1.EnableDevice(iMachineNumber, False)
                            axCZKEM1.ClearAdministrators(iMachineNumber)
                            axCZKEM1.EnableDevice(iMachineNumber, True)
                        End If
                        axCZKEM1.Disconnect()
                    End If
                End If
            Next
            For Each c As Control In Me.Controls
                c.Enabled = True
            Next
            TextPassword.Text = ""
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
        End If
    End Sub
    Private Sub SimpleButton1_Click_1(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
    Private Sub btnClearDeviceData_Click(sender As System.Object, e As System.EventArgs) Handles btnClearDeviceData.Click
        Dim bConn As Boolean
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim vEMachineNumber As Long
        Dim vEnrollNumber As Long
        Dim vFingerNumber As Long
        Dim vPrivilege As Long
        Dim vEnable As Long
        Dim vRet As Boolean
        Dim mKey As String
        Dim vnResultCode As Long
        Dim RsDevice As New DataSet 'ADODB.Recordset
        vnResultCode = 0
        Dim vnMachineNumber As Long

        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If TextPassword.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please Enter Login Password</size>", "<size=9>Error</size>")
            TextPassword.Select()
            Exit Sub
        Else
            If TextPassword.Text <> Common.PASSWORD Then
                XtraMessageBox.Show(ulf, "<size=10>Login Password Incorrect</size>", "<size=9>Error</size>")
                TextPassword.Select()
                Exit Sub
            End If
        End If
        If XtraMessageBox.Show(ulf, "<size=10>Are you sure to Clear Device Data for selected machine</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Me.Cursor = Cursors.WaitCursor
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next
            Dim sSql As String = ""
            Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            Dim LstMachineId As String
            Dim mMachineNumber As Long
            Dim commkey As Integer
            For i = 0 To selectedRows.Length - 1
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridView1.IsGroupRow(rowHandle) Then
                    LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                    bConn = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)

                    If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0 'PROTOCOL_TCPIP
                        'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            vnResultCode = FK_ConnectUSB(vnMachineNumber, vnLicense)
                        Else
                            vpszIPAddress = Trim(lpszIPAddress)
                            vpszNetPort = CLng("5005")
                            vpszNetPassword = CLng("0")
                            vnTimeOut = CLng("5000")
                            vnProtocolType = 0
                            vnResultCode = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                            'vnResultCode = FK_ConnectNet(mMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        End If
                        If vnResultCode > 0 Then
                            bConn = True
                        Else
                            bConn = False
                        End If
                        If vnResultCode > 0 Then
                            vnResultCode = FK_EnableDevice(vnResultCode, 0)
                            'vnResultCode = FK623Attend.BenumbAllManager()
                            'vnResultCode = FK_BenumbAllManager(1)
                            vnResultCode = FK_ClearKeeperData(vnResultCode)
                            If vnResultCode <> RUN_SUCCESS Then
                                'MsgBox("Unable to clear Admin")
                                XtraMessageBox.Show(ulf, "<size=10>Unable to Clear Device Data</size>", "<size=9>Information</size>")
                            End If
                        Else
                            'MsgBox("Unable to connect Device No - " & mMachineNumber)
                            XtraMessageBox.Show(ulf, "<size=10>Unable to connect Device No - " & mMachineNumber & "</size>", "<size=9>Information</size>")
                        End If
                        'FK623Attend.Disconnect
                        vnResultCode = FK_EnableDevice(vnResultCode, 1)
                        FK_DisConnect(vnResultCode)
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                        Dim sdwEnrollNumber As Integer
                        Dim idwVerifyMode As Integer
                        Dim idwWorkcode As Integer

                        Dim bIsConnected = False
                        Dim iMachineNumber As Integer = result(i)
                        Dim idwErrorCode As Integer
                        Dim com As Common = New Common
                        Dim axCZKEM1 As New zkemkeeper.CZKEM
                        commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                        Dim sName As String = ""
                        Dim sPassword As String = ""
                        Dim iPrivilege As Integer
                        Dim bEnabled As Boolean = False
                        Dim idwFingerIndex As Integer
                        Dim sTmpData As String = ""
                        Dim iTmpLength As Integer
                        Dim iFlag As Integer
                        Dim flagenabled As String = ""
                        Dim adap As SqlDataAdapter
                        Dim adapA As OleDbDataAdapter
                        Dim dsRecord As DataSet = New DataSet
                        axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                        bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                        If bIsConnected = True Then
                            Dim lvDownload As System.Windows.Forms.ListView = New System.Windows.Forms.ListView
                            lvDownload.Items.Clear()
                            lvDownload.BeginUpdate()
                            axCZKEM1.EnableDevice(iMachineNumber, False)
                            axCZKEM1.ClearKeeperData(iMachineNumber)
                            'axCZKEM1.ClearAdministrators(iMachineNumber)
                            axCZKEM1.EnableDevice(iMachineNumber, True)
                        End If
                        axCZKEM1.Disconnect()
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "HIKVISION" Then
                        Dim cn As Common = New Common()
                        Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                        Dim DeviceAdd As String = vpszIPAddress, userName As String = Trim(GridView1.GetRowCellValue(rowHandle, "HLogin").ToString.Trim), pwd As String = Trim(GridView1.GetRowCellValue(rowHandle, "HPassword").ToString.Trim)
                        Dim lUserID As Integer = -1
                        Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID)
                        If logistatus = False Then
                            XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                            Me.Cursor = Cursors.Default
                            For Each c As Control In Me.Controls
                                c.Enabled = True
                            Next
                            Exit Sub
                        Else
                            Dim struAcsParam As CHCNetSDK.NET_DVR_ACS_PARAM_TYPE = New CHCNetSDK.NET_DVR_ACS_PARAM_TYPE
                            struAcsParam.dwSize = CType(Marshal.SizeOf(struAcsParam), UInteger)
                            struAcsParam.dwParamType = 4096 ' (struAcsParam.dwParamType Or (1 + 12))
                            Dim dwSize As UInteger = CType(Marshal.SizeOf(struAcsParam), UInteger)
                            Dim ptrAcsParam As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Integer))
                            Marshal.StructureToPtr(struAcsParam, ptrAcsParam, False)
                            If Not CHCNetSDK.NET_DVR_RemoteControl(lUserID, CHCNetSDK.NET_DVR_CLEAR_ACS_PARAM, ptrAcsParam, dwSize) Then
                                Dim listItem As ListViewItem = New ListViewItem
                                listItem.Text = "FAIL"
                                Dim strTemp As String = String.Format("NET_DVR_CLEAR_ACS_PARAM FAIL, ERROR CODE {0}", CHCNetSDK.NET_DVR_GetLastError)
                                listItem.SubItems.Add(strTemp)
                                'Me.AddList(listViewMessage, listItem, True)
                                XtraMessageBox.Show(ulf, "<size=10>Failed To Clear Device Data</size>", "Failed")
                            Else
                                Dim listItem As ListViewItem = New ListViewItem
                                listItem.Text = "SUCC"
                                listItem.SubItems.Add("NET_DVR_CLEAR_ACS_PARAM SUCC")
                                'Me.AddList(listViewMessage, listItem, True)
                                XtraMessageBox.Show(ulf, "<size=10>Task Completeda</size>", "Success")
                            End If

                            Marshal.FreeHGlobal(ptrAcsParam)
                        End If
                    End If
                End If
            Next
            For Each c As Control In Me.Controls
                c.Enabled = True
            Next
            TextPassword.Text = ""
            Me.Cursor = Cursors.Default

        End If
    End Sub
    Private Sub btnLockOpen_Click(sender As System.Object, e As System.EventArgs) Handles btnLockOpen.Click
        Dim bConn As Boolean
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim vEMachineNumber As Long
        Dim vEnrollNumber As Long
        Dim vFingerNumber As Long
        Dim vPrivilege As Long
        Dim vEnable As Long
        Dim vRet As Boolean
        Dim mKey As String
        Dim vnResultCode As Long
        Dim RsDevice As New DataSet 'ADODB.Recordset
        vnResultCode = 0
        Dim vnMachineNumber As Long

        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If TextPassword.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please Enter Login Password</size>", "<size=9>Error</size>")
            TextPassword.Select()
            Exit Sub
        Else
            If TextPassword.Text <> Common.PASSWORD Then
                XtraMessageBox.Show(ulf, "<size=10>Login Password Incorrect</size>", "<size=9>Error</size>")
                TextPassword.Select()
                Exit Sub
            End If
        End If
        If XtraMessageBox.Show(ulf, "<size=10>Are you sure to Open Door Lock for selected machine</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Me.Cursor = Cursors.WaitCursor
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next
            Dim sSql As String = ""
            Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            Dim LstMachineId As String
            Dim mMachineNumber As Long
            Dim commkey As Integer
            For i = 0 To selectedRows.Length - 1
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridView1.IsGroupRow(rowHandle) Then
                    LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                    bConn = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)

                    If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                        'vpszNetPort = CLng("5005")
                        'vpszNetPassword = CLng("0")
                        'vnTimeOut = CLng("5000")
                        'vnProtocolType = 0 'PROTOCOL_TCPIP
                        ''bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        'If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                        '    vnResultCode = FK_ConnectUSB(vnMachineNumber, vnLicense)
                        'Else
                        '    vpszIPAddress = Trim(lpszIPAddress)
                        '    vpszNetPort = CLng("5005")
                        '    vpszNetPassword = CLng("0")
                        '    vnTimeOut = CLng("5000")
                        '    vnProtocolType = 0
                        '    vnResultCode = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        '    'vnResultCode = FK_ConnectNet(mMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        'End If
                        'If vnResultCode > 0 Then
                        '    bConn = True
                        'Else
                        '    bConn = False
                        'End If
                        'If vnResultCode > 0 Then
                        '    vnResultCode = FK_EnableDevice(vnResultCode, 0)
                        '    'vnResultCode = FK623Attend.BenumbAllManager()
                        '    'vnResultCode = FK_BenumbAllManager(1)
                        '    vnResultCode = FK_ClearKeeperData(vnResultCode)
                        '    If vnResultCode <> RUN_SUCCESS Then
                        '        'MsgBox("Unable to clear Admin")
                        '        XtraMessageBox.Show(ulf, "<size=10>Unable to Clear Device Data</size>", "<size=9>Information</size>")
                        '    End If
                        'Else
                        '    'MsgBox("Unable to connect Device No - " & mMachineNumber)
                        '    XtraMessageBox.Show(ulf, "<size=10>Unable to connect Device No - " & mMachineNumber & "</size>", "<size=9>Information</size>")
                        'End If
                        ''FK623Attend.Disconnect
                        'vnResultCode = FK_EnableDevice(vnResultCode, 1)
                        'FK_DisConnect(vnResultCode)
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                        Dim sdwEnrollNumber As Integer
                        Dim idwVerifyMode As Integer
                        Dim idwWorkcode As Integer

                        Dim bIsConnected = False
                        Dim iMachineNumber As Integer = result(i)
                        Dim idwErrorCode As Integer
                        Dim com As Common = New Common
                        Dim axCZKEM1 As New zkemkeeper.CZKEM
                        commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                        Dim sName As String = ""
                        Dim sPassword As String = ""
                        Dim iPrivilege As Integer
                        Dim bEnabled As Boolean = False
                        Dim idwFingerIndex As Integer
                        Dim sTmpData As String = ""
                        Dim iTmpLength As Integer
                        Dim iFlag As Integer
                        Dim flagenabled As String = ""
                        Dim adap As SqlDataAdapter
                        Dim adapA As OleDbDataAdapter
                        Dim dsRecord As DataSet = New DataSet
                        axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                        bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                        If bIsConnected = True Then
                            Dim lvDownload As System.Windows.Forms.ListView = New System.Windows.Forms.ListView
                            lvDownload.Items.Clear()
                            lvDownload.BeginUpdate()
                            axCZKEM1.EnableDevice(iMachineNumber, False)
                            axCZKEM1.ACUnlock(iMachineNumber, 10)
                            'axCZKEM1.ClearAdministrators(iMachineNumber)
                            axCZKEM1.EnableDevice(iMachineNumber, True)
                        End If
                        axCZKEM1.Disconnect()
                    End If
                End If
            Next
            For Each c As Control In Me.Controls
                c.Enabled = True
            Next
            TextPassword.Text = ""
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
        End If
    End Sub

End Class