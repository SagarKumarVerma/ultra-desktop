﻿Imports DevExpress.LookAndFeel

Public Class XtraFullPayrollMenuMaster
    Dim ulf As UserLookAndFeel
    Public Shared PayEmpMster As String

    Public Sub New()
        InitializeComponent()
    End Sub
    Private Sub XtraFullPayrollMenuMaster_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True

        SidePanelTitle.Visible = True
        PayEmpMster = "E"
        LabelTitle.Text = " Employee Setup"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraEmployeePayroll
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
    Private Sub BarButtonItemEmpSetup_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemEmpSetup.ItemClick
        SidePanelTitle.Visible = True
        PayEmpMster = "E"
        LabelTitle.Text = " Employee Setup"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraEmployeePayroll
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
    Private Sub BarButtonItemPaySetup_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemPaySetup.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Payroll Setup"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraPayrollSetup
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
    Private Sub BarButtonItemFormulaSetup_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemFormulaSetup.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Formula Setup"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraFormulaSetup
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
    Private Sub BarButtonItemPayProcess_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemPayProcess.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Payroll Processing"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraPayrollProcess
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
    Private Sub BarButtonItemMaint_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemMaint.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Performance Maintain"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraPayMaintenance
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
    Private Sub BarButtonItemLoan_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemLoan.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Loan/Advance Setup"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraLoanAdvance
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
    Private Sub BarSubItemReports_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarSubItemReports.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Reports"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraReportsPay
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
    Private Sub BarButtonItemArrer_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemArrer.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Arrear Entry"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraPayArrear
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
    Private Sub BarButtonItemInc_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemInc.ItemClick
        SidePanelTitle.Visible = True
        PayEmpMster = "I"
        LabelTitle.Text = " Increment Master"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraEmployeePayroll
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemIncTax_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemIncTax.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Income Tax Slab"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraIncomTaxSlab
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemProTax_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemProTax.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Professional Tax Slab"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraProfessionalTaxSlab
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemLeaveEncash_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemLeaveEncash.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Leave Encashment"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraLeaveEncashment
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemGratuity_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemGratuity.ItemClick
        XtraGratuity.ShowDialog()
    End Sub

    Private Sub BarButtonItemReImb_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemReImb.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Reimbursement"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraReimburse
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarSubItemReport_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarSubItemReport.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Pay Reports"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraReportsPay
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemPieceM_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemPieceM.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Piece Master"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraPieceMaster
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemPieceEntry_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemPieceEntry.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Piece Entry"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraPieceEntry
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemFFE_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemFFE.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Full and Final"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraPayFullNFinal
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
End Class
