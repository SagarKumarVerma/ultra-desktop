﻿
Public Class CommonZK
    Public axCZKEM1 As New zkemkeeper.CZKEM

    Public Function connectZK(ByVal IP As String, ByVal port As Integer, ByVal axCZKEM1 As zkemkeeper.CZKEM) As Boolean
        Dim bIsConnected = False
        Dim iMachineNumber As Integer
        Dim idwErrorCode As Integer

        bIsConnected = axCZKEM1.Connect_Net(IP, port)
        If bIsConnected = True Then
            iMachineNumber = 1 'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
            axCZKEM1.RegEvent(iMachineNumber, 65535) 'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
        Else
            axCZKEM1.GetLastError(idwErrorCode)
            'MsgBox("Unable to connect the device,ErrorCode=" & idwErrorCode, MsgBoxStyle.Exclamation, "Error")
        End If
        Return bIsConnected
    End Function
    Public Function DisconnectZK(ByVal axCZKEM1 As zkemkeeper.CZKEM)
        axCZKEM1.Disconnect()
    End Function
End Class