﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraPayrollProcess
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraPayrollProcess))
        Me.SSSDBDataSet = New ULtra.SSSDBDataSet()
        Me.TblCompanyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblCompanyTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblCompanyTableAdapter()
        Me.TblCompany1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblCompany1TableAdapter()
        Me.TblDepartmentBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblDepartmentTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter()
        Me.TblDepartment1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter()
        Me.TblbranchBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblbranchTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblbranchTableAdapter()
        Me.Tblbranch1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblbranch1TableAdapter()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblEmployeeTableAdapter = New ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblEmployee1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.PopupContainerControlEmp = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlEmp = New DevExpress.XtraGrid.GridControl()
        Me.GridViewEmp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPAYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PopupContainerControlCompany = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlComp = New DevExpress.XtraGrid.GridControl()
        Me.GridViewComp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit4 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.ComboNepaliYearTo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.DateEditTo = New DevExpress.XtraEditors.DateEdit()
        Me.ComboNEpaliMonthTo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboNepaliDateTo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.DateEditFrom = New DevExpress.XtraEditors.DateEdit()
        Me.ComboNepaliYearFrm = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboNEpaliMonthFrm = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.PopupContainerEdit1 = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.ComboNepaliDateFrm = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit1 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.CheckEdit5 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit6 = New DevExpress.XtraEditors.CheckEdit()
        Me.PopupContainerControlLocation = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlLocation = New DevExpress.XtraGrid.GridControl()
        Me.GridViewLocation = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBRANCHCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRANCHNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlDept = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlDept = New DevExpress.XtraGrid.GridControl()
        Me.GridViewDept = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colDEPARTMENTCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEPARTMENTNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.SimpleButtonCapture = New DevExpress.XtraEditors.SimpleButton()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlEmp.SuspendLayout()
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlCompany, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlCompany.SuspendLayout()
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.ComboNepaliYearTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonthTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliDateTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliYearFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonthFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliDateFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlLocation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlLocation.SuspendLayout()
        CType(Me.GridControlLocation, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewLocation, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlDept.SuspendLayout()
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblCompanyBindingSource
        '
        Me.TblCompanyBindingSource.DataMember = "tblCompany"
        Me.TblCompanyBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblCompanyTableAdapter
        '
        Me.TblCompanyTableAdapter.ClearBeforeFill = True
        '
        'TblCompany1TableAdapter1
        '
        Me.TblCompany1TableAdapter1.ClearBeforeFill = True
        '
        'TblDepartmentBindingSource
        '
        Me.TblDepartmentBindingSource.DataMember = "tblDepartment"
        Me.TblDepartmentBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblDepartmentTableAdapter
        '
        Me.TblDepartmentTableAdapter.ClearBeforeFill = True
        '
        'TblDepartment1TableAdapter1
        '
        Me.TblDepartment1TableAdapter1.ClearBeforeFill = True
        '
        'TblbranchBindingSource
        '
        Me.TblbranchBindingSource.DataMember = "tblbranch"
        Me.TblbranchBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblbranchTableAdapter
        '
        Me.TblbranchTableAdapter.ClearBeforeFill = True
        '
        'Tblbranch1TableAdapter1
        '
        Me.Tblbranch1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlEmp)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlCompany)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl4)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlLocation)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlDept)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl2)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1250, 568)
        Me.SplitContainerControl1.SplitterPosition = 1165
        Me.SplitContainerControl1.TabIndex = 1
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'PopupContainerControlEmp
        '
        Me.PopupContainerControlEmp.Controls.Add(Me.GridControlEmp)
        Me.PopupContainerControlEmp.Location = New System.Drawing.Point(921, 507)
        Me.PopupContainerControlEmp.Name = "PopupContainerControlEmp"
        Me.PopupContainerControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlEmp.TabIndex = 31
        '
        'GridControlEmp
        '
        Me.GridControlEmp.DataSource = Me.TblEmployeeBindingSource
        Me.GridControlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlEmp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlEmp.MainView = Me.GridViewEmp
        Me.GridControlEmp.Name = "GridControlEmp"
        Me.GridControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlEmp.TabIndex = 6
        Me.GridControlEmp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewEmp})
        '
        'GridViewEmp
        '
        Me.GridViewEmp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPAYCODE, Me.colEMPNAME})
        Me.GridViewEmp.GridControl = Me.GridControlEmp
        Me.GridViewEmp.Name = "GridViewEmp"
        Me.GridViewEmp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.Editable = False
        Me.GridViewEmp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewEmp.OptionsSelection.MultiSelect = True
        Me.GridViewEmp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        '
        'colPAYCODE
        '
        Me.colPAYCODE.Caption = "Paycode"
        Me.colPAYCODE.FieldName = "PAYCODE"
        Me.colPAYCODE.Name = "colPAYCODE"
        Me.colPAYCODE.Visible = True
        Me.colPAYCODE.VisibleIndex = 1
        '
        'colEMPNAME
        '
        Me.colEMPNAME.Caption = "Employee Name"
        Me.colEMPNAME.FieldName = "EMPNAME"
        Me.colEMPNAME.Name = "colEMPNAME"
        Me.colEMPNAME.Visible = True
        Me.colEMPNAME.VisibleIndex = 2
        '
        'PopupContainerControlCompany
        '
        Me.PopupContainerControlCompany.Controls.Add(Me.GridControlComp)
        Me.PopupContainerControlCompany.Location = New System.Drawing.Point(615, 510)
        Me.PopupContainerControlCompany.Name = "PopupContainerControlCompany"
        Me.PopupContainerControlCompany.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlCompany.TabIndex = 30
        '
        'GridControlComp
        '
        Me.GridControlComp.DataSource = Me.TblCompanyBindingSource
        Me.GridControlComp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlComp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlComp.MainView = Me.GridViewComp
        Me.GridControlComp.Name = "GridControlComp"
        Me.GridControlComp.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit4})
        Me.GridControlComp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlComp.TabIndex = 6
        Me.GridControlComp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewComp})
        '
        'GridViewComp
        '
        Me.GridViewComp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridViewComp.GridControl = Me.GridControlComp
        Me.GridViewComp.Name = "GridViewComp"
        Me.GridViewComp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.Editable = False
        Me.GridViewComp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewComp.OptionsSelection.MultiSelect = True
        Me.GridViewComp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Company Code"
        Me.GridColumn1.FieldName = "COMPANYCODE"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 1
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Company Name"
        Me.GridColumn2.FieldName = "COMPANYNAME"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 2
        '
        'RepositoryItemTimeEdit4
        '
        Me.RepositoryItemTimeEdit4.AutoHeight = False
        Me.RepositoryItemTimeEdit4.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit4.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit4.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit4.Name = "RepositoryItemTimeEdit4"
        '
        'GroupControl4
        '
        Me.GroupControl4.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl4.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl4.AppearanceCaption.Options.UseFont = True
        Me.GroupControl4.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl4.Controls.Add(Me.ComboNepaliYearTo)
        Me.GroupControl4.Controls.Add(Me.DateEditTo)
        Me.GroupControl4.Controls.Add(Me.ComboNEpaliMonthTo)
        Me.GroupControl4.Controls.Add(Me.LabelControl1)
        Me.GroupControl4.Controls.Add(Me.ComboNepaliDateTo)
        Me.GroupControl4.Controls.Add(Me.DateEditFrom)
        Me.GroupControl4.Controls.Add(Me.ComboNepaliYearFrm)
        Me.GroupControl4.Controls.Add(Me.LabelControl7)
        Me.GroupControl4.Controls.Add(Me.ComboNEpaliMonthFrm)
        Me.GroupControl4.Controls.Add(Me.PopupContainerEdit1)
        Me.GroupControl4.Controls.Add(Me.ComboNepaliDateFrm)
        Me.GroupControl4.Controls.Add(Me.LabelControl6)
        Me.GroupControl4.Controls.Add(Me.LabelControl5)
        Me.GroupControl4.Controls.Add(Me.ComboBoxEdit1)
        Me.GroupControl4.Controls.Add(Me.CheckEdit5)
        Me.GroupControl4.Controls.Add(Me.CheckEdit6)
        Me.GroupControl4.Location = New System.Drawing.Point(21, 15)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(880, 139)
        Me.GroupControl4.TabIndex = 29
        Me.GroupControl4.Text = "Selection Criteria"
        '
        'ComboNepaliYearTo
        '
        Me.ComboNepaliYearTo.Location = New System.Drawing.Point(536, 40)
        Me.ComboNepaliYearTo.Name = "ComboNepaliYearTo"
        Me.ComboNepaliYearTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboNepaliYearTo.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYearTo.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearTo.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYearTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYearTo.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYearTo.Size = New System.Drawing.Size(61, 24)
        Me.ComboNepaliYearTo.TabIndex = 37
        '
        'DateEditTo
        '
        Me.DateEditTo.EditValue = New Date(2018, 1, 1, 16, 13, 14, 0)
        Me.DateEditTo.Location = New System.Drawing.Point(402, 40)
        Me.DateEditTo.Name = "DateEditTo"
        Me.DateEditTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.DateEditTo.Properties.Appearance.Options.UseFont = True
        Me.DateEditTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditTo.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.DateEditTo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditTo.Size = New System.Drawing.Size(135, 24)
        Me.DateEditTo.TabIndex = 29
        '
        'ComboNEpaliMonthTo
        '
        Me.ComboNEpaliMonthTo.Location = New System.Drawing.Point(450, 40)
        Me.ComboNEpaliMonthTo.Name = "ComboNEpaliMonthTo"
        Me.ComboNEpaliMonthTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboNEpaliMonthTo.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonthTo.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthTo.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonthTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonthTo.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonthTo.Size = New System.Drawing.Size(80, 24)
        Me.ComboNEpaliMonthTo.TabIndex = 36
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(329, 42)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(54, 18)
        Me.LabelControl1.TabIndex = 28
        Me.LabelControl1.Text = "To Date"
        '
        'ComboNepaliDateTo
        '
        Me.ComboNepaliDateTo.Location = New System.Drawing.Point(402, 40)
        Me.ComboNepaliDateTo.Name = "ComboNepaliDateTo"
        Me.ComboNepaliDateTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboNepaliDateTo.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliDateTo.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateTo.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliDateTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliDateTo.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNepaliDateTo.Size = New System.Drawing.Size(42, 24)
        Me.ComboNepaliDateTo.TabIndex = 35
        '
        'DateEditFrom
        '
        Me.DateEditFrom.EditValue = New Date(2018, 1, 1, 16, 13, 14, 0)
        Me.DateEditFrom.Location = New System.Drawing.Point(112, 39)
        Me.DateEditFrom.Name = "DateEditFrom"
        Me.DateEditFrom.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.DateEditFrom.Properties.Appearance.Options.UseFont = True
        Me.DateEditFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditFrom.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.DateEditFrom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditFrom.Size = New System.Drawing.Size(135, 24)
        Me.DateEditFrom.TabIndex = 27
        '
        'ComboNepaliYearFrm
        '
        Me.ComboNepaliYearFrm.Location = New System.Drawing.Point(246, 39)
        Me.ComboNepaliYearFrm.Name = "ComboNepaliYearFrm"
        Me.ComboNepaliYearFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboNepaliYearFrm.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYearFrm.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearFrm.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYearFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYearFrm.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYearFrm.Size = New System.Drawing.Size(61, 24)
        Me.ComboNepaliYearFrm.TabIndex = 34
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(15, 87)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(77, 18)
        Me.LabelControl7.TabIndex = 26
        Me.LabelControl7.Text = "Select Type"
        '
        'ComboNEpaliMonthFrm
        '
        Me.ComboNEpaliMonthFrm.Location = New System.Drawing.Point(160, 39)
        Me.ComboNEpaliMonthFrm.Name = "ComboNEpaliMonthFrm"
        Me.ComboNEpaliMonthFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboNEpaliMonthFrm.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonthFrm.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthFrm.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonthFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonthFrm.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonthFrm.Size = New System.Drawing.Size(80, 24)
        Me.ComboNEpaliMonthFrm.TabIndex = 33
        '
        'PopupContainerEdit1
        '
        Me.PopupContainerEdit1.Location = New System.Drawing.Point(455, 84)
        Me.PopupContainerEdit1.Name = "PopupContainerEdit1"
        Me.PopupContainerEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.PopupContainerEdit1.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEdit1.Size = New System.Drawing.Size(165, 24)
        Me.PopupContainerEdit1.TabIndex = 25
        '
        'ComboNepaliDateFrm
        '
        Me.ComboNepaliDateFrm.Location = New System.Drawing.Point(112, 39)
        Me.ComboNepaliDateFrm.Name = "ComboNepaliDateFrm"
        Me.ComboNepaliDateFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboNepaliDateFrm.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliDateFrm.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateFrm.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliDateFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliDateFrm.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNepaliDateFrm.Size = New System.Drawing.Size(42, 24)
        Me.ComboNepaliDateFrm.TabIndex = 32
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(15, 42)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(70, 18)
        Me.LabelControl6.TabIndex = 16
        Me.LabelControl6.Text = "From Date"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(316, 87)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(98, 18)
        Me.LabelControl5.TabIndex = 21
        Me.LabelControl5.Text = "Select Paycode"
        '
        'ComboBoxEdit1
        '
        Me.ComboBoxEdit1.EditValue = "Paycode"
        Me.ComboBoxEdit1.Location = New System.Drawing.Point(112, 84)
        Me.ComboBoxEdit1.Name = "ComboBoxEdit1"
        Me.ComboBoxEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboBoxEdit1.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboBoxEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEdit1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboBoxEdit1.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit1.Properties.Items.AddRange(New Object() {"Paycode", "Company", "Location", "Department"})
        Me.ComboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit1.Size = New System.Drawing.Size(135, 24)
        Me.ComboBoxEdit1.TabIndex = 24
        '
        'CheckEdit5
        '
        Me.CheckEdit5.EditValue = True
        Me.CheckEdit5.Location = New System.Drawing.Point(644, 42)
        Me.CheckEdit5.Name = "CheckEdit5"
        Me.CheckEdit5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.CheckEdit5.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit5.Properties.Caption = "All"
        Me.CheckEdit5.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit5.Properties.RadioGroupIndex = 2
        Me.CheckEdit5.Size = New System.Drawing.Size(53, 22)
        Me.CheckEdit5.TabIndex = 17
        '
        'CheckEdit6
        '
        Me.CheckEdit6.Location = New System.Drawing.Point(718, 44)
        Me.CheckEdit6.Name = "CheckEdit6"
        Me.CheckEdit6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.CheckEdit6.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit6.Properties.Caption = "Selective"
        Me.CheckEdit6.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit6.Properties.RadioGroupIndex = 2
        Me.CheckEdit6.Size = New System.Drawing.Size(99, 22)
        Me.CheckEdit6.TabIndex = 18
        Me.CheckEdit6.TabStop = False
        '
        'PopupContainerControlLocation
        '
        Me.PopupContainerControlLocation.Controls.Add(Me.GridControlLocation)
        Me.PopupContainerControlLocation.Location = New System.Drawing.Point(309, 507)
        Me.PopupContainerControlLocation.Name = "PopupContainerControlLocation"
        Me.PopupContainerControlLocation.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlLocation.TabIndex = 28
        '
        'GridControlLocation
        '
        Me.GridControlLocation.DataSource = Me.TblbranchBindingSource
        Me.GridControlLocation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlLocation.Location = New System.Drawing.Point(0, 0)
        Me.GridControlLocation.MainView = Me.GridViewLocation
        Me.GridControlLocation.Name = "GridControlLocation"
        Me.GridControlLocation.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit3})
        Me.GridControlLocation.Size = New System.Drawing.Size(300, 300)
        Me.GridControlLocation.TabIndex = 6
        Me.GridControlLocation.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewLocation})
        '
        'GridViewLocation
        '
        Me.GridViewLocation.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBRANCHCODE, Me.colBRANCHNAME})
        Me.GridViewLocation.GridControl = Me.GridControlLocation
        Me.GridViewLocation.Name = "GridViewLocation"
        Me.GridViewLocation.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewLocation.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewLocation.OptionsBehavior.Editable = False
        Me.GridViewLocation.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewLocation.OptionsSelection.MultiSelect = True
        Me.GridViewLocation.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        '
        'colBRANCHCODE
        '
        Me.colBRANCHCODE.Caption = "Location Code"
        Me.colBRANCHCODE.FieldName = "BRANCHCODE"
        Me.colBRANCHCODE.Name = "colBRANCHCODE"
        Me.colBRANCHCODE.Visible = True
        Me.colBRANCHCODE.VisibleIndex = 1
        '
        'colBRANCHNAME
        '
        Me.colBRANCHNAME.Caption = "Location Name"
        Me.colBRANCHNAME.FieldName = "BRANCHNAME"
        Me.colBRANCHNAME.Name = "colBRANCHNAME"
        Me.colBRANCHNAME.Visible = True
        Me.colBRANCHNAME.VisibleIndex = 2
        '
        'RepositoryItemTimeEdit3
        '
        Me.RepositoryItemTimeEdit3.AutoHeight = False
        Me.RepositoryItemTimeEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit3.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit3.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit3.Name = "RepositoryItemTimeEdit3"
        '
        'PopupContainerControlDept
        '
        Me.PopupContainerControlDept.Controls.Add(Me.GridControlDept)
        Me.PopupContainerControlDept.Location = New System.Drawing.Point(3, 510)
        Me.PopupContainerControlDept.Name = "PopupContainerControlDept"
        Me.PopupContainerControlDept.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlDept.TabIndex = 27
        '
        'GridControlDept
        '
        Me.GridControlDept.DataSource = Me.TblDepartmentBindingSource
        Me.GridControlDept.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlDept.Location = New System.Drawing.Point(0, 0)
        Me.GridControlDept.MainView = Me.GridViewDept
        Me.GridControlDept.Name = "GridControlDept"
        Me.GridControlDept.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit2})
        Me.GridControlDept.Size = New System.Drawing.Size(300, 300)
        Me.GridControlDept.TabIndex = 6
        Me.GridControlDept.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewDept})
        '
        'GridViewDept
        '
        Me.GridViewDept.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colDEPARTMENTCODE, Me.colDEPARTMENTNAME})
        Me.GridViewDept.GridControl = Me.GridControlDept
        Me.GridViewDept.Name = "GridViewDept"
        Me.GridViewDept.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.Editable = False
        Me.GridViewDept.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewDept.OptionsSelection.MultiSelect = True
        Me.GridViewDept.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        '
        'colDEPARTMENTCODE
        '
        Me.colDEPARTMENTCODE.Caption = "Department Code"
        Me.colDEPARTMENTCODE.FieldName = "DEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Name = "colDEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Visible = True
        Me.colDEPARTMENTCODE.VisibleIndex = 1
        '
        'colDEPARTMENTNAME
        '
        Me.colDEPARTMENTNAME.Caption = "Department Name"
        Me.colDEPARTMENTNAME.FieldName = "DEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Name = "colDEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Visible = True
        Me.colDEPARTMENTNAME.VisibleIndex = 2
        '
        'RepositoryItemTimeEdit2
        '
        Me.RepositoryItemTimeEdit2.AutoHeight = False
        Me.RepositoryItemTimeEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit2.Name = "RepositoryItemTimeEdit2"
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl2.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl2.Controls.Add(Me.SimpleButtonCapture)
        Me.GroupControl2.Location = New System.Drawing.Point(21, 160)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(335, 100)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = " Processing Attendance Data"
        '
        'SimpleButtonCapture
        '
        Me.SimpleButtonCapture.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.SimpleButtonCapture.Appearance.Options.UseFont = True
        Me.SimpleButtonCapture.Location = New System.Drawing.Point(26, 43)
        Me.SimpleButtonCapture.Name = "SimpleButtonCapture"
        Me.SimpleButtonCapture.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonCapture.TabIndex = 21
        Me.SimpleButtonCapture.Text = "Capture"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(77, 568)
        Me.MemoEdit1.TabIndex = 4
        '
        'XtraPayrollProcess
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraPayrollProcess"
        Me.Size = New System.Drawing.Size(1250, 568)
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlEmp.ResumeLayout(False)
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlCompany, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlCompany.ResumeLayout(False)
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.ComboNepaliYearTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonthTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliDateTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliYearFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonthFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliDateFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlLocation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlLocation.ResumeLayout(False)
        CType(Me.GridControlLocation, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewLocation, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlDept.ResumeLayout(False)
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SSSDBDataSet As ULtra.SSSDBDataSet
    Friend WithEvents TblCompanyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblCompanyTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblCompanyTableAdapter
    Friend WithEvents TblCompany1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblCompany1TableAdapter
    Friend WithEvents TblDepartmentBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblDepartmentTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter
    Friend WithEvents TblDepartment1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter
    Friend WithEvents TblbranchBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblbranchTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblbranchTableAdapter
    Friend WithEvents Tblbranch1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblbranch1TableAdapter
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblEmployeeTableAdapter As ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents TblEmployee1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents PopupContainerControlEmp As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlEmp As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewEmp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colPAYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents PopupContainerControlCompany As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlComp As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewComp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit4 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEdit1 As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit1 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents CheckEdit5 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit6 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PopupContainerControlLocation As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlLocation As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewLocation As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colBRANCHCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRANCHNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlDept As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlDept As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewDept As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colDEPARTMENTCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEPARTMENTNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SimpleButtonCapture As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents DateEditTo As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEditFrom As DevExpress.XtraEditors.DateEdit
    Friend WithEvents ComboNepaliYearTo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonthTo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliDateTo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliYearFrm As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonthFrm As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliDateFrm As DevExpress.XtraEditors.ComboBoxEdit

End Class
