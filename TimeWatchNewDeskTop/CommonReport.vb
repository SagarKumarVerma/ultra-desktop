﻿Imports System.Data.OleDb
Imports System.Data.SqlClient

Public Class CommonReport
    Dim mCondition As String
    Dim mChkActive As Boolean
    'Dim g_WhereClause As String
    Public Shared g_CompanyNames As String

    Public Sub Monthly_Performance_ow(strsortorder As String, startdate As DateTime, enddate As DateTime, g_WhereClause As String)
        '  On Error GoTo ErrorGen
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String
        Dim strEmpName As String
        Dim strPresentCardNo As String
        Dim strLineForDays As String
        Dim strIn1 As String
        Dim strOut1 As String
        Dim strIn2 As String
        Dim strOut2 As String
        Dim strHoursWorked As String
        Dim strOtDuration As String
        Dim strOutwork As String
        Dim strStatus As String, strshift As String, strLate As String, strDays As String
        Dim dtmStartDate As Date
        Dim dblPresentValue As Double
        Dim dblAbsentValue As Double
        Dim dblLeaveValue As Double
        Dim dblHoliday_Value As Double
        Dim dblWo_Value As Double
        Dim dblHoursWorked As Double
        Dim dblOtDuration As Double
        Dim dblOtAmount As Double
        Dim intFirstTime As Integer
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim intGroupLine As Integer, tempfromdt As DateTime, temptodt As DateTime

        Dim mReportPrintStatus As Boolean
        Dim Rs_Report As DataSet 'ADODB.Recordset
        Dim Rs_Lvdata As DataSet 'ADODB.Recordset
        Dim Rs_Lvdesc As DataSet 'ADODB.Recordset
        Dim mvntFile_Report As Object
        Dim mvntFile_Name As Object
        Dim mstrDepartmentCode As String
        Dim mstrFile_Name As String
        Dim mblnOption_Click As Boolean
        Dim mintPageNo As Integer
        Dim mShift As String
        Dim mintLine As Integer
        Dim mFileNumber As Integer
        Dim mblnCheckReport As Boolean
        Dim mlngStartingNoOfDays As Long

        Dim mCode As String


        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Rs_Report = New DataSet

        'If Len(Trim(g_CompanyNames)) = 0 Then
        '    GetCompanies()
        'End If

        intFirstTime = 1
        mblnCheckReport = False
        mFirst = True
        mintPageNo = 1
        mintLine = 1
        intGroupLine = 0

        If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True

        If UCase(strsortorder) = "" Then
            'If g_Report_view Then
            If Common.servername = "Access" Then
                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregister.Shift, tblEmployee.Divisioncode,TBLgrade.GRADENAME, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   TblTimeRegister.DateOffice, tblTimeRegister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName," & _
                " tbltimeregister.OUTWORKDURATION,tblTimeRegister.In1,tblTimeRegister.Out1,TBLTIMEREGISTER.IN1MANNUAL,TBLTIMEREGISTER.IN2MANNUAL,TBLTIMEREGISTER.OUT1MANNUAL,TBLTIMEREGISTER.OUT2MANNUAL,tblTimeRegister.In2,tblTimeRegister.Out2,tblTimeRegister.HoursWorked,tblTimeRegister.Status,tblTimeRegister.LATEARRIVAL,tblTimeRegister.SHIFTATTENDED," & _
                " tblTimeRegister.OtDuration,tblTimeRegister.OtAmount," & _
                " tblTimeRegister.PresentValue,tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Holiday_Value,tblTimeRegister.Wo_Value" & _
                " from tblCatagory,tblDivision, tblgrade, tblTimeRegister,tblEmployee,tblCompany ,tblDepartment" & _
                " Where tblgrade.gradecode=tblemployee.gradecode and tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.DateOffice Between #" & startdate.ToString("MMM dd yyyy") & "# AND #" & enddate.ToString("MMM dd yyyy") & "#   AND (tblTimeRegister.PayCode = tblEmployee.PayCode)" & _
                " And tblEmployee.CompanyCode = tblcompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & _
                " And  tblemployee.active='Y' " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
            Else
                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregister.Shift, tblEmployee.Divisioncode,TBLgrade.GRADENAME, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   TblTimeRegister.DateOffice, tblTimeRegister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName," & _
                    " tbltimeregister.OUTWORKDURATION,tblTimeRegister.In1,tblTimeRegister.Out1,TBLTIMEREGISTER.IN1MANNUAL,TBLTIMEREGISTER.IN2MANNUAL,TBLTIMEREGISTER.OUT1MANNUAL,TBLTIMEREGISTER.OUT2MANNUAL,tblTimeRegister.In2,tblTimeRegister.Out2,tblTimeRegister.HoursWorked,tblTimeRegister.Status,tblTimeRegister.LATEARRIVAL,tblTimeRegister.SHIFTATTENDED," & _
                    " tblTimeRegister.OtDuration,tblTimeRegister.OtAmount," & _
                    " tblTimeRegister.PresentValue,tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Holiday_Value,tblTimeRegister.Wo_Value" & _
                    " from tblCatagory,tblDivision, tblgrade, tblTimeRegister,tblEmployee,tblCompany ,tblDepartment" & _
                    " Where tblgrade.gradecode=tblemployee.gradecode and tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.DateOffice Between '" & startdate.ToString("MMM dd yyyy") & "' AND '" & enddate.ToString("MMM dd yyyy") & "' AND (tblTimeRegister.PayCode = tblEmployee.PayCode)" & _
                    " And tblEmployee.CompanyCode = tblcompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & _
                    " And  tblemployee.active='Y' " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
            End If
            'Else
            '    strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblTimeRegister.DateOffice, tblTimeRegister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName," & _
            '        " tblTimeRegister.In1,tblTimeRegister.Out1,tblTimeRegister.In2,tblTimeRegister.Out2,tblTimeRegister.HoursWorked,tblTimeRegister.Status,tblTimeRegister.LATEARRIVAL,tblTimeRegister.SHIFTATTENDED," & _
            '        " tblTimeRegister.OtDuration,tblTimeRegister.OtAmount," & _
            '        " tblTimeRegister.PresentValue,tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Holiday_Value,tblTimeRegister.Wo_Value" & _
            '        " from tblCatagory,tblDivision,  tblTimeRegister,tblEmployee,tblCompany ,tblDepartment" & _
            '        " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.DateOffice Between '" & startdate.ToString("MMM dd yyyy") & "' AND '" & enddate.ToString("MMM dd yyyy") & "' AND (tblTimeRegister.PayCode = tblEmployee.PayCode)" & _
            '        " And tblEmployee.CompanyCode = tblcompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & _
            '        " And (tblemployee.LeavingDate>='" & startdate.ToString("MMM dd yyyy") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
            'End If
            'Rs_Report = Cn.Execute(strsql)

            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
            'Rs_Report.Sort = "PayCode, DateOffice"
            If Rs_Report.Tables(0).Rows.Count < 1 Then
                'G_Report_Error = False
                'Screen.MousePointer = vbDefault
                MsgBox("No Data Available For this Report.", vbInformation + vbExclamation, "ULtra")
                mReportPrintStatus = False
                Exit Sub
            End If
        Else
            'Rs_Report.Sort = strsortorder & ", DateOffice"
        End If

        intFile = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)
        'objWriter.WriteLine(x)
        'Open mstrFile_Name For Output As #intFile
        dtmStartDate = startdate.ToString("yyyy-MM-dd")
        strLineForDays = Space(11)
        Do While dtmStartDate <= enddate
            strLineForDays = strLineForDays & Format(dtmStartDate, "dd") & Space(5)
            strDays = strDays + Format(dtmStartDate, "ddd") & Space(4)
            dtmStartDate = dtmStartDate.AddDays(1)
        Loop
        Dim g_SkipAfterDept As Boolean = True   'need to take it from tblsetup
        Dim g_LinesPerPage As Integer = 58

        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            Dim count As Integer = 0 'nitin
            If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode") And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) + vbFormFeed)
                Else
                    mFirst = False
                End If
                'objWriter.WriteLine(Space(2) + Space(centerAllign(Len(g_CompanyNames), Len("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"))) & g_CompanyNames)
                objWriter.WriteLine(Space(2) + Space(Len("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")))

                objWriter.WriteLine(Space(2) + "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) + "Page No." & mintPageNo)
                objWriter.WriteLine(Space(2) + "                                                                                                                                                                   Run Date & Time : " & Format(Now(), "dd/MMM/yyyy HH:MM"))
                'Print #intFile, Space(2) + Space(centerAllign(Len("Performance Register from " & Format(frmMonthlyattReport.txtFromDate.Value, "dd/mm/yyyy") & " To " & Format(frmMonthlyattReport.TxtToDate.Value, "dd/mm/yyyy")), Len("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"))) & "Performance Register from " & Format(frmMonthlyattReport.txtFromDate.Value, "dd/mm/yyyy") & " To " & Format(frmMonthlyattReport.TxtToDate.Value, "dd/mm/yyyy")
                objWriter.WriteLine(Space(2) + "Performance Register from " & startdate.ToString("dd/MMM/yyyy") & " To " & Today.ToString("dd/MMM/yyyy"), Len("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------") & "Performance Register from " & startdate.ToString("dd/MMM/yyyy") & " To " & enddate.ToString("dd/MMM/yyyy"))
                mintLine = 5
                mintPageNo = mintPageNo + 1
            End If
            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DepartmentCode
            '        Print #intFile, Space(2) + "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 8) = "Division" And strDeptDivCode <> !DivisionCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DivisionCode
            '        Print #intFile, Space(2) + "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 3) = "Cat" And strDeptDivCode <> !Cat Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !Cat
            '        Print #intFile, Space(2) + "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    End If
            'End If

            mCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            strEmpName = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            strPresentCardNo = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            strIn1 = ""
            strOut1 = ""
            strIn2 = ""
            strOut2 = ""
            strOutwork = ""
            strHoursWorked = ""
            strOtDuration = ""
            strStatus = ""
            strshift = ""
            strLate = ""
            dblPresentValue = 0
            dblAbsentValue = 0
            dblLeaveValue = 0
            dblHoliday_Value = 0
            dblWo_Value = 0
            dblHoursWorked = 0
            dblOtDuration = 0
            dblOtAmount = 0
            tempfromdt = startdate
            temptodt = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("DATEOFFICE").ToString.Trim)
            'Do While Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim = mCode
            For j As Integer = 0 To enddate.Subtract(startdate).Days
                strIn1 = strIn1 + Space(5) + Space(2)
                strOut1 = strOut1 + Space(5) + Space(2)
                strIn2 = strIn2 + Space(5) + Space(2)
                strOut2 = strOut2 + Space(5) + Space(2)
                strHoursWorked = strHoursWorked + Space(5) + Space(2)
                strOtDuration = strOtDuration + Space(5) + Space(2)
                strStatus = strStatus + Space(6) + Space(1)
                strshift = strshift + Space(6) + Space(1)
                strLate = strLate + Space(5) + Space(2)
                tempfromdt = DateAdd("d", 1, tempfromdt)
            Next

            'Do While CDate(Format(tempfromdt, "dd/MM/yyyy")) < CDate(Format(temptodt, "dd/MM/yyyy"))
            '    'Do While Convert.ToDateTime(tempfromdt) < Convert.ToDateTime(temptodt)

            '    'If tempfromdt > temptodt Then
            '    '    'Exit Do
            '    '    Continue For
            '    'End If
            'Loop
            dblPresentValue = dblPresentValue + Rs_Report.Tables(0).Rows(i).Item("PRESENTVALUE").ToString.Trim
            dblAbsentValue = dblAbsentValue + Rs_Report.Tables(0).Rows(i).Item("ABSENTVALUE").ToString.Trim
            dblLeaveValue = dblLeaveValue + Rs_Report.Tables(0).Rows(i).Item("LEAVEVALUE").ToString.Trim
            dblHoliday_Value = dblHoliday_Value + Rs_Report.Tables(0).Rows(i).Item("HOLIDAY_VALUE").ToString.Trim
            dblWo_Value = dblWo_Value + Rs_Report.Tables(0).Rows(i).Item("WO_VALUE").ToString.Trim
            dblHoursWorked = dblHoursWorked + Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim
            dblOtDuration = dblOtDuration + Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim
            dblOtAmount = dblOtAmount + Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim

            'strIn1 = strIn1 + IIf(IsNull(Rs_Report.Tables(0).Rows(i).Item("In1") Or Format(!In1, "HH:MM") = "  :  ", Space(5), Format(!In1, "HH:MM")) & IIf(IsNull(!IN1MANNUAL) Or !IN1MANNUAL = "N", " ", "*") & Space(1)
            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                strIn1 = strIn1 & Space(5) & IIf(Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim = "N", " ", "*") & Space(1)
            Else
                strIn1 = strIn1 & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString).ToString("HH:mm") & IIf(Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim = "N", " ", "*") & Space(1)
            End If
            'strIn1 = strIn1 + IIf((Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = ""), Space(5), Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString).ToString("HH:mm")) & IIf(Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim = "N", " ", "*") & Space(1)


            'strOut1 = strOut1 + IIf(IsNull(!out1) Or Format(!out1, "HH:MM") = "  :  ", Space(5), Format(!out1, "HH:MM")) & IIf(IsNull(!OUT2MANNUAL) Or !OUT2MANNUAL = "N", " ", "*") & Space(1)
            'strIn2 = strIn2 + IIf(IsNull(!in2) Or Format(!in2, "HH:MM") = "  :  ", Space(5), Format(!in2, "HH:MM")) & IIf(IsNull(!IN2MANNUAL) Or !IN2MANNUAL = "N", " ", "*") & Space(1)
            'strOut2 = strOut2 + IIf(IsNull(!out2) Or Format(!out2, "HH:MM") = "  :  ", Space(5), Format(!out2, "HH:MM")) & IIf(IsNull(!OUT2MANNUAL) Or !OUT2MANNUAL = "N", " ", "*") & Space(1)

            'strHoursWorked = strHoursWorked + IIf(IsNull(!HOURSWORKED) Or !HOURSWORKED = 0, Space(5), Length5(Min2Hr(!HOURSWORKED))) & Space(2)
            'If IsNull(!OUTWORKDURATION) Or !OUTWORKDURATION = 0 Then
            If Rs_Report.Tables(0).Rows(i).Item("OUTWORKDURATION").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("OUTWORKDURATION").ToString.Trim = "0" Then
                strOutwork = strOutwork + Space(5) & Space(2)
            Else

                'strOutwork = strOutwork + IIf(IsNull(!OUTWORKDURATION) Or !OUTWORKDURATION = 0, Space(5), Length5(Min2Hr(!OUTWORKDURATION))) & Space(2)
            End If

            'strOtDuration = strOtDuration + IIf(IsNull(!OtDuration) Or !OtDuration = 0, Space(5), Length5(Min2Hr(!OtDuration))) & Space(2)
            'strStatus = strStatus & IIf(IsNull(!Status) Or Len(Trim(!Status)) = 0, Space(6), !Status) & Space(1)
            'strshift = strshift + !ShiftAttended + Space(4)
            'strLate = strLate + IIf(IsNull(!latearrival) Or !latearrival = 0, Space(5), Length5(Min2Hr(!latearrival))) & Space(2)
            'Next
            'Loop
            ' If Trim(mCode) = "0005" Then Stop
            objWriter.WriteLine(Space(2) + "")
            intFirstTime = 2
            objWriter.WriteLine(Space(2) + "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
            objWriter.WriteLine(Space(2) + strLineForDays)
            objWriter.WriteLine(Space(2) + "           " & strDays)
            objWriter.WriteLine(Space(2) + "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
            objWriter.WriteLine(Space(2) + "")
            objWriter.WriteLine(Space(2) + "PayCode : " & mCode & " Card No. : " & strPresentCardNo & " Name : " & strEmpName & "  Present " & _
                                   dblPresentValue & "  Absent " & dblAbsentValue & "  Holiday ") '& _
            'dblHoliday_Value & "  Weekly_Off " & dblWo_Value & "  Leave " & dblLeaveValue & " Hours_Worked " & _
            'Length5(Min2Hr(Str(dblHoursWorked))) & "  Overtime " & Length5(Min2Hr(Str(dblOtDuration))) & "  OT Amount " & _
            'dblOtAmount)
            objWriter.WriteLine(Space(2) + "")
            objWriter.WriteLine(Space(2) + "In  Time   " & strIn1)
            objWriter.WriteLine(Space(2) + "Out Time   " & strOut2)
            objWriter.WriteLine(Space(2) + "Late       " & strLate)
            objWriter.WriteLine(Space(2) + "Hrs. Wkd.  " & strHoursWorked)
            objWriter.WriteLine(Space(2) + "Out Work   " & strOutwork)
            objWriter.WriteLine(Space(2) + "Over Time  " & strOtDuration)
            objWriter.WriteLine(Space(2) + "Status     " & strStatus)
            objWriter.WriteLine(Space(2) + "Shift Att. " & strshift)
            intGroupLine = intGroupLine + 1
            mintLine = mintLine + 13
            mblnCheckReport = True
        Next

        objWriter.Close()
        Exit Sub
ErrorGen:
        MsgBox(Err.Description, vbInformation + vbOKOnly, "Report Generation Information")
    End Sub

    Public Shared Sub GetCompanies()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim rsComp As DataSet = New DataSet
        Dim strsql As String, intDummy As Integer
        g_CompanyNames = ""
        'strsql = "Select CompanyCode, CompanyName From tblCompany order by CompanyCode" 'where " & gcompany
        Dim emp() As String = Common.auth_comp.Split(",")
        Dim ls As New List(Of String)()
        For x As Integer = 0 To emp.Length - 1
            ls.Add(emp(x).Trim)
        Next
        strsql = "select * from TBLCompany where COMPANYCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"

        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(rsComp)
        Else
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(rsComp)
        End If

       
        For i As Integer = 0 To rsComp.Tables(0).Rows.Count - 1
            g_CompanyNames = g_CompanyNames & ", " & rsComp.Tables(0).Rows(i).Item("CompanyName").ToString.Trim
        Next
        'For i As Integer = 0 To Common.ComArr.Length - 1
        '    g_CompanyNames = g_CompanyNames & ", " & Common.ComArr(i).ToString.Trim
        'Next
        g_CompanyNames = g_CompanyNames.TrimStart(",")
        'MsgBox("g_CompanyNames  " & g_CompanyNames)
    End Sub

End Class