﻿Imports System
Imports System.Runtime.InteropServices

Namespace AscDemo
    Public Class CHCNetSDK
        Public Const NET_DVR_NOERROR As Integer = 0
        Public Const NET_DVR_PASSWORD_ERROR As Integer = 1
        Public Const NET_DVR_NOENOUGHPRI As Integer = 2
        Public Const NET_DVR_NOINIT As Integer = 3
        Public Const NET_DVR_CHANNEL_ERROR As Integer = 4
        Public Const NET_DVR_OVER_MAXLINK As Integer = 5
        Public Const NET_DVR_VERSIONNOMATCH As Integer = 6
        Public Const NET_DVR_NETWORK_FAIL_CONNECT As Integer = 7
        Public Const NET_DVR_NETWORK_SEND_ERROR As Integer = 8
        Public Const NET_DVR_NETWORK_RECV_ERROR As Integer = 9
        Public Const NET_DVR_NETWORK_RECV_TIMEOUT As Integer = 10
        Public Const NET_DVR_NETWORK_ERRORDATA As Integer = 11
        Public Const NET_DVR_ORDER_ERROR As Integer = 12
        Public Const NET_DVR_OPERNOPERMIT As Integer = 13
        Public Const NET_DVR_COMMANDTIMEOUT As Integer = 14
        Public Const NET_DVR_ERRORSERIALPORT As Integer = 15
        Public Const NET_DVR_ERRORALARMPORT As Integer = 16
        Public Const NET_DVR_PARAMETER_ERROR As Integer = 17
        Public Const NET_DVR_CHAN_EXCEPTION As Integer = 18
        Public Const NET_DVR_NODISK As Integer = 19
        Public Const NET_DVR_ERRORDISKNUM As Integer = 20
        Public Const NET_DVR_DISK_FULL As Integer = 21
        Public Const NET_DVR_DISK_ERROR As Integer = 22
        Public Const NET_DVR_NOSUPPORT As Integer = 23
        Public Const NET_DVR_BUSY As Integer = 24
        Public Const NET_DVR_MODIFY_FAIL As Integer = 25
        Public Const NET_DVR_PASSWORD_FORMAT_ERROR As Integer = 26
        Public Const NET_DVR_DISK_FORMATING As Integer = 27
        Public Const NET_DVR_DVRNORESOURCE As Integer = 28
        Public Const NET_DVR_DVROPRATEFAILED As Integer = 29
        Public Const NET_DVR_OPENHOSTSOUND_FAIL As Integer = 30
        Public Const NET_DVR_DVRVOICEOPENED As Integer = 31
        Public Const NET_DVR_TIMEINPUTERROR As Integer = 32
        Public Const NET_DVR_NOSPECFILE As Integer = 33
        Public Const NET_DVR_CREATEFILE_ERROR As Integer = 34
        Public Const NET_DVR_FILEOPENFAIL As Integer = 35
        Public Const NET_DVR_OPERNOTFINISH As Integer = 36
        Public Const NET_DVR_GETPLAYTIMEFAIL As Integer = 37
        Public Const NET_DVR_PLAYFAIL As Integer = 38
        Public Const NET_DVR_FILEFORMAT_ERROR As Integer = 39
        Public Const NET_DVR_DIR_ERROR As Integer = 40
        Public Const NET_DVR_ALLOC_RESOURCE_ERROR As Integer = 41
        Public Const NET_DVR_AUDIO_MODE_ERROR As Integer = 42
        Public Const NET_DVR_NOENOUGH_BUF As Integer = 43
        Public Const NET_DVR_CREATESOCKET_ERROR As Integer = 44
        Public Const NET_DVR_SETSOCKET_ERROR As Integer = 45
        Public Const NET_DVR_MAX_NUM As Integer = 46
        Public Const NET_DVR_USERNOTEXIST As Integer = 47
        Public Const NET_DVR_WRITEFLASHERROR As Integer = 48
        Public Const NET_DVR_UPGRADEFAIL As Integer = 49
        Public Const NET_DVR_CARDHAVEINIT As Integer = 50
        Public Const NET_DVR_PLAYERFAILED As Integer = 51
        Public Const NET_DVR_MAX_USERNUM As Integer = 52
        Public Const NET_DVR_GETLOCALIPANDMACFAIL As Integer = 53
        Public Const NET_DVR_NOENCODEING As Integer = 54
        Public Const NET_DVR_IPMISMATCH As Integer = 55
        Public Const NET_DVR_MACMISMATCH As Integer = 56
        Public Const NET_DVR_USER_LOCKED As Integer = 153
        Public Const NET_DVR_DEV_ADDRESS_MAX_LEN As Integer = 129
        Public Const NET_DVR_LOGIN_USERNAME_MAX_LEN As Integer = 64
        Public Const NET_DVR_LOGIN_PASSWD_MAX_LEN As Integer = 64
        Public Const SERIALNO_LEN As Integer = 48
        Public Const STREAM_ID_LEN As Integer = 32
        Public Const MAX_AUDIO_V40 As Integer = 8
        Public Const LOG_INFO_LEN As Integer = 11840
        Public Const MAX_NAMELEN As Integer = 16
        Public Const MAX_DOMAIN_NAME As Integer = 64
        Public Const MAX_ETHERNET As Integer = 2
        Public Const NAME_LEN As Integer = 32
        Public Const PASSWD_LEN As Integer = 16
        Public Const MAX_RIGHT As Integer = 32
        Public Const MACADDR_LEN As Integer = 6
        Public Const DEV_TYPE_NAME_LEN As Integer = 24
        Public Const MAX_ANALOG_CHANNUM As Integer = 32
        Public Const MAX_IP_CHANNEL As Integer = 32
        Public Const MAX_CHANNUM_V30 As Integer = (MAX_ANALOG_CHANNUM + MAX_IP_CHANNEL)
        Public Const MAX_CHANNUM_V40 As Integer = 512
        Public Const MAX_IP_DEVICE_V40 As Integer = 64
        Public Const DEV_ID_LEN As Integer = 32
        Public Const MAX_IP_DEVICE As Integer = 32
        Public Const MAX_IP_ALARMIN_V40 As Integer = 4096
        Public Const MAX_IP_ALARMOUT_V40 As Integer = 4096
        Public Const MAX_IP_ALARMIN As Integer = 128
        Public Const MAX_IP_ALARMOUT As Integer = 64
        Public Const URL_LEN As Integer = 240
        Public Const MAX_AUDIOOUT_PRO_TYPE As Integer = 8
        Public Const ACS_ABILITY As Integer = &H801
        Public Const NET_DVR_CLEAR_ACS_PARAM As Integer = 2118
        Public Const NET_DVR_GET_ACS_EVENT As Integer = 2514
        Public Const COMM_ALARM_ACS As Integer = &H5002
        Public Const COMM_ISAPI_ALARM As Integer = &H6009 'Newly Add Face Temperature Test Event
        Public Const MAJOR_ALARM As Integer = &H1
        Public Const MINOR_ALARMIN_SHORT_CIRCUIT As Integer = &H400
        Public Const MINOR_ALARMIN_BROKEN_CIRCUIT As Integer = &H401
        Public Const MINOR_ALARMIN_EXCEPTION As Integer = &H402
        Public Const MINOR_ALARMIN_RESUME As Integer = &H403
        Public Const MINOR_HOST_DESMANTLE_ALARM As Integer = &H404
        Public Const MINOR_HOST_DESMANTLE_RESUME As Integer = &H405
        Public Const MINOR_CARD_READER_DESMANTLE_ALARM As Integer = &H406
        Public Const MINOR_CARD_READER_DESMANTLE_RESUME As Integer = &H407
        Public Const MINOR_CASE_SENSOR_ALARM As Integer = &H408
        Public Const MINOR_CASE_SENSOR_RESUME As Integer = &H409
        Public Const MINOR_STRESS_ALARM As Integer = &H40A
        Public Const MINOR_OFFLINE_ECENT_NEARLY_FULL As Integer = &H40B
        Public Const MINOR_CARD_MAX_AUTHENTICATE_FAIL As Integer = &H40C
        Public Const MINOR_SD_CARD_FULL As Integer = &H40D
        Public Const MINOR_LINKAGE_CAPTURE_PIC As Integer = &H40E
        Public Const MINOR_SECURITY_MODULE_DESMANTLE_ALARM As Integer = &H40F
        Public Const MINOR_SECURITY_MODULE_DESMANTLE_RESUME As Integer = &H410
        Public Const MINOR_POS_START_ALARM As Integer = &H411
        Public Const MINOR_POS_END_ALARM As Integer = &H412
        Public Const MINOR_FACE_IMAGE_QUALITY_LOW As Integer = &H413
        Public Const MINOR_FINGE_RPRINT_QUALITY_LOW As Integer = &H414
        Public Const MINOR_FIRE_IMPORT_SHORT_CIRCUIT As Integer = &H415
        Public Const MINOR_FIRE_IMPORT_BROKEN_CIRCUIT As Integer = &H416
        Public Const MINOR_FIRE_IMPORT_RESUME As Integer = &H417
        Public Const MINOR_FIRE_BUTTON_TRIGGER As Integer = &H418
        Public Const MINOR_FIRE_BUTTON_RESUME As Integer = &H419
        Public Const MINOR_MAINTENANCE_BUTTON_TRIGGER As Integer = &H41A
        Public Const MINOR_MAINTENANCE_BUTTON_RESUME As Integer = &H41B
        Public Const MINOR_EMERGENCY_BUTTON_TRIGGER As Integer = &H41C
        Public Const MINOR_EMERGENCY_BUTTON_RESUME As Integer = &H41D
        Public Const MINOR_DISTRACT_CONTROLLER_ALARM As Integer = &H41E
        Public Const MINOR_DISTRACT_CONTROLLER_RESUME As Integer = &H41F
        Public Const MINOR_CHANNEL_CONTROLLER_DESMANTLE_ALARM As Integer = &H422
        Public Const MINOR_CHANNEL_CONTROLLER_DESMANTLE_RESUME As Integer = &H423
        Public Const MINOR_CHANNEL_CONTROLLER_FIRE_IMPORT_ALARM As Integer = &H424
        Public Const MINOR_CHANNEL_CONTROLLER_FIRE_IMPORT_RESUME As Integer = &H425
        Public Const MINOR_PRINTER_OUT_OF_PAPER As Integer = &H440
        Public Const MINOR_LEGAL_EVENT_NEARLY_FULL As Integer = &H442
        Public Const MAJOR_EXCEPTION As Integer = &H2
        Public Const MINOR_NET_BROKEN As Integer = &H27
        Public Const MINOR_RS485_DEVICE_ABNORMAL As Integer = &H3A
        Public Const MINOR_RS485_DEVICE_REVERT As Integer = &H3B
        Public Const MINOR_DEV_POWER_ON As Integer = &H400
        Public Const MINOR_DEV_POWER_OFF As Integer = &H401
        Public Const MINOR_WATCH_DOG_RESET As Integer = &H402
        Public Const MINOR_LOW_BATTERY As Integer = &H403
        Public Const MINOR_BATTERY_RESUME As Integer = &H404
        Public Const MINOR_AC_OFF As Integer = &H405
        Public Const MINOR_AC_RESUME As Integer = &H406
        Public Const MINOR_NET_RESUME As Integer = &H407
        Public Const MINOR_FLASH_ABNORMAL As Integer = &H408
        Public Const MINOR_CARD_READER_OFFLINE As Integer = &H409
        Public Const MINOR_CARD_READER_RESUME As Integer = &H40A
        Public Const MINOR_INDICATOR_LIGHT_OFF As Integer = &H40B
        Public Const MINOR_INDICATOR_LIGHT_RESUME As Integer = &H40C
        Public Const MINOR_CHANNEL_CONTROLLER_OFF As Integer = &H40D
        Public Const MINOR_CHANNEL_CONTROLLER_RESUME As Integer = &H40E
        Public Const MINOR_SECURITY_MODULE_OFF As Integer = &H40F
        Public Const MINOR_SECURITY_MODULE_RESUME As Integer = &H410
        Public Const MINOR_BATTERY_ELECTRIC_LOW As Integer = &H411
        Public Const MINOR_BATTERY_ELECTRIC_RESUME As Integer = &H412
        Public Const MINOR_LOCAL_CONTROL_NET_BROKEN As Integer = &H413
        Public Const MINOR_LOCAL_CONTROL_NET_RSUME As Integer = &H414
        Public Const MINOR_MASTER_RS485_LOOPNODE_BROKEN As Integer = &H415
        Public Const MINOR_MASTER_RS485_LOOPNODE_RESUME As Integer = &H416
        Public Const MINOR_LOCAL_CONTROL_OFFLINE As Integer = &H417
        Public Const MINOR_LOCAL_CONTROL_RESUME As Integer = &H418
        Public Const MINOR_LOCAL_DOWNSIDE_RS485_LOOPNODE_BROKEN As Integer = &H419
        Public Const MINOR_LOCAL_DOWNSIDE_RS485_LOOPNODE_RESUME As Integer = &H41A
        Public Const MINOR_DISTRACT_CONTROLLER_ONLINE As Integer = &H41B
        Public Const MINOR_DISTRACT_CONTROLLER_OFFLINE As Integer = &H41C
        Public Const MINOR_ID_CARD_READER_NOT_CONNECT As Integer = &H41D
        Public Const MINOR_ID_CARD_READER_RESUME As Integer = &H41E
        Public Const MINOR_FINGER_PRINT_MODULE_NOT_CONNECT As Integer = &H41F
        Public Const MINOR_FINGER_PRINT_MODULE_RESUME As Integer = &H420
        Public Const MINOR_CAMERA_NOT_CONNECT As Integer = &H421
        Public Const MINOR_CAMERA_RESUME As Integer = &H422
        Public Const MINOR_COM_NOT_CONNECT As Integer = &H423
        Public Const MINOR_COM_RESUME As Integer = &H424
        Public Const MINOR_DEVICE_NOT_AUTHORIZE As Integer = &H425
        Public Const MINOR_PEOPLE_AND_ID_CARD_DEVICE_ONLINE As Integer = &H426
        Public Const MINOR_PEOPLE_AND_ID_CARD_DEVICE_OFFLINE As Integer = &H427
        Public Const MINOR_LOCAL_LOGIN_LOCK As Integer = &H428
        Public Const MINOR_LOCAL_LOGIN_UNLOCK As Integer = &H429
        Public Const MINOR_SUBMARINEBACK_COMM_BREAK As Integer = &H42A
        Public Const MINOR_SUBMARINEBACK_COMM_RESUME As Integer = &H42B
        Public Const MINOR_MOTOR_SENSOR_EXCEPTION As Integer = &H42C
        Public Const MINOR_CAN_BUS_EXCEPTION As Integer = &H42D
        Public Const MINOR_CAN_BUS_RESUME As Integer = &H42E
        Public Const MINOR_GATE_TEMPERATURE_OVERRUN As Integer = &H42F
        Public Const MINOR_IR_EMITTER_EXCEPTION As Integer = &H430
        Public Const MINOR_IR_EMITTER_RESUME As Integer = &H431
        Public Const MINOR_LAMP_BOARD_COMM_EXCEPTION As Integer = &H432
        Public Const MINOR_LAMP_BOARD_COMM_RESUME As Integer = &H433
        Public Const MINOR_IR_ADAPTOR_COMM_EXCEPTION As Integer = &H434
        Public Const MINOR_IR_ADAPTOR_COMM_RESUME As Integer = &H435
        Public Const MINOR_PRINTER_ONLINE As Integer = &H436
        Public Const MINOR_PRINTER_OFFLINE As Integer = &H437
        Public Const MINOR_4G_MOUDLE_ONLINE As Integer = &H438
        Public Const MINOR_4G_MOUDLE_OFFLINE As Integer = &H439
        Public Const MAJOR_OPERATION As Integer = &H3
        Public Const MINOR_LOCAL_UPGRADE As Integer = &H5A
        Public Const MINOR_REMOTE_LOGIN As Integer = &H70
        Public Const MINOR_REMOTE_LOGOUT As Integer = &H71
        Public Const MINOR_REMOTE_ARM As Integer = &H79
        Public Const MINOR_REMOTE_DISARM As Integer = &H7A
        Public Const MINOR_REMOTE_REBOOT As Integer = &H7B
        Public Const MINOR_REMOTE_UPGRADE As Integer = &H7E
        Public Const MINOR_REMOTE_CFGFILE_OUTPUT As Integer = &H86
        Public Const MINOR_REMOTE_CFGFILE_INTPUT As Integer = &H87
        Public Const MINOR_REMOTE_ALARMOUT_OPEN_MAN As Integer = &HD6
        Public Const MINOR_REMOTE_ALARMOUT_CLOSE_MAN As Integer = &HD7
        Public Const MINOR_REMOTE_OPEN_DOOR As Integer = &H400
        Public Const MINOR_REMOTE_CLOSE_DOOR As Integer = &H401
        Public Const MINOR_REMOTE_ALWAYS_OPEN As Integer = &H402
        Public Const MINOR_REMOTE_ALWAYS_CLOSE As Integer = &H403
        Public Const MINOR_REMOTE_CHECK_TIME As Integer = &H404
        Public Const MINOR_NTP_CHECK_TIME As Integer = &H405
        Public Const MINOR_REMOTE_CLEAR_CARD As Integer = &H406
        Public Const MINOR_REMOTE_RESTORE_CFG As Integer = &H407
        Public Const MINOR_ALARMIN_ARM As Integer = &H408
        Public Const MINOR_ALARMIN_DISARM As Integer = &H409
        Public Const MINOR_LOCAL_RESTORE_CFG As Integer = &H40A
        Public Const MINOR_REMOTE_CAPTURE_PIC As Integer = &H40B
        Public Const MINOR_MOD_NET_REPORT_CFG As Integer = &H40C
        Public Const MINOR_MOD_GPRS_REPORT_PARAM As Integer = &H40D
        Public Const MINOR_MOD_REPORT_GROUP_PARAM As Integer = &H40E
        Public Const MINOR_UNLOCK_PASSWORD_OPEN_DOOR As Integer = &H40F
        Public Const MINOR_AUTO_RENUMBER As Integer = &H410
        Public Const MINOR_AUTO_COMPLEMENT_NUMBER As Integer = &H411
        Public Const MINOR_NORMAL_CFGFILE_INPUT As Integer = &H412
        Public Const MINOR_NORMAL_CFGFILE_OUTTPUT As Integer = &H413
        Public Const MINOR_CARD_RIGHT_INPUT As Integer = &H414
        Public Const MINOR_CARD_RIGHT_OUTTPUT As Integer = &H415
        Public Const MINOR_LOCAL_USB_UPGRADE As Integer = &H416
        Public Const MINOR_REMOTE_VISITOR_CALL_LADDER As Integer = &H417
        Public Const MINOR_REMOTE_HOUSEHOLD_CALL_LADDER As Integer = &H418
        Public Const MINOR_REMOTE_ACTUAL_GUARD As Integer = &H419
        Public Const MINOR_REMOTE_ACTUAL_UNGUARD As Integer = &H41A
        Public Const MINOR_REMOTE_CONTROL_NOT_CODE_OPER_FAILED As Integer = &H41B
        Public Const MINOR_REMOTE_CONTROL_CLOSE_DOOR As Integer = &H41C
        Public Const MINOR_REMOTE_CONTROL_OPEN_DOOR As Integer = &H41D
        Public Const MINOR_REMOTE_CONTROL_ALWAYS_OPEN_DOOR As Integer = &H41E
        Public Const MAJOR_EVENT As Integer = &H5
        Public Const MINOR_LEGAL_CARD_PASS As Integer = &H1
        Public Const MINOR_CARD_AND_PSW_PASS As Integer = &H2
        Public Const MINOR_CARD_AND_PSW_FAIL As Integer = &H3
        Public Const MINOR_CARD_AND_PSW_TIMEOUT As Integer = &H4
        Public Const MINOR_CARD_AND_PSW_OVER_TIME As Integer = &H5
        Public Const MINOR_CARD_NO_RIGHT As Integer = &H6
        Public Const MINOR_CARD_INVALID_PERIOD As Integer = &H7
        Public Const MINOR_CARD_OUT_OF_DATE As Integer = &H8
        Public Const MINOR_INVALID_CARD As Integer = &H9
        Public Const MINOR_ANTI_SNEAK_FAIL As Integer = &HA
        Public Const MINOR_INTERLOCK_DOOR_NOT_CLOSE As Integer = &HB
        Public Const MINOR_NOT_BELONG_MULTI_GROUP As Integer = &HC
        Public Const MINOR_INVALID_MULTI_VERIFY_PERIOD As Integer = &HD
        Public Const MINOR_MULTI_VERIFY_SUPER_RIGHT_FAIL As Integer = &HE
        Public Const MINOR_MULTI_VERIFY_REMOTE_RIGHT_FAIL As Integer = &HF
        Public Const MINOR_MULTI_VERIFY_SUCCESS As Integer = &H10
        Public Const MINOR_LEADER_CARD_OPEN_BEGIN As Integer = &H11
        Public Const MINOR_LEADER_CARD_OPEN_END As Integer = &H12
        Public Const MINOR_ALWAYS_OPEN_BEGIN As Integer = &H13
        Public Const MINOR_ALWAYS_OPEN_END As Integer = &H14
        Public Const MINOR_LOCK_OPEN As Integer = &H15
        Public Const MINOR_LOCK_CLOSE As Integer = &H16
        Public Const MINOR_DOOR_BUTTON_PRESS As Integer = &H17
        Public Const MINOR_DOOR_BUTTON_RELEASE As Integer = &H18
        Public Const MINOR_DOOR_OPEN_NORMAL As Integer = &H19
        Public Const MINOR_DOOR_CLOSE_NORMAL As Integer = &H1A
        Public Const MINOR_DOOR_OPEN_ABNORMAL As Integer = &H1B
        Public Const MINOR_DOOR_OPEN_TIMEOUT As Integer = &H1C
        Public Const MINOR_ALARMOUT_ON As Integer = &H1D
        Public Const MINOR_ALARMOUT_OFF As Integer = &H1E
        Public Const MINOR_ALWAYS_CLOSE_BEGIN As Integer = &H1F
        Public Const MINOR_ALWAYS_CLOSE_END As Integer = &H20
        Public Const MINOR_MULTI_VERIFY_NEED_REMOTE_OPEN As Integer = &H21
        Public Const MINOR_MULTI_VERIFY_SUPERPASSWD_VERIFY_SUCCESS As Integer = &H22
        Public Const MINOR_MULTI_VERIFY_REPEAT_VERIFY As Integer = &H23
        Public Const MINOR_MULTI_VERIFY_TIMEOUT As Integer = &H24
        Public Const MINOR_DOORBELL_RINGING As Integer = &H25
        Public Const MINOR_FINGERPRINT_COMPARE_PASS As Integer = &H26
        Public Const MINOR_FINGERPRINT_COMPARE_FAIL As Integer = &H27
        Public Const MINOR_CARD_FINGERPRINT_VERIFY_PASS As Integer = &H28
        Public Const MINOR_CARD_FINGERPRINT_VERIFY_FAIL As Integer = &H29
        Public Const MINOR_CARD_FINGERPRINT_VERIFY_TIMEOUT As Integer = &H2A
        Public Const MINOR_CARD_FINGERPRINT_PASSWD_VERIFY_PASS As Integer = &H2B
        Public Const MINOR_CARD_FINGERPRINT_PASSWD_VERIFY_FAIL As Integer = &H2C
        Public Const MINOR_CARD_FINGERPRINT_PASSWD_VERIFY_TIMEOUT As Integer = &H2D
        Public Const MINOR_FINGERPRINT_PASSWD_VERIFY_PASS As Integer = &H2E
        Public Const MINOR_FINGERPRINT_PASSWD_VERIFY_FAIL As Integer = &H2F
        Public Const MINOR_FINGERPRINT_PASSWD_VERIFY_TIMEOUT As Integer = &H30
        Public Const MINOR_FINGERPRINT_INEXISTENCE As Integer = &H31
        Public Const MINOR_CARD_PLATFORM_VERIFY As Integer = &H32
        Public Const MINOR_CALL_CENTER As Integer = &H33
        Public Const MINOR_FIRE_RELAY_TURN_ON_DOOR_ALWAYS_OPEN As Integer = &H34
        Public Const MINOR_FIRE_RELAY_RECOVER_DOOR_RECOVER_NORMAL As Integer = &H35
        Public Const MINOR_FACE_AND_FP_VERIFY_PASS As Integer = &H36
        Public Const MINOR_FACE_AND_FP_VERIFY_FAIL As Integer = &H37
        Public Const MINOR_FACE_AND_FP_VERIFY_TIMEOUT As Integer = &H38
        Public Const MINOR_FACE_AND_PW_VERIFY_PASS As Integer = &H39
        Public Const MINOR_FACE_AND_PW_VERIFY_FAIL As Integer = &H3A
        Public Const MINOR_FACE_AND_PW_VERIFY_TIMEOUT As Integer = &H3B
        Public Const MINOR_FACE_AND_CARD_VERIFY_PASS As Integer = &H3C
        Public Const MINOR_FACE_AND_CARD_VERIFY_FAIL As Integer = &H3D
        Public Const MINOR_FACE_AND_CARD_VERIFY_TIMEOUT As Integer = &H3E
        Public Const MINOR_FACE_AND_PW_AND_FP_VERIFY_PASS As Integer = &H3F
        Public Const MINOR_FACE_AND_PW_AND_FP_VERIFY_FAIL As Integer = &H40
        Public Const MINOR_FACE_AND_PW_AND_FP_VERIFY_TIMEOUT As Integer = &H41
        Public Const MINOR_FACE_CARD_AND_FP_VERIFY_PASS As Integer = &H42
        Public Const MINOR_FACE_CARD_AND_FP_VERIFY_FAIL As Integer = &H43
        Public Const MINOR_FACE_CARD_AND_FP_VERIFY_TIMEOUT As Integer = &H44
        Public Const MINOR_EMPLOYEENO_AND_FP_VERIFY_PASS As Integer = &H45
        Public Const MINOR_EMPLOYEENO_AND_FP_VERIFY_FAIL As Integer = &H46
        Public Const MINOR_EMPLOYEENO_AND_FP_VERIFY_TIMEOUT As Integer = &H47
        Public Const MINOR_EMPLOYEENO_AND_FP_AND_PW_VERIFY_PASS As Integer = &H48
        Public Const MINOR_EMPLOYEENO_AND_FP_AND_PW_VERIFY_FAIL As Integer = &H49
        Public Const MINOR_EMPLOYEENO_AND_FP_AND_PW_VERIFY_TIMEOUT As Integer = &H4A
        Public Const MINOR_FACE_VERIFY_PASS As Integer = &H4B
        Public Const MINOR_FACE_VERIFY_FAIL As Integer = &H4C
        Public Const MINOR_EMPLOYEENO_AND_FACE_VERIFY_PASS As Integer = &H4D
        Public Const MINOR_EMPLOYEENO_AND_FACE_VERIFY_FAIL As Integer = &H4E
        Public Const MINOR_EMPLOYEENO_AND_FACE_VERIFY_TIMEOUT As Integer = &H4F
        Public Const MINOR_FACE_RECOGNIZE_FAIL As Integer = &H50
        Public Const MINOR_FIRSTCARD_AUTHORIZE_BEGIN As Integer = &H51
        Public Const MINOR_FIRSTCARD_AUTHORIZE_END As Integer = &H52
        Public Const MINOR_DOORLOCK_INPUT_SHORT_CIRCUIT As Integer = &H53
        Public Const MINOR_DOORLOCK_INPUT_BROKEN_CIRCUIT As Integer = &H54
        Public Const MINOR_DOORLOCK_INPUT_EXCEPTION As Integer = &H55
        Public Const MINOR_DOORCONTACT_INPUT_SHORT_CIRCUIT As Integer = &H56
        Public Const MINOR_DOORCONTACT_INPUT_BROKEN_CIRCUIT As Integer = &H57
        Public Const MINOR_DOORCONTACT_INPUT_EXCEPTION As Integer = &H58
        Public Const MINOR_OPENBUTTON_INPUT_SHORT_CIRCUIT As Integer = &H59
        Public Const MINOR_OPENBUTTON_INPUT_BROKEN_CIRCUIT As Integer = &H5A
        Public Const MINOR_OPENBUTTON_INPUT_EXCEPTION As Integer = &H5B
        Public Const MINOR_DOORLOCK_OPEN_EXCEPTION As Integer = &H5C
        Public Const MINOR_DOORLOCK_OPEN_TIMEOUT As Integer = &H5D
        Public Const MINOR_FIRSTCARD_OPEN_WITHOUT_AUTHORIZE As Integer = &H5E
        Public Const MINOR_CALL_LADDER_RELAY_BREAK As Integer = &H5F
        Public Const MINOR_CALL_LADDER_RELAY_CLOSE As Integer = &H60
        Public Const MINOR_AUTO_KEY_RELAY_BREAK As Integer = &H61
        Public Const MINOR_AUTO_KEY_RELAY_CLOSE As Integer = &H62
        Public Const MINOR_KEY_CONTROL_RELAY_BREAK As Integer = &H63
        Public Const MINOR_KEY_CONTROL_RELAY_CLOSE As Integer = &H64
        Public Const MINOR_EMPLOYEENO_AND_PW_PASS As Integer = &H65
        Public Const MINOR_EMPLOYEENO_AND_PW_FAIL As Integer = &H66
        Public Const MINOR_EMPLOYEENO_AND_PW_TIMEOUT As Integer = &H67
        Public Const MINOR_HUMAN_DETECT_FAIL As Integer = &H68
        Public Const MINOR_PEOPLE_AND_ID_CARD_COMPARE_PASS As Integer = &H69
        Public Const MINOR_PEOPLE_AND_ID_CARD_COMPARE_FAIL As Integer = &H70
        Public Const MINOR_CERTIFICATE_BLACK_LIST As Integer = &H71
        Public Const MINOR_LEGAL_MESSAGE As Integer = &H72
        Public Const MINOR_ILLEGAL_MESSAGE As Integer = &H73
        Public Const MINOR_MAC_DETECT As Integer = &H74
        Public Const MINOR_DOOR_OPEN_OR_DORMANT_FAIL As Integer = &H75
        Public Const MINOR_AUTH_PLAN_DORMANT_FAIL As Integer = &H76
        Public Const MINOR_CARD_ENCRYPT_VERIFY_FAIL As Integer = &H77
        Public Const MINOR_SUBMARINEBACK_REPLY_FAIL As Integer = &H78
        Public Const MINOR_DOOR_OPEN_OR_DORMANT_OPEN_FAIL As Integer = &H82
        Public Const MINOR_DOOR_OPEN_OR_DORMANT_LINKAGE_OPEN_FAIL As Integer = &H84
        Public Const MINOR_TRAILING As Integer = &H85
        Public Const MINOR_HEART_BEAT As Integer = &H83
        Public Const MINOR_REVERSE_ACCESS As Integer = &H86
        Public Const MINOR_FORCE_ACCESS As Integer = &H87
        Public Const MINOR_CLIMBING_OVER_GATE As Integer = &H88
        Public Const MINOR_PASSING_TIMEOUT As Integer = &H89
        Public Const MINOR_INTRUSION_ALARM As Integer = &H8A
        Public Const MINOR_FREE_GATE_PASS_NOT_AUTH As Integer = &H8B
        Public Const MINOR_DROP_ARM_BLOCK As Integer = &H8C
        Public Const MINOR_DROP_ARM_BLOCK_RESUME As Integer = &H8D
        Public Const MINOR_LOCAL_FACE_MODELING_FAIL As Integer = &H8E
        Public Const MINOR_STAY_EVENT As Integer = &H8F
        Public Const MINOR_PASSWORD_MISMATCH As Integer = &H97
        Public Const MINOR_EMPLOYEE_NO_NOT_EXIST As Integer = &H98
        Public Const MINOR_COMBINED_VERIFY_PASS As Integer = &H99
        Public Const MINOR_COMBINED_VERIFY_TIMEOUT As Integer = &H9A
        Public Const MINOR_VERIFY_MODE_MISMATCH As Integer = &H9B
        Public Const CARD_PARAM_CARD_VALID As Integer = &H1
        Public Const CARD_PARAM_VALID As Integer = &H2
        Public Const CARD_PARAM_CARD_TYPE As Integer = &H4
        Public Const CARD_PARAM_DOOR_RIGHT As Integer = &H8
        Public Const CARD_PARAM_LEADER_CARD As Integer = &H10
        Public Const CARD_PARAM_SWIPE_NUM As Integer = &H20
        Public Const CARD_PARAM_GROUP As Integer = &H40
        Public Const CARD_PARAM_PASSWORD As Integer = &H80
        Public Const CARD_PARAM_RIGHT_PLAN As Integer = &H100
        Public Const CARD_PARAM_SWIPED_NUM As Integer = &H200
        Public Const CARD_PARAM_EMPLOYEE_NO As Integer = &H400
        Public Const ACS_CARD_NO_LEN As Integer = 32
        Public Const MAX_DOOR_NUM_256 As Integer = 256
        Public Const MAX_GROUP_NUM_128 As Integer = 128
        Public Const CARD_PASSWORD_LEN As Integer = 8
        Public Const MAX_CARD_RIGHT_PLAN_NUM As Integer = 4
        Public Const MAX_DOOR_CODE_LEN As Integer = 8
        Public Const MAX_LOCK_CODE_LEN As Integer = 8
        Public Const MAX_CASE_SENSOR_NUM As Integer = 8
        Public Const MAX_CARD_READER_NUM_512 As Integer = 512
        Public Const MAX_ALARMHOST_ALARMIN_NUM As Integer = 512
        Public Const MAX_ALARMHOST_ALARMOUT_NUM As Integer = 512
        Public Const NET_DVR_GET_ACS_WORK_STATUS_V50 As Integer = 2180
        Public Const NET_DVR_GET_CARD_CFG_V50 As Integer = 2178
        Public Const NET_DVR_SET_CARD_CFG_V50 As Integer = 2179
        Public Const NET_DVR_GET_TIMECFG As Integer = 118
        Public Const NET_DVR_SET_TIMECFG As Integer = 119
        Public Const NET_DVR_GET_AUDIOIN_VOLUME_CFG As Integer = 6355
        Public Const NET_DVR_SET_AUDIOIN_VOLUME_CFG As Integer = 6356
        Public Const NET_DVR_GET_AUDIOOUT_VOLUME_CFG As Integer = 6369
        Public Const NET_DVR_SET_AUDIOOUT_VOLUME_CFG As Integer = 6370
        Public Const DOOR_NAME_LEN As Integer = 32
        Public Const STRESS_PASSWORD_LEN As Integer = 8
        Public Const SUPER_PASSWORD_LEN As Integer = 8
        Public Const UNLOCK_PASSWORD_LEN As Integer = 8
        Public Const MAX_DOOR_NUM As Integer = 32
        Public Const MAX_GROUP_NUM As Integer = 32
        Public Const LOCAL_CONTROLLER_NAME_LEN As Integer = 32
        Public Const NET_DVR_GET_DOOR_CFG As Integer = 2108
        Public Const NET_DVR_SET_DOOR_CFG As Integer = 2109
        Public Const NET_SDK_MONITOR_ID_LEN As Integer = 64
        Public Const WM_MSG_ADD_ACS_EVENT_TOLIST As Integer = 1002
        Public Const WM_MSG_GET_ACS_EVENT_FINISH As Integer = 1003
        Public Const GROUP_NAME_LEN As Integer = 32
        Public Const NET_DVR_GET_GROUP_CFG As Integer = 2112
        Public Const NET_DVR_SET_GROUP_CFG As Integer = 2113
        Public Const MAX_ALARMHOST_VIDEO_CHAN As Integer = 64
        Public Const NET_DVR_GET_DEVICECFG_V40 As Integer = 1100
        Public Const NET_DVR_SET_DEVICECFG_V40 As Integer = 1101
        Public Const CARD_READER_DESCRIPTION As Integer = 32
        Public Const NET_DVR_GET_CARD_READER_CFG_V50 As Integer = 2505
        Public Const NET_DVR_SET_CARD_READER_CFG_V50 As Integer = 2506
        Public Const MAX_FACE_NUM As Integer = 2
        Public Const NET_DVR_GET_FACE_PARAM_CFG As Integer = 2507
        Public Const NET_DVR_SET_FACE_PARAM_CFG As Integer = 2508
        Public Const NET_DVR_DEL_FACE_PARAM_CFG As Integer = 2509
        Public Const NET_DVR_CAPTURE_FACE_INFO As Integer = 2510
        Public Const MAX_FINGER_PRINT_LEN As Integer = 768
        Public Const MAX_FINGER_PRINT_NUM As Integer = 10
        Public Const ERROR_MSG_LEN As Integer = 32
        Public Const NET_SDK_EMPLOYEE_NO_LEN As Integer = 32
        Public Const NET_DVR_GET_FINGERPRINT_CFG As Integer = 2150
        Public Const NET_DVR_SET_FINGERPRINT_CFG As Integer = 2151
        Public Const NET_DVR_DEL_FINGERPRINT_CFG As Integer = 2152
        Public Const NET_DVR_GET_FINGERPRINT_CFG_V50 As Integer = 2183
        Public Const NET_DVR_SET_FINGERPRINT_CFG_V50 As Integer = 2184
        Public Const MAX_DAYS As Integer = 7
        Public Const MAX_TIMESEGMENT_V30 As Integer = 8
        Public Const HOLIDAY_GROUP_NAME_LEN As Integer = 32
        Public Const MAX_HOLIDAY_PLAN_NUM As Integer = 16
        Public Const TEMPLATE_NAME_LEN As Integer = 32
        Public Const MAX_HOLIDAY_GROUP_NUM As Integer = 16
        Public Const NET_DVR_GET_WEEK_PLAN_CFG As Integer = 2100
        Public Const NET_DVR_SET_WEEK_PLAN_CFG As Integer = 2101
        Public Const NET_DVR_GET_DOOR_STATUS_HOLIDAY_PLAN As Integer = 2102
        Public Const NET_DVR_SET_DOOR_STATUS_HOLIDAY_PLAN As Integer = 2103
        Public Const NET_DVR_GET_DOOR_STATUS_HOLIDAY_GROUP As Integer = 2104
        Public Const NET_DVR_SET_DOOR_STATUS_HOLIDAY_GROUP As Integer = 2105
        Public Const NET_DVR_GET_DOOR_STATUS_PLAN_TEMPLATE As Integer = 2106
        Public Const NET_DVR_SET_DOOR_STATUS_PLAN_TEMPLATE As Integer = 2107
        Public Const NET_DVR_GET_VERIFY_WEEK_PLAN As Integer = 2124
        Public Const NET_DVR_SET_VERIFY_WEEK_PLAN As Integer = 2125
        Public Const NET_DVR_GET_CARD_RIGHT_WEEK_PLAN As Integer = 2126
        Public Const NET_DVR_SET_CARD_RIGHT_WEEK_PLAN As Integer = 2127
        Public Const NET_DVR_GET_VERIFY_HOLIDAY_PLAN As Integer = 2128
        Public Const NET_DVR_SET_VERIFY_HOLIDAY_PLAN As Integer = 2129
        Public Const NET_DVR_GET_CARD_RIGHT_HOLIDAY_PLAN As Integer = 2130
        Public Const NET_DVR_SET_CARD_RIGHT_HOLIDAY_PLAN As Integer = 2131
        Public Const NET_DVR_GET_VERIFY_HOLIDAY_GROUP As Integer = 2132
        Public Const NET_DVR_SET_VERIFY_HOLIDAY_GROUP As Integer = 2133
        Public Const NET_DVR_GET_CARD_RIGHT_HOLIDAY_GROUP As Integer = 2134
        Public Const NET_DVR_SET_CARD_RIGHT_HOLIDAY_GROUP As Integer = 2135
        Public Const NET_DVR_GET_VERIFY_PLAN_TEMPLATE As Integer = 2136
        Public Const NET_DVR_SET_VERIFY_PLAN_TEMPLATE As Integer = 2137
        Public Const NET_DVR_GET_CARD_RIGHT_PLAN_TEMPLATE As Integer = 2138
        Public Const NET_DVR_SET_CARD_RIGHT_PLAN_TEMPLATE As Integer = 2139
        Public Const NET_DVR_GET_CARD_RIGHT_WEEK_PLAN_V50 As Integer = 2304
        Public Const NET_DVR_SET_CARD_RIGHT_WEEK_PLAN_V50 As Integer = 2305
        Public Const NET_DVR_GET_CARD_RIGHT_HOLIDAY_PLAN_V50 As Integer = 2310
        Public Const NET_DVR_SET_CARD_RIGHT_HOLIDAY_PLAN_V50 As Integer = 2311
        Public Const NET_DVR_GET_CARD_RIGHT_HOLIDAY_GROUP_V50 As Integer = 2316
        Public Const NET_DVR_SET_CARD_RIGHT_HOLIDAY_GROUP_V50 As Integer = 2317
        Public Const NET_DVR_GET_CARD_RIGHT_PLAN_TEMPLATE_V50 As Integer = 2322
        Public Const NET_DVR_SET_CARD_RIGHT_PLAN_TEMPLATE_V50 As Integer = 2323
        Public Const NET_DVR_GET_DOOR_STATUS_PLAN As Integer = 2110
        Public Const NET_DVR_SET_DOOR_STATUS_PLAN As Integer = 2111
        Public Const NET_DVR_GET_CARD_READER_PLAN As Integer = 2142
        Public Const NET_DVR_SET_CARD_READER_PLAN As Integer = 2143
        Public Const NET_DVR_GET_CARD_USERINFO_CFG As Integer = 2163
        Public Const NET_DVR_SET_CARD_USERINFO_CFG As Integer = 2164
        Public Const NET_DVR_GET_EVENT_CARD_LINKAGE_CFG_V50 As Integer = 2181
        Public Const NET_DVR_SET_EVENT_CARD_LINKAGE_CFG_V50 As Integer = 2182
        Public Const NET_DVR_DEL_FINGERPRINT_CFG_V50 As Integer = 2517
        Public Const NET_DVR_GET_EVENT_CARD_LINKAGE_CFG_V51 As Integer = 2518
        Public Const NET_DVR_SET_EVENT_CARD_LINKAGE_CFG_V51 As Integer = 2519
        Public Const NET_DVR_JSON_CONFIG As Integer = 2550
        Public Const NET_DVR_FACE_DATA_RECORD As Integer = 2551
        Public Const NET_DVR_FACE_DATA_SEARCH As Integer = 2552
        Public Const NET_DVR_FACE_DATA_MODIFY As Integer = 2553
        Public Const NET_DVR_GET_NETCFG_V30 As Integer = 1000
        Public Const NET_DVR_SET_NETCFG_V30 As Integer = 1001
        Public Const NET_DVR_GET_NETCFG_V50 As Integer = 1015
        Public Const NET_DVR_SET_NETCFG_V50 As Integer = 1016
        Public Const NET_DVR_VIDEO_CALL_SIGNAL_PROCESS As Integer = 16032
        Public Const WM_MSG_SET_FACE_PARAM_FINISH As Integer = 1002
        Public Const WM_MSG_GET_FACE_PARAM_FINISH As Integer = 1003
        Public Const WM_MSG_ADD_FACE_PARAM_TOLIST As Integer = 1004



        'face
        Public Const NET_SDK_GET_NEXT_STATUS_SUCCESS As Integer = 1000
        Public Const NET_SDK_GET_NEXT_STATUS_NEED_WAIT As Integer = 1001
        Public Const NET_SDK_GET_NEXT_STATUS_FINISH As Integer = 1002
        Public Const NET_SDK_GET_NEXT_STATUS_FAILED As Integer = 1003
        'end face
        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_DATE
            Public wYear As UShort
            Public byMonth As Byte
            Public byDay As Byte
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_SIMPLE_DAYTIME
            Public byHour As Byte
            Public byMinute As Byte
            Public bySecond As Byte
            Public byRes As Byte
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_TIME
            Public dwYear As Integer
            Public dwMonth As Integer
            Public dwDay As Integer
            Public dwHour As Integer
            Public dwMinute As Integer
            Public dwSecond As Integer
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_TIME_EX
            Public wYear As UShort
            Public byMonth As Byte
            Public byDay As Byte
            Public byHour As Byte
            Public byMinute As Byte
            Public bySecond As Byte
            Public byRes As Byte
        End Structure

        Public Enum NET_SDK_CALLBACK_TYPE
            NET_SDK_CALLBACK_TYPE_STATUS = 0
            NET_SDK_CALLBACK_TYPE_PROGRESS
            NET_SDK_CALLBACK_TYPE_DATA
        End Enum

        Public Enum NET_SDK_CALLBACK_STATUS_NORMAL
            NET_SDK_CALLBACK_STATUS_SUCCESS = 1000
            NET_SDK_CALLBACK_STATUS_PROCESSING
            NET_SDK_CALLBACK_STATUS_FAILED
            NET_SDK_CALLBACK_STATUS_EXCEPTION
            NET_SDK_CALLBACK_STATUS_LANGUAGE_MISMATCH
            NET_SDK_CALLBACK_STATUS_DEV_TYPE_MISMATCH
            NET_DVR_CALLBACK_STATUS_SEND_WAIT
        End Enum

        Public Enum LONG_CFG_SEND_DATA_TYPE_ENUM
            ENUM_DVR_VEHICLE_CHECK = 1
            ENUM_MSC_SEND_DATA = 2
            ENUM_ACS_SEND_DATA = 3
            ENUM_TME_CARD_SEND_DATA = 4
            ENUM_TME_VEHICLE_SEND_DATA = 5
            ENUM_DVR_DEBUG_CMD = 6
            ENUM_DVR_SCREEN_CTRL_CMD = 7
            ENUM_CVR_PASSBACK_SEND_DATA = 8
            ENUM_ACS_INTELLIGENT_IDENTITY_DATA = 9
            ENUM_VIDEO_INTERCOM_SEND_DATA = 10
            ENUM_SEND_JSON_DATA = 11
        End Enum

        Public Enum ENUM_UPGRADE_TYPE
            ENUM_UPGRADE_DVR = 0
            ENUM_UPGRADE_ACS = 1
        End Enum

        Public Enum NET_SDK_GET_NEXT_STATUS
            NET_SDK_GET_NEXT_STATUS_SUCCESS = 1000
            NET_SDK_GET_NETX_STATUS_NEED_WAIT
            NET_SDK_GET_NEXT_STATUS_FINISH
            NET_SDK_GET_NEXT_STATUS_FAILED
        End Enum

        Public Enum LONG_CFG_RECV_DATA_TYPE_ENUM
            ENUM_DVR_ERROR_CODE = 1
            ENUM_MSC_RECV_DATA = 2
            ENUM_ACS_RECV_DATA = 3
        End Enum

        Public Enum ACS_DEV_SUBEVENT_ENUM
            EVENT_ACS_HOST_ANTI_DISMANTLE = 0
            EVENT_ACS_OFFLINE_ECENT_NEARLY_FULL
            EVENT_ACS_NET_BROKEN
            EVENT_ACS_NET_RESUME
            EVENT_ACS_LOW_BATTERY
            EVENT_ACS_BATTERY_RESUME
            EVENT_ACS_AC_OFF
            EVENT_ACS_AC_RESUME
            EVENT_ACS_SD_CARD_FULL
            EVENT_ACS_LINKAGE_CAPTURE_PIC
            EVENT_ACS_IMAGE_QUALITY_LOW
            EVENT_ACS_FINGER_PRINT_QUALITY_LOW
            EVENT_ACS_BATTERY_ELECTRIC_LOW
            EVENT_ACS_BATTERY_ELECTRIC_RESUME
            EVENT_ACS_FIRE_IMPORT_SHORT_CIRCUIT
            EVENT_ACS_FIRE_IMPORT_BROKEN_CIRCUIT
            EVENT_ACS_FIRE_IMPORT_RESUME
            EVENT_ACS_MASTER_RS485_LOOPNODE_BROKEN
            EVENT_ACS_MASTER_RS485_LOOPNODE_RESUME
            EVENT_ACS_LOCAL_CONTROL_OFFLINE
            EVENT_ACS_LOCAL_CONTROL_RESUME
            EVENT_ACS_LOCAL_DOWNSIDE_RS485_LOOPNODE_BROKEN
            EVENT_ACS_LOCAL_DOWNSIDE_RS485_LOOPNODE_RESUME
            EVENT_ACS_DISTRACT_CONTROLLER_ONLINE
            EVENT_ACS_DISTRACT_CONTROLLER_OFFLINE
            EVENT_ACS_FIRE_BUTTON_TRIGGER
            EVENT_ACS_FIRE_BUTTON_RESUME
            EVENT_ACS_MAINTENANCE_BUTTON_TRIGGER
            EVENT_ACS_MAINTENANCE_BUTTON_RESUME
            EVENT_ACS_EMERGENCY_BUTTON_TRIGGER
            EVENT_ACS_EMERGENCY_BUTTON_RESUME
            EVENT_ACS_MAC_DETECT
        End Enum

        Public Enum ACS_ALARM_SUBEVENT_ENUM
            EVENT_ACS_ALARMIN_SHORT_CIRCUIT = 0
            EVENT_ACS_ALARMIN_BROKEN_CIRCUIT
            EVENT_ACS_ALARMIN_EXCEPTION
            EVENT_ACS_ALARMIN_RESUME
            EVENT_ACS_CASE_SENSOR_ALARM
            EVENT_ACS_CASE_SENSOR_RESUME
        End Enum

        Public Enum ACS_DOOR_SUBEVENT_ENUM
            EVENT_ACS_LEADER_CARD_OPEN_BEGIN = 0
            EVENT_ACS_LEADER_CARD_OPEN_END
            EVENT_ACS_ALWAYS_OPEN_BEGIN
            EVENT_ACS_ALWAYS_OPEN_END
            EVENT_ACS_ALWAYS_CLOSE_BEGIN
            EVENT_ACS_ALWAYS_CLOSE_END
            EVENT_ACS_LOCK_OPEN
            EVENT_ACS_LOCK_CLOSE
            EVENT_ACS_DOOR_BUTTON_PRESS
            EVENT_ACS_DOOR_BUTTON_RELEASE
            EVENT_ACS_DOOR_OPEN_NORMAL
            EVENT_ACS_DOOR_CLOSE_NORMAL
            EVENT_ACS_DOOR_OPEN_ABNORMAL
            EVENT_ACS_DOOR_OPEN_TIMEOUT
            EVENT_ACS_REMOTE_OPEN_DOOR
            EVENT_ACS_REMOTE_CLOSE_DOOR
            EVENT_ACS_REMOTE_ALWAYS_OPEN
            EVENT_ACS_REMOTE_ALWAYS_CLOSE
            EVENT_ACS_NOT_BELONG_MULTI_GROUP
            EVENT_ACS_INVALID_MULTI_VERIFY_PERIOD
            EVENT_ACS_MULTI_VERIFY_SUPER_RIGHT_FAIL
            EVENT_ACS_MULTI_VERIFY_REMOTE_RIGHT_FAIL
            EVENT_ACS_MULTI_VERIFY_SUCCESS
            EVENT_ACS_MULTI_VERIFY_NEED_REMOTE_OPEN
            EVENT_ACS_MULTI_VERIFY_SUPERPASSWD_VERIFY_SUCCESS
            EVENT_ACS_MULTI_VERIFY_REPEAT_VERIFY_FAIL
            EVENT_ACS_MULTI_VERIFY_TIMEOUT
            EVENT_ACS_REMOTE_CAPTURE_PIC
            EVENT_ACS_DOORBELL_RINGING
            EVENT_ACS_SECURITY_MODULE_DESMANTLE_ALARM
            EVENT_ACS_CALL_CENTER
            EVENT_ACS_FIRSTCARD_AUTHORIZE_BEGIN
            EVENT_ACS_FIRSTCARD_AUTHORIZE_END
            EVENT_ACS_DOORLOCK_INPUT_SHORT_CIRCUIT
            EVENT_ACS_DOORLOCK_INPUT_BROKEN_CIRCUIT
            EVENT_ACS_DOORLOCK_INPUT_EXCEPTION
            EVENT_ACS_DOORCONTACT_INPUT_SHORT_CIRCUIT
            EVENT_ACS_DOORCONTACT_INPUT_BROKEN_CIRCUIT
            EVENT_ACS_DOORCONTACT_INPUT_EXCEPTION
            EVENT_ACS_OPENBUTTON_INPUT_SHORT_CIRCUIT
            EVENT_ACS_OPENBUTTON_INPUT_BROKEN_CIRCUIT
            EVENT_ACS_OPENBUTTON_INPUT_EXCEPTION
            EVENT_ACS_DOORLOCK_OPEN_EXCEPTION
            EVENT_ACS_DOORLOCK_OPEN_TIMEOUT
            EVENT_ACS_FIRSTCARD_OPEN_WITHOUT_AUTHORIZE
            EVENT_ACS_CALL_LADDER_RELAY_BREAK
            EVENT_ACS_CALL_LADDER_RELAY_CLOSE
            EVENT_ACS_AUTO_KEY_RELAY_BREAK
            EVENT_ACS_AUTO_KEY_RELAY_CLOSE
            EVENT_ACS_KEY_CONTROL_RELAY_BREAK
            EVENT_ACS_KEY_CONTROL_RELAY_CLOSE
            EVENT_ACS_REMOTE_VISITOR_CALL_LADDER
            EVENT_ACS_REMOTE_HOUSEHOLD_CALL_LADDER
            EVENT_ACS_LEGAL_MESSAGE
            EVENT_ACS_ILLEGAL_MESSAGE
        End Enum

        Public Enum ACS_CARD_READER_SUBEVENT_ENUM
            EVENT_ACS_STRESS_ALARM = 0
            EVENT_ACS_CARD_READER_DESMANTLE_ALARM
            EVENT_ACS_LEGAL_CARD_PASS
            EVENT_ACS_CARD_AND_PSW_PASS
            EVENT_ACS_CARD_AND_PSW_FAIL
            EVENT_ACS_CARD_AND_PSW_TIMEOUT
            EVENT_ACS_CARD_MAX_AUTHENTICATE_FAIL
            EVENT_ACS_CARD_NO_RIGHT
            EVENT_ACS_CARD_INVALID_PERIOD
            EVENT_ACS_CARD_OUT_OF_DATE
            EVENT_ACS_INVALID_CARD
            EVENT_ACS_ANTI_SNEAK_FAIL
            EVENT_ACS_INTERLOCK_DOOR_NOT_CLOSE
            EVENT_ACS_FINGERPRINT_COMPARE_PASS
            EVENT_ACS_FINGERPRINT_COMPARE_FAIL
            EVENT_ACS_CARD_FINGERPRINT_VERIFY_PASS
            EVENT_ACS_CARD_FINGERPRINT_VERIFY_FAIL
            EVENT_ACS_CARD_FINGERPRINT_VERIFY_TIMEOUT
            EVENT_ACS_CARD_FINGERPRINT_PASSWD_VERIFY_PASS
            EVENT_ACS_CARD_FINGERPRINT_PASSWD_VERIFY_FAIL
            EVENT_ACS_CARD_FINGERPRINT_PASSWD_VERIFY_TIMEOUT
            EVENT_ACS_FINGERPRINT_PASSWD_VERIFY_PASS
            EVENT_ACS_FINGERPRINT_PASSWD_VERIFY_FAIL
            EVENT_ACS_FINGERPRINT_PASSWD_VERIFY_TIMEOUT
            EVENT_ACS_FINGERPRINT_INEXISTENCE
            EVENT_ACS_FACE_VERIFY_PASS
            EVENT_ACS_FACE_VERIFY_FAIL
            EVENT_ACS_FACE_AND_FP_VERIFY_PASS
            EVENT_ACS_FACE_AND_FP_VERIFY_FAIL
            EVENT_ACS_FACE_AND_FP_VERIFY_TIMEOUT
            EVENT_ACS_FACE_AND_PW_VERIFY_PASS
            EVENT_ACS_FACE_AND_PW_VERIFY_FAIL
            EVENT_ACS_FACE_AND_PW_VERIFY_TIMEOUT
            EVENT_ACS_FACE_AND_CARD_VERIFY_PASS
            EVENT_ACS_FACE_AND_CARD_VERIFY_FAIL
            EVENT_ACS_FACE_AND_CARD_VERIFY_TIMEOUT
            EVENT_ACS_FACE_AND_PW_AND_FP_VERIFY_PASS
            EVENT_ACS_FACE_AND_PW_AND_FP_VERIFY_FAIL
            EVENT_ACS_FACE_AND_PW_AND_FP_VERIFY_TIMEOUT
            EVENT_ACS_FACE_AND_CARD_AND_FP_VERIFY_PASS
            EVENT_ACS_FACE_AND_CARD_AND_FP_VERIFY_FAIL
            EVENT_ACS_FACE_AND_CARD_AND_FP_VERIFY_TIMEOUT
            EVENT_ACS_EMPLOYEENO_AND_FP_VERIFY_PASS
            EVENT_ACS_EMPLOYEENO_AND_FP_VERIFY_FAIL
            EVENT_ACS_EMPLOYEENO_AND_FP_VERIFY_TIMEOUT
            EVENT_ACS_EMPLOYEENO_AND_FP_AND_PW_VERIFY_PASS
            EVENT_ACS_EMPLOYEENO_AND_FP_AND_PW_VERIFY_FAIL
            EVENT_ACS_EMPLOYEENO_AND_FP_AND_PW_VERIFY_TIMEOUT
            EVENT_ACS_EMPLOYEENO_AND_FACE_VERIFY_PASS
            EVENT_ACS_EMPLOYEENO_AND_FACE_VERIFY_FAIL
            EVENT_ACS_EMPLOYEENO_AND_FACE_VERIFY_TIMEOUT
            EVENT_ACS_FACE_RECOGNIZE_FAIL
            EVENT_ACS_EMPLOYEENO_AND_PW_PASS
            EVENT_ACS_EMPLOYEENO_AND_PW_FAIL
            EVENT_ACS_EMPLOYEENO_AND_PW_TIMEOUT
            EVENT_ACS_HUMAN_DETECT_FAIL
        End Enum

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_XML_CONFIG_INPUT
            Public dwSize As UInteger
            Public lpRequestUrl As IntPtr
            Public dwRequestUrlLen As UInteger
            Public lpInBuffer As IntPtr
            Public dwInBufferSize As UInteger
            Public dwRecvTimeOut As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_XML_CONFIG_OUTPUT
            Public dwSize As UInteger
            Public lpOutBuffer As IntPtr
            Public dwOutBufferSize As UInteger
            Public dwReturnedXMLSize As UInteger
            Public lpStatusBuffer As IntPtr
            Public dwStatusSize As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_JSON_DATA_CFG
            Public dwSize As UInteger
            Public lpJsonData As IntPtr
            Public dwJsonDataSize As UInteger
            Public lpPicData As IntPtr
            Public dwPicDataSize As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=256)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
        Public Structure NET_DVR_LOG_V30
            Public strLogTime As NET_DVR_TIME
            Public dwMajorType As UInteger
            Public dwMinorType As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_NAMELEN, ArraySubType:=UnmanagedType.I1)>
            Public sPanelUser As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_NAMELEN, ArraySubType:=UnmanagedType.I1)>
            Public sNetUser As Byte()
            Public struRemoteHostAddr As NET_DVR_IPADDR
            Public dwParaType As UInteger
            Public dwChannel As UInteger
            Public dwDiskNumber As UInteger
            Public dwAlarmInPort As UInteger
            Public dwAlarmOutPort As UInteger
            Public dwInfoLen As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=LOG_INFO_LEN)>
            Public sInfo As String
        End Structure

        'Public Structure NET_DVR_ACS_EVENT_INFO
        '    Public dwSize As UInteger
        '    <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
        '    Public byCardNo As Byte()
        '    Public byCardType As Byte
        '    Public byWhiteListNo As Byte
        '    Public byReportChannel As Byte
        '    Public byCardReaderKind As Byte
        '    Public dwCardReaderNo As UInteger
        '    Public dwDoorNo As UInteger
        '    Public dwVerifyNo As UInteger
        '    Public dwAlarmInNo As UInteger
        '    Public dwAlarmOutNo As UInteger
        '    Public dwCaseSensorNo As UInteger
        '    Public dwRs485No As UInteger
        '    Public dwMultiCardGroupNo As UInteger
        '    Public wAccessChannel As UShort
        '    Public byDeviceNo As Byte
        '    Public byDistractControlNo As Byte
        '    Public dwEmployeeNo As UInteger
        '    Public wLocalControllerID As UShort
        '    Public byInternetAccess As Byte
        '    Public byType As Byte
        '    <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MACADDR_LEN, ArraySubType:=UnmanagedType.I1)>
        '    Public byMACAddr As Byte()
        '    Public bySwipeCardType As Byte
        '    <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=13, ArraySubType:=UnmanagedType.I1)>
        '    Public byRes As Byte()
        'End Structure

        'Public Structure NET_DVR_ACS_ALARM_INFO
        '    Public dwSize As UInteger
        '    Public dwMajor As UInteger
        '    Public dwMinor As UInteger
        '    Public struTime As NET_DVR_TIME
        '    <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_NAMELEN, ArraySubType:=UnmanagedType.I1)>
        '    Public sNetUser As Byte()
        '    Public struRemoteHostAddr As NET_DVR_IPADDR
        '    Public struAcsEventInfo As NET_DVR_ACS_EVENT_INFO
        '    Public dwPicDataLen As UInteger
        '    Public pPicData As IntPtr
        '    <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=24, ArraySubType:=UnmanagedType.I1)>
        '    Public byRes As Byte()
        'End Structure

        <StructLayoutAttribute(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
        Public Structure NET_DVR_ALARMER
            Public byUserIDValid As Byte
            Public bySerialValid As Byte
            Public byVersionValid As Byte
            Public byDeviceNameValid As Byte
            Public byMacAddrValid As Byte
            Public byLinkPortValid As Byte
            Public byDeviceIPValid As Byte
            Public bySocketIPValid As Byte
            Public lUserID As Integer
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=SERIALNO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public sSerialNumber As Byte()
            Public dwDeviceVersion As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=NAME_LEN)>
            Public sDeviceName As String
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MACADDR_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byMacAddr As Byte()
            Public wLinkPort As UShort
            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=128)>
            Public sDeviceIP As String
            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=128)>
            Public sSocketIP As String
            Public byIpProtocol As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=11, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_SETUPALARM_PARAM
            Public dwSize As UInteger
            Public byLevel As Byte
            Public byAlarmInfoType As Byte
            Public byRetAlarmTypeV40 As Byte
            Public byRetDevInfoVersion As Byte
            Public byRetVQDAlarmType As Byte
            Public byFaceAlarmDetection As Byte
            Public bySupport As Byte
            Public byBrokenNetHttp As Byte
            Public wTaskNo As UShort
            Public byDeployType As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            Public byAlarmTypeURL As Byte
            Public byCustomCtrl As Byte
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_VALID_PERIOD_CFG
            Public byEnable As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            Public struBeginTime As NET_DVR_TIME_EX
            Public struEndTime As NET_DVR_TIME_EX
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_CARD_CFG_V50
            Public dwSize As UInteger
            Public dwModifyParamType As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            Public byCardValid As Byte
            Public byCardType As Byte
            Public byLeaderCard As Byte
            Public byRes1 As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)>
            Public byDoorRight As Byte()
            Public struValid As NET_DVR_VALID_PERIOD_CFG
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_GROUP_NUM_128, ArraySubType:=UnmanagedType.I1)>
            Public byBelongGroup As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=CARD_PASSWORD_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardPassword As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256 * MAX_CARD_RIGHT_PLAN_NUM, ArraySubType:=UnmanagedType.I1)>
            Public wCardRightPlan As UShort()
            Public dwMaxSwipeTime As UInteger
            Public dwSwipeTime As UInteger
            Public wRoomNumber As UShort
            Public wFloorNumber As Short
            Public dwEmployeeNo As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byName As Byte()
            Public wDepartmentNo As UShort
            Public wSchedulePlanNo As UShort
            Public bySchedulePlanType As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()
            Public dwLockID As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_LOCK_CODE_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byLockCode As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_CODE_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byRoomCode As Byte()
            Public dwCardRight As UInteger
            Public dwPlanTemplate As UInteger
            Public dwCardUserId As UInteger
            Public byCardModelType As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=83, ArraySubType:=UnmanagedType.I1)>
            Public byRes3 As Byte()

            Public Sub Init()
                byDoorRight = New Byte(255) {}
                byBelongGroup = New Byte(127) {}
                wCardRightPlan = New UShort(1023) {}
                byCardNo = New Byte(31) {}
                byCardPassword = New Byte(7) {}
                byName = New Byte(31) {}
                byRes2 = New Byte(2) {}
                byLockCode = New Byte(7) {}
                byRoomCode = New Byte(7) {}
                byRes3 = New Byte(82) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_CARD_CFG_COND
            Public dwSize As UInteger
            Public dwCardNum As UInteger
            Public byCheckCardNo As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            Public wLocalControllerID As UShort
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()
            Public dwLockID As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=20, ArraySubType:=UnmanagedType.I1)>
            Public byRes3 As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_CARD_CFG_SEND_DATA
            Public dwSize As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            Public dwCardUserId As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=12, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FACE_PARAM_COND
            Public dwSize As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byEnableCardReader As Byte()
            Public dwFaceNum As UInteger
            Public byFaceID As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=127, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byCardNo = New Byte(31) {}
                byEnableCardReader = New Byte(511) {}
                byRes = New Byte(126) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FACE_PARAM_CFG
            Public dwSize As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            Public dwFaceLen As UInteger
            Public pFaceBuffer As IntPtr
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byEnableCardReader As Byte()
            Public byFaceID As Byte
            Public byFaceDataType As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=126, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byCardNo = New Byte(31) {}
                byEnableCardReader = New Byte(511) {}
                byRes = New Byte(125) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FACE_PARAM_STATUS
            Public dwSize As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byCardReaderRecvStatus As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ERROR_MSG_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byErrorMsg As Byte()
            Public dwCardReaderNo As UInteger
            Public byTotalStatus As Byte
            Public byFaceID As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=130, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
        End Structure

        '<StructLayoutAttribute(LayoutKind.Sequential)>
        'Public Structure NET_DVR_CAPTURE_FACE_COND
        '    Public dwSize As UInteger
        '    <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128, ArraySubType:=UnmanagedType.I1)>
        '    Public byRes As Byte()
        'End Structure

        '<StructLayoutAttribute(LayoutKind.Sequential)>
        'Public Structure NET_DVR_CAPTURE_FACE_CFG
        '    Public dwSize As UInteger
        '    Public dwFaceTemplate1Size As UInteger
        '    Public pFaceTemplate1Buffer As IntPtr
        '    Public dwFaceTemplate2Size As UInteger
        '    Public pFaceTemplate2Buffer As IntPtr
        '    Public dwFacePicSize As UInteger
        '    Public pFacePicBuffer As IntPtr
        '    Public byFaceQuality1 As Byte
        '    Public byFaceQuality2 As Byte
        '    Public byCaptureProgress As Byte
        '    <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=125, ArraySubType:=UnmanagedType.I1)>
        '    Public byRes As Byte()
        'End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_ACS_PARAM_TYPE
            Public dwSize As UInteger
            Public dwParamType As UInteger
            Public wLocalControllerID As UShort
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=30)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_DOOR_CFG
            Public dwSize As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=DOOR_NAME_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byDoorName As Byte()
            Public byMagneticType As Byte
            Public byOpenButtonType As Byte
            Public byOpenDuration As Byte
            Public byDisabledOpenDuration As Byte
            Public byMagneticAlarmTimeout As Byte
            Public byEnableDoorLock As Byte
            Public byEnableLeaderCard As Byte
            Public byLeaderCardMode As Byte
            Public dwLeaderCardOpenDuration As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=STRESS_PASSWORD_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byStressPassword As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=SUPER_PASSWORD_LEN, ArraySubType:=UnmanagedType.I1)>
            Public bySuperPassword As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=UNLOCK_PASSWORD_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byUnlockPassword As Byte()
            Public byUseLocalController As Byte
            Public byRes1 As Byte
            Public wLocalControllerID As UShort
            Public wLocalControllerDoorNumber As UShort
            Public wLocalControllerStatus As UShort
            Public byLockInputCheck As Byte
            Public byLockInputType As Byte
            Public byDoorTerminalMode As Byte
            Public byOpenButton As Byte
            Public byLadderControlDelayTime As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=43, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()

            Public Sub Init()
                byDoorName = New Byte(31) {}
                byStressPassword = New Byte(7) {}
                bySuperPassword = New Byte(7) {}
                byUnlockPassword = New Byte(7) {}
                byRes2 = New Byte(42) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_GROUP_CFG
            Public dwSize As UInteger
            Public byEnable As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            Public struValidPeriodCfg As NET_DVR_VALID_PERIOD_CFG
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=GROUP_NAME_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byGroupName As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_ALARM_DEVICE_USER
            Public dwSize As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)>
            Public sUserName As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=PASSWD_LEN, ArraySubType:=UnmanagedType.I1)>
            Public sPassword As Byte()
            Public struUserIP As NET_DVR_IPADDR
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MACADDR_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byAMCAddr As Byte()
            Public byUserType As Byte
            Public byAlarmOnRight As Byte
            Public byAlarmOffRight As Byte
            Public byBypassRight As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_RIGHT, ArraySubType:=UnmanagedType.I1)>
            Public byOtherRight As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_VIDEO_CHAN / 8, ArraySubType:=UnmanagedType.I1)>
            Public byNetPreviewRight As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_VIDEO_CHAN / 8, ArraySubType:=UnmanagedType.I1)>
            Public byNetRecordRight As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_VIDEO_CHAN / 8, ArraySubType:=UnmanagedType.I1)>
            Public byNetPlaybackRight As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_VIDEO_CHAN / 8, ArraySubType:=UnmanagedType.I1)>
            Public byNetPTZRight As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=PASSWD_LEN, ArraySubType:=UnmanagedType.I1)>
            Public sOriginalPassword As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=152, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_CARD_READER_CFG_V50
            Public dwSize As UInteger
            Public byEnable As Byte
            Public byCardReaderType As Byte
            Public byOkLedPolarity As Byte
            Public byErrorLedPolarity As Byte
            Public byBuzzerPolarity As Byte
            Public bySwipeInterval As Byte
            Public byPressTimeout As Byte
            Public byEnableFailAlarm As Byte
            Public byMaxReadCardFailNum As Byte
            Public byEnableTamperCheck As Byte
            Public byOfflineCheckTime As Byte
            Public byFingerPrintCheckLevel As Byte
            Public byUseLocalController As Byte
            Public byRes1 As Byte
            Public wLocalControllerID As UShort
            Public wLocalControllerReaderID As UShort
            Public wCardReaderChannel As UShort
            Public byFingerPrintImageQuality As Byte
            Public byFingerPrintContrastTimeOut As Byte
            Public byFingerPrintRecogizeInterval As Byte
            Public byFingerPrintMatchFastMode As Byte
            Public byFingerPrintModuleSensitive As Byte
            Public byFingerPrintModuleLightCondition As Byte
            Public byFaceMatchThresholdN As Byte
            Public byFaceQuality As Byte
            Public byFaceRecogizeTimeOut As Byte
            Public byFaceRecogizeInterval As Byte
            Public wCardReaderFunction As UShort
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CARD_READER_DESCRIPTION, ArraySubType:=UnmanagedType.I1)>
            Public byCardReaderDescription As Byte()
            Public wFaceImageSensitometry As UShort
            Public byLivingBodyDetect As Byte
            Public byFaceMatchThreshold1 As Byte
            Public wBuzzerTime As UShort
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=254, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byCardReaderDescription = New Byte(31) {}
                byRes = New Byte(253) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FINGER_PRINT_CFG
            Public dwSize As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            Public dwFingerPrintLen As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byEnableCardReader As Byte()
            Public byFingerPrintID As Byte
            Public byFingerType As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=30, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_FINGER_PRINT_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byFingerData As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byCardNo = New Byte(31) {}
                byEnableCardReader = New Byte(511) {}
                byRes1 = New Byte(29) {}
                byFingerData = New Byte(767) {}
                byRes = New Byte(63) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FINGER_PRINT_STATUS
            Public dwSize As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byCardReaderRecvStatus As Byte()
            Public byFingerPrintID As Byte
            Public byFingerType As Byte
            Public byTotalStatus As Byte
            Public byRes1 As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ERROR_MSG_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byErrorMsg As Byte()
            Public dwCardReaderNo As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=24, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byCardNo = New Byte(31) {}
                byCardReaderRecvStatus = New Byte(511) {}
                byErrorMsg = New Byte(31) {}
                byRes = New Byte(23) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FINGER_PRINT_INFO_COND
            Public dwSize As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byEnableCardReader As Byte()
            Public dwFingerPrintNum As UInteger
            Public byFingerPrintID As Byte
            Public byCallbackMode As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=26, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()

            Public Sub Init()
                byCardNo = New Byte(31) {}
                byEnableCardReader = New Byte(511) {}
                byRes1 = New Byte(25) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FINGER_PRINT_CFG_V50
            Public dwSize As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            Public dwFingerPrintLen As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byEnableCardReader As Byte()
            Public byFingerPrintID As Byte
            Public byFingerType As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=30, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_FINGER_PRINT_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byFingerData As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NET_SDK_EMPLOYEE_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byEmployeeNo As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)>
            Public byLeaderFP As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=128, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byCardNo = New Byte(31) {}
                byEnableCardReader = New Byte(511) {}
                byRes1 = New Byte(29) {}
                byFingerData = New Byte(767) {}
                byEmployeeNo = New Byte(31) {}
                byLeaderFP = New Byte(255) {}
                byRes = New Byte(127) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FINGER_PRINT_STATUS_V50
            Public dwSize As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byCardReaderRecvStatus As Byte()
            Public byFingerPrintID As Byte
            Public byFingerType As Byte
            Public byTotalStatus As Byte
            Public byRecvStatus As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ERROR_MSG_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byErrorMsg As Byte()
            Public dwCardReaderNo As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NET_SDK_EMPLOYEE_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byEmployeeNo As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NET_SDK_EMPLOYEE_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byErrorEmployeeNo As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=128, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byCardNo = New Byte(31) {}
                byCardReaderRecvStatus = New Byte(511) {}
                byErrorMsg = New Byte(31) {}
                byEmployeeNo = New Byte(31) {}
                byErrorEmployeeNo = New Byte(31) {}
                byRes = New Byte(127) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FINGER_PRINT_INFO_COND_V50
            Public dwSize As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byEnableCardReader As Byte()
            Public dwFingerPrintNum As UInteger
            Public byFingerPrintID As Byte
            Public byCallbackMode As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NET_SDK_EMPLOYEE_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byEmployeeNo As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=128, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()

            Public Sub Init()
                byCardNo = New Byte(31) {}
                byEnableCardReader = New Byte(511) {}
                byRes2 = New Byte(1) {}
                byEmployeeNo = New Byte(31) {}
                byRes1 = New Byte(127) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FINGER_PRINT_BYCARD
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byEnableCardReader As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_FINGER_PRINT_NUM, ArraySubType:=UnmanagedType.I1)>
            Public byFingerPrintID As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=34, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FINGER_PRINT_BYREADER
            Public dwCardReaderNo As UInteger
            Public byClearAllCard As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=548, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FINGER_PRINT_INFO_CTRL_BYCARD
            Public dwSize As UInteger
            Public byMode As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)>
            Public byRes1 As Byte()
            Public struByCard As NET_DVR_FINGER_PRINT_BYCARD
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FINGER_PRINT_INFO_CTRL_BYREADER
            Public dwSize As UInteger
            Public byMode As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)>
            Public byRes1 As Byte()
            Public struByReader As NET_DVR_FINGER_PRINT_BYREADER
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FINGER_PRINT_BYCARD_V50
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byEnableCardReader As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_FINGER_PRINT_NUM, ArraySubType:=UnmanagedType.I1)>
            Public byFingerPrintID As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NET_SDK_EMPLOYEE_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byEmployeeNo As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FINGER_PRINT_BYREADER_V50
            Public dwCardReaderNo As UInteger
            Public byClearAllCard As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NET_SDK_EMPLOYEE_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byEmployeeNo As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=516, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FINGER_PRINT_INFO_CTRL_BYCARD_V50
            Public dwSize As UInteger
            Public byMode As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)>
            Public byRes1 As Byte()
            Public struByCard As NET_DVR_FINGER_PRINT_BYCARD_V50
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FINGER_PRINT_INFO_CTRL_BYREADER_V50
            Public dwSize As UInteger
            Public byMode As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)>
            Public byRes1 As Byte()
            Public struByReader As NET_DVR_FINGER_PRINT_BYREADER_V50
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FINGER_PRINT_INFO_STATUS_V50
            Public dwSize As UInteger
            Public dwCardReaderNo As UInteger
            Public byStatus As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=63)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FAILED_FACE_INFO
            Public dwSize As Integer
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN)>
            Public byCardNo As Byte()
            Public byErrorCode As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=127)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FACE_PARAM_CTRL_ByCard
            Public dwSize As Integer
            Public byMode As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            Public struProcessMode As NET_DVR_FACE_PARAM_BYCARD
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byRes1 = New Byte(2) {}
                byRes = New Byte(63) {}
                struProcessMode = New NET_DVR_FACE_PARAM_BYCARD()
                struProcessMode.Init()
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FACE_PARAM_CTRL_ByReader
            Public dwSize As Integer
            Public byMode As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            Public struProcessMode As NET_DVR_FACE_PARAM_BYREADER
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byRes1 = New Byte(2) {}
                byRes = New Byte(63) {}
                struProcessMode = New NET_DVR_FACE_PARAM_BYREADER()
                struProcessMode.Init()
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FACE_PARAM_BYCARD
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byEnableCardReader As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.MAX_FACE_NUM, ArraySubType:=UnmanagedType.I1)>
            Public byFaceID As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=42, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()

            Public Sub Init()
                byCardNo = New Byte(31) {}
                byEnableCardReader = New Byte(511) {}
                byFaceID = New Byte(1) {}
                byRes1 = New Byte(41) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_FACE_PARAM_BYREADER
            Public dwCardReaderNo As Integer
            Public byClearAllCard As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=548, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byRes1 = New Byte(2) {}
                byCardNo = New Byte(31) {}
                byRes = New Byte(547) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_TIME_SEGMENT
            Public struBeginTime As NET_DVR_SIMPLE_DAYTIME
            Public struEndTime As NET_DVR_SIMPLE_DAYTIME
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_SINGLE_PLAN_SEGMENT
            Public byEnable As Byte
            Public byDoorStatus As Byte
            Public byVerifyMode As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=5, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
            Public struTimeSegment As NET_DVR_TIME_SEGMENT

            Public Sub Init()
                byRes = New Byte(4) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_WEEK_PLAN_CFG
            Public dwSize As UInteger
            Public byEnable As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_DAYS * MAX_TIMESEGMENT_V30, ArraySubType:=UnmanagedType.Struct)>
            Public struPlanCfg As NET_DVR_SINGLE_PLAN_SEGMENT()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()

            Public Sub Init()
                struPlanCfg = New NET_DVR_SINGLE_PLAN_SEGMENT(55) {}

                For Each singlStruPlanCfg As NET_DVR_SINGLE_PLAN_SEGMENT In struPlanCfg
                    singlStruPlanCfg.Init()
                Next

                byRes1 = New Byte(2) {}
                byRes2 = New Byte(15) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_HOLIDAY_PLAN_CFG
            Public dwSize As UInteger
            Public byEnable As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            Public struBeginDate As NET_DVR_DATE
            Public struEndDate As NET_DVR_DATE
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_TIMESEGMENT_V30, ArraySubType:=UnmanagedType.Struct)>
            Public struPlanCfg As NET_DVR_SINGLE_PLAN_SEGMENT()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)>
            Public byRes2 As Byte()

            Public Sub Init()
                struPlanCfg = New NET_DVR_SINGLE_PLAN_SEGMENT(7) {}

                For Each singlStruPlanCfg As NET_DVR_SINGLE_PLAN_SEGMENT In struPlanCfg
                    singlStruPlanCfg.Init()
                Next

                byRes1 = New Byte(2) {}
                byRes2 = New Byte(15) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_HOLIDAY_GROUP_CFG
            Public dwSize As UInteger
            Public byEnable As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=HOLIDAY_GROUP_NAME_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byGroupName As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_HOLIDAY_PLAN_NUM, ArraySubType:=UnmanagedType.U4)>
            Public dwHolidayPlanNo As UInteger()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()

            Public Sub Init()
                byGroupName = New Byte(31) {}
                dwHolidayPlanNo = New UInteger(15) {}
                byRes1 = New Byte(2) {}
                byRes2 = New Byte(31) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_PLAN_TEMPLATE
            Public dwSize As UInteger
            Public byEnable As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=TEMPLATE_NAME_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byTemplateName As Byte()
            Public dwWeekPlanNo As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_HOLIDAY_GROUP_NUM, ArraySubType:=UnmanagedType.U4)>
            Public dwHolidayGroupNo As UInteger()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()

            Public Sub Init()
                byTemplateName = New Byte(31) {}
                dwHolidayGroupNo = New UInteger(15) {}
                byRes1 = New Byte(2) {}
                byRes2 = New Byte(31) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_HOLIDAY_PLAN_COND
            Public dwSize As UInteger
            Public dwHolidayPlanNumber As UInteger
            Public wLocalControllerID As UShort
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=106, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byRes = New Byte(105) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_WEEK_PLAN_COND
            Public dwSize As UInteger
            Public dwWeekPlanNumber As UInteger
            Public wLocalControllerID As UShort
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=106, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byRes = New Byte(105) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_HOLIDAY_GROUP_COND
            Public dwSize As UInteger
            Public dwHolidayGroupNumber As UInteger
            Public wLocalControllerID As UShort
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=106, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byRes = New Byte(105) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_PLAN_TEMPLATE_COND
            Public dwSize As UInteger
            Public dwPlanTemplateNumber As UInteger
            Public wLocalControllerID As UShort
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=106, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byRes = New Byte(105) {}
            End Sub
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_DOOR_STATUS_PLAN
            Public dwSize As UInteger
            Public dwTemplateNo As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)>
            Public byRes As Byte()

            Public Sub Init()
                byRes = New Byte(63) {}
            End Sub
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_CARD_READER_PLAN
            Public dwSize As UInteger
            Public dwTemplateNo As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)>
            Public byRes As Byte()

            Public Sub Init()
                byRes = New Byte(63) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_CARD_USER_INFO_CFG
            Public dwSize As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN)>
            Public byUsername As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=256)>
            Public byRes2 As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_DEVICEINFO_V30
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=SERIALNO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public sSerialNumber As Byte()
            Public byAlarmInPortNum As Byte
            Public byAlarmOutPortNum As Byte
            Public byDiskNum As Byte
            Public byDVRType As Byte
            Public byChanNum As Byte
            Public byStartChan As Byte
            Public byAudioChanNum As Byte
            Public byIPChanNum As Byte
            Public byZeroChanNum As Byte
            Public byMainProto As Byte
            Public bySubProto As Byte
            Public bySupport As Byte
            Public bySupport1 As Byte
            Public bySupport2 As Byte
            Public wDevType As UShort
            Public bySupport3 As Byte
            Public byMultiStreamProto As Byte
            Public byStartDChan As Byte
            Public byStartDTalkChan As Byte
            Public byHighDChanNum As Byte
            Public bySupport4 As Byte
            Public byLanguageType As Byte
            Public byVoiceInChanNum As Byte
            Public byStartVoiceInChanNo As Byte
            Public bySupport5 As Byte
            Public bySupport6 As Byte
            Public byMirrorChanNum As Byte
            Public wStartMirrorChanNo As UShort
            Public bySupport7 As Byte
            Public byRes2 As Byte
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_DEVICEINFO_V40
            Public struDeviceV30 As NET_DVR_DEVICEINFO_V30
            Public bySupportLock As Byte
            Public byRetryLoginTime As Byte
            Public byPasswordLevel As Byte
            Public byProxyType As Byte
            Public dwSurplusLockTime As UInteger
            Public byCharEncodeType As Byte
            Public byRes1 As Byte
            Public bySupport As Byte
            Public byRes As Byte
            Public dwOEMCode As UInteger
            Public bySupportDev5 As Byte
            Public byLoginMode As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=246)>
            Public byRes2 As Byte()
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_DEVICECFG_V40
            Public dwSize As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN)>
            Public sDVRName As Byte()
            Public dwDVRID As UInteger
            Public dwRecycleRecord As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=SERIALNO_LEN)>
            Public sSerialNumber As Byte()
            Public dwSoftwareVersion As UInteger
            Public dwSoftwareBuildDate As UInteger
            Public dwDSPSoftwareVersion As UInteger
            Public dwDSPSoftwareBuildDate As UInteger
            Public dwPanelVersion As UInteger
            Public dwHardwareVersion As UInteger
            Public byAlarmInPortNum As Byte
            Public byAlarmOutPortNum As Byte
            Public byRS232Num As Byte
            Public byRS485Num As Byte
            Public byNetworkPortNum As Byte
            Public byDiskCtrlNum As Byte
            Public byDiskNum As Byte
            Public byDVRType As Byte
            Public byChanNum As Byte
            Public byStartChan As Byte
            Public byDecordChans As Byte
            Public byVGANum As Byte
            Public byUSBNum As Byte
            Public byAuxoutNum As Byte
            Public byAudioNum As Byte
            Public byIPChanNum As Byte
            Public byZeroChanNum As Byte
            Public bySupport As Byte
            Public byEsataUseage As Byte
            Public byIPCPlug As Byte
            Public byStorageMode As Byte
            Public bySupport1 As Byte
            Public wDevType As UShort
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=DEV_TYPE_NAME_LEN)>
            Public byDevTypeName As Byte()
            Public bySupport2 As Byte
            Public byAnalogAlarmInPortNum As Byte
            Public byStartAlarmInNo As Byte
            Public byStartAlarmOutNo As Byte
            Public byStartIPAlarmInNo As Byte
            Public byStartIPAlarmOutNo As Byte
            Public byHighIPChanNum As Byte
            Public byEnableRemotePowerOn As Byte
            Public wDevClass As UShort
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=6)>
            Public byRes2 As Byte()
        End Structure

        Public Delegate Sub LoginResultCallBack(ByVal lUserID As Integer, ByVal dwResult As UInteger, ByRef lpDeviceInfo As NET_DVR_DEVICEINFO_V30, ByVal pUser As IntPtr)

        <StructLayoutAttribute(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
        Public Structure NET_DVR_USER_LOGIN_INFO
            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=NET_DVR_DEV_ADDRESS_MAX_LEN)>
            Public sDeviceAddress As String
            Public byUseTransport As Byte
            Public wPort As UShort
            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=NET_DVR_LOGIN_USERNAME_MAX_LEN)>
            Public sUserName As String
            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=NET_DVR_LOGIN_PASSWD_MAX_LEN)>
            Public sPassword As String
            Public cbLoginResult As LoginResultCallBack
            Public pUser As IntPtr
            Public bUseAsynLogin As Boolean
            Public byProxyType As Byte
            Public byUseUTCTime As Byte
            Public byLoginMode As Byte
            Public byHttps As Byte
            Public iProxyID As Integer
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=120, ArraySubType:=UnmanagedType.I1)>
            Public byRes3 As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_PREVIEWINFO
            Public lChannel As Integer
            Public dwStreamType As UInteger
            Public dwLinkMode As UInteger
            Public hPlayWnd As IntPtr
            Public bBlocked As UInteger
            Public bPassbackRecord As UInteger
            Public byPreviewMode As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=STREAM_ID_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byStreamID As Byte()
            Public byProtoType As Byte
            Public byRes1 As Byte
            Public byVideoCodingType As Byte
            Public dwDisplayBufNum As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=216, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
        Public Structure NET_DVR_IPADDR
            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=16)>
            Public sIpV4 As String
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128, ArraySubType:=UnmanagedType.I1)>
            Public byIPv6 As Byte()

            Public Sub Init()
                byIPv6 = New Byte(127) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_ETHERNET_V30
            Public struDVRIP As NET_DVR_IPADDR
            Public struDVRIPMask As NET_DVR_IPADDR
            Public dwNetInterface As UInteger
            Public wDVRPort As UShort
            Public wMTU As UShort
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MACADDR_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byMACAddr As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
        Public Structure NET_DVR_PPPOECFG
            Public dwPPPOE As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)>
            Public sPPPoEUser As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=PASSWD_LEN)>
            Public sPPPoEPassword As String
            Public struPPPoEIP As NET_DVR_IPADDR
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_NETCFG_V30
            Public dwSize As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ETHERNET, ArraySubType:=UnmanagedType.Struct)>
            Public struEtherNet As NET_DVR_ETHERNET_V30()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.Struct)>
            Public struRes1 As NET_DVR_IPADDR()
            Public struAlarmHostIpAddr As NET_DVR_IPADDR
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.U2)>
            Public wRes2 As UShort()
            Public wAlarmHostIpPort As UShort
            Public byUseDhcp As Byte
            Public byIPv6Mode As Byte
            Public struDnsServer1IpAddr As NET_DVR_IPADDR
            Public struDnsServer2IpAddr As NET_DVR_IPADDR
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOMAIN_NAME, ArraySubType:=UnmanagedType.I1)>
            Public byIpResolver As Byte()
            Public wIpResolverPort As UShort
            Public wHttpPortNo As UShort
            Public struMulticastIpAddr As NET_DVR_IPADDR
            Public struGatewayIpAddr As NET_DVR_IPADDR
            Public struPPPoE As NET_DVR_PPPOECFG
            Public byEnablePrivateMulticastDiscovery As Byte
            Public byEnableOnvifMulticastDiscovery As Byte
            Public byEnableDNS As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=61, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                struEtherNet = New NET_DVR_ETHERNET_V30(1) {}
                struAlarmHostIpAddr = New NET_DVR_IPADDR()
                struDnsServer1IpAddr = New NET_DVR_IPADDR()
                struDnsServer2IpAddr = New NET_DVR_IPADDR()
                byIpResolver = New Byte(63) {}
                struMulticastIpAddr = New NET_DVR_IPADDR()
                struGatewayIpAddr = New NET_DVR_IPADDR()
                struPPPoE = New NET_DVR_PPPOECFG()
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_NETCFG_V50
            Public dwSize As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ETHERNET, ArraySubType:=UnmanagedType.Struct)>
            Public struEtherNet As NET_DVR_ETHERNET_V30()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.Struct)>
            Public struRes1 As NET_DVR_IPADDR()
            Public struAlarmHostIpAddr As NET_DVR_IPADDR
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()
            Public wAlarmHostIpPort As UShort
            Public byUseDhcp As Byte
            Public byIPv6Mode As Byte
            Public struDnsServer1IpAddr As NET_DVR_IPADDR
            Public struDnsServer2IpAddr As NET_DVR_IPADDR
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOMAIN_NAME, ArraySubType:=UnmanagedType.I1)>
            Public byIpResolver As Byte()
            Public wIpResolverPort As UShort
            Public wHttpPortNo As UShort
            Public struMulticastIpAddr As NET_DVR_IPADDR
            Public struGatewayIpAddr As NET_DVR_IPADDR
            Public struPPPoE As NET_DVR_PPPOECFG
            Public byEnablePrivateMulticastDiscovery As Byte
            Public byEnableOnvifMulticastDiscovery As Byte
            Public wAlarmHost2IpPort As UShort
            Public struAlarmHost2IpAddr As NET_DVR_IPADDR
            Public byEnableDNS As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=599, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                struEtherNet = New NET_DVR_ETHERNET_V30(1) {}
                struRes1 = New NET_DVR_IPADDR(1) {}
                struAlarmHostIpAddr = New NET_DVR_IPADDR()
                struAlarmHost2IpAddr = New NET_DVR_IPADDR()
                struDnsServer1IpAddr = New NET_DVR_IPADDR()
                struDnsServer2IpAddr = New NET_DVR_IPADDR()
                byIpResolver = New Byte(63) {}
                struMulticastIpAddr = New NET_DVR_IPADDR()
                struGatewayIpAddr = New NET_DVR_IPADDR()
                struPPPoE = New NET_DVR_PPPOECFG()
                byRes = New Byte(598) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_IPDEVINFO_V31
            Public byEnable As Byte
            Public byProType As Byte
            Public byEnableQuickAdd As Byte
            Public byRes1 As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)>
            Public sUserName As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=PASSWD_LEN, ArraySubType:=UnmanagedType.I1)>
            Public sPassword As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOMAIN_NAME, ArraySubType:=UnmanagedType.I1)>
            Public byDomain As Byte()
            Public struIP As NET_DVR_IPADDR
            Public wDVRPort As UShort
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=DEV_ID_LEN, ArraySubType:=UnmanagedType.I1)>
            Public szDeviceID As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()

            Public Sub Init()
                sUserName = New Byte(31) {}
                sPassword = New Byte(15) {}
                byDomain = New Byte(63) {}
                szDeviceID = New Byte(31) {}
                byRes2 = New Byte(1) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
        Public Structure NET_DVR_EVENT_CARD_LINKAGE_COND
            Public dwSize As UInteger
            Public dwEventID As UInteger
            Public wLocalControllerID As UShort
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=106, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_EVENT_LINKAGE_INFO
            Public wMainEventType As UShort
            Public wSubEventType As UShort
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Explicit)>
        Public Structure NET_DVR_EVETN_CARD_LINKAGE_UNION
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            <FieldOffsetAttribute(0)>
            Public byCardNo As Byte()
            <FieldOffsetAttribute(0)>
            Public struEventLinkage As NET_DVR_EVENT_LINKAGE_INFO
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MACADDR_LEN, ArraySubType:=UnmanagedType.I1)>
            <FieldOffsetAttribute(0)>
            Public byMACAddr As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=NET_SDK_EMPLOYEE_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            <FieldOffsetAttribute(0)>
            Public byEmployeeNo As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_EVENT_CARD_LINKAGE_CFG_V50
            Public dwSize As UInteger
            Public byProMode As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            Public dwEventSourceID As UInteger
            Public uLinkageInfo As NET_DVR_EVETN_CARD_LINKAGE_UNION
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMOUT_NUM, ArraySubType:=UnmanagedType.I1)>
            Public byAlarmout As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)>
            Public byOpenDoor As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)>
            Public byCloseDoor As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)>
            Public byNormalOpen As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)>
            Public byNormalClose As Byte()
            Public byMainDevBuzzer As Byte
            Public byCapturePic As Byte
            Public byRecordVideo As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=29, ArraySubType:=UnmanagedType.I1)>
            Public byRes3 As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byReaderBuzzer As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMOUT_NUM, ArraySubType:=UnmanagedType.I1)>
            Public byAlarmOutClose As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMIN_NUM, ArraySubType:=UnmanagedType.I1)>
            Public byAlarmInSetup As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMIN_NUM, ArraySubType:=UnmanagedType.I1)>
            Public byAlarmInClose As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=500, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byRes1 = New Byte(2) {}
                byAlarmout = New Byte(511) {}
                byOpenDoor = New Byte(255) {}
                byCloseDoor = New Byte(255) {}
                byNormalOpen = New Byte(255) {}
                byNormalClose = New Byte(255) {}
                byRes3 = New Byte(28) {}
                byReaderBuzzer = New Byte(511) {}
                byAlarmOutClose = New Byte(511) {}
                byAlarmInSetup = New Byte(511) {}
                byAlarmInClose = New Byte(511) {}
                byRes = New Byte(499) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_EVENT_CARD_LINKAGE_CFG_V51
            Public dwSize As UInteger
            Public byProMode As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            Public dwEventSourceID As UInteger
            Public uLinkageInfo As NET_DVR_EVETN_CARD_LINKAGE_UNION
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMOUT_NUM, ArraySubType:=UnmanagedType.I1)>
            Public byAlarmout As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)>
            Public byOpenDoor As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)>
            Public byCloseDoor As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)>
            Public byNormalOpen As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)>
            Public byNormalClose As Byte()
            Public byMainDevBuzzer As Byte
            Public byCapturePic As Byte
            Public byRecordVideo As Byte
            Public byMainDevStopBuzzer As Byte
            Public wAudioDisplayID As UShort
            Public byAudioDisplayMode As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=25, ArraySubType:=UnmanagedType.I1)>
            Public byRes3 As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byReaderBuzzer As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMOUT_NUM, ArraySubType:=UnmanagedType.I1)>
            Public byAlarmOutClose As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMIN_NUM, ArraySubType:=UnmanagedType.I1)>
            Public byAlarmInSetup As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMIN_NUM, ArraySubType:=UnmanagedType.I1)>
            Public byAlarmInClose As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byReaderStopBuzzer As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=512, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byRes1 = New Byte(2) {}
                byAlarmout = New Byte(511) {}
                byRes2 = New Byte(31) {}
                byOpenDoor = New Byte(255) {}
                byCloseDoor = New Byte(255) {}
                byNormalOpen = New Byte(255) {}
                byNormalClose = New Byte(255) {}
                byRes3 = New Byte(24) {}
                byReaderBuzzer = New Byte(511) {}
                byAlarmOutClose = New Byte(511) {}
                byAlarmInSetup = New Byte(511) {}
                byAlarmInClose = New Byte(511) {}
                byReaderStopBuzzer = New Byte(511) {}
                byRes = New Byte(511) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_IPALARMOUTINFO
            Public byIPID As Byte
            Public byAlarmOut As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=18, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_IPALARMOUTCFG
            Public dwSize As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_IP_ALARMOUT, ArraySubType:=UnmanagedType.Struct)>
            Public struIPAlarmOutInfo As NET_DVR_IPALARMOUTINFO()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_IPALARMOUTINFO_V40
            Public dwIPID As UInteger
            Public dwAlarmOut As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_IPALARMOUTCFG_V40
            Public dwSize As UInteger
            Public dwCurIPAlarmOutNum As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_IP_ALARMIN_V40, ArraySubType:=UnmanagedType.Struct)>
            Public struIPAlarmOutInfo As NET_DVR_IPALARMOUTINFO_V40()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=256, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_IPALARMININFO
            Public byIPID As Byte
            Public byAlarmIn As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=18, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_IPALARMINCFG
            Public dwSize As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_IP_ALARMIN, ArraySubType:=UnmanagedType.Struct)>
            Public struIPAlarmInInfo As NET_DVR_IPALARMININFO()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_IPALARMININFO_V40
            Public dwIPID As UInteger
            Public dwAlarmIn As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_IPALARMINCFG_V40
            Public dwSize As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_IP_ALARMIN_V40, ArraySubType:=UnmanagedType.Struct)>
            Public struIPAlarmInInfo As NET_DVR_IPALARMININFO_V40()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=256, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_IPCHANINFO
            Public byEnable As Byte
            Public byIPID As Byte
            Public byChannel As Byte
            Public byIPIDHigh As Byte
            Public byTransProtocol As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=31, ArraySubType:=UnmanagedType.I1)>
            Public byres As Byte()
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_IPSERVER_STREAM
            Public byEnable As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
            Public struIPServer As NET_DVR_IPADDR
            Public wPort As UShort
            Public wDvrNameLen As UShort
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byDVRName As Byte()
            Public wDVRSerialLen As UShort
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.U2)>
            Public byRes1 As UShort()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=SERIALNO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byDVRSerialNumber As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byUserName As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=PASSWD_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byPassWord As Byte()
            Public byChannel As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=11, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()

            Public Sub Init()
                byRes = New Byte(2) {}
                byRes1 = New UShort(1) {}
                byUserName = New Byte(31) {}
                byPassWord = New Byte(15) {}
                byDVRSerialNumber = New Byte(47) {}
                byDVRName = New Byte(31) {}
                byRes2 = New Byte(10) {}
            End Sub
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_STREAM_MEDIA_SERVER_CFG
            Public byValid As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            Public struDevIP As NET_DVR_IPADDR
            Public wDevPort As UShort
            Public byTransmitType As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=69, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()

            Public Sub Init()
                byRes1 = New Byte(2) {}
                byRes2 = New Byte(68) {}
            End Sub
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_DEV_CHAN_INFO
            Public struIP As NET_DVR_IPADDR
            Public wDVRPort As UShort
            Public byChannel As Byte
            Public byTransProtocol As Byte
            Public byTransMode As Byte
            Public byFactoryType As Byte
            Public byDeviceType As Byte
            Public byDispChan As Byte
            Public bySubDispChan As Byte
            Public byResolution As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_DOMAIN_NAME, ArraySubType:=UnmanagedType.I1)>
            Public byDomain As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)>
            Public sUserName As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=PASSWD_LEN, ArraySubType:=UnmanagedType.I1)>
            Public sPassword As Byte()

            Public Sub Init()
                byRes = New Byte(1) {}
                byDomain = New Byte(63) {}
                sUserName = New Byte(31) {}
                sPassword = New Byte(15) {}
            End Sub
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_PU_STREAM_CFG
            Public dwSize As UInteger
            Public struStreamMediaSvrCfg As NET_DVR_STREAM_MEDIA_SERVER_CFG
            Public struDevChanInfo As NET_DVR_DEV_CHAN_INFO
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_DDNS_STREAM_CFG
            Public byEnable As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            Private struStreamServer As NET_DVR_IPADDR
            Public wStreamServerPort As UShort
            Public byStreamServerTransmitType As Byte
            Public byRes2 As Byte
            Private struIPServer As NET_DVR_IPADDR
            Public wIPServerPort As UShort
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)>
            Public byRes3 As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)>
            Public sDVRName As Byte()
            Public wDVRNameLen As UShort
            Public wDVRSerialLen As UShort
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=SERIALNO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public sDVRSerialNumber As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)>
            Public sUserName As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=PASSWD_LEN, ArraySubType:=UnmanagedType.I1)>
            Public sPassWord As Byte()
            Public wDVRPort As UShort
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)>
            Public byRes4 As Byte()
            Public byChannel As Byte
            Public byTransProtocol As Byte
            Public byTransMode As Byte
            Public byFactoryType As Byte

            Public Sub Init()
                byRes1 = New Byte(2) {}
                byRes3 = New Byte(1) {}
                sDVRSerialNumber = New Byte(47) {}
                sUserName = New Byte(31) {}
                sPassWord = New Byte(15) {}
                byRes4 = New Byte(1) {}
            End Sub
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_PU_STREAM_URL
            Public byEnable As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=URL_LEN, ArraySubType:=UnmanagedType.I1)>
            Public strURL As Byte()
            Public byTransPortocol As Byte
            Public wIPID As UShort
            Public byChannel As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=7, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byRes = New Byte(6) {}
                strURL = New Byte(239) {}
            End Sub
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_HKDDNS_STREAM
            Public byEnable As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64, ArraySubType:=UnmanagedType.I1)>
            Public byDDNSDomain As Byte()
            Public wPort As UShort
            Public wAliasLen As UShort
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byAlias As Byte()
            Public wDVRSerialLen As UShort
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=SERIALNO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byDVRSerialNumber As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byUserName As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=PASSWD_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byPassWord As Byte()
            Public byChannel As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=11, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()

            Public Sub Init()
                byRes = New Byte(2) {}
                byDDNSDomain = New Byte(63) {}
                byAlias = New Byte(31) {}
                byRes1 = New Byte(1) {}
                byDVRSerialNumber = New Byte(47) {}
                byUserName = New Byte(31) {}
                byPassWord = New Byte(15) {}
                byRes2 = New Byte(10) {}
            End Sub
        End Structure

        Public Const NET_DVR_GET_IPPARACFG_V40 As Integer = 1062
        Public Const NET_DVR_SET_IPPARACFG_V40 As Integer = 1063

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_IPCHANINFO_V40
            Public byEnable As Byte
            Public byRes1 As Byte
            Public wIPID As UShort
            Public dwChannel As UInteger
            Public byTransProtocol As Byte
            Public byTransMode As Byte
            Public byFactoryType As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=241, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byRes = New Byte(240) {}
            End Sub
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_GET_STREAM_UNION
            Public struChanInfo As NET_DVR_IPCHANINFO
            Public struIPServerStream As NET_DVR_IPSERVER_STREAM
            Public struPUStream As NET_DVR_PU_STREAM_CFG
            Public struDDNSStream As NET_DVR_DDNS_STREAM_CFG
            Public struStreamUrl As NET_DVR_PU_STREAM_URL
            Public struHkDDNSStream As NET_DVR_HKDDNS_STREAM
            Public struIPChan As NET_DVR_IPCHANINFO_V40
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_STREAM_MODE
            Public byGetStreamType As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)>
            Public byRes As Byte()
            Public uGetStream As NET_DVR_GET_STREAM_UNION
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_IPPARACFG_V40
            Public dwSize As UInteger
            Public dwGroupNum As UInteger
            Public dwAChanNum As UInteger
            Public dwDChanNum As UInteger
            Public dwStartDChan As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CHANNUM_V30)>
            Public byAnalogChanEnable As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_IP_DEVICE_V40)>
            Public struIPDevInfo As NET_DVR_IPDEVINFO_V31()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CHANNUM_V30)>
            Public struStreamMode As NET_DVR_STREAM_MODE()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=20)>
            Public byRes2 As Byte()
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_JPEGPARA
            Public wPicSize As Short
            Public wPicQuality As Short
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_ACS_WORK_STATUS_V50
            Public dwSize As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)>
            Public byDoorLockStatus As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)>
            Public byDoorStatus As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)>
            Public byMagneticStatus As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CASE_SENSOR_NUM, ArraySubType:=UnmanagedType.I1)>
            Public byCaseStatus As Byte()
            Public wBatteryVoltage As UShort
            Public byBatteryLowVoltage As Byte
            Public byPowerSupplyStatus As Byte
            Public byMultiDoorInterlockStatus As Byte
            Public byAntiSneakStatus As Byte
            Public byHostAntiDismantleStatus As Byte
            Public byIndicatorLightStatus As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byCardReaderOnlineStatus As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byCardReaderAntiDismantleStatus As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)>
            Public byCardReaderVerifyMode As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMIN_NUM, ArraySubType:=UnmanagedType.I1)>
            Public bySetupAlarmStatus As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMIN_NUM, ArraySubType:=UnmanagedType.I1)>
            Public byAlarmInStatus As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMOUT_NUM, ArraySubType:=UnmanagedType.I1)>
            Public byAlarmOutStatus As Byte()
            Public dwCardNum As UInteger
            Public byFireAlarmStatus As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=123, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()

            Public Sub Init()
                byDoorLockStatus = New Byte(255) {}
                byDoorStatus = New Byte(255) {}
                byMagneticStatus = New Byte(255) {}
                byCaseStatus = New Byte(7) {}
                byCardReaderOnlineStatus = New Byte(511) {}
                byCardReaderAntiDismantleStatus = New Byte(511) {}
                byCardReaderVerifyMode = New Byte(511) {}
                bySetupAlarmStatus = New Byte(511) {}
                byAlarmInStatus = New Byte(511) {}
                byAlarmOutStatus = New Byte(511) {}
                byRes2 = New Byte(122) {}
            End Sub
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_ACS_EVENT_COND
            Public dwSize As UInteger
            Public dwMajor As UInteger
            Public dwMinor As UInteger
            Public struStartTime As CHCNetSDK.NET_DVR_TIME
            Public struEndTime As CHCNetSDK.NET_DVR_TIME
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.NAME_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byName As Byte()
            Public dwBeginSerialNo As UInteger
            Public byPicEnable As Byte
            Public byTimeType As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()
            Public dwEndSerialNo As UInteger
            Public dwIOTChannelNo As UInteger
            Public wInductiveEventType As UShort
            Public bySearchType As Byte
            Public byRes1 As Byte
            <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=CHCNetSDK.NET_SDK_MONITOR_ID_LEN)>
            Public szMonitorID As String
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.NET_SDK_EMPLOYEE_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byEmployeeNo As Byte()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=140, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byCardNo = New Byte(31) {}
                byName = New Byte(31) {}
                byRes2 = New Byte(1) {}
                byEmployeeNo = New Byte(31) {}
                byRes = New Byte(139) {}
            End Sub
        End Structure

        '<StructLayout(LayoutKind.Sequential)>
        'Public Structure NET_DVR_ACS_EVENT_CFG
        '    Public dwSize As UInteger
        '    Public dwMajor As UInteger
        '    Public dwMinor As UInteger
        '    Public struTime As CHCNetSDK.NET_DVR_TIME
        '    <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.MAX_NAMELEN)>
        '    Public sNetUser As Byte()
        '    Public struRemoteHostAddr As CHCNetSDK.NET_DVR_IPADDR
        '    Public struAcsEventInfo As CHCNetSDK.NET_DVR_ACS_EVENT_DETAIL
        '    Public dwPicDataLen As UInteger
        '    Public pPicData As IntPtr
        '    Public wInductiveEventType As UShort
        '    Public byTimeType As Byte
        '    <MarshalAs(UnmanagedType.ByValArray, SizeConst:=61)>
        '    Public byRes As Byte()
        'End Structure
        'New
        '<StructLayoutAttribute(LayoutKind.Sequential)>  'original
        'Public Structure NET_DVR_ACS_EVENT_CFG
        '    Public dwSize As UInteger
        '    Public dwMajor As UInteger
        '    Public dwMinor As UInteger
        '    Public struTime As CHCNetSDK.NET_DVR_TIME
        '    <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.MAX_NAMELEN)>
        '    Public sNetUser As Byte()
        '    Public struRemoteHostAddr As CHCNetSDK.NET_DVR_IPADDR
        '    Public struAcsEventInfo As CHCNetSDK.NET_DVR_ACS_EVENT_DETAIL
        '    Public dwPicDataLen As UInteger
        '    Public pPicData As IntPtr
        '    Public wInductiveEventType As UShort
        '    Public byTimeType As Byte
        '    <MarshalAs(UnmanagedType.ByValArray, SizeConst:=61)>
        '    Public byRes As Byte()
        '    Public Sub init()
        '        sNetUser = New Byte(CHCNetSDK.MAX_NAMELEN - 1) {}
        '        struRemoteHostAddr.Init()
        '        struAcsEventInfo.init()
        '        byRes = New Byte(60) {}
        '    End Sub
        'End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_ACS_EVENT_CFG
            Public dwSize As UInteger
            Public dwMajor As UInteger
            Public dwMinor As UInteger
            Public struTime As CHCNetSDK.NET_DVR_TIME
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.MAX_NAMELEN)>
            Public sNetUser() As Byte
            Public struRemoteHostAddr As CHCNetSDK.NET_DVR_IPADDR
            Public struAcsEventInfo As CHCNetSDK.NET_DVR_ACS_EVENT_DETAIL
            Public dwPicDataLen As UInteger
            Public pPicData As IntPtr
            ' picture data
            Public wInductiveEventType As System.UInt16
            Public byTimeType As Byte
            Public byRes1 As Byte
            Public dwQRCodeInfoLen As UInteger
            Public dwVisibleLightDataLen As UInteger
            Public dwThermalDataLen As UInteger
            Public pQRCodeInfo As IntPtr
            Public pVisibleLightData As IntPtr
            Public pThermalData As IntPtr
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=36)>
            Public byRes() As Byte
            Public Sub init()
                sNetUser = New Byte((CHCNetSDK.MAX_NAMELEN) - 1) {}
                struRemoteHostAddr.Init()
                struAcsEventInfo.init()
                byRes = New Byte((36) - 1) {}
            End Sub
        End Structure


        '<StructLayoutAttribute(LayoutKind.Sequential)>
        'Public Structure NET_DVR_ACS_EVENT_DETAIL
        '    Public dwSize As UInteger
        '    <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.ACS_CARD_NO_LEN)>
        '    Public byCardNo As Byte()
        '    Public byCardType As Byte
        '    Public byWhiteListNo As Byte
        '    Public byReportChannel As Byte
        '    Public byCardReaderKind As Byte
        '    Public dwCardReaderNo As UInteger
        '    Public dwDoorNo As UInteger
        '    Public dwVerifyNo As UInteger
        '    Public dwAlarmInNo As UInteger
        '    Public dwAlarmOutNo As UInteger
        '    Public dwCaseSensorNo As UInteger
        '    Public dwRs485No As UInteger
        '    Public dwMultiCardGroupNo As UInteger
        '    Public wAccessChannel As UShort
        '    Public byDeviceNo As Byte
        '    Public byDistractControlNo As Byte
        '    Public dwEmployeeNo As UInteger
        '    Public wLocalControllerID As UShort
        '    Public byInternetAccess As Byte
        '    Public byType As Byte
        '    <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.MACADDR_LEN)>
        '    Public byMACAddr As Byte()
        '    Public bySwipeCardType As Byte
        '    Public byRes2 As Byte
        '    Public dwSerialNo As UInteger
        '    Public byChannelControllerID As Byte
        '    Public byChannelControllerLampID As Byte
        '    Public byChannelControllerIRAdaptorID As Byte
        '    Public byChannelControllerIREmitterID As Byte
        '    Public dwRecordChannelNum As UInteger
        '    Public pRecordChannelData As UInteger
        '    Public byUserType As Byte
        '    Public byCurrentVerifyMode As Byte
        '    <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)>
        '    Public byRe2 As Byte()
        '    <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.NET_SDK_EMPLOYEE_NO_LEN)>
        '    Public byEmployeeNo As Byte()
        '    <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)>
        '    Public byRes As Byte()

        '    Public Sub init()
        '        byCardNo = New Byte(CHCNetSDK.ACS_CARD_NO_LEN - 1) {}
        '        byMACAddr = New Byte(CHCNetSDK.MACADDR_LEN - 1) {}
        '        byRe2 = New Byte(1) {}
        '        byEmployeeNo = New Byte(CHCNetSDK.NET_SDK_EMPLOYEE_NO_LEN - 1) {}
        '        byRes = New Byte(63) {}
        '    End Sub
        'End Structure 'original

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_ACS_EVENT_DETAIL
            Public dwSize As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.ACS_CARD_NO_LEN)>
            Public byCardNo() As Byte
            Public byCardType As Byte
            Public byWhiteListNo As Byte
            Public byReportChannel As Byte
            Public byCardReaderKind As Byte
            Public dwCardReaderNo As UInteger
            Public dwDoorNo As UInteger
            Public dwVerifyNo As UInteger
            Public dwAlarmInNo As UInteger
            Public dwAlarmOutNo As UInteger
            Public dwCaseSensorNo As UInteger
            Public dwRs485No As UInteger
            Public dwMultiCardGroupNo As UInteger
            Public wAccessChannel As System.UInt16
            Public byDeviceNo As Byte
            Public byDistractControlNo As Byte
            Public dwEmployeeNo As UInteger
            Public wLocalControllerID As System.UInt16
            Public byInternetAccess As Byte
            Public byType As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.MACADDR_LEN)>
            Public byMACAddr() As Byte
            Public bySwipeCardType As Byte
            Public byEventAttribute As Byte
            Public dwSerialNo As UInteger
            Public byChannelControllerID As Byte
            Public byChannelControllerLampID As Byte
            Public byChannelControllerIRAdaptorID As Byte
            Public byChannelControllerIREmitterID As Byte
            Public dwRecordChannelNum As UInteger
            Public pRecordChannelData As IntPtr
            Public byUserType As Byte
            Public byCurrentVerifyMode As Byte
            Public byAttendanceStatus As Byte
            Public byStatusValue As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.NET_SDK_EMPLOYEE_NO_LEN)>
            Public byEmployeeNo() As Byte
            Public byRes1 As Byte
            Public byMask As Byte
            Public byThermometryUnit As Byte
            Public byIsAbnomalTemperature As Byte
            Public fCurrTemperature As Single
            Public struRegionCoordinates As CHCNetSDK.NET_VCA_POINT
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=48)>
            Public byRes() As Byte
            Public Sub init()
                byCardNo = New Byte((CHCNetSDK.ACS_CARD_NO_LEN) - 1) {}
                byMACAddr = New Byte((CHCNetSDK.MACADDR_LEN) - 1) {}
                byEmployeeNo = New Byte((CHCNetSDK.NET_SDK_EMPLOYEE_NO_LEN) - 1) {}
                byRes = New Byte((48) - 1) {}
            End Sub
        End Structure

        'Public Structure NET_DVR_ACS_EVENT_DETAIL
        '    Public dwSize As UInteger
        '    <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.ACS_CARD_NO_LEN)>
        '    Public byCardNo As Byte()
        '    Public byCardType As Byte
        '    Public byWhiteListNo As Byte
        '    Public byReportChannel As Byte
        '    Public byCardReaderKind As Byte
        '    Public dwCardReaderNo As UInteger
        '    Public dwDoorNo As UInteger
        '    Public dwVerifyNo As UInteger
        '    Public dwAlarmInNo As UInteger
        '    Public dwAlarmOutNo As UInteger
        '    Public dwCaseSensorNo As UInteger
        '    Public dwRs485No As UInteger
        '    Public dwMultiCardGroupNo As UInteger
        '    Public wAccessChannel As UShort
        '    Public byDeviceNo As Byte
        '    Public byDistractControlNo As Byte
        '    Public dwEmployeeNo As UInteger
        '    Public wLocalControllerID As UShort
        '    Public byInternetAccess As Byte
        '    Public byType As Byte
        '    <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.MACADDR_LEN)>
        '    Public byMACAddr As Byte()
        '    Public bySwipeCardType As Byte
        '    Public byRes2 As Byte
        '    Public dwSerialNo As UInteger
        '    Public byChannelControllerID As Byte
        '    Public byChannelControllerLampID As Byte
        '    Public byChannelControllerIRAdaptorID As Byte
        '    Public byChannelControllerIREmitterID As Byte
        '    Public dwRecordChannelNum As UInteger
        '    Public pRecordChannelData As UInteger
        '    Public byUserType As Byte
        '    Public byCurrentVerifyMode As Byte
        '    <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)>
        '    Public byRe2 As Byte()
        '    <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.NET_SDK_EMPLOYEE_NO_LEN)>
        '    Public byEmployeeNo As Byte()
        '    <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)>
        '    Public byRes As Byte()
        'End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_VIDEO_CALL_COND
            Public dwSize As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=128, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byRes = New Byte(127) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_VIDEO_CALL_PARAM
            Public dwSize As UInteger
            Public dwCmdType As UInteger
            Public wPeriod As UShort
            Public wBuildingNumber As UShort
            Public wUnitNumber As UShort
            Public wFloorNumber As UShort
            Public wRoomNumber As UShort
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=118, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                byRes = New Byte(117) {}
            End Sub
        End Structure

        Public Structure NET_DVR_CLIENTINFO
            Public lChannel As Int32
            Public lLinkMode As UInteger
            Public hPlayWnd As IntPtr
            Public sMultiCastIP As String
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_VOLUME_CFG
            Public dwSize As UInteger
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_AUDIOOUT_PRO_TYPE)>
            Public wVolume As UShort()
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)>
            Public byRes As Byte()
        End Structure



        Public Delegate Sub RemoteConfigCallback(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
        Public Delegate Sub RealDataCallBack(ByVal lPlayHandle As Integer, ByVal dwDataType As UInteger, ByVal pBuffer As IntPtr, ByVal dwBufSize As UInteger, ByVal pUser As IntPtr)
        Public Delegate Sub MSGCallBack(ByVal lCommand As Integer, ByRef pAlarmer As NET_DVR_ALARMER, ByVal pAlarmInfo As IntPtr, ByVal dwBufLen As UInteger, ByVal pUser As IntPtr)
        Public Delegate Function MSGCallBack_V31(ByVal lCommand As Integer, ByRef pAlarmer As NET_DVR_ALARMER, ByVal pAlarmInfo As IntPtr, ByVal dwBufLen As UInteger, ByVal pUser As IntPtr) As Boolean
        Public Delegate Sub REALDATACALLBACKCAPS(ByVal lRealHandle As Int32, ByVal dwDataType As UInt32, ByRef pBuffer As Byte, ByVal dwBufSize As UInt32, ByVal pUser As IntPtr)
        Public Delegate Sub VOICEDATACALLBACKV30(ByVal lVoiceComHandle As Integer, ByVal pRecvDataBuffer As String, ByVal dwBufSize As UInteger, ByVal byAudioFlag As Byte, ByVal pUser As System.IntPtr)


        Public Declare Function NET_DVR_Init Lib "HCNetSDK.dll" () As Boolean

        Public Declare Function NET_DVR_Cleanup Lib "HCNetSDK.dll" () As Boolean

        Public Declare Function NET_DVR_SetLogToFile Lib "HCNetSDK.dll" (ByVal nLogLevel As Integer, ByVal strLogDir As String, ByVal bAutoDel As Boolean) As Boolean

        Public Declare Function NET_DVR_GetLastError Lib "HCNetSDK.dll" () As UInteger

        Public Declare Function NET_DVR_GetErrorMsg Lib "HCNetSDK.dll" (ByRef pErrorNo As Integer) As IntPtr

        Public Declare Function NET_DVR_SetAlarmDeviceUser Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal lUserIndex As Integer, ByRef lpDeviceUser As NET_DVR_ALARM_DEVICE_USER) As Boolean

        Public Declare Function NET_DVR_GetAlarmDeviceUser Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal lUserIndex As Integer, ByRef lpDeviceUser As NET_DVR_ALARM_DEVICE_USER) As Boolean

        Public Declare Function NET_DVR_GetDVRConfig Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwCommand As UInteger, ByVal lChannel As Integer, ByVal lpOutBuffer As IntPtr, ByVal dwOutBufferSize As UInteger, ByRef lpBytesReturned As UInteger) As Boolean

        Public Declare Function NET_DVR_SetDVRConfig Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwCommand As UInteger, ByVal lChannel As Integer, ByVal lpInBuffer As IntPtr, ByVal dwInBufferSize As UInteger) As Boolean

        '<DllImportAttribute("HCNetSDK.dll")> _
        Public Declare Function NET_DVR_StartRemoteConfig Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwCommand As UInteger, ByVal lpInBuffer As IntPtr, ByVal dwInBufferLen As Int32, ByVal cbStateCallback As RemoteConfigCallback, ByVal pUserData As IntPtr) As Integer

        '<DllImportAttribute("HCNetSDK.dll")> _
        Public Declare Function NET_DVR_SendRemoteConfig Lib "HCNetSDK.dll" (ByVal lHandle As Integer, ByVal dwDataType As UInteger, ByVal pSendBuf As IntPtr, ByVal dwBufSize As UInteger) As Boolean

        ' stop a long connection
        ' [in] lHandle - handle ,NET_DVR_StartRemoteConfig return value
        '<DllImportAttribute("HCNetSDK.dll")> _
        Public Declare Function NET_DVR_StopRemoteConfig Lib "HCNetSDK.dll" (ByVal lHandle As Integer) As Boolean

        '<DllImportAttribute("HCNetSDK.dll")> _
        Public Declare Function NET_DVR_Upgrade_V40 Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwUpgradeType As UInteger, ByVal sFileName As String, ByVal pInbuffer As IntPtr, ByVal dwInBufferLen As Int32) As Integer

        '<DllImportAttribute("HCNetSDK.dll")> _
        Public Declare Function NET_DVR_GetUpgradeProgress Lib "HCNetSDK.dll" (ByVal lUpgradeHandle As Integer) As Integer

        '<DllImportAttribute("HCNetSDK.dll")> _
        Public Declare Function NET_DVR_CloseUpgradeHandle Lib "HCNetSDK.dll" (ByVal lUpgradeHandle As Integer) As Integer

        '<DllImportAttribute("HCNetSDK.dll")> _
        Public Declare Function NET_DVR_GetRemoteConfigState Lib "HCNetSDK.dll" (ByVal lHandle As Integer, ByVal pState As IntPtr) As Boolean

        '<DllImportAttribute("HCNetSDK.dll")> _
        Public Declare Function NET_DVR_GetNextRemoteConfig Lib "HCNetSDK.dll" (ByVal lHandle As Integer, ByVal lpOutBuff As IntPtr, ByVal dwOutBuffSize As UInteger) As Integer

        'Public Declare Function NET_DVR_GetNextRemoteConfig Lib "HCNetSDK.dll" (ByVal lHandle As Integer, ByVal lpOutBuff As CHCNetSDK.NET_DVR_ACS_EVENT_CFG, ByVal dwOutBuffSize As Integer) As Integer 'new


        Public Declare Function NET_DVR_GetDeviceConfig Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwCommand As UInteger, ByVal dwCount As UInteger, ByVal lpInBuffer As IntPtr, ByVal dwInBufferSize As UInteger, ByVal lpStatusList As IntPtr, ByVal lpOutBuffer As IntPtr, ByVal dwOutBufferSize As UInteger) As Boolean

        Public Declare Function NET_DVR_SetDeviceConfig Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwCommand As UInteger, ByVal dwCount As UInteger, ByVal lpInBuffer As IntPtr, ByVal dwInBufferSize As UInteger, ByVal lpStatusList As IntPtr, ByVal lpInParamBuffer As IntPtr, ByVal dwInParamBufferSize As UInteger) As Boolean

        Public Declare Function NET_DVR_RemoteControl Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwCommand As UInteger, ByVal lpInBuffer As IntPtr, ByVal dwInBufferSize As UInteger) As Boolean

        Public Declare Function NET_DVR_Login_V40 Lib "HCNetSDK.dll" (ByRef pLoginInfo As NET_DVR_USER_LOGIN_INFO, ByRef lpDeviceInfo As NET_DVR_DEVICEINFO_V40) As Integer

        Public Declare Function NET_DVR_Logout_V30 Lib "HCNetSDK.dll" (ByVal lUserID As Int32) As Boolean

        Public Declare Function NET_DVR_RealPlay_V40 Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByRef lpPreviewInfo As NET_DVR_PREVIEWINFO, ByVal fRealDataCallBack_V30 As RealDataCallBack, ByVal pUser As IntPtr) As Integer

        ' alarm
        Public Declare Function NET_DVR_SetupAlarmChan Lib "HCNetSDK.dll" (ByVal lUserID As Integer) As Integer

        Public Declare Function NET_DVR_SetupAlarmChan_V30 Lib "HCNetSDK.dll" (ByVal lUserID As Integer) As Integer

        Public Declare Function NET_DVR_SetupAlarmChan_V41 Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByRef lpSetupParam As NET_DVR_SETUPALARM_PARAM) As Integer

        Public Declare Function NET_DVR_CloseAlarmChan Lib "HCNetSDK.dll" (ByVal lAlarmHandle As Integer) As Boolean

        Public Declare Function NET_DVR_CloseAlarmChan_V30 Lib "HCNetSDK.dll" (ByVal lAlarmHandle As Integer) As Boolean

        Public Declare Function NET_DVR_SetDVRMessageCallBack_V50 Lib "HCNetSDK.dll" (ByVal iIndex As Integer, ByVal fMessageCallBack As MSGCallBack, ByVal pUser As IntPtr) As Boolean

        Public Declare Function NET_DVR_SetDVRMessageCallBack_V31 Lib "HCNetSDK.dll" (ByVal fMessageCallBack As MSGCallBack_V31, ByVal pUser As IntPtr) As Boolean

        Public Declare Function NET_DVR_GetDeviceAbility Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwAbilityType As UInteger, ByVal pInBuf As IntPtr, ByVal dwInLength As UInteger, ByVal pOutBuf As IntPtr, ByVal dwOutLength As UInteger) As Boolean

        Public Declare Function NET_DVR_GetSDKVersion Lib "HCNetSDK.dll" () As UInteger

        Public Declare Function NET_DVR_GetSDKBuildVersion Lib "HCNetSDK.dll" () As UInteger

        Public Declare Function NET_DVR_ControlGateway Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal lGatewayIndex As Integer, ByVal dwStaic As UInteger) As Boolean

        Public Declare Function NET_DVR_STDXMLConfig Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal lpInputParam As IntPtr, ByVal lpOutputParam As IntPtr) As Boolean

        Public Declare Function NET_DVR_RealPlay_V30 Lib "HCNetSDK.dll" (ByVal iUserID As Integer, ByRef lpClientInfo As NET_DVR_CLIENTINFO, ByVal fRealDataCallBack_V30 As REALDATACALLBACKCAPS, ByVal pUser As IntPtr, ByVal bBlocked As UInt32) As Integer

        Public Declare Function NET_DVR_StopRealPlay Lib "HCNetSDK.dll" (ByVal iRealHandle As Integer) As Boolean

        Public Declare Function NET_DVR_StartVoiceCom_V30 Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwVoiceChan As UInteger, ByVal bNeedCBNoEncData As Boolean, ByVal fVoiceDataCallBack As VOICEDATACALLBACKV30, ByVal pUser As IntPtr) As Integer

        Public Declare Function NET_DVR_SetVoiceComClientVolume Lib "HCNetSDK.dll" (ByVal lVoiceComHandle As Integer, ByVal wVolume As System.UInt16) As Boolean

        Public Declare Function NET_DVR_StopVoiceCom Lib "HCNetSDK.dll" (ByVal lVoiceComHandle As Integer) As Boolean

        Public Delegate Function ISAPIMsgCallBack(ByRef IsapiAlarm As NET_DVR_ALARM_ISAPI_INFO, ByVal pAlarmInfo As IntPtr, ByVal dwBufLen As UInteger) As Boolean

        'Public Declare Function PostMessage Lib "User32.dll" (ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As Integer) As Integer
        <DllImport("User32.dll", EntryPoint:="PostMessage")>
        Shared Function PostMessage(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As Integer) As Integer
        End Function

        '<DllImport("User32.dll", EntryPoint:="SendMessage")>
        'Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As Integer) As Integer
        'End Function

        '<DllImport("HCNetSDK.dll")>
        Public Declare Function NET_DVR_CaptureJPEGPicture Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal lChannel As Integer, ByRef lpJpegPara As CHCNetSDK.NET_DVR_JPEGPARA, ByVal sPicFileName As IntPtr) As Boolean

        Public Declare Function NET_DVR_SendWithRecvRemoteConfig Lib "HCNetSDK.dll" (ByVal lHandle As Integer, ByVal lpInBuff As IntPtr, ByVal dwInBuffSize As UInteger, ByVal lpOutBuff As IntPtr, ByVal dwOutBuffSize As UInteger, ByRef dwOutDataLen As UInteger) As Integer


        Public Enum NET_SDK_SENDWITHRECV_STATUS
            NET_SDK_CONFIG_STATUS_SUCCESS = 1000
            NET_SDK_CONFIG_STATUS_NEEDWAIT
            NET_SDK_CONFIG_STATUS_FINISH
            NET_SDK_CONFIG_STATUS_FAILED
            NET_SDK_CONFIG_STATUS_EXCEPTION
        End Enum

        'realtime logs
        Public Structure NET_DVR_ALARM_ISAPI_INFO
            Public pAlarmData As IntPtr ' Alarm Data
            Public dwAlarmDataLen As UInteger ' Length of Alarm Data
            Public byDataType As Byte ' 0-invalid,1-xml,2-json
            Public byPicturesNumber As Byte ' Num of Pic
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)>
            Public byRes() As Byte
            Public pPicPackData As IntPtr
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)>
            Public byRes1() As Byte
        End Structure
        Public Structure NET_DVR_ALARM_ISAPI_PICDATA
            Public dwPicLen As UInteger
            Public byPicType As Byte '图片格式: 1- jpg
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes() As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=256, ArraySubType:=UnmanagedType.ByValArray)>
            Public szFilename() As Char
            Public pPicData As IntPtr
        End Structure
        Public Structure NET_DVR_ACS_ALARM_INFO
            Public dwSize As UInteger
            Public dwMajor As UInteger ' alarm major, reference to macro
            Public dwMinor As UInteger ' alarm minor, reference to macro
            Public struTime As NET_DVR_TIME
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_NAMELEN, ArraySubType:=UnmanagedType.I1)>
            Public sNetUser() As Byte ' net operator user
            Public struRemoteHostAddr As NET_DVR_IPADDR ' remote host address
            Public struAcsEventInfo As NET_DVR_ACS_EVENT_INFO
            Public dwPicDataLen As UInteger ' picture length, when 0 ,means has no picture
            Public pPicData As IntPtr ' picture data
            Public wInductiveEventType As UShort '归纳事件类型，0-无效，客户端判断该值为非0值后，报警类型通过归纳事件类型区分，否则通过原有报警主次类型（dwMajor、dwMinor）区分
            Public byPicTransType As Byte '图片数据传输方式: 0-二进制；1-url
            Public byRes1 As Byte '保留字节
            Public dwIOTChannelNo As UInteger
            Public pAcsEventInfoExtend As IntPtr
            Public byAcsEventInfoExtend As Byte 'pAcsEventInfoExtend是否有效：0-无效，1-有效
            Public byTimeType As Byte '时间类型：0-设备本地时间，1-UTC时间（struTime的时间）
            Public byRes2 As Byte
            Public byAcsEventInfoExtendV20 As Byte 'pAcsEventInfoExtendV20 is valid: 0-invalid, 1-valid'pAcsEventInfoExtendV20是否有效：0-无效，1-有效
            Public pAcsEventInfoExtendV20 As IntPtr 'byAcsEventInfoExtendV20为1时，表示指向一个NET_DVR_ACS_EVENT_INFO_EXTEND_V20结构体
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4, ArraySubType:=UnmanagedType.I1)>
            Public byRes() As Byte
        End Structure
        Public Structure NET_DVR_ACS_EVENT_INFO
            Public dwSize As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byCardNo() As Byte ' card No, 0 means invalid
            Public byCardType As Byte ' card type,1-ordinary card,2-disable card,3-black list card, 4-patrol card,5-stress card,6-super card,7-client card, 0 means invalid
            Public byWhiteListNo As Byte ' white list No, 1-8, 0 means invalid
            Public byReportChannel As Byte ' report channel, 1-alarmin updata, 2-center group 1, 3-center group 2, 0 means invalid
            Public byCardReaderKind As Byte ' card reader type: 0-invalid, 1-IC card reader, 2-Id card reader, 3-Qr code reader, 4-Fingerprint head
            Public dwCardReaderNo As UInteger ' card reader No, 0 means invalid
            Public dwDoorNo As UInteger ' door No(floor No), 0 means invalid
            Public dwVerifyNo As UInteger ' mutilcard verify No. 0 means invalid
            Public dwAlarmInNo As UInteger ' alarm in No, 0 means invalid
            Public dwAlarmOutNo As UInteger ' alarm out No 0 means invalid
            Public dwCaseSensorNo As UInteger ' case sensor No 0 means invalid
            Public dwRs485No As UInteger ' RS485 channel,0 means invalid
            Public dwMultiCardGroupNo As UInteger ' multicard group No.
            Public wAccessChannel As UShort ' Staff channel number
            Public byDeviceNo As Byte ' device No,0 means invalid
            Public byDistractControlNo As Byte ' distract control,0 means invalid
            Public dwEmployeeNo As UInteger ' employee No,0 means invalid
            Public wLocalControllerID As UShort ' On the controller number, 0 - access the host, 1-64 on behalf of the local controller
            Public byInternetAccess As Byte ' Internet access ID (1-uplink network port 1, 2-uplink network port 2,3- downstream network interface 1
            Public byType As Byte ' protection zone type, 0-real time, 1-24 hours, 2-delay, 3-internal, 4-the key, 5-fire, 6-perimeter, 7-24 hours of silent
            ' 8-24 hours auxiliary, 9-24 hours vibration, 10-door emergency open, 11-door emergency shutdown, 0xff-null
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MACADDR_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byMACAddr() As Byte ' mac addr 0 means invalid
            Public bySwipeCardType As Byte ' swipe card type, 0-invalid,1-Qr code
            Public byMask As Byte 'Mask or not：0-Default，1-unkonow，2-Not Mask，3-Mask
            Public dwSerialNo As UInteger 'Event No ,0--Invalid
            Public byChannelControllerID As Byte 'Channel Controller ID，0--Invaild，1-Main Channel Controller，2-Sub Channel Controller
            Public byChannelControllerLampID As Byte 'Channel Controller Board ID，0--Invaild（1-255）
            Public byChannelControllerIRAdaptorID As Byte 'Channel controller infrared adapterID，0--Invalid（1-255）
            Public byChannelControllerIREmitterID As Byte 'Channel controller infrared target ID，0--Invalid（1-255）
            Public byHelmet As Byte 'Safe Hat：0-Default，1-Unknow，2-Not, 3-Yes
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes() As Byte
        End Structure
        Public Structure NET_DVR_ACS_EVENT_INFO_EXTEND_V20
            Public byRemoteCheck As Byte 'Whether remote verification is required（0-Invalid，1-Not（Default），2-yes）
            Public byThermometryUnit As Byte 'Temperature measurement unit（0-Centigrade temperature（Default），1-Fahrenheit degree，2-Kelvin）
            Public byIsAbnomalTemperature As Byte 'Whether the temperature of face capture temperature measurement is abnormal：1-yes，0-no
            Public byRes2 As Byte
            Public fCurrTemperature As Single 'Face Temperature
            Public struRegionCoordinates As NET_VCA_POINT 'Face Temperature coordinate
            Public dwQRCodeInfoLen As UInteger 'QR code information length
            Public dwVisibleLightDataLen As UInteger 'Visible image length of thermal imaging camera
            Public dwThermalDataLen As UInteger 'Thermal image length
            Public pQRCodeInfo As IntPtr 'QR code information pointer
            Public pVisibleLightData As IntPtr 'Visible light picture pointer of thermal imaging camera
            Public pThermalData As IntPtr 'Thermal image pointer
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=64, ArraySubType:=UnmanagedType.I1)>
            Public byAttendanceLabel() As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=960, ArraySubType:=UnmanagedType.I1)>
            Public byRes() As Byte
        End Structure
        Public Structure NET_VCA_POINT
            Public fX As Single ' X轴坐标, 0.000~1
            Public fY As Single 'Y轴坐标, 0.000~1
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
        Public Structure NET_DVR_ACS_EVENT_INFO_EXTEND
            Public dwFrontSerialNo As UInteger
            Public byUserType As Byte
            Public byCurrentVerifyMode As Byte
            Public byCurrentEvent As Byte
            Public byPurePwdVerifyEnable As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)>
            Public byEmployeeNo As Byte()
            Public byAttendanceStatus As Byte
            Public byStatusValue As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=36, ArraySubType:=UnmanagedType.I1)>
            Public byUUID As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=64, ArraySubType:=UnmanagedType.I1)>
            Public byDeviceName As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=24, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
        End Structure
        'end realtime logs


        'set ISUP (Ehome)
        Public Const ACCOUNTNUM_LEN_32 As Integer = 32
        Public Const MAX_CENTERNUM As Integer = 4
        Public Const NET_SDK_EHOME_KEY_LEN As Integer = 32
        Public Const NET_DVR_SET_ALARMHOST_NETCFG_V50 As Integer = 2225
        Public Const NET_DVR_GET_ALARMHOST_NETCFG_V50 As Integer = 2224
        '<StructLayoutAttribute(LayoutKind.Sequential)>
        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_DVR_ALARMHOST_NETPARAM_V50
            Public dwSize As UInteger
            Public struIP As NET_DVR_IPADDR
            Public wPort As UShort
            Public byAddressType As Byte
            Public byEnable As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOMAIN_NAME, ArraySubType:=UnmanagedType.I1)>
            Public byDomainName As Byte()
            Public byReportProtocol As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ACCOUNTNUM_LEN_32, ArraySubType:=UnmanagedType.I1)>
            Public byDevID As Byte()
            Public byProtocolVersion As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=NET_SDK_EHOME_KEY_LEN, ArraySubType:=UnmanagedType.I1)>
            Public byEHomeKey As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28, ArraySubType:=UnmanagedType.I1)>
            Public byRes2 As Byte()

            Public Sub Init()
                struIP = New NET_DVR_IPADDR()
                byDomainName = New Byte(MAX_DOMAIN_NAME - 1) {}
                byDevID = New Byte(ACCOUNTNUM_LEN_32 - 1) {}
                byRes1 = New Byte(2) {}
                byEHomeKey = New Byte(NET_SDK_EHOME_KEY_LEN - 1) {}
                byRes2 = New Byte(27) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_ALARMHOST_NETCFG_V50
            Public dwSize As UInteger
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CENTERNUM, ArraySubType:=UnmanagedType.Struct)>
            Public struNetCenter As NET_DVR_ALARMHOST_NETPARAM_V50()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128, ArraySubType:=UnmanagedType.I1)>
            Public byRes1 As Byte()

            Public Sub Init()
                struNetCenter = New NET_DVR_ALARMHOST_NETPARAM_V50(MAX_CENTERNUM - 1) {}
                byRes1 = New Byte(127) {}
            End Sub
        End Structure
        Public Const ALARMHOST_ABILITY As Integer = &H500
        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_ALARMHOST_ABILITY
            Public dwSize As UInteger
            Public wTotalAlarmInNum As UShort
            Public wLocalAlarmInNum As UShort
            Public wExpandAlarmInNum As UShort
            Public wTotalAlarmOutNum As UShort
            Public wLocalAlarmOutNum As UShort
            Public wExpandAlarmOutNum As UShort
            Public wTotalRs485Num As UShort
            Public wLocalRs485Num As UShort
            Public wExpandRs485Num As UShort
            Public wFullDuplexRs485Num As UShort
            Public wTotalSensorNum As UShort
            Public wLocalSensorNum As UShort
            Public wExpandSensorNum As UShort
            Public wAudioOutNum As UShort
            Public wGatewayNum As UShort
            Public wElectroLockNum As UShort
            Public wSirenNum As UShort
            Public wSubSystemNum As UShort
            Public wNetUserNum As UShort
            Public wKeyboardNum As UShort
            Public wOperatorUserNum As UShort
            Public bySupportDetector As Byte
            Public bySupportSensitivity As Byte
            Public bySupportArrayBypass As Byte
            Public bySupportAlarmInDelay As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=16, ArraySubType:=UnmanagedType.I1)>
            Public bySupportAlarmInType As Byte()
            Public byTelNum As Byte
            Public byCenterGroupNum As Byte
            Public byGPRSNum As Byte
            Public byNetNum As Byte
            Public byAudioNum As Byte
            Public by3GNum As Byte
            Public byAnalogVideoChanNum As Byte
            Public byDigitalVideoChanNum As Byte
            Public bySubSystemArmType As Byte
            Public byPublicSubSystemNum As Byte
            Public dwSupport1 As UInteger
            Public dwSubSystemEvent As UInteger
            Public dwOverallEvent As UInteger
            Public dwFaultType As UInteger
            Public byPublicSubsystemAssociateSubsystemNum As Byte
            Public byOverallKeyboard As Byte
            Public wSafetyCabinSupport As UShort
            Public by485SlotNum As Byte
            Public bySubSystemAttributeAbility As Byte
            Public wKeyboardAddrNum As UShort
            Public byAlarmLampNum As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=117, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()

            Public Sub Init()
                bySupportAlarmInType = New Byte(15) {}
                byRes = New Byte(116) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_ALARMHOST_REPORT_CENTER_CFG_V40
            Public dwSize As UInteger
            Public byValid As Byte
            Public byDataType As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)>
            Public byRes As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4, ArraySubType:=UnmanagedType.Struct)>
            Public byChanAlarmMode As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=16, ArraySubType:=UnmanagedType.Struct)>
            Public byDealFailCenter As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=512, ArraySubType:=UnmanagedType.Struct)>
            Public byZoneReport As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.Struct)>
            Public byNonZoneReport As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4, ArraySubType:=UnmanagedType.Struct)>
            Public byAlarmNetCard As Byte()
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=252, ArraySubType:=UnmanagedType.Struct)>
            Public byRes2 As Byte()

            Public Sub Init()
                byRes = New Byte(1) {}
                byChanAlarmMode = New Byte(3) {}
                byDealFailCenter = New Byte(15) {}
                byZoneReport = New Byte(511) {}
                byNonZoneReport = New Byte(31) {}
                byAlarmNetCard = New Byte(3) {}
                byRes2 = New Byte(251) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_INDEX
            Public iIndex As UInteger

            Public Sub Init()
                iIndex = 1
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_DVR_INBUFF
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=16, ArraySubType:=UnmanagedType.U4)>
            Public StatusList As UInteger()

            Public Sub Init()
                StatusList = New UInteger(15) {}
            End Sub
        End Structure
        Public Const NET_DVR_SET_ALARMHOST_REPORT_CENTER_V40 As Integer = 2065
        'end Set ISUP (Ehome)
    End Class

    'for getting user info (not having card number) added from staffmanagament.cs (Acs Demo)
    'Public Class CUserInfoSearch
    '    Public Property UserInfoSearch As CUserInfoSearchContent
    'End Class
    Public Class CUserInfoSearchContent
        Public Property searchID As String
        Public Property responseStatusStrg As String
        Public Property numOfMatches As Integer
        Public Property totalMatches As Integer
        Public Property UserInfo As List(Of CUserInfoContent)
    End Class
    Public Class CUserInfoContent
        Public Property employeeNo As String
        Public Property name As String
        Public Property userType As String
        Public Property closeDelayEnabled As Boolean
        Public Property Valid As CVaild
        Public Property belongGroup As String
        Public Property password As String
        Public Property doorRight As String
        Public Property RightPlan As List(Of CRightPlan)
        Public Property maxOpenDoorTime As Integer
        Public Property openDoorTime As Integer
        Public Property roomNumber As Integer
        Public Property floorNumber As Integer
        Public Property doubleLockRight As Boolean
        Public Property alwaysOpenRight As Boolean
        Public Property localUIRight As Boolean
        Public Property userVerifyMode As String
    End Class
    Public Class CVaild
        Public Property enable As Boolean
        Public Property beginTime As String
        Public Property endTime As String
        Public Property timeType As String
    End Class
    Public Class CRightPlan
        Public Property doorNo As Integer
        Public Property planTemplateNo As String
    End Class
    'Public Class CUserInfoSearchCond
    '    Public Property UserInfoSearchCond As CUserInfoSearchCondContent
    'End Class
    Public Class CUserInfoSearchCondContent
        Public Property searchID As String
        Public Property searchResultPosition As Integer
        Public Property maxResults As Integer
        Public Property EmployeeNoList As List(Of CEmployeeNoList)
    End Class
    'Public Class CEmployeeNoList
    '    Public Property employeeNo As String
    'End Class

    'face 
    Public Class CSearchFaceDataCond
        Public Property searchResultPosition As Integer
        Public Property maxResults As Integer
        Public Property faceLibType As String
        Public Property FDID As String
        Public Property FPID As String
    End Class
    Public Class CSearchFaceDataReturn
        Public Property requestURL As String
        Public Property statusCode As Integer
        Public Property statusString As String
        Public Property subStatusCode As String
        Public Property errorCode As Integer
        Public Property errorMsg As String
        Public Property responseStatusStrg As String
        Public Property numOfMatches As Integer
        Public Property totalMatches As Integer
        Public Property MatchList As List(Of CMatchList)
    End Class
    Public Class CMatchList
        Public Property FPID As String
        Public Property employeeNo As Integer
    End Class
    Public Class CSetFaceDataCond
        Public Property faceLibType As String
        Public Property FDID As String
        Public Property FPID As String
        Public Property deleteFP As Boolean
    End Class
    Public Class CResponseStatus
        Public Property requestURL As String
        Public Property statusCode As Integer
        Public Property statusString As String
        Public Property subStatusCode As String
        Public Property errorCode As Integer
        Public Property errorMsg As String
    End Class
    Public Class CUserInfoCfg
        Public Property UserInfo As CUserInfo
    End Class
    'Public Class CUserInfo
    '    Public Property employeeNo As String
    '    Public Property name As String
    '    Public Property userType As String
    '    Public Property closeDelayEnabled As Boolean
    '    Public Property Valid As CValid
    '    Public Property belongGroup As String
    '    Public Property password As String
    '    Public Property doorRight As String
    '    Public Property RightPlan As List(Of CRightPlan)
    '    Public Property maxOpenDoorTime As Integer
    '    Public Property openDoorTime As Integer
    '    Public Property roomNumber As Integer
    '    Public Property floorNumber As Integer
    '    Public Property doubleLockRight As Boolean
    '    Public Property alwaysOpenRight As Boolean
    '    Public Property localUIRight As Boolean 'admin user rights
    '    Public Property userVerifyMode As String
    'End Class
    Public Class CValid
        Public Property enable As Boolean
        Public Property beginTime As String
        Public Property endTime As String
        '<DefaultValue(Nothing)> _
        Public Property timeType As String
    End Class
    Public Class CUserInfoDetailCfg
        Public Property UserInfoDetail As CUserInfoDetail
    End Class
    Public Class CUserInfoDetail
        Public Property mode As String
        Public Property EmployeeNoList As List(Of CEmployeeNoList)
    End Class
    Public Class CUserInfoDetailDeleteProcessCfg
        Public Property UserInfoDetailDeleteProcess As CUserInfoDetailDeleteProcess
    End Class
    Public Class CUserInfoDetailDeleteProcess
        Public Property status As String
    End Class

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_DVR_CAPTURE_FACE_CFG
        Public dwSize As Integer
        Public dwFaceTemplate1Size As Integer
        Public pFaceTemplate1Buffer As IntPtr
        Public dwFaceTemplate2Size As Integer
        Public pFaceTemplate2Buffer As IntPtr
        Public dwFacePicSize As Integer
        Public pFacePicBuffer As IntPtr
        Public byFaceQuality1 As Byte
        Public byFaceQuality2 As Byte
        Public byCaptureProgress As Byte
        Public byRes1 As Byte
        Public dwInfraredFacePicSize As Integer
        Public pInfraredFacePicBuffer As IntPtr
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=116, ArraySubType:=UnmanagedType.I1)>
        Public byRes As Byte()

        Public Sub init()
            byRes = New Byte(115) {}
        End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_DVR_CAPTURE_FACE_COND
        Public dwSize As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128, ArraySubType:=UnmanagedType.I1)>
        Public byRes As Byte()

        Public Sub init()
            byRes = New Byte(127) {}
        End Sub
    End Structure
    'end face
    Public Class CFingerPrintCondCfg
        Public Property FingerPrintCond As CFingerPrintCond
    End Class

    Public Class CFingerPrintCond
        Public Property searchID As String
        Public Property employeeNo As String
        Public Property cardReaderNo As Integer
        Public Property fingerPrintID As Integer
    End Class
    Public Class CFingerPrintInfoCfg
        Public Property FingerPrintInfo As CFingerPrintInfo
    End Class

    Public Class CFingerPrintInfo
        Public Property searchID As String
        Public Property status As String
        Public Property FingerPrintList As List(Of CFingerPrintList)
    End Class

    Public Class CFingerPrintList
        Public Property cardReaderNo As Integer
        Public Property fingerPrintID As Integer
        Public Property fingerType As String
        Public Property fingerData As String
        Public Property leaderFP As Integer()
    End Class
    Public Class CFingerPrintCfgCfg
        Public Property FingerPrintCfg As CFingerPrintCfg
    End Class

    Public Class CFingerPrintCfg
        Public Property employeeNo As String
        Public Property enableCardReader As Integer()
        Public Property fingerPrintID As Integer
        Public Property deleteFingerPrint As Boolean
        Public Property fingerType As String
        Public Property fingerData As String
        Public Property leaderFP As Integer()
        Public Property checkEmployeeNo As Boolean
    End Class
    Public Class CFingerPrintStatusCfg
        Public Property FingerPrintStatus As CFingerPrintStatus
    End Class

    Public Class CFingerPrintStatus
        Public Property status As String
        Public Property StatusList As List(Of CStatusList)
    End Class
    Public Class CStatusList
        Public Property id As Integer
        Public Property cardReaderRecvStatus As Integer
        Public Property errorMsg As String
    End Class
    Public Class CCardInfoCfg
        Public Property CardInfo As CCardInfo
    End Class

    Public Class CCardInfo
        Public Property employeeNo As String
        Public Property cardNo As String
        Public Property deleteCard As Boolean
        Public Property cardType As String
    End Class
    Public Class CCardInfoSearchCondCfg
        Public Property CardInfoSearchCond As CCardInfoSearchCond
    End Class

    Public Class CCardInfoSearchCond
        Public Property searchID As String '= -1
        '<DefaultValue(-1)>
        Public Property searchResultPosition As Integer
        Public Property maxResults As Integer
        Public Property EmployeeNoList As List(Of CEmployeeNoList)
        Public Property CardNoList As List(Of CCardNoList)
    End Class

    Public Class CCardNoList
        Public Property cardNo As String
    End Class
    Public Class CCardInfoSearchCfg
        Public Property CardInfoSearch As CCardInfoSearch
    End Class

    Public Class CCardInfoSearch
        Public Property searchID As String
        Public Property responseStatusStrg As String
        Public Property numOfMatches As Integer
        Public Property totalMatches As Integer
        Public Property CardInfo As List(Of CCardInfo)
    End Class

    Public Class CUserInfoSearchCfg
        Public Property UserInfoSearch As CUserInfoSearch
    End Class

    Public Class CUserInfoSearch
        Public Property searchID As String
        Public Property responseStatusStrg As String
        Public Property numOfMatches As Integer
        Public Property totalMatches As Integer
        Public Property UserInfo As List(Of CUserInfo)
    End Class

    Public Class CUserInfo
        Public Property employeeNo As String
        Public Property name As String
        Public Property userType As String
        Public Property closeDelayEnabled As Boolean
        Public Property Valid As CValid
        Public Property belongGroup As String
        Public Property password As String
        Public Property doorRight As String
        Public Property RightPlan As List(Of CRightPlan)
        Public Property maxOpenDoorTime As Integer
        Public Property openDoorTime As Integer
        Public Property roomNumber As Integer
        Public Property floorNumber As Integer
        Public Property doubleLockRight As Boolean
        Public Property alwaysOpenRight As Boolean
        Public Property localUIRight As Boolean
        Public Property userVerifyMode As String
    End Class
    Public Class CUserInfoSearchCondCfg
        Public Property UserInfoSearchCond As CUserInfoSearchCond
    End Class

    Public Class CUserInfoSearchCond
        Public Property searchID As String
        Public Property searchResultPosition As Integer
        Public Property maxResults As Integer
        Public Property EmployeeNoList As List(Of CEmployeeNoList)
    End Class

    Public Class CEmployeeNoList
        Public Property employeeNo As String
    End Class

    'for getting user info (not having card number)



End Namespace
