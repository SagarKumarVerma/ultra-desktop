﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraLeaveEncashment
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraLeaveEncashment))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.txtPayableMonth = New DevExpress.XtraEditors.DateEdit()
        Me.ComboNepaliYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New ULtra.SSSDBDataSet()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.TxtLeave20 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeave19 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeave18 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeave17 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeave16 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeave15 = New DevExpress.XtraEditors.TextEdit()
        Me.lbLeave20 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLeave19 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLeave18 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLeave17 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLeave16 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLeave15 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.TxtLeave14 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeave13 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeave12 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeave11 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeave10 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeave9 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeave8 = New DevExpress.XtraEditors.TextEdit()
        Me.lbLeave14 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLeave13 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLeave12 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLeave11 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLeave10 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLeave9 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLeave8 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.TxtLeave7 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeave6 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeave5 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeave4 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeave3 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeave2 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeave1 = New DevExpress.XtraEditors.TextEdit()
        Me.lbLeave7 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLeave6 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLeave5 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLeave4 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLeave3 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLeave2 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLeave1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.txtOnlyYear = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerControl1 = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.EmployeeGroupBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colGroupId = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGroupName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEdit1 = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.EmployeeGroupTableAdapter = New ULtra.SSSDBDataSetTableAdapters.EmployeeGroupTableAdapter()
        Me.EmployeeGroup1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.EmployeeGroup1TableAdapter()
        Me.TblEmployee1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        Me.TblEmployeeTableAdapter = New ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TxtLeaveC7 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC6 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC5 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC4 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC3 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC2 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtLeaveC14 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC13 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC12 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC11 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC10 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC9 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC8 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC20 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC19 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC18 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC17 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC16 = New DevExpress.XtraEditors.TextEdit()
        Me.TxtLeaveC15 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.txtPayableMonth.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPayableMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.TxtLeave20.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeave19.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeave18.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeave17.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeave16.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeave15.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.TxtLeave14.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeave13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeave12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeave11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeave10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeave9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeave8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.TxtLeave7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeave6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeave5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeave4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeave3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeave2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeave1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOnlyYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmployeeGroupBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC14.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC20.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC19.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC18.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC17.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC16.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtLeaveC15.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.LookAndFeel.SkinName = "iMaginary"
        Me.SplitContainerControl1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl15)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.txtPayableMonth)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.ComboNepaliYear)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LookUpEdit1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl14)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl13)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl12)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl11)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl10)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl9)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl8)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl7)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl6)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl5)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl4)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl3)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.TextEdit2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.txtOnlyYear)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControl1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerEdit1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.CheckEdit2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.CheckEdit1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 3
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(627, 44)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(134, 14)
        Me.LabelControl15.TabIndex = 33
        Me.LabelControl15.Text = "Payable In the Month of"
        '
        'txtPayableMonth
        '
        Me.txtPayableMonth.EditValue = Nothing
        Me.txtPayableMonth.Location = New System.Drawing.Point(767, 41)
        Me.txtPayableMonth.Name = "txtPayableMonth"
        Me.txtPayableMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtPayableMonth.Properties.Appearance.Options.UseFont = True
        Me.txtPayableMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtPayableMonth.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtPayableMonth.Properties.Mask.EditMask = "MM/yyyy"
        Me.txtPayableMonth.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPayableMonth.Size = New System.Drawing.Size(100, 20)
        Me.txtPayableMonth.TabIndex = 32
        '
        'ComboNepaliYear
        '
        Me.ComboNepaliYear.Location = New System.Drawing.Point(480, 41)
        Me.ComboNepaliYear.Name = "ComboNepaliYear"
        Me.ComboNepaliYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYear.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYear.Size = New System.Drawing.Size(81, 20)
        Me.ComboNepaliYear.TabIndex = 4
        '
        'LookUpEdit1
        '
        Me.LookUpEdit1.Location = New System.Drawing.Point(130, 41)
        Me.LookUpEdit1.Name = "LookUpEdit1"
        Me.LookUpEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.Appearance.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceFocused.Options.UseFont = True
        Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PAYCODE", "PAYCODE", 30, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EMPNAME", "Name")})
        Me.LookUpEdit1.Properties.DataSource = Me.TblEmployeeBindingSource
        Me.LookUpEdit1.Properties.DisplayMember = "PAYCODE"
        Me.LookUpEdit1.Properties.MaxLength = 12
        Me.LookUpEdit1.Properties.NullText = ""
        Me.LookUpEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.LookUpEdit1.Properties.ValueMember = "PAYCODE"
        Me.LookUpEdit1.Size = New System.Drawing.Size(163, 20)
        Me.LookUpEdit1.TabIndex = 31
        Me.LookUpEdit1.Visible = False
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.GridControl2)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel1.Location = New System.Drawing.Point(0, 463)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1036, 105)
        Me.SidePanel1.TabIndex = 30
        Me.SidePanel1.Text = "SidePanel1"
        Me.SidePanel1.Visible = False
        '
        'GridControl2
        '
        Me.GridControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl2.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.Remove.Visible = False
        Me.GridControl2.Location = New System.Drawing.Point(0, 1)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(1036, 104)
        Me.GridControl2.TabIndex = 0
        Me.GridControl2.UseEmbeddedNavigator = True
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsCustomization.CustomizationFormSnapMode = DevExpress.Utils.Controls.SnapMode.None
        Me.GridView2.OptionsView.ShowGroupPanel = False
        Me.GridView2.OptionsView.ShowViewCaption = True
        Me.GridView2.ViewCaption = "Employee group opening balance "
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(811, 262)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 29
        Me.SimpleButton1.Text = "Save"
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.PanelControl3)
        Me.GroupControl1.Controls.Add(Me.PanelControl2)
        Me.GroupControl1.Controls.Add(Me.PanelControl1)
        Me.GroupControl1.Controls.Add(Me.SimpleButton1)
        Me.GroupControl1.Location = New System.Drawing.Point(11, 151)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(911, 306)
        Me.GroupControl1.TabIndex = 28
        Me.GroupControl1.Text = "Leave Balance"
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.LabelControl20)
        Me.PanelControl3.Controls.Add(Me.LabelControl21)
        Me.PanelControl3.Controls.Add(Me.TxtLeaveC20)
        Me.PanelControl3.Controls.Add(Me.TxtLeaveC19)
        Me.PanelControl3.Controls.Add(Me.TxtLeaveC18)
        Me.PanelControl3.Controls.Add(Me.TxtLeaveC17)
        Me.PanelControl3.Controls.Add(Me.TxtLeaveC16)
        Me.PanelControl3.Controls.Add(Me.TxtLeaveC15)
        Me.PanelControl3.Controls.Add(Me.TxtLeave20)
        Me.PanelControl3.Controls.Add(Me.TxtLeave19)
        Me.PanelControl3.Controls.Add(Me.TxtLeave18)
        Me.PanelControl3.Controls.Add(Me.TxtLeave17)
        Me.PanelControl3.Controls.Add(Me.TxtLeave16)
        Me.PanelControl3.Controls.Add(Me.TxtLeave15)
        Me.PanelControl3.Controls.Add(Me.lbLeave20)
        Me.PanelControl3.Controls.Add(Me.lbLeave19)
        Me.PanelControl3.Controls.Add(Me.lbLeave18)
        Me.PanelControl3.Controls.Add(Me.lbLeave17)
        Me.PanelControl3.Controls.Add(Me.lbLeave16)
        Me.PanelControl3.Controls.Add(Me.lbLeave15)
        Me.PanelControl3.Location = New System.Drawing.Point(612, 26)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(295, 230)
        Me.PanelControl3.TabIndex = 33
        Me.PanelControl3.Visible = False
        '
        'TxtLeave20
        '
        Me.TxtLeave20.EditValue = "000.00"
        Me.TxtLeave20.Location = New System.Drawing.Point(134, 162)
        Me.TxtLeave20.Name = "TxtLeave20"
        Me.TxtLeave20.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave20.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave20.Properties.Mask.EditMask = "f"
        Me.TxtLeave20.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave20.Properties.MaxLength = 6
        Me.TxtLeave20.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave20.TabIndex = 31
        Me.TxtLeave20.Visible = False
        '
        'TxtLeave19
        '
        Me.TxtLeave19.EditValue = "000.00"
        Me.TxtLeave19.Location = New System.Drawing.Point(134, 136)
        Me.TxtLeave19.Name = "TxtLeave19"
        Me.TxtLeave19.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave19.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave19.Properties.Mask.EditMask = "f"
        Me.TxtLeave19.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave19.Properties.MaxLength = 6
        Me.TxtLeave19.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave19.TabIndex = 30
        Me.TxtLeave19.Visible = False
        '
        'TxtLeave18
        '
        Me.TxtLeave18.EditValue = "000.00"
        Me.TxtLeave18.Location = New System.Drawing.Point(134, 110)
        Me.TxtLeave18.Name = "TxtLeave18"
        Me.TxtLeave18.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave18.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave18.Properties.Mask.EditMask = "f"
        Me.TxtLeave18.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave18.Properties.MaxLength = 6
        Me.TxtLeave18.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave18.TabIndex = 29
        Me.TxtLeave18.Visible = False
        '
        'TxtLeave17
        '
        Me.TxtLeave17.EditValue = "000.00"
        Me.TxtLeave17.Location = New System.Drawing.Point(134, 84)
        Me.TxtLeave17.Name = "TxtLeave17"
        Me.TxtLeave17.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave17.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave17.Properties.Mask.EditMask = "f"
        Me.TxtLeave17.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave17.Properties.MaxLength = 6
        Me.TxtLeave17.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave17.TabIndex = 28
        Me.TxtLeave17.Visible = False
        '
        'TxtLeave16
        '
        Me.TxtLeave16.EditValue = "000.00"
        Me.TxtLeave16.Location = New System.Drawing.Point(134, 58)
        Me.TxtLeave16.Name = "TxtLeave16"
        Me.TxtLeave16.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave16.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave16.Properties.Mask.EditMask = "f"
        Me.TxtLeave16.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave16.Properties.MaxLength = 6
        Me.TxtLeave16.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave16.TabIndex = 27
        Me.TxtLeave16.Visible = False
        '
        'TxtLeave15
        '
        Me.TxtLeave15.EditValue = "000.00"
        Me.TxtLeave15.Location = New System.Drawing.Point(134, 32)
        Me.TxtLeave15.Name = "TxtLeave15"
        Me.TxtLeave15.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave15.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave15.Properties.Mask.EditMask = "f"
        Me.TxtLeave15.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave15.Properties.MaxLength = 6
        Me.TxtLeave15.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave15.TabIndex = 26
        Me.TxtLeave15.Visible = False
        '
        'lbLeave20
        '
        Me.lbLeave20.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave20.Appearance.Options.UseFont = True
        Me.lbLeave20.Location = New System.Drawing.Point(12, 165)
        Me.lbLeave20.Name = "lbLeave20"
        Me.lbLeave20.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave20.TabIndex = 24
        Me.lbLeave20.Text = "Leave"
        Me.lbLeave20.Visible = False
        '
        'lbLeave19
        '
        Me.lbLeave19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave19.Appearance.Options.UseFont = True
        Me.lbLeave19.Location = New System.Drawing.Point(13, 139)
        Me.lbLeave19.Name = "lbLeave19"
        Me.lbLeave19.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave19.TabIndex = 23
        Me.lbLeave19.Text = "Leave"
        Me.lbLeave19.Visible = False
        '
        'lbLeave18
        '
        Me.lbLeave18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave18.Appearance.Options.UseFont = True
        Me.lbLeave18.Location = New System.Drawing.Point(13, 113)
        Me.lbLeave18.Name = "lbLeave18"
        Me.lbLeave18.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave18.TabIndex = 22
        Me.lbLeave18.Text = "Leave"
        Me.lbLeave18.Visible = False
        '
        'lbLeave17
        '
        Me.lbLeave17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave17.Appearance.Options.UseFont = True
        Me.lbLeave17.Location = New System.Drawing.Point(12, 87)
        Me.lbLeave17.Name = "lbLeave17"
        Me.lbLeave17.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave17.TabIndex = 21
        Me.lbLeave17.Text = "Leave"
        Me.lbLeave17.Visible = False
        '
        'lbLeave16
        '
        Me.lbLeave16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave16.Appearance.Options.UseFont = True
        Me.lbLeave16.Location = New System.Drawing.Point(12, 61)
        Me.lbLeave16.Name = "lbLeave16"
        Me.lbLeave16.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave16.TabIndex = 20
        Me.lbLeave16.Text = "Leave"
        Me.lbLeave16.Visible = False
        '
        'lbLeave15
        '
        Me.lbLeave15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave15.Appearance.Options.UseFont = True
        Me.lbLeave15.Location = New System.Drawing.Point(13, 35)
        Me.lbLeave15.Name = "lbLeave15"
        Me.lbLeave15.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave15.TabIndex = 19
        Me.lbLeave15.Text = "Leave"
        Me.lbLeave15.Visible = False
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.LabelControl18)
        Me.PanelControl2.Controls.Add(Me.LabelControl19)
        Me.PanelControl2.Controls.Add(Me.TxtLeaveC14)
        Me.PanelControl2.Controls.Add(Me.TxtLeaveC13)
        Me.PanelControl2.Controls.Add(Me.TxtLeaveC12)
        Me.PanelControl2.Controls.Add(Me.TxtLeaveC11)
        Me.PanelControl2.Controls.Add(Me.TxtLeaveC10)
        Me.PanelControl2.Controls.Add(Me.TxtLeaveC9)
        Me.PanelControl2.Controls.Add(Me.TxtLeaveC8)
        Me.PanelControl2.Controls.Add(Me.TxtLeave14)
        Me.PanelControl2.Controls.Add(Me.TxtLeave13)
        Me.PanelControl2.Controls.Add(Me.TxtLeave12)
        Me.PanelControl2.Controls.Add(Me.TxtLeave11)
        Me.PanelControl2.Controls.Add(Me.TxtLeave10)
        Me.PanelControl2.Controls.Add(Me.TxtLeave9)
        Me.PanelControl2.Controls.Add(Me.TxtLeave8)
        Me.PanelControl2.Controls.Add(Me.lbLeave14)
        Me.PanelControl2.Controls.Add(Me.lbLeave13)
        Me.PanelControl2.Controls.Add(Me.lbLeave12)
        Me.PanelControl2.Controls.Add(Me.lbLeave11)
        Me.PanelControl2.Controls.Add(Me.lbLeave10)
        Me.PanelControl2.Controls.Add(Me.lbLeave9)
        Me.PanelControl2.Controls.Add(Me.lbLeave8)
        Me.PanelControl2.Location = New System.Drawing.Point(311, 26)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(295, 230)
        Me.PanelControl2.TabIndex = 20
        Me.PanelControl2.Visible = False
        '
        'TxtLeave14
        '
        Me.TxtLeave14.EditValue = "000.00"
        Me.TxtLeave14.Location = New System.Drawing.Point(126, 188)
        Me.TxtLeave14.Name = "TxtLeave14"
        Me.TxtLeave14.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave14.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave14.Properties.Mask.EditMask = "f"
        Me.TxtLeave14.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave14.Properties.MaxLength = 6
        Me.TxtLeave14.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave14.TabIndex = 32
        Me.TxtLeave14.Visible = False
        '
        'TxtLeave13
        '
        Me.TxtLeave13.EditValue = "000.00"
        Me.TxtLeave13.Location = New System.Drawing.Point(126, 162)
        Me.TxtLeave13.Name = "TxtLeave13"
        Me.TxtLeave13.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave13.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave13.Properties.Mask.EditMask = "f"
        Me.TxtLeave13.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave13.Properties.MaxLength = 6
        Me.TxtLeave13.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave13.TabIndex = 31
        Me.TxtLeave13.Visible = False
        '
        'TxtLeave12
        '
        Me.TxtLeave12.EditValue = "000.00"
        Me.TxtLeave12.Location = New System.Drawing.Point(126, 136)
        Me.TxtLeave12.Name = "TxtLeave12"
        Me.TxtLeave12.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave12.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave12.Properties.Mask.EditMask = "f"
        Me.TxtLeave12.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave12.Properties.MaxLength = 6
        Me.TxtLeave12.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave12.TabIndex = 30
        Me.TxtLeave12.Visible = False
        '
        'TxtLeave11
        '
        Me.TxtLeave11.EditValue = "000.00"
        Me.TxtLeave11.Location = New System.Drawing.Point(126, 110)
        Me.TxtLeave11.Name = "TxtLeave11"
        Me.TxtLeave11.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave11.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave11.Properties.Mask.EditMask = "f"
        Me.TxtLeave11.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave11.Properties.MaxLength = 6
        Me.TxtLeave11.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave11.TabIndex = 29
        Me.TxtLeave11.Visible = False
        '
        'TxtLeave10
        '
        Me.TxtLeave10.EditValue = "000.00"
        Me.TxtLeave10.Location = New System.Drawing.Point(126, 84)
        Me.TxtLeave10.Name = "TxtLeave10"
        Me.TxtLeave10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave10.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave10.Properties.Mask.EditMask = "f"
        Me.TxtLeave10.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave10.Properties.MaxLength = 6
        Me.TxtLeave10.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave10.TabIndex = 28
        Me.TxtLeave10.Visible = False
        '
        'TxtLeave9
        '
        Me.TxtLeave9.EditValue = "000.00"
        Me.TxtLeave9.Location = New System.Drawing.Point(126, 58)
        Me.TxtLeave9.Name = "TxtLeave9"
        Me.TxtLeave9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave9.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave9.Properties.Mask.EditMask = "f"
        Me.TxtLeave9.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave9.Properties.MaxLength = 6
        Me.TxtLeave9.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave9.TabIndex = 27
        Me.TxtLeave9.Visible = False
        '
        'TxtLeave8
        '
        Me.TxtLeave8.EditValue = "000.00"
        Me.TxtLeave8.Location = New System.Drawing.Point(126, 32)
        Me.TxtLeave8.Name = "TxtLeave8"
        Me.TxtLeave8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave8.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave8.Properties.Mask.EditMask = "f"
        Me.TxtLeave8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave8.Properties.MaxLength = 6
        Me.TxtLeave8.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave8.TabIndex = 26
        Me.TxtLeave8.Visible = False
        '
        'lbLeave14
        '
        Me.lbLeave14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave14.Appearance.Options.UseFont = True
        Me.lbLeave14.Location = New System.Drawing.Point(14, 191)
        Me.lbLeave14.Name = "lbLeave14"
        Me.lbLeave14.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave14.TabIndex = 25
        Me.lbLeave14.Text = "Leave"
        Me.lbLeave14.Visible = False
        '
        'lbLeave13
        '
        Me.lbLeave13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave13.Appearance.Options.UseFont = True
        Me.lbLeave13.Location = New System.Drawing.Point(13, 166)
        Me.lbLeave13.Name = "lbLeave13"
        Me.lbLeave13.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave13.TabIndex = 24
        Me.lbLeave13.Text = "Leave"
        Me.lbLeave13.Visible = False
        '
        'lbLeave12
        '
        Me.lbLeave12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave12.Appearance.Options.UseFont = True
        Me.lbLeave12.Location = New System.Drawing.Point(14, 139)
        Me.lbLeave12.Name = "lbLeave12"
        Me.lbLeave12.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave12.TabIndex = 23
        Me.lbLeave12.Text = "Leave"
        Me.lbLeave12.Visible = False
        '
        'lbLeave11
        '
        Me.lbLeave11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave11.Appearance.Options.UseFont = True
        Me.lbLeave11.Location = New System.Drawing.Point(14, 113)
        Me.lbLeave11.Name = "lbLeave11"
        Me.lbLeave11.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave11.TabIndex = 22
        Me.lbLeave11.Text = "Leave"
        Me.lbLeave11.Visible = False
        '
        'lbLeave10
        '
        Me.lbLeave10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave10.Appearance.Options.UseFont = True
        Me.lbLeave10.Location = New System.Drawing.Point(14, 87)
        Me.lbLeave10.Name = "lbLeave10"
        Me.lbLeave10.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave10.TabIndex = 21
        Me.lbLeave10.Text = "Leave"
        Me.lbLeave10.Visible = False
        '
        'lbLeave9
        '
        Me.lbLeave9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave9.Appearance.Options.UseFont = True
        Me.lbLeave9.Location = New System.Drawing.Point(13, 61)
        Me.lbLeave9.Name = "lbLeave9"
        Me.lbLeave9.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave9.TabIndex = 20
        Me.lbLeave9.Text = "Leave"
        Me.lbLeave9.Visible = False
        '
        'lbLeave8
        '
        Me.lbLeave8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave8.Appearance.Options.UseFont = True
        Me.lbLeave8.Location = New System.Drawing.Point(14, 35)
        Me.lbLeave8.Name = "lbLeave8"
        Me.lbLeave8.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave8.TabIndex = 19
        Me.lbLeave8.Text = "Leave"
        Me.lbLeave8.Visible = False
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.LabelControl17)
        Me.PanelControl1.Controls.Add(Me.LabelControl16)
        Me.PanelControl1.Controls.Add(Me.TxtLeaveC7)
        Me.PanelControl1.Controls.Add(Me.TxtLeaveC6)
        Me.PanelControl1.Controls.Add(Me.TxtLeaveC5)
        Me.PanelControl1.Controls.Add(Me.TxtLeaveC4)
        Me.PanelControl1.Controls.Add(Me.TxtLeaveC3)
        Me.PanelControl1.Controls.Add(Me.TxtLeaveC2)
        Me.PanelControl1.Controls.Add(Me.TxtLeaveC1)
        Me.PanelControl1.Controls.Add(Me.TxtLeave7)
        Me.PanelControl1.Controls.Add(Me.TxtLeave6)
        Me.PanelControl1.Controls.Add(Me.TxtLeave5)
        Me.PanelControl1.Controls.Add(Me.TxtLeave4)
        Me.PanelControl1.Controls.Add(Me.TxtLeave3)
        Me.PanelControl1.Controls.Add(Me.TxtLeave2)
        Me.PanelControl1.Controls.Add(Me.TxtLeave1)
        Me.PanelControl1.Controls.Add(Me.lbLeave7)
        Me.PanelControl1.Controls.Add(Me.lbLeave6)
        Me.PanelControl1.Controls.Add(Me.lbLeave5)
        Me.PanelControl1.Controls.Add(Me.lbLeave4)
        Me.PanelControl1.Controls.Add(Me.lbLeave3)
        Me.PanelControl1.Controls.Add(Me.lbLeave2)
        Me.PanelControl1.Controls.Add(Me.lbLeave1)
        Me.PanelControl1.Location = New System.Drawing.Point(10, 26)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(295, 230)
        Me.PanelControl1.TabIndex = 19
        '
        'TxtLeave7
        '
        Me.TxtLeave7.EditValue = "000.00"
        Me.TxtLeave7.Location = New System.Drawing.Point(127, 188)
        Me.TxtLeave7.Name = "TxtLeave7"
        Me.TxtLeave7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave7.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave7.Properties.Mask.EditMask = "f"
        Me.TxtLeave7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave7.Properties.MaxLength = 6
        Me.TxtLeave7.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave7.TabIndex = 32
        Me.TxtLeave7.Visible = False
        '
        'TxtLeave6
        '
        Me.TxtLeave6.EditValue = "000.00"
        Me.TxtLeave6.Location = New System.Drawing.Point(127, 162)
        Me.TxtLeave6.Name = "TxtLeave6"
        Me.TxtLeave6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave6.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave6.Properties.Mask.EditMask = "f"
        Me.TxtLeave6.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave6.Properties.MaxLength = 6
        Me.TxtLeave6.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave6.TabIndex = 31
        Me.TxtLeave6.Visible = False
        '
        'TxtLeave5
        '
        Me.TxtLeave5.EditValue = "000.00"
        Me.TxtLeave5.Location = New System.Drawing.Point(127, 136)
        Me.TxtLeave5.Name = "TxtLeave5"
        Me.TxtLeave5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave5.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave5.Properties.Mask.EditMask = "f"
        Me.TxtLeave5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave5.Properties.MaxLength = 6
        Me.TxtLeave5.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave5.TabIndex = 30
        Me.TxtLeave5.Visible = False
        '
        'TxtLeave4
        '
        Me.TxtLeave4.EditValue = "000.00"
        Me.TxtLeave4.Location = New System.Drawing.Point(127, 110)
        Me.TxtLeave4.Name = "TxtLeave4"
        Me.TxtLeave4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave4.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave4.Properties.Mask.EditMask = "f"
        Me.TxtLeave4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave4.Properties.MaxLength = 6
        Me.TxtLeave4.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave4.TabIndex = 29
        Me.TxtLeave4.Visible = False
        '
        'TxtLeave3
        '
        Me.TxtLeave3.EditValue = "000.00"
        Me.TxtLeave3.Location = New System.Drawing.Point(127, 84)
        Me.TxtLeave3.Name = "TxtLeave3"
        Me.TxtLeave3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave3.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave3.Properties.Mask.EditMask = "f"
        Me.TxtLeave3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave3.Properties.MaxLength = 6
        Me.TxtLeave3.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave3.TabIndex = 28
        Me.TxtLeave3.Visible = False
        '
        'TxtLeave2
        '
        Me.TxtLeave2.EditValue = "000.00"
        Me.TxtLeave2.Location = New System.Drawing.Point(127, 58)
        Me.TxtLeave2.Name = "TxtLeave2"
        Me.TxtLeave2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave2.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave2.Properties.Mask.EditMask = "f"
        Me.TxtLeave2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave2.Properties.MaxLength = 6
        Me.TxtLeave2.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave2.TabIndex = 27
        Me.TxtLeave2.Visible = False
        '
        'TxtLeave1
        '
        Me.TxtLeave1.EditValue = "000.00"
        Me.TxtLeave1.Location = New System.Drawing.Point(127, 32)
        Me.TxtLeave1.Name = "TxtLeave1"
        Me.TxtLeave1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeave1.Properties.Appearance.Options.UseFont = True
        Me.TxtLeave1.Properties.Mask.EditMask = "f"
        Me.TxtLeave1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeave1.Properties.MaxLength = 6
        Me.TxtLeave1.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeave1.TabIndex = 26
        Me.TxtLeave1.Visible = False
        '
        'lbLeave7
        '
        Me.lbLeave7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave7.Appearance.Options.UseFont = True
        Me.lbLeave7.Location = New System.Drawing.Point(12, 191)
        Me.lbLeave7.Name = "lbLeave7"
        Me.lbLeave7.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave7.TabIndex = 25
        Me.lbLeave7.Text = "Leave"
        Me.lbLeave7.Visible = False
        '
        'lbLeave6
        '
        Me.lbLeave6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave6.Appearance.Options.UseFont = True
        Me.lbLeave6.Location = New System.Drawing.Point(11, 166)
        Me.lbLeave6.Name = "lbLeave6"
        Me.lbLeave6.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave6.TabIndex = 24
        Me.lbLeave6.Text = "Leave"
        Me.lbLeave6.Visible = False
        '
        'lbLeave5
        '
        Me.lbLeave5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave5.Appearance.Options.UseFont = True
        Me.lbLeave5.Location = New System.Drawing.Point(11, 139)
        Me.lbLeave5.Name = "lbLeave5"
        Me.lbLeave5.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave5.TabIndex = 23
        Me.lbLeave5.Text = "Leave"
        Me.lbLeave5.Visible = False
        '
        'lbLeave4
        '
        Me.lbLeave4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave4.Appearance.Options.UseFont = True
        Me.lbLeave4.Location = New System.Drawing.Point(12, 113)
        Me.lbLeave4.Name = "lbLeave4"
        Me.lbLeave4.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave4.TabIndex = 22
        Me.lbLeave4.Text = "Leave"
        Me.lbLeave4.Visible = False
        '
        'lbLeave3
        '
        Me.lbLeave3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave3.Appearance.Options.UseFont = True
        Me.lbLeave3.Location = New System.Drawing.Point(11, 87)
        Me.lbLeave3.Name = "lbLeave3"
        Me.lbLeave3.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave3.TabIndex = 21
        Me.lbLeave3.Text = "Leave"
        Me.lbLeave3.Visible = False
        '
        'lbLeave2
        '
        Me.lbLeave2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave2.Appearance.Options.UseFont = True
        Me.lbLeave2.Location = New System.Drawing.Point(11, 61)
        Me.lbLeave2.Name = "lbLeave2"
        Me.lbLeave2.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave2.TabIndex = 20
        Me.lbLeave2.Text = "Leave"
        Me.lbLeave2.Visible = False
        '
        'lbLeave1
        '
        Me.lbLeave1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lbLeave1.Appearance.Options.UseFont = True
        Me.lbLeave1.Location = New System.Drawing.Point(12, 35)
        Me.lbLeave1.Name = "lbLeave1"
        Me.lbLeave1.Size = New System.Drawing.Size(32, 14)
        Me.lbLeave1.TabIndex = 19
        Me.lbLeave1.Text = "Leave"
        Me.lbLeave1.Visible = False
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Appearance.Options.UseForeColor = True
        Me.LabelControl14.Location = New System.Drawing.Point(523, 123)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl14.TabIndex = 27
        Me.LabelControl14.Text = "     "
        Me.LabelControl14.Visible = False
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(430, 123)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(63, 14)
        Me.LabelControl13.TabIndex = 26
        Me.LabelControl13.Text = "Designation"
        Me.LabelControl13.Visible = False
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Appearance.Options.UseForeColor = True
        Me.LabelControl12.Location = New System.Drawing.Point(130, 123)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl12.TabIndex = 25
        Me.LabelControl12.Text = "     "
        Me.LabelControl12.Visible = False
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(21, 123)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(48, 14)
        Me.LabelControl11.TabIndex = 24
        Me.LabelControl11.Text = "Catagory"
        Me.LabelControl11.Visible = False
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Appearance.Options.UseForeColor = True
        Me.LabelControl10.Location = New System.Drawing.Point(523, 95)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(24, 14)
        Me.LabelControl10.TabIndex = 23
        Me.LabelControl10.Text = "      "
        Me.LabelControl10.Visible = False
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(430, 95)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl9.TabIndex = 22
        Me.LabelControl9.Text = "Department"
        Me.LabelControl9.Visible = False
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Appearance.Options.UseForeColor = True
        Me.LabelControl8.Location = New System.Drawing.Point(130, 95)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl8.TabIndex = 21
        Me.LabelControl8.Text = "     "
        Me.LabelControl8.Visible = False
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(21, 95)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(90, 14)
        Me.LabelControl7.TabIndex = 20
        Me.LabelControl7.Text = "Employee Group"
        Me.LabelControl7.Visible = False
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Appearance.Options.UseForeColor = True
        Me.LabelControl6.Location = New System.Drawing.Point(523, 69)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(16, 14)
        Me.LabelControl6.TabIndex = 19
        Me.LabelControl6.Text = "    "
        Me.LabelControl6.Visible = False
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(430, 69)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(47, 14)
        Me.LabelControl5.TabIndex = 18
        Me.LabelControl5.Text = "Card No."
        Me.LabelControl5.Visible = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Appearance.Options.UseForeColor = True
        Me.LabelControl4.Location = New System.Drawing.Point(130, 69)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl4.TabIndex = 17
        Me.LabelControl4.Text = "     "
        Me.LabelControl4.Visible = False
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(21, 69)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(31, 14)
        Me.LabelControl3.TabIndex = 16
        Me.LabelControl3.Text = "Name"
        Me.LabelControl3.Visible = False
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(383, 18)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit2.Properties.Appearance.Options.UseFont = True
        Me.TextEdit2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit2.Properties.MaxLength = 12
        Me.TextEdit2.Size = New System.Drawing.Size(129, 20)
        Me.TextEdit2.TabIndex = 3
        Me.TextEdit2.Visible = False
        '
        'txtOnlyYear
        '
        Me.txtOnlyYear.Location = New System.Drawing.Point(480, 41)
        Me.txtOnlyYear.Name = "txtOnlyYear"
        Me.txtOnlyYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtOnlyYear.Properties.Appearance.Options.UseFont = True
        Me.txtOnlyYear.Properties.Mask.EditMask = "[0-9]*"
        Me.txtOnlyYear.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtOnlyYear.Properties.MaxLength = 4
        Me.txtOnlyYear.Size = New System.Drawing.Size(85, 20)
        Me.txtOnlyYear.TabIndex = 4
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(430, 44)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(25, 14)
        Me.LabelControl2.TabIndex = 13
        Me.LabelControl2.Text = "Year"
        '
        'PopupContainerControl1
        '
        Me.PopupContainerControl1.Controls.Add(Me.GridControl1)
        Me.PopupContainerControl1.Location = New System.Drawing.Point(796, 107)
        Me.PopupContainerControl1.Name = "PopupContainerControl1"
        Me.PopupContainerControl1.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControl1.TabIndex = 12
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.EmployeeGroupBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit1})
        Me.GridControl1.Size = New System.Drawing.Size(300, 300)
        Me.GridControl1.TabIndex = 6
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'EmployeeGroupBindingSource
        '
        Me.EmployeeGroupBindingSource.DataMember = "EmployeeGroup"
        Me.EmployeeGroupBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colGroupId, Me.colGroupName})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colGroupId, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colGroupId
        '
        Me.colGroupId.FieldName = "GroupId"
        Me.colGroupId.Name = "colGroupId"
        Me.colGroupId.Visible = True
        Me.colGroupId.VisibleIndex = 1
        '
        'colGroupName
        '
        Me.colGroupName.FieldName = "GroupName"
        Me.colGroupName.Name = "colGroupName"
        Me.colGroupName.Visible = True
        Me.colGroupName.VisibleIndex = 2
        Me.colGroupName.Width = 112
        '
        'RepositoryItemTimeEdit1
        '
        Me.RepositoryItemTimeEdit1.AutoHeight = False
        Me.RepositoryItemTimeEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit1.Name = "RepositoryItemTimeEdit1"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(21, 44)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(71, 14)
        Me.LabelControl1.TabIndex = 11
        Me.LabelControl1.Text = "Select Group"
        '
        'PopupContainerEdit1
        '
        Me.PopupContainerEdit1.Location = New System.Drawing.Point(130, 41)
        Me.PopupContainerEdit1.Name = "PopupContainerEdit1"
        Me.PopupContainerEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEdit1.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEdit1.Properties.PopupControl = Me.PopupContainerControl1
        Me.PopupContainerEdit1.Size = New System.Drawing.Size(215, 20)
        Me.PopupContainerEdit1.TabIndex = 2
        '
        'CheckEdit2
        '
        Me.CheckEdit2.EditValue = True
        Me.CheckEdit2.Location = New System.Drawing.Point(222, 16)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit2.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit2.Properties.Caption = "Employee Wise"
        Me.CheckEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit2.Properties.RadioGroupIndex = 0
        Me.CheckEdit2.Size = New System.Drawing.Size(155, 19)
        Me.CheckEdit2.TabIndex = 1
        Me.CheckEdit2.Visible = False
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(15, 16)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit1.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit1.Properties.Caption = "Employee Group Wise"
        Me.CheckEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit1.Properties.RadioGroupIndex = 0
        Me.CheckEdit1.Size = New System.Drawing.Size(155, 19)
        Me.CheckEdit1.TabIndex = 0
        Me.CheckEdit1.TabStop = False
        Me.CheckEdit1.Visible = False
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(101, 568)
        Me.MemoEdit1.TabIndex = 1
        '
        'EmployeeGroupTableAdapter
        '
        Me.EmployeeGroupTableAdapter.ClearBeforeFill = True
        '
        'EmployeeGroup1TableAdapter1
        '
        Me.EmployeeGroup1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TxtLeaveC7
        '
        Me.TxtLeaveC7.EditValue = "000.00"
        Me.TxtLeaveC7.Location = New System.Drawing.Point(203, 188)
        Me.TxtLeaveC7.Name = "TxtLeaveC7"
        Me.TxtLeaveC7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC7.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC7.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC7.Properties.MaxLength = 6
        Me.TxtLeaveC7.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC7.TabIndex = 39
        Me.TxtLeaveC7.Visible = False
        '
        'TxtLeaveC6
        '
        Me.TxtLeaveC6.EditValue = "000.00"
        Me.TxtLeaveC6.Location = New System.Drawing.Point(203, 162)
        Me.TxtLeaveC6.Name = "TxtLeaveC6"
        Me.TxtLeaveC6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC6.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC6.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC6.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC6.Properties.MaxLength = 6
        Me.TxtLeaveC6.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC6.TabIndex = 38
        Me.TxtLeaveC6.Visible = False
        '
        'TxtLeaveC5
        '
        Me.TxtLeaveC5.EditValue = "000.00"
        Me.TxtLeaveC5.Location = New System.Drawing.Point(203, 136)
        Me.TxtLeaveC5.Name = "TxtLeaveC5"
        Me.TxtLeaveC5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC5.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC5.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC5.Properties.MaxLength = 6
        Me.TxtLeaveC5.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC5.TabIndex = 37
        Me.TxtLeaveC5.Visible = False
        '
        'TxtLeaveC4
        '
        Me.TxtLeaveC4.EditValue = "000.00"
        Me.TxtLeaveC4.Location = New System.Drawing.Point(203, 110)
        Me.TxtLeaveC4.Name = "TxtLeaveC4"
        Me.TxtLeaveC4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC4.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC4.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC4.Properties.MaxLength = 6
        Me.TxtLeaveC4.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC4.TabIndex = 36
        Me.TxtLeaveC4.Visible = False
        '
        'TxtLeaveC3
        '
        Me.TxtLeaveC3.EditValue = "000.00"
        Me.TxtLeaveC3.Location = New System.Drawing.Point(203, 84)
        Me.TxtLeaveC3.Name = "TxtLeaveC3"
        Me.TxtLeaveC3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC3.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC3.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC3.Properties.MaxLength = 6
        Me.TxtLeaveC3.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC3.TabIndex = 35
        Me.TxtLeaveC3.Visible = False
        '
        'TxtLeaveC2
        '
        Me.TxtLeaveC2.EditValue = "000.00"
        Me.TxtLeaveC2.Location = New System.Drawing.Point(203, 58)
        Me.TxtLeaveC2.Name = "TxtLeaveC2"
        Me.TxtLeaveC2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC2.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC2.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC2.Properties.MaxLength = 6
        Me.TxtLeaveC2.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC2.TabIndex = 34
        Me.TxtLeaveC2.Visible = False
        '
        'TxtLeaveC1
        '
        Me.TxtLeaveC1.EditValue = "000.00"
        Me.TxtLeaveC1.Location = New System.Drawing.Point(203, 32)
        Me.TxtLeaveC1.Name = "TxtLeaveC1"
        Me.TxtLeaveC1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC1.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC1.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC1.Properties.MaxLength = 6
        Me.TxtLeaveC1.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC1.TabIndex = 33
        Me.TxtLeaveC1.Visible = False
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(136, 12)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(41, 14)
        Me.LabelControl16.TabIndex = 40
        Me.LabelControl16.Text = "Balance"
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(216, 12)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(38, 14)
        Me.LabelControl17.TabIndex = 41
        Me.LabelControl17.Text = "Encash"
        '
        'TxtLeaveC14
        '
        Me.TxtLeaveC14.EditValue = "000.00"
        Me.TxtLeaveC14.Location = New System.Drawing.Point(202, 188)
        Me.TxtLeaveC14.Name = "TxtLeaveC14"
        Me.TxtLeaveC14.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC14.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC14.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC14.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC14.Properties.MaxLength = 6
        Me.TxtLeaveC14.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC14.TabIndex = 39
        Me.TxtLeaveC14.Visible = False
        '
        'TxtLeaveC13
        '
        Me.TxtLeaveC13.EditValue = "000.00"
        Me.TxtLeaveC13.Location = New System.Drawing.Point(202, 162)
        Me.TxtLeaveC13.Name = "TxtLeaveC13"
        Me.TxtLeaveC13.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC13.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC13.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC13.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC13.Properties.MaxLength = 6
        Me.TxtLeaveC13.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC13.TabIndex = 38
        Me.TxtLeaveC13.Visible = False
        '
        'TxtLeaveC12
        '
        Me.TxtLeaveC12.EditValue = "000.00"
        Me.TxtLeaveC12.Location = New System.Drawing.Point(202, 136)
        Me.TxtLeaveC12.Name = "TxtLeaveC12"
        Me.TxtLeaveC12.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC12.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC12.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC12.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC12.Properties.MaxLength = 6
        Me.TxtLeaveC12.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC12.TabIndex = 37
        Me.TxtLeaveC12.Visible = False
        '
        'TxtLeaveC11
        '
        Me.TxtLeaveC11.EditValue = "000.00"
        Me.TxtLeaveC11.Location = New System.Drawing.Point(202, 110)
        Me.TxtLeaveC11.Name = "TxtLeaveC11"
        Me.TxtLeaveC11.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC11.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC11.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC11.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC11.Properties.MaxLength = 6
        Me.TxtLeaveC11.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC11.TabIndex = 36
        Me.TxtLeaveC11.Visible = False
        '
        'TxtLeaveC10
        '
        Me.TxtLeaveC10.EditValue = "000.00"
        Me.TxtLeaveC10.Location = New System.Drawing.Point(202, 84)
        Me.TxtLeaveC10.Name = "TxtLeaveC10"
        Me.TxtLeaveC10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC10.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC10.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC10.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC10.Properties.MaxLength = 6
        Me.TxtLeaveC10.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC10.TabIndex = 35
        Me.TxtLeaveC10.Visible = False
        '
        'TxtLeaveC9
        '
        Me.TxtLeaveC9.EditValue = "000.00"
        Me.TxtLeaveC9.Location = New System.Drawing.Point(202, 58)
        Me.TxtLeaveC9.Name = "TxtLeaveC9"
        Me.TxtLeaveC9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC9.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC9.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC9.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC9.Properties.MaxLength = 6
        Me.TxtLeaveC9.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC9.TabIndex = 34
        Me.TxtLeaveC9.Visible = False
        '
        'TxtLeaveC8
        '
        Me.TxtLeaveC8.EditValue = "000.00"
        Me.TxtLeaveC8.Location = New System.Drawing.Point(202, 32)
        Me.TxtLeaveC8.Name = "TxtLeaveC8"
        Me.TxtLeaveC8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC8.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC8.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC8.Properties.MaxLength = 6
        Me.TxtLeaveC8.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC8.TabIndex = 33
        Me.TxtLeaveC8.Visible = False
        '
        'TxtLeaveC20
        '
        Me.TxtLeaveC20.EditValue = "000.00"
        Me.TxtLeaveC20.Location = New System.Drawing.Point(210, 162)
        Me.TxtLeaveC20.Name = "TxtLeaveC20"
        Me.TxtLeaveC20.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC20.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC20.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC20.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC20.Properties.MaxLength = 6
        Me.TxtLeaveC20.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC20.TabIndex = 37
        Me.TxtLeaveC20.Visible = False
        '
        'TxtLeaveC19
        '
        Me.TxtLeaveC19.EditValue = "000.00"
        Me.TxtLeaveC19.Location = New System.Drawing.Point(210, 136)
        Me.TxtLeaveC19.Name = "TxtLeaveC19"
        Me.TxtLeaveC19.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC19.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC19.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC19.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC19.Properties.MaxLength = 6
        Me.TxtLeaveC19.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC19.TabIndex = 36
        Me.TxtLeaveC19.Visible = False
        '
        'TxtLeaveC18
        '
        Me.TxtLeaveC18.EditValue = "000.00"
        Me.TxtLeaveC18.Location = New System.Drawing.Point(210, 110)
        Me.TxtLeaveC18.Name = "TxtLeaveC18"
        Me.TxtLeaveC18.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC18.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC18.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC18.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC18.Properties.MaxLength = 6
        Me.TxtLeaveC18.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC18.TabIndex = 35
        Me.TxtLeaveC18.Visible = False
        '
        'TxtLeaveC17
        '
        Me.TxtLeaveC17.EditValue = "000.00"
        Me.TxtLeaveC17.Location = New System.Drawing.Point(210, 84)
        Me.TxtLeaveC17.Name = "TxtLeaveC17"
        Me.TxtLeaveC17.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC17.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC17.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC17.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC17.Properties.MaxLength = 6
        Me.TxtLeaveC17.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC17.TabIndex = 34
        Me.TxtLeaveC17.Visible = False
        '
        'TxtLeaveC16
        '
        Me.TxtLeaveC16.EditValue = "000.00"
        Me.TxtLeaveC16.Location = New System.Drawing.Point(210, 58)
        Me.TxtLeaveC16.Name = "TxtLeaveC16"
        Me.TxtLeaveC16.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC16.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC16.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC16.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC16.Properties.MaxLength = 6
        Me.TxtLeaveC16.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC16.TabIndex = 33
        Me.TxtLeaveC16.Visible = False
        '
        'TxtLeaveC15
        '
        Me.TxtLeaveC15.EditValue = "000.00"
        Me.TxtLeaveC15.Location = New System.Drawing.Point(210, 32)
        Me.TxtLeaveC15.Name = "TxtLeaveC15"
        Me.TxtLeaveC15.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtLeaveC15.Properties.Appearance.Options.UseFont = True
        Me.TxtLeaveC15.Properties.Mask.EditMask = "f"
        Me.TxtLeaveC15.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TxtLeaveC15.Properties.MaxLength = 6
        Me.TxtLeaveC15.Size = New System.Drawing.Size(70, 20)
        Me.TxtLeaveC15.TabIndex = 32
        Me.TxtLeaveC15.Visible = False
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl18.Appearance.Options.UseFont = True
        Me.LabelControl18.Location = New System.Drawing.Point(213, 12)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(38, 14)
        Me.LabelControl18.TabIndex = 43
        Me.LabelControl18.Text = "Encash"
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Location = New System.Drawing.Point(133, 12)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(41, 14)
        Me.LabelControl19.TabIndex = 42
        Me.LabelControl19.Text = "Balance"
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl20.Appearance.Options.UseFont = True
        Me.LabelControl20.Location = New System.Drawing.Point(224, 12)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(38, 14)
        Me.LabelControl20.TabIndex = 43
        Me.LabelControl20.Text = "Encash"
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(144, 12)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(41, 14)
        Me.LabelControl21.TabIndex = 42
        Me.LabelControl21.Text = "Balance"
        '
        'XtraLeaveEncashment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "XtraLeaveEncashment"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.txtPayableMonth.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPayableMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.TxtLeave20.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeave19.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeave18.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeave17.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeave16.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeave15.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.TxtLeave14.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeave13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeave12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeave11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeave10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeave9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeave8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.TxtLeave7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeave6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeave5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeave4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeave3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeave2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeave1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOnlyYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmployeeGroupBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC14.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC20.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC19.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC18.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC17.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC16.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtLeaveC15.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEdit1 As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents PopupContainerControl1 As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents EmployeeGroupBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As ULtra.SSSDBDataSet
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colGroupId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGroupName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents EmployeeGroupTableAdapter As ULtra.SSSDBDataSetTableAdapters.EmployeeGroupTableAdapter
    Friend WithEvents EmployeeGroup1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.EmployeeGroup1TableAdapter
    Friend WithEvents txtOnlyYear As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents lbLeave7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLeave6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLeave5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLeave4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLeave3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLeave2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLeave1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtLeave1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeave7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeave6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeave5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeave4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeave3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeave2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents TxtLeave20 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeave19 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeave18 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeave17 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeave16 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeave15 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbLeave20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLeave19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLeave18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLeave17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLeave16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLeave15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents TxtLeave14 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeave13 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeave12 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeave11 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeave10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeave9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeave8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbLeave14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLeave13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLeave12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLeave11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLeave10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLeave9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLeave8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TblEmployee1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents TblEmployeeTableAdapter As ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents ComboNepaliYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtPayableMonth As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtLeaveC20 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeaveC19 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeaveC18 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeaveC17 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeaveC16 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeaveC15 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtLeaveC14 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeaveC13 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeaveC12 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeaveC11 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeaveC10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeaveC9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeaveC8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtLeaveC7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeaveC6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeaveC5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeaveC4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeaveC3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeaveC2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtLeaveC1 As DevExpress.XtraEditors.TextEdit

End Class
