/************************************************************************
 *
 *  Program : FK623Attend.h
 *
 *  Purpose : Communication Interface Functions For Fingerkeeper family.
 *
 *  Version : 2.4
 *
 ************************************************************************/

#ifndef FK623ATTEND_H_INCLUDED
#define FK623ATTEND_H_INCLUDED

#ifdef FK623ATTEND_DLL
	#define FP_EXPORT __declspec(dllexport) APIENTRY
#else
	#define FP_EXPORT __declspec(dllimport) APIENTRY
#endif

//{-------------------------------------------------------------------------------------------------
// Connection
long FP_EXPORT FK_ConnectComm(long nMachineNo, long anComPort, long anBaudRate, const char* astrTelNumber, long anWaitDialTime, long anLicense, long anComTimeOut);
long FP_EXPORT FK_ConnectNet(long anMachineNo, const char* astrIpAddress, long anNetPort, long anTimeOut, long anProtocolType, long anNetPassword, long anLicense);
long FP_EXPORT FK_ConnectUSB(long anMachineNo, long anLicense);
void FP_EXPORT FK_DisConnect(long anHandleIndex);
//}

//{-------------------------------------------------------------------------------------------------
// Error processing
long FP_EXPORT FK_GetLastError(long anHandleIndex);
//}

//{-------------------------------------------------------------------------------------------------
// Device Setting
long FP_EXPORT FK_EnableDevice(long anHandleIndex, unsigned char nEnableFlag);
void FP_EXPORT FK_PowerOnAllDevice(long anHandleIndex);
long FP_EXPORT FK_PowerOffDevice(long anHandleIndex);
long FP_EXPORT FK_GetDeviceStatus(long anHandleIndex, long anStatusIndex, long* apnValue);
long FP_EXPORT FK_GetDeviceTime(long anHandleIndex, DATE* apnDateTime);
long FP_EXPORT FK_GetDeviceTime_1(long anHandleIndex, long* apnYear, long* apnMonth, long* apnDay, long* apnHour, long* apnMinute, long* apnSec, long* apnDayOfWeek);
long FP_EXPORT FK_SetDeviceTime(long anHandleIndex, DATE anDateTime);
long FP_EXPORT FK_SetDeviceTime_1(long anHandleIndex, long anYear, long anMonth, long anDay, long anHour, long anMinute, long anSec, long anDayOfWeek);
long FP_EXPORT FK_GetDeviceInfo(long anHandleIndex, long anInfoIndex, long* apnValue);
long FP_EXPORT FK_SetDeviceInfo(long anHandleIndex, long anInfoIndex, long anValue);
long FP_EXPORT FK_GetProductData(long anHandleIndex, long anDataIndex, char** apstrValue);
long FP_EXPORT FK_GetDeviceVersion(long anHandleIndex, long* apnVersion);
//}

//{-------------------------------------------------------------------------------------------------
// Log Data
long FP_EXPORT FK_LoadSuperLogData(long anHandleIndex, long anReadMark);
long FP_EXPORT FK_GetSuperLogData(long anHandleIndex, long* apnSEnrollNumber, long* apnGEnrollNumber, long* apnManipulation, long* apnBackupNumber, DATE* apnDateTime);
long FP_EXPORT FK_GetSuperLogData_1(long anHandleIndex, long* apnSEnrollNumber, long* apnGEnrollNumber, long* apnManipulation, long* apnBackupNumber, long* apnYear, long* apnMonth, long* apnDay, long* apnHour, long* apnMinute, long* apnSec);
long FP_EXPORT FK_EmptySuperLogData(long anHandleIndex);

long FP_EXPORT FK_LoadGeneralLogData(long anHandleIndex, long anReadMark);
long FP_EXPORT FK_GetGeneralLogData(long anHandleIndex, long* apnEnrollNumber, long* apnVerifyMode, long* apnInOutMode, DATE* apnDateTime);
long FP_EXPORT FK_GetGeneralLogData_1(long anHandleIndex, long* apnEnrollNumber, long* apnVerifyMode, long* apnInOutMode, long* apnYear, long* apnMonth, long* apnDay, long* apnHour, long* apnMinute, long* apnSec);
long FP_EXPORT FK_EmptyGeneralLogData(long anHandleIndex);

long FP_EXPORT FK_USBLoadSuperLogDataFromFile(long anHandleIndex, const char* astrFilePath);
long FP_EXPORT FK_USBLoadGeneralLogDataFromFile(long anHandleIndex, const char* astrFilePath);
//}

//{-------------------------------------------------------------------------------------------------
// Bell Time
long FP_EXPORT FK_GetBellTime(long anHandleIndex, long* apnBellCount, void* aptBellInfo);
long FP_EXPORT FK_GetBellTimeWithString(long anHandleIndex, long* apnBellCount, char** apstrBellInfo);
long FP_EXPORT FK_SetBellTime(long anHandleIndex, long anBellCount, void* aptBellInfo);
long FP_EXPORT FK_SetBellTimeWithString(long anHandleIndex, long anBellCount, const char* astrBellInfo);
//}

//{-------------------------------------------------------------------------------------------------
// Enroll Data, User Name, Message
long FP_EXPORT FK_EnableUser(long anHandleIndex, long anEnrollNumber, long anBackupNumber, long anEnableFlag);
long FP_EXPORT FK_ModifyPrivilege(long anHandleIndex, long anEnrollNumber, long anBackupNumber, long anMachinePrivilege);
long FP_EXPORT FK_BenumbAllManager(long anHandleIndex);

long FP_EXPORT FK_ReadAllUserID(long anHandleIndex);
long FP_EXPORT FK_GetAllUserID(long anHandleIndex, long* apnEnrollNumber, long* apnBackupNumber, long* apnMachinePrivilege, long* apnEnableFlag);
long FP_EXPORT FK_GetEnrollData(long anHandleIndex, long anEnrollNumber, long anBackupNumber, long* apnMachinePrivilege, void* apEnrollData, long* apnPassWord);
long FP_EXPORT FK_GetEnrollDataWithString(long anHandleIndex, long anEnrollNumber, long anBackupNumber, long* apnMachinePrivilege, char** apstrEnrollData);
long FP_EXPORT FK_PutEnrollData(long anHandleIndex, long anEnrollNumber, long anBackupNumber, long anMachinePrivilege, void* apEnrollData, long anPassWord);
long FP_EXPORT FK_PutEnrollDataWithString(long anHandleIndex, long anEnrollNumber, long anBackupNumber, long anMachinePrivilege, const char* astrEnrollData);
long FP_EXPORT FK_SaveEnrollData(long anHandleIndex);

long FP_EXPORT FK_DeleteEnrollData(long anHandleIndex, long anEnrollNumber, long anBackupNumber);
long FP_EXPORT FK_EmptyEnrollData(long anHandleIndex);
long FP_EXPORT FK_ClearKeeperData(long anHandleIndex);

long FP_EXPORT FK_GetUserName(long anHandleIndex, long anEnrollNumber, char** apstrUserName);
long FP_EXPORT FK_SetUserName(long anHandleIndex, long anEnrollNumber, const char* astrUserName);
long FP_EXPORT FK_GetNewsMessage(long anHandleIndex, long anNewsId, char** apstrNews);
long FP_EXPORT FK_SetNewsMessage(long anHandleIndex, long anNewsId, const char* astrNews);
long FP_EXPORT FK_GetUserNewsID(long anHandleIndex, long anEnrollNumber, long* apnNewsId); 
long FP_EXPORT FK_SetUserNewsID(long anHandleIndex, long anEnrollNumber, long anNewsId);
//}

//{-------------------------------------------------------------------------------------------------
// USB File
long FP_EXPORT FK_SetUSBModel(long anHandleIndex, long anModel); // old
long FP_EXPORT FK_SetUDiskFileFKModel(long anHandleIndex, const char* astrFKModel); // new
long FP_EXPORT FK_USBReadAllEnrollDataFromFile(long anHandleIndex, const char* astrFilePath);
long FP_EXPORT FK_USBReadAllEnrollDataCount(long anHandleIndex, long* apnValue);
long FP_EXPORT FK_USBGetOneEnrollData(long anHandleIndex, long* apnEnrollNumber, long* apnBackupNumber, long* apnMachinePrivilege, void* apEnrollData, long* apnPassWord, long* apnEnableFlag, char** apstrEnrollName);
long FP_EXPORT FK_USBGetOneEnrollDataWithString(long anHandleIndex, long*  apnEnrollNumber, long* apnBackupNumber, long* apnMachinePrivilege, char** apstrEnrollData, long* apnEnableFlag, char** apstrEnrollName);
long FP_EXPORT FK_USBGetOneEnrollData_1(long* apnEnrollNumber, long* apnBackupNumber, long* apnVerifyMode, long* apnMachinePrivilege, void* apEnrollData, long* apnPassWord, long* apnEnableFlag, char** apstrEnrollName);
long FP_EXPORT FK_USBGetOneEnrollDataWithString_1(long* apnEnrollNumber, long* apnBackupNumber, long* apnVerifyMode, long* apnMachinePrivilege, char** apstrEnrollData, long* apnEnableFlag, char** apstrEnrollName);

long FP_EXPORT FK_USBSetOneEnrollData(long anHandleIndex, long anEnrollNumber, long anBackupNumber, long anMachinePrivilege, void* apEnrollData, long anPassWord, long anEnableFlag, const char* astrEnrollName);
long FP_EXPORT FK_USBSetOneEnrollDataWithString(long anHandleIndex, long anEnrollNumber, long anBackupNumber, long anMachinePrivilege, const char* astrEnrollData, long anEnableFlag, const char* astrEnrollName);
long FP_EXPORT FK_USBSetOneEnrollData_1(long anEnrollNumber, long anBackupNumber, long anVerifyMode, long anMachinePrivilege, long* apnEnrollData, long anPassWord, long anEnableFlag, const char* astrEnrollName);
long FP_EXPORT FK_USBSetOneEnrollDataWithString_1(long anEnrollNumber, long anBackupNumber, long anVerifyMode, long anMachinePrivilege, const char* astrEnrollData, long anEnableFlag, const char* astrEnrollName);
long FP_EXPORT FK_USBWriteAllEnrollDataToFile(long anHandleIndex, const char* astrFilePath);

long FP_EXPORT FK_USBReadAllEnrollDataFromFile_Color(long anHandleIndex, const char* astrFilePath);
long FP_EXPORT FK_USBGetOneEnrollData_Color(long anHandleIndex, long* apnEnrollNumber, long* apnBackupNumber, long* apnMachinePrivilege, void* apEnrollData, long* apnPassWord, long* apnEnableFlag, char** apstrEnrollName, long anNewsKind);
long FP_EXPORT FK_USBGetOneEnrollDataWithString_Color(long anHandleIndex, long* apnEnrollNumber, long* apnBackupNumber, long* apnMachinePrivilege, char** apstrEnrollData, long* apnEnableFlag, char** apstrEnrollName, long anNewsKind);

long FP_EXPORT FK_USBSetOneEnrollData_Color(long anHandleIndex, long anEnrollNumber, long anBackupNumber, long anMachinePrivilege, void* apEnrollData, long anPassWord, long anEnableFlag, const char* astrEnrollName, long anNewsKind);
long FP_EXPORT FK_USBSetOneEnrollDataWithString_Color(long anHandleIndex, long anEnrollNumber, long anBackupNumber, long anMachinePrivilege, const char* astrEnrollData, long anEnableFlag, const char* astrEnrollName, long anNewsKind);
long FP_EXPORT FK_USBWriteAllEnrollDataToFile_Color(long anHandleIndex, const char* astrFilePath, long anNewsKind);
//}

//{----------------------------------------------------------------------------------
// Post/Shift (EXCEL version)
long FP_EXPORT FK_GetPostShiftInfo(long anHandleIndex, void* apPostShiftInfo,  long* apnPostShiftInfoLen);
long FP_EXPORT FK_SetPostShiftInfo(long anHandleIndex, void* apPostShiftInfo,  long anPostShiftInfoLen);
long FP_EXPORT FK_GetUserInfoEx(long anHandleIndex, long anEnrollNumber, void* apUserInfo,  long* apnUserInfoLen);
long FP_EXPORT FK_SetUserInfoEx(long anHandleIndex, long anEnrollNumber, void* apUserInfo,  long anUserInfoLen);
//}

//{----------------------------------------------------------------------------------
// Photo image transfer
long FP_EXPORT FK_GetEnrollPhoto(long anHandleIndex, long anEnrollNumber, void* apPhotoImage, long* apnPhotoLength);
long FP_EXPORT FK_SetEnrollPhoto(long anHandleIndex, long anEnrollNumber, void* apPhotoImage, long anPhotoLength);
long FP_EXPORT FK_DeleteEnrollPhoto(long anHandleIndex, long anEnrollNumber);

long FP_EXPORT FK_GetLogPhoto(long anHandleIndex, long anEnrollNumber, long anYear, long anMonth, long anDay, long anHour, long anMinute, long anSec, void* apPhotoImage, long* apnPhotoLength);
//}

// Check whether specific enroll data type is supported on FK machine
long FP_EXPORT FK_IsSupportedEnrollData(long anHandleIndex, long anBackupNumber, long* apSupportFlag);

// All purpose function
long FP_EXPORT FK_ExecCommand(long anHandleIndex, const char* astrCommand, char** apstrResult);
//}


long FP_EXPORT FK_GetIsSupportStringID(long anHandleIndex);
long FP_EXPORT FK_GetUSBEnrollDataIsSupportStringID(long anHandleIndex);
long FP_EXPORT FK_GetLogDataIsSupportStringID(long anHandleIndex);

long FP_EXPORT FK_GetSuperLogData_StringID(long anHandleIndex, char** apnSEnrollNumber, char** apnGEnrollNumber, long* apnManipulation, long* apnBackupNumber, DATE* apnDateTime);

long FP_EXPORT FK_GetGeneralLogData_StringID(long anHandleIndex, char** apnEnrollNumber, long* apnVerifyMode, long* apnInOutMode, DATE* apnDateTime);

long FP_EXPORT FK_EnableUser_StringID(long anHandleIndex, char* apEnrollNumber, long anBackupNumber, long anEnableFlag);

long FP_EXPORT FK_ModifyPrivilege_StringID(long anHandleIndex, char* apEnrollNumber, long anBackupNumber, long anMachinePrivilege);

long FP_EXPORT FK_GetAllUserID_StringID(long anHandleIndex, char** apEnrollNumber, long* apnBackupNumber, long* apnMachinePrivilege, long* apnEnableFlag);

long FP_EXPORT FK_GetEnrollData_StringID(long anHandleIndex, char* apEnrollNumber, long anBackupNumber, long* apnMachinePrivilege, void* apEnrollData, long* apnPassWord);

long FP_EXPORT FK_PutEnrollData_StringID(long anHandleIndex, char* apEnrollNumber, long anBackupNumber, long anMachinePrivilege, void* apEnrollData, long anPassWord);

long FP_EXPORT FK_DeleteEnrollData_StringID(long anHandleIndex, char* apEnrollNumber, long anBackupNumber);

long FP_EXPORT FK_GetUserName_StringID(long anHandleIndex, char* apEnrollNumber, char** apstrUserName);

long FP_EXPORT FK_SetUserName_StringID(long anHandleIndex, char* apEnrollNumber, const char* astrUserName);

long FP_EXPORT FK_GetUserPassTime_StringID(long anHandleIndex, char* apEnrollNumber, long* apnGroupID, void* apUserPassTimeInfo, long anUserPassTimeInfoSize);

long FP_EXPORT FK_SetUserPassTime_StringID(long anHandleIndex, char* anEnrollNumber, long anGroupID, void* apUserPassTimeInfo, long anUserPassTimeInfoSize);

long FP_EXPORT FK_USBGetOneEnrollData_StringID(long anHandleIndex, char** apEnrollNumber, long* apnBackupNumber, long* apnMachinePrivilege, void* apEnrollData, long* apnPassWord, long* apnEnableFlag, char** apstrEnrollName);

long FP_EXPORT FK_USBSetOneEnrollData_StringID(long anHandleIndex, char* apEnrollNumber, long anBackupNumber, long anMachinePrivilege, void* apEnrollData, long anPassWord, long anEnableFlag, const char* astrEnrollName);

long FP_EXPORT FK_GetUserInfoEx_StringID(long anHandleIndex, char* apEnrollNumber, void* apUserInfo,  long* apnUserInfoLen);

long FP_EXPORT FK_SetUserInfoEx_StringID(long anHandleIndex, char* apEnrollNumber, void* apUserInfo,  long anUserInfoLen);

long FP_EXPORT FK_GetEnrollPhoto_StringID(long anHandleIndex, char* apEnrollNumber, void* apPhotoImage, long* apnPhotoLength);

long FP_EXPORT FK_SetEnrollPhoto_StringID(long anHandleIndex, char* apEnrollNumber, void* apPhotoImage, long anPhotoLength);

long FP_EXPORT FK_DeleteEnrollPhoto_StringID(long anHandleIndex, char* apEnrollNumber);


long FP_EXPORT FK_GetLogPhoto_StringID(long anHandleIndex, char* apEnrollNumber, long anYear, long anMonth, long anDay, long anHour, long anMinute, long anSec, void* apPhotoImage, long* apnPhotoLength);

#endif //!FK623ATTEND_H_INCLUDED
/************************************************************************
 *
 *                        End of file : FK623Attend.h
 *
 ************************************************************************/