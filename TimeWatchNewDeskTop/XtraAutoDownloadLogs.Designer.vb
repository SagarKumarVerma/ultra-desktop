﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraAutoDownloadLogs
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraAutoDownloadLogs))
        Me.TblMachineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New ULtra.SSSDBDataSet()
        Me.TblMachineTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblMachineTableAdapter()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TblMachine1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblMachine1TableAdapter()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLOCATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbranch = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDeviceType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colA_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcommkey = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIN_OUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHLogin = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHPassword = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TblMachineBindingSource
        '
        Me.TblMachineBindingSource.DataMember = "tblMachine"
        Me.TblMachineBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblMachineTableAdapter
        '
        Me.TblMachineTableAdapter.ClearBeforeFill = True
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.LabelControl1)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel1.Location = New System.Drawing.Point(0, 281)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(505, 23)
        Me.SidePanel1.TabIndex = 0
        Me.SidePanel1.Text = "SidePanel1"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Appearance.Options.UseForeColor = True
        Me.LabelControl1.Location = New System.Drawing.Point(4, 8)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(48, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "            "
        '
        'TblMachine1TableAdapter1
        '
        Me.TblMachine1TableAdapter1.ClearBeforeFill = True
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblMachineBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Remove.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(505, 281)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID_NO, Me.colLOCATION, Me.colbranch, Me.colDeviceType, Me.colA_R, Me.GridColumn1, Me.colcommkey, Me.colIN_OUT, Me.colHLogin, Me.colHPassword})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colID_NO, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colID_NO
        '
        Me.colID_NO.FieldName = "ID_NO"
        Me.colID_NO.Name = "colID_NO"
        Me.colID_NO.Visible = True
        Me.colID_NO.VisibleIndex = 1
        '
        'colLOCATION
        '
        Me.colLOCATION.FieldName = "LOCATION"
        Me.colLOCATION.Name = "colLOCATION"
        Me.colLOCATION.Visible = True
        Me.colLOCATION.VisibleIndex = 3
        Me.colLOCATION.Width = 120
        '
        'colbranch
        '
        Me.colbranch.Caption = "GridColumn1"
        Me.colbranch.FieldName = "branch"
        Me.colbranch.Name = "colbranch"
        Me.colbranch.Visible = True
        Me.colbranch.VisibleIndex = 2
        '
        'colDeviceType
        '
        Me.colDeviceType.Caption = "GridColumn1"
        Me.colDeviceType.FieldName = "DeviceType"
        Me.colDeviceType.Name = "colDeviceType"
        '
        'colA_R
        '
        Me.colA_R.FieldName = "A_R"
        Me.colA_R.Name = "colA_R"
        '
        'GridColumn1
        '
        Me.GridColumn1.FieldName = "Purpose"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'colcommkey
        '
        Me.colcommkey.Caption = "GridColumn2"
        Me.colcommkey.FieldName = "commkey"
        Me.colcommkey.Name = "colcommkey"
        '
        'colIN_OUT
        '
        Me.colIN_OUT.Caption = "GridColumn2"
        Me.colIN_OUT.FieldName = "IN_OUT"
        Me.colIN_OUT.Name = "colIN_OUT"
        '
        'colHLogin
        '
        Me.colHLogin.Caption = "HLogin"
        Me.colHLogin.FieldName = "HLogin"
        Me.colHLogin.Name = "colHLogin"
        Me.colHLogin.Visible = True
        Me.colHLogin.VisibleIndex = 4
        '
        'colHPassword
        '
        Me.colHPassword.Caption = "HPassword"
        Me.colHPassword.FieldName = "HPassword"
        Me.colHPassword.Name = "colHPassword"
        Me.colHPassword.Visible = True
        Me.colHPassword.VisibleIndex = 5
        '
        'XtraAutoDownloadLogs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(505, 304)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.SidePanel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraAutoDownloadLogs"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Auto Download Logs"
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        Me.SidePanel1.PerformLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SSSDBDataSet As ULtra.SSSDBDataSet
    Friend WithEvents TblMachineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblMachineTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblMachineTableAdapter
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents TblMachine1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblMachine1TableAdapter
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLOCATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbranch As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDeviceType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colA_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcommkey As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIN_OUT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHLogin As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHPassword As DevExpress.XtraGrid.Columns.GridColumn
End Class
