﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraLoanAdvanceGrid
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.TBLADVANCEDATABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New ULtra.SSSDBDataSet()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPAYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colA_L = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIDNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMON_YEAR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colINST_AMT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCASH_AMT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBALANCE_AMT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPAMT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIAMT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colINSTNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TBLADVANCEDATATableAdapter = New ULtra.SSSDBDataSetTableAdapters.TBLADVANCEDATATableAdapter()
        Me.TbladvancedatA1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.TBLADVANCEDATA1TableAdapter()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEditCash = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditSalary = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TBLADVANCEDATABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditCash.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditSalary.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TBLADVANCEDATABindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Remove.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(2, 2)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(980, 359)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'TBLADVANCEDATABindingSource
        '
        Me.TBLADVANCEDATABindingSource.DataMember = "TBLADVANCEDATA"
        Me.TBLADVANCEDATABindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPAYCODE, Me.colA_L, Me.colIDNO, Me.colMON_YEAR, Me.colINST_AMT, Me.colCASH_AMT, Me.colBALANCE_AMT, Me.colPAMT, Me.colIAMT, Me.colINSTNO})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridView1.OptionsBehavior.Editable = False
        '
        'colPAYCODE
        '
        Me.colPAYCODE.FieldName = "PAYCODE"
        Me.colPAYCODE.Name = "colPAYCODE"
        '
        'colA_L
        '
        Me.colA_L.Caption = "Type"
        Me.colA_L.FieldName = "A_L"
        Me.colA_L.Name = "colA_L"
        Me.colA_L.Visible = True
        Me.colA_L.VisibleIndex = 0
        '
        'colIDNO
        '
        Me.colIDNO.FieldName = "IDNO"
        Me.colIDNO.Name = "colIDNO"
        '
        'colMON_YEAR
        '
        Me.colMON_YEAR.Caption = "Date"
        Me.colMON_YEAR.FieldName = "MON_YEAR"
        Me.colMON_YEAR.Name = "colMON_YEAR"
        Me.colMON_YEAR.Visible = True
        Me.colMON_YEAR.VisibleIndex = 1
        '
        'colINST_AMT
        '
        Me.colINST_AMT.Caption = "Installment Amt"
        Me.colINST_AMT.FieldName = "INST_AMT"
        Me.colINST_AMT.Name = "colINST_AMT"
        Me.colINST_AMT.Visible = True
        Me.colINST_AMT.VisibleIndex = 2
        '
        'colCASH_AMT
        '
        Me.colCASH_AMT.Caption = "Cash Amt"
        Me.colCASH_AMT.FieldName = "CASH_AMT"
        Me.colCASH_AMT.Name = "colCASH_AMT"
        Me.colCASH_AMT.Visible = True
        Me.colCASH_AMT.VisibleIndex = 3
        '
        'colBALANCE_AMT
        '
        Me.colBALANCE_AMT.Caption = "Balance Amt"
        Me.colBALANCE_AMT.FieldName = "BALANCE_AMT"
        Me.colBALANCE_AMT.Name = "colBALANCE_AMT"
        Me.colBALANCE_AMT.Visible = True
        Me.colBALANCE_AMT.VisibleIndex = 4
        '
        'colPAMT
        '
        Me.colPAMT.Caption = "Principle Amt"
        Me.colPAMT.FieldName = "PAMT"
        Me.colPAMT.Name = "colPAMT"
        Me.colPAMT.Visible = True
        Me.colPAMT.VisibleIndex = 5
        '
        'colIAMT
        '
        Me.colIAMT.Caption = "Intrest Amount"
        Me.colIAMT.FieldName = "IAMT"
        Me.colIAMT.Name = "colIAMT"
        Me.colIAMT.Visible = True
        Me.colIAMT.VisibleIndex = 6
        '
        'colINSTNO
        '
        Me.colINSTNO.Caption = "No Of Installments"
        Me.colINSTNO.FieldName = "INSTNO"
        Me.colINSTNO.Name = "colINSTNO"
        Me.colINSTNO.Visible = True
        Me.colINSTNO.VisibleIndex = 7
        '
        'TBLADVANCEDATATableAdapter
        '
        Me.TBLADVANCEDATATableAdapter.ClearBeforeFill = True
        '
        'TbladvancedatA1TableAdapter1
        '
        Me.TbladvancedatA1TableAdapter1.ClearBeforeFill = True
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.GridControl1)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(984, 363)
        Me.PanelControl1.TabIndex = 2
        '
        'PanelControl2
        '
        Me.PanelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl2.Controls.Add(Me.GroupControl1)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(0, 363)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(984, 98)
        Me.PanelControl2.TabIndex = 3
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl1.Controls.Add(Me.SimpleButtonSave)
        Me.GroupControl1.Controls.Add(Me.TextEdit1)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.CheckEditCash)
        Me.GroupControl1.Controls.Add(Me.CheckEditSalary)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(984, 98)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Adjustment"
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(608, 49)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 22
        Me.SimpleButtonSave.Text = "Save"
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(476, 50)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.Mask.EditMask = "######.##"
        Me.TextEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit1.Properties.MaxLength = 9
        Me.TextEdit1.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit1.TabIndex = 5
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(347, 53)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(112, 14)
        Me.LabelControl1.TabIndex = 4
        Me.LabelControl1.Text = "Adjustment Amount"
        '
        'CheckEditCash
        '
        Me.CheckEditCash.Location = New System.Drawing.Point(149, 51)
        Me.CheckEditCash.Name = "CheckEditCash"
        Me.CheckEditCash.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditCash.Properties.Appearance.Options.UseFont = True
        Me.CheckEditCash.Properties.Caption = "Cash"
        Me.CheckEditCash.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditCash.Properties.RadioGroupIndex = 0
        Me.CheckEditCash.Size = New System.Drawing.Size(77, 19)
        Me.CheckEditCash.TabIndex = 3
        Me.CheckEditCash.TabStop = False
        '
        'CheckEditSalary
        '
        Me.CheckEditSalary.EditValue = True
        Me.CheckEditSalary.Location = New System.Drawing.Point(27, 51)
        Me.CheckEditSalary.Name = "CheckEditSalary"
        Me.CheckEditSalary.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditSalary.Properties.Appearance.Options.UseFont = True
        Me.CheckEditSalary.Properties.Caption = "Salary"
        Me.CheckEditSalary.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditSalary.Properties.RadioGroupIndex = 0
        Me.CheckEditSalary.Size = New System.Drawing.Size(77, 19)
        Me.CheckEditSalary.TabIndex = 2
        '
        'XtraLoanAdvanceGrid
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 461)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraLoanAdvanceGrid"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TBLADVANCEDATABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditCash.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditSalary.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SSSDBDataSet As ULtra.SSSDBDataSet
    Friend WithEvents TBLADVANCEDATABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TBLADVANCEDATATableAdapter As ULtra.SSSDBDataSetTableAdapters.TBLADVANCEDATATableAdapter
    Friend WithEvents colPAYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colA_L As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIDNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMON_YEAR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colINST_AMT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCASH_AMT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBALANCE_AMT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPAMT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIAMT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colINSTNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TbladvancedatA1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.TBLADVANCEDATA1TableAdapter
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEditCash As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditSalary As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
End Class
