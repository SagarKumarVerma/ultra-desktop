﻿Imports System.Resources
Imports System.Globalization
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO
Imports System.Text
Imports DevExpress.XtraSplashScreen
Imports DevExpress.XtraEditors
Imports DevExpress.LookAndFeel
Imports ULtra.AscDemo
Imports System.Runtime.InteropServices
Imports ULtra.AcsDemo
Imports System.Threading

Public Class XtraCommunicationForm
    Public Delegate Sub invokeDelegate()
    Dim ConnectionString As String
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim DC As New DateConverter()
    Dim datafile As String = "" '= System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"
    Dim serialNo As String = "" 'Hikvision

    Dim IN_OUT As String = ""
    Dim MC_NO As String = ""
    Dim ID_NO As String = ""
    Dim g_fGetAcsEventCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim m_lGetAcsEvent As Integer = 0

    Public Sub New()
        InitializeComponent()
        'MsgBox("")
        'If Common.servername = "Access" Then
        '    'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\HTDB.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine1
        'Else
        '    'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
        '    TblMachineTableAdapter1.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
        '    Me.TblMachineTableAdapter1.Fill(Me.SSSDBDataSet.tblMachine)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine
        'End If
        GridControl1.DataSource = Common.MachineNonAdmin
        Common.SetGridFont(GridView1, New Font("Tahoma", 9))
    End Sub
    Private Sub XtraCommunicationForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Me.Width = Common.NavWidth 'Me.Parent.Width
        'Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = SplitContainerControl1.Parent.Width  'Common.splitforMasterMenuWidth '
        'SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100 
        'MsgBox(SplitContainerControl1.Width)
        'MsgBox(Common.SplitterPosition)
        'MsgBox(SplitContainerControl1.SplitterPosition)

        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraDevice).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")
        LabelControl1.Text = Common.res_man.GetString("selectdevice", Common.cul)
        LabelControl2.Text = Common.res_man.GetString("download_from_all", Common.cul)
        LabelControl3.Text = Common.res_man.GetString("delete_from_device", Common.cul)
        ToggleSwitch1.Properties.OffText = Common.res_man.GetString("off", Common.cul)
        ToggleSwitch1.Properties.OnText = Common.res_man.GetString("onn", Common.cul)
        ToggleSwitch2.Properties.OffText = Common.res_man.GetString("off", Common.cul)
        ToggleSwitch2.Properties.OnText = Common.res_man.GetString("onn", Common.cul)
        'CheckEdit1.Text = Common.res_man.GetString("oldlog", Common.cul)
        'CheckEdit2.Text = Common.res_man.GetString("newlog", Common.cul)
        SimpleButtonRead.Text = Common.res_man.GetString("readdate", Common.cul)
        SimpleButtonSet.Text = Common.res_man.GetString("setdate", Common.cul)
        SimpleButtonDownload.Text = Common.res_man.GetString("downloadlogs", Common.cul)
        colID_NO.Caption = Common.res_man.GetString("controller_id", Common.cul)
        colLOCATION.Caption = Common.res_man.GetString("device_ip", Common.cul)
        colbranch.Caption = Common.res_man.GetString("location", Common.cul)


        ''this is if device modified in device grid then immidiate update in device list of download device select 
        'If Common.servername = "Access" Then
        '    'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\HTDB.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine1
        'Else
        '    'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
        '    TblMachineTableAdapter1.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
        '    Me.TblMachineTableAdapter1.Fill(Me.SSSDBDataSet.tblMachine)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine
        'End If
        GridControl1.DataSource = Common.MachineNonAdmin
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        PopupContainerEdit1.EditValue = ""
        TextEdit1.Text = ""
        CheckEdit2.Checked = True
        ToggleSwitch2.IsOn = False
        CheckEditTimePerioad.Checked = False
        'DateEditFrmDate.Enabled = False
        'DateEditToDate.Enabled = False
        'LabelControl4.Enabled = False
        'LabelControl5.Enabled = False
        DateEditFrmDate.DateTime = Now.AddDays(-1)
        DateEditToDate.DateTime = Now

        'If CheckEditTimePerioad.Checked = True Then
        '    ComboNYearStr.Enabled = True
        '    ComboNMonthStr.Enabled = True
        '    ComboNDateStr.Enabled = True
        '    ComboNYearEnd.Enabled = True
        '    ComboNMonthEnd.Enabled = True
        '    ComboNDateEnd.Enabled = True
        '    LabelControl4.Enabled = True
        '    LabelControl5.Enabled = True
        'Else
        '    ComboNYearStr.Enabled = False
        '    ComboNMonthStr.Enabled = False
        '    ComboNDateStr.Enabled = False
        '    ComboNYearEnd.Enabled = False
        '    ComboNMonthEnd.Enabled = False
        '    ComboNDateEnd.Enabled = False
        '    LabelControl4.Enabled = False
        '    LabelControl5.Enabled = False
        'End If

        If Common.IsNepali = "Y" Then
            DateEditFrmDate.Visible = False
            DateEditToDate.Visible = False
            ComboNYearStr.Visible = True
            ComboNMonthStr.Visible = True
            ComboNDateStr.Visible = True
            ComboNYearEnd.Visible = True
            ComboNMonthEnd.Visible = True
            ComboNDateEnd.Visible = True

            Dim doj As DateTime = DC.ToBS(New Date(DateEditFrmDate.DateTime.Year, DateEditFrmDate.DateTime.Month, DateEditFrmDate.DateTime.Day))
            ComboNYearStr.EditValue = doj.Year
            ComboNMonthStr.SelectedIndex = doj.Month - 1
            ComboNDateStr.EditValue = doj.Day

            doj = DC.ToBS(New Date(DateEditToDate.DateTime.Year, DateEditToDate.DateTime.Month, DateEditToDate.DateTime.Day))
            ComboNYearEnd.EditValue = doj.Year
            ComboNMonthEnd.SelectedIndex = doj.Month - 1
            ComboNDateEnd.EditValue = doj.Day
        Else
            DateEditFrmDate.Visible = True
            DateEditToDate.Visible = True
            ComboNYearStr.Visible = False
            ComboNMonthStr.Visible = False
            ComboNDateStr.Visible = False
            ComboNYearEnd.Visible = False
            ComboNMonthEnd.Visible = False
            ComboNDateEnd.Visible = False
        End If
        GridView1.ClearSelection()
    End Sub
    Private Sub PopupContainerEdit1_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEdit1.QueryPopUp
        Dim val As Object = PopupContainerEdit1.EditValue
        If (val Is Nothing) Then
            GridView1.ClearSelection()
        Else
            'Dim texts() As String = val.ToString.Split(Microsoft.VisualBasic.ChrW(44))
            Dim texts() As String = val.ToString.Split(",")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                End If
                'MsgBox(text & "  " & text.Length & " " & GridView1.LocateByValue("SHIFT", text))
                Dim rowHandle As Integer = GridView1.LocateByValue("ID_NO", text)
                GridView1.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub PopupContainerEdit1_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEdit1.QueryResultValue
        Dim selectedRows() As Integer = GridView1.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridView1.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("ID_NO"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub SimpleButton4_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton4.Click
        'Dim comclass As Common = New Common
        'comclass.Load_Corporate_PolicySql()
        'Me.Cursor = Cursors.WaitCursor

        'comclass.Process_AllnonRTC(DateEdit3.DateTime, Now.Date, "000000000000", "zzzzzzzzzzzz")
        'comclass.Process_AllRTC(DateEdit3.DateTime, Now.Date, "000000000000", "zzzzzzzzzzzz")
        'Me.Cursor = Cursors.Default
        Me.Close()
    End Sub
    Private Sub SimpleButtonRead_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonRead.Click
        If PopupContainerEdit1.EditValue.ToString.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select device</size>", "Failed")
            PopupContainerEdit1.Select()
            Exit Sub
        End If
        Dim a As DateTime
        Dim c As Common = New Common()
        Dim vnMachineNumber As Integer
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim vnReadMark As Integer

        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType, A_R As Object
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()
        Me.Cursor = Cursors.WaitCursor
        IP = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("LOCATION"))
        Dim commkey As Integer '= Convert.ToInt32(GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("commkey")))
        ID_NO = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("ID_NO"))
        DeviceType = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("DeviceType"))
        A_R = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("A_R"))
        If DeviceType = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
            
        ElseIf DeviceType = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
            
        ElseIf DeviceType = "HKSeries" Then
            Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
            Dim DeviceAdd As String = IP
            Dim userName As String = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("HLogin"))
            Dim pwd As String = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("HPassword"))
            Dim lUserID As Integer = -1
            'MsgBox(DeviceAdd & vbCrLf & userName & vbCrLf & pwd)
            Dim failReason As String = ""
            Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
            'MsgBox("lUserID " & lUserID.ToString)
            If logistatus = False Then
                'MsgBox(CHCNetSDK.NET_DVR_GetLastError.ToString & "  Login failed")
                If failReason <> "" Then
                    XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                    XtraMasterTest.LabelControlStatus.Text = ""
                    Application.DoEvents()
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If

            Else
                If failReason <> "" Then
                    XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                    XtraMasterTest.LabelControlStatus.Text = ""
                    Application.DoEvents()
                    Me.Cursor = Cursors.Default
                    logistatus = cn.HikvisionLogOut(lUserID)
                    Exit Sub
                End If

                Dim struTimeCfg As CHCNetSDK.NET_DVR_TIME = New CHCNetSDK.NET_DVR_TIME
                Dim dwReturned As UInteger = 0
                Dim dwSize As UInteger = CType(Marshal.SizeOf(struTimeCfg), UInteger)
                Dim ptrTimeCfg As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Int32))
                Marshal.StructureToPtr(struTimeCfg, ptrTimeCfg, False)
                If Not CHCNetSDK.NET_DVR_GetDVRConfig(lUserID, CHCNetSDK.NET_DVR_GET_TIMECFG, 1, ptrTimeCfg, dwSize, dwReturned) Then
                    MessageBox.Show((lUserID & vbCrLf & "Get device time error code :" + CHCNetSDK.NET_DVR_GetLastError.ToString))
                    Marshal.FreeHGlobal(ptrTimeCfg)
                    ptrTimeCfg = IntPtr.Zero
                    XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    XtraMasterTest.LabelControlStatus.Text = ""
                    Application.DoEvents()
                    Me.Cursor = Cursors.Default
                    Return
                Else
                    struTimeCfg = CType(Marshal.PtrToStructure(ptrTimeCfg, GetType(CHCNetSDK.NET_DVR_TIME)), CHCNetSDK.NET_DVR_TIME)
                    TextEdit1.Text = struTimeCfg.dwYear.ToString("0000") & "-" & struTimeCfg.dwMonth.ToString("00") & "-" & struTimeCfg.dwDay.ToString("00") & _
                         " " & struTimeCfg.dwHour.ToString("00") & ":" & struTimeCfg.dwMinute.ToString("00") & ":" & struTimeCfg.dwSecond.ToString("00")
                End If
                'logistatus = cn.HikvisionLogOut(lUserID)
            End If

        End If
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub SimpleButtonSet_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSet.Click
        If PopupContainerEdit1.EditValue.ToString.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select device</size>", "Failed")
            PopupContainerEdit1.Select()
            Exit Sub
        End If
        Dim a As DateTime
        Dim c As Common = New Common()
        Dim vnMachineNumber As Integer
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim vnReadMark As Integer
        Me.Cursor = Cursors.WaitCursor

        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType, A_R As Object
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()

        IP = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("LOCATION"))
        ID_NO = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("ID_NO"))
        DeviceType = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("DeviceType"))
        A_R = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("A_R"))
        Dim commkey As Integer '= Convert.ToInt32(GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("commkey")))
        If DeviceType = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then

        ElseIf DeviceType = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then

        ElseIf DeviceType = "HKSeries" Then
            Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
            Dim DeviceAdd As String = IP, userName As String = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("HLogin")), pwd As String = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("HPassword"))
            Dim lUserID As Integer = -1
            Dim failReason As String = ""
            Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
            If logistatus = False Then
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            Else
                Dim struTimeCfg As CHCNetSDK.NET_DVR_TIME = New CHCNetSDK.NET_DVR_TIME
                struTimeCfg.dwYear = Now.Year
                struTimeCfg.dwMonth = Now.Month
                struTimeCfg.dwDay = Now.Day
                struTimeCfg.dwHour = Now.Hour
                struTimeCfg.dwMinute = Now.Minute
                struTimeCfg.dwSecond = Now.Second
                Dim dwReturned As UInteger = 0
                Dim dwSize As UInteger = CType(Marshal.SizeOf(struTimeCfg), UInteger)
                Dim ptrTimeCfg As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Int32))
                Marshal.StructureToPtr(struTimeCfg, ptrTimeCfg, False)
                'CHCNetSDK.NET_DVR_SetDVRConfig(userID, CHCNetSDK.NET_DVR_SET_TIMECFG, 1, ptrTimeCfg, dwSize)
                If Not CHCNetSDK.NET_DVR_SetDVRConfig(lUserID, CHCNetSDK.NET_DVR_SET_TIMECFG, 1, ptrTimeCfg, dwSize) Then
                    MessageBox.Show((lUserID & vbCrLf & "Get device time error code :" + CHCNetSDK.NET_DVR_GetLastError.ToString))
                    Marshal.FreeHGlobal(ptrTimeCfg)
                    ptrTimeCfg = IntPtr.Zero
                    XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    Me.Cursor = Cursors.Default
                    Return
                Else
                    struTimeCfg = CType(Marshal.PtrToStructure(ptrTimeCfg, GetType(CHCNetSDK.NET_DVR_TIME)), CHCNetSDK.NET_DVR_TIME)
                    TextEdit1.Text = struTimeCfg.dwYear.ToString("0000") & "-" & struTimeCfg.dwMonth.ToString("00") & "-" & struTimeCfg.dwDay.ToString("00") & _
                         " " & struTimeCfg.dwHour.ToString("00") & ":" & struTimeCfg.dwMinute.ToString("00") & ":" & struTimeCfg.dwSecond.ToString("00")
                End If
                'logistatus = cn.HikvisionLogOut(lUserID)
            End If
        End If
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub SimpleButtonDownload_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonDownload.Click
        If PopupContainerEdit1.EditValue.ToString.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select device</size>", "Failed")
            PopupContainerEdit1.Select()
            Exit Sub
        End If
        Dim datafile As String = System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"
        Dim vnMachineNumber As Integer
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim vnReadMark As Integer

        If CheckEdit1.Checked = True Then   'All logs
            vnReadMark = 0
        ElseIf CheckEdit2.Checked Then   'new logs
            vnReadMark = 1
        End If
        Dim clearLog As Boolean
        If ToggleSwitch2.IsOn = True Then
            clearLog = True
        Else
            clearLog = False
        End If

        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, DeviceType, A_R, Purpose As Object
        Dim commkey As Integer
        'Dim IN_OUT As String
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()
        'DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
        For Each rowHandle As Integer In RowHandles
            IP = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("LOCATION"))
            IN_OUT = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("IN_OUT"))
            ID_NO = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("ID_NO"))
            XtraMasterTest.LabelControlStatus.Text = "Connecting " & IP & "..."
            Application.DoEvents()

            ID_NO = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("ID_NO"))
            DeviceType = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("DeviceType"))
            A_R = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("A_R"))
            Purpose = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("Purpose"))
            If DeviceType.ToString.Trim = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                'vnMachineNumber = ID_NO.ToString  '1
                'vpszIPAddress = IP.ToString '"192.168.0.111"
                'vpszNetPort = 5005
                'vpszNetPassword = 0
                'vnTimeOut = 5000
                'vnProtocolType = PROTOCOL_TCPIP
                'vnLicense = 1261

                'Dim result As String = cn.funcGetGeneralLogData(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense, vnReadMark, clearLog, A_R, Purpose, IN_OUT, ID_NO, datafile)
                ''MsgBox("Result " & result)
                'If result <> "Success" Then
                '    failIP.Add(IP.ToString)
                'End If
            ElseIf DeviceType.ToString.Trim = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                'Dim adap, adap1 As SqlDataAdapter
                'Dim adapA, adapA1 As OleDbDataAdapter
                'Dim ds, ds1 As DataSet
                'Dim sdwEnrollNumber As String = ""
                'Dim idwVerifyMode As Integer
                'Dim idwInOutMode As Integer
                'Dim idwYear As Integer
                'Dim idwMonth As Integer
                'Dim idwDay As Integer
                'Dim idwHour As Integer
                'Dim idwMinute As Integer
                'Dim idwSecond As Integer
                'Dim idwWorkcode As Integer
                'commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                'Dim comZK As CommonZK = New CommonZK
                'Dim bIsConnected = False
                'Dim iMachineNumber As Integer
                'Dim idwErrorCode As Integer
                'Dim com As Common = New Common
                'Dim axCZKEM1 As New zkemkeeper.CZKEM
                'axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                'bIsConnected = axCZKEM1.Connect_Net(IP, 4370)
                'If bIsConnected = True Then
                '    iMachineNumber = 1 'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                '    axCZKEM1.RegEvent(iMachineNumber, 65535) 'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                '    axCZKEM1.EnableDevice(iMachineNumber, False) 'disable the device
                '    Dim McSrno As String = ""
                '    Dim vRet As Boolean = axCZKEM1.GetSerialNumber(iMachineNumber, McSrno)
                '    'If McSrno.Substring(0, 3) = "AF6" Or McSrno.Substring(0, 3) = "AEH" Or McSrno.Substring(0, 3) = "AJV" Or McSrno.Substring(0, 3) = "AOO" Or McSrno.Substring(0, 3) = "719" Or McSrno.Substring(0, 3) = "ALM" Or McSrno.Substring(0, 3) = "701" Or McSrno.Substring(0, 3) = "BYR" Or McSrno.Substring(0, 3) = "OIN" Or McSrno.Substring(0, 3) = "AF6" Or McSrno.Substring(0, 3) = "BYX" Or McSrno.Substring(0, 3) = "AIJ" Or McSrno.Substring(0, 3) = "A6G" Or McSrno.Substring(0, 3) = "AEH" Or McSrno.Substring(0, 3) = "CCG" Or McSrno.Substring(0, 3) = "BJV" _
                '    '    Or Mid(Trim(McSrno), 1, 1) = "A" Or Mid(Trim(McSrno), 1, 3) = "OIN" Or Mid(Trim(McSrno), 1, 5) = "FPCTA" Or Mid(Trim(McSrno), 1, 4) = "0000" Or Mid(Trim(McSrno), 1, 4) = "1808" Or Mid(Trim(McSrno), 1, 4) = "ATPL" Or Mid(Trim(McSrno), 1, 4) = "TIPL" Or Mid(Trim(McSrno), 1, 4) = "ZXJK" Or Mid(Trim(McSrno), 1, 8) = "10122013" Or Mid(Trim(McSrno), 1, 8) = "76140122" Or Mid(Trim(McSrno), 1, 8) = "17614012" Or Mid(Trim(McSrno), 1, 8) = "17214012" Or Mid(Trim(McSrno), 1, 8) = "27022014" Or Mid(Trim(McSrno), 1, 8) = "24042014" Or Mid(Trim(McSrno), 1, 8) = "25042014" _
                '    '    Or Mid(Trim(McSrno), 1, 8) = "26042014" Or Mid(Trim(McSrno), 1, 7) = "SN:0000" Or Mid(Trim(McSrno), 1, 4) = "BOCK" Or McSrno.Substring(0, 1) = "B" Or McSrno.Substring(0, 4) = "2455" Or McSrno.Substring(0, 4) = "CDSL" _
                '    '    Or McSrno = "6583151100400" Then
                '    If Common.SerialNo.Contains(McSrno.Substring(0, 3)) Then
                '    Else
                '        SplashScreenManager.CloseForm(False)
                '        axCZKEM1.EnableDevice(iMachineNumber, True)
                '        axCZKEM1.Disconnect()
                '        XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & McSrno & "</size>", "<size=9>Error</size>")
                '        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
                '        Continue For
                '    End If
                '    Dim LogResult As Boolean
                '    If CheckEditTimePerioad.Checked = True Then
                '        If Common.IsNepali = "Y" Then
                '            Try
                '                DateEditFrmDate.DateTime = DC.ToAD(New Date(ComboNYearStr.EditValue, ComboNMonthStr.SelectedIndex + 1, ComboNDateStr.EditValue)) & " 00:00:00"
                '            Catch ex As Exception
                '                XtraMessageBox.Show(ulf, "<size=10>Invalid From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                '                DateEditFrmDate.Select()
                '                Exit Sub
                '            End Try
                '            Try
                '                DateEditToDate.DateTime = DC.ToAD(New Date(ComboNYearEnd.EditValue, ComboNMonthEnd.SelectedIndex + 1, ComboNDateEnd.EditValue)) & " 23:59:59"
                '            Catch ex As Exception
                '                XtraMessageBox.Show(ulf, "<size=10>Invalid To Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                '                DateEditToDate.Select()
                '                Exit Sub
                '            End Try
                '        End If
                '        LogResult = axCZKEM1.ReadTimeGLogData(iMachineNumber, DateEditFrmDate.DateTime.ToString("yyyy-MM-dd HH:mm:ss"), DateEditToDate.DateTime.ToString("yyyy-MM-dd HH:mm:ss"))
                '    ElseIf CheckEdit1.Checked = True Then   'all logs
                '        LogResult = axCZKEM1.ReadGeneralLogData(iMachineNumber)
                '    ElseIf CheckEdit2.Checked Then   ' new logs
                '        LogResult = axCZKEM1.ReadNewGLogData(iMachineNumber)
                '    End If
                '    If LogResult Then 'read all the attendance records to the memory
                '        XtraMasterTest.LabelControlStatus.Text = "Downloading from " & IP & "..."
                '        Application.DoEvents()
                '        'get records from the memory
                '        Dim x As Integer = 0
                '        Dim startdate As DateTime
                '        Dim paycodelist As New List(Of String)()

                '        While axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, sdwEnrollNumber, idwVerifyMode, idwInOutMode, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwWorkcode)
                '            paycodelist.Add(sdwEnrollNumber)
                '            Dim punchdate = idwYear.ToString() & "-" + idwMonth.ToString("00") & "-" & idwDay.ToString("00") & " " & idwHour.ToString("00") & ":" & idwMinute.ToString("00") & ":" & idwSecond.ToString("00")
                '            If x = 0 Then
                '                startdate = punchdate
                '            End If
                '            com.funcGetGeneralLogDataZK(sdwEnrollNumber, idwVerifyMode, idwInOutMode, punchdate, idwWorkcode, Purpose, IN_OUT, ID_NO, datafile)
                '            x = x + 1
                '        End While
                '        Dim paycodeArray = paycodelist.Distinct.ToArray ' paycodelist.ToArray
                '        ds = New DataSet
                '        For i As Integer = 0 To paycodeArray.Length - 1
                '            Dim PRESENTCARDNO As String
                '            If IsNumeric(paycodeArray(i)) Then
                '                PRESENTCARDNO = Convert.ToInt64(paycodeArray(i)).ToString("000000000000")
                '            Else
                '                PRESENTCARDNO = paycodeArray(i)
                '            End If
                '            Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & PRESENTCARDNO & "'"
                '            ds = New DataSet
                '            If Common.servername = "Access" Then
                '                adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                '                adapA.Fill(ds)
                '            Else
                '                adap = New SqlDataAdapter(sSqltmp, Common.con)
                '                adap.Fill(ds)
                '            End If
                '            If ds.Tables(0).Rows.Count > 0 Then
                '                cn.Remove_Duplicate_Punches(startdate, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim)
                '                If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                '                    com.Process_AllRTC(startdate.AddDays(-1), Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim)
                '                Else
                '                    com.Process_AllnonRTC(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim)
                '                End If
                '            End If
                '        Next
                '    Else
                '        Cursor = Cursors.Default
                '        axCZKEM1.GetLastError(idwErrorCode)
                '        If idwErrorCode <> 0 Then
                '            failIP.Add(IP.ToString)
                '        Else
                '            'MsgBox("No data from terminal returns!", MsgBoxStyle.Exclamation, "Error")
                '        End If
                '    End If
                '    axCZKEM1.EnableDevice(iMachineNumber, True)
                '    If clearLog = True Then
                '        axCZKEM1.ClearGLog(iMachineNumber)
                '    End If
                'Else
                '    axCZKEM1.GetLastError(idwErrorCode)
                '    failIP.Add(IP.ToString)
                '    Continue For
                'End If
                'axCZKEM1.Disconnect()
                'If Common.servername = "Access" Then
                '    If Common.con1.State <> ConnectionState.Open Then
                '        Common.con1.Open()
                '    End If
                '    cmd1 = New OleDbCommand("delete from Rawdata", Common.con1)
                '    cmd1.ExecuteNonQuery()
                '    If Common.con1.State <> ConnectionState.Closed Then
                '        Common.con1.Close()
                '    End If
                'Else
                '    If Common.con.State <> ConnectionState.Open Then
                '        Common.con.Open()
                '    End If
                '    cmd = New SqlCommand("delete from Rawdata", Common.con)
                '    cmd.ExecuteNonQuery()
                '    If Common.con.State <> ConnectionState.Closed Then
                '        Common.con.Close()
                '    End If
                'End If
            ElseIf DeviceType = "HKSeries" Then

                Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                Dim DeviceAdd As String = IP, userName As String = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("HLogin")), pwd As String = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("HPassword"))
                Dim lUserID As Integer = -1
                Dim failReason
                Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
                If logistatus = False Then
                    XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                Else
                    serialNo = System.Text.Encoding.UTF8.GetString(struDeviceInfoV40.struDeviceV30.sSerialNumber).TrimEnd(Microsoft.VisualBasic.ChrW(92))
                    datafile = System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"
                    'IP = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("LOCATION"))
                    IN_OUT = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("IN_OUT"))
                    ID_NO = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("ID_NO"))
                    Dim m_struAcsEventCond As CHCNetSDK.NET_DVR_ACS_EVENT_COND = New CHCNetSDK.NET_DVR_ACS_EVENT_COND
                    m_struAcsEventCond.Init()
                    Dim m_lUserID As Integer = 0
                    Dim m_iDeviceIndex As Integer = 0
                    Dim MinorType As String = Nothing
                    Dim MajorType As String = Nothing
                    Dim m_lLogNum As Integer = 0

                    'Dim g_formList As DeviceLogList = DeviceLogList.Instance
                    'Dim g_fGetAcsEventCallback As CHCNetSDK.RemoteConfigCallback = Nothing
                    Dim CsTemp As String = Nothing
                    m_lUserID = lUserID


                    m_struAcsEventCond.dwSize = CType(Marshal.SizeOf(m_struAcsEventCond), UInteger)
                    MajorType = "All" 'comboBoxMainType.SelectedItem.ToString
                    m_struAcsEventCond.dwMajor = GetAcsEventType.ReturnMajorTypeValue(MajorType)
                    MinorType = "All" 'comboBoxSecondType.SelectedItem.ToString
                    m_struAcsEventCond.dwMinor = GetAcsEventType.ReturnMinorTypeValue(MinorType)
                    m_struAcsEventCond.struStartTime.dwYear = DateEditFrmDate.DateTime.Year
                    m_struAcsEventCond.struStartTime.dwMonth = DateEditFrmDate.DateTime.Month
                    m_struAcsEventCond.struStartTime.dwDay = DateEditFrmDate.DateTime.Day
                    m_struAcsEventCond.struStartTime.dwHour = DateEditFrmDate.DateTime.Hour
                    m_struAcsEventCond.struStartTime.dwMinute = DateEditFrmDate.DateTime.Minute
                    m_struAcsEventCond.struStartTime.dwSecond = DateEditFrmDate.DateTime.Second
                    m_struAcsEventCond.struEndTime.dwYear = DateEditToDate.DateTime.Year
                    m_struAcsEventCond.struEndTime.dwMonth = DateEditToDate.DateTime.Month
                    m_struAcsEventCond.struEndTime.dwDay = DateEditToDate.DateTime.Day
                    m_struAcsEventCond.struEndTime.dwHour = DateEditToDate.DateTime.Hour
                    m_struAcsEventCond.struEndTime.dwMinute = DateEditToDate.DateTime.Minute
                    m_struAcsEventCond.struEndTime.dwSecond = DateEditToDate.DateTime.Second
                    'If Not StrToByteArray(m_struAcsEventCond.byCardNo, textBoxCardNo.Text) Then
                    If Not cn.StrToByteArray(m_struAcsEventCond.byCardNo, "") Then
                        Return
                    End If
                    If Not cn.StrToByteArray(m_struAcsEventCond.byName, "") Then
                        Return
                    End If
                    m_struAcsEventCond.byPicEnable = 0
                    Dim BeginSerialNo As UInteger = 0
                    m_struAcsEventCond.dwBeginSerialNo = BeginSerialNo
                    Dim EndSerialNo As UInteger = 0
                    m_struAcsEventCond.dwEndSerialNo = EndSerialNo
                    m_struAcsEventCond.dwIOTChannelNo = 0 ' UInteger.Parse(textBoxChanNo.Text)
                    m_struAcsEventCond.wInductiveEventType = GetAcsEventType.ReturnInductiveEventTypeValue("all")
                    m_struAcsEventCond.bySearchType = 0
                    m_struAcsEventCond.szMonitorID = "" 'textBoxMonitorID.Text
                    m_struAcsEventCond.byTimeType = 0
                    If Not cn.StrToByteArray(m_struAcsEventCond.byEmployeeNo, "") Then
                        Return
                    End If

                    m_lLogNum = 0
                    Dim dwSize As UInteger = CType(Marshal.SizeOf(m_struAcsEventCond), UInteger)
                    Dim ptrCond As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Integer))
                    Marshal.StructureToPtr(m_struAcsEventCond, ptrCond, False)
                    g_fGetAcsEventCallback = New CHCNetSDK.RemoteConfigCallback(AddressOf ProcessGetAcsEventCallback)
                    m_lGetAcsEvent = CHCNetSDK.NET_DVR_StartRemoteConfig(m_lUserID, CHCNetSDK.NET_DVR_GET_ACS_EVENT, ptrCond, CType(dwSize, Integer), g_fGetAcsEventCallback, Handle)
                    If (-1 = m_lGetAcsEvent) Then
                        'g_formList.AddLog(m_iDeviceIndex, AcsDemoPublic.OPERATION_FAIL_T, "NET_DVR_GET_ACS_EVENT")
                        Marshal.FreeHGlobal(ptrCond)
                    Else
                        'g_formList.AddLog(m_iDeviceIndex, AcsDemoPublic.OPERATION_SUCC_T, "NET_DVR_GET_ACS_EVENT")
                        Marshal.FreeHGlobal(ptrCond)
                    End If
                    'logistatus = cn.HikvisionLogOut(lUserID)

                End If
            End If
        Next
        '"Connection fail" ' write logic to show connection fail devices
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        SplashScreenManager.CloseForm(False)
        Dim failedIpArr() As String = failIP.ToArray
        Dim failIpStr As String = ""
        'If failedIpArr.Length > 0 Then
        '    For i As Integer = 0 To failedIpArr.Length - 1
        '        failIpStr = failIpStr & " " & failedIpArr(i)
        '    Next
        '    XtraMessageBox.Show(ulf, "<size=10>" & failIpStr & " failed to download data</size>", "Failed")
        'Else
        '    XtraMessageBox.Show(ulf, "<size=10>Download Started...</size>", "Success")
        'End If
        Dim msg As New Message("Downloading", "Download Started...")
        XtraMaster.AlertControl1.Show(XtraMaster, msg.Caption, msg.Text, "", msg.Image, msg)
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
    End Sub
    Private Sub ProcessGetAcsEventCallback(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
        If dwType = CUInt(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_DATA) Then
            Dim lpAcsEventCfg As CHCNetSDK.NET_DVR_ACS_EVENT_CFG = New CHCNetSDK.NET_DVR_ACS_EVENT_CFG()
            lpAcsEventCfg = CType(Marshal.PtrToStructure(lpBuffer, GetType(CHCNetSDK.NET_DVR_ACS_EVENT_CFG)), CHCNetSDK.NET_DVR_ACS_EVENT_CFG)
            Dim ptrAcsEventCfg As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(lpAcsEventCfg))
            Marshal.StructureToPtr(lpAcsEventCfg, ptrAcsEventCfg, True)
            'AddAcsEventToList(lpAcsEventCfg)
            CHCNetSDK.PostMessage(pUserData, CHCNetSDK.WM_MSG_ADD_ACS_EVENT_TOLIST, CInt(ptrAcsEventCfg), 0)
        ElseIf dwType = CUInt(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_STATUS) Then
            Dim dwStatus As Integer = Marshal.ReadInt32(lpBuffer)
            If dwStatus = CUInt(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_SUCCESS) Then
                CHCNetSDK.PostMessage(pUserData, CHCNetSDK.WM_MSG_GET_ACS_EVENT_FINISH, 0, 0)
            ElseIf dwStatus = CUInt(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_FAILED) Then
                'g_formList.AddLog(m_iDeviceIndex, AcsDemoPublic.OPERATION_FAIL_T, "NET_DVR_GET_ACS_EVENT failed")
            End If
        End If
    End Sub
    Protected Overrides Sub DefWndProc(ByRef m As System.Windows.Forms.Message)
        Select Case (m.Msg)
            Case CHCNetSDK.WM_MSG_ADD_ACS_EVENT_TOLIST
                Dim pAcsEventCfg As IntPtr = CType(m.WParam.ToInt32, IntPtr)
                Dim struEventCfg As CHCNetSDK.NET_DVR_ACS_EVENT_CFG = New CHCNetSDK.NET_DVR_ACS_EVENT_CFG
                struEventCfg = CType(Marshal.PtrToStructure(pAcsEventCfg, GetType(CHCNetSDK.NET_DVR_ACS_EVENT_CFG)), CHCNetSDK.NET_DVR_ACS_EVENT_CFG)
                Marshal.FreeHGlobal(pAcsEventCfg)
                AddAcsEventToList(struEventCfg)
            Case CHCNetSDK.WM_MSG_GET_ACS_EVENT_FINISH
                GetAcsEventFinish()
            Case Else
                MyBase.DefWndProc(m)
        End Select
    End Sub
    Private Sub GetAcsEventFinish()
        Dim msg As New Message("Success", "Download Finished")
        XtraMaster.AlertControl1.Show(XtraMaster, msg.Caption, msg.Text, "", msg.Image, msg)
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()

        CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetAcsEvent)
        m_lGetAcsEvent = -1
        'm_lLogNum = 0
        'g_formList.AddLog(m_iDeviceIndex, AcsDemoPublic.OPERATION_SUCC_T, "NET_DVR_GET_ACS_EVENT finish")
        Return
    End Sub

    Public Delegate Sub SetTextCallback(ByRef struEventCfg As CHCNetSDK.NET_DVR_ACS_EVENT_CFG)
    Private Sub AddAcsEventToList(ByRef struEventCfg As CHCNetSDK.NET_DVR_ACS_EVENT_CFG)
        If InvokeRequired = True Then
            Dim d As SetTextCallback = New SetTextCallback(AddressOf Me.AddAcsEventToList)
            Me.Invoke(d, New Object() {struEventCfg})
            'Dim tmp = IN_OUT
        Else
            'Threading.Thread.MemoryBarrier()
            'Thread.Sleep(20)
            'MsgBox(struEventCfg.struAcsEventInfo.dwSerialNo.ToString)
            Dim CsTemp As String
            Dim cn As Common = New Common
            Dim con1 As OleDbConnection
            Dim con As SqlConnection
            If Common.servername = "Access" Then
                con1 = New OleDbConnection(Common.ConnectionString)
            Else
                con = New SqlConnection(Common.ConnectionString)
            End If

            Dim CARDNO As String = System.Text.Encoding.UTF8.GetString(struEventCfg.struAcsEventInfo.byCardNo).Trim
            Dim EMPNO As String = System.Text.Encoding.UTF8.GetString(struEventCfg.struAcsEventInfo.byEmployeeNo).Trim
            If CARDNO = "" And EMPNO.Trim = "" Then
                Exit Sub
            ElseIf CARDNO = "" And EMPNO <> "" Then
                CARDNO = EMPNO
            End If

            If IsNumeric(CARDNO) Then
                CARDNO = Convert.ToInt64(CARDNO).ToString("000000000000")
            End If


            Dim OFFICEPUNCH As String = Convert.ToDateTime(cn.GetStrLogTime(struEventCfg.struTime)).ToString("yyyy-MM-dd HH:mm:ss")
            XtraMasterTest.LabelControlStatus.Text = "Downloading " & CARDNO & " " & OFFICEPUNCH
            Application.DoEvents()

            'Dim IP As String = struEventCfg.struRemoteHostAddr.sIpV4

            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            Dim sSql As String

            ''insert in text file
            'Dim fs As FileStream = New FileStream(datafile, FileMode.OpenOrCreate, FileAccess.Write)
            'Dim sw As StreamWriter = New StreamWriter(fs)
            ''find the end of the underlying filestream
            'sw.BaseStream.Seek(0, SeekOrigin.End)
            'sw.WriteLine(CARDNO & " " & OFFICEPUNCH & " " & ID_NO)
            'sw.Flush()
            'sw.Close()
            ''end insert in text file


            'sSql = "Select Paycode  from tblEmployee where presentcardno='" & CARDNO & "'"
            'sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.presentcardno = '" & CARDNO & "'"
            sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & CARDNO & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"

            ds = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                Dim paycode As String = ds.Tables(0).Rows(0)("Paycode").ToString.Trim
                Dim ISROUNDTHECLOCKWORK As String = ds.Tables(0).Rows(0)("ISROUNDTHECLOCKWORK").ToString.Trim
                'MsgBox(serialNo)
                'Dim sSql2 As String = "select * from tblMachine where MAC_ADDRESS ='" & serialNo.Trim & "'"
                'Dim adap1 As SqlDataAdapter
                'Dim adapA1 As OleDbDataAdapter
                'Dim ds3 As DataSet = New DataSet()
                'If Common.servername = "Access" Then
                '    adapA1 = New OleDbDataAdapter(sSql2, con1)
                '    adapA1.Fill(ds3)
                'Else
                '    adap1 = New SqlDataAdapter(sSql2, con)
                '    adap1.Fill(ds3)
                'End If

                If IN_OUT = "B" Then
                    Dim ds2 As DataSet = New DataSet
                    Dim sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & CARDNO & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd") & "'"

                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql1, con1)
                        adapA.Fill(ds2)
                    Else
                        sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & CARDNO & "' and convert(varchar, OFFICEPUNCH, 23) = '" & Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd") & "'"
                        adap = New SqlDataAdapter(sSql1, con)
                        adap.Fill(ds2)
                    End If

                    If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                        IN_OUT = "I"
                    Else
                        If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                            IN_OUT = "I"
                        Else
                            IN_OUT = "O"
                        End If
                    End If
                End If
                Try

                    Dim del As invokeDelegate = Function()
                                                    XtraMasterTest.LabelControlStatus.Text = "Downloading " & CARDNO & " " & OFFICEPUNCH
                                                End Function
                    Invoke(del)
                    Thread.Sleep(200)
                    If Common.servername = "Access" Then
                        con1.Open()
                        cmd1 = New OleDbCommand("insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & paycode & "' ) ", con1)
                        cmd1.ExecuteNonQuery()
                        cmd1 = New OleDbCommand("insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & paycode & "' ) ", con1)
                        cmd1.ExecuteNonQuery()
                        con1.Close()
                    Else
                        con.Open()
                        cmd = New SqlCommand("insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & paycode & "' ) ", con)
                        cmd.ExecuteNonQuery()
                        cmd = New SqlCommand("insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & paycode & "' ) ", con)
                        cmd.ExecuteNonQuery()
                        con.Close()
                    End If
                    Dim pDate As DateTime = Convert.ToDateTime(OFFICEPUNCH)
                    cn.Remove_Duplicate_Punches(pDate, paycode, ds.Tables(0).Rows(0).Item("Id"))
                    If ISROUNDTHECLOCKWORK = "Y" Then
                        cn.Process_AllRTC(pDate.AddDays(-1), Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"))
                    Else
                        cn.Process_AllnonRTC(pDate, Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"))
                    End If
                    'sms
                    If Common.g_SMSApplicable = "" Then
                        Common.Load_SMS_Policy()
                    End If
                    If Common.g_SMSApplicable = "Y" Then
                        If Common.g_isAllSMS = "Y" Then
                            Try
                                If ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim <> "" Then
                                    Dim SmsContent As String = Common.g_AllContent1 & " " & OFFICEPUNCH & " " & Common.g_AllContent2
                                    cn.sendSMS(ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim, SmsContent)
                                End If
                            Catch ex As Exception

                            End Try
                        End If
                    End If
                    'sms end
                Catch ex As Exception

                End Try
            End If


            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()

            ''Me.listViewEvent.BeginUpdate()
            'Dim Item As ListViewItem = New ListViewItem
            ''Item.Text = (m_lLogNum + 1).ToString
            'Dim LogTime As String = cn.GetStrLogTime(struEventCfg.struTime)
            'Item.SubItems.Add(LogTime)
            'Dim Major As String = cn.ProcessMajorType(struEventCfg.dwMajor)
            'Item.SubItems.Add(Major)
            'CsTemp = cn.ProcessMinorType(struEventCfg)
            'Item.SubItems.Add(CsTemp)

            'CsTemp = cn.CardTypeMap(struEventCfg)
            'Item.SubItems.Add(CsTemp)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.byWhiteListNo.ToString)
            ''WhiteList
            'CsTemp = cn.ProcessReportChannel(struEventCfg)
            'Item.SubItems.Add(CsTemp)
            'CsTemp = cn.ProcessCardReader(struEventCfg)
            'Item.SubItems.Add(CsTemp)
            'CsTemp = struEventCfg.struAcsEventInfo.dwCardReaderNo.ToString
            'Item.SubItems.Add(CsTemp)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwDoorNo.ToString)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwVerifyNo.ToString)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwAlarmInNo.ToString)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwAlarmOutNo.ToString)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwCaseSensorNo.ToString)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwRs485No.ToString)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwMultiCardGroupNo.ToString)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.wAccessChannel.ToString)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.byDeviceNo.ToString)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwEmployeeNo.ToString)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.byDistractControlNo.ToString)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.wLocalControllerID.ToString)
            'CsTemp = cn.ProcessInternatAccess(struEventCfg)
            'Item.SubItems.Add(CsTemp)
            'CsTemp = cn.ProcessByType(struEventCfg)
            'Item.SubItems.Add(CsTemp)
            'CsTemp = cn.ProcessMacAdd(struEventCfg)
            'Item.SubItems.Add(CsTemp)
            'CsTemp = cn.ProcessSwipeCard(struEventCfg)
            'Item.SubItems.Add(CsTemp)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwSerialNo.ToString)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.byChannelControllerID.ToString)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.byChannelControllerLampID.ToString)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.byChannelControllerIRAdaptorID.ToString)
            'Item.SubItems.Add(struEventCfg.struAcsEventInfo.byChannelControllerIREmitterID.ToString)
            'If (struEventCfg.wInductiveEventType < CType(GetAcsEventType.NumOfInductiveEvent, System.UInt16)) Then
            '    Item.SubItems.Add(GetAcsEventType.FindKeyOfInductive(struEventCfg.wInductiveEventType))
            'Else
            '    Item.SubItems.Add("Invalid")
            'End If

            'Item.SubItems.Add("0")
            ''RecordChannelNum
            'CsTemp = cn.ProcessbyUserType(struEventCfg)
            'Item.SubItems.Add(CsTemp)
            'CsTemp = cn.ProcessVerifyMode(struEventCfg)
            'Item.SubItems.Add(CsTemp)
            'CsTemp = System.Text.Encoding.UTF8.GetString(struEventCfg.struAcsEventInfo.byEmployeeNo)
            'Item.SubItems.Add(CsTemp)
            'CsTemp = Nothing
            'If Item.SubItems.Item(4).Text.Trim <> "" Then
            '    Me.listViewEvent.Items.Add(Item)
            'End If
            ''Me.listViewEvent.Items.Add(Item)
            'Me.listViewEvent.EndUpdate()
            'cn.ProcessPicData(struEventCfg)
        End If
    End Sub
    Private Sub CheckEditTimePerioad_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditTimePerioad.CheckedChanged
        If Common.IsNepali = "Y" Then
            If CheckEditTimePerioad.Checked = True Then
                ComboNYearStr.Enabled = True
                ComboNMonthStr.Enabled = True
                ComboNDateStr.Enabled = True
                ComboNYearEnd.Enabled = True
                ComboNMonthEnd.Enabled = True
                ComboNDateEnd.Enabled = True
                LabelControl4.Enabled = True
                LabelControl5.Enabled = True
            Else
                ComboNYearStr.Enabled = False
                ComboNMonthStr.Enabled = False
                ComboNDateStr.Enabled = False
                ComboNYearEnd.Enabled = False
                ComboNMonthEnd.Enabled = False
                ComboNDateEnd.Enabled = False
                LabelControl4.Enabled = False
                LabelControl5.Enabled = False
            End If
        Else
            If CheckEditTimePerioad.Checked = True Then
                DateEditFrmDate.Enabled = True
                DateEditToDate.Enabled = True
                LabelControl4.Enabled = True
                LabelControl5.Enabled = True
            Else
                DateEditFrmDate.Enabled = False
                DateEditToDate.Enabled = False
                LabelControl4.Enabled = False
                LabelControl5.Enabled = False
            End If
        End If

    End Sub
End Class