﻿Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports System.Data.SqlClient
Imports System.Data.OleDb

Public Class XtraBankMaster
    'Dim res_man As ResourceManager     'declare Resource manager to access to specific cultureinfo
    'Dim cul As CultureInfo     'declare culture info
    'Dim servername As String
    'Dim ConnectionString As String
    Dim ulf As UserLookAndFeel
    Public Sub New()
        InitializeComponent()
        'Dim fs As FileStream = New FileStream("db.txt", FileMode.Open, FileAccess.Read)
        'Dim sr As StreamReader = New StreamReader(fs)
        'Dim str As String
        'Dim str1() As String
        'Do While sr.Peek <> -1
        '    str = sr.ReadLine
        '    str1 = str.Split(",")
        '    servername = str1(0)
        'Loop
        'sr.Close()
        'fs.Close()

        If Common.servername = "Access" Then
            ''ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            Me.TblBank1TableAdapter1.Fill(Me.SSSDBDataSet.tblBank1)
            GridControl1.DataSource = SSSDBDataSet.tblBank1
        Else
            ''ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            TblBankTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.TblBankTableAdapter.Fill(Me.SSSDBDataSet.tblBank)
            GridControl1.DataSource = SSSDBDataSet.tblBank
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraBankMaster_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraBankMaster).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")

        GridView1.Columns.Item(0).Caption = Common.res_man.GetString("bankcode", Common.cul)
        GridView1.Columns.Item(1).Caption = Common.res_man.GetString("bankname", Common.cul)
        GridView1.Columns.Item(2).Caption = Common.res_man.GetString("bankadd", Common.cul)

    End Sub
    Private Sub GridView1_InitNewRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles GridView1.InitNewRow
        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
    End Sub
    Private Sub GridView1_RowDeleted(sender As System.Object, e As DevExpress.Data.RowDeletedEventArgs) Handles GridView1.RowDeleted
        Me.TblBankTableAdapter.Update(Me.SSSDBDataSet.tblBank)
        Me.TblBank1TableAdapter1.Update(Me.SSSDBDataSet.tblBank1)
        XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul), Common.res_man.GetString("msgsuccess", Common.cul) & "</size>")
    End Sub
    Private Sub GridView1_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles GridView1.RowUpdated
        Me.TblBankTableAdapter.Update(Me.SSSDBDataSet.tblBank)
        Me.TblBank1TableAdapter1.Update(Me.SSSDBDataSet.tblBank1)
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            Common.LogPost("Bank Add; ") 'COMPANYCODE= '" & CellId)
        Else
            Common.LogPost("Bank Update;") ' COMPANYCODE= '" & CellId)
        End If
    End Sub
    Private Sub GridView1_ValidateRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles GridView1.ValidateRow
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim CellId As String = row(0).ToString.Trim
        If CellId = "" Then
            e.Valid = False
            e.ErrorText = "<size=10>" & Common.res_man.GetString("bankcode", Common.cul) & " " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
        End If
        Dim cellname As String = row(1).ToString.Trim
        If cellname = "" Then
            e.Valid = False
            e.ErrorText = "<size=10>" & Common.res_man.GetString("bankname", Common.cul) & " " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
        End If

        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet
            ds = New DataSet
            Dim sSql As String = "select BANKCODE from tblBank where BANKCODE = '" & row(0).ToString & "'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                e.Valid = False
                e.ErrorText = "<size=10>Duplicate Bank Code ,"
            End If
        End If


        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
    End Sub

    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", "<size=10>" & Common.res_man.GetString("confirmdeleteion", Common.cul) & "</size>", _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then

                Me.Validate()
                e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
                Dim CellId As String = row("bankCODE").ToString.Trim
                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter("select count(*) from TblEmployee where BANKCODE = '" & CellId & "'", Common.con1)
                    ds = New DataSet
                    adapA.Fill(ds)
                    If ds.Tables(0).Rows(0).Item(0).ToString > 0 Then
                        XtraMessageBox.Show(ulf, "<size=10>Bank already assigned to Employee. Cannot delete.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Me.Validate()
                        e.Handled = True
                        Exit Sub
                    End If
                Else
                    adap = New SqlDataAdapter("select count(*) from TblEmployee where BANKCODE = '" & CellId & "'", Common.con)
                    ds = New DataSet
                    adap.Fill(ds)
                    If ds.Tables(0).Rows(0).Item(0).ToString > 0 Then
                        XtraMessageBox.Show(ulf, "<size=10>Bank already assigned to Employee. Cannot delete.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Me.Validate()
                        e.Handled = True
                        Exit Sub
                    End If
                End If
                Common.LogPost("Bank Delete; Bank Code: " & CellId)
            End If
        End If
    End Sub
    Private Sub GridView1_EditFormPrepared(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventArgs) Handles GridView1.EditFormPrepared
        For Each control As Control In e.Panel.Controls
            For Each button As Control In control.Controls
                If (button.Text = "Update") Then
                    button.Text = Common.res_man.GetString("save", Common.cul)
                End If
                If (button.Text = "Cancel") Then
                    button.Text = Common.res_man.GetString("cancel", Common.cul)
                End If
            Next
        Next
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            e.BindableControls(colBANKCODE).Enabled = True 'e.RowHandle Mod 2 = 0
        Else
            e.BindableControls(colBANKCODE).Enabled = False
        End If
    End Sub
    Private Sub GridView1_ShowingPopupEditForm(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.ShowingPopupEditFormEventArgs) Handles GridView1.ShowingPopupEditForm
        For Each control As Control In e.EditForm.Controls
            Common.SetFont(control, 9)
        Next control
        e.EditForm.StartPosition = FormStartPosition.CenterParent
    End Sub
End Class
