﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraDataMaintenance
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraDataMaintenance))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.TblTimeRegisterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New ULtra.SSSDBDataSet()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPAYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateOFFICE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.colSHIFT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSTATUS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLATEARRIVAL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEARLYDEPARTURE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIN1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.colOUT1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIN2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOUT2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHOURSWORKED = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOUTWORKDURATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SidePanel2 = New DevExpress.XtraEditors.SidePanel()
        Me.XtraScrollableControl1 = New DevExpress.XtraEditors.XtraScrollableControl()
        Me.ToggleSwitch1 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.lblCardNum = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.lblGrade = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.lblName = New DevExpress.XtraEditors.LabelControl()
        Me.lblEmpGrp = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.lblDesi = New DevExpress.XtraEditors.LabelControl()
        Me.lblCat = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.lblDept = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.SimpleButtonExcel = New DevExpress.XtraEditors.SimpleButton()
        Me.ComboBoxEditNepaliYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditNEpaliMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBoxEdit2 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblTimeRegisterTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblTimeRegisterTableAdapter()
        Me.TblTimeRegister1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblTimeRegister1TableAdapter()
        Me.TblEmployee1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        Me.TblEmployeeTableAdapter = New ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblTimeRegisterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel2.SuspendLayout()
        Me.XtraScrollableControl1.SuspendLayout()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.ComboBoxEditNepaliYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GridControl1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1250, 568)
        Me.SplitContainerControl1.SplitterPosition = 1165
        Me.SplitContainerControl1.TabIndex = 4
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblTimeRegisterBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Remove.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 142)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.RepositoryItemDateEdit2})
        Me.GridControl1.Size = New System.Drawing.Size(1165, 426)
        Me.GridControl1.TabIndex = 2
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'TblTimeRegisterBindingSource
        '
        Me.TblTimeRegisterBindingSource.DataMember = "tblTimeRegister"
        Me.TblTimeRegisterBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Appearance.TopNewRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GridView1.Appearance.TopNewRow.ForeColor = System.Drawing.Color.Blue
        Me.GridView1.Appearance.TopNewRow.Options.UseFont = True
        Me.GridView1.Appearance.TopNewRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPAYCODE, Me.colDateOFFICE, Me.colSHIFT, Me.colSTATUS, Me.colLATEARRIVAL, Me.colEARLYDEPARTURE, Me.colIN1, Me.colOUT1, Me.colIN2, Me.colOUT2, Me.colHOURSWORKED, Me.colOUTWORKDURATION})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.NewItemRowText = "Click here to add new Holiday"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        '
        'colPAYCODE
        '
        Me.colPAYCODE.FieldName = "PAYCODE"
        Me.colPAYCODE.Name = "colPAYCODE"
        '
        'colDateOFFICE
        '
        Me.colDateOFFICE.Caption = "Date"
        Me.colDateOFFICE.ColumnEdit = Me.RepositoryItemDateEdit2
        Me.colDateOFFICE.FieldName = "DateOFFICE"
        Me.colDateOFFICE.Name = "colDateOFFICE"
        Me.colDateOFFICE.Visible = True
        Me.colDateOFFICE.VisibleIndex = 0
        Me.colDateOFFICE.Width = 104
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'colSHIFT
        '
        Me.colSHIFT.Caption = "Shift"
        Me.colSHIFT.FieldName = "SHIFT"
        Me.colSHIFT.Name = "colSHIFT"
        Me.colSHIFT.Visible = True
        Me.colSHIFT.VisibleIndex = 1
        Me.colSHIFT.Width = 104
        '
        'colSTATUS
        '
        Me.colSTATUS.Caption = "Status"
        Me.colSTATUS.FieldName = "STATUS"
        Me.colSTATUS.Name = "colSTATUS"
        Me.colSTATUS.Visible = True
        Me.colSTATUS.VisibleIndex = 2
        Me.colSTATUS.Width = 104
        '
        'colLATEARRIVAL
        '
        Me.colLATEARRIVAL.Caption = "Late Arrival"
        Me.colLATEARRIVAL.FieldName = "LATEARRIVAL"
        Me.colLATEARRIVAL.Name = "colLATEARRIVAL"
        Me.colLATEARRIVAL.Visible = True
        Me.colLATEARRIVAL.VisibleIndex = 3
        Me.colLATEARRIVAL.Width = 104
        '
        'colEARLYDEPARTURE
        '
        Me.colEARLYDEPARTURE.Caption = "Early Departure"
        Me.colEARLYDEPARTURE.FieldName = "EARLYDEPARTURE"
        Me.colEARLYDEPARTURE.Name = "colEARLYDEPARTURE"
        Me.colEARLYDEPARTURE.Visible = True
        Me.colEARLYDEPARTURE.VisibleIndex = 4
        Me.colEARLYDEPARTURE.Width = 104
        '
        'colIN1
        '
        Me.colIN1.Caption = "In Time"
        Me.colIN1.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colIN1.FieldName = "IN1"
        Me.colIN1.Name = "colIN1"
        Me.colIN1.Visible = True
        Me.colIN1.VisibleIndex = 5
        Me.colIN1.Width = 104
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'colOUT1
        '
        Me.colOUT1.Caption = "Lunch Out"
        Me.colOUT1.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colOUT1.FieldName = "OUT1"
        Me.colOUT1.Name = "colOUT1"
        Me.colOUT1.Visible = True
        Me.colOUT1.VisibleIndex = 6
        Me.colOUT1.Width = 104
        '
        'colIN2
        '
        Me.colIN2.Caption = "Lunch In"
        Me.colIN2.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colIN2.FieldName = "IN2"
        Me.colIN2.Name = "colIN2"
        Me.colIN2.Visible = True
        Me.colIN2.VisibleIndex = 7
        Me.colIN2.Width = 104
        '
        'colOUT2
        '
        Me.colOUT2.Caption = "Out Time"
        Me.colOUT2.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colOUT2.FieldName = "OUT2"
        Me.colOUT2.Name = "colOUT2"
        Me.colOUT2.Visible = True
        Me.colOUT2.VisibleIndex = 8
        Me.colOUT2.Width = 104
        '
        'colHOURSWORKED
        '
        Me.colHOURSWORKED.Caption = "Worked Hours"
        Me.colHOURSWORKED.FieldName = "HOURSWORKED"
        Me.colHOURSWORKED.Name = "colHOURSWORKED"
        Me.colHOURSWORKED.Visible = True
        Me.colHOURSWORKED.VisibleIndex = 9
        Me.colHOURSWORKED.Width = 104
        '
        'colOUTWORKDURATION
        '
        Me.colOUTWORKDURATION.Caption = "Over Time"
        Me.colOUTWORKDURATION.FieldName = "OTDURATION"
        Me.colOUTWORKDURATION.Name = "colOUTWORKDURATION"
        Me.colOUTWORKDURATION.Visible = True
        Me.colOUTWORKDURATION.VisibleIndex = 10
        Me.colOUTWORKDURATION.Width = 104
        '
        'SidePanel2
        '
        Me.SidePanel2.Controls.Add(Me.XtraScrollableControl1)
        Me.SidePanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanel2.Location = New System.Drawing.Point(0, 53)
        Me.SidePanel2.Name = "SidePanel2"
        Me.SidePanel2.Size = New System.Drawing.Size(1165, 89)
        Me.SidePanel2.TabIndex = 1
        Me.SidePanel2.Text = "SidePanel2"
        '
        'XtraScrollableControl1
        '
        Me.XtraScrollableControl1.Controls.Add(Me.ToggleSwitch1)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl4)
        Me.XtraScrollableControl1.Controls.Add(Me.lblCardNum)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl6)
        Me.XtraScrollableControl1.Controls.Add(Me.lblGrade)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl5)
        Me.XtraScrollableControl1.Controls.Add(Me.lblName)
        Me.XtraScrollableControl1.Controls.Add(Me.lblEmpGrp)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl12)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl7)
        Me.XtraScrollableControl1.Controls.Add(Me.lblDesi)
        Me.XtraScrollableControl1.Controls.Add(Me.lblCat)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl11)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl9)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl10)
        Me.XtraScrollableControl1.Controls.Add(Me.lblDept)
        Me.XtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraScrollableControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraScrollableControl1.Name = "XtraScrollableControl1"
        Me.XtraScrollableControl1.Size = New System.Drawing.Size(1165, 88)
        Me.XtraScrollableControl1.TabIndex = 0
        '
        'ToggleSwitch1
        '
        Me.ToggleSwitch1.Enabled = False
        Me.ToggleSwitch1.Location = New System.Drawing.Point(815, 43)
        Me.ToggleSwitch1.Name = "ToggleSwitch1"
        Me.ToggleSwitch1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitch1.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitch1.Properties.OffText = "No"
        Me.ToggleSwitch1.Properties.OnText = "Yes"
        Me.ToggleSwitch1.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitch1.TabIndex = 62
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(660, 48)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(121, 14)
        Me.LabelControl4.TabIndex = 61
        Me.LabelControl4.Text = "Round the clock work"
        '
        'lblCardNum
        '
        Me.lblCardNum.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblCardNum.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblCardNum.Appearance.Options.UseFont = True
        Me.lblCardNum.Appearance.Options.UseForeColor = True
        Me.lblCardNum.Location = New System.Drawing.Point(772, 14)
        Me.lblCardNum.Name = "lblCardNum"
        Me.lblCardNum.Size = New System.Drawing.Size(24, 14)
        Me.lblCardNum.TabIndex = 60
        Me.lblCardNum.Text = "      "
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(660, 14)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(47, 14)
        Me.LabelControl6.TabIndex = 59
        Me.LabelControl6.Text = "Card No."
        '
        'lblGrade
        '
        Me.lblGrade.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblGrade.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblGrade.Appearance.Options.UseFont = True
        Me.lblGrade.Appearance.Options.UseForeColor = True
        Me.lblGrade.Location = New System.Drawing.Point(563, 48)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(20, 14)
        Me.lblGrade.TabIndex = 58
        Me.lblGrade.Text = "     "
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(12, 14)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(31, 14)
        Me.LabelControl5.TabIndex = 47
        Me.LabelControl5.Text = "Name"
        '
        'lblName
        '
        Me.lblName.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblName.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblName.Appearance.Options.UseFont = True
        Me.lblName.Appearance.Options.UseForeColor = True
        Me.lblName.Location = New System.Drawing.Point(89, 14)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(20, 14)
        Me.lblName.TabIndex = 53
        Me.lblName.Text = "     "
        '
        'lblEmpGrp
        '
        Me.lblEmpGrp.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblEmpGrp.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblEmpGrp.Appearance.Options.UseFont = True
        Me.lblEmpGrp.Appearance.Options.UseForeColor = True
        Me.lblEmpGrp.Location = New System.Drawing.Point(333, 48)
        Me.lblEmpGrp.Name = "lblEmpGrp"
        Me.lblEmpGrp.Size = New System.Drawing.Size(20, 14)
        Me.lblEmpGrp.TabIndex = 57
        Me.lblEmpGrp.Text = "     "
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(451, 48)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(32, 14)
        Me.LabelControl12.TabIndex = 52
        Me.LabelControl12.Text = "Grade"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(221, 14)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(63, 14)
        Me.LabelControl7.TabIndex = 48
        Me.LabelControl7.Text = "Designation"
        '
        'lblDesi
        '
        Me.lblDesi.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblDesi.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblDesi.Appearance.Options.UseFont = True
        Me.lblDesi.Appearance.Options.UseForeColor = True
        Me.lblDesi.Location = New System.Drawing.Point(333, 14)
        Me.lblDesi.Name = "lblDesi"
        Me.lblDesi.Size = New System.Drawing.Size(20, 14)
        Me.lblDesi.TabIndex = 54
        Me.lblDesi.Text = "     "
        '
        'lblCat
        '
        Me.lblCat.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblCat.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblCat.Appearance.Options.UseFont = True
        Me.lblCat.Appearance.Options.UseForeColor = True
        Me.lblCat.Location = New System.Drawing.Point(87, 48)
        Me.lblCat.Name = "lblCat"
        Me.lblCat.Size = New System.Drawing.Size(20, 14)
        Me.lblCat.TabIndex = 56
        Me.lblCat.Text = "     "
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(221, 48)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(90, 14)
        Me.LabelControl11.TabIndex = 51
        Me.LabelControl11.Text = "Employee Group"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(451, 14)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl9.TabIndex = 49
        Me.LabelControl9.Text = "Department"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(10, 48)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(48, 14)
        Me.LabelControl10.TabIndex = 50
        Me.LabelControl10.Text = "Catagory"
        '
        'lblDept
        '
        Me.lblDept.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblDept.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblDept.Appearance.Options.UseFont = True
        Me.lblDept.Appearance.Options.UseForeColor = True
        Me.lblDept.Location = New System.Drawing.Point(563, 14)
        Me.lblDept.Name = "lblDept"
        Me.lblDept.Size = New System.Drawing.Size(24, 14)
        Me.lblDept.TabIndex = 55
        Me.lblDept.Text = "      "
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.SimpleButtonExcel)
        Me.SidePanel1.Controls.Add(Me.ComboBoxEditNepaliYear)
        Me.SidePanel1.Controls.Add(Me.ComboBoxEditNEpaliMonth)
        Me.SidePanel1.Controls.Add(Me.LookUpEdit1)
        Me.SidePanel1.Controls.Add(Me.ComboBoxEdit2)
        Me.SidePanel1.Controls.Add(Me.LabelControl3)
        Me.SidePanel1.Controls.Add(Me.TextEdit1)
        Me.SidePanel1.Controls.Add(Me.LabelControl2)
        Me.SidePanel1.Controls.Add(Me.TextEdit2)
        Me.SidePanel1.Controls.Add(Me.LabelControl1)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanel1.Location = New System.Drawing.Point(0, 0)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1165, 53)
        Me.SidePanel1.TabIndex = 0
        Me.SidePanel1.Text = "SidePanel1"
        '
        'SimpleButtonExcel
        '
        Me.SimpleButtonExcel.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonExcel.Appearance.Options.UseFont = True
        Me.SimpleButtonExcel.Location = New System.Drawing.Point(902, 15)
        Me.SimpleButtonExcel.Name = "SimpleButtonExcel"
        Me.SimpleButtonExcel.Size = New System.Drawing.Size(85, 23)
        Me.SimpleButtonExcel.TabIndex = 11
        Me.SimpleButtonExcel.Text = "Roster Upload"
        '
        'ComboBoxEditNepaliYear
        '
        Me.ComboBoxEditNepaliYear.Location = New System.Drawing.Point(324, 16)
        Me.ComboBoxEditNepaliYear.Name = "ComboBoxEditNepaliYear"
        Me.ComboBoxEditNepaliYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYear.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNepaliYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNepaliYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNepaliYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboBoxEditNepaliYear.Size = New System.Drawing.Size(81, 20)
        Me.ComboBoxEditNepaliYear.TabIndex = 2
        '
        'ComboBoxEditNEpaliMonth
        '
        Me.ComboBoxEditNEpaliMonth.Location = New System.Drawing.Point(630, 16)
        Me.ComboBoxEditNEpaliMonth.Name = "ComboBoxEditNEpaliMonth"
        Me.ComboBoxEditNEpaliMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNEpaliMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboBoxEditNEpaliMonth.Size = New System.Drawing.Size(86, 20)
        Me.ComboBoxEditNEpaliMonth.TabIndex = 3
        '
        'LookUpEdit1
        '
        Me.LookUpEdit1.Location = New System.Drawing.Point(76, 16)
        Me.LookUpEdit1.Name = "LookUpEdit1"
        Me.LookUpEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.Appearance.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceFocused.Options.UseFont = True
        Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PAYCODE", "PAYCODE", 30, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EMPNAME", "Name")})
        Me.LookUpEdit1.Properties.DataSource = Me.TblEmployeeBindingSource
        Me.LookUpEdit1.Properties.DisplayMember = "PAYCODE"
        Me.LookUpEdit1.Properties.MaxLength = 12
        Me.LookUpEdit1.Properties.NullText = ""
        Me.LookUpEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.LookUpEdit1.Properties.ValueMember = "PAYCODE"
        Me.LookUpEdit1.Size = New System.Drawing.Size(163, 20)
        Me.LookUpEdit1.TabIndex = 1
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'ComboBoxEdit2
        '
        Me.ComboBoxEdit2.EditValue = ""
        Me.ComboBoxEdit2.Location = New System.Drawing.Point(630, 16)
        Me.ComboBoxEdit2.Name = "ComboBoxEdit2"
        Me.ComboBoxEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit2.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit2.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit2.Properties.AppearanceDisabled.Options.UseFont = True
        Me.ComboBoxEdit2.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit2.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEdit2.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit2.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBoxEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit2.Properties.Items.AddRange(New Object() {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"})
        Me.ComboBoxEdit2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit2.Size = New System.Drawing.Size(100, 20)
        Me.ComboBoxEdit2.TabIndex = 3
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(570, 19)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(35, 14)
        Me.LabelControl3.TabIndex = 6
        Me.LabelControl3.Text = "Month"
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(324, 16)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit1.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEdit1.Properties.MaxLength = 4
        Me.TextEdit1.Size = New System.Drawing.Size(129, 20)
        Me.TextEdit1.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(275, 19)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(25, 14)
        Me.LabelControl2.TabIndex = 4
        Me.LabelControl2.Text = "Year"
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(736, 16)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit2.Properties.Appearance.Options.UseFont = True
        Me.TextEdit2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit2.Properties.MaxLength = 12
        Me.TextEdit2.Size = New System.Drawing.Size(129, 20)
        Me.TextEdit2.TabIndex = 3
        Me.TextEdit2.Visible = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(12, 19)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(46, 14)
        Me.LabelControl1.TabIndex = 2
        Me.LabelControl1.Text = "Paycode"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(80, 568)
        Me.MemoEdit1.TabIndex = 4
        '
        'TblTimeRegisterTableAdapter
        '
        Me.TblTimeRegisterTableAdapter.ClearBeforeFill = True
        '
        'TblTimeRegister1TableAdapter1
        '
        Me.TblTimeRegister1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'XtraDataMaintenance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "XtraDataMaintenance"
        Me.Size = New System.Drawing.Size(1250, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblTimeRegisterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel2.ResumeLayout(False)
        Me.XtraScrollableControl1.ResumeLayout(False)
        Me.XtraScrollableControl1.PerformLayout()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        Me.SidePanel1.PerformLayout()
        CType(Me.ComboBoxEditNepaliYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit2 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SidePanel2 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TblTimeRegisterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As ULtra.SSSDBDataSet
    Friend WithEvents colPAYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateOFFICE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTATUS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLATEARRIVAL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEARLYDEPARTURE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIN1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOUT1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIN2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOUT2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHOURSWORKED As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOUTWORKDURATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblTimeRegisterTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblTimeRegisterTableAdapter
    Friend WithEvents TblTimeRegister1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblTimeRegister1TableAdapter
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents TblEmployee1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblEmployeeTableAdapter As ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents ComboBoxEditNepaliYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNEpaliMonth As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents XtraScrollableControl1 As DevExpress.XtraEditors.XtraScrollableControl
    Friend WithEvents ToggleSwitch1 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCardNum As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblGrade As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEmpGrp As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblDesi As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCat As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblDept As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonExcel As DevExpress.XtraEditors.SimpleButton

End Class
