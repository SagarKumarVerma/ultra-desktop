﻿Imports System
Imports System.Runtime.InteropServices
Namespace AscDemo
    Public Class CHCNetSDK_old
        Public Const NET_DVR_NOERROR As Integer = 0
        Public Const NET_DVR_PASSWORD_ERROR As Integer = 1
        Public Const NET_DVR_NOENOUGHPRI As Integer = 2
        Public Const NET_DVR_NOINIT As Integer = 3
        Public Const NET_DVR_CHANNEL_ERROR As Integer = 4
        Public Const NET_DVR_OVER_MAXLINK As Integer = 5
        Public Const NET_DVR_VERSIONNOMATCH As Integer = 6
        Public Const NET_DVR_NETWORK_FAIL_CONNECT As Integer = 7
        Public Const NET_DVR_NETWORK_SEND_ERROR As Integer = 8
        Public Const NET_DVR_NETWORK_RECV_ERROR As Integer = 9
        Public Const NET_DVR_NETWORK_RECV_TIMEOUT As Integer = 10
        Public Const NET_DVR_NETWORK_ERRORDATA As Integer = 11
        Public Const NET_DVR_ORDER_ERROR As Integer = 12
        Public Const NET_DVR_OPERNOPERMIT As Integer = 13
        Public Const NET_DVR_COMMANDTIMEOUT As Integer = 14
        Public Const NET_DVR_ERRORSERIALPORT As Integer = 15
        Public Const NET_DVR_ERRORALARMPORT As Integer = 16
        Public Const NET_DVR_PARAMETER_ERROR As Integer = 17
        Public Const NET_DVR_CHAN_EXCEPTION As Integer = 18
        Public Const NET_DVR_NODISK As Integer = 19
        Public Const NET_DVR_ERRORDISKNUM As Integer = 20
        Public Const NET_DVR_DISK_FULL As Integer = 21
        Public Const NET_DVR_DISK_ERROR As Integer = 22
        Public Const NET_DVR_NOSUPPORT As Integer = 23
        Public Const NET_DVR_BUSY As Integer = 24
        Public Const NET_DVR_MODIFY_FAIL As Integer = 25
        Public Const NET_DVR_PASSWORD_FORMAT_ERROR As Integer = 26
        Public Const NET_DVR_DISK_FORMATING As Integer = 27
        Public Const NET_DVR_DVRNORESOURCE As Integer = 28
        Public Const NET_DVR_DVROPRATEFAILED As Integer = 29
        Public Const NET_DVR_OPENHOSTSOUND_FAIL As Integer = 30
        Public Const NET_DVR_DVRVOICEOPENED As Integer = 31
        Public Const NET_DVR_TIMEINPUTERROR As Integer = 32
        Public Const NET_DVR_NOSPECFILE As Integer = 33
        Public Const NET_DVR_CREATEFILE_ERROR As Integer = 34
        Public Const NET_DVR_FILEOPENFAIL As Integer = 35
        Public Const NET_DVR_OPERNOTFINISH As Integer = 36
        Public Const NET_DVR_GETPLAYTIMEFAIL As Integer = 37
        Public Const NET_DVR_PLAYFAIL As Integer = 38
        Public Const NET_DVR_FILEFORMAT_ERROR As Integer = 39
        Public Const NET_DVR_DIR_ERROR As Integer = 40
        Public Const NET_DVR_ALLOC_RESOURCE_ERROR As Integer = 41
        Public Const NET_DVR_AUDIO_MODE_ERROR As Integer = 42
        Public Const NET_DVR_NOENOUGH_BUF As Integer = 43
        Public Const NET_DVR_CREATESOCKET_ERROR As Integer = 44
        Public Const NET_DVR_SETSOCKET_ERROR As Integer = 45
        Public Const NET_DVR_MAX_NUM As Integer = 46
        Public Const NET_DVR_USERNOTEXIST As Integer = 47
        Public Const NET_DVR_WRITEFLASHERROR As Integer = 48
        Public Const NET_DVR_UPGRADEFAIL As Integer = 49
        Public Const NET_DVR_CARDHAVEINIT As Integer = 50
        Public Const NET_DVR_PLAYERFAILED As Integer = 51
        Public Const NET_DVR_MAX_USERNUM As Integer = 52
        Public Const NET_DVR_GETLOCALIPANDMACFAIL As Integer = 53
        Public Const NET_DVR_NOENCODEING As Integer = 54
        Public Const NET_DVR_IPMISMATCH As Integer = 55
        Public Const NET_DVR_MACMISMATCH As Integer = 56
        Public Const NET_DVR_USER_LOCKED As Integer = 153
        Public Const NET_DVR_DEV_ADDRESS_MAX_LEN As Integer = 129
        Public Const NET_DVR_LOGIN_USERNAME_MAX_LEN As Integer = 64
        Public Const NET_DVR_LOGIN_PASSWD_MAX_LEN As Integer = 64
        Public Const SERIALNO_LEN As Integer = 48
        Public Const STREAM_ID_LEN As Integer = 32
        Public Const MAX_AUDIO_V40 As Integer = 8
        Public Const LOG_INFO_LEN As Integer = 11840
        Public Const MAX_NAMELEN As Integer = 16
        Public Const MAX_DOMAIN_NAME As Integer = 64
        Public Const MAX_ETHERNET As Integer = 2
        Public Const NAME_LEN As Integer = 32
        Public Const PASSWD_LEN As Integer = 16
        Public Const MAX_RIGHT As Integer = 32
        Public Const MACADDR_LEN As Integer = 6
        Public Const DEV_TYPE_NAME_LEN As Integer = 24
        Public Const MAX_ANALOG_CHANNUM As Integer = 32
        Public Const MAX_IP_CHANNEL As Integer = 32
        Public Const MAX_CHANNUM_V30 As Integer = (MAX_ANALOG_CHANNUM + MAX_IP_CHANNEL)
        Public Const MAX_CHANNUM_V40 As Integer = 512
        Public Const MAX_IP_DEVICE_V40 As Integer = 64
        Public Const DEV_ID_LEN As Integer = 32
        Public Const MAX_IP_DEVICE As Integer = 32
        Public Const MAX_IP_ALARMIN_V40 As Integer = 4096
        Public Const MAX_IP_ALARMOUT_V40 As Integer = 4096
        Public Const MAX_IP_ALARMIN As Integer = 128
        Public Const MAX_IP_ALARMOUT As Integer = 64
        Public Const URL_LEN As Integer = 240
        Public Const MAX_AUDIOOUT_PRO_TYPE As Integer = 8
        Public Const ACS_ABILITY As Integer = 2049
        Public Const NET_DVR_CLEAR_ACS_PARAM As Integer = 2118
        Public Const NET_DVR_GET_ACS_EVENT As Integer = 2514
        'clear acs host parameters

        Public Const COMM_ALARM_ACS As Integer = 20482
        Public Const MAJOR_ALARM As Integer = 1
        Public Const MINOR_ALARMIN_SHORT_CIRCUIT As Integer = 1024
        Public Const MINOR_ALARMIN_BROKEN_CIRCUIT As Integer = 1025
        Public Const MINOR_ALARMIN_EXCEPTION As Integer = 1026
        Public Const MINOR_ALARMIN_RESUME As Integer = 1027
        Public Const MINOR_HOST_DESMANTLE_ALARM As Integer = 1028
        Public Const MINOR_HOST_DESMANTLE_RESUME As Integer = 1029
        Public Const MINOR_CARD_READER_DESMANTLE_ALARM As Integer = 1030
        Public Const MINOR_CARD_READER_DESMANTLE_RESUME As Integer = 1031
        Public Const MINOR_CASE_SENSOR_ALARM As Integer = 1032
        Public Const MINOR_CASE_SENSOR_RESUME As Integer = 1033
        Public Const MINOR_STRESS_ALARM As Integer = 1034
        Public Const MINOR_OFFLINE_ECENT_NEARLY_FULL As Integer = 1035
        Public Const MINOR_CARD_MAX_AUTHENTICATE_FAIL As Integer = 1036
        Public Const MINOR_SD_CARD_FULL As Integer = 1037
        Public Const MINOR_LINKAGE_CAPTURE_PIC As Integer = 1038
        Public Const MINOR_SECURITY_MODULE_DESMANTLE_ALARM As Integer = 1039
        Public Const MINOR_SECURITY_MODULE_DESMANTLE_RESUME As Integer = 1040
        Public Const MINOR_POS_START_ALARM As Integer = 1041
        Public Const MINOR_POS_END_ALARM As Integer = 1042
        Public Const MINOR_FACE_IMAGE_QUALITY_LOW As Integer = 1043
        Public Const MINOR_FINGE_RPRINT_QUALITY_LOW As Integer = 1044
        Public Const MINOR_FIRE_IMPORT_SHORT_CIRCUIT As Integer = 1045
        Public Const MINOR_FIRE_IMPORT_BROKEN_CIRCUIT As Integer = 1046
        Public Const MINOR_FIRE_IMPORT_RESUME As Integer = 1047
        Public Const MINOR_FIRE_BUTTON_TRIGGER As Integer = 1048
        Public Const MINOR_FIRE_BUTTON_RESUME As Integer = 1049
        Public Const MINOR_MAINTENANCE_BUTTON_TRIGGER As Integer = 1050
        Public Const MINOR_MAINTENANCE_BUTTON_RESUME As Integer = 1051
        Public Const MINOR_EMERGENCY_BUTTON_TRIGGER As Integer = 1052
        Public Const MINOR_EMERGENCY_BUTTON_RESUME As Integer = 1053
        Public Const MINOR_DISTRACT_CONTROLLER_ALARM As Integer = 1054
        Public Const MINOR_DISTRACT_CONTROLLER_RESUME As Integer = 1055
        Public Const MINOR_CHANNEL_CONTROLLER_DESMANTLE_ALARM As Integer = 1058
        Public Const MINOR_CHANNEL_CONTROLLER_DESMANTLE_RESUME As Integer = 1059
        Public Const MINOR_CHANNEL_CONTROLLER_FIRE_IMPORT_ALARM As Integer = 1060
        Public Const MINOR_CHANNEL_CONTROLLER_FIRE_IMPORT_RESUME As Integer = 1061
        Public Const MINOR_PRINTER_OUT_OF_PAPER As Integer = 1088
        Public Const MINOR_LEGAL_EVENT_NEARLY_FULL As Integer = 1090
        Public Const MAJOR_EXCEPTION As Integer = 2
        Public Const MINOR_NET_BROKEN As Integer = 39
        Public Const MINOR_RS485_DEVICE_ABNORMAL As Integer = 58
        Public Const MINOR_RS485_DEVICE_REVERT As Integer = 59
        Public Const MINOR_DEV_POWER_ON As Integer = 1024
        Public Const MINOR_DEV_POWER_OFF As Integer = 1025
        Public Const MINOR_WATCH_DOG_RESET As Integer = 1026
        Public Const MINOR_LOW_BATTERY As Integer = 1027
        Public Const MINOR_BATTERY_RESUME As Integer = 1028
        Public Const MINOR_AC_OFF As Integer = 1029
        Public Const MINOR_AC_RESUME As Integer = 1030
        Public Const MINOR_NET_RESUME As Integer = 1031
        Public Const MINOR_FLASH_ABNORMAL As Integer = 1032
        Public Const MINOR_CARD_READER_OFFLINE As Integer = 1033
        Public Const MINOR_CARD_READER_RESUME As Integer = 1034
        Public Const MINOR_INDICATOR_LIGHT_OFF As Integer = 1035
        Public Const MINOR_INDICATOR_LIGHT_RESUME As Integer = 1036
        Public Const MINOR_CHANNEL_CONTROLLER_OFF As Integer = 1037
        Public Const MINOR_CHANNEL_CONTROLLER_RESUME As Integer = 1038
        Public Const MINOR_SECURITY_MODULE_OFF As Integer = 1039
        Public Const MINOR_SECURITY_MODULE_RESUME As Integer = 1040
        Public Const MINOR_BATTERY_ELECTRIC_LOW As Integer = 1041
        Public Const MINOR_BATTERY_ELECTRIC_RESUME As Integer = 1042
        Public Const MINOR_LOCAL_CONTROL_NET_BROKEN As Integer = 1043
        Public Const MINOR_LOCAL_CONTROL_NET_RSUME As Integer = 1044
        Public Const MINOR_MASTER_RS485_LOOPNODE_BROKEN As Integer = 1045
        Public Const MINOR_MASTER_RS485_LOOPNODE_RESUME As Integer = 1046
        Public Const MINOR_LOCAL_CONTROL_OFFLINE As Integer = 1047
        Public Const MINOR_LOCAL_CONTROL_RESUME As Integer = 1048
        Public Const MINOR_LOCAL_DOWNSIDE_RS485_LOOPNODE_BROKEN As Integer = 1049
        Public Const MINOR_LOCAL_DOWNSIDE_RS485_LOOPNODE_RESUME As Integer = 1050
        Public Const MINOR_DISTRACT_CONTROLLER_ONLINE As Integer = 1051
        Public Const MINOR_DISTRACT_CONTROLLER_OFFLINE As Integer = 1052
        Public Const MINOR_ID_CARD_READER_NOT_CONNECT As Integer = 1053
        Public Const MINOR_ID_CARD_READER_RESUME As Integer = 1054
        Public Const MINOR_FINGER_PRINT_MODULE_NOT_CONNECT As Integer = 1055
        Public Const MINOR_FINGER_PRINT_MODULE_RESUME As Integer = 1056
        Public Const MINOR_CAMERA_NOT_CONNECT As Integer = 1057
        Public Const MINOR_CAMERA_RESUME As Integer = 1058
        Public Const MINOR_COM_NOT_CONNECT As Integer = 1059
        Public Const MINOR_COM_RESUME As Integer = 1060
        Public Const MINOR_DEVICE_NOT_AUTHORIZE As Integer = 1061
        Public Const MINOR_PEOPLE_AND_ID_CARD_DEVICE_ONLINE As Integer = 1062
        Public Const MINOR_PEOPLE_AND_ID_CARD_DEVICE_OFFLINE As Integer = 1063
        Public Const MINOR_LOCAL_LOGIN_LOCK As Integer = 1064
        Public Const MINOR_LOCAL_LOGIN_UNLOCK As Integer = 1065
        Public Const MINOR_SUBMARINEBACK_COMM_BREAK As Integer = 1066
        Public Const MINOR_SUBMARINEBACK_COMM_RESUME As Integer = 1067
        Public Const MINOR_MOTOR_SENSOR_EXCEPTION As Integer = 1068
        Public Const MINOR_CAN_BUS_EXCEPTION As Integer = 1069
        Public Const MINOR_CAN_BUS_RESUME As Integer = 1070
        Public Const MINOR_GATE_TEMPERATURE_OVERRUN As Integer = 1071
        Public Const MINOR_IR_EMITTER_EXCEPTION As Integer = 1072
        Public Const MINOR_IR_EMITTER_RESUME As Integer = 1073
        Public Const MINOR_LAMP_BOARD_COMM_EXCEPTION As Integer = 1074
        Public Const MINOR_LAMP_BOARD_COMM_RESUME As Integer = 1075
        Public Const MINOR_IR_ADAPTOR_COMM_EXCEPTION As Integer = 1076
        Public Const MINOR_IR_ADAPTOR_COMM_RESUME As Integer = 1077
        Public Const MINOR_PRINTER_ONLINE As Integer = 1078
        Public Const MINOR_PRINTER_OFFLINE As Integer = 1079
        Public Const MINOR_4G_MOUDLE_ONLINE As Integer = 1080
        Public Const MINOR_4G_MOUDLE_OFFLINE As Integer = 1081
        Public Const MAJOR_OPERATION As Integer = 3
        Public Const MINOR_LOCAL_UPGRADE As Integer = 90
        Public Const MINOR_REMOTE_LOGIN As Integer = 112 'Login  (remote)
        Public Const MINOR_REMOTE_LOGOUT As Integer = 113 'Logout   (remote)
        Public Const MINOR_REMOTE_ARM As Integer = 121
        Public Const MINOR_REMOTE_DISARM As Integer = 122
        Public Const MINOR_REMOTE_REBOOT As Integer = 123
        Public Const MINOR_REMOTE_UPGRADE As Integer = 126
        Public Const MINOR_REMOTE_CFGFILE_OUTPUT As Integer = 134
        Public Const MINOR_REMOTE_CFGFILE_INTPUT As Integer = 135
        Public Const MINOR_REMOTE_ALARMOUT_OPEN_MAN As Integer = 214
        Public Const MINOR_REMOTE_ALARMOUT_CLOSE_MAN As Integer = 215
        Public Const MINOR_REMOTE_OPEN_DOOR As Integer = 1024
        Public Const MINOR_REMOTE_CLOSE_DOOR As Integer = 1025
        Public Const MINOR_REMOTE_ALWAYS_OPEN As Integer = 1026
        Public Const MINOR_REMOTE_ALWAYS_CLOSE As Integer = 1027
        Public Const MINOR_REMOTE_CHECK_TIME As Integer = 1028
        Public Const MINOR_NTP_CHECK_TIME As Integer = 1029
        Public Const MINOR_REMOTE_CLEAR_CARD As Integer = 1030
        Public Const MINOR_REMOTE_RESTORE_CFG As Integer = 1031
        Public Const MINOR_ALARMIN_ARM As Integer = 1032
        Public Const MINOR_ALARMIN_DISARM As Integer = 1033
        Public Const MINOR_LOCAL_RESTORE_CFG As Integer = 1034
        Public Const MINOR_REMOTE_CAPTURE_PIC As Integer = 1035
        Public Const MINOR_MOD_NET_REPORT_CFG As Integer = 1036
        Public Const MINOR_MOD_GPRS_REPORT_PARAM As Integer = 1037
        Public Const MINOR_MOD_REPORT_GROUP_PARAM As Integer = 1038
        Public Const MINOR_UNLOCK_PASSWORD_OPEN_DOOR As Integer = 1039
        Public Const MINOR_AUTO_RENUMBER As Integer = 1040
        Public Const MINOR_AUTO_COMPLEMENT_NUMBER As Integer = 1041
        Public Const MINOR_NORMAL_CFGFILE_INPUT As Integer = 1042
        Public Const MINOR_NORMAL_CFGFILE_OUTTPUT As Integer = 1043
        Public Const MINOR_CARD_RIGHT_INPUT As Integer = 1044
        Public Const MINOR_CARD_RIGHT_OUTTPUT As Integer = 1045
        Public Const MINOR_LOCAL_USB_UPGRADE As Integer = 1046
        Public Const MINOR_REMOTE_VISITOR_CALL_LADDER As Integer = 1047
        Public Const MINOR_REMOTE_HOUSEHOLD_CALL_LADDER As Integer = 1048
        Public Const MINOR_REMOTE_ACTUAL_GUARD As Integer = 1049
        Public Const MINOR_REMOTE_ACTUAL_UNGUARD As Integer = 1050
        Public Const MINOR_REMOTE_CONTROL_NOT_CODE_OPER_FAILED As Integer = 1051
        Public Const MINOR_REMOTE_CONTROL_CLOSE_DOOR As Integer = 1052
        Public Const MINOR_REMOTE_CONTROL_OPEN_DOOR As Integer = 1053
        Public Const MINOR_REMOTE_CONTROL_ALWAYS_OPEN_DOOR As Integer = 1054
        Public Const MAJOR_EVENT As Integer = 5
        Public Const MINOR_LEGAL_CARD_PASS As Integer = 1
        Public Const MINOR_CARD_AND_PSW_PASS As Integer = 2
        Public Const MINOR_CARD_AND_PSW_FAIL As Integer = 3
        Public Const MINOR_CARD_AND_PSW_TIMEOUT As Integer = 4
        Public Const MINOR_CARD_AND_PSW_OVER_TIME As Integer = 5
        Public Const MINOR_CARD_NO_RIGHT As Integer = 6
        Public Const MINOR_CARD_INVALID_PERIOD As Integer = 7
        Public Const MINOR_CARD_OUT_OF_DATE As Integer = 8
        Public Const MINOR_INVALID_CARD As Integer = 9
        Public Const MINOR_ANTI_SNEAK_FAIL As Integer = 10
        Public Const MINOR_INTERLOCK_DOOR_NOT_CLOSE As Integer = 11
        Public Const MINOR_NOT_BELONG_MULTI_GROUP As Integer = 12
        Public Const MINOR_INVALID_MULTI_VERIFY_PERIOD As Integer = 13
        Public Const MINOR_MULTI_VERIFY_SUPER_RIGHT_FAIL As Integer = 14
        Public Const MINOR_MULTI_VERIFY_REMOTE_RIGHT_FAIL As Integer = 15
        Public Const MINOR_MULTI_VERIFY_SUCCESS As Integer = 16
        Public Const MINOR_LEADER_CARD_OPEN_BEGIN As Integer = 17
        Public Const MINOR_LEADER_CARD_OPEN_END As Integer = 18
        Public Const MINOR_ALWAYS_OPEN_BEGIN As Integer = 19
        Public Const MINOR_ALWAYS_OPEN_END As Integer = 20
        Public Const MINOR_LOCK_OPEN As Integer = 21
        Public Const MINOR_LOCK_CLOSE As Integer = 22
        Public Const MINOR_DOOR_BUTTON_PRESS As Integer = 23
        Public Const MINOR_DOOR_BUTTON_RELEASE As Integer = 24
        Public Const MINOR_DOOR_OPEN_NORMAL As Integer = 25
        Public Const MINOR_DOOR_CLOSE_NORMAL As Integer = 26
        Public Const MINOR_DOOR_OPEN_ABNORMAL As Integer = 27
        Public Const MINOR_DOOR_OPEN_TIMEOUT As Integer = 28
        Public Const MINOR_ALARMOUT_ON As Integer = 29
        Public Const MINOR_ALARMOUT_OFF As Integer = 30
        Public Const MINOR_ALWAYS_CLOSE_BEGIN As Integer = 31
        Public Const MINOR_ALWAYS_CLOSE_END As Integer = 32
        Public Const MINOR_MULTI_VERIFY_NEED_REMOTE_OPEN As Integer = 33
        Public Const MINOR_MULTI_VERIFY_SUPERPASSWD_VERIFY_SUCCESS As Integer = 34
        Public Const MINOR_MULTI_VERIFY_REPEAT_VERIFY As Integer = 35
        Public Const MINOR_MULTI_VERIFY_TIMEOUT As Integer = 36
        Public Const MINOR_DOORBELL_RINGING As Integer = 37
        Public Const MINOR_FINGERPRINT_COMPARE_PASS As Integer = 38
        Public Const MINOR_FINGERPRINT_COMPARE_FAIL As Integer = 39
        Public Const MINOR_CARD_FINGERPRINT_VERIFY_PASS As Integer = 40
        Public Const MINOR_CARD_FINGERPRINT_VERIFY_FAIL As Integer = 41
        Public Const MINOR_CARD_FINGERPRINT_VERIFY_TIMEOUT As Integer = 42
        Public Const MINOR_CARD_FINGERPRINT_PASSWD_VERIFY_PASS As Integer = 43
        Public Const MINOR_CARD_FINGERPRINT_PASSWD_VERIFY_FAIL As Integer = 44
        Public Const MINOR_CARD_FINGERPRINT_PASSWD_VERIFY_TIMEOUT As Integer = 45
        Public Const MINOR_FINGERPRINT_PASSWD_VERIFY_PASS As Integer = 46
        Public Const MINOR_FINGERPRINT_PASSWD_VERIFY_FAIL As Integer = 47
        Public Const MINOR_FINGERPRINT_PASSWD_VERIFY_TIMEOUT As Integer = 48
        Public Const MINOR_FINGERPRINT_INEXISTENCE As Integer = 49
        Public Const MINOR_CARD_PLATFORM_VERIFY As Integer = 50
        Public Const MINOR_CALL_CENTER As Integer = 51
        Public Const MINOR_FIRE_RELAY_TURN_ON_DOOR_ALWAYS_OPEN As Integer = 52
        Public Const MINOR_FIRE_RELAY_RECOVER_DOOR_RECOVER_NORMAL As Integer = 53
        Public Const MINOR_FACE_AND_FP_VERIFY_PASS As Integer = 54
        Public Const MINOR_FACE_AND_FP_VERIFY_FAIL As Integer = 55
        Public Const MINOR_FACE_AND_FP_VERIFY_TIMEOUT As Integer = 56
        Public Const MINOR_FACE_AND_PW_VERIFY_PASS As Integer = 57
        Public Const MINOR_FACE_AND_PW_VERIFY_FAIL As Integer = 58
        Public Const MINOR_FACE_AND_PW_VERIFY_TIMEOUT As Integer = 59
        Public Const MINOR_FACE_AND_CARD_VERIFY_PASS As Integer = 60
        Public Const MINOR_FACE_AND_CARD_VERIFY_FAIL As Integer = 61
        Public Const MINOR_FACE_AND_CARD_VERIFY_TIMEOUT As Integer = 62
        Public Const MINOR_FACE_AND_PW_AND_FP_VERIFY_PASS As Integer = 63
        Public Const MINOR_FACE_AND_PW_AND_FP_VERIFY_FAIL As Integer = 64
        Public Const MINOR_FACE_AND_PW_AND_FP_VERIFY_TIMEOUT As Integer = 65
        Public Const MINOR_FACE_CARD_AND_FP_VERIFY_PASS As Integer = 66
        Public Const MINOR_FACE_CARD_AND_FP_VERIFY_FAIL As Integer = 67
        Public Const MINOR_FACE_CARD_AND_FP_VERIFY_TIMEOUT As Integer = 68
        Public Const MINOR_EMPLOYEENO_AND_FP_VERIFY_PASS As Integer = 69
        Public Const MINOR_EMPLOYEENO_AND_FP_VERIFY_FAIL As Integer = 70
        Public Const MINOR_EMPLOYEENO_AND_FP_VERIFY_TIMEOUT As Integer = 71
        Public Const MINOR_EMPLOYEENO_AND_FP_AND_PW_VERIFY_PASS As Integer = 72
        Public Const MINOR_EMPLOYEENO_AND_FP_AND_PW_VERIFY_FAIL As Integer = 73
        Public Const MINOR_EMPLOYEENO_AND_FP_AND_PW_VERIFY_TIMEOUT As Integer = 74
        Public Const MINOR_FACE_VERIFY_PASS As Integer = 75
        Public Const MINOR_FACE_VERIFY_FAIL As Integer = 76
        Public Const MINOR_EMPLOYEENO_AND_FACE_VERIFY_PASS As Integer = 77
        Public Const MINOR_EMPLOYEENO_AND_FACE_VERIFY_FAIL As Integer = 78
        Public Const MINOR_EMPLOYEENO_AND_FACE_VERIFY_TIMEOUT As Integer = 79
        Public Const MINOR_FACE_RECOGNIZE_FAIL As Integer = 80
        Public Const MINOR_FIRSTCARD_AUTHORIZE_BEGIN As Integer = 81
        Public Const MINOR_FIRSTCARD_AUTHORIZE_END As Integer = 82
        Public Const MINOR_DOORLOCK_INPUT_SHORT_CIRCUIT As Integer = 83
        Public Const MINOR_DOORLOCK_INPUT_BROKEN_CIRCUIT As Integer = 84
        Public Const MINOR_DOORLOCK_INPUT_EXCEPTION As Integer = 85
        Public Const MINOR_DOORCONTACT_INPUT_SHORT_CIRCUIT As Integer = 86
        Public Const MINOR_DOORCONTACT_INPUT_BROKEN_CIRCUIT As Integer = 87
        Public Const MINOR_DOORCONTACT_INPUT_EXCEPTION As Integer = 88
        Public Const MINOR_OPENBUTTON_INPUT_SHORT_CIRCUIT As Integer = 89
        Public Const MINOR_OPENBUTTON_INPUT_BROKEN_CIRCUIT As Integer = 90
        Public Const MINOR_OPENBUTTON_INPUT_EXCEPTION As Integer = 91
        Public Const MINOR_DOORLOCK_OPEN_EXCEPTION As Integer = 92
        Public Const MINOR_DOORLOCK_OPEN_TIMEOUT As Integer = 93
        Public Const MINOR_FIRSTCARD_OPEN_WITHOUT_AUTHORIZE As Integer = 94
        Public Const MINOR_CALL_LADDER_RELAY_BREAK As Integer = 95
        Public Const MINOR_CALL_LADDER_RELAY_CLOSE As Integer = 96
        Public Const MINOR_AUTO_KEY_RELAY_BREAK As Integer = 97
        Public Const MINOR_AUTO_KEY_RELAY_CLOSE As Integer = 98
        Public Const MINOR_KEY_CONTROL_RELAY_BREAK As Integer = 99
        Public Const MINOR_KEY_CONTROL_RELAY_CLOSE As Integer = 100
        Public Const MINOR_EMPLOYEENO_AND_PW_PASS As Integer = 101
        Public Const MINOR_EMPLOYEENO_AND_PW_FAIL As Integer = 102
        Public Const MINOR_EMPLOYEENO_AND_PW_TIMEOUT As Integer = 103
        Public Const MINOR_HUMAN_DETECT_FAIL As Integer = 104
        Public Const MINOR_PEOPLE_AND_ID_CARD_COMPARE_PASS As Integer = 105
        Public Const MINOR_PEOPLE_AND_ID_CARD_COMPARE_FAIL As Integer = 112
        Public Const MINOR_CERTIFICATE_BLACK_LIST As Integer = 113
        Public Const MINOR_LEGAL_MESSAGE As Integer = 114
        Public Const MINOR_ILLEGAL_MESSAGE As Integer = 115
        Public Const MINOR_MAC_DETECT As Integer = 116
        Public Const MINOR_DOOR_OPEN_OR_DORMANT_FAIL As Integer = 117
        Public Const MINOR_AUTH_PLAN_DORMANT_FAIL As Integer = 118
        Public Const MINOR_CARD_ENCRYPT_VERIFY_FAIL As Integer = 119
        Public Const MINOR_SUBMARINEBACK_REPLY_FAIL As Integer = 120
        Public Const MINOR_DOOR_OPEN_OR_DORMANT_OPEN_FAIL As Integer = 130
        Public Const MINOR_DOOR_OPEN_OR_DORMANT_LINKAGE_OPEN_FAIL As Integer = 132
        Public Const MINOR_TRAILING As Integer = 133
        Public Const MINOR_HEART_BEAT As Integer = 131
        Public Const MINOR_REVERSE_ACCESS As Integer = 134
        Public Const MINOR_FORCE_ACCESS As Integer = 135
        Public Const MINOR_CLIMBING_OVER_GATE As Integer = 136
        Public Const MINOR_PASSING_TIMEOUT As Integer = 137
        Public Const MINOR_INTRUSION_ALARM As Integer = 138
        Public Const MINOR_FREE_GATE_PASS_NOT_AUTH As Integer = 139
        Public Const MINOR_DROP_ARM_BLOCK As Integer = 140
        Public Const MINOR_DROP_ARM_BLOCK_RESUME As Integer = 141
        Public Const MINOR_LOCAL_FACE_MODELING_FAIL As Integer = 142
        Public Const MINOR_STAY_EVENT As Integer = 143
        Public Const MINOR_PASSWORD_MISMATCH As Integer = 151
        Public Const MINOR_EMPLOYEE_NO_NOT_EXIST As Integer = 152
        Public Const MINOR_COMBINED_VERIFY_PASS As Integer = 153
        Public Const MINOR_COMBINED_VERIFY_TIMEOUT As Integer = 154
        Public Const MINOR_VERIFY_MODE_MISMATCH As Integer = 155
        'verify mode mismatch


        Public Const CARD_PARAM_CARD_VALID As Integer = 1
        Public Const CARD_PARAM_VALID As Integer = 2
        Public Const CARD_PARAM_CARD_TYPE As Integer = 4
        Public Const CARD_PARAM_DOOR_RIGHT As Integer = 8
        Public Const CARD_PARAM_LEADER_CARD As Integer = 16
        Public Const CARD_PARAM_SWIPE_NUM As Integer = 32
        Public Const CARD_PARAM_GROUP As Integer = 64
        Public Const CARD_PARAM_PASSWORD As Integer = 128
        Public Const CARD_PARAM_RIGHT_PLAN As Integer = 256
        Public Const CARD_PARAM_SWIPED_NUM As Integer = 512
        Public Const CARD_PARAM_EMPLOYEE_NO As Integer = 1024
        Public Const ACS_CARD_NO_LEN As Integer = 32
        Public Const MAX_DOOR_NUM_256 As Integer = 256
        Public Const MAX_GROUP_NUM_128 As Integer = 128
        Public Const CARD_PASSWORD_LEN As Integer = 8
        Public Const MAX_CARD_RIGHT_PLAN_NUM As Integer = 4
        Public Const MAX_DOOR_CODE_LEN As Integer = 8
        Public Const MAX_LOCK_CODE_LEN As Integer = 8
        Public Const MAX_CASE_SENSOR_NUM As Integer = 8
        Public Const MAX_CARD_READER_NUM_512 As Integer = 512
        Public Const MAX_ALARMHOST_ALARMIN_NUM As Integer = 512
        Public Const MAX_ALARMHOST_ALARMOUT_NUM As Integer = 512
        Public Const NET_DVR_GET_ACS_WORK_STATUS_V50 As Integer = 2180
        Public Const NET_DVR_GET_CARD_CFG_V50 As Integer = 2178
        Public Const NET_DVR_SET_CARD_CFG_V50 As Integer = 2179
        'Setting up the new parameters (V50)

        Public Const NET_DVR_GET_TIMECFG As Integer = 118
        Public Const NET_DVR_SET_TIMECFG As Integer = 119
        Public Const NET_DVR_GET_AUDIOIN_VOLUME_CFG As Integer = 6355
        Public Const NET_DVR_SET_AUDIOIN_VOLUME_CFG As Integer = 6356
        Public Const NET_DVR_GET_AUDIOOUT_VOLUME_CFG As Integer = 6369
        Public Const NET_DVR_SET_AUDIOOUT_VOLUME_CFG As Integer = 6370


        Public Const DOOR_NAME_LEN As Integer = 32
        Public Const STRESS_PASSWORD_LEN As Integer = 8
        Public Const SUPER_PASSWORD_LEN As Integer = 8
        Public Const UNLOCK_PASSWORD_LEN As Integer = 8
        Public Const MAX_DOOR_NUM As Integer = 32
        Public Const MAX_GROUP_NUM As Integer = 32
        Public Const LOCAL_CONTROLLER_NAME_LEN As Integer = 32
        Public Const NET_DVR_GET_DOOR_CFG As Integer = 2108
        Public Const NET_DVR_SET_DOOR_CFG As Integer = 2109

        Public Const NET_SDK_MONITOR_ID_LEN As Integer = 64
        Public Const WM_MSG_ADD_ACS_EVENT_TOLIST As Integer = 1002
        Public Const WM_MSG_GET_ACS_EVENT_FINISH As Integer = 1003

        Public Const GROUP_NAME_LEN As Integer = 32
        Public Const NET_DVR_GET_GROUP_CFG As Integer = 2112
        Public Const NET_DVR_SET_GROUP_CFG As Integer = 2113

        Public Const MAX_ALARMHOST_VIDEO_CHAN As Integer = 64
        Public Const NET_DVR_GET_DEVICECFG_V40 As Integer = 1100
        Public Const NET_DVR_SET_DEVICECFG_V40 As Integer = 1101

        Public Const CARD_READER_DESCRIPTION As Integer = 32
        Public Const NET_DVR_GET_CARD_READER_CFG_V50 As Integer = 2505
        Public Const NET_DVR_SET_CARD_READER_CFG_V50 As Integer = 2506

        Public Const MAX_FACE_NUM As Integer = 2
        Public Const NET_DVR_GET_FACE_PARAM_CFG As Integer = 2507
        Public Const NET_DVR_SET_FACE_PARAM_CFG As Integer = 2508
        Public Const NET_DVR_DEL_FACE_PARAM_CFG As Integer = 2509
        Public Const NET_DVR_CAPTURE_FACE_INFO As Integer = 2510

        Public Const MAX_FINGER_PRINT_LEN As Integer = 768
        Public Const MAX_FINGER_PRINT_NUM As Integer = 10
        Public Const ERROR_MSG_LEN As Integer = 32
        Public Const NET_SDK_EMPLOYEE_NO_LEN As Integer = 32
        Public Const NET_DVR_GET_FINGERPRINT_CFG As Integer = 2150
        Public Const NET_DVR_SET_FINGERPRINT_CFG As Integer = 2151
        Public Const NET_DVR_DEL_FINGERPRINT_CFG As Integer = 2152
        Public Const NET_DVR_GET_FINGERPRINT_CFG_V50 As Integer = 2183
        Public Const NET_DVR_SET_FINGERPRINT_CFG_V50 As Integer = 2184

        Public Const MAX_DAYS As Integer = 7
        Public Const MAX_TIMESEGMENT_V30 As Integer = 8
        Public Const HOLIDAY_GROUP_NAME_LEN As Integer = 32
        Public Const MAX_HOLIDAY_PLAN_NUM As Integer = 16
        Public Const TEMPLATE_NAME_LEN As Integer = 32
        Public Const MAX_HOLIDAY_GROUP_NUM As Integer = 16
        Public Const NET_DVR_GET_WEEK_PLAN_CFG As Integer = 2100
        Public Const NET_DVR_SET_WEEK_PLAN_CFG As Integer = 2101
        Public Const NET_DVR_GET_DOOR_STATUS_HOLIDAY_PLAN As Integer = 2102
        Public Const NET_DVR_SET_DOOR_STATUS_HOLIDAY_PLAN As Integer = 2103
        Public Const NET_DVR_GET_DOOR_STATUS_HOLIDAY_GROUP As Integer = 2104
        Public Const NET_DVR_SET_DOOR_STATUS_HOLIDAY_GROUP As Integer = 2105
        Public Const NET_DVR_GET_DOOR_STATUS_PLAN_TEMPLATE As Integer = 2106
        Public Const NET_DVR_SET_DOOR_STATUS_PLAN_TEMPLATE As Integer = 2107
        Public Const NET_DVR_GET_VERIFY_WEEK_PLAN As Integer = 2124
        Public Const NET_DVR_SET_VERIFY_WEEK_PLAN As Integer = 2125
        Public Const NET_DVR_GET_CARD_RIGHT_WEEK_PLAN As Integer = 2126
        Public Const NET_DVR_SET_CARD_RIGHT_WEEK_PLAN As Integer = 2127
        Public Const NET_DVR_GET_VERIFY_HOLIDAY_PLAN As Integer = 2128
        Public Const NET_DVR_SET_VERIFY_HOLIDAY_PLAN As Integer = 2129
        Public Const NET_DVR_GET_CARD_RIGHT_HOLIDAY_PLAN As Integer = 2130
        Public Const NET_DVR_SET_CARD_RIGHT_HOLIDAY_PLAN As Integer = 2131
        Public Const NET_DVR_GET_VERIFY_HOLIDAY_GROUP As Integer = 2132
        Public Const NET_DVR_SET_VERIFY_HOLIDAY_GROUP As Integer = 2133
        Public Const NET_DVR_GET_CARD_RIGHT_HOLIDAY_GROUP As Integer = 2134
        Public Const NET_DVR_SET_CARD_RIGHT_HOLIDAY_GROUP As Integer = 2135
        Public Const NET_DVR_GET_VERIFY_PLAN_TEMPLATE As Integer = 2136
        Public Const NET_DVR_SET_VERIFY_PLAN_TEMPLATE As Integer = 2137
        Public Const NET_DVR_GET_CARD_RIGHT_PLAN_TEMPLATE As Integer = 2138
        Public Const NET_DVR_SET_CARD_RIGHT_PLAN_TEMPLATE As Integer = 2139
        Public Const NET_DVR_GET_CARD_RIGHT_WEEK_PLAN_V50 As Integer = 2304
        Public Const NET_DVR_SET_CARD_RIGHT_WEEK_PLAN_V50 As Integer = 2305
        Public Const NET_DVR_GET_CARD_RIGHT_HOLIDAY_PLAN_V50 As Integer = 2310
        Public Const NET_DVR_SET_CARD_RIGHT_HOLIDAY_PLAN_V50 As Integer = 2311
        Public Const NET_DVR_GET_CARD_RIGHT_HOLIDAY_GROUP_V50 As Integer = 2316
        Public Const NET_DVR_SET_CARD_RIGHT_HOLIDAY_GROUP_V50 As Integer = 2317
        Public Const NET_DVR_GET_CARD_RIGHT_PLAN_TEMPLATE_V50 As Integer = 2322
        Public Const NET_DVR_SET_CARD_RIGHT_PLAN_TEMPLATE_V50 As Integer = 2323

        Public Const NET_DVR_GET_DOOR_STATUS_PLAN As Integer = 2110
        Public Const NET_DVR_SET_DOOR_STATUS_PLAN As Integer = 2111
        Public Const NET_DVR_GET_CARD_READER_PLAN As Integer = 2142
        Public Const NET_DVR_SET_CARD_READER_PLAN As Integer = 2143
        'get card reader verify plan parameter 

        Public Const NET_DVR_GET_CARD_USERINFO_CFG As Integer = 2163
        Public Const NET_DVR_SET_CARD_USERINFO_CFG As Integer = 2164

        Public Const NET_DVR_GET_EVENT_CARD_LINKAGE_CFG_V50 As Integer = 2181

        'get event card linkage cfg 
        Public Const NET_DVR_SET_EVENT_CARD_LINKAGE_CFG_V50 As Integer = 2182

        Public Const NET_DVR_DEL_FINGERPRINT_CFG_V50 As Integer = 2517

        'delete fingerprint parameter V50
        Public Const NET_DVR_GET_EVENT_CARD_LINKAGE_CFG_V51 As Integer = 2518

        'get event card linkage cfg V51
        Public Const NET_DVR_SET_EVENT_CARD_LINKAGE_CFG_V51 As Integer = 2519

        'set event card linkage cfg V51
        Public Const NET_DVR_JSON_CONFIG As Integer = 2550

        Public Const NET_DVR_FACE_DATA_RECORD As Integer = 2551

        Public Const NET_DVR_FACE_DATA_SEARCH As Integer = 2552

        Public Const NET_DVR_FACE_DATA_MODIFY As Integer = 2553

        Public Const NET_DVR_GET_NETCFG_V30 As Integer = 1000
        Public Const NET_DVR_SET_NETCFG_V30 As Integer = 1001
        Public Const NET_DVR_GET_NETCFG_V50 As Integer = 1015
        Public Const NET_DVR_SET_NETCFG_V50 As Integer = 1016
        Public Const NET_DVR_VIDEO_CALL_SIGNAL_PROCESS As Integer = 16032



        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_DATE

            Public wYear As System.UInt16

            'year
            Public byMonth As Byte

            'month    
            Public byDay As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_SIMPLE_DAYTIME

            Public byHour As Byte

            'hour
            Public byMinute As Byte

            'minute
            Public bySecond As Byte

            'second
            Public byRes As Byte
        End Structure
        ' Time correction structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_TIME

            Public dwYear As Integer

            Public dwMonth As Integer

            Public dwDay As Integer

            Public dwHour As Integer

            Public dwMinute As Integer

            Public dwSecond As Integer
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_TIME_EX

            Public wYear As System.UInt16

            Public byMonth As Byte

            Public byDay As Byte

            Public byHour As Byte

            Public byMinute As Byte

            Public bySecond As Byte

            Public byRes As Byte
        End Structure
        ' Long config callback type
        Public Enum NET_SDK_CALLBACK_TYPE

            NET_SDK_CALLBACK_TYPE_STATUS = 0

            NET_SDK_CALLBACK_TYPE_PROGRESS

            NET_SDK_CALLBACK_TYPE_DATA
        End Enum
        ' Long config status value
        Public Enum NET_SDK_CALLBACK_STATUS_NORMAL

            NET_SDK_CALLBACK_STATUS_SUCCESS = 1000

            NET_SDK_CALLBACK_STATUS_PROCESSING

            NET_SDK_CALLBACK_STATUS_FAILED

            NET_SDK_CALLBACK_STATUS_EXCEPTION

            NET_SDK_CALLBACK_STATUS_LANGUAGE_MISMATCH

            NET_SDK_CALLBACK_STATUS_DEV_TYPE_MISMATCH

            NET_DVR_CALLBACK_STATUS_SEND_WAIT
        End Enum
        Public Enum LONG_CFG_SEND_DATA_TYPE_ENUM

            ENUM_DVR_VEHICLE_CHECK = 1

            ENUM_MSC_SEND_DATA = 2

            ENUM_ACS_SEND_DATA = 3

            ENUM_TME_CARD_SEND_DATA = 4

            ENUM_TME_VEHICLE_SEND_DATA = 5

            ENUM_DVR_DEBUG_CMD = 6

            ENUM_DVR_SCREEN_CTRL_CMD = 7

            ENUM_CVR_PASSBACK_SEND_DATA = 8

            ENUM_ACS_INTELLIGENT_IDENTITY_DATA = 9

            ENUM_VIDEO_INTERCOM_SEND_DATA = 10

            ENUM_SEND_JSON_DATA = 11
        End Enum
        Public Enum ENUM_UPGRADE_TYPE

            ENUM_UPGRADE_DVR = 0

            ENUM_UPGRADE_ACS = 1
        End Enum
        Public Enum NET_SDK_GET_NEXT_STATUS

            NET_SDK_GET_NEXT_STATUS_SUCCESS = 1000

            NET_SDK_GET_NETX_STATUS_NEED_WAIT

            NET_SDK_GET_NEXT_STATUS_FINISH

            NET_SDK_GET_NEXT_STATUS_FAILED
        End Enum
        Public Enum LONG_CFG_RECV_DATA_TYPE_ENUM

            ENUM_DVR_ERROR_CODE = 1

            ENUM_MSC_RECV_DATA = 2

            ENUM_ACS_RECV_DATA = 3
        End Enum
        Public Enum ACS_DEV_SUBEVENT_ENUM

            EVENT_ACS_HOST_ANTI_DISMANTLE = 0

            EVENT_ACS_OFFLINE_ECENT_NEARLY_FULL

            EVENT_ACS_NET_BROKEN

            EVENT_ACS_NET_RESUME

            EVENT_ACS_LOW_BATTERY

            EVENT_ACS_BATTERY_RESUME

            EVENT_ACS_AC_OFF

            EVENT_ACS_AC_RESUME

            EVENT_ACS_SD_CARD_FULL

            EVENT_ACS_LINKAGE_CAPTURE_PIC

            EVENT_ACS_IMAGE_QUALITY_LOW

            EVENT_ACS_FINGER_PRINT_QUALITY_LOW

            EVENT_ACS_BATTERY_ELECTRIC_LOW

            EVENT_ACS_BATTERY_ELECTRIC_RESUME

            EVENT_ACS_FIRE_IMPORT_SHORT_CIRCUIT

            EVENT_ACS_FIRE_IMPORT_BROKEN_CIRCUIT

            EVENT_ACS_FIRE_IMPORT_RESUME

            EVENT_ACS_MASTER_RS485_LOOPNODE_BROKEN

            EVENT_ACS_MASTER_RS485_LOOPNODE_RESUME

            EVENT_ACS_LOCAL_CONTROL_OFFLINE

            EVENT_ACS_LOCAL_CONTROL_RESUME

            EVENT_ACS_LOCAL_DOWNSIDE_RS485_LOOPNODE_BROKEN

            EVENT_ACS_LOCAL_DOWNSIDE_RS485_LOOPNODE_RESUME

            EVENT_ACS_DISTRACT_CONTROLLER_ONLINE

            EVENT_ACS_DISTRACT_CONTROLLER_OFFLINE

            EVENT_ACS_FIRE_BUTTON_TRIGGER

            EVENT_ACS_FIRE_BUTTON_RESUME

            EVENT_ACS_MAINTENANCE_BUTTON_TRIGGER

            EVENT_ACS_MAINTENANCE_BUTTON_RESUME

            EVENT_ACS_EMERGENCY_BUTTON_TRIGGER

            EVENT_ACS_EMERGENCY_BUTTON_RESUME

            EVENT_ACS_MAC_DETECT
        End Enum
        Public Enum ACS_ALARM_SUBEVENT_ENUM

            EVENT_ACS_ALARMIN_SHORT_CIRCUIT = 0

            EVENT_ACS_ALARMIN_BROKEN_CIRCUIT

            EVENT_ACS_ALARMIN_EXCEPTION

            EVENT_ACS_ALARMIN_RESUME

            EVENT_ACS_CASE_SENSOR_ALARM

            EVENT_ACS_CASE_SENSOR_RESUME
        End Enum
        Public Enum ACS_DOOR_SUBEVENT_ENUM

            EVENT_ACS_LEADER_CARD_OPEN_BEGIN = 0

            EVENT_ACS_LEADER_CARD_OPEN_END

            EVENT_ACS_ALWAYS_OPEN_BEGIN

            EVENT_ACS_ALWAYS_OPEN_END

            EVENT_ACS_ALWAYS_CLOSE_BEGIN

            EVENT_ACS_ALWAYS_CLOSE_END

            EVENT_ACS_LOCK_OPEN

            EVENT_ACS_LOCK_CLOSE

            EVENT_ACS_DOOR_BUTTON_PRESS

            EVENT_ACS_DOOR_BUTTON_RELEASE

            EVENT_ACS_DOOR_OPEN_NORMAL

            EVENT_ACS_DOOR_CLOSE_NORMAL

            EVENT_ACS_DOOR_OPEN_ABNORMAL

            EVENT_ACS_DOOR_OPEN_TIMEOUT

            EVENT_ACS_REMOTE_OPEN_DOOR

            EVENT_ACS_REMOTE_CLOSE_DOOR

            EVENT_ACS_REMOTE_ALWAYS_OPEN

            EVENT_ACS_REMOTE_ALWAYS_CLOSE

            EVENT_ACS_NOT_BELONG_MULTI_GROUP

            EVENT_ACS_INVALID_MULTI_VERIFY_PERIOD

            EVENT_ACS_MULTI_VERIFY_SUPER_RIGHT_FAIL

            EVENT_ACS_MULTI_VERIFY_REMOTE_RIGHT_FAIL

            EVENT_ACS_MULTI_VERIFY_SUCCESS

            EVENT_ACS_MULTI_VERIFY_NEED_REMOTE_OPEN

            EVENT_ACS_MULTI_VERIFY_SUPERPASSWD_VERIFY_SUCCESS

            EVENT_ACS_MULTI_VERIFY_REPEAT_VERIFY_FAIL

            EVENT_ACS_MULTI_VERIFY_TIMEOUT

            EVENT_ACS_REMOTE_CAPTURE_PIC

            EVENT_ACS_DOORBELL_RINGING

            EVENT_ACS_SECURITY_MODULE_DESMANTLE_ALARM

            EVENT_ACS_CALL_CENTER

            EVENT_ACS_FIRSTCARD_AUTHORIZE_BEGIN

            EVENT_ACS_FIRSTCARD_AUTHORIZE_END

            EVENT_ACS_DOORLOCK_INPUT_SHORT_CIRCUIT

            EVENT_ACS_DOORLOCK_INPUT_BROKEN_CIRCUIT

            EVENT_ACS_DOORLOCK_INPUT_EXCEPTION

            EVENT_ACS_DOORCONTACT_INPUT_SHORT_CIRCUIT

            EVENT_ACS_DOORCONTACT_INPUT_BROKEN_CIRCUIT

            EVENT_ACS_DOORCONTACT_INPUT_EXCEPTION

            EVENT_ACS_OPENBUTTON_INPUT_SHORT_CIRCUIT

            EVENT_ACS_OPENBUTTON_INPUT_BROKEN_CIRCUIT

            EVENT_ACS_OPENBUTTON_INPUT_EXCEPTION

            EVENT_ACS_DOORLOCK_OPEN_EXCEPTION

            EVENT_ACS_DOORLOCK_OPEN_TIMEOUT

            EVENT_ACS_FIRSTCARD_OPEN_WITHOUT_AUTHORIZE

            EVENT_ACS_CALL_LADDER_RELAY_BREAK

            EVENT_ACS_CALL_LADDER_RELAY_CLOSE

            EVENT_ACS_AUTO_KEY_RELAY_BREAK

            EVENT_ACS_AUTO_KEY_RELAY_CLOSE

            EVENT_ACS_KEY_CONTROL_RELAY_BREAK

            EVENT_ACS_KEY_CONTROL_RELAY_CLOSE

            EVENT_ACS_REMOTE_VISITOR_CALL_LADDER

            EVENT_ACS_REMOTE_HOUSEHOLD_CALL_LADDER

            EVENT_ACS_LEGAL_MESSAGE

            EVENT_ACS_ILLEGAL_MESSAGE
        End Enum
        Public Enum ACS_CARD_READER_SUBEVENT_ENUM

            EVENT_ACS_STRESS_ALARM = 0

            EVENT_ACS_CARD_READER_DESMANTLE_ALARM

            EVENT_ACS_LEGAL_CARD_PASS

            EVENT_ACS_CARD_AND_PSW_PASS

            EVENT_ACS_CARD_AND_PSW_FAIL

            EVENT_ACS_CARD_AND_PSW_TIMEOUT

            EVENT_ACS_CARD_MAX_AUTHENTICATE_FAIL

            EVENT_ACS_CARD_NO_RIGHT

            EVENT_ACS_CARD_INVALID_PERIOD

            EVENT_ACS_CARD_OUT_OF_DATE

            EVENT_ACS_INVALID_CARD

            EVENT_ACS_ANTI_SNEAK_FAIL

            EVENT_ACS_INTERLOCK_DOOR_NOT_CLOSE

            EVENT_ACS_FINGERPRINT_COMPARE_PASS

            EVENT_ACS_FINGERPRINT_COMPARE_FAIL

            EVENT_ACS_CARD_FINGERPRINT_VERIFY_PASS

            EVENT_ACS_CARD_FINGERPRINT_VERIFY_FAIL

            EVENT_ACS_CARD_FINGERPRINT_VERIFY_TIMEOUT

            EVENT_ACS_CARD_FINGERPRINT_PASSWD_VERIFY_PASS

            EVENT_ACS_CARD_FINGERPRINT_PASSWD_VERIFY_FAIL

            EVENT_ACS_CARD_FINGERPRINT_PASSWD_VERIFY_TIMEOUT

            EVENT_ACS_FINGERPRINT_PASSWD_VERIFY_PASS

            EVENT_ACS_FINGERPRINT_PASSWD_VERIFY_FAIL

            EVENT_ACS_FINGERPRINT_PASSWD_VERIFY_TIMEOUT

            EVENT_ACS_FINGERPRINT_INEXISTENCE

            EVENT_ACS_FACE_VERIFY_PASS

            EVENT_ACS_FACE_VERIFY_FAIL

            EVENT_ACS_FACE_AND_FP_VERIFY_PASS

            EVENT_ACS_FACE_AND_FP_VERIFY_FAIL

            EVENT_ACS_FACE_AND_FP_VERIFY_TIMEOUT

            EVENT_ACS_FACE_AND_PW_VERIFY_PASS

            EVENT_ACS_FACE_AND_PW_VERIFY_FAIL

            EVENT_ACS_FACE_AND_PW_VERIFY_TIMEOUT

            EVENT_ACS_FACE_AND_CARD_VERIFY_PASS

            EVENT_ACS_FACE_AND_CARD_VERIFY_FAIL

            EVENT_ACS_FACE_AND_CARD_VERIFY_TIMEOUT

            EVENT_ACS_FACE_AND_PW_AND_FP_VERIFY_PASS

            EVENT_ACS_FACE_AND_PW_AND_FP_VERIFY_FAIL

            EVENT_ACS_FACE_AND_PW_AND_FP_VERIFY_TIMEOUT

            EVENT_ACS_FACE_AND_CARD_AND_FP_VERIFY_PASS

            EVENT_ACS_FACE_AND_CARD_AND_FP_VERIFY_FAIL

            EVENT_ACS_FACE_AND_CARD_AND_FP_VERIFY_TIMEOUT

            EVENT_ACS_EMPLOYEENO_AND_FP_VERIFY_PASS

            EVENT_ACS_EMPLOYEENO_AND_FP_VERIFY_FAIL

            EVENT_ACS_EMPLOYEENO_AND_FP_VERIFY_TIMEOUT

            EVENT_ACS_EMPLOYEENO_AND_FP_AND_PW_VERIFY_PASS

            EVENT_ACS_EMPLOYEENO_AND_FP_AND_PW_VERIFY_FAIL

            EVENT_ACS_EMPLOYEENO_AND_FP_AND_PW_VERIFY_TIMEOUT

            EVENT_ACS_EMPLOYEENO_AND_FACE_VERIFY_PASS

            EVENT_ACS_EMPLOYEENO_AND_FACE_VERIFY_FAIL

            EVENT_ACS_EMPLOYEENO_AND_FACE_VERIFY_TIMEOUT

            EVENT_ACS_FACE_RECOGNIZE_FAIL

            EVENT_ACS_EMPLOYEENO_AND_PW_PASS

            EVENT_ACS_EMPLOYEENO_AND_PW_FAIL

            EVENT_ACS_EMPLOYEENO_AND_PW_TIMEOUT

            EVENT_ACS_HUMAN_DETECT_FAIL
        End Enum
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_XML_CONFIG_INPUT

            Public dwSize As UInteger

            'size of NET_DVR_XML_CONFIG_INPUT
            Public lpRequestUrl As IntPtr

            'command string
            Public dwRequestUrlLen As UInteger

            'command string length
            Public lpInBuffer As IntPtr

            'input buffer XML format
            Public dwInBufferSize As UInteger

            'input buffer length
            Public dwRecvTimeOut As UInteger

            'receive timeoutunitms0 represent 5s
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_XML_CONFIG_OUTPUT

            Public dwSize As UInteger

            'size of NET_DVR_XML_CONFIG_OUTPUT
            Public lpOutBuffer As IntPtr

            'output bufferXMLformat
            Public dwOutBufferSize As UInteger

            'input buffer length
            Public dwReturnedXMLSize As UInteger

            'the real receive Xml size
            Public lpStatusBuffer As IntPtr

            'return status(XML format),no assignment with success, If you don't care about it ,just set it NULL
            Public dwStatusSize As UInteger

            'status length(unit byte)
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_JSON_DATA_CFG

            Public dwSize As UInteger

            'size of NET_DVR_JSON_DATA_CFG
            Public lpJsonData As IntPtr

            'Json data
            Public dwJsonDataSize As UInteger

            'Json data size
            Public lpPicData As IntPtr

            'picture data
            Public dwPicDataSize As UInteger

            'picture data size
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=256)> _
            Public byRes() As Byte
        End Structure


        <StructLayoutAttribute(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
        Public Structure NET_DVR_LOG_V30

            Public strLogTime As NET_DVR_TIME

            Public dwMajorType As UInteger

            'Main type 1- alarm;  2- abnormal;  3- operation;  0xff- all 
            Public dwMinorType As UInteger

            'Hypo- Type 0- all; 
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_NAMELEN, ArraySubType:=UnmanagedType.I1)> _
            Public sPanelUser() As Byte

            'user ID for local panel operation
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_NAMELEN, ArraySubType:=UnmanagedType.I1)> _
            Public sNetUser() As Byte

            'user ID for network operation
            Public struRemoteHostAddr As NET_DVR_IPADDR

            'remote host IP
            Public dwParaType As UInteger

            'parameter type,  for 9000 series MINOR_START_VT/MINOR_STOP_VT,  channel of the voice talking
            Public dwChannel As UInteger

            'channel number
            Public dwDiskNumber As UInteger

            'HD number
            Public dwAlarmInPort As UInteger

            'alarm input port
            Public dwAlarmOutPort As UInteger

            'alarm output port
            Public dwInfoLen As UInteger

            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=LOG_INFO_LEN)> _
            Public sInfo As String
        End Structure
        '  ACS event informations
        Public Structure NET_DVR_ACS_EVENT_INFO

            Public dwSize As UInteger

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardNo() As Byte

            ' card No, 0 means invalid 
            Public byCardType As Byte

            ' card type,1-ordinary card,2-disable card,3-black list card, 4-patrol card,5-stress card,6-super card,7-client card, 0 means invalid
            Public byWhiteListNo As Byte

            ' white list No, 1-8, 0 means invalid
            Public byReportChannel As Byte

            ' report channel, 1-alarmin updata, 2-center group 1, 3-center group 2, 0 means invalid
            Public byCardReaderKind As Byte

            ' card reader type: 0-invalid, 1-IC card reader, 2-Id card reader, 3-Qr code reader, 4-Fingerprint head
            Public dwCardReaderNo As UInteger

            ' card reader No, 0 means invalid
            Public dwDoorNo As UInteger

            ' door No(floor No), 0 means invalid
            Public dwVerifyNo As UInteger

            ' mutilcard verify No. 0 means invalid
            Public dwAlarmInNo As UInteger

            ' alarm in No, 0 means invalid
            Public dwAlarmOutNo As UInteger

            ' alarm out No 0 means invalid
            Public dwCaseSensorNo As UInteger

            ' case sensor No 0 means invalid
            Public dwRs485No As UInteger

            ' RS485 channel,0 means invalid
            Public dwMultiCardGroupNo As UInteger

            ' multicard group No.
            Public wAccessChannel As System.UInt16

            ' Staff channel number 
            Public byDeviceNo As Byte

            ' device No,0 means invalid
            Public byDistractControlNo As Byte

            ' distract control,0 means invalid
            Public dwEmployeeNo As UInteger

            ' employee No,0 means invalid
            Public wLocalControllerID As System.UInt16

            ' On the controller number, 0 - access the host, 1-64 on behalf of the local controller 
            Public byInternetAccess As Byte

            ' Internet access ID (1-uplink network port 1, 2-uplink network port 2,3- downstream network interface 1
            Public byType As Byte

            ' protection zone type, 0-real time, 1-24 hours, 2-delay, 3-internal, 4-the key, 5-fire, 6-perimeter, 7-24 hours of silent
            ' 8-24 hours auxiliary, 9-24 hours vibration, 10-door emergency open, 11-door emergency shutdown, 0xff-null
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MACADDR_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byMACAddr() As Byte

            ' mac addr 0 means invalid
            Public bySwipeCardType As Byte

            ' swipe card type, 0-invalid,1-Qr code
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=13, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure
        ' Entrance guard alarm information structure
        Public Structure NET_DVR_ACS_ALARM_INFO

            Public dwSize As UInteger

            Public dwMajor As UInteger

            ' alarm major, reference to macro
            Public dwMinor As UInteger

            ' alarm minor, reference to macro
            Public struTime As NET_DVR_TIME

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_NAMELEN, ArraySubType:=UnmanagedType.I1)> _
            Public sNetUser() As Byte

            ' net operator user
            Public struRemoteHostAddr As NET_DVR_IPADDR

            ' remote host address
            Public struAcsEventInfo As NET_DVR_ACS_EVENT_INFO

            Public dwPicDataLen As UInteger

            ' picture length, when 0 ,means has no picture
            Public pPicData As IntPtr

            ' picture data
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=24, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure
        'Alarm Device Infor
        <StructLayoutAttribute(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
        Public Structure NET_DVR_ALARMER

            Public byUserIDValid As Byte

            Public bySerialValid As Byte

            Public byVersionValid As Byte

            Public byDeviceNameValid As Byte

            Public byMacAddrValid As Byte

            Public byLinkPortValid As Byte

            Public byDeviceIPValid As Byte

            Public bySocketIPValid As Byte

            Public lUserID As Integer

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=SERIALNO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public sSerialNumber() As Byte

            Public dwDeviceVersion As UInteger

            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=NAME_LEN)> _
            Public sDeviceName As String

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MACADDR_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byMacAddr() As Byte

            Public wLinkPort As System.UInt16

            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=128)> _
            Public sDeviceIP As String

            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=128)> _
            Public sSocketIP As String

            Public byIpProtocol As Byte

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=11, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte
        End Structure
        'Alarm protection structure parameters
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_SETUPALARM_PARAM

            Public dwSize As UInteger

            Public byLevel As Byte

            'Arming priority: 0-high, 1-middle, 2-low
            Public byAlarmInfoType As Byte

            'Upload alarm information typesIntelligent traffic camera support    0- oldNET_DVR_PLATE_RESULT    1- new(NET_ITS_PLATE_RESULT)
            Public byRetAlarmTypeV40 As Byte

            '0- Ret NET_DVR_ALARMINFO_V30 or Older, 1- if Device Support NET_DVR_ALARMINFO_V40,  Ret NET_DVR_ALARMINFO_V40, else Ret NET_DVR_ALARMINFO_V30 Or NET_DVR_ALARMINFO
            Public byRetDevInfoVersion As Byte

            'CVR alarm 0-COMM_ALARM_DEVICE, 1-COMM_ALARM_DEVICE_V40
            Public byRetVQDAlarmType As Byte

            'Exptected VQD alarm type,0-upload NET_DVR_VQD_DIAGNOSE_INFO,1-upload NET_DVR_VQD_ALARM
            '1-(INTER_FACE_DETECTION),0-(INTER_FACESNAP_RESULT)
            Public byFaceAlarmDetection As Byte

            'Bit0 - indicates whether the secondary protection to upload pictures: 0 - upload, 1 - do not upload 
            'Bit1 - said open data upload confirmation mechanism; 0 - don't open, 1 - to open
            Public bySupport As Byte

            'broken Net Http 
            'bit0-Vehicle Detection(IPC) (0 - not continuingly, 1 - continuingly)
            'bit1-PDC(IPC)  (0 - not continuingly, 1 - continuingly)
            'bit2-HeatMap(IPC)  (0 - not continuingly, 1 - continuingly)
            Public byBrokenNetHttp As Byte

            Public wTaskNo As System.UInt16

            'Tasking number and the (field dwTaskNo corresponding data upload NET_DVR_VEHICLE_RECOG_RESULT the same time issued a task structure NET_DVR_VEHICLE_RECOG_COND corresponding fields in dwTaskNo
            Public byDeployType As Byte

            'deploy type:0-client deploy,1-real time deploy
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            Public byAlarmTypeURL As Byte

            'bit0-(NET_DVR_FACESNAP_RESULT),0-binary,1-URL
            Public byCustomCtrl As Byte
        End Structure


        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_VALID_PERIOD_CFG

            Public byEnable As Byte

            'whether to enable , 0-disable 1-enable
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            Public struBeginTime As NET_DVR_TIME_EX

            'valid begin time
            Public struEndTime As NET_DVR_TIME_EX

            'valid end time 
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_CARD_CFG_V50

            Public dwSize As UInteger

            Public dwModifyParamType As UInteger

            ' the card parameter need to modify, valid when set card parameter, use by bit, every bit means a kind of parameter, 1 means modify, 0 means not 
            ' #define CARD_PARAM_CARD_VALID       0x00000001 //card valid parameter 
            ' #define CARD_PARAM_VALID            0x00000002  //valid period parameter
            ' #define CARD_PARAM_CARD_TYPE        0x00000004  //card type parameter
            ' #define CARD_PARAM_DOOR_RIGHT       0x00000008  //door right parameter
            ' #define CARD_PARAM_LEADER_CARD      0x00000010  //leader card parameter
            ' #define CARD_PARAM_SWIPE_NUM        0x00000020  //max swipe time parameter
            ' #define CARD_PARAM_GROUP            0x00000040  //belong group parameter
            ' #define CARD_PARAM_PASSWORD         0x00000080  //card password parameter
            ' #define CARD_PARAM_RIGHT_PLAN       0x00000100  //card right plan parameter
            ' #define CARD_PARAM_SWIPED_NUM       0x00000200  //has swiped card times parameter
            ' #define CARD_PARAM_EMPLOYEE_NO      0x00000400  //employee no
            ' #define CARD_PARAM_NAME             0x00000800  //name
            ' #define CARD_PARAM_DEPARTMENT_NO    0x00001000  //department no
            ' #define CARD_SCHEDULE_PLAN_NO       0x00002000  //schedule plan no
            ' #define CARD_SCHEDULE_PLAN_TYPE     0x00004000  //schedule plan type
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardNo() As Byte

            'card No
            Public byCardValid As Byte

            'whether is a valid card,0-invalid,1-valid(use to delete card, 0 means delete card when setting, it will be 1 when getting)
            Public byCardType As Byte

            'card type ,1-ordinary card,2-disabled card,3-black list card, 4-patrol card,5-stress card,6-super card,7-guest card,8-remove card, 9-employee card,10-emergency card,11-emergency management card,default ordinary card 
            Public byLeaderCard As Byte

            'whether is leader card, 0-no, 1-yes
            Public byRes1 As Byte

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)> _
            Public byDoorRight() As Byte

            'door right (floor right), accord to bit, 1-has right 0-no right, from low bit to high bit means door 1-N have right
            Public struValid As NET_DVR_VALID_PERIOD_CFG

            'valid period parameter
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_GROUP_NUM_128, ArraySubType:=UnmanagedType.I1)> _
            Public byBelongGroup() As Byte

            'Subordinate to the group, in public bytes, 1 - belongs to, 0 - does not belong to 
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=CARD_PASSWORD_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardPassword() As Byte

            'card password 
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=(MAX_DOOR_NUM_256 * MAX_CARD_RIGHT_PLAN_NUM), ArraySubType:=UnmanagedType.I1)> _
            Public wCardRightPlan() As System.UInt16

            'card right plan, value is from plan template No. use or method when same door has different plan template
            Public dwMaxSwipeTime As UInteger

            'max card time, 0 means infinite time
            Public dwSwipeTime As UInteger

            'has swiped card
            Public wRoomNumber As System.UInt16

            'room number
            Public wFloorNumber As Short

            'floor number
            Public dwEmployeeNo As UInteger

            'employee no
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byName() As Byte

            'name
            Public wDepartmentNo As System.UInt16

            'department no
            Public wSchedulePlanNo As System.UInt16

            'schedule plan no
            Public bySchedulePlanType As Byte

            'schedule plan type:0-no mean,1-personal,2-department
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte

            Public dwLockID As UInteger

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_LOCK_CODE_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byLockCode() As Byte

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_CODE_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byRoomCode() As Byte

            'bit0-no1-yes
            'bit0low voltage alarm
            'bit1open door with prompt tone
            'bit2limit customer card 
            'bit3channel
            'bit4open locked door
            'bit5patrol function
            Public dwCardRight As UInteger

            Public dwPlanTemplate As UInteger

            Public dwCardUserId As UInteger

            Public byCardModelType As Byte

            ' 0-NULL,1-MIFARE,2-S50MIFARE,3-S70FM1208,4-CPUFM1216,5-CPUGMB Algorithm CPU,6-ID Card,7-NFC
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=83, ArraySubType:=UnmanagedType.I1)> _
            Public byRes3() As Byte

            Public Sub Init()
                Me.byDoorRight = New Byte((CHCNetSDK.MAX_DOOR_NUM_256) - 1) {}
                Me.byBelongGroup = New Byte((CHCNetSDK.MAX_GROUP_NUM_128) - 1) {}
                Me.wCardRightPlan = New System.UInt16(((CHCNetSDK.MAX_DOOR_NUM_256 * CHCNetSDK.MAX_CARD_RIGHT_PLAN_NUM)) - 1) {}
                Me.byCardNo = New Byte((CHCNetSDK.ACS_CARD_NO_LEN) - 1) {}
                Me.byCardPassword = New Byte((CHCNetSDK.CARD_PASSWORD_LEN) - 1) {}
                Me.byName = New Byte((CHCNetSDK.NAME_LEN) - 1) {}
                Me.byRes2 = New Byte((3) - 1) {}
                Me.byLockCode = New Byte((CHCNetSDK.MAX_LOCK_CODE_LEN) - 1) {}
                Me.byRoomCode = New Byte((CHCNetSDK.MAX_DOOR_CODE_LEN) - 1) {}
                Me.byRes3 = New Byte((83) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_CARD_CFG_COND

            Public dwSize As UInteger

            Public dwCardNum As UInteger

            'card number, 0xffffffff means to get all card information when getting
            Public byCheckCardNo As Byte

            'whether to verify card No. 0-not to verify, 1-verify
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            Public wLocalControllerID As System.UInt16

            'On-site controller serial number, said to the local controller issued offline card parameters, 0 is access control host 
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte

            Public dwLockID As UInteger

            'lock ID
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=20, ArraySubType:=UnmanagedType.I1)> _
            Public byRes3() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_CARD_CFG_SEND_DATA

            Public dwSize As UInteger

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardNo() As Byte

            'card No 
            Public dwCardUserId As UInteger

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=12, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FACE_PARAM_COND

            Public dwSize As UInteger

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardNo() As Byte

            'card No
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)> _
            Public byEnableCardReader() As Byte

            'enable card reader:0-invalid,1-valid
            Public dwFaceNum As UInteger

            'face number
            Public byFaceID As Byte

            'face id:1-2,0xff present this card all face
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=127, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FACE_PARAM_CFG

            Public dwSize As UInteger

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardNo() As Byte

            'card No
            Public dwFaceLen As UInteger

            'face length
            Public pFaceBuffer As IntPtr

            'face buffer
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)> _
            Public byEnableCardReader() As Byte

            'enable card reader:0-invalid,1-valid
            Public byFaceID As Byte

            'face id:1-2,0xff present this card all face
            Public byFaceDataType As Byte

            'face data type:0-module(default),1-picture
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=126, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FACE_PARAM_STATUS

            Public dwSize As UInteger

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardNo() As Byte

            'card No
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)> _
            Public byCardReaderRecvStatus() As Byte

            'card reader receive status:0-fail,1-success,2-face of poor quality,3-memory full,4-face already exist,5-illegal face ID
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ERROR_MSG_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byErrorMsg() As Byte

            'error message:when byCardReaderRecvStatus is 4,present face already exist correspond card number
            Public dwCardReaderNo As UInteger

            'card reader No
            Public byTotalStatus As Byte

            'total status:0-not set all card readers face,1-set all card readers face
            Public byFaceID As Byte

            'face id:1-2
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=130, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_ACS_PARAM_TYPE

            Public dwSize As UInteger

            Public dwParamType As UInteger

            'parameter type,bitwise representation 
            '#define ACS_PARAM_DOOR_STATUS_WEEK_PLAN        0x00000001 //door status week plan
            '#define ACS_PARAM_VERIFY_WEEK_PALN             0x00000002 //card reader week plan
            '#define ACS_PARAM_CARD_RIGHT_WEEK_PLAN         0x00000004 //card right week plan
            '#define ACS_PARAM_DOOR_STATUS_HOLIDAY_PLAN     0x00000008 //door status holiday plan 
            '#define ACS_PARAM_VERIFY_HOLIDAY_PALN          0x00000010 //card reader holiday plan
            '#define ACS_PARAM_CARD_RIGHT_HOLIDAY_PLAN      0x00000020 //card right holiday plan
            '#define ACS_PARAM_DOOR_STATUS_HOLIDAY_GROUP    0x00000040 //door status holiday group plan
            '#define ACS_PARAM_VERIFY_HOLIDAY_GROUP         0x00000080 //card reader verify  holiday group plan
            '#define ACS_PARAM_CARD_RIGHT_HOLIDAY_GROUP     0x00000100 //card right holiday group plan
            '#define ACS_PARAM_DOOR_STATUS_PLAN_TEMPLATE    0x00000200 // door status plan template 
            '#define ACS_PARAM_VERIFY_PALN_TEMPLATE         0x00000400 //card reader verify plan template 
            '#define ACS_PARAM_CARD_RIGHT_PALN_TEMPLATE     0x00000800 //card right plan template 
            '#define ACS_PARAM_CARD                         0x00001000 //card configure
            '#define ACS_PARAM_GROUP                        0x00002000 //group configure
            '#define ACS_PARAM_ANTI_SNEAK_CFG               0x00004000 //anti-sneak configure
            '#define ACS_PAPAM_EVENT_CARD_LINKAGE           0x00008000 //event linkage card
            '#define ACS_PAPAM_CARD_PASSWD_CFG              0x00010000 //open door by password 
            Public wLocalControllerID As System.UInt16

            'On-site controller serial number[1,64],0 represent guard host
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=30)> _
            Public byRes() As Byte
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_DOOR_CFG

            Public dwSize As UInteger

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=DOOR_NAME_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byDoorName() As Byte

            'door name
            Public byMagneticType As Byte

            'magnetic type, 0-always close 1-always open
            Public byOpenButtonType As Byte

            'open button type,  0-always close 1-always open
            Public byOpenDuration As Byte

            'open duration time, 1-255s(ladder control relay action time)
            Public byDisabledOpenDuration As Byte

            'disable open duration , 1-255s  
            Public byMagneticAlarmTimeout As Byte

            'magnetic alarm time out , 0-255s,0 means not to alarm
            Public byEnableDoorLock As Byte

            'whether to enable door lock, 0-disable, 1-enable
            Public byEnableLeaderCard As Byte

            'whether to enable leader card , 0-disable, 1-enable
            Public byLeaderCardMode As Byte

            'First card mode, 0 - first card function is not enabled, and 1 - the first card normally open mode, 2 - the first card authorization mode (using this field, the byEnableLeaderCard is invalid ) 
            Public dwLeaderCardOpenDuration As UInteger

            'leader card open duration 1-1440min
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=STRESS_PASSWORD_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byStressPassword() As Byte

            'stress ppassword
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=SUPER_PASSWORD_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public bySuperPassword() As Byte

            'super password
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=UNLOCK_PASSWORD_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byUnlockPassword() As Byte

            Public byUseLocalController As Byte

            'Read-only, whether the connection on the local controller, 0 - no, 1 - yes
            Public byRes1 As Byte

            Public wLocalControllerID As System.UInt16

            'Read-only, on-site controller serial number, 1-64, 0 on behalf of unregistered 
            Public wLocalControllerDoorNumber As System.UInt16

            'Read-only, on-site controller door number, 1-4, 0 represents the unregistered 
            Public wLocalControllerStatus As System.UInt16

            'Read-only, on-site controller online status: 0 - offline, 1 - online, 2 - loop of RS485 serial port 1 on 1, 3 - loop of RS485 serial port 2 on 2, 4 - loop of RS485 serial port 1, 5 - loop of RS485 serial port 2, 6 - loop 3 of RS485 serial port 1, 7 - the loop on the RS485 serial port on the 3 4 2, 8 - loop on the RS485 serial port 1, 9 - loop 4 of RS485 serial port 2 (read-only) 
            Public byLockInputCheck As Byte

            'Whether to enable the door input detection (1 public byte, 0 is not enabled, 1 is enabled, is not enabled by default) 
            Public byLockInputType As Byte

            'Door lock input type 
            Public byDoorTerminalMode As Byte

            'Gate terminal working mode 
            Public byOpenButton As Byte

            'Whether to enable the open button 
            Public byLadderControlDelayTime As Byte

            'ladder control delay time,1-255min
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=43, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte

            Public Sub Init()
                Me.byDoorName = New Byte((DOOR_NAME_LEN) - 1) {}
                Me.byStressPassword = New Byte((STRESS_PASSWORD_LEN) - 1) {}
                Me.bySuperPassword = New Byte((SUPER_PASSWORD_LEN) - 1) {}
                Me.byUnlockPassword = New Byte((UNLOCK_PASSWORD_LEN) - 1) {}
                Me.byRes2 = New Byte((43) - 1) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_GROUP_CFG

            Public dwSize As UInteger

            Public byEnable As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            Public struValidPeriodCfg As NET_DVR_VALID_PERIOD_CFG

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=GROUP_NAME_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byGroupName() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_ALARM_DEVICE_USER

            Public dwSize As UInteger

            'Structure size
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public sUserName() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=PASSWD_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public sPassword() As Byte

            Public struUserIP As NET_DVR_IPADDR

            'User IP (0 stands for no IP restriction)
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MACADDR_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byAMCAddr() As Byte

            Public byUserType As Byte

            '0- general user, 1- administrator user
            Public byAlarmOnRight As Byte

            'Arming authority
            Public byAlarmOffRight As Byte

            'Disarming authority
            Public byBypassRight As Byte

            'Bypass authority
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_RIGHT, ArraySubType:=UnmanagedType.I1)> _
            Public byOtherRight() As Byte

            'Other authority 
            ' 0 -- log
            ' 1 -- reboot/shutdown
            ' 2 -- set parameter
            ' 3 -- get parameter
            ' 4 -- resume
            ' 5 -- siren 
            ' 6 -- PTZ
            ' 7 -- remote upgrade
            ' 8 -- preview
            ' 9 -- manual record
            ' 10 --remote playback
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=(MAX_ALARMHOST_VIDEO_CHAN / 8), ArraySubType:=UnmanagedType.I1)> _
            Public byNetPreviewRight() As Byte

            ' preview channels,eg. bit0-channel 1,0-no permission 1-permission enable
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=(MAX_ALARMHOST_VIDEO_CHAN / 8), ArraySubType:=UnmanagedType.I1)> _
            Public byNetRecordRight() As Byte

            ' record channels,eg. bit0-channel 1,0-no permission 1-permission enable
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=(MAX_ALARMHOST_VIDEO_CHAN / 8), ArraySubType:=UnmanagedType.I1)> _
            Public byNetPlaybackRight() As Byte

            ' playback channels,eg. bit0-channel 1,0-no permission 1-permission enable
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=(MAX_ALARMHOST_VIDEO_CHAN / 8), ArraySubType:=UnmanagedType.I1)> _
            Public byNetPTZRight() As Byte

            ' PTZ channels,eg. bit0-channel 1,0-no permission 1-permission enable
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=PASSWD_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public sOriginalPassword() As Byte

            ' Original password
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=152, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_CARD_READER_CFG_V50

            Public dwSize As UInteger

            Public byEnable As Byte

            'whether to enable, 1-enable, 0-disable
            Public byCardReaderType As Byte

            'card reader type,1-DS-K110XM/MK/C/CK,2-DS-K192AM/AMP,3-DS-K192BM/BMP,4-DS-K182AM/AMP,5-DS-K182BM/BMP,6-DS-K182AMF/ACF,7-wiegand or 485 not online,8- DS-K1101M/MK,9- DS-K1101C/CK,10- DS-K1102M/MK/M-A
            '11- DS-K1102C/CK,12- DS-K1103M/MK,13- DS-K1103C/CK,14- DS-K1104M/MK,15- DS-K1104C/CK,16- DS-K1102S/SK/S-A,17- DS-K1102G/GK,18- DS-K1100S-B,19- DS-K1102EM/EMK,20- DS-K1102E/EK,
            '21- DS-K1200EF,22- DS-K1200MF,23- DS-K1200CF,24- DS-K1300EF,25- DS-K1300MF,26- DS-K1300CF,27- DS-K1105E,28- DS-K1105M,29- DS-K1105C,30- DS-K182AMF,31- DS-K196AMF,32-DS-K194AMP
            '33-DS-K1T200EF/EF-C/MF/MF-C/CF/CF-C,34-DS-K1T300EF/EF-C/MF/MF-C/CF/CF-C,35-DS-K1T105E/E-C/M/M-C/C/C-C,36-DS-K1T803F/MF/SF/EF,37-DS-K1A801F/MF/SF/EF,38-DS-K1107M/MK,39-DS-K1107E/EK,
            '40-DS-K1107S/SK,41-DS-K1108M/MK,42-DS-K1108E/EK,43-DS-K1108S/SK,44-DS-K1200F,45-DS-K1S110-I,46-DS-K1T200M-PG/PGC,47-DS-K1T200M-PZ/PZC,48-DS-K1109H
            Public byOkLedPolarity As Byte

            'OK LED polarity,0-negative,1-positive
            Public byErrorLedPolarity As Byte

            'Error LED polarity,0-negative,1-positive
            Public byBuzzerPolarity As Byte

            'buzzer polarity,0-negative,1-positive
            Public bySwipeInterval As Byte

            'swipe interval, unit: second
            Public byPressTimeout As Byte

            'press time out, unit:second
            Public byEnableFailAlarm As Byte

            'whether to enable fail alarm, 0-disable 1-enable
            Public byMaxReadCardFailNum As Byte

            'max reader card fail time
            Public byEnableTamperCheck As Byte

            'whether to support tamper check, 0-disable ,1-enable
            Public byOfflineCheckTime As Byte

            'offline check time, Uint second
            Public byFingerPrintCheckLevel As Byte

            'fingerprint check lever,1-1/10,2-1/100,3-1/1000,4-1/10000,5-1/100000,6-1/1000000,7-1/10000000,8-1/100000000,9-3/100,10-3/1000,11-3/10000,12-3/100000,13-3/1000000,14-3/10000000,15-3/100000000,16-Automatic Normal,17-Automatic Secure,18-Automatic More Secure
            Public byUseLocalController As Byte

            'read only,weather connect with local control:0-no,1-yes
            Public byRes1 As Byte

            Public wLocalControllerID As System.UInt16

            'read only,local controller ID, byUseLocalController=1 effective,1-64,0 present not register
            Public wLocalControllerReaderID As System.UInt16

            'read only,local controller reader ID,byUseLocalController=1 effective,0 present not register
            Public wCardReaderChannel As System.UInt16

            'read only,card reader channel,byUseLocalController=1 effective,0-wiegand or offline,1-RS485A,2-RS485B
            Public byFingerPrintImageQuality As Byte

            'finger print image quality,0-no effective,1-weak qualification(V1),2-moderate qualification(V1),3-strong qualification(V1),4-strongest qualification(V1),5-weak qualification(V2),6-moderate qualification(V2),7-strong qualification(V2),8-strongest qualification(V2)
            Public byFingerPrintContrastTimeOut As Byte

            'finger print contrast time out,0-no effective,1-20 present:1s-20s,0xff-infinite
            Public byFingerPrintRecogizeInterval As Byte

            'finger print recognize interval,0-no effective,1-10 present:1s-10s,0xff-no delay
            Public byFingerPrintMatchFastMode As Byte

            'finger print match fast mode,0-no effective,1-5 present:fast mode 1-fast mode 5,0xff-auto
            Public byFingerPrintModuleSensitive As Byte

            'finger print module sensitive,0-no effective,1-8 present:sensitive level 1-sensitive level 8
            Public byFingerPrintModuleLightCondition As Byte

            'finger print module light condition,0-no effective,1-out door,2-in door
            Public byFaceMatchThresholdN As Byte

            'range 0-100
            Public byFaceQuality As Byte

            'face quality,range 0-100
            Public byFaceRecogizeTimeOut As Byte

            'face recognize time out,1-20 present:1s-20s,0xff-infinite
            Public byFaceRecogizeInterval As Byte

            'face recognize interval,0-no effective,1-10 present:1s-10s,0xff-no delay
            Public wCardReaderFunction As System.UInt16

            'read only,card reader function
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CARD_READER_DESCRIPTION, ArraySubType:=UnmanagedType.I1)> _
            Public byCardReaderDescription() As Byte

            'read only,card reader description
            Public wFaceImageSensitometry As System.UInt16

            'face image sensitometry,range 0-65535
            Public byLivingBodyDetect As Byte

            'living body detect,0-no effective,1-disable,2-enable
            Public byFaceMatchThreshold1 As Byte

            'range 0-100
            Public wBuzzerTime As System.UInt16

            'buzzer time,range 0-5999(s) 0 present yowl
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=254, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byCardReaderDescription = New Byte((CARD_READER_DESCRIPTION) - 1) {}
                Me.byRes = New Byte((254) - 1) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FINGER_PRINT_CFG

            Public dwSize As UInteger

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardNo() As Byte

            'card NO
            Public dwFingerPrintLen As UInteger

            'fingerprint len
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)> _
            Public byEnableCardReader() As Byte

            'the card reader which finger print send to,according to the values,0-not send,1-send
            Public byFingerPrintID As Byte

            'finger print ID,[1,10]
            Public byFingerType As Byte

            'finger type  0-normal,1-stress
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=30, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_FINGER_PRINT_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byFingerData() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byCardNo = New Byte((ACS_CARD_NO_LEN) - 1) {}
                Me.byEnableCardReader = New Byte((MAX_CARD_READER_NUM_512) - 1) {}
                Me.byRes1 = New Byte((30) - 1) {}
                Me.byFingerData = New Byte((MAX_FINGER_PRINT_LEN) - 1) {}
                Me.byRes = New Byte((64) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FINGER_PRINT_STATUS

            Public dwSize As UInteger

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardNo() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)> _
            Public byCardReaderRecvStatus() As Byte

            'Fingerprint reader state, press the public bytes, 0 - failure, 1 -, 2 - the fingerprint module is not online, 3 - try again or poor quality of fingerprint, 4 - memory is full, 5 - existing the fingerprints, 6 - existing the fingerprint ID, illegal fingerprint ID, 7-8 - don't need to configure the fingerprint module 
            Public byFingerPrintID As Byte

            'finger print ID,[1,10]
            Public byFingerType As Byte

            'finger type  0-normal,1-stress
            Public byTotalStatus As Byte

            '
            Public byRes1 As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ERROR_MSG_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byErrorMsg() As Byte

            'Issued false information, when the byCardReaderRecvStatus is 5, said existing fingerprint matching card number 
            Public dwCardReaderNo As UInteger

            'Grain number card reader, can be used to return issued by mistake
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=24, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byCardNo = New Byte((ACS_CARD_NO_LEN) - 1) {}
                Me.byCardReaderRecvStatus = New Byte((MAX_CARD_READER_NUM_512) - 1) {}
                Me.byErrorMsg = New Byte((ERROR_MSG_LEN) - 1) {}
                Me.byRes = New Byte((24) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FINGER_PRINT_INFO_COND

            Public dwSize As UInteger

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardNo() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)> _
            Public byEnableCardReader() As Byte

            'which card reader to send,according to the values
            Public dwFingerPrintNum As UInteger

            'the number send or get. if get,0xffffffff means all
            Public byFingerPrintID As Byte

            'finger print ID,[1,10],   0xff means all
            Public byCallbackMode As Byte

            '
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=26, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            Public Sub Init()
                Me.byCardNo = New Byte((ACS_CARD_NO_LEN) - 1) {}
                Me.byEnableCardReader = New Byte((MAX_CARD_READER_NUM_512) - 1) {}
                Me.byRes1 = New Byte((26) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FINGER_PRINT_CFG_V50

            Public dwSize As UInteger

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardNo() As Byte

            'card NO
            Public dwFingerPrintLen As UInteger

            'fingerprint len
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)> _
            Public byEnableCardReader() As Byte

            'the card reader which finger print send to,according to the values,0-not send,1-send
            Public byFingerPrintID As Byte

            'finger print ID,[1,10]
            Public byFingerType As Byte

            'finger type  0-normal,1-stress
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=30, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_FINGER_PRINT_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byFingerData() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NET_SDK_EMPLOYEE_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byEmployeeNo() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)> _
            Public byLeaderFP() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=128, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byCardNo = New Byte((ACS_CARD_NO_LEN) - 1) {}
                Me.byEnableCardReader = New Byte((MAX_CARD_READER_NUM_512) - 1) {}
                Me.byRes1 = New Byte((30) - 1) {}
                Me.byFingerData = New Byte((MAX_FINGER_PRINT_LEN) - 1) {}
                Me.byEmployeeNo = New Byte((NET_SDK_EMPLOYEE_NO_LEN) - 1) {}
                Me.byLeaderFP = New Byte((MAX_DOOR_NUM_256) - 1) {}
                Me.byRes = New Byte((128) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FINGER_PRINT_STATUS_V50

            Public dwSize As UInteger

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardNo() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)> _
            Public byCardReaderRecvStatus() As Byte

            'Fingerprint reader state, press the public bytes, 0 - failure, 1 -, 2 - the fingerprint module is not online, 3 - try again or poor quality of fingerprint, 4 - memory is full, 5 - existing the fingerprints, 6 - existing the fingerprint ID, illegal fingerprint ID, 7-8 - don't need to configure the fingerprint module 
            Public byFingerPrintID As Byte

            'finger print ID,[1,10]
            Public byFingerType As Byte

            'finger type  0-normal,1-stress
            Public byTotalStatus As Byte

            '
            Public byRecvStatus As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ERROR_MSG_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byErrorMsg() As Byte

            'Issued false information, when the byCardReaderRecvStatus is 5, said existing fingerprint matching card number 
            Public dwCardReaderNo As UInteger

            'Grain number card reader, can be used to return issued by mistake
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NET_SDK_EMPLOYEE_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byEmployeeNo() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NET_SDK_EMPLOYEE_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byErrorEmployeeNo() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=128, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byCardNo = New Byte((ACS_CARD_NO_LEN) - 1) {}
                Me.byCardReaderRecvStatus = New Byte((MAX_CARD_READER_NUM_512) - 1) {}
                Me.byErrorMsg = New Byte((ERROR_MSG_LEN) - 1) {}
                Me.byEmployeeNo = New Byte((NET_SDK_EMPLOYEE_NO_LEN) - 1) {}
                Me.byErrorEmployeeNo = New Byte((NET_SDK_EMPLOYEE_NO_LEN) - 1) {}
                Me.byRes = New Byte((128) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FINGER_PRINT_INFO_COND_V50

            Public dwSize As UInteger

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardNo() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)> _
            Public byEnableCardReader() As Byte

            'which card reader to send,according to the values
            Public dwFingerPrintNum As UInteger

            'the number send or get. if get,0xffffffff means all
            Public byFingerPrintID As Byte

            'finger print ID,[1,10],   0xff means all
            Public byCallbackMode As Byte

            '
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NET_SDK_EMPLOYEE_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byEmployeeNo() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=128, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            Public Sub Init()
                Me.byCardNo = New Byte((ACS_CARD_NO_LEN) - 1) {}
                Me.byEnableCardReader = New Byte((MAX_CARD_READER_NUM_512) - 1) {}
                Me.byRes2 = New Byte((2) - 1) {}
                Me.byEmployeeNo = New Byte((NET_SDK_EMPLOYEE_NO_LEN) - 1) {}
                Me.byRes1 = New Byte((128) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FINGER_PRINT_BYCARD

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardNo() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)> _
            Public byEnableCardReader() As Byte

            'be enable card reader,according to the values
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_FINGER_PRINT_NUM, ArraySubType:=UnmanagedType.I1)> _
            Public byFingerPrintID() As Byte

            'finger print ID,according to the values,0-not delete,1-delete
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=34, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FINGER_PRINT_BYREADER

            Public dwCardReaderNo As UInteger

            Public byClearAllCard As Byte

            'clear all card,0-delete by card,1-delete all card
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardNo() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=548, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FINGER_PRINT_INFO_CTRL_BYCARD

            Public dwSize As UInteger

            Public byMode As Byte

            'delete mode,0-delete by card,1-delete by reader
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)> _
            Public byRes1() As Byte

            Public struByCard As NET_DVR_FINGER_PRINT_BYCARD

            'delete by card
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FINGER_PRINT_INFO_CTRL_BYREADER

            Public dwSize As UInteger

            Public byMode As Byte

            'delete mode,0-delete by card,1-delete by reader
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)> _
            Public byRes1() As Byte

            Public struByReader As NET_DVR_FINGER_PRINT_BYREADER

            'delete by reader
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FINGER_PRINT_BYCARD_V50

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardNo() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)> _
            Public byEnableCardReader() As Byte

            'be enable card reader,according to the values
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_FINGER_PRINT_NUM, ArraySubType:=UnmanagedType.I1)> _
            Public byFingerPrintID() As Byte

            'finger print ID,according to the values,0-not delete,1-delete
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NET_SDK_EMPLOYEE_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byEmployeeNo() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FINGER_PRINT_BYREADER_V50

            Public dwCardReaderNo As UInteger

            Public byClearAllCard As Byte

            'clear all card,0-delete by card,1-delete all card
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardNo() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NET_SDK_EMPLOYEE_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byEmployeeNo() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=516, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FINGER_PRINT_INFO_CTRL_BYCARD_V50

            Public dwSize As UInteger

            Public byMode As Byte

            'delete mode,0-delete by card,1-delete by reader
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)> _
            Public byRes1() As Byte

            Public struByCard As NET_DVR_FINGER_PRINT_BYCARD_V50

            'delete by card
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FINGER_PRINT_INFO_CTRL_BYREADER_V50

            Public dwSize As UInteger

            Public byMode As Byte

            'delete mode,0-delete by card,1-delete by reader
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)> _
            Public byRes1() As Byte

            Public struByReader As NET_DVR_FINGER_PRINT_BYREADER_V50

            'delete by reader
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_FINGER_PRINT_INFO_STATUS_V50

            Public dwSize As UInteger

            Public dwCardReaderNo As UInteger

            'card reader no
            Public byStatus As Byte

            'status:0-invalid,1-processing,2-failed,3-success
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=63)> _
            Public byRes() As Byte
        End Structure
        '[StructLayoutAttribute(LayoutKind.Sequential)]
        'public const int DEL_FINGER_PRINT_MODE_LEN = 588; //TS'
        'public union NET_DVR_DEL_FINGER_PRINT_MODE
        '{
        '    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 588, ArraySubType = UnmanagedType.I1)]
        '    public byte[] uLen;
        '    public NET_DVR_FINGER_PRINT_BYCARD struByCard;     //delete by card
        '    public NET_DVR_FINGER_PRINT_BYREADER struByReader;   //delete by reader
        '}
        '[StructLayoutAttribute(LayoutKind.Sequential)]
        'public struct NET_DVR_FINGER_PRINT_INFO_CTRL
        '{
        '    public uint dwSize;
        '    public byte byMode;          //delete mode,0-delete by card,1-delete by reader
        '    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3, ArraySubType = UnmanagedType.I1)]
        '    public byte[] byRes1;
        '    public NET_DVR_DEL_FINGER_PRINT_MODE struProcessMode;  //delete mode
        '    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64, ArraySubType = UnmanagedType.I1)]
        '    public byte[] byRes;
        '}

        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_TIME_SEGMENT

            Public struBeginTime As NET_DVR_SIMPLE_DAYTIME

            'begin time
            Public struEndTime As NET_DVR_SIMPLE_DAYTIME
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_SINGLE_PLAN_SEGMENT

            Public byEnable As Byte

            'whether to enable, 1-enable, 0-disable
            Public byDoorStatus As Byte

            'door status(control ladder status),0-invaild, 1-always open(free), 2-always close(forbidden), 3-ordinary status(used by door plan)
            Public byVerifyMode As Byte

            'verify method, 0-invaild, 1-swipe card, 2-swipe card +password(used by card verify ) 3-swipe card(used by card verify) 4-swipe card or password(used by card verify)
            '5-fingerprint, 6-fingerprint and passwd, 7-fingerprint or swipe card, 8-fingerprint and swipe card, 9-fingerprint and passwd and swipe card,
            '10-face or finger print or swipe card or password,11-face and finger print,12-face and password,13-face and swipe card,14-face,15-employee no and password,
            '16-finger print or password,17-employee no and finger print,18-employee no and finger print and password,
            '19-face and finger print and swipe card,20-face and password and finger print,21-employee no and face,22-face or face and swipe card
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=5, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public struTimeSegment As NET_DVR_TIME_SEGMENT

            'time segment parameter 
            Public Sub Init()
                Me.byRes = New Byte((5) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_WEEK_PLAN_CFG

            Public dwSize As UInteger

            Public byEnable As Byte

            'whether to enable, 1-enable, 0-disable
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=(MAX_DAYS * MAX_TIMESEGMENT_V30), ArraySubType:=UnmanagedType.Struct)> _
            Public struPlanCfg() As NET_DVR_SINGLE_PLAN_SEGMENT

            'week plan parameter
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte

            Public Sub Init()
                Me.struPlanCfg = New NET_DVR_SINGLE_PLAN_SEGMENT(((MAX_DAYS * MAX_TIMESEGMENT_V30)) - 1) {}
                For Each singlStruPlanCfg As NET_DVR_SINGLE_PLAN_SEGMENT In Me.struPlanCfg
                    singlStruPlanCfg.Init()
                Next
                Me.byRes1 = New Byte((3) - 1) {}
                Me.byRes2 = New Byte((16) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_HOLIDAY_PLAN_CFG

            Public dwSize As UInteger

            Public byEnable As Byte

            'whether to enable, 1-enable, 0-disable
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            Public struBeginDate As NET_DVR_DATE

            'holiday begin date
            Public struEndDate As NET_DVR_DATE

            'holiday end date
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_TIMESEGMENT_V30, ArraySubType:=UnmanagedType.Struct)> _
            Public struPlanCfg() As NET_DVR_SINGLE_PLAN_SEGMENT

            'time segment parameter 
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
            Public byRes2() As Byte

            Public Sub Init()
                Me.struPlanCfg = New NET_DVR_SINGLE_PLAN_SEGMENT((MAX_TIMESEGMENT_V30) - 1) {}
                For Each singlStruPlanCfg As NET_DVR_SINGLE_PLAN_SEGMENT In Me.struPlanCfg
                    singlStruPlanCfg.Init()
                Next
                Me.byRes1 = New Byte((3) - 1) {}
                Me.byRes2 = New Byte((16) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_HOLIDAY_GROUP_CFG

            Public dwSize As UInteger

            Public byEnable As Byte

            'whether to enable, 1-enable, 0-disable 
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=HOLIDAY_GROUP_NAME_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byGroupName() As Byte

            'holiday group name 
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_HOLIDAY_PLAN_NUM, ArraySubType:=UnmanagedType.U4)> _
            Public dwHolidayPlanNo() As UInteger

            'holiday plan No. fill in from the front side, invalid when meet zero.
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte

            Public Sub Init()
                Me.byGroupName = New Byte((HOLIDAY_GROUP_NAME_LEN) - 1) {}
                Me.dwHolidayPlanNo = New UInteger((MAX_HOLIDAY_PLAN_NUM) - 1) {}
                Me.byRes1 = New Byte((3) - 1) {}
                Me.byRes2 = New Byte((32) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_PLAN_TEMPLATE

            Public dwSize As UInteger

            Public byEnable As Byte

            'whether to enable, 1-enable, 0-disable 
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=TEMPLATE_NAME_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byTemplateName() As Byte

            'template name 
            Public dwWeekPlanNo As UInteger

            'week plan no. 0 invalid
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_HOLIDAY_GROUP_NUM, ArraySubType:=UnmanagedType.U4)> _
            Public dwHolidayGroupNo() As UInteger

            'holiday group. fill in from the front side, invalid when meet zero.
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte

            Public Sub Init()
                Me.byTemplateName = New Byte((TEMPLATE_NAME_LEN) - 1) {}
                Me.dwHolidayGroupNo = New UInteger((MAX_HOLIDAY_GROUP_NUM) - 1) {}
                Me.byRes1 = New Byte((3) - 1) {}
                Me.byRes2 = New Byte((32) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_HOLIDAY_PLAN_COND

            Public dwSize As UInteger

            Public dwHolidayPlanNumber As UInteger

            'Holiday plan number 
            Public wLocalControllerID As System.UInt16

            'On the controller serial number [1, 64]
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=106, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byRes = New Byte((106) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_WEEK_PLAN_COND

            Public dwSize As UInteger

            Public dwWeekPlanNumber As UInteger

            'Week plan number 
            Public wLocalControllerID As System.UInt16

            'On the controller serial number [1, 64]
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=106, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byRes = New Byte((106) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_HOLIDAY_GROUP_COND

            Public dwSize As UInteger

            Public dwHolidayGroupNumber As UInteger

            'Holiday group number 
            Public wLocalControllerID As System.UInt16

            'On the controller serial number [1, 64]
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=106, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byRes = New Byte((106) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_PLAN_TEMPLATE_COND

            Public dwSize As UInteger

            Public dwPlanTemplateNumber As UInteger

            'Plan template number, starting from 1, the maximum value from the entrance guard capability sets 
            Public wLocalControllerID As System.UInt16

            'On the controller serial number[1,64], 0 is invalid 
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=106, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byRes = New Byte((106) - 1) {}
            End Sub
        End Structure


        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_DOOR_STATUS_PLAN

            Public dwSize As UInteger

            Public dwTemplateNo As UInteger

            ' plan template No. 0 means cancel relation,resolve default status(ordinary status)
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byRes = New Byte((64) - 1) {}
            End Sub
        End Structure
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_CARD_READER_PLAN

            Public dwSize As UInteger

            Public dwTemplateNo As UInteger

            ' plan template No. 0 means cancel relation,resolve default status(swipe card)
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byRes = New Byte((64) - 1) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_CARD_USER_INFO_CFG

            Public dwSize As UInteger

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN)> _
            Public byUsername() As Byte

            ' user name
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=256)> _
            Public byRes2() As Byte
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_DEVICEINFO_V30

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=SERIALNO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public sSerialNumber() As Byte

            'serial number
            Public byAlarmInPortNum As Byte

            'Number of Alarm input
            Public byAlarmOutPortNum As Byte

            'Number of Alarm Output
            Public byDiskNum As Byte

            'Number of Hard Disk
            Public byDVRType As Byte

            'DVR Type, 1: DVR 2: ATM DVR 3: DVS ......
            Public byChanNum As Byte

            'Number of Analog Channel
            Public byStartChan As Byte

            'The first Channel No. E.g. DVS- 1, DVR- 1
            Public byAudioChanNum As Byte

            'Number of Audio Channel
            Public byIPChanNum As Byte

            'Maximum number of IP Channel  low
            Public byZeroChanNum As Byte

            'Zero channel encoding number//2010- 01- 16
            Public byMainProto As Byte

            'Main stream transmission protocol 0- private,  1- rtsp,2-both private and rtsp
            Public bySubProto As Byte

            'Sub stream transmission protocol 0- private,  1- rtsp,2-both private and rtsp
            Public bySupport As Byte

            'Ability, the 'AND' result by bit: 0- not support;  1- support
            'bySupport & 0x1,  smart search
            'bySupport & 0x2,  backup
            'bySupport & 0x4,  get compression configuration ability
            'bySupport & 0x8,  multi network adapter
            'bySupport & 0x10, support remote SADP
            'bySupport & 0x20  support Raid card
            'bySupport & 0x40 support IPSAN directory search
            Public bySupport1 As Byte

            'Ability expand, the 'AND' result by bit: 0- not support;  1- support
            'bySupport1 & 0x1, support snmp v30
            'bySupport1& 0x2,support distinguish download and playback
            'bySupport1 & 0x4, support deployment level
            'bySupport1 & 0x8, support vca alarm time extension 
            'bySupport1 & 0x10, support muti disks(more than 33)
            'bySupport1 & 0x20, support rtsp over http
            'bySupport1 & 0x40, support delay preview
            'bySuppory1 & 0x80 support NET_DVR_IPPARACFG_V40, in addition  support  License plate of the new alarm information
            Public bySupport2 As Byte

            'Ability expand, the 'AND' result by bit: 0- not support;  1- support
            'bySupport & 0x1, decoder support get stream by URL
            'bySupport2 & 0x2,  support FTPV40
            'bySupport2 & 0x4,  support ANR
            'bySupport2 & 0x20, support get single item of device status
            'bySupport2 & 0x40,  support stream encryt
            Public wDevType As System.UInt16

            'device type
            Public bySupport3 As Byte

            'Support  epresent by bit, 0 - not support 1 - support 
            'bySupport3 & 0x1-muti stream support 
            'bySupport3 & 0x8  support use delay preview parameter when delay preview
            'bySupport3 & 0x10 support the interface of getting alarmhost main status V40
            Public byMultiStreamProto As Byte

            'support multi stream, represent by bit, 0-not support ;1- support; bit1-stream 3 ;bit2-stream 4, bit7-main stream, bit8-sub stream
            Public byStartDChan As Byte

            'Start digital channel
            Public byStartDTalkChan As Byte

            'Start digital talk channel
            Public byHighDChanNum As Byte

            'Digital channel number high
            Public bySupport4 As Byte

            'Support  epresent by bit, 0 - not support 1 - support
            'bySupport4 & 0x4 whether support video wall unified interface
            ' bySupport4 & 0x80 Support device upload center alarm enable
            Public byLanguageType As Byte

            'support language type by bit,0-support,1-not support  
            'byLanguageType 0 -old device
            'byLanguageType & 0x1 support chinese
            'byLanguageType & 0x2 support english
            Public byVoiceInChanNum As Byte

            'voice in chan num
            Public byStartVoiceInChanNo As Byte

            'start voice in chan num
            Public bySupport5 As Byte

            '0-no support,1-support,bit0-muti stream
            'bySupport5 &0x01support wEventTypeEx 
            'bySupport5 &0x04support sence expend
            Public bySupport6 As Byte

            Public byMirrorChanNum As Byte

            'mirror channel num,<it represents direct channel in the recording host
            Public wStartMirrorChanNo As System.UInt16

            'start mirror chan
            Public bySupport7 As Byte

            'Support  epresent by bit, 0 - not support 1 - support 
            'bySupport7 & 0x1- supports INTER_VCA_RULECFG_V42 extension    
            ' bySupport7 & 0x2  Supports HVT IPC mode expansion
            ' bySupport7 & 0x04  Back lock time
            ' bySupport7 & 0x08  Set the pan PTZ position, whether to support the band channel
            ' bySupport7 & 0x10  Support for dual system upgrade backup
            ' bySupport7 & 0x20  Support OSD character overlay V50
            ' bySupport7 & 0x40  Support master slave tracking (slave camera)
            ' bySupport7 & 0x80  Support message encryption 
            Public byRes2 As Byte
        End Structure
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_DEVICEINFO_V40

            Public struDeviceV30 As NET_DVR_DEVICEINFO_V30

            Public bySupportLock As Byte

            'the device support lock function,this byte assigned by SDK.when bySupportLock is 1,dwSurplusLockTime and byRetryLoginTime is valid 
            Public byRetryLoginTime As Byte

            'retry login times
            Public byPasswordLevel As Byte

            'PasswordLevel,0-invalid,1-default password,2-valid password,3-risk password       
            Public byProxyType As Byte

            'Proxy Type,0-not use proxy, 1-use socks5 proxy, 2-use EHome proxy
            Public dwSurplusLockTime As UInteger

            'surplus locked time
            Public byCharEncodeType As Byte

            'character encode type
            Public byRes1 As Byte

            Public bySupport As Byte

            Public byRes As Byte

            Public dwOEMCode As UInteger

            Public bySupportDev5 As Byte

            'Support v50 version of the device parameters, device name and device type name length is extended to 64 bytes 
            Public byLoginMode As Byte

            '{U! 0-Private{U 1-ISAPI{U
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=246)> _
            Public byRes2() As Byte
        End Structure
        'DVR device parameters
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_DEVICECFG_V40

            Public dwSize As UInteger

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN)> _
            Public sDVRName() As Byte

            'DVR name
            Public dwDVRID As UInteger

            'DVR ID //V1.4 (0- 99) ,  V1.5 (0- 255) 
            Public dwRecycleRecord As UInteger

            'cycle record, 0-disable, 1-enable
            'the following to the end is Read-only
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=SERIALNO_LEN)> _
            Public sSerialNumber() As Byte

            'SN
            Public dwSoftwareVersion As UInteger

            'Software version,Major version:16 MSB,minor version:16 LSB
            Public dwSoftwareBuildDate As UInteger

            'Build, 0xYYYYMMDD
            Public dwDSPSoftwareVersion As UInteger

            'DSP Version: 16 high bit is the major version, and 16 low bit is the minor version
            Public dwDSPSoftwareBuildDate As UInteger

            ' DSP Build, 0xYYYYMMDD
            Public dwPanelVersion As UInteger

            ' Front panel version,Major version:16 MSB,minor version:16 LSB
            Public dwHardwareVersion As UInteger

            ' Hardware version,Major version:16 MSB,minor version:16 LSB
            Public byAlarmInPortNum As Byte

            'DVR Alarm input
            Public byAlarmOutPortNum As Byte

            'DVR Alarm output
            Public byRS232Num As Byte

            'DVR 232 port number
            Public byRS485Num As Byte

            'DVR 485 port number 
            Public byNetworkPortNum As Byte

            'Network port number
            Public byDiskCtrlNum As Byte

            'DVR HDD number
            Public byDiskNum As Byte

            'DVR disk number
            Public byDVRType As Byte

            'DVRtype, 1:DVR 2:ATM DVR 3:DVS ......
            Public byChanNum As Byte

            'DVR channel number
            Public byStartChan As Byte

            'start,e.g.1: DVR 2: ATM DVR 3: DVS ......- - 
            Public byDecordChans As Byte

            'DVR decoding channels
            Public byVGANum As Byte

            'VGA interface number 
            Public byUSBNum As Byte

            'USB interface number 
            Public byAuxoutNum As Byte

            'Aux output number
            Public byAudioNum As Byte

            'voice interface number
            Public byIPChanNum As Byte

            'Max. IP channel number  8 LSB 8 MSB with byHighIPChanNum 
            Public byZeroChanNum As Byte

            'Zero channel number
            Public bySupport As Byte

            'Ability set0 represent not support 1 represent support,
            'bySupport & 0x1, smart search
            'bySupport & 0x2, backup
            'bySupport & 0x4, compression ability set
            'bySupport & 0x8, multiple network adapter
            'bySupport & 0x10, remote SADP
            'bySupport & 0x20, support Raid
            'bySupport & 0x40, support IPSAN
            'bySupport & 0x80, support RTP over RTSP
            Public byEsataUseage As Byte

            'Default E-SATA: 0- backup, 1- record
            Public byIPCPlug As Byte

            '0- disable plug-and-play, 1- enable plug-and-play
            Public byStorageMode As Byte

            'Hard Disk Mode:0-group,1-quota,2-draw frame,3-Auto
            Public bySupport1 As Byte

            'Ability set0 represent not support 1 represent support,
            'bySupport1 & 0x1, support snmp v30
            'bySupport1 & 0x2, support distinguish download and playback
            'bySupport1 & 0x4, support deployment level    
            'bySupport1 & 0x8, support vca alarm time extension 
            'bySupport1 & 0x10, support muti disks(more than 33)
            'bySupport1 & 0x20, support rtsp over http    
            Public wDevType As System.UInt16

            'Device type
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=DEV_TYPE_NAME_LEN)> _
            Public byDevTypeName() As Byte

            'Device model name
            Public bySupport2 As Byte

            'The ability to set extension, bit 0 indicates does not support one expressed support for
            'bySupport2 & 0x1, Whether to support extended the OSD character overlay (terminal and capture machine expansion distinguish)
            Public byAnalogAlarmInPortNum As Byte

            'Analog alarm in number
            Public byStartAlarmInNo As Byte

            'Analog alarm in Start No.
            Public byStartAlarmOutNo As Byte

            'Analog alarm Out Start No.
            Public byStartIPAlarmInNo As Byte

            'IP alarm in Start No.  0-Invalid
            Public byStartIPAlarmOutNo As Byte

            'IP Alarm Out Start No.  0-Invalid
            Public byHighIPChanNum As Byte

            'Ip Chan Num High 8 Bit 
            Public byEnableRemotePowerOn As Byte

            'enable the equipment in a dormant state remote boot function, 0- is not enabled, the 1- enabled
            Public wDevClass As System.UInt16

            'device class 
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=6)> _
            Public byRes2() As Byte
        End Structure
        Public Delegate Sub LoginResultCallBack(ByVal lUserID As Integer, ByVal dwResult As UInteger, ByRef lpDeviceInfo As NET_DVR_DEVICEINFO_V30, ByVal pUser As IntPtr)
        <StructLayoutAttribute(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
        Public Structure NET_DVR_USER_LOGIN_INFO

            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=NET_DVR_DEV_ADDRESS_MAX_LEN)> _
            Public sDeviceAddress As String

            Public byUseTransport As Byte

            Public wPort As System.UInt16

            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=NET_DVR_LOGIN_USERNAME_MAX_LEN)> _
            Public sUserName As String

            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=NET_DVR_LOGIN_PASSWD_MAX_LEN)> _
            Public sPassword As String

            Public cbLoginResult As LoginResultCallBack

            Public pUser As IntPtr

            Public bUseAsynLogin As Boolean

            Public byProxyType As Byte

            Public byUseUTCTime As Byte

            Public byLoginMode As Byte

            Public byHttps As Byte

            Public iProxyID As Integer

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=120, ArraySubType:=UnmanagedType.I1)> _
            Public byRes3() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_PREVIEWINFO

            Public lChannel As Integer

            'Channel no.
            Public dwStreamType As UInteger

            'Stream type 0-main stream,1-sub stream,2-third stream,3-forth stream, and so on
            Public dwLinkMode As UInteger

            'Protocol type: 0-TCP, 1-UDP, 2-Muticast, 3-RTP,4-RTP/RTSP, 5-RSTP/HTTP
            Public hPlayWnd As IntPtr

            'Play window's handle;  set NULL to disable preview
            Public bBlocked As UInteger

            'If data stream requesting process is blocked or not: 0-no, 1-yes
            'if true, the SDK Connect failure return until 5s timeout  , not suitable for polling to preview.
            Public bPassbackRecord As UInteger

            '0- not enable  ,1 enable
            Public byPreviewMode As Byte

            'Preview mode 0-normal preview,2-delay preview
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=STREAM_ID_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byStreamID() As Byte

            'Stream ID
            Public byProtoType As Byte

            '0-private,1-RTSP
            Public byRes1 As Byte

            Public byVideoCodingType As Byte

            Public dwDisplayBufNum As UInteger

            'soft player display buffer size(number of frames), range:1-50, default:1
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=216, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
        Public Structure NET_DVR_IPADDR

            ''' char[16]
            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=16)> _
            Public sIpV4 As String

            ''' BYTE[128]
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128, ArraySubType:=UnmanagedType.I1)> _
            Public byIPv6() As Byte

            Public Sub Init()
                Me.byIPv6 = New Byte((128) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_ETHERNET_V30

            Public struDVRIP As NET_DVR_IPADDR

            'DVR IP address
            Public struDVRIPMask As NET_DVR_IPADDR

            'DVR IP address mask
            Public dwNetInterface As UInteger

            'net card: 1-10MBase-T 2-10MBase-T Full duplex 3-100MBase-TX 4-100M Full duplex 5-10M/100M adaptive
            Public wDVRPort As System.UInt16

            'port
            Public wMTU As System.UInt16

            'MTU default:1500
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MACADDR_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byMACAddr() As Byte

            ' mac address
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
        Public Structure NET_DVR_PPPOECFG

            Public dwPPPOE As UInteger

            '0-disable,1-enable
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public sPPPoEUser() As Byte

            'PPPoE user name
            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=PASSWD_LEN)> _
            Public sPPPoEPassword As String

            ' PPPoE password
            Public struPPPoEIP As NET_DVR_IPADDR
        End Structure
        'network configuration struct(9000 extension)
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_NETCFG_V30

            Public dwSize As UInteger

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ETHERNET, ArraySubType:=UnmanagedType.Struct)> _
            Public struEtherNet() As NET_DVR_ETHERNET_V30

            'Ethernet 
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.Struct)> _
            Public struRes1() As NET_DVR_IPADDR

            'reserve
            Public struAlarmHostIpAddr As NET_DVR_IPADDR

            ' alarm host IP address
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.U2)> _
            Public wRes2() As System.UInt16

            Public wAlarmHostIpPort As System.UInt16

            Public byUseDhcp As Byte

            Public byIPv6Mode As Byte

            'IPv6 distribute methods0-Routing announcement1-manually2-Enable the DHCP allocation
            Public struDnsServer1IpAddr As NET_DVR_IPADDR

            ' primary dns server
            Public struDnsServer2IpAddr As NET_DVR_IPADDR

            ' secondary dns server
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOMAIN_NAME, ArraySubType:=UnmanagedType.I1)> _
            Public byIpResolver() As Byte

            Public wIpResolverPort As System.UInt16

            Public wHttpPortNo As System.UInt16

            Public struMulticastIpAddr As NET_DVR_IPADDR

            ' Multicast group address
            Public struGatewayIpAddr As NET_DVR_IPADDR

            ' The gateway address
            Public struPPPoE As NET_DVR_PPPOECFG

            Public byEnablePrivateMulticastDiscovery As Byte

            'Private multicast search0~default1~enable 2-disable
            Public byEnableOnvifMulticastDiscovery As Byte

            'Onvif multicast search0~default1~enable2-disable
            Public byEnableDNS As Byte

            'DNS Atuo enable, 0-Res,1-open, 2-close
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=61, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.struEtherNet = New NET_DVR_ETHERNET_V30((MAX_ETHERNET) - 1) {}
                Me.struAlarmHostIpAddr = New NET_DVR_IPADDR
                Me.struDnsServer1IpAddr = New NET_DVR_IPADDR
                Me.struDnsServer2IpAddr = New NET_DVR_IPADDR
                Me.byIpResolver = New Byte((MAX_DOMAIN_NAME) - 1) {}
                Me.struMulticastIpAddr = New NET_DVR_IPADDR
                Me.struGatewayIpAddr = New NET_DVR_IPADDR
                Me.struPPPoE = New NET_DVR_PPPOECFG
            End Sub
        End Structure
        'Network Configure Structure(V50)
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_NETCFG_V50

            Public dwSize As UInteger

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ETHERNET, ArraySubType:=UnmanagedType.Struct)> _
            Public struEtherNet() As NET_DVR_ETHERNET_V30

            'Network Port
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.Struct)> _
            Public struRes1() As NET_DVR_IPADDR

            Public struAlarmHostIpAddr As NET_DVR_IPADDR

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte

            Public wAlarmHostIpPort As System.UInt16

            Public byUseDhcp As Byte

            Public byIPv6Mode As Byte

            'IPv6 allocation, 0- routing announcement, 1- manually, 2- enable DHCP allocation 
            Public struDnsServer1IpAddr As NET_DVR_IPADDR

            Public struDnsServer2IpAddr As NET_DVR_IPADDR

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOMAIN_NAME, ArraySubType:=UnmanagedType.I1)> _
            Public byIpResolver() As Byte

            Public wIpResolverPort As System.UInt16

            Public wHttpPortNo As System.UInt16

            Public struMulticastIpAddr As NET_DVR_IPADDR

            Public struGatewayIpAddr As NET_DVR_IPADDR

            Public struPPPoE As NET_DVR_PPPOECFG

            Public byEnablePrivateMulticastDiscovery As Byte

            'Private multicast search, 0- default, 1- enabled, 2 - disabled 
            Public byEnableOnvifMulticastDiscovery As Byte

            'Onvif multicast search, 0- default, 1- enabled, 2 - disabled 
            Public wAlarmHost2IpPort As System.UInt16

            Public struAlarmHost2IpAddr As NET_DVR_IPADDR

            Public byEnableDNS As Byte

            'DNS Enabled, 0-close,1-open 
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=599, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.struEtherNet = New NET_DVR_ETHERNET_V30((MAX_ETHERNET) - 1) {}
                Me.struRes1 = New NET_DVR_IPADDR((2) - 1) {}
                Me.struAlarmHostIpAddr = New NET_DVR_IPADDR
                Me.struAlarmHost2IpAddr = New NET_DVR_IPADDR
                Me.struDnsServer1IpAddr = New NET_DVR_IPADDR
                Me.struDnsServer2IpAddr = New NET_DVR_IPADDR
                Me.byIpResolver = New Byte((MAX_DOMAIN_NAME) - 1) {}
                Me.struMulticastIpAddr = New NET_DVR_IPADDR
                Me.struGatewayIpAddr = New NET_DVR_IPADDR
                Me.struPPPoE = New NET_DVR_PPPOECFG
                Me.byRes = New Byte((599) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_IPDEVINFO_V31

            Public byEnable As Byte

            'Valid status for IP device
            Public byProType As Byte

            'Protocol type,  0- private (default) ,  1-  Panasonic,  2-  SONY
            Public byEnableQuickAdd As Byte

            '0-  does not support quick adding of IP device;  1-   enable quick adding of IP device
            'Quick add of device IP and protocol,  fill in the other parameters as system default 
            Public byRes1 As Byte

            'reserved as 0
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public sUserName() As Byte

            'user name
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=PASSWD_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public sPassword() As Byte

            'Password
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOMAIN_NAME, ArraySubType:=UnmanagedType.I1)> _
            Public byDomain() As Byte

            'Domain name of the device
            Public struIP As NET_DVR_IPADDR

            'IP
            Public wDVRPort As System.UInt16

            ' Port number
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=DEV_ID_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public szDeviceID() As Byte

            'Device ID 
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte

            'Reserved as 0
            Public Sub Init()
                Me.sUserName = New Byte((NAME_LEN) - 1) {}
                Me.sPassword = New Byte((PASSWD_LEN) - 1) {}
                Me.byDomain = New Byte((MAX_DOMAIN_NAME) - 1) {}
                Me.szDeviceID = New Byte((DEV_ID_LEN) - 1) {}
                Me.byRes2 = New Byte((2) - 1) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
        Public Structure NET_DVR_EVENT_CARD_LINKAGE_COND

            Public dwSize As UInteger

            Public dwEventID As UInteger

            'Event ID 
            Public wLocalControllerID As System.UInt16

            'On the controller serial number [1, 64]
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=106, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_EVENT_LINKAGE_INFO

            Public wMainEventType As System.UInt16

            'main event type,0-device,1-alarmin,2-door,3-card reader
            Public wSubEventType As System.UInt16

            'sub event type
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Explicit)> _
        Public Structure NET_DVR_EVETN_CARD_LINKAGE_UNION

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1), _
             FieldOffsetAttribute(0)> _
            Public byCardNo() As Byte

            <FieldOffsetAttribute(0)> _
            Public struEventLinkage As NET_DVR_EVENT_LINKAGE_INFO

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MACADDR_LEN, ArraySubType:=UnmanagedType.I1), _
             FieldOffsetAttribute(0)> _
            Public byMACAddr() As Byte

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=NET_SDK_EMPLOYEE_NO_LEN, ArraySubType:=UnmanagedType.I1), _
             FieldOffsetAttribute(0)> _
            Public byEmployeeNo() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_EVENT_CARD_LINKAGE_CFG_V50

            Public dwSize As UInteger

            Public byProMode As Byte

            'linkage type,0-by event,1-by card, 2-by mac
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            Public dwEventSourceID As UInteger

            'event source ID,when the main event is device ,it not use; when the main event is door ,it is the door No; when the main event is card reader ,it is the card reader No; when the main event is alarmin,it is the alarmin ID; 0xffffffff means all
            Public uLinkageInfo As NET_DVR_EVETN_CARD_LINKAGE_UNION

            'Linkage mode parameters 
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMOUT_NUM, ArraySubType:=UnmanagedType.I1)> _
            Public byAlarmout() As Byte

            'linkage alarmout NO,according to the values,0-not linkage,1-linkage
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)> _
            Public byOpenDoor() As Byte

            'whether linkage open door,according to the values,0-not linkage,1-linkage
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)> _
            Public byCloseDoor() As Byte

            'whether linkage close door,according to the values,0-not linkage,1-linkage
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)> _
            Public byNormalOpen() As Byte

            'whether linkage normal open door,according to the values,0-not linkage,1-linkage
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)> _
            Public byNormalClose() As Byte

            'whether linkage normal close door,according to the values,0-not linkage,1-linkage
            Public byMainDevBuzzer As Byte

            'whether linkage main device buzzer, 0-not linkage,1-linkage
            Public byCapturePic As Byte

            'whether linkage capture picture, 0-no, 1-yes
            Public byRecordVideo As Byte

            'whether linkage record video, 0-no, 1-yes
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=29, ArraySubType:=UnmanagedType.I1)> _
            Public byRes3() As Byte

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)> _
            Public byReaderBuzzer() As Byte

            'linkage reader buzzer,according to the values,0-not linkage,1-linkage
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMOUT_NUM, ArraySubType:=UnmanagedType.I1)> _
            Public byAlarmOutClose() As Byte

            'Associated alarm output shut down, in bytes, 0-not linkage,1-linkage 
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMIN_NUM, ArraySubType:=UnmanagedType.I1)> _
            Public byAlarmInSetup() As Byte

            'Associated slip protection, in bytes, 0-not linkage,1-linkage
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMIN_NUM, ArraySubType:=UnmanagedType.I1)> _
            Public byAlarmInClose() As Byte

            'Removal associated protection zones, in bytes, 0-not linkage,1-linkage
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=500, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byRes1 = New Byte((3) - 1) {}
                Me.byAlarmout = New Byte((MAX_ALARMHOST_ALARMOUT_NUM) - 1) {}
                Me.byOpenDoor = New Byte((MAX_DOOR_NUM_256) - 1) {}
                Me.byCloseDoor = New Byte((MAX_DOOR_NUM_256) - 1) {}
                Me.byNormalOpen = New Byte((MAX_DOOR_NUM_256) - 1) {}
                Me.byNormalClose = New Byte((MAX_DOOR_NUM_256) - 1) {}
                Me.byRes3 = New Byte((29) - 1) {}
                Me.byReaderBuzzer = New Byte((MAX_CARD_READER_NUM_512) - 1) {}
                Me.byAlarmOutClose = New Byte((MAX_ALARMHOST_ALARMOUT_NUM) - 1) {}
                Me.byAlarmInSetup = New Byte((MAX_ALARMHOST_ALARMIN_NUM) - 1) {}
                Me.byAlarmInClose = New Byte((MAX_ALARMHOST_ALARMIN_NUM) - 1) {}
                Me.byRes = New Byte((500) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_EVENT_CARD_LINKAGE_CFG_V51

            Public dwSize As UInteger

            Public byProMode As Byte

            'linkage type,0-by event,1-by card, 2-by mac, 3-by employee No
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            Public dwEventSourceID As UInteger

            'event source ID,when the main event is device ,it not use; when the main event is door ,it is the door No; when the main event is card reader ,it is the card reader No; when the main event is alarmin,it is the alarmin ID; 0xffffffff means all
            Public uLinkageInfo As NET_DVR_EVETN_CARD_LINKAGE_UNION

            'Linkage mode parameters 
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMOUT_NUM, ArraySubType:=UnmanagedType.I1)> _
            Public byAlarmout() As Byte

            'linkage alarmout NO,according to the values,0-not linkage,1-linkage
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)> _
            Public byOpenDoor() As Byte

            'whether linkage open door,according to the values,0-not linkage,1-linkage
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)> _
            Public byCloseDoor() As Byte

            'whether linkage close door,according to the values,0-not linkage,1-linkage
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)> _
            Public byNormalOpen() As Byte

            'whether linkage normal open door,according to the values,0-not linkage,1-linkage
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)> _
            Public byNormalClose() As Byte

            'whether linkage normal close door,according to the values,0-not linkage,1-linkage
            Public byMainDevBuzzer As Byte

            'whether linkage main device buzzer, 0-not linkage,1-linkage
            Public byCapturePic As Byte

            'whether linkage capture picture, 0-no, 1-yes
            Public byRecordVideo As Byte

            'whether linkage record video, 0-no, 1-yes
            Public byMainDevStopBuzzer As Byte

            'whether linkage record video, 0-no, 1-yes
            Public wAudioDisplayID As System.UInt16

            Public byAudioDisplayMode As Byte

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=25, ArraySubType:=UnmanagedType.I1)> _
            Public byRes3() As Byte

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)> _
            Public byReaderBuzzer() As Byte

            'linkage reader buzzer,according to the values,0-not linkage,1-linkage
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMOUT_NUM, ArraySubType:=UnmanagedType.I1)> _
            Public byAlarmOutClose() As Byte

            'Associated alarm output shut down, in bytes, 0-not linkage,1-linkage 
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMIN_NUM, ArraySubType:=UnmanagedType.I1)> _
            Public byAlarmInSetup() As Byte

            'Associated slip protection, in bytes, 0-not linkage,1-linkage
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMIN_NUM, ArraySubType:=UnmanagedType.I1)> _
            Public byAlarmInClose() As Byte

            'Removal associated protection zones, in bytes, 0-not linkage,1-linkage
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)> _
            Public byReaderStopBuzzer() As Byte

            'Removal associated protection zones, in bytes, 0-not linkage,1-linkage
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=512, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byRes1 = New Byte((3) - 1) {}
                Me.byAlarmout = New Byte((MAX_ALARMHOST_ALARMOUT_NUM) - 1) {}
                Me.byRes2 = New Byte((32) - 1) {}
                Me.byOpenDoor = New Byte((MAX_DOOR_NUM_256) - 1) {}
                Me.byCloseDoor = New Byte((MAX_DOOR_NUM_256) - 1) {}
                Me.byNormalOpen = New Byte((MAX_DOOR_NUM_256) - 1) {}
                Me.byNormalClose = New Byte((MAX_DOOR_NUM_256) - 1) {}
                Me.byRes3 = New Byte((25) - 1) {}
                Me.byReaderBuzzer = New Byte((MAX_CARD_READER_NUM_512) - 1) {}
                Me.byAlarmOutClose = New Byte((MAX_ALARMHOST_ALARMOUT_NUM) - 1) {}
                Me.byAlarmInSetup = New Byte((MAX_ALARMHOST_ALARMIN_NUM) - 1) {}
                Me.byAlarmInClose = New Byte((MAX_ALARMHOST_ALARMIN_NUM) - 1) {}
                Me.byReaderStopBuzzer = New Byte((MAX_CARD_READER_NUM_512) - 1) {}
                Me.byRes = New Byte((512) - 1) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_IPALARMOUTINFO

            Public byIPID As Byte

            Public byAlarmOut As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=18, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_IPALARMOUTCFG

            Public dwSize As UInteger

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_IP_ALARMOUT, ArraySubType:=UnmanagedType.Struct)> _
            Public struIPAlarmOutInfo() As NET_DVR_IPALARMOUTINFO
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_IPALARMOUTINFO_V40

            Public dwIPID As UInteger

            Public dwAlarmOut As UInteger

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_IPALARMOUTCFG_V40

            Public dwSize As UInteger

            Public dwCurIPAlarmOutNum As UInteger

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_IP_ALARMIN_V40, ArraySubType:=UnmanagedType.Struct)> _
            Public struIPAlarmOutInfo() As NET_DVR_IPALARMOUTINFO_V40

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=256, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_IPALARMININFO

            Public byIPID As Byte

            Public byAlarmIn As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=18, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_IPALARMINCFG

            Public dwSize As UInteger

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_IP_ALARMIN, ArraySubType:=UnmanagedType.Struct)> _
            Public struIPAlarmInInfo() As NET_DVR_IPALARMININFO
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_IPALARMININFO_V40

            Public dwIPID As UInteger

            Public dwAlarmIn As UInteger

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_IPALARMINCFG_V40

            Public dwSize As UInteger

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_IP_ALARMIN_V40, ArraySubType:=UnmanagedType.Struct)> _
            Public struIPAlarmInInfo() As NET_DVR_IPALARMININFO_V40

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=256, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte
        End Structure
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_IPCHANINFO

            Public byEnable As Byte

            '0- Failed to connect IP device; 1- Successfully; 
            Public byIPID As Byte

            'ID of IP device,  low 8 bit 
            Public byChannel As Byte

            'Channel No. 
            Public byIPIDHigh As Byte

            'ID of IP device,  high 8 bit 
            Public byTransProtocol As Byte

            'Trans Protocol Type 0-TCP/auto (Determined by the device),1-UDP 2-Multicast 3-only TCP 4-auto
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=31, ArraySubType:=UnmanagedType.I1)> _
            Public byres() As Byte
        End Structure
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_IPSERVER_STREAM

            Public byEnable As Byte

            'Is enable
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public struIPServer As NET_DVR_IPADDR

            'IPServer Address
            Public wPort As System.UInt16

            'IPServer port
            Public wDvrNameLen As System.UInt16

            'DVR Name Length
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byDVRName() As Byte

            'DVR Name
            Public wDVRSerialLen As System.UInt16

            'Serial Length
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.U2)> _
            Public byRes1() As System.UInt16

            'reserved
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=SERIALNO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byDVRSerialNumber() As Byte

            'DVR Serial
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byUserName() As Byte

            'DVR User name
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=PASSWD_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byPassWord() As Byte

            'DVR User password
            Public byChannel As Byte

            'DVR channel
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=11, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte

            'Reserved
            Public Sub Init()
                Me.byRes = New Byte((3) - 1) {}
                Me.byRes1 = New System.UInt16((2) - 1) {}
                Me.byUserName = New Byte((NAME_LEN) - 1) {}
                Me.byPassWord = New Byte((PASSWD_LEN) - 1) {}
                Me.byDVRSerialNumber = New Byte((SERIALNO_LEN) - 1) {}
                Me.byDVRName = New Byte((NAME_LEN) - 1) {}
                Me.byRes2 = New Byte((11) - 1) {}
            End Sub
        End Structure
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_STREAM_MEDIA_SERVER_CFG

            Public byValid As Byte

            'Is enable
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            Public struDevIP As NET_DVR_IPADDR

            'stream server IP    
            Public wDevPort As System.UInt16

            'stream server Port    
            Public byTransmitType As Byte

            'Protocol: 0-TCP, 1-UDP
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=69, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte

            Public Sub Init()
                Me.byRes1 = New Byte((3) - 1) {}
                Me.byRes2 = New Byte((69) - 1) {}
            End Sub
        End Structure
        'device information
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_DEV_CHAN_INFO

            Public struIP As NET_DVR_IPADDR

            'DVR IP address
            Public wDVRPort As System.UInt16

            'DVR PORT
            Public byChannel As Byte

            'Channel
            Public byTransProtocol As Byte

            'Transmit protocol:0-TCP,1-UDP
            Public byTransMode As Byte

            Public byFactoryType As Byte

            Public byDeviceType As Byte

            'Device type(Used by videoplatfom VCA card),1-decoder(use decode channel No. or display channel depends on byVcaSupportChanMode in videoplatform ability struct),2-coder
            Public byDispChan As Byte

            'Display channel No. used by VCA configuration
            Public bySubDispChan As Byte

            'Display sub channel No. used by VCA configuration
            Public byResolution As Byte

            'Resolution: 1-CIF 2-4CIF 3-720P 4-1080P 5-500w used by big screen controler
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_DOMAIN_NAME, ArraySubType:=UnmanagedType.I1)> _
            Public byDomain() As Byte

            'Device domain name
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public sUserName() As Byte

            'Remote device user name
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=PASSWD_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public sPassword() As Byte

            'Remote device password
            Public Sub Init()
                Me.byRes = New Byte((2) - 1) {}
                Me.byDomain = New Byte((MAX_DOMAIN_NAME) - 1) {}
                Me.sUserName = New Byte((NAME_LEN) - 1) {}
                Me.sPassword = New Byte((PASSWD_LEN) - 1) {}
            End Sub
        End Structure
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_PU_STREAM_CFG

            Public dwSize As UInteger

            Public struStreamMediaSvrCfg As NET_DVR_STREAM_MEDIA_SERVER_CFG

            Public struDevChanInfo As NET_DVR_DEV_CHAN_INFO
        End Structure
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_DDNS_STREAM_CFG

            Public byEnable As Byte

            'Is Enable.
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            Private struStreamServer As NET_DVR_IPADDR

            'Stream server IP
            Public wStreamServerPort As System.UInt16

            'Stream server Port   
            Public byStreamServerTransmitType As Byte

            'Stream protocol
            Public byRes2 As Byte

            Private struIPServer As NET_DVR_IPADDR

            'IPserver IP
            Public wIPServerPort As System.UInt16

            'IPserver Port
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)> _
            Public byRes3() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public sDVRName() As Byte

            'DVR Name
            Public wDVRNameLen As System.UInt16

            'DVR Name Len
            Public wDVRSerialLen As System.UInt16

            'Serial Len
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=SERIALNO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public sDVRSerialNumber() As Byte

            'Serial number
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public sUserName() As Byte

            'the user name which is used to login DVR.
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=PASSWD_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public sPassWord() As Byte

            'the password which is used to login DVR.
            Public wDVRPort As System.UInt16

            'DVR port
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)> _
            Public byRes4() As Byte

            Public byChannel As Byte

            'channel
            Public byTransProtocol As Byte

            'protocol 
            Public byTransMode As Byte

            'transform mode
            Public byFactoryType As Byte

            'The type of factory who product the device.
            Public Sub Init()
                Me.byRes1 = New Byte((3) - 1) {}
                Me.byRes3 = New Byte((2) - 1) {}
                Me.sDVRSerialNumber = New Byte((SERIALNO_LEN) - 1) {}
                Me.sUserName = New Byte((NAME_LEN) - 1) {}
                Me.sPassWord = New Byte((PASSWD_LEN) - 1) {}
                Me.byRes4 = New Byte((2) - 1) {}
            End Sub
        End Structure
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_PU_STREAM_URL

            Public byEnable As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=URL_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public strURL() As Byte

            Public byTransPortocol As Byte

            ' transport protocol type  0-tcp  1-UDP
            Public wIPID As System.UInt16

            'Device ID,wIPID = iDevInfoIndex + iGroupNO*64 +1
            Public byChannel As Byte

            'channel NO.
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=7, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byRes = New Byte((7) - 1) {}
                Me.strURL = New Byte((URL_LEN) - 1) {}
            End Sub
        End Structure
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_HKDDNS_STREAM

            Public byEnable As Byte

            'Is enable
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64, ArraySubType:=UnmanagedType.I1)> _
            Public byDDNSDomain() As Byte

            ' hiDDNS domain
            Public wPort As System.UInt16

            'IPServer port
            Public wAliasLen As System.UInt16

            'Alias Length
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byAlias() As Byte

            'Alias
            Public wDVRSerialLen As System.UInt16

            'Serial Length
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)> _
            Public byRes1() As Byte

            'reserved
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=SERIALNO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byDVRSerialNumber() As Byte

            'DVR Serial
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=NAME_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byUserName() As Byte

            'DVR User name
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=PASSWD_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byPassWord() As Byte

            'DVR User passward
            Public byChannel As Byte

            'DVR channel
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=11, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte

            'Reserved
            Public Sub Init()
                Me.byRes = New Byte((3) - 1) {}
                Me.byDDNSDomain = New Byte((64) - 1) {}
                Me.byAlias = New Byte((NAME_LEN) - 1) {}
                Me.byRes1 = New Byte((2) - 1) {}
                Me.byDVRSerialNumber = New Byte((SERIALNO_LEN) - 1) {}
                Me.byUserName = New Byte((NAME_LEN) - 1) {}
                Me.byPassWord = New Byte((PASSWD_LEN) - 1) {}
                Me.byRes2 = New Byte((11) - 1) {}
            End Sub
        End Structure
        Dim NET_DVR_GET_IPPARACFG_V40 As Integer = 1062
        Dim NET_DVR_SET_IPPARACFG_V40 As Integer = 1063
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_IPCHANINFO_V40

            Public byEnable As Byte

            Public byRes1 As Byte

            Public wIPID As System.UInt16

            'IP ID
            Public dwChannel As UInteger

            'channel
            Public byTransProtocol As Byte

            'Trans protocol,0-TCP,1-UDP
            Public byTransMode As Byte

            Public byFactoryType As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=241, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byRes = New Byte((241) - 1) {}
            End Sub
        End Structure
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_GET_STREAM_UNION

            Public struChanInfo As NET_DVR_IPCHANINFO

            'Get stream from Device.
            Public struIPServerStream As NET_DVR_IPSERVER_STREAM

            ' //Get stream from Device which register the IPServer
            Public struPUStream As NET_DVR_PU_STREAM_CFG

            'Get stream from stream server.
            Public struDDNSStream As NET_DVR_DDNS_STREAM_CFG

            'Get stream by IPserver and stream server.
            Public struStreamUrl As NET_DVR_PU_STREAM_URL

            'get stream through stream server by url.
            Public struHkDDNSStream As NET_DVR_HKDDNS_STREAM

            'get stream through hiDDNS
            Public struIPChan As NET_DVR_IPCHANINFO_V40
        End Structure
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_STREAM_MODE

            Public byGetStreamType As Byte

            'the type of gettin stream:0-Get stream from Device, 1-Get stream fram stream server, 
            '2-Get stream from Device which register the IPServer, 3.Get stream by IPserver and stream server
            '4-get stream by url,5-hkDDNS,6-Get stream from Device,NET_DVR_IPCHANINFO_V40,7- Get Stream by Rtsp Protocal 
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)> _
            Public byRes() As Byte

            Public uGetStream As NET_DVR_GET_STREAM_UNION
        End Structure
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_IPPARACFG_V40

            Public dwSize As UInteger

            Public dwGroupNum As UInteger

            'The number of group    
            Public dwAChanNum As UInteger

            'The number of simulate channel
            Public dwDChanNum As UInteger

            'the number of IP channel
            Public dwStartDChan As UInteger

            'the begin NO. of IP channel
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CHANNUM_V30)> _
            Public byAnalogChanEnable() As Byte

            'Is simulate channel enable? represent by bit
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_IP_DEVICE_V40)> _
            Public struIPDevInfo() As NET_DVR_IPDEVINFO_V31

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_CHANNUM_V30)> _
            Public struStreamMode() As NET_DVR_STREAM_MODE

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=20)> _
            Public byRes2() As Byte
        End Structure

        Public Structure NET_DVR_ACS_WORK_STATUS_V50

            Public dwSize As UInteger

            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)> _
            Public byDoorLockStatus() As Byte

            'door lock status(relay status), 0 normally closed,1 normally open, 2 damage short - circuit alarm, 3 damage breaking alarm, 4 abnormal alarm
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)> _
            Public byDoorStatus() As Byte

            'Door status(floor status), 1 - dormancy, 2 - normally open state, 3 - normally closed state, 4 - ordinary state 
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DOOR_NUM_256, ArraySubType:=UnmanagedType.I1)> _
            Public byMagneticStatus() As Byte

            'magnetic status 0 normally closed,1 normally open, 2 damage short - circuit alarm, 3 damage breaking alarm, 4 abnormal alarm
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CASE_SENSOR_NUM, ArraySubType:=UnmanagedType.I1)> _
            Public byCaseStatus() As Byte

            'case status, 0-no input, 1-input    
            Public wBatteryVoltage As System.UInt16

            'vattery voltage , multiply 10, unit: V
            Public byBatteryLowVoltage As Byte

            'Is battery in low voltage, 0-no 1-yes
            Public byPowerSupplyStatus As Byte

            'power supply status, 1-alternating current supply, 2-battery supply
            Public byMultiDoorInterlockStatus As Byte

            'multi door interlock status, 0-close 1-open
            Public byAntiSneakStatus As Byte

            'anti sneak status, 0-close 1-open
            Public byHostAntiDismantleStatus As Byte

            'host anti dismantle status, 0-close, 1-open
            Public byIndicatorLightStatus As Byte

            'Indicator Light Status 0-offLine,1-Online
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)> _
            Public byCardReaderOnlineStatus() As Byte

            'card reader online status, 0-offline 1-online
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)> _
            Public byCardReaderAntiDismantleStatus() As Byte

            'card reader anti dismantle status, 0-close 1-open
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CARD_READER_NUM_512, ArraySubType:=UnmanagedType.I1)> _
            Public byCardReaderVerifyMode() As Byte

            'card reader verify mode, 1-swipe 2-swipe+password 3-swipe card 4-swipe card or password
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMIN_NUM, ArraySubType:=UnmanagedType.I1)> _
            Public bySetupAlarmStatus() As Byte

            'alarm in setup alarm status,0- alarm in disarm status, 1 - alarm in arm status
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMIN_NUM, ArraySubType:=UnmanagedType.I1)> _
            Public byAlarmInStatus() As Byte

            'alarm in status, 0-alarm in no alarm, 1-alarm in has alarm 
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_ALARMHOST_ALARMOUT_NUM, ArraySubType:=UnmanagedType.I1)> _
            Public byAlarmOutStatus() As Byte

            'alarm out status, 0-alarm out no alarm, 1-alarm out has alarm 
            Public dwCardNum As UInteger

            'add card number
            Public byFireAlarmStatus As Byte

            'Fire alarm status is displayed: 0 - normal, short-circuit alarm 1 -, 2 - disconnect the alarm 
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=123, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte

            Public Sub Init()
                Me.byDoorLockStatus = New Byte((MAX_DOOR_NUM_256) - 1) {}
                Me.byDoorStatus = New Byte((MAX_DOOR_NUM_256) - 1) {}
                Me.byMagneticStatus = New Byte((MAX_DOOR_NUM_256) - 1) {}
                Me.byCaseStatus = New Byte((MAX_CASE_SENSOR_NUM) - 1) {}
                Me.byCardReaderOnlineStatus = New Byte((MAX_CARD_READER_NUM_512) - 1) {}
                Me.byCardReaderAntiDismantleStatus = New Byte((MAX_CARD_READER_NUM_512) - 1) {}
                Me.byCardReaderVerifyMode = New Byte((MAX_CARD_READER_NUM_512) - 1) {}
                Me.bySetupAlarmStatus = New Byte((MAX_ALARMHOST_ALARMIN_NUM) - 1) {}
                Me.byAlarmInStatus = New Byte((MAX_ALARMHOST_ALARMIN_NUM) - 1) {}
                Me.byAlarmOutStatus = New Byte((MAX_ALARMHOST_ALARMOUT_NUM) - 1) {}
                Me.byRes2 = New Byte((123) - 1) {}
            End Sub
        End Structure
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_ACS_EVENT_COND

            Public dwSize As UInteger

            Public dwMajor As UInteger

            Public dwMinor As UInteger

            Public struStartTime As CHCNetSDK.NET_DVR_TIME

            Public struEndTime As CHCNetSDK.NET_DVR_TIME

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byCardNo() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.NAME_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byName() As Byte

            Public dwBeginSerialNo As UInteger

            Public byPicEnable As Byte

            Public byTimeType As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)> _
            Public byRes2() As Byte

            Public dwEndSerialNo As UInteger

            Public dwIOTChannelNo As UInteger

            Public wInductiveEventType As System.UInt16

            Public bySearchType As Byte

            Public byRes1 As Byte

            <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=CHCNetSDK.NET_SDK_MONITOR_ID_LEN)> _
            Public szMonitorID As String

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.NET_SDK_EMPLOYEE_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
            Public byEmployeeNo() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=140, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byCardNo = New Byte((CHCNetSDK.ACS_CARD_NO_LEN) - 1) {}
                Me.byName = New Byte((CHCNetSDK.NAME_LEN) - 1) {}
                Me.byRes2 = New Byte((2) - 1) {}
                Me.byEmployeeNo = New Byte((CHCNetSDK.NET_SDK_EMPLOYEE_NO_LEN) - 1) {}
                Me.byRes = New Byte((140) - 1) {}
            End Sub
        End Structure
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_ACS_EVENT_CFG
            Public dwSize As UInteger
            Public dwMajor As UInteger
            Public dwMinor As UInteger
            Public struTime As CHCNetSDK.NET_DVR_TIME
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.MAX_NAMELEN)> _
            Public sNetUser() As Byte
            Public struRemoteHostAddr As CHCNetSDK.NET_DVR_IPADDR
            Public struAcsEventInfo As CHCNetSDK.NET_DVR_ACS_EVENT_DETAIL
            Public dwPicDataLen As UInteger
            Public pPicData As IntPtr

            ' picture data
            Public wInductiveEventType As System.UInt16

            Public byTimeType As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=61)> _
            Public byRes() As Byte
        End Structure
        Public Structure NET_DVR_ACS_EVENT_DETAIL

            Public dwSize As UInteger

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.ACS_CARD_NO_LEN)> _
            Public byCardNo() As Byte

            Public byCardType As Byte

            Public byWhiteListNo As Byte

            Public byReportChannel As Byte

            Public byCardReaderKind As Byte

            Public dwCardReaderNo As UInteger

            Public dwDoorNo As UInteger

            Public dwVerifyNo As UInteger

            Public dwAlarmInNo As UInteger

            Public dwAlarmOutNo As UInteger

            Public dwCaseSensorNo As UInteger

            Public dwRs485No As UInteger

            Public dwMultiCardGroupNo As UInteger

            Public wAccessChannel As UInteger

            'word
            Public byDeviceNo As Byte

            Public byDistractControlNo As Byte

            Public dwEmployeeNo As UInteger

            Public wLocalControllerID As UInteger

            'word
            Public byInternetAccess As Byte

            Public byType As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.MACADDR_LEN)> _
            Public byMACAddr() As Byte

            Public bySwipeCardType As Byte

            Public byRes2 As Byte

            Public dwSerialNo As UInteger

            Public byChannelControllerID As Byte

            Public byChannelControllerLampID As Byte

            Public byChannelControllerIRAdaptorID As Byte

            Public byChannelControllerIREmitterID As Byte

            Public dwRecordChannelNum As UInteger

            Public pRecordChannelData As UInteger

            Public byUserType As Byte

            Public byCurrentVerifyMode As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)> _
            Public byRe2() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.NET_SDK_EMPLOYEE_NO_LEN)> _
            Public byEmployeeNo() As Byte

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)> _
            Public byRes() As Byte
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_VIDEO_CALL_COND

            Public dwSize As UInteger

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=128, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byRes = New Byte((128) - 1) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)> _
        Public Structure NET_DVR_VIDEO_CALL_PARAM

            Public dwSize As UInteger

            Public dwCmdType As UInteger

            'command type:0-request call;1-cancel this call;2-answer this call;3-deny local call;4-called timeout;5-finish this call;6-device is busy;7-client is busy;8-indoor offline
            Public wPeriod As System.UInt16

            'period number
            Public wBuildingNumber As System.UInt16

            'building number
            Public wUnitNumber As System.UInt16

            'unit number
            Public wFloorNumber As System.UInt16

            'floor number
            Public wRoomNumber As System.UInt16

            'room number
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=118, ArraySubType:=UnmanagedType.I1)> _
            Public byRes() As Byte

            Public Sub Init()
                Me.byRes = New Byte((118) - 1) {}
            End Sub
        End Structure
        Public Structure NET_DVR_CLIENTINFO

            Public lChannel As Int32

            Public lLinkMode As UInteger

            Public hPlayWnd As IntPtr

            Public sMultiCastIP As String
        End Structure
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure NET_DVR_VOLUME_CFG

            Public dwSize As UInteger

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=MAX_AUDIOOUT_PRO_TYPE)> _
            Public wVolume() As System.UInt16

            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)> _
            Public byRes() As Byte
        End Structure



        Public Delegate Sub RemoteConfigCallback(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
        Public Delegate Sub RealDataCallBack(ByVal lPlayHandle As Integer, ByVal dwDataType As UInteger, ByVal pBuffer As IntPtr, ByVal dwBufSize As UInteger, ByVal pUser As IntPtr)
        Public Delegate Sub MSGCallBack(ByVal lCommand As Integer, ByRef pAlarmer As NET_DVR_ALARMER, ByVal pAlarmInfo As IntPtr, ByVal dwBufLen As UInteger, ByVal pUser As IntPtr)
        Public Delegate Function MSGCallBack_V31(ByVal lCommand As Integer, ByRef pAlarmer As NET_DVR_ALARMER, ByVal pAlarmInfo As IntPtr, ByVal dwBufLen As UInteger, ByVal pUser As IntPtr) As Boolean
        Public Delegate Sub REALDATACALLBACKCAPS(ByVal lRealHandle As Int32, ByVal dwDataType As UInt32, ByRef pBuffer As Byte, ByVal dwBufSize As UInt32, ByVal pUser As IntPtr)
        Public Delegate Sub VOICEDATACALLBACKV30(ByVal lVoiceComHandle As Integer, ByVal pRecvDataBuffer As String, ByVal dwBufSize As UInteger, ByVal byAudioFlag As Byte, ByVal pUser As System.IntPtr)


        Public Declare Function NET_DVR_Init Lib "HCNetSDK.dll" () As Boolean

        Public Declare Function NET_DVR_Cleanup Lib "HCNetSDK.dll" () As Boolean

        Public Declare Function NET_DVR_SetLogToFile Lib "HCNetSDK.dll" (ByVal nLogLevel As Integer, ByVal strLogDir As String, ByVal bAutoDel As Boolean) As Boolean

        Public Declare Function NET_DVR_GetLastError Lib "HCNetSDK.dll" () As UInteger

        Public Declare Function NET_DVR_GetErrorMsg Lib "HCNetSDK.dll" (ByRef pErrorNo As Integer) As IntPtr

        Public Declare Function NET_DVR_SetAlarmDeviceUser Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal lUserIndex As Integer, ByRef lpDeviceUser As NET_DVR_ALARM_DEVICE_USER) As Boolean

        Public Declare Function NET_DVR_GetAlarmDeviceUser Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal lUserIndex As Integer, ByRef lpDeviceUser As NET_DVR_ALARM_DEVICE_USER) As Boolean

        Public Declare Function NET_DVR_GetDVRConfig Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwCommand As UInteger, ByVal lChannel As Integer, ByVal lpOutBuffer As IntPtr, ByVal dwOutBufferSize As UInteger, ByRef lpBytesReturned As UInteger) As Boolean

        Public Declare Function NET_DVR_SetDVRConfig Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwCommand As UInteger, ByVal lChannel As Integer, ByVal lpInBuffer As IntPtr, ByVal dwInBufferSize As UInteger) As Boolean

        '<DllImportAttribute("HCNetSDK.dll")> _
        Public Declare Function NET_DVR_StartRemoteConfig Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwCommand As UInteger, ByVal lpInBuffer As IntPtr, ByVal dwInBufferLen As Int32, ByVal cbStateCallback As RemoteConfigCallback, ByVal pUserData As IntPtr) As Integer

        '<DllImportAttribute("HCNetSDK.dll")> _
        Public Declare Function NET_DVR_SendRemoteConfig Lib "HCNetSDK.dll" (ByVal lHandle As Integer, ByVal dwDataType As UInteger, ByVal pSendBuf As IntPtr, ByVal dwBufSize As UInteger) As Boolean

        ' stop a long connection
        ' [in] lHandle - handle ,NET_DVR_StartRemoteConfig return value
        '<DllImportAttribute("HCNetSDK.dll")> _
        Public Declare Function NET_DVR_StopRemoteConfig Lib "HCNetSDK.dll" (ByVal lHandle As Integer) As Boolean

        '<DllImportAttribute("HCNetSDK.dll")> _
        Public Declare Function NET_DVR_Upgrade_V40 Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwUpgradeType As UInteger, ByVal sFileName As String, ByVal pInbuffer As IntPtr, ByVal dwInBufferLen As Int32) As Integer

        '<DllImportAttribute("HCNetSDK.dll")> _
        Public Declare Function NET_DVR_GetUpgradeProgress Lib "HCNetSDK.dll" (ByVal lUpgradeHandle As Integer) As Integer

        '<DllImportAttribute("HCNetSDK.dll")> _
        Public Declare Function NET_DVR_CloseUpgradeHandle Lib "HCNetSDK.dll" (ByVal lUpgradeHandle As Integer) As Integer

        '<DllImportAttribute("HCNetSDK.dll")> _
        Public Declare Function NET_DVR_GetRemoteConfigState Lib "HCNetSDK.dll" (ByVal lHandle As Integer, ByVal pState As IntPtr) As Boolean

        '<DllImportAttribute("HCNetSDK.dll")> _
        Public Declare Function NET_DVR_GetNextRemoteConfig Lib "HCNetSDK.dll" (ByVal lHandle As Integer, ByVal lpOutBuff As IntPtr, ByVal dwOutBuffSize As UInteger) As Integer

        Public Declare Function NET_DVR_GetDeviceConfig Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwCommand As UInteger, ByVal dwCount As UInteger, ByVal lpInBuffer As IntPtr, ByVal dwInBufferSize As UInteger, ByVal lpStatusList As IntPtr, ByVal lpOutBuffer As IntPtr, ByVal dwOutBufferSize As UInteger) As Boolean

        Public Declare Function NET_DVR_SetDeviceConfig Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwCommand As UInteger, ByVal dwCount As UInteger, ByVal lpInBuffer As IntPtr, ByVal dwInBufferSize As UInteger, ByVal lpStatusList As IntPtr, ByVal lpInParamBuffer As IntPtr, ByVal dwInParamBufferSize As UInteger) As Boolean

        Public Declare Function NET_DVR_RemoteControl Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwCommand As UInteger, ByVal lpInBuffer As IntPtr, ByVal dwInBufferSize As UInteger) As Boolean

        Public Declare Function NET_DVR_Login_V40 Lib "HCNetSDK.dll" (ByRef pLoginInfo As NET_DVR_USER_LOGIN_INFO, ByRef lpDeviceInfo As NET_DVR_DEVICEINFO_V40) As Integer

        Public Declare Function NET_DVR_Logout_V30 Lib "HCNetSDK.dll" (ByVal lUserID As Int32) As Boolean

        Public Declare Function NET_DVR_RealPlay_V40 Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByRef lpPreviewInfo As NET_DVR_PREVIEWINFO, ByVal fRealDataCallBack_V30 As RealDataCallBack, ByVal pUser As IntPtr) As Integer

        ' alarm
        Public Declare Function NET_DVR_SetupAlarmChan Lib "HCNetSDK.dll" (ByVal lUserID As Integer) As Integer

        Public Declare Function NET_DVR_SetupAlarmChan_V30 Lib "HCNetSDK.dll" (ByVal lUserID As Integer) As Integer

        Public Declare Function NET_DVR_SetupAlarmChan_V41 Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByRef lpSetupParam As NET_DVR_SETUPALARM_PARAM) As Integer

        Public Declare Function NET_DVR_CloseAlarmChan Lib "HCNetSDK.dll" (ByVal lAlarmHandle As Integer) As Boolean

        Public Declare Function NET_DVR_CloseAlarmChan_V30 Lib "HCNetSDK.dll" (ByVal lAlarmHandle As Integer) As Boolean

        Public Declare Function NET_DVR_SetDVRMessageCallBack_V50 Lib "HCNetSDK.dll" (ByVal iIndex As Integer, ByVal fMessageCallBack As MSGCallBack, ByVal pUser As IntPtr) As Boolean

        Public Declare Function NET_DVR_SetDVRMessageCallBack_V31 Lib "HCNetSDK.dll" (ByVal fMessageCallBack As MSGCallBack_V31, ByVal pUser As IntPtr) As Boolean

        Public Declare Function NET_DVR_GetDeviceAbility Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwAbilityType As UInteger, ByVal pInBuf As IntPtr, ByVal dwInLength As UInteger, ByVal pOutBuf As IntPtr, ByVal dwOutLength As UInteger) As Boolean

        Public Declare Function NET_DVR_GetSDKVersion Lib "HCNetSDK.dll" () As UInteger

        Public Declare Function NET_DVR_GetSDKBuildVersion Lib "HCNetSDK.dll" () As UInteger

        Public Declare Function NET_DVR_ControlGateway Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal lGatewayIndex As Integer, ByVal dwStaic As UInteger) As Boolean

        Public Declare Function NET_DVR_STDXMLConfig Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal lpInputParam As IntPtr, ByVal lpOutputParam As IntPtr) As Boolean

        Public Declare Function NET_DVR_RealPlay_V30 Lib "HCNetSDK.dll" (ByVal iUserID As Integer, ByRef lpClientInfo As NET_DVR_CLIENTINFO, ByVal fRealDataCallBack_V30 As REALDATACALLBACKCAPS, ByVal pUser As IntPtr, ByVal bBlocked As UInt32) As Integer

        Public Declare Function NET_DVR_StopRealPlay Lib "HCNetSDK.dll" (ByVal iRealHandle As Integer) As Boolean

        Public Declare Function NET_DVR_StartVoiceCom_V30 Lib "HCNetSDK.dll" (ByVal lUserID As Integer, ByVal dwVoiceChan As UInteger, ByVal bNeedCBNoEncData As Boolean, ByVal fVoiceDataCallBack As VOICEDATACALLBACKV30, ByVal pUser As IntPtr) As Integer

        Public Declare Function NET_DVR_SetVoiceComClientVolume Lib "HCNetSDK.dll" (ByVal lVoiceComHandle As Integer, ByVal wVolume As System.UInt16) As Boolean

        Public Declare Function NET_DVR_StopVoiceCom Lib "HCNetSDK.dll" (ByVal lVoiceComHandle As Integer) As Boolean

        'Public Declare Function PostMessage Lib "User32.dll" Alias "PostMessage" (ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As Integer) As Integer

        <DllImport("User32.dll", EntryPoint:="PostMessage")>
        Shared Function PostMessage(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As Integer) As Integer
        End Function
    End Class
End Namespace