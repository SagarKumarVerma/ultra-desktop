﻿Imports DevExpress.XtraGrid.Views.Grid
Imports System.Resources
Imports System.Globalization
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid.Views.Base
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO
Imports DevExpress.LookAndFeel
Public Class XtraComplianceOT
    Dim ulf As UserLookAndFeel
    Dim rowCount As Integer = 0
    Public Sub New()
        InitializeComponent()
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub XtraComplianceOT_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim sSql = "select * from OTSETUP"
        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        rowCount = ds.Tables(0).Rows.Count
        If ds.Tables(0).Rows.Count > 0 Then
            mskOtperDay.Text = ds.Tables(0).Rows(0)("OtperDay").ToString.Trim
            mskOtperWeek.Text = ds.Tables(0).Rows(0)("OtperWeek").ToString.Trim
            mskOtMonthly.Text = ds.Tables(0).Rows(0)("OtMonthly").ToString.Trim
        Else
            mskOtperDay.Text = "0"
            mskOtperWeek.Text = "0"
            mskOtMonthly.Text = "0"
        End If
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If mskOtperDay.Text.Trim = "" Then
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>OT Per Day cannot be Empty</size>", "<size=9>Error</size>")
            mskOtperDay.Select()
            Exit Sub
        End If
        If mskOtperWeek.Text.Trim = "" Then
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>OT Per Week cannot be Empty</size>", "<size=9>Error</size>")
            mskOtperWeek.Select()
            Exit Sub
        End If
        If mskOtMonthly.Text.Trim = "" Then
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>OT Per Month cannot be Empty</size>", "<size=9>Error</size>")
            mskOtMonthly.Select()
            Exit Sub
        End If
        Try
            Dim sSql As String
            If rowCount > 0 Then
                sSql = "Update OtSETUP set otMonthly='" & mskOtMonthly.Text.Trim & "',otperDay='" & mskOtperDay.Text.Trim & "',otperWeek='" & mskOtperWeek.Text.Trim & "'"
            Else
                sSql = "insert into OTSETUP (OTMONTHLY,OTPERDAY, OTPERWEEK) values ('" & mskOtMonthly.Text.Trim & "','" & mskOtperDay.Text.Trim & "','" & mskOtperWeek.Text.Trim & "'')"
            End If

            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                Dim cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            XtraMessageBox.Show(ulf, "<size=10>Saved Successfuly</size>", "<size=9>Success</size>")
            Me.Close()
        Catch ex As Exception
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            XtraMessageBox.Show(ulf, "<size=10>" & ex.Message & "</size>", "<size=9>Failed</size>")
        End Try
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
End Class