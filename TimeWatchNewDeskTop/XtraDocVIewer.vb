﻿Imports System
Imports System.Windows.Forms
Imports DevExpress.XtraCharts
Imports System.Text
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient

Public Class XtraDocVIewer

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Dim cryRpt As New ReportDocument
        Dim crtableLogoninfos As New TableLogOnInfos
        Dim crtableLogoninfo As New TableLogOnInfo
        'Dim crConnectionInfo As New ConnectionInfo
        Dim CrTables As Tables
        'Dim CrTable As Table

        'to hide SAP logo from report
        Dim oToolStrip As System.Windows.Forms.ToolStrip = CType(CrystalReportViewer1.Controls(4), System.Windows.Forms.ToolStrip)
        oToolStrip.Items(oToolStrip.Items.Count - 1).Visible = False

        'If Common.CryReportType = "SalarySlip" Then
        '    cryRpt.Load(My.Application.Info.DirectoryPath & "\CrystalReportSalarySlip.rpt")
        'End If
        ''cryRpt.DataSourceConnections(0).IntegratedSecurity = True
        'cryRpt.DataSourceConnections(0).SetConnection(Common.servername, "SSSDB", Common.SQLUserId, Common.SQLPassword)
        ''cryRpt.DataSourceConnections(0).SetConnection("DESKTOP-PG6NDV8", "SSSDB", "", "")
        'CrystalReportViewer1.ReportSource = cryRpt
        ''CrystalReportViewer1.Refresh()


        Dim crConnectionInfo As ConnectionInfo = New ConnectionInfo()
        If Common.CryReportType = "SalarySlip" Then
            If Common.servername = "Access" Then
                'cryRpt.Refresh()
                cryRpt.Load(My.Application.Info.DirectoryPath & "\CrystalReportSalarySlipAccess.rpt")
                'cryRpt.Load(My.Application.Info.DirectoryPath & "\CrystalReportSalarySlipAccess_Temp.rpt")
            Else
                cryRpt.Load(My.Application.Info.DirectoryPath & "\CrystalReportSalarySlip.rpt")
            End If
        ElseIf Common.CryReportType = "SalaryRegister" Then
            If Common.servername = "Access" Then
                'cryRpt.Refresh()
                cryRpt.Load(My.Application.Info.DirectoryPath & "\CrystalReportSalaryRegisterAccess.rpt")
            Else
                cryRpt.Load(My.Application.Info.DirectoryPath & "\CrystalReportSalaryRegister.rpt")
            End If
        ElseIf Common.CryReportType = "CheckSalaryCheque" Then
            cryRpt.Load(My.Application.Info.DirectoryPath & "\SalaryCheque.rpt")
        ElseIf Common.CryReportType = "CheckSalaryCash" Then
            cryRpt.Load(My.Application.Info.DirectoryPath & "\SalaryCheque.rpt")
        ElseIf Common.CryReportType = "CheckSalaryBank" Then
            cryRpt.Load(My.Application.Info.DirectoryPath & "\SalaryBank.rpt")
        ElseIf Common.CryReportType = "CheckSalaryNonBank" Then
            cryRpt.Load(My.Application.Info.DirectoryPath & "\SalaryCheque.rpt")
        ElseIf Common.CryReportType = "EmpWiseCTC" Then
            If Common.servername = "Access" Then
                cryRpt.Load(My.Application.Info.DirectoryPath & "\CrystalReportEmployeeWiseCTCAccess.rpt")
            Else
                cryRpt.Load(My.Application.Info.DirectoryPath & "\CrystalReportEmployeeWiseCTC.rpt")
            End If
        ElseIf Common.CryReportType = "LoanEntryStatement" Then
            If Common.servername = "Access" Then
                cryRpt.Load(My.Application.Info.DirectoryPath & "\CrystalReportLoanEntryStatementAccess.rpt")
            Else
                cryRpt.Load(My.Application.Info.DirectoryPath & "\CrystalReportLoanEntryStatement.rpt")
            End If
            CrystalReportViewer1.SelectionFormula = Common.SelectionFormula '" {TBLADVANCE.A_L}='L' and " '
            ' CrystalReportViewer1.SelectionFormula = "tblEmployee.Paycode='10000') AND {TBLADVANCE.A_L}='L' AND {TBLADVANCE.MON_YEAR} >= #2018-09-01 00:00:00# and {TBLADVANCE.MON_YEAR} <= #2018-09-30 00:00:00# "
        ElseIf Common.CryReportType = "LoanEntryDetails" Then
            If Common.servername = "Access" Then
                cryRpt.Load(My.Application.Info.DirectoryPath & "\CrystalReportLoanEntryDetailsAccess.rpt")
            Else
                cryRpt.Load(My.Application.Info.DirectoryPath & "\CrystalReportLoanEntryDetails.rpt")
            End If
            CrystalReportViewer1.SelectionFormula = Common.SelectionFormula
        ElseIf Common.CryReportType = "PFStatement" Then
            cryRpt.Load(My.Application.Info.DirectoryPath & "\PfStatement.rpt")
            CrystalReportViewer1.SelectionFormula = Common.SelectionFormula
        ElseIf Common.CryReportType = "PFChalan" Then
            cryRpt.Load(My.Application.Info.DirectoryPath & "\PfChalan.rpt")
            CrystalReportViewer1.SelectionFormula = Common.SelectionFormula
            cryRpt.DataDefinition.FormulaFields("totemp").Text = ControlChars.Quote & XtraReportsPay.totemp & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("totepf").Text = ControlChars.Quote & XtraReportsPay.totepf & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("totfpf").Text = ControlChars.Quote & XtraReportsPay.totfpf & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("totpf").Text = ControlChars.Quote & XtraReportsPay.totpf & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("amtonpf").Text = ControlChars.Quote & XtraReportsPay.amtonpf & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("pfac21").Text = ControlChars.Quote & XtraReportsPay.pfac21 & ControlChars.Quote

            cryRpt.DataDefinition.FormulaFields("pfac02").Text = ControlChars.Quote & XtraReportsPay.pfac02 & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("pfac22").Text = ControlChars.Quote & XtraReportsPay.pfac22 & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("amt2").Text = ControlChars.Quote & XtraReportsPay.amt2 & ControlChars.Quote

            cryRpt.DataDefinition.FormulaFields("PFBANK").Text = ControlChars.Quote & XtraReportsPay.PFBank & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("Bank").Text = ControlChars.Quote & XtraReportsPay.Bank & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("Branch").Text = ControlChars.Quote & XtraReportsPay.Branch & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("Cheqamt").Text = ControlChars.Quote & XtraReportsPay.Cheqamt & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("CheqDate").Text = ControlChars.Quote & XtraReportsPay.CheqDate & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("CheqNo").Text = ControlChars.Quote & XtraReportsPay.CheqNo & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("Depositor").Text = ControlChars.Quote & XtraReportsPay.Depositor & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("formonth").Text = ControlChars.Quote & XtraReportsPay.formonth & ControlChars.Quote

            cryRpt.DataDefinition.FormulaFields("totemp1").Text = ControlChars.Quote & XtraReportsPay.totemp1 & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("AMTONPF1").Text = ControlChars.Quote & XtraReportsPay.AMTONPF1 & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("AMTONPF2").Text = ControlChars.Quote & XtraReportsPay.AMTONPF2 & ControlChars.Quote
            GoTo x
        ElseIf Common.CryReportType = "ESItatement" Then
            CrystalReportViewer1.SelectionFormula = Common.SelectionFormula
            cryRpt.Load(My.Application.Info.DirectoryPath & "\ESIStatement.rpt")

            cryRpt.DataDefinition.FormulaFields("Esiemployer").Text = ControlChars.Quote & XtraReportsPay.Esiemployer & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("Esiemp").Text = ControlChars.Quote & XtraReportsPay.Esiemp & ControlChars.Quote
            GoTo x
        ElseIf Common.CryReportType = "ESIChalan" Then
            cryRpt.Load(My.Application.Info.DirectoryPath & "\ESIChalan.rpt")
            CrystalReportViewer1.SelectionFormula = Common.SelectionFormula

            cryRpt.DataDefinition.FormulaFields("TOTESI").Text = ControlChars.Quote & XtraReportsPay.TOTESI & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("TOTEESI").Text = ControlChars.Quote & XtraReportsPay.TOTEESI & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("TOTAMT").Text = ControlChars.Quote & XtraReportsPay.TOTAMT & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("TOTAMTTEXT").Text = ControlChars.Quote & XtraReportsPay.TOTAMTTEXT & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("PFBank").Text = ControlChars.Quote & XtraReportsPay.PFBank & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("Bank").Text = ControlChars.Quote & XtraReportsPay.Bank & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("CheqDate").Text = ControlChars.Quote & XtraReportsPay.CheqDate & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("CheqNo").Text = ControlChars.Quote & XtraReportsPay.CheqNo & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("AMTONWHICH").Text = ControlChars.Quote & XtraReportsPay.AMTONWHICH & ControlChars.Quote

            GoTo x
        ElseIf Common.CryReportType = "ArrearSlip" Then
            CrystalReportViewer1.SelectionFormula = Common.SelectionFormula
            cryRpt.Load(My.Application.Info.DirectoryPath & "\ArrearSlip.rpt")
            GoTo x
        ElseIf Common.CryReportType = "ArrearReg" Then
            CrystalReportViewer1.SelectionFormula = Common.SelectionFormula
            cryRpt.Load(My.Application.Info.DirectoryPath & "\ArrearReg.rpt")
            cryRpt.DataDefinition.FormulaFields("EMPNO").Text = ControlChars.Quote & XtraReportsPay.EMPNO & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("EMPPF").Text = ControlChars.Quote & XtraReportsPay.EMPPF & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("EMPESI").Text = ControlChars.Quote & XtraReportsPay.EMPESI & ControlChars.Quote
        ElseIf Common.CryReportType = "PieceMaster" Then
           cryRpt.Load(My.Application.Info.DirectoryPath & "\PieceMaster.rpt")
        ElseIf Common.CryReportType = "PieceEmp" Then
            cryRpt.Load(My.Application.Info.DirectoryPath & "\PieceDetails.rpt")
            CrystalReportViewer1.SelectionFormula = Common.SelectionFormula
        ElseIf Common.CryReportType = "CheckEditReemSt" Then
            cryRpt.Load(My.Application.Info.DirectoryPath & "\ReimbursmentStat.rpt")
            cryRpt.DataDefinition.FormulaFields("hbank").Text = ControlChars.Quote & XtraReportsPay.hbank & ControlChars.Quote
        ElseIf Common.CryReportType = "CheckEditReesPyReg" Then
            cryRpt.Load(My.Application.Info.DirectoryPath & "\ReimbursmentPayReg.rpt")
            cryRpt.DataDefinition.FormulaFields("hbank").Text = ControlChars.Quote & XtraReportsPay.hbank & ControlChars.Quote
        ElseIf Common.CryReportType = "CheckEditSummaryStat" Then
            cryRpt.Load(My.Application.Info.DirectoryPath & "\SummaryStat.rpt")
            cryRpt.DataDefinition.FormulaFields("hk").Text = ControlChars.Quote & XtraReportsPay.hk & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("amtpfmember1").Text = ControlChars.Quote & XtraReportsPay.amtpfmember1 & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("amtpfmember2").Text = ControlChars.Quote & XtraReportsPay.amtpfmember2 & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("esiamount").Text = ControlChars.Quote & XtraReportsPay.esiamount & ControlChars.Quote
            cryRpt.DataDefinition.FormulaFields("edli").Text = ControlChars.Quote & XtraReportsPay.edli & ControlChars.Quote
            GoTo x
        ElseIf Common.CryReportType = "CheckEdidFullNFinal" Then
            cryRpt.Load(My.Application.Info.DirectoryPath & "\FullNFinal.rpt")
            CrystalReportViewer1.SelectionFormula = Common.SelectionFormula
        ElseIf Common.CryReportType = "CheckEditCashAdj" Then
            cryRpt.Load(My.Application.Info.DirectoryPath & "\CashAdj.rpt")
            CrystalReportViewer1.SelectionFormula = Common.SelectionFormula
        ElseIf Common.CryReportType = "CheckEditPendingReEm" Then
            cryRpt.Load(My.Application.Info.DirectoryPath & "\PendingReimburs.rpt")
            cryRpt.DataDefinition.FormulaFields("hbank").Text = ControlChars.Quote & XtraReportsPay.hbank & ControlChars.Quote
            CrystalReportViewer1.SelectionFormula = Common.SelectionFormula
        ElseIf Common.CryReportType = "CheckEditLEaveEncash" Then
            cryRpt.Load(My.Application.Info.DirectoryPath & "\LeaveEncash.rpt")
            CrystalReportViewer1.SelectionFormula = Common.SelectionFormula
        End If
        cryRpt.DataDefinition.FormulaFields("Akhilesh").Text = ControlChars.Quote & Common.CrystalReportTitle & ControlChars.Quote


x:      If Common.servername = "Access" Then
            crConnectionInfo.ServerName = My.Application.Info.DirectoryPath & "\HTDB.mdb" 'Common.servername
            'crConnectionInfo.DatabaseName = "HTDB.mdb"
            crConnectionInfo.Password = "SSS"
            CrTables = cryRpt.Database.Tables
            For Each CrTable As CrystalDecisions.CrystalReports.Engine.Table In CrTables
                crtableLogoninfo = CrTable.LogOnInfo
                crtableLogoninfo.ConnectionInfo = crConnectionInfo
                CrTable.ApplyLogOnInfo(crtableLogoninfo)
            Next
        Else
            crConnectionInfo.ServerName = Common.servername
            crConnectionInfo.DatabaseName = Common.DB ' "SSSDB"
            crConnectionInfo.UserID = Common.SQLUserId
            crConnectionInfo.Password = Common.SQLPassword
            CrTables = cryRpt.Database.Tables
            For Each CrTable As CrystalDecisions.CrystalReports.Engine.Table In CrTables
                crtableLogoninfo = CrTable.LogOnInfo
                crtableLogoninfo.ConnectionInfo = crConnectionInfo
                CrTable.ApplyLogOnInfo(crtableLogoninfo)
            Next
        End If
        CrystalReportViewer1.ReportSource = cryRpt
    End Sub
    'Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
    '    Dim sb As StringBuilder = New System.Text.StringBuilder()
    '    For i As Integer = 0 To 10
    '        sb.Append("<table width='100%' cellpadding='0' cellspacing='0'  border='1'>")
    '        sb.Append("<tr>")
    '        sb.Append("<td colspan='2' align='left' style='font-family:Verdana;font-size:11px;color:Black;'>Dear Ajitesh,</td>")
    '        sb.Append("</tr>")
    '        sb.Append("<tr>")
    '        sb.Append("<td colspan='2' align='center'><span style='font-family:Verdana;font-size:11px;color:Blue;'><b>" & "TimeWatch" & "</b> has requestd for new user creation</span></td>")
    '        sb.Append("</tr>")
    '        sb.Append("<tr >")
    '        sb.Append("<td style='height:10px'></td>")
    '        sb.Append("</tr>")
    '        sb.Append("<tr >")
    '        sb.Append("<td style='height:10px'></td>")
    '        sb.Append("</tr>")
    '        sb.Append("<tr >")
    '        sb.Append("<td colspan='2' style='font-family:Verdana;font-size:11px;color:Black;'>")
    '        sb.Append("Company have " & "TimeWatch" & " number of devices and " & "User No" & " numbers of users.<>br><br> Contact no." & "123456987" & ", email:" & "test@test.com" & "")
    '        sb.Append("</td>")
    '        sb.Append("</tr>")
    '        sb.Append("<tr >")
    '        sb.Append("<td style='height:10px'></td>")
    '        sb.Append("</tr>")
    '        sb.Append("</table>")
    '        sb.Append("h1 {page-break-before: always;}")
    '    Next

    '    Dim mstrFile_Name As String = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".html"
    '    Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)
    '    objWriter.WriteLine(sb)
    '    objWriter.Close()
    '    Process.Start(mstrFile_Name)
    'End Sub
End Class