﻿Imports System.Resources
Imports System.Globalization

Public Class XtraMasterMenu
    'Dim res_man As ResourceManager     'declare Resource manager to access to specific cultureinfo
    'Dim cul As CultureInfo     'declare culture info
    Public Sub New()
        InitializeComponent()
        NavigationPage1.Caption = Common.res_man.GetString("company", Common.cul)
        NavigationPage2.Caption = Common.res_man.GetString("branch", Common.cul)
        NavigationPage3.Caption = Common.res_man.GetString("department", Common.cul)
        NavigationPage4.Caption = Common.res_man.GetString("shift", Common.cul)
        NavigationPage5.Caption = Common.res_man.GetString("grade", Common.cul)
        NavigationPage6.Caption = Common.res_man.GetString("employeegroup", Common.cul)
        NavigationPage7.Caption = Common.res_man.GetString("bankmaster", Common.cul)
        NavigationPage8.Caption = Common.res_man.GetString("dispensary", Common.cul)
        NavigationPage9.Caption = Common.res_man.GetString("category", Common.cul)
        'Me.Width = Me.Parent.Width
        'Me.Height = Me.Parent.Height
        NavigationPane1.Width = NavigationPane1.Parent.Width
        Common.NavHeight = NavigationPage1.Height
        Common.NavWidth = NavigationPage1.Width

        
    End Sub
    Private Sub XtraMasterMenu_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Me.Width = My.Computer.Screen.WorkingArea.Width
        NavigationPage1.Width = NavigationPage1.Parent.Width
        Me.Width = Me.Parent.Width
        Me.Height = Me.Parent.Height
        NavigationPane1.Width = NavigationPage1.Parent.Width
        Common.NavHeight = NavigationPage1.Height
        Common.NavWidth = NavigationPage1.Width


        If Common.Company = "Y" Then
            NavigationPage1.PageVisible = True
            NavigationPane1.SelectedPage = NavigationPage1
        Else
            NavigationPage1.PageVisible = False
        End If

        If Common.Department = "Y" Then
            NavigationPage3.PageVisible = True
        Else
            NavigationPage3.PageVisible = False
        End If

        If Common.Section = "Y" Then 'emp grp
            NavigationPage6.PageVisible = True
        Else
            NavigationPage6.PageVisible = False
        End If

        If Common.Grade = "Y" Then
            NavigationPage5.PageVisible = True
        Else
            NavigationPage5.PageVisible = False
        End If
        If Common.Category = "Y" Then
            NavigationPage9.PageVisible = True
        Else
            NavigationPage9.PageVisible = False
        End If
        If Common.Shift = "Y" Then
            NavigationPage4.PageVisible = True
        Else
            NavigationPage4.PageVisible = False
        End If
        If Common.Employee = "Y" Then
            NavigationPage10.PageVisible = True
        Else
            NavigationPage10.PageVisible = False
        End If

        If NavigationPage1.PageVisible = False Then
            If NavigationPage2.PageVisible = True Then
                NavigationPane1.SelectedPage = NavigationPage2
            ElseIf NavigationPage3.PageVisible = True Then
                NavigationPane1.SelectedPage = NavigationPage3
            ElseIf NavigationPage4.PageVisible = True Then
                NavigationPane1.SelectedPage = NavigationPage4
            ElseIf NavigationPage5.PageVisible = True Then
                NavigationPane1.SelectedPage = NavigationPage5
            ElseIf NavigationPage6.PageVisible = True Then
                NavigationPane1.SelectedPage = NavigationPage6
            ElseIf NavigationPage7.PageVisible = True Then
                NavigationPane1.SelectedPage = NavigationPage7
            ElseIf NavigationPage8.PageVisible = True Then
                NavigationPane1.SelectedPage = NavigationPage8
            ElseIf NavigationPage9.PageVisible = True Then
                NavigationPane1.SelectedPage = NavigationPage9
            ElseIf NavigationPage10.PageVisible = True Then
                NavigationPane1.SelectedPage = NavigationPage10
            End If
        End If
    End Sub

    Private Sub NavigationPane1_SelectedPageIndexChanged(sender As System.Object, e As System.EventArgs) Handles NavigationPane1.SelectedPageIndexChanged
        If NavigationPane1.SelectedPageIndex = 0 Then
            NavigationPage1.Controls.Clear()
            Dim form As UserControl = New XtraCompany
            form.Dock = DockStyle.Fill
            NavigationPage1.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 1 Then
            NavigationPage2.Controls.Clear()
            Dim form As UserControl = New XtraBranch
            form.Dock = DockStyle.Fill
            NavigationPage2.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 2 Then
            NavigationPage3.Controls.Clear()
            Dim form As UserControl = New XtraDeparment
            form.Dock = DockStyle.Fill
            NavigationPage3.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 3 Then
            NavigationPage4.Controls.Clear()
            Dim form As UserControl = New XtraShift
            form.Dock = DockStyle.Fill
            NavigationPage4.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 4 Then
            NavigationPage5.Controls.Clear()
            Dim form As UserControl = New XtraGrade
            form.Dock = DockStyle.Fill
            NavigationPage5.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 5 Then
            NavigationPage6.Controls.Clear()
            Dim form As UserControl = New XtraEmployeeGroup
            form.Dock = DockStyle.Fill
            NavigationPage6.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 6 Then
            NavigationPage7.Controls.Clear()
            Dim form As UserControl = New XtraBankMaster
            form.Dock = DockStyle.Fill
            NavigationPage7.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 7 Then
            NavigationPage8.Controls.Clear()
            Dim form As UserControl = New XtraDespansary
            form.Dock = DockStyle.Fill
            NavigationPage8.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 8 Then
            NavigationPage9.Controls.Clear()
            Dim form As UserControl = New XtraCategory
            form.Dock = DockStyle.Fill
            NavigationPage9.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 9 Then
            NavigationPage9.Controls.Clear()
            Dim form As UserControl = New XtraEmployee
            form.Dock = DockStyle.Fill
            NavigationPage10.Controls.Add(form)
            form.Show()
        End If
    End Sub
End Class
