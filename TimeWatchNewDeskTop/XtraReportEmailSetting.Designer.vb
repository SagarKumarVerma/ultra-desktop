﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraReportEmailSetting
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraReportEmailSetting))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.ComboType = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButtonTest = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditTest = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextCC = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.TextSenderName = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitch1 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPort = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.TextType = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.TextSender = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPwd = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.TextUser = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.TextServer = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditEmp = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerControlEmp = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlEmp = New DevExpress.XtraGrid.GridControl()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New ULtra.SSSDBDataSet()
        Me.GridViewEmp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPAYCODE1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPNAME1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControl1 = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlReports = New DevExpress.XtraGrid.GridControl()
        Me.ReportTypeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewReports = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colReportType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit4 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlDept = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlDept = New DevExpress.XtraGrid.GridControl()
        Me.TblDepartmentBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewDept = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlComp = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlComp = New DevExpress.XtraGrid.GridControl()
        Me.TblCompanyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewComp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlBranch = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlBranch = New DevExpress.XtraGrid.GridControl()
        Me.GridViewBranch = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit7 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.PopupContainerEditLocation = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditReports = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.PopupContainerEditComp = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditDept = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.Tblbranch1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblbranch1TableAdapter()
        Me.TblbranchTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblbranchTableAdapter()
        Me.TblDepartmentTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter()
        Me.TblEmployee1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        Me.TblEmployeeTableAdapter = New ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblCompany1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblCompany1TableAdapter()
        Me.TblDepartment1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter()
        Me.TblCompanyTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblCompanyTableAdapter()
        Me.colDEPARTMENTCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEPARTMENTNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRANCHCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRANCHNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPAYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSTARTTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colENDTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ReportTypeTableAdapter = New ULtra.SSSDBDataSetTableAdapters.ReportTypeTableAdapter()
        Me.ReportType1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.ReportType1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.ComboType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditTest.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextCC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextSenderName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPort.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextSender.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPwd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextUser.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextServer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.PopupContainerEditEmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlEmp.SuspendLayout()
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControl1.SuspendLayout()
        CType(Me.GridControlReports, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReportTypeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewReports, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlDept.SuspendLayout()
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlComp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlComp.SuspendLayout()
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlBranch.SuspendLayout()
        CType(Me.GridControlBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditLocation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditReports.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.PopupContainerEditComp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditDept.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl4)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControl1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlEmp)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlDept)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlComp)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlBranch)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl3)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl2)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 5
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GroupControl4
        '
        Me.GroupControl4.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl4.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl4.AppearanceCaption.Options.UseFont = True
        Me.GroupControl4.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl4.Controls.Add(Me.ComboType)
        Me.GroupControl4.Controls.Add(Me.SimpleButtonTest)
        Me.GroupControl4.Controls.Add(Me.TextEditTest)
        Me.GroupControl4.Controls.Add(Me.LabelControl16)
        Me.GroupControl4.Controls.Add(Me.SimpleButton2)
        Me.GroupControl4.Controls.Add(Me.TextCC)
        Me.GroupControl4.Controls.Add(Me.LabelControl15)
        Me.GroupControl4.Controls.Add(Me.TextSenderName)
        Me.GroupControl4.Controls.Add(Me.LabelControl14)
        Me.GroupControl4.Controls.Add(Me.ToggleSwitch1)
        Me.GroupControl4.Controls.Add(Me.LabelControl13)
        Me.GroupControl4.Controls.Add(Me.TextPort)
        Me.GroupControl4.Controls.Add(Me.LabelControl12)
        Me.GroupControl4.Controls.Add(Me.TextType)
        Me.GroupControl4.Controls.Add(Me.LabelControl11)
        Me.GroupControl4.Controls.Add(Me.TextSender)
        Me.GroupControl4.Controls.Add(Me.LabelControl10)
        Me.GroupControl4.Controls.Add(Me.TextPwd)
        Me.GroupControl4.Controls.Add(Me.LabelControl9)
        Me.GroupControl4.Controls.Add(Me.TextUser)
        Me.GroupControl4.Controls.Add(Me.LabelControl7)
        Me.GroupControl4.Controls.Add(Me.TextServer)
        Me.GroupControl4.Controls.Add(Me.LabelControl6)
        Me.GroupControl4.Location = New System.Drawing.Point(13, 20)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(534, 349)
        Me.GroupControl4.TabIndex = 1
        Me.GroupControl4.Text = "Email SetUp"
        '
        'ComboType
        '
        Me.ComboType.EditValue = "SMTP"
        Me.ComboType.Location = New System.Drawing.Point(106, 140)
        Me.ComboType.Name = "ComboType"
        Me.ComboType.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboType.Properties.Appearance.Options.UseFont = True
        Me.ComboType.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboType.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboType.Properties.Items.AddRange(New Object() {"SMTP", "POP", "IMAP"})
        Me.ComboType.Size = New System.Drawing.Size(107, 20)
        Me.ComboType.TabIndex = 5
        '
        'SimpleButtonTest
        '
        Me.SimpleButtonTest.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonTest.Appearance.Options.UseFont = True
        Me.SimpleButtonTest.Location = New System.Drawing.Point(402, 312)
        Me.SimpleButtonTest.Name = "SimpleButtonTest"
        Me.SimpleButtonTest.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonTest.TabIndex = 12
        Me.SimpleButtonTest.Text = "Send Test Mail"
        '
        'TextEditTest
        '
        Me.TextEditTest.Location = New System.Drawing.Point(106, 313)
        Me.TextEditTest.Name = "TextEditTest"
        Me.TextEditTest.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditTest.Properties.Appearance.Options.UseFont = True
        Me.TextEditTest.Properties.MaxLength = 200
        Me.TextEditTest.Size = New System.Drawing.Size(290, 20)
        Me.TextEditTest.TabIndex = 11
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(5, 316)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(35, 14)
        Me.LabelControl16.TabIndex = 46
        Me.LabelControl16.Text = "Mail ID"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(6, 282)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 10
        Me.SimpleButton2.Text = "Save"
        '
        'TextCC
        '
        Me.TextCC.Location = New System.Drawing.Point(106, 249)
        Me.TextCC.Name = "TextCC"
        Me.TextCC.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextCC.Properties.Appearance.Options.UseFont = True
        Me.TextCC.Properties.MaxLength = 200
        Me.TextCC.Size = New System.Drawing.Size(290, 20)
        Me.TextCC.TabIndex = 9
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(5, 252)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(53, 14)
        Me.LabelControl15.TabIndex = 43
        Me.LabelControl15.Text = "CC Mail ID"
        '
        'TextSenderName
        '
        Me.TextSenderName.Location = New System.Drawing.Point(106, 223)
        Me.TextSenderName.Name = "TextSenderName"
        Me.TextSenderName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextSenderName.Properties.Appearance.Options.UseFont = True
        Me.TextSenderName.Properties.MaxLength = 50
        Me.TextSenderName.Size = New System.Drawing.Size(235, 20)
        Me.TextSenderName.TabIndex = 8
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(7, 225)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl14.TabIndex = 41
        Me.LabelControl14.Text = "Sender Name *"
        '
        'ToggleSwitch1
        '
        Me.ToggleSwitch1.Location = New System.Drawing.Point(106, 192)
        Me.ToggleSwitch1.Name = "ToggleSwitch1"
        Me.ToggleSwitch1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitch1.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitch1.Properties.OffText = "No"
        Me.ToggleSwitch1.Properties.OnText = "Yes"
        Me.ToggleSwitch1.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitch1.TabIndex = 7
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(7, 197)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl13.TabIndex = 39
        Me.LabelControl13.Text = "SSL Enable"
        '
        'TextPort
        '
        Me.TextPort.Location = New System.Drawing.Point(106, 166)
        Me.TextPort.Name = "TextPort"
        Me.TextPort.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPort.Properties.Appearance.Options.UseFont = True
        Me.TextPort.Properties.Mask.EditMask = "[0-9]*"
        Me.TextPort.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextPort.Properties.MaxLength = 3
        Me.TextPort.Size = New System.Drawing.Size(150, 20)
        Me.TextPort.TabIndex = 6
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(7, 168)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(34, 14)
        Me.LabelControl12.TabIndex = 37
        Me.LabelControl12.Text = "Port *"
        '
        'TextType
        '
        Me.TextType.Location = New System.Drawing.Point(377, 140)
        Me.TextType.Name = "TextType"
        Me.TextType.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextType.Properties.Appearance.Options.UseFont = True
        Me.TextType.Properties.MaxLength = 10
        Me.TextType.Size = New System.Drawing.Size(150, 20)
        Me.TextType.TabIndex = 36
        Me.TextType.Visible = False
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(7, 142)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(89, 14)
        Me.LabelControl11.TabIndex = 35
        Me.LabelControl11.Text = "Account Type *"
        '
        'TextSender
        '
        Me.TextSender.Location = New System.Drawing.Point(106, 114)
        Me.TextSender.Name = "TextSender"
        Me.TextSender.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextSender.Properties.Appearance.Options.UseFont = True
        Me.TextSender.Properties.MaxLength = 100
        Me.TextSender.Size = New System.Drawing.Size(235, 20)
        Me.TextSender.TabIndex = 4
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(7, 116)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(50, 14)
        Me.LabelControl10.TabIndex = 33
        Me.LabelControl10.Text = "Sender *"
        '
        'TextPwd
        '
        Me.TextPwd.Location = New System.Drawing.Point(106, 88)
        Me.TextPwd.Name = "TextPwd"
        Me.TextPwd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPwd.Properties.Appearance.Options.UseFont = True
        Me.TextPwd.Properties.MaxLength = 50
        Me.TextPwd.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextPwd.Size = New System.Drawing.Size(235, 20)
        Me.TextPwd.TabIndex = 3
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(7, 90)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(62, 14)
        Me.LabelControl9.TabIndex = 31
        Me.LabelControl9.Text = "Password *"
        '
        'TextUser
        '
        Me.TextUser.Location = New System.Drawing.Point(106, 62)
        Me.TextUser.Name = "TextUser"
        Me.TextUser.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextUser.Properties.Appearance.Options.UseFont = True
        Me.TextUser.Properties.MaxLength = 50
        Me.TextUser.Size = New System.Drawing.Size(235, 20)
        Me.TextUser.TabIndex = 2
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(7, 64)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(35, 14)
        Me.LabelControl7.TabIndex = 29
        Me.LabelControl7.Text = "User *"
        '
        'TextServer
        '
        Me.TextServer.Location = New System.Drawing.Point(106, 36)
        Me.TextServer.Name = "TextServer"
        Me.TextServer.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextServer.Properties.Appearance.Options.UseFont = True
        Me.TextServer.Properties.MaxLength = 50
        Me.TextServer.Size = New System.Drawing.Size(235, 20)
        Me.TextServer.TabIndex = 1
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(7, 38)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(46, 14)
        Me.LabelControl6.TabIndex = 27
        Me.LabelControl6.Text = "Server *"
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.PopupContainerEditEmp)
        Me.GroupControl1.Location = New System.Drawing.Point(13, 336)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(356, 116)
        Me.GroupControl1.TabIndex = 32
        Me.GroupControl1.Text = "Mail Receiver"
        Me.GroupControl1.Visible = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(10, 39)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl1.TabIndex = 19
        Me.LabelControl1.Text = "Select Employee"
        '
        'PopupContainerEditEmp
        '
        Me.PopupContainerEditEmp.Location = New System.Drawing.Point(107, 36)
        Me.PopupContainerEditEmp.Name = "PopupContainerEditEmp"
        Me.PopupContainerEditEmp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditEmp.Properties.PopupControl = Me.PopupContainerControlEmp
        Me.PopupContainerEditEmp.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditEmp.TabIndex = 20
        Me.PopupContainerEditEmp.ToolTip = "Leave blank if want for all Employees"
        '
        'PopupContainerControlEmp
        '
        Me.PopupContainerControlEmp.Controls.Add(Me.GridControlEmp)
        Me.PopupContainerControlEmp.Location = New System.Drawing.Point(375, 265)
        Me.PopupContainerControlEmp.Name = "PopupContainerControlEmp"
        Me.PopupContainerControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlEmp.TabIndex = 37
        '
        'GridControlEmp
        '
        Me.GridControlEmp.DataSource = Me.TblEmployeeBindingSource
        Me.GridControlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlEmp.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlEmp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlEmp.MainView = Me.GridViewEmp
        Me.GridControlEmp.Name = "GridControlEmp"
        Me.GridControlEmp.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit1})
        Me.GridControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlEmp.TabIndex = 6
        Me.GridControlEmp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewEmp})
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridViewEmp
        '
        Me.GridViewEmp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPAYCODE1, Me.colEMPNAME1})
        Me.GridViewEmp.GridControl = Me.GridControlEmp
        Me.GridViewEmp.Name = "GridViewEmp"
        Me.GridViewEmp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.Editable = False
        Me.GridViewEmp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewEmp.OptionsSelection.MultiSelect = True
        Me.GridViewEmp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewEmp.OptionsView.ColumnAutoWidth = False
        '
        'colPAYCODE1
        '
        Me.colPAYCODE1.Caption = "Paycode"
        Me.colPAYCODE1.FieldName = "PAYCODE"
        Me.colPAYCODE1.Name = "colPAYCODE1"
        Me.colPAYCODE1.Visible = True
        Me.colPAYCODE1.VisibleIndex = 1
        '
        'colEMPNAME1
        '
        Me.colEMPNAME1.Caption = "Name"
        Me.colEMPNAME1.FieldName = "EMPNAME"
        Me.colEMPNAME1.Name = "colEMPNAME1"
        Me.colEMPNAME1.Visible = True
        Me.colEMPNAME1.VisibleIndex = 2
        Me.colEMPNAME1.Width = 150
        '
        'RepositoryItemTimeEdit1
        '
        Me.RepositoryItemTimeEdit1.AutoHeight = False
        Me.RepositoryItemTimeEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit1.Name = "RepositoryItemTimeEdit1"
        '
        'PopupContainerControl1
        '
        Me.PopupContainerControl1.Controls.Add(Me.GridControlReports)
        Me.PopupContainerControl1.Location = New System.Drawing.Point(681, 265)
        Me.PopupContainerControl1.Name = "PopupContainerControl1"
        Me.PopupContainerControl1.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControl1.TabIndex = 38
        '
        'GridControlReports
        '
        Me.GridControlReports.DataSource = Me.ReportTypeBindingSource
        Me.GridControlReports.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlReports.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlReports.Location = New System.Drawing.Point(0, 0)
        Me.GridControlReports.MainView = Me.GridViewReports
        Me.GridControlReports.Name = "GridControlReports"
        Me.GridControlReports.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit4})
        Me.GridControlReports.Size = New System.Drawing.Size(300, 300)
        Me.GridControlReports.TabIndex = 6
        Me.GridControlReports.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewReports})
        '
        'ReportTypeBindingSource
        '
        Me.ReportTypeBindingSource.DataMember = "ReportType"
        Me.ReportTypeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewReports
        '
        Me.GridViewReports.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colReportType})
        Me.GridViewReports.GridControl = Me.GridControlReports
        Me.GridViewReports.Name = "GridViewReports"
        Me.GridViewReports.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewReports.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewReports.OptionsBehavior.Editable = False
        Me.GridViewReports.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewReports.OptionsSelection.MultiSelect = True
        Me.GridViewReports.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        '
        'colReportType
        '
        Me.colReportType.FieldName = "ReportType"
        Me.colReportType.Name = "colReportType"
        Me.colReportType.Visible = True
        Me.colReportType.VisibleIndex = 1
        '
        'RepositoryItemTimeEdit4
        '
        Me.RepositoryItemTimeEdit4.AutoHeight = False
        Me.RepositoryItemTimeEdit4.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit4.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit4.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit4.Name = "RepositoryItemTimeEdit4"
        '
        'PopupContainerControlDept
        '
        Me.PopupContainerControlDept.Controls.Add(Me.GridControlDept)
        Me.PopupContainerControlDept.Location = New System.Drawing.Point(628, 395)
        Me.PopupContainerControlDept.Name = "PopupContainerControlDept"
        Me.PopupContainerControlDept.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlDept.TabIndex = 36
        '
        'GridControlDept
        '
        Me.GridControlDept.DataSource = Me.TblDepartmentBindingSource
        Me.GridControlDept.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlDept.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlDept.Location = New System.Drawing.Point(0, 0)
        Me.GridControlDept.MainView = Me.GridViewDept
        Me.GridControlDept.Name = "GridControlDept"
        Me.GridControlDept.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit3})
        Me.GridControlDept.Size = New System.Drawing.Size(300, 300)
        Me.GridControlDept.TabIndex = 6
        Me.GridControlDept.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewDept})
        '
        'TblDepartmentBindingSource
        '
        Me.TblDepartmentBindingSource.DataMember = "tblDepartment"
        Me.TblDepartmentBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewDept
        '
        Me.GridViewDept.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn10, Me.GridColumn11})
        Me.GridViewDept.GridControl = Me.GridControlDept
        Me.GridViewDept.Name = "GridViewDept"
        Me.GridViewDept.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.Editable = False
        Me.GridViewDept.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewDept.OptionsSelection.MultiSelect = True
        Me.GridViewDept.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewDept.OptionsView.ColumnAutoWidth = False
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Depatment Code"
        Me.GridColumn10.FieldName = "DEPARTMENTCODE"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 1
        Me.GridColumn10.Width = 100
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Name"
        Me.GridColumn11.FieldName = "DEPARTMENTNAME"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 2
        Me.GridColumn11.Width = 120
        '
        'RepositoryItemTimeEdit3
        '
        Me.RepositoryItemTimeEdit3.AutoHeight = False
        Me.RepositoryItemTimeEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit3.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit3.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit3.Name = "RepositoryItemTimeEdit3"
        '
        'PopupContainerControlComp
        '
        Me.PopupContainerControlComp.Controls.Add(Me.GridControlComp)
        Me.PopupContainerControlComp.Location = New System.Drawing.Point(322, 395)
        Me.PopupContainerControlComp.Name = "PopupContainerControlComp"
        Me.PopupContainerControlComp.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlComp.TabIndex = 35
        '
        'GridControlComp
        '
        Me.GridControlComp.DataSource = Me.TblCompanyBindingSource
        Me.GridControlComp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlComp.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlComp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlComp.MainView = Me.GridViewComp
        Me.GridControlComp.Name = "GridControlComp"
        Me.GridControlComp.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit2})
        Me.GridControlComp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlComp.TabIndex = 6
        Me.GridControlComp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewComp})
        '
        'TblCompanyBindingSource
        '
        Me.TblCompanyBindingSource.DataMember = "tblCompany"
        Me.TblCompanyBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewComp
        '
        Me.GridViewComp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn4, Me.GridColumn9})
        Me.GridViewComp.GridControl = Me.GridControlComp
        Me.GridViewComp.Name = "GridViewComp"
        Me.GridViewComp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.Editable = False
        Me.GridViewComp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewComp.OptionsSelection.MultiSelect = True
        Me.GridViewComp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewComp.OptionsView.ColumnAutoWidth = False
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Company Code"
        Me.GridColumn4.FieldName = "COMPANYCODE"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 1
        Me.GridColumn4.Width = 100
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Name"
        Me.GridColumn9.FieldName = "COMPANYNAME"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 2
        Me.GridColumn9.Width = 120
        '
        'RepositoryItemTimeEdit2
        '
        Me.RepositoryItemTimeEdit2.AutoHeight = False
        Me.RepositoryItemTimeEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit2.Name = "RepositoryItemTimeEdit2"
        '
        'PopupContainerControlBranch
        '
        Me.PopupContainerControlBranch.Controls.Add(Me.GridControlBranch)
        Me.PopupContainerControlBranch.Location = New System.Drawing.Point(16, 395)
        Me.PopupContainerControlBranch.Name = "PopupContainerControlBranch"
        Me.PopupContainerControlBranch.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlBranch.TabIndex = 34
        '
        'GridControlBranch
        '
        Me.GridControlBranch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlBranch.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlBranch.Location = New System.Drawing.Point(0, 0)
        Me.GridControlBranch.MainView = Me.GridViewBranch
        Me.GridControlBranch.Name = "GridControlBranch"
        Me.GridControlBranch.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit7, Me.RepositoryItemDateEdit2})
        Me.GridControlBranch.Size = New System.Drawing.Size(300, 300)
        Me.GridControlBranch.TabIndex = 6
        Me.GridControlBranch.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewBranch})
        '
        'GridViewBranch
        '
        Me.GridViewBranch.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn3, Me.GridColumn12})
        Me.GridViewBranch.GridControl = Me.GridControlBranch
        Me.GridViewBranch.Name = "GridViewBranch"
        Me.GridViewBranch.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewBranch.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewBranch.OptionsBehavior.Editable = False
        Me.GridViewBranch.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewBranch.OptionsSelection.MultiSelect = True
        Me.GridViewBranch.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewBranch.OptionsView.ColumnAutoWidth = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Location Code"
        Me.GridColumn1.FieldName = "BRANCHCODE"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 1
        Me.GridColumn1.Width = 100
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Name"
        Me.GridColumn3.FieldName = "BRANCHNAME"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 120
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Email Id"
        Me.GridColumn12.FieldName = "Email"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 3
        '
        'RepositoryItemTimeEdit7
        '
        Me.RepositoryItemTimeEdit7.AutoHeight = False
        Me.RepositoryItemTimeEdit7.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit7.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit7.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit7.Name = "RepositoryItemTimeEdit7"
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl3.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl3.Controls.Add(Me.LabelControl8)
        Me.GroupControl3.Controls.Add(Me.SimpleButton1)
        Me.GroupControl3.Controls.Add(Me.TextEdit3)
        Me.GroupControl3.Controls.Add(Me.PopupContainerEditLocation)
        Me.GroupControl3.Controls.Add(Me.LabelControl3)
        Me.GroupControl3.Controls.Add(Me.LabelControl2)
        Me.GroupControl3.Controls.Add(Me.PopupContainerEditReports)
        Me.GroupControl3.Location = New System.Drawing.Point(553, 20)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(356, 151)
        Me.GroupControl3.TabIndex = 2
        Me.GroupControl3.Text = "Reports"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(10, 38)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(84, 14)
        Me.LabelControl8.TabIndex = 26
        Me.LabelControl8.Text = "Select Location"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(10, 123)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 4
        Me.SimpleButton1.Text = "Save"
        '
        'TextEdit3
        '
        Me.TextEdit3.EditValue = "10:00"
        Me.TextEdit3.Location = New System.Drawing.Point(128, 87)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit3.Properties.Appearance.Options.UseFont = True
        Me.TextEdit3.Properties.Mask.EditMask = "HH:mm"
        Me.TextEdit3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEdit3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit3.Properties.MaxLength = 5
        Me.TextEdit3.Size = New System.Drawing.Size(72, 20)
        Me.TextEdit3.TabIndex = 3
        '
        'PopupContainerEditLocation
        '
        Me.PopupContainerEditLocation.Location = New System.Drawing.Point(107, 35)
        Me.PopupContainerEditLocation.Name = "PopupContainerEditLocation"
        Me.PopupContainerEditLocation.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditLocation.Properties.PopupControl = Me.PopupContainerControlBranch
        Me.PopupContainerEditLocation.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditLocation.TabIndex = 1
        Me.PopupContainerEditLocation.ToolTip = "Leave blank if want for all Employees"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(10, 90)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(105, 14)
        Me.LabelControl3.TabIndex = 21
        Me.LabelControl3.Text = "Reports Send Time"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(10, 64)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(80, 14)
        Me.LabelControl2.TabIndex = 19
        Me.LabelControl2.Text = "Select Reports"
        '
        'PopupContainerEditReports
        '
        Me.PopupContainerEditReports.Location = New System.Drawing.Point(107, 61)
        Me.PopupContainerEditReports.Name = "PopupContainerEditReports"
        Me.PopupContainerEditReports.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditReports.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditReports.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditReports.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditReports.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditReports.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditReports.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditReports.Properties.PopupControl = Me.PopupContainerControl1
        Me.PopupContainerEditReports.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditReports.TabIndex = 2
        Me.PopupContainerEditReports.ToolTip = "Leave blank if want for all Employees"
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl2.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl2.Controls.Add(Me.PopupContainerEditComp)
        Me.GroupControl2.Controls.Add(Me.LabelControl4)
        Me.GroupControl2.Controls.Add(Me.PopupContainerEditDept)
        Me.GroupControl2.Controls.Add(Me.LabelControl5)
        Me.GroupControl2.Location = New System.Drawing.Point(553, 177)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(356, 82)
        Me.GroupControl2.TabIndex = 27
        Me.GroupControl2.Text = "Selection"
        Me.GroupControl2.Visible = False
        '
        'PopupContainerEditComp
        '
        Me.PopupContainerEditComp.Location = New System.Drawing.Point(118, 25)
        Me.PopupContainerEditComp.Name = "PopupContainerEditComp"
        Me.PopupContainerEditComp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditComp.Properties.PopupControl = Me.PopupContainerControlComp
        Me.PopupContainerEditComp.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditComp.TabIndex = 21
        Me.PopupContainerEditComp.ToolTip = "Leave blank if want for all Employees"
        Me.PopupContainerEditComp.Visible = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(21, 27)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(88, 14)
        Me.LabelControl4.TabIndex = 22
        Me.LabelControl4.Text = "Select Company"
        Me.LabelControl4.Visible = False
        '
        'PopupContainerEditDept
        '
        Me.PopupContainerEditDept.Location = New System.Drawing.Point(118, 51)
        Me.PopupContainerEditDept.Name = "PopupContainerEditDept"
        Me.PopupContainerEditDept.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditDept.Properties.PopupControl = Me.PopupContainerControlDept
        Me.PopupContainerEditDept.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditDept.TabIndex = 23
        Me.PopupContainerEditDept.ToolTip = "Leave blank if want for all Employees"
        Me.PopupContainerEditDept.Visible = False
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(5, 54)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(104, 14)
        Me.LabelControl5.TabIndex = 24
        Me.LabelControl5.Text = "Select Department"
        Me.LabelControl5.Visible = False
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(101, 568)
        Me.MemoEdit1.TabIndex = 3
        '
        'Tblbranch1TableAdapter1
        '
        Me.Tblbranch1TableAdapter1.ClearBeforeFill = True
        '
        'TblbranchTableAdapter
        '
        Me.TblbranchTableAdapter.ClearBeforeFill = True
        '
        'TblDepartmentTableAdapter
        '
        Me.TblDepartmentTableAdapter.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblCompany1TableAdapter1
        '
        Me.TblCompany1TableAdapter1.ClearBeforeFill = True
        '
        'TblDepartment1TableAdapter1
        '
        Me.TblDepartment1TableAdapter1.ClearBeforeFill = True
        '
        'TblCompanyTableAdapter
        '
        Me.TblCompanyTableAdapter.ClearBeforeFill = True
        '
        'colDEPARTMENTCODE
        '
        Me.colDEPARTMENTCODE.Caption = "Depatment Code"
        Me.colDEPARTMENTCODE.FieldName = "DEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Name = "colDEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Visible = True
        Me.colDEPARTMENTCODE.VisibleIndex = 1
        Me.colDEPARTMENTCODE.Width = 100
        '
        'colDEPARTMENTNAME
        '
        Me.colDEPARTMENTNAME.Caption = "Name"
        Me.colDEPARTMENTNAME.FieldName = "DEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Name = "colDEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Visible = True
        Me.colDEPARTMENTNAME.VisibleIndex = 2
        Me.colDEPARTMENTNAME.Width = 120
        '
        'colCOMPANYCODE
        '
        Me.colCOMPANYCODE.Caption = "Company Code"
        Me.colCOMPANYCODE.FieldName = "COMPANYCODE"
        Me.colCOMPANYCODE.Name = "colCOMPANYCODE"
        Me.colCOMPANYCODE.Visible = True
        Me.colCOMPANYCODE.VisibleIndex = 1
        Me.colCOMPANYCODE.Width = 100
        '
        'colCOMPANYNAME
        '
        Me.colCOMPANYNAME.Caption = "Name"
        Me.colCOMPANYNAME.FieldName = "COMPANYNAME"
        Me.colCOMPANYNAME.Name = "colCOMPANYNAME"
        Me.colCOMPANYNAME.Visible = True
        Me.colCOMPANYNAME.VisibleIndex = 2
        Me.colCOMPANYNAME.Width = 120
        '
        'colBRANCHCODE
        '
        Me.colBRANCHCODE.Caption = "Location Code"
        Me.colBRANCHCODE.FieldName = "BRANCHCODE"
        Me.colBRANCHCODE.Name = "colBRANCHCODE"
        Me.colBRANCHCODE.Visible = True
        Me.colBRANCHCODE.VisibleIndex = 1
        Me.colBRANCHCODE.Width = 100
        '
        'colBRANCHNAME
        '
        Me.colBRANCHNAME.Caption = "Name"
        Me.colBRANCHNAME.FieldName = "BRANCHNAME"
        Me.colBRANCHNAME.Name = "colBRANCHNAME"
        Me.colBRANCHNAME.Visible = True
        Me.colBRANCHNAME.VisibleIndex = 2
        Me.colBRANCHNAME.Width = 120
        '
        'colPAYCODE
        '
        Me.colPAYCODE.Caption = "Paycode"
        Me.colPAYCODE.FieldName = "PAYCODE"
        Me.colPAYCODE.Name = "colPAYCODE"
        Me.colPAYCODE.Visible = True
        Me.colPAYCODE.VisibleIndex = 1
        '
        'colEMPNAME
        '
        Me.colEMPNAME.Caption = "Name"
        Me.colEMPNAME.FieldName = "EMPNAME"
        Me.colEMPNAME.Name = "colEMPNAME"
        Me.colEMPNAME.Visible = True
        Me.colEMPNAME.VisibleIndex = 2
        Me.colEMPNAME.Width = 125
        '
        'colSHIFT
        '
        Me.colSHIFT.Caption = "Shift"
        Me.colSHIFT.FieldName = "SHIFT"
        Me.colSHIFT.Name = "colSHIFT"
        Me.colSHIFT.Visible = True
        Me.colSHIFT.VisibleIndex = 1
        '
        'colSTARTTIME
        '
        Me.colSTARTTIME.Caption = "Start Time"
        Me.colSTARTTIME.FieldName = "STARTTIME"
        Me.colSTARTTIME.Name = "colSTARTTIME"
        Me.colSTARTTIME.Visible = True
        Me.colSTARTTIME.VisibleIndex = 2
        '
        'colENDTIME
        '
        Me.colENDTIME.Caption = "End Time"
        Me.colENDTIME.FieldName = "ENDTIME"
        Me.colENDTIME.Name = "colENDTIME"
        Me.colENDTIME.Visible = True
        Me.colENDTIME.VisibleIndex = 3
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Name"
        Me.GridColumn2.FieldName = "BRANCHNAME"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 2
        Me.GridColumn2.Width = 120
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Depatment Code"
        Me.GridColumn5.FieldName = "DEPARTMENTCODE"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 1
        Me.GridColumn5.Width = 100
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Name"
        Me.GridColumn6.FieldName = "DEPARTMENTNAME"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 2
        Me.GridColumn6.Width = 120
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Company Code"
        Me.GridColumn7.FieldName = "COMPANYCODE"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 1
        Me.GridColumn7.Width = 100
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Name"
        Me.GridColumn8.FieldName = "COMPANYNAME"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 2
        Me.GridColumn8.Width = 120
        '
        'ReportTypeTableAdapter
        '
        Me.ReportTypeTableAdapter.ClearBeforeFill = True
        '
        'ReportType1TableAdapter1
        '
        Me.ReportType1TableAdapter1.ClearBeforeFill = True
        '
        'XtraReportEmailSetting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraReportEmailSetting"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.ComboType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditTest.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextCC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextSenderName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPort.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextSender.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPwd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextUser.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextServer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.PopupContainerEditEmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlEmp.ResumeLayout(False)
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControl1.ResumeLayout(False)
        CType(Me.GridControlReports, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReportTypeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewReports, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlDept.ResumeLayout(False)
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlComp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlComp.ResumeLayout(False)
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlBranch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlBranch.ResumeLayout(False)
        CType(Me.GridControlBranch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewBranch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditLocation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditReports.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.PopupContainerEditComp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditDept.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditEmp As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditLocation As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents PopupContainerEditComp As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditDept As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Tblbranch1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblbranch1TableAdapter
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As ULtra.SSSDBDataSet
    Friend WithEvents TblDepartmentBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblbranchTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblbranchTableAdapter
    Friend WithEvents TblCompanyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblDepartmentTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter
    Friend WithEvents TblEmployee1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents TblEmployeeTableAdapter As ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents TblCompany1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblCompany1TableAdapter
    Friend WithEvents TblDepartment1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter
    Friend WithEvents TblCompanyTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblCompanyTableAdapter
    Friend WithEvents colDEPARTMENTCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEPARTMENTNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRANCHCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRANCHNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPAYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents colSHIFT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTARTTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colENDTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditReports As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PopupContainerControlBranch As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlBranch As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewBranch As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit7 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents PopupContainerControlComp As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlComp As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewComp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlDept As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlDept As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewDept As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlEmp As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlEmp As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewEmp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemTimeEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControl1 As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlReports As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewReports As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemTimeEdit4 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents ReportTypeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents colReportType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ReportTypeTableAdapter As ULtra.SSSDBDataSetTableAdapters.ReportTypeTableAdapter
    Friend WithEvents ReportType1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.ReportType1TableAdapter
    Friend WithEvents colPAYCODE1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPNAME1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TextType As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextSender As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPwd As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextUser As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextServer As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPort As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitch1 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents TextSenderName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextCC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonTest As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditTest As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboType As DevExpress.XtraEditors.ComboBoxEdit

End Class
