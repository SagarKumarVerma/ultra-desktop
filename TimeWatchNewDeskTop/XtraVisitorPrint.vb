﻿Public Class XtraVisitorPrint

    Private Sub XtraVisitorPrint_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        PrintForm1.Form = Nothing
        PictureEditPhoto.Image = XtraVisitorEntry.Vimage
        LabelControlName.Text = XtraVisitorEntry.VName
        LabelControlComp.Text = XtraVisitorEntry.VCom
        LabelControlToMeet.Text = XtraVisitorEntry.VToMeet
        LabelControlDept.Text = XtraVisitorEntry.VDept
        LabelControlDate.Text = XtraVisitorEntry.VDate
        LabelControlTime.Text = XtraVisitorEntry.VTime
        LabelControlSrNo.Text = XtraVisitorEntry.VSrNo
        Timer1.Enabled = True
    End Sub

    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        Timer1.Enabled = False
        PrintForm1.Form = Me
        PrintForm1.PrintAction = Printing.PrintAction.PrintToPreview
        PrintForm1.Print(Me, PowerPacks.Printing.PrintForm.PrintOption.Scrollable)
    End Sub
End Class