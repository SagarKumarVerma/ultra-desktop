﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraPayrollSetup
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridLevelNode2 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraPayrollSetup))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.PictureEdit21 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit20 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit19 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit18 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit17 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit16 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit15 = New DevExpress.XtraEditors.PictureEdit()
        Me.CheckEditRndD11 = New DevExpress.XtraEditors.CheckEdit()
        Me.PictureEdit14 = New DevExpress.XtraEditors.PictureEdit()
        Me.CheckEditRndD10 = New DevExpress.XtraEditors.CheckEdit()
        Me.PictureEdit13 = New DevExpress.XtraEditors.PictureEdit()
        Me.CheckEditRndD9 = New DevExpress.XtraEditors.CheckEdit()
        Me.PictureEdit12 = New DevExpress.XtraEditors.PictureEdit()
        Me.CheckEditRndD8 = New DevExpress.XtraEditors.CheckEdit()
        Me.PictureEdit11 = New DevExpress.XtraEditors.PictureEdit()
        Me.CheckEditRndD7 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditRndD6 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditRndD5 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditRndD4 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditRndD3 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditRndD2 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEditRndD1 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditTDSE = New DevExpress.XtraEditors.TextEdit()
        Me.GridLookUpEditFormulaD11 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.PAYFORMULABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New ULtra.SSSDBDataSet()
        Me.GridView21 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn41 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn42 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextEditRate10 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescp10 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditRate9 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescp9 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditRate8 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescp8 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditRate7 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescp7 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditRate6 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescp6 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditRate5 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescp5 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditRate4 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescp4 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditRate3 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescp3 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditRate2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescp2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditRate1 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescp1 = New DevExpress.XtraEditors.TextEdit()
        Me.GridLookUpEditFormulaD10 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView11 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn21 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn22 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaD9 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView12 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn23 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn24 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaD8 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView13 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn25 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn26 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaD7 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView14 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn27 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn28 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaD6 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView15 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn29 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn30 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaD5 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView16 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn31 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn32 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaD4 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView17 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn33 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn34 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaD3 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView18 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn35 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn36 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaD2 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView19 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn37 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn38 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaD1 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView20 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn39 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn40 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.PictureEdit10 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit9 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit8 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit7 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit6 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit5 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit4 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit3 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit2 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit1 = New DevExpress.XtraEditors.PictureEdit()
        Me.CheckEditOT = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditMR = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditCR = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditDA = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditHRA = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditBR = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditRndE10 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditRndE9 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditRndE8 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditRndE7 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditRndE6 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditRndE5 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditRndE4 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditRndE3 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditRndE2 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEditRndE1 = New DevExpress.XtraEditors.CheckEdit()
        Me.GridLookUpEditFormulaE10 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView10 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn19 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn20 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaE9 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView9 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn17 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn18 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaE8 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView8 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaE7 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView7 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaE6 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView6 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaE5 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView5 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaE4 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaE3 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaE2 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaE1 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl46 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRateE10 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescpE10 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl47 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRateE9 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescpE9 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl48 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRateE8 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescpE8 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl49 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRateE7 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescpE7 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl50 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRateE6 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescpE6 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditRateE5 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl45 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditDescpE1 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescpE5 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditRateE1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl44 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRateE4 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl43 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditDescpE4 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditDescpE2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditRateE3 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditRateE2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescpE3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.CheckEditPFE10 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPFE8 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPFE6 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPFE4 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPFE2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPFE9 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPFE7 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPFE5 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPFE3 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPFE1 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPFMedical = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPFConv = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPFDA = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPFHRA = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPFBasic = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl59 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditPF22 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl58 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditPF21 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl57 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditPF02 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl56 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditPFRound = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl55 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditPFVPFDec = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl54 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditPFPFDec = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl53 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditPFFPRDec = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl52 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditPFEPRDec = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl51 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditPFPRErDec = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl37 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditPFLimite = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.CheckEditESIEOT = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESIE10 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESIE8 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESIE6 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESIE4 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESIE2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESIE9 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESIE7 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESIE5 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESIE3 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESIE1 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESIMedical = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESIConv = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESIDA = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESIHRA = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESIBasic = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl60 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditESIRound = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl61 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditESIEpDec = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl62 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditESIErDec = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl63 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditESILimite = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl64 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage4 = New DevExpress.XtraTab.XtraTabPage()
        Me.LabelControl71 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditGratuityFormula = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl70 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl69 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditBonusRate = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl68 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEditBonusAllOnArr = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEditBonusAmtLimit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl67 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditBonusWageLimit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl66 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditBonusMinWorking = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl65 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage5 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControlLeave = New DevExpress.XtraGrid.GridControl()
        Me.TblLeaveMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewLeave = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colLEAVEFIELD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLEAVECODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckEditchklvPF = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowLeaveMedical = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowLeaveConveyance = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowLeaveDA = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowLeaveHRA = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowLeaveE10 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowLeaveE8 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowLeaveE6 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowLeaveE4 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowLeaveE2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowLeaveE9 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowLeaveE7 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowLeaveE5 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowLeaveE3 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowLeaveE1 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowLeaveBasic = New DevExpress.XtraEditors.CheckEdit()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckEditAllowReE10 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowReE8 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowReE6 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowReE4 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowReE2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowReE9 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowReE7 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowReE5 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowReE3 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowReE1 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowReMedical = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowReConveyance = New DevExpress.XtraEditors.CheckEdit()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckEditAllowGratuity = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowBonus = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowVPF = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowESI = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowPF = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAllowProfTax = New DevExpress.XtraEditors.CheckEdit()
        Me.XtraTabPage6 = New DevExpress.XtraTab.XtraTabPage()
        Me.TextEditGrossEar10 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditGrossEar09 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditGrossEar08 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditGrossEar07 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditGrossEar06 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditGrossEar05 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditGrossEar04 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditGrossEar03 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditGrossEar02 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditGrossEar01 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditGrossMed = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditGrossConv = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditGrossHRA = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditGrossDA = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditGrossBasic = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.PAY_FORMULATableAdapter = New ULtra.SSSDBDataSetTableAdapters.PAY_FORMULATableAdapter()
        Me.PaY_FORMULA1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.PAY_FORMULA1TableAdapter()
        Me.TblLeaveMasterTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblLeaveMasterTableAdapter()
        Me.TblLeaveMaster1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblLeaveMaster1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.PictureEdit21.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit20.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit19.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit18.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit17.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit16.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit15.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndD11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit14.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndD10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndD9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndD8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndD7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndD6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndD5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndD4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndD3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndD2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndD1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditTDSE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PAYFORMULABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.PictureEdit10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditOT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditMR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditCR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditDA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditHRA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditBR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndE10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndE9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndE8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndE7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndE6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndE5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndE4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndE3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndE2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditRndE1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.CheckEditPFE10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPFE8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPFE6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPFE4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPFE2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPFE9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPFE7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPFE5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPFE3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPFE1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPFMedical.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPFConv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPFDA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPFHRA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPFBasic.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPF22.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPF21.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPF02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPFRound.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPFVPFDec.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPFPFDec.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPFFPRDec.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPFEPRDec.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPFPRErDec.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPFLimite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.CheckEditESIEOT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESIE10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESIE8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESIE6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESIE4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESIE2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESIE9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESIE7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESIE5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESIE3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESIE1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESIMedical.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESIConv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESIDA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESIHRA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESIBasic.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditESIRound.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditESIEpDec.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditESIErDec.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditESILimite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage4.SuspendLayout()
        CType(Me.TextEditGratuityFormula.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditBonusRate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditBonusAllOnArr.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditBonusAmtLimit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditBonusWageLimit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditBonusMinWorking.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage5.SuspendLayout()
        CType(Me.GridControlLeave, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblLeaveMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewLeave, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.CheckEditchklvPF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowLeaveMedical.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowLeaveConveyance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowLeaveDA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowLeaveHRA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowLeaveE10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowLeaveE8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowLeaveE6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowLeaveE4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowLeaveE2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowLeaveE9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowLeaveE7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowLeaveE5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowLeaveE3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowLeaveE1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowLeaveBasic.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.CheckEditAllowReE10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowReE8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowReE6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowReE4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowReE2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowReE9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowReE7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowReE5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowReE3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowReE1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowReMedical.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowReConveyance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.CheckEditAllowGratuity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowBonus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowVPF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowESI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowPF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAllowProfTax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage6.SuspendLayout()
        CType(Me.TextEditGrossEar10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditGrossEar09.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditGrossEar08.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditGrossEar07.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditGrossEar06.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditGrossEar05.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditGrossEar04.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditGrossEar03.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditGrossEar02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditGrossEar01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditGrossMed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditGrossConv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditGrossHRA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditGrossDA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditGrossBasic.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.XtraTabControl1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.AppearancePage.Header.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.XtraTabControl1.AppearancePage.Header.Options.UseFont = True
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.[True]
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(1036, 534)
        Me.XtraTabControl1.TabIndex = 2
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3, Me.XtraTabPage4, Me.XtraTabPage5, Me.XtraTabPage6})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.GroupControl2)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl1)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(1028, 504)
        Me.XtraTabPage1.Text = "General Setup"
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GroupControl2.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl2.Controls.Add(Me.PictureEdit21)
        Me.GroupControl2.Controls.Add(Me.PictureEdit20)
        Me.GroupControl2.Controls.Add(Me.PictureEdit19)
        Me.GroupControl2.Controls.Add(Me.PictureEdit18)
        Me.GroupControl2.Controls.Add(Me.PictureEdit17)
        Me.GroupControl2.Controls.Add(Me.PictureEdit16)
        Me.GroupControl2.Controls.Add(Me.PictureEdit15)
        Me.GroupControl2.Controls.Add(Me.CheckEditRndD11)
        Me.GroupControl2.Controls.Add(Me.PictureEdit14)
        Me.GroupControl2.Controls.Add(Me.CheckEditRndD10)
        Me.GroupControl2.Controls.Add(Me.PictureEdit13)
        Me.GroupControl2.Controls.Add(Me.CheckEditRndD9)
        Me.GroupControl2.Controls.Add(Me.PictureEdit12)
        Me.GroupControl2.Controls.Add(Me.CheckEditRndD8)
        Me.GroupControl2.Controls.Add(Me.PictureEdit11)
        Me.GroupControl2.Controls.Add(Me.CheckEditRndD7)
        Me.GroupControl2.Controls.Add(Me.CheckEditRndD6)
        Me.GroupControl2.Controls.Add(Me.CheckEditRndD5)
        Me.GroupControl2.Controls.Add(Me.CheckEditRndD4)
        Me.GroupControl2.Controls.Add(Me.CheckEditRndD3)
        Me.GroupControl2.Controls.Add(Me.CheckEditRndD2)
        Me.GroupControl2.Controls.Add(Me.LabelControl16)
        Me.GroupControl2.Controls.Add(Me.CheckEditRndD1)
        Me.GroupControl2.Controls.Add(Me.LabelControl14)
        Me.GroupControl2.Controls.Add(Me.TextEditTDSE)
        Me.GroupControl2.Controls.Add(Me.GridLookUpEditFormulaD11)
        Me.GroupControl2.Controls.Add(Me.TextEditRate10)
        Me.GroupControl2.Controls.Add(Me.TextEditDescp10)
        Me.GroupControl2.Controls.Add(Me.TextEditRate9)
        Me.GroupControl2.Controls.Add(Me.TextEditDescp9)
        Me.GroupControl2.Controls.Add(Me.TextEditRate8)
        Me.GroupControl2.Controls.Add(Me.TextEditDescp8)
        Me.GroupControl2.Controls.Add(Me.TextEditRate7)
        Me.GroupControl2.Controls.Add(Me.TextEditDescp7)
        Me.GroupControl2.Controls.Add(Me.TextEditRate6)
        Me.GroupControl2.Controls.Add(Me.TextEditDescp6)
        Me.GroupControl2.Controls.Add(Me.TextEditRate5)
        Me.GroupControl2.Controls.Add(Me.TextEditDescp5)
        Me.GroupControl2.Controls.Add(Me.TextEditRate4)
        Me.GroupControl2.Controls.Add(Me.TextEditDescp4)
        Me.GroupControl2.Controls.Add(Me.TextEditRate3)
        Me.GroupControl2.Controls.Add(Me.TextEditDescp3)
        Me.GroupControl2.Controls.Add(Me.TextEditRate2)
        Me.GroupControl2.Controls.Add(Me.TextEditDescp2)
        Me.GroupControl2.Controls.Add(Me.TextEditRate1)
        Me.GroupControl2.Controls.Add(Me.TextEditDescp1)
        Me.GroupControl2.Controls.Add(Me.GridLookUpEditFormulaD10)
        Me.GroupControl2.Controls.Add(Me.GridLookUpEditFormulaD9)
        Me.GroupControl2.Controls.Add(Me.GridLookUpEditFormulaD8)
        Me.GroupControl2.Controls.Add(Me.GridLookUpEditFormulaD7)
        Me.GroupControl2.Controls.Add(Me.GridLookUpEditFormulaD6)
        Me.GroupControl2.Controls.Add(Me.GridLookUpEditFormulaD5)
        Me.GroupControl2.Controls.Add(Me.GridLookUpEditFormulaD4)
        Me.GroupControl2.Controls.Add(Me.GridLookUpEditFormulaD3)
        Me.GroupControl2.Controls.Add(Me.GridLookUpEditFormulaD2)
        Me.GroupControl2.Controls.Add(Me.GridLookUpEditFormulaD1)
        Me.GroupControl2.Controls.Add(Me.LabelControl1)
        Me.GroupControl2.Controls.Add(Me.LabelControl2)
        Me.GroupControl2.Controls.Add(Me.LabelControl3)
        Me.GroupControl2.Controls.Add(Me.LabelControl4)
        Me.GroupControl2.Controls.Add(Me.LabelControl5)
        Me.GroupControl2.Controls.Add(Me.LabelControl6)
        Me.GroupControl2.Controls.Add(Me.LabelControl7)
        Me.GroupControl2.Controls.Add(Me.LabelControl8)
        Me.GroupControl2.Controls.Add(Me.LabelControl9)
        Me.GroupControl2.Controls.Add(Me.LabelControl10)
        Me.GroupControl2.Controls.Add(Me.LabelControl11)
        Me.GroupControl2.Controls.Add(Me.LabelControl12)
        Me.GroupControl2.Controls.Add(Me.LabelControl13)
        Me.GroupControl2.Location = New System.Drawing.Point(428, 4)
        Me.GroupControl2.LookAndFeel.SkinName = "iMaginary"
        Me.GroupControl2.LookAndFeel.UseDefaultLookAndFeel = False
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(418, 472)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "Deductions"
        '
        'PictureEdit21
        '
        Me.PictureEdit21.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit21.EditValue = CType(resources.GetObject("PictureEdit21.EditValue"), Object)
        Me.PictureEdit21.Location = New System.Drawing.Point(323, 310)
        Me.PictureEdit21.Name = "PictureEdit21"
        Me.PictureEdit21.Properties.AllowFocused = False
        Me.PictureEdit21.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit21.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit21.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit21.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit21.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit21.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit21.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit21.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit21.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit21.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit21.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit21.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit21.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit21.TabIndex = 121
        '
        'PictureEdit20
        '
        Me.PictureEdit20.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit20.EditValue = CType(resources.GetObject("PictureEdit20.EditValue"), Object)
        Me.PictureEdit20.Location = New System.Drawing.Point(323, 285)
        Me.PictureEdit20.Name = "PictureEdit20"
        Me.PictureEdit20.Properties.AllowFocused = False
        Me.PictureEdit20.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit20.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit20.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit20.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit20.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit20.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit20.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit20.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit20.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit20.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit20.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit20.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit20.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit20.TabIndex = 120
        '
        'PictureEdit19
        '
        Me.PictureEdit19.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit19.EditValue = CType(resources.GetObject("PictureEdit19.EditValue"), Object)
        Me.PictureEdit19.Location = New System.Drawing.Point(323, 259)
        Me.PictureEdit19.Name = "PictureEdit19"
        Me.PictureEdit19.Properties.AllowFocused = False
        Me.PictureEdit19.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit19.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit19.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit19.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit19.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit19.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit19.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit19.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit19.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit19.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit19.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit19.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit19.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit19.TabIndex = 119
        '
        'PictureEdit18
        '
        Me.PictureEdit18.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit18.EditValue = CType(resources.GetObject("PictureEdit18.EditValue"), Object)
        Me.PictureEdit18.Location = New System.Drawing.Point(323, 233)
        Me.PictureEdit18.Name = "PictureEdit18"
        Me.PictureEdit18.Properties.AllowFocused = False
        Me.PictureEdit18.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit18.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit18.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit18.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit18.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit18.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit18.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit18.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit18.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit18.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit18.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit18.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit18.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit18.TabIndex = 118
        '
        'PictureEdit17
        '
        Me.PictureEdit17.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit17.EditValue = CType(resources.GetObject("PictureEdit17.EditValue"), Object)
        Me.PictureEdit17.Location = New System.Drawing.Point(323, 207)
        Me.PictureEdit17.Name = "PictureEdit17"
        Me.PictureEdit17.Properties.AllowFocused = False
        Me.PictureEdit17.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit17.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit17.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit17.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit17.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit17.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit17.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit17.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit17.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit17.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit17.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit17.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit17.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit17.TabIndex = 117
        '
        'PictureEdit16
        '
        Me.PictureEdit16.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit16.EditValue = CType(resources.GetObject("PictureEdit16.EditValue"), Object)
        Me.PictureEdit16.Location = New System.Drawing.Point(323, 181)
        Me.PictureEdit16.Name = "PictureEdit16"
        Me.PictureEdit16.Properties.AllowFocused = False
        Me.PictureEdit16.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit16.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit16.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit16.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit16.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit16.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit16.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit16.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit16.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit16.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit16.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit16.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit16.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit16.TabIndex = 116
        '
        'PictureEdit15
        '
        Me.PictureEdit15.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit15.EditValue = CType(resources.GetObject("PictureEdit15.EditValue"), Object)
        Me.PictureEdit15.Location = New System.Drawing.Point(323, 154)
        Me.PictureEdit15.Name = "PictureEdit15"
        Me.PictureEdit15.Properties.AllowFocused = False
        Me.PictureEdit15.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit15.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit15.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit15.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit15.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit15.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit15.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit15.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit15.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit15.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit15.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit15.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit15.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit15.TabIndex = 115
        '
        'CheckEditRndD11
        '
        Me.CheckEditRndD11.Location = New System.Drawing.Point(355, 311)
        Me.CheckEditRndD11.Name = "CheckEditRndD11"
        Me.CheckEditRndD11.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndD11.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndD11.Properties.Caption = ""
        Me.CheckEditRndD11.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndD11.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndD11.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndD11.Properties.ValueChecked = "Y"
        Me.CheckEditRndD11.Properties.ValueGrayed = "N"
        Me.CheckEditRndD11.Properties.ValueUnchecked = "N"
        Me.CheckEditRndD11.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndD11.TabIndex = 43
        '
        'PictureEdit14
        '
        Me.PictureEdit14.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit14.EditValue = CType(resources.GetObject("PictureEdit14.EditValue"), Object)
        Me.PictureEdit14.Location = New System.Drawing.Point(323, 128)
        Me.PictureEdit14.Name = "PictureEdit14"
        Me.PictureEdit14.Properties.AllowFocused = False
        Me.PictureEdit14.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit14.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit14.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit14.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit14.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit14.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit14.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit14.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit14.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit14.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit14.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit14.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit14.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit14.TabIndex = 114
        '
        'CheckEditRndD10
        '
        Me.CheckEditRndD10.Location = New System.Drawing.Point(355, 285)
        Me.CheckEditRndD10.Name = "CheckEditRndD10"
        Me.CheckEditRndD10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndD10.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndD10.Properties.Caption = ""
        Me.CheckEditRndD10.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndD10.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndD10.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndD10.Properties.ValueChecked = "Y"
        Me.CheckEditRndD10.Properties.ValueGrayed = "N"
        Me.CheckEditRndD10.Properties.ValueUnchecked = "N"
        Me.CheckEditRndD10.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndD10.TabIndex = 40
        '
        'PictureEdit13
        '
        Me.PictureEdit13.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit13.EditValue = CType(resources.GetObject("PictureEdit13.EditValue"), Object)
        Me.PictureEdit13.Location = New System.Drawing.Point(323, 102)
        Me.PictureEdit13.Name = "PictureEdit13"
        Me.PictureEdit13.Properties.AllowFocused = False
        Me.PictureEdit13.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit13.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit13.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit13.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit13.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit13.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit13.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit13.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit13.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit13.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit13.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit13.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit13.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit13.TabIndex = 113
        '
        'CheckEditRndD9
        '
        Me.CheckEditRndD9.Location = New System.Drawing.Point(355, 259)
        Me.CheckEditRndD9.Name = "CheckEditRndD9"
        Me.CheckEditRndD9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndD9.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndD9.Properties.Caption = ""
        Me.CheckEditRndD9.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndD9.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndD9.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndD9.Properties.ValueChecked = "Y"
        Me.CheckEditRndD9.Properties.ValueGrayed = "N"
        Me.CheckEditRndD9.Properties.ValueUnchecked = "N"
        Me.CheckEditRndD9.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndD9.TabIndex = 36
        '
        'PictureEdit12
        '
        Me.PictureEdit12.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit12.EditValue = CType(resources.GetObject("PictureEdit12.EditValue"), Object)
        Me.PictureEdit12.Location = New System.Drawing.Point(323, 76)
        Me.PictureEdit12.Name = "PictureEdit12"
        Me.PictureEdit12.Properties.AllowFocused = False
        Me.PictureEdit12.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit12.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit12.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit12.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit12.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit12.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit12.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit12.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit12.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit12.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit12.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit12.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit12.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit12.TabIndex = 112
        '
        'CheckEditRndD8
        '
        Me.CheckEditRndD8.Location = New System.Drawing.Point(355, 233)
        Me.CheckEditRndD8.Name = "CheckEditRndD8"
        Me.CheckEditRndD8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndD8.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndD8.Properties.Caption = ""
        Me.CheckEditRndD8.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndD8.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndD8.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndD8.Properties.ValueChecked = "Y"
        Me.CheckEditRndD8.Properties.ValueGrayed = "N"
        Me.CheckEditRndD8.Properties.ValueUnchecked = "N"
        Me.CheckEditRndD8.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndD8.TabIndex = 32
        '
        'PictureEdit11
        '
        Me.PictureEdit11.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit11.EditValue = CType(resources.GetObject("PictureEdit11.EditValue"), Object)
        Me.PictureEdit11.Location = New System.Drawing.Point(323, 50)
        Me.PictureEdit11.Name = "PictureEdit11"
        Me.PictureEdit11.Properties.AllowFocused = False
        Me.PictureEdit11.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit11.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit11.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit11.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit11.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit11.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit11.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit11.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit11.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit11.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit11.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit11.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit11.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit11.TabIndex = 111
        '
        'CheckEditRndD7
        '
        Me.CheckEditRndD7.Location = New System.Drawing.Point(355, 207)
        Me.CheckEditRndD7.Name = "CheckEditRndD7"
        Me.CheckEditRndD7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndD7.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndD7.Properties.Caption = ""
        Me.CheckEditRndD7.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndD7.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndD7.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndD7.Properties.ValueChecked = "Y"
        Me.CheckEditRndD7.Properties.ValueGrayed = "N"
        Me.CheckEditRndD7.Properties.ValueUnchecked = "N"
        Me.CheckEditRndD7.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndD7.TabIndex = 28
        '
        'CheckEditRndD6
        '
        Me.CheckEditRndD6.Location = New System.Drawing.Point(355, 181)
        Me.CheckEditRndD6.Name = "CheckEditRndD6"
        Me.CheckEditRndD6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndD6.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndD6.Properties.Caption = ""
        Me.CheckEditRndD6.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndD6.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndD6.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndD6.Properties.ValueChecked = "Y"
        Me.CheckEditRndD6.Properties.ValueGrayed = "N"
        Me.CheckEditRndD6.Properties.ValueUnchecked = "N"
        Me.CheckEditRndD6.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndD6.TabIndex = 24
        '
        'CheckEditRndD5
        '
        Me.CheckEditRndD5.Location = New System.Drawing.Point(355, 155)
        Me.CheckEditRndD5.Name = "CheckEditRndD5"
        Me.CheckEditRndD5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndD5.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndD5.Properties.Caption = ""
        Me.CheckEditRndD5.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndD5.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndD5.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndD5.Properties.ValueChecked = "Y"
        Me.CheckEditRndD5.Properties.ValueGrayed = "N"
        Me.CheckEditRndD5.Properties.ValueUnchecked = "N"
        Me.CheckEditRndD5.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndD5.TabIndex = 20
        '
        'CheckEditRndD4
        '
        Me.CheckEditRndD4.Location = New System.Drawing.Point(355, 129)
        Me.CheckEditRndD4.Name = "CheckEditRndD4"
        Me.CheckEditRndD4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndD4.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndD4.Properties.Caption = ""
        Me.CheckEditRndD4.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndD4.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndD4.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndD4.Properties.ValueChecked = "Y"
        Me.CheckEditRndD4.Properties.ValueGrayed = "N"
        Me.CheckEditRndD4.Properties.ValueUnchecked = "N"
        Me.CheckEditRndD4.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndD4.TabIndex = 16
        '
        'CheckEditRndD3
        '
        Me.CheckEditRndD3.Location = New System.Drawing.Point(355, 103)
        Me.CheckEditRndD3.Name = "CheckEditRndD3"
        Me.CheckEditRndD3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndD3.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndD3.Properties.Caption = ""
        Me.CheckEditRndD3.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndD3.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndD3.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndD3.Properties.ValueChecked = "Y"
        Me.CheckEditRndD3.Properties.ValueGrayed = "N"
        Me.CheckEditRndD3.Properties.ValueUnchecked = "N"
        Me.CheckEditRndD3.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndD3.TabIndex = 12
        '
        'CheckEditRndD2
        '
        Me.CheckEditRndD2.Location = New System.Drawing.Point(355, 75)
        Me.CheckEditRndD2.Name = "CheckEditRndD2"
        Me.CheckEditRndD2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndD2.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndD2.Properties.Caption = ""
        Me.CheckEditRndD2.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndD2.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndD2.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndD2.Properties.ValueChecked = "Y"
        Me.CheckEditRndD2.Properties.ValueGrayed = "N"
        Me.CheckEditRndD2.Properties.ValueUnchecked = "N"
        Me.CheckEditRndD2.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndD2.TabIndex = 8
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(348, 30)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(35, 14)
        Me.LabelControl16.TabIndex = 111
        Me.LabelControl16.Text = "Round"
        '
        'CheckEditRndD1
        '
        Me.CheckEditRndD1.Location = New System.Drawing.Point(355, 48)
        Me.CheckEditRndD1.Name = "CheckEditRndD1"
        Me.CheckEditRndD1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndD1.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndD1.Properties.Caption = ""
        Me.CheckEditRndD1.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndD1.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndD1.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndD1.Properties.ValueChecked = "Y"
        Me.CheckEditRndD1.Properties.ValueGrayed = "N"
        Me.CheckEditRndD1.Properties.ValueUnchecked = "N"
        Me.CheckEditRndD1.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndD1.TabIndex = 4
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(103, 313)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(23, 14)
        Me.LabelControl14.TabIndex = 99
        Me.LabelControl14.Text = "TDS"
        '
        'TextEditTDSE
        '
        Me.TextEditTDSE.Location = New System.Drawing.Point(134, 310)
        Me.TextEditTDSE.Name = "TextEditTDSE"
        Me.TextEditTDSE.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditTDSE.Properties.Appearance.Options.UseFont = True
        Me.TextEditTDSE.Properties.Mask.EditMask = "######.##"
        Me.TextEditTDSE.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditTDSE.Properties.MaxLength = 9
        Me.TextEditTDSE.Size = New System.Drawing.Size(100, 20)
        Me.TextEditTDSE.TabIndex = 41
        '
        'GridLookUpEditFormulaD11
        '
        Me.GridLookUpEditFormulaD11.Location = New System.Drawing.Point(240, 310)
        Me.GridLookUpEditFormulaD11.Name = "GridLookUpEditFormulaD11"
        Me.GridLookUpEditFormulaD11.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD11.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD11.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD11.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD11.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD11.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD11.Properties.NullText = ""
        Me.GridLookUpEditFormulaD11.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD11.Properties.View = Me.GridView21
        Me.GridLookUpEditFormulaD11.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD11.TabIndex = 42
        '
        'PAYFORMULABindingSource
        '
        Me.PAYFORMULABindingSource.DataMember = "PAY_FORMULA"
        Me.PAYFORMULABindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView21
        '
        Me.GridView21.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn41, Me.GridColumn42})
        Me.GridView21.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView21.Name = "GridView21"
        Me.GridView21.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView21.OptionsView.ShowGroupPanel = False
        '
        'GridColumn41
        '
        Me.GridColumn41.Caption = "Code"
        Me.GridColumn41.FieldName = "CODE"
        Me.GridColumn41.Name = "GridColumn41"
        Me.GridColumn41.Visible = True
        Me.GridColumn41.VisibleIndex = 0
        '
        'GridColumn42
        '
        Me.GridColumn42.Caption = "Formula"
        Me.GridColumn42.FieldName = "FORM"
        Me.GridColumn42.Name = "GridColumn42"
        Me.GridColumn42.Visible = True
        Me.GridColumn42.VisibleIndex = 1
        '
        'TextEditRate10
        '
        Me.TextEditRate10.Location = New System.Drawing.Point(134, 284)
        Me.TextEditRate10.Name = "TextEditRate10"
        Me.TextEditRate10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate10.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate10.Properties.Mask.EditMask = "######.##"
        Me.TextEditRate10.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRate10.Properties.MaxLength = 9
        Me.TextEditRate10.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate10.TabIndex = 38
        '
        'TextEditDescp10
        '
        Me.TextEditDescp10.Location = New System.Drawing.Point(28, 284)
        Me.TextEditDescp10.Name = "TextEditDescp10"
        Me.TextEditDescp10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp10.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp10.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp10.TabIndex = 37
        '
        'TextEditRate9
        '
        Me.TextEditRate9.Location = New System.Drawing.Point(134, 258)
        Me.TextEditRate9.Name = "TextEditRate9"
        Me.TextEditRate9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate9.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate9.Properties.Mask.EditMask = "######.##"
        Me.TextEditRate9.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRate9.Properties.MaxLength = 9
        Me.TextEditRate9.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate9.TabIndex = 34
        '
        'TextEditDescp9
        '
        Me.TextEditDescp9.Location = New System.Drawing.Point(28, 258)
        Me.TextEditDescp9.Name = "TextEditDescp9"
        Me.TextEditDescp9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp9.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp9.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp9.TabIndex = 33
        '
        'TextEditRate8
        '
        Me.TextEditRate8.Location = New System.Drawing.Point(134, 232)
        Me.TextEditRate8.Name = "TextEditRate8"
        Me.TextEditRate8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate8.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate8.Properties.Mask.EditMask = "######.##"
        Me.TextEditRate8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRate8.Properties.MaxLength = 9
        Me.TextEditRate8.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate8.TabIndex = 30
        '
        'TextEditDescp8
        '
        Me.TextEditDescp8.Location = New System.Drawing.Point(28, 232)
        Me.TextEditDescp8.Name = "TextEditDescp8"
        Me.TextEditDescp8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp8.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp8.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp8.TabIndex = 29
        '
        'TextEditRate7
        '
        Me.TextEditRate7.Location = New System.Drawing.Point(134, 206)
        Me.TextEditRate7.Name = "TextEditRate7"
        Me.TextEditRate7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate7.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate7.Properties.Mask.EditMask = "######.##"
        Me.TextEditRate7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRate7.Properties.MaxLength = 9
        Me.TextEditRate7.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate7.TabIndex = 26
        '
        'TextEditDescp7
        '
        Me.TextEditDescp7.Location = New System.Drawing.Point(28, 206)
        Me.TextEditDescp7.Name = "TextEditDescp7"
        Me.TextEditDescp7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp7.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp7.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp7.TabIndex = 25
        '
        'TextEditRate6
        '
        Me.TextEditRate6.Location = New System.Drawing.Point(134, 180)
        Me.TextEditRate6.Name = "TextEditRate6"
        Me.TextEditRate6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate6.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate6.Properties.Mask.EditMask = "######.##"
        Me.TextEditRate6.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRate6.Properties.MaxLength = 9
        Me.TextEditRate6.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate6.TabIndex = 22
        '
        'TextEditDescp6
        '
        Me.TextEditDescp6.Location = New System.Drawing.Point(28, 180)
        Me.TextEditDescp6.Name = "TextEditDescp6"
        Me.TextEditDescp6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp6.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp6.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp6.TabIndex = 21
        '
        'TextEditRate5
        '
        Me.TextEditRate5.Location = New System.Drawing.Point(134, 154)
        Me.TextEditRate5.Name = "TextEditRate5"
        Me.TextEditRate5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate5.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate5.Properties.Mask.EditMask = "######.##"
        Me.TextEditRate5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRate5.Properties.MaxLength = 9
        Me.TextEditRate5.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate5.TabIndex = 18
        '
        'TextEditDescp5
        '
        Me.TextEditDescp5.Location = New System.Drawing.Point(28, 154)
        Me.TextEditDescp5.Name = "TextEditDescp5"
        Me.TextEditDescp5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp5.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp5.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp5.TabIndex = 17
        '
        'TextEditRate4
        '
        Me.TextEditRate4.Location = New System.Drawing.Point(134, 128)
        Me.TextEditRate4.Name = "TextEditRate4"
        Me.TextEditRate4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate4.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate4.Properties.Mask.EditMask = "######.##"
        Me.TextEditRate4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRate4.Properties.MaxLength = 9
        Me.TextEditRate4.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate4.TabIndex = 14
        '
        'TextEditDescp4
        '
        Me.TextEditDescp4.Location = New System.Drawing.Point(28, 128)
        Me.TextEditDescp4.Name = "TextEditDescp4"
        Me.TextEditDescp4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp4.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp4.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp4.TabIndex = 13
        '
        'TextEditRate3
        '
        Me.TextEditRate3.Location = New System.Drawing.Point(134, 102)
        Me.TextEditRate3.Name = "TextEditRate3"
        Me.TextEditRate3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate3.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate3.Properties.Mask.EditMask = "######.##"
        Me.TextEditRate3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRate3.Properties.MaxLength = 9
        Me.TextEditRate3.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate3.TabIndex = 10
        '
        'TextEditDescp3
        '
        Me.TextEditDescp3.Location = New System.Drawing.Point(28, 102)
        Me.TextEditDescp3.Name = "TextEditDescp3"
        Me.TextEditDescp3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp3.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp3.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp3.TabIndex = 9
        '
        'TextEditRate2
        '
        Me.TextEditRate2.Location = New System.Drawing.Point(134, 76)
        Me.TextEditRate2.Name = "TextEditRate2"
        Me.TextEditRate2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate2.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate2.Properties.Mask.EditMask = "######.##"
        Me.TextEditRate2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRate2.Properties.MaxLength = 9
        Me.TextEditRate2.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate2.TabIndex = 6
        '
        'TextEditDescp2
        '
        Me.TextEditDescp2.Location = New System.Drawing.Point(28, 76)
        Me.TextEditDescp2.Name = "TextEditDescp2"
        Me.TextEditDescp2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp2.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp2.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp2.TabIndex = 5
        '
        'TextEditRate1
        '
        Me.TextEditRate1.Location = New System.Drawing.Point(134, 50)
        Me.TextEditRate1.Name = "TextEditRate1"
        Me.TextEditRate1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate1.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate1.Properties.Mask.EditMask = "######.##"
        Me.TextEditRate1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRate1.Properties.MaxLength = 9
        Me.TextEditRate1.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate1.TabIndex = 2
        '
        'TextEditDescp1
        '
        Me.TextEditDescp1.Location = New System.Drawing.Point(28, 50)
        Me.TextEditDescp1.Name = "TextEditDescp1"
        Me.TextEditDescp1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp1.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp1.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp1.TabIndex = 1
        '
        'GridLookUpEditFormulaD10
        '
        Me.GridLookUpEditFormulaD10.Location = New System.Drawing.Point(240, 284)
        Me.GridLookUpEditFormulaD10.Name = "GridLookUpEditFormulaD10"
        Me.GridLookUpEditFormulaD10.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD10.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD10.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD10.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD10.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD10.Properties.NullText = ""
        Me.GridLookUpEditFormulaD10.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD10.Properties.View = Me.GridView11
        Me.GridLookUpEditFormulaD10.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD10.TabIndex = 39
        '
        'GridView11
        '
        Me.GridView11.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn21, Me.GridColumn22})
        Me.GridView11.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView11.Name = "GridView11"
        Me.GridView11.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView11.OptionsView.ShowGroupPanel = False
        '
        'GridColumn21
        '
        Me.GridColumn21.Caption = "Code"
        Me.GridColumn21.FieldName = "CODE"
        Me.GridColumn21.Name = "GridColumn21"
        Me.GridColumn21.Visible = True
        Me.GridColumn21.VisibleIndex = 0
        '
        'GridColumn22
        '
        Me.GridColumn22.Caption = "Formula"
        Me.GridColumn22.FieldName = "FORM"
        Me.GridColumn22.Name = "GridColumn22"
        Me.GridColumn22.Visible = True
        Me.GridColumn22.VisibleIndex = 1
        '
        'GridLookUpEditFormulaD9
        '
        Me.GridLookUpEditFormulaD9.Location = New System.Drawing.Point(240, 258)
        Me.GridLookUpEditFormulaD9.Name = "GridLookUpEditFormulaD9"
        Me.GridLookUpEditFormulaD9.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD9.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD9.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD9.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD9.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD9.Properties.NullText = ""
        Me.GridLookUpEditFormulaD9.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD9.Properties.View = Me.GridView12
        Me.GridLookUpEditFormulaD9.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD9.TabIndex = 35
        '
        'GridView12
        '
        Me.GridView12.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn23, Me.GridColumn24})
        Me.GridView12.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView12.Name = "GridView12"
        Me.GridView12.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView12.OptionsView.ShowGroupPanel = False
        '
        'GridColumn23
        '
        Me.GridColumn23.Caption = "Code"
        Me.GridColumn23.FieldName = "CODE"
        Me.GridColumn23.Name = "GridColumn23"
        Me.GridColumn23.Visible = True
        Me.GridColumn23.VisibleIndex = 0
        '
        'GridColumn24
        '
        Me.GridColumn24.Caption = "Formula"
        Me.GridColumn24.FieldName = "FORM"
        Me.GridColumn24.Name = "GridColumn24"
        Me.GridColumn24.Visible = True
        Me.GridColumn24.VisibleIndex = 1
        '
        'GridLookUpEditFormulaD8
        '
        Me.GridLookUpEditFormulaD8.Location = New System.Drawing.Point(240, 232)
        Me.GridLookUpEditFormulaD8.Name = "GridLookUpEditFormulaD8"
        Me.GridLookUpEditFormulaD8.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD8.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD8.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD8.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD8.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD8.Properties.NullText = ""
        Me.GridLookUpEditFormulaD8.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD8.Properties.View = Me.GridView13
        Me.GridLookUpEditFormulaD8.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD8.TabIndex = 31
        '
        'GridView13
        '
        Me.GridView13.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn25, Me.GridColumn26})
        Me.GridView13.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView13.Name = "GridView13"
        Me.GridView13.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView13.OptionsView.ShowGroupPanel = False
        '
        'GridColumn25
        '
        Me.GridColumn25.Caption = "Code"
        Me.GridColumn25.FieldName = "CODE"
        Me.GridColumn25.Name = "GridColumn25"
        Me.GridColumn25.Visible = True
        Me.GridColumn25.VisibleIndex = 0
        '
        'GridColumn26
        '
        Me.GridColumn26.Caption = "Formula"
        Me.GridColumn26.FieldName = "FORM"
        Me.GridColumn26.Name = "GridColumn26"
        Me.GridColumn26.Visible = True
        Me.GridColumn26.VisibleIndex = 1
        '
        'GridLookUpEditFormulaD7
        '
        Me.GridLookUpEditFormulaD7.Location = New System.Drawing.Point(240, 206)
        Me.GridLookUpEditFormulaD7.Name = "GridLookUpEditFormulaD7"
        Me.GridLookUpEditFormulaD7.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD7.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD7.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD7.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD7.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD7.Properties.NullText = ""
        Me.GridLookUpEditFormulaD7.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD7.Properties.View = Me.GridView14
        Me.GridLookUpEditFormulaD7.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD7.TabIndex = 27
        '
        'GridView14
        '
        Me.GridView14.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn27, Me.GridColumn28})
        Me.GridView14.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView14.Name = "GridView14"
        Me.GridView14.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView14.OptionsView.ShowGroupPanel = False
        '
        'GridColumn27
        '
        Me.GridColumn27.Caption = "Code"
        Me.GridColumn27.FieldName = "CODE"
        Me.GridColumn27.Name = "GridColumn27"
        Me.GridColumn27.Visible = True
        Me.GridColumn27.VisibleIndex = 0
        '
        'GridColumn28
        '
        Me.GridColumn28.Caption = "Formula"
        Me.GridColumn28.FieldName = "FORM"
        Me.GridColumn28.Name = "GridColumn28"
        Me.GridColumn28.Visible = True
        Me.GridColumn28.VisibleIndex = 1
        '
        'GridLookUpEditFormulaD6
        '
        Me.GridLookUpEditFormulaD6.Location = New System.Drawing.Point(240, 180)
        Me.GridLookUpEditFormulaD6.Name = "GridLookUpEditFormulaD6"
        Me.GridLookUpEditFormulaD6.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD6.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD6.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD6.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD6.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD6.Properties.NullText = ""
        Me.GridLookUpEditFormulaD6.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD6.Properties.View = Me.GridView15
        Me.GridLookUpEditFormulaD6.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD6.TabIndex = 23
        '
        'GridView15
        '
        Me.GridView15.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn29, Me.GridColumn30})
        Me.GridView15.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView15.Name = "GridView15"
        Me.GridView15.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView15.OptionsView.ShowGroupPanel = False
        '
        'GridColumn29
        '
        Me.GridColumn29.Caption = "Code"
        Me.GridColumn29.FieldName = "CODE"
        Me.GridColumn29.Name = "GridColumn29"
        Me.GridColumn29.Visible = True
        Me.GridColumn29.VisibleIndex = 0
        '
        'GridColumn30
        '
        Me.GridColumn30.Caption = "Formula"
        Me.GridColumn30.FieldName = "FORM"
        Me.GridColumn30.Name = "GridColumn30"
        Me.GridColumn30.Visible = True
        Me.GridColumn30.VisibleIndex = 1
        '
        'GridLookUpEditFormulaD5
        '
        Me.GridLookUpEditFormulaD5.Location = New System.Drawing.Point(240, 154)
        Me.GridLookUpEditFormulaD5.Name = "GridLookUpEditFormulaD5"
        Me.GridLookUpEditFormulaD5.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD5.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD5.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD5.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD5.Properties.NullText = ""
        Me.GridLookUpEditFormulaD5.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD5.Properties.View = Me.GridView16
        Me.GridLookUpEditFormulaD5.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD5.TabIndex = 19
        '
        'GridView16
        '
        Me.GridView16.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn31, Me.GridColumn32})
        Me.GridView16.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView16.Name = "GridView16"
        Me.GridView16.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView16.OptionsView.ShowGroupPanel = False
        '
        'GridColumn31
        '
        Me.GridColumn31.Caption = "Code"
        Me.GridColumn31.FieldName = "CODE"
        Me.GridColumn31.Name = "GridColumn31"
        Me.GridColumn31.Visible = True
        Me.GridColumn31.VisibleIndex = 0
        '
        'GridColumn32
        '
        Me.GridColumn32.Caption = "Formula"
        Me.GridColumn32.FieldName = "FORM"
        Me.GridColumn32.Name = "GridColumn32"
        Me.GridColumn32.Visible = True
        Me.GridColumn32.VisibleIndex = 1
        '
        'GridLookUpEditFormulaD4
        '
        Me.GridLookUpEditFormulaD4.Location = New System.Drawing.Point(240, 128)
        Me.GridLookUpEditFormulaD4.Name = "GridLookUpEditFormulaD4"
        Me.GridLookUpEditFormulaD4.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD4.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD4.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD4.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD4.Properties.NullText = ""
        Me.GridLookUpEditFormulaD4.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD4.Properties.View = Me.GridView17
        Me.GridLookUpEditFormulaD4.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD4.TabIndex = 15
        '
        'GridView17
        '
        Me.GridView17.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn33, Me.GridColumn34})
        Me.GridView17.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView17.Name = "GridView17"
        Me.GridView17.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView17.OptionsView.ShowGroupPanel = False
        '
        'GridColumn33
        '
        Me.GridColumn33.Caption = "Code"
        Me.GridColumn33.FieldName = "CODE"
        Me.GridColumn33.Name = "GridColumn33"
        Me.GridColumn33.Visible = True
        Me.GridColumn33.VisibleIndex = 0
        '
        'GridColumn34
        '
        Me.GridColumn34.Caption = "Formula"
        Me.GridColumn34.FieldName = "FORM"
        Me.GridColumn34.Name = "GridColumn34"
        Me.GridColumn34.Visible = True
        Me.GridColumn34.VisibleIndex = 1
        '
        'GridLookUpEditFormulaD3
        '
        Me.GridLookUpEditFormulaD3.Location = New System.Drawing.Point(240, 102)
        Me.GridLookUpEditFormulaD3.Name = "GridLookUpEditFormulaD3"
        Me.GridLookUpEditFormulaD3.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD3.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD3.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD3.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD3.Properties.NullText = ""
        Me.GridLookUpEditFormulaD3.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD3.Properties.View = Me.GridView18
        Me.GridLookUpEditFormulaD3.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD3.TabIndex = 11
        '
        'GridView18
        '
        Me.GridView18.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn35, Me.GridColumn36})
        Me.GridView18.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView18.Name = "GridView18"
        Me.GridView18.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView18.OptionsView.ShowGroupPanel = False
        '
        'GridColumn35
        '
        Me.GridColumn35.Caption = "Code"
        Me.GridColumn35.FieldName = "CODE"
        Me.GridColumn35.Name = "GridColumn35"
        Me.GridColumn35.Visible = True
        Me.GridColumn35.VisibleIndex = 0
        '
        'GridColumn36
        '
        Me.GridColumn36.Caption = "Formula"
        Me.GridColumn36.FieldName = "FORM"
        Me.GridColumn36.Name = "GridColumn36"
        Me.GridColumn36.Visible = True
        Me.GridColumn36.VisibleIndex = 1
        '
        'GridLookUpEditFormulaD2
        '
        Me.GridLookUpEditFormulaD2.Location = New System.Drawing.Point(240, 76)
        Me.GridLookUpEditFormulaD2.Name = "GridLookUpEditFormulaD2"
        Me.GridLookUpEditFormulaD2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD2.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD2.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD2.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD2.Properties.NullText = ""
        Me.GridLookUpEditFormulaD2.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD2.Properties.View = Me.GridView19
        Me.GridLookUpEditFormulaD2.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD2.TabIndex = 7
        '
        'GridView19
        '
        Me.GridView19.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn37, Me.GridColumn38})
        Me.GridView19.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView19.Name = "GridView19"
        Me.GridView19.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView19.OptionsView.ShowGroupPanel = False
        '
        'GridColumn37
        '
        Me.GridColumn37.Caption = "Code"
        Me.GridColumn37.FieldName = "CODE"
        Me.GridColumn37.Name = "GridColumn37"
        Me.GridColumn37.Visible = True
        Me.GridColumn37.VisibleIndex = 0
        '
        'GridColumn38
        '
        Me.GridColumn38.Caption = "Formula"
        Me.GridColumn38.FieldName = "FORM"
        Me.GridColumn38.Name = "GridColumn38"
        Me.GridColumn38.Visible = True
        Me.GridColumn38.VisibleIndex = 1
        '
        'GridLookUpEditFormulaD1
        '
        Me.GridLookUpEditFormulaD1.Location = New System.Drawing.Point(240, 50)
        Me.GridLookUpEditFormulaD1.Name = "GridLookUpEditFormulaD1"
        Me.GridLookUpEditFormulaD1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD1.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD1.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD1.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD1.Properties.NullText = ""
        Me.GridLookUpEditFormulaD1.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD1.Properties.View = Me.GridView20
        Me.GridLookUpEditFormulaD1.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD1.TabIndex = 3
        '
        'GridView20
        '
        Me.GridView20.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn39, Me.GridColumn40})
        Me.GridView20.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView20.Name = "GridView20"
        Me.GridView20.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView20.OptionsView.ShowGroupPanel = False
        '
        'GridColumn39
        '
        Me.GridColumn39.Caption = "Code"
        Me.GridColumn39.FieldName = "CODE"
        Me.GridColumn39.Name = "GridColumn39"
        Me.GridColumn39.Visible = True
        Me.GridColumn39.VisibleIndex = 0
        '
        'GridColumn40
        '
        Me.GridColumn40.Caption = "Formula"
        Me.GridColumn40.FieldName = "FORM"
        Me.GridColumn40.Name = "GridColumn40"
        Me.GridColumn40.Visible = True
        Me.GridColumn40.VisibleIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(8, 287)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(14, 14)
        Me.LabelControl1.TabIndex = 98
        Me.LabelControl1.Text = "10"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(8, 261)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl2.TabIndex = 95
        Me.LabelControl2.Text = "9"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(8, 235)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl3.TabIndex = 92
        Me.LabelControl3.Text = "8"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(8, 209)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl4.TabIndex = 89
        Me.LabelControl4.Text = "7"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(8, 183)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl5.TabIndex = 86
        Me.LabelControl5.Text = "6"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(8, 157)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl6.TabIndex = 78
        Me.LabelControl6.Text = "5"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(47, 30)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl7.TabIndex = 61
        Me.LabelControl7.Text = "Description"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(8, 131)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl8.TabIndex = 75
        Me.LabelControl8.Text = "4"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(150, 30)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(61, 14)
        Me.LabelControl9.TabIndex = 64
        Me.LabelControl9.Text = "Rate / Amt"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(256, 30)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(42, 14)
        Me.LabelControl10.TabIndex = 65
        Me.LabelControl10.Text = "Formula"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(8, 53)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl11.TabIndex = 66
        Me.LabelControl11.Text = "1"
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(8, 105)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl12.TabIndex = 72
        Me.LabelControl12.Text = "3"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(8, 79)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl13.TabIndex = 69
        Me.LabelControl13.Text = "2"
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GroupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl1.Controls.Add(Me.PictureEdit10)
        Me.GroupControl1.Controls.Add(Me.PictureEdit9)
        Me.GroupControl1.Controls.Add(Me.PictureEdit8)
        Me.GroupControl1.Controls.Add(Me.PictureEdit7)
        Me.GroupControl1.Controls.Add(Me.PictureEdit6)
        Me.GroupControl1.Controls.Add(Me.PictureEdit5)
        Me.GroupControl1.Controls.Add(Me.PictureEdit4)
        Me.GroupControl1.Controls.Add(Me.PictureEdit3)
        Me.GroupControl1.Controls.Add(Me.PictureEdit2)
        Me.GroupControl1.Controls.Add(Me.PictureEdit1)
        Me.GroupControl1.Controls.Add(Me.CheckEditOT)
        Me.GroupControl1.Controls.Add(Me.CheckEditMR)
        Me.GroupControl1.Controls.Add(Me.CheckEditCR)
        Me.GroupControl1.Controls.Add(Me.CheckEditDA)
        Me.GroupControl1.Controls.Add(Me.CheckEditHRA)
        Me.GroupControl1.Controls.Add(Me.CheckEditBR)
        Me.GroupControl1.Controls.Add(Me.CheckEditRndE10)
        Me.GroupControl1.Controls.Add(Me.CheckEditRndE9)
        Me.GroupControl1.Controls.Add(Me.CheckEditRndE8)
        Me.GroupControl1.Controls.Add(Me.CheckEditRndE7)
        Me.GroupControl1.Controls.Add(Me.CheckEditRndE6)
        Me.GroupControl1.Controls.Add(Me.CheckEditRndE5)
        Me.GroupControl1.Controls.Add(Me.CheckEditRndE4)
        Me.GroupControl1.Controls.Add(Me.CheckEditRndE3)
        Me.GroupControl1.Controls.Add(Me.CheckEditRndE2)
        Me.GroupControl1.Controls.Add(Me.LabelControl15)
        Me.GroupControl1.Controls.Add(Me.CheckEditRndE1)
        Me.GroupControl1.Controls.Add(Me.GridLookUpEditFormulaE10)
        Me.GroupControl1.Controls.Add(Me.GridLookUpEditFormulaE9)
        Me.GroupControl1.Controls.Add(Me.GridLookUpEditFormulaE8)
        Me.GroupControl1.Controls.Add(Me.GridLookUpEditFormulaE7)
        Me.GroupControl1.Controls.Add(Me.GridLookUpEditFormulaE6)
        Me.GroupControl1.Controls.Add(Me.GridLookUpEditFormulaE5)
        Me.GroupControl1.Controls.Add(Me.GridLookUpEditFormulaE4)
        Me.GroupControl1.Controls.Add(Me.GridLookUpEditFormulaE3)
        Me.GroupControl1.Controls.Add(Me.GridLookUpEditFormulaE2)
        Me.GroupControl1.Controls.Add(Me.GridLookUpEditFormulaE1)
        Me.GroupControl1.Controls.Add(Me.LabelControl46)
        Me.GroupControl1.Controls.Add(Me.TextEditRateE10)
        Me.GroupControl1.Controls.Add(Me.TextEditDescpE10)
        Me.GroupControl1.Controls.Add(Me.LabelControl47)
        Me.GroupControl1.Controls.Add(Me.TextEditRateE9)
        Me.GroupControl1.Controls.Add(Me.TextEditDescpE9)
        Me.GroupControl1.Controls.Add(Me.LabelControl48)
        Me.GroupControl1.Controls.Add(Me.TextEditRateE8)
        Me.GroupControl1.Controls.Add(Me.TextEditDescpE8)
        Me.GroupControl1.Controls.Add(Me.LabelControl49)
        Me.GroupControl1.Controls.Add(Me.TextEditRateE7)
        Me.GroupControl1.Controls.Add(Me.TextEditDescpE7)
        Me.GroupControl1.Controls.Add(Me.LabelControl50)
        Me.GroupControl1.Controls.Add(Me.TextEditRateE6)
        Me.GroupControl1.Controls.Add(Me.TextEditDescpE6)
        Me.GroupControl1.Controls.Add(Me.TextEditRateE5)
        Me.GroupControl1.Controls.Add(Me.LabelControl38)
        Me.GroupControl1.Controls.Add(Me.LabelControl45)
        Me.GroupControl1.Controls.Add(Me.TextEditDescpE1)
        Me.GroupControl1.Controls.Add(Me.TextEditDescpE5)
        Me.GroupControl1.Controls.Add(Me.TextEditRateE1)
        Me.GroupControl1.Controls.Add(Me.LabelControl39)
        Me.GroupControl1.Controls.Add(Me.LabelControl44)
        Me.GroupControl1.Controls.Add(Me.TextEditRateE4)
        Me.GroupControl1.Controls.Add(Me.LabelControl43)
        Me.GroupControl1.Controls.Add(Me.TextEditDescpE4)
        Me.GroupControl1.Controls.Add(Me.LabelControl42)
        Me.GroupControl1.Controls.Add(Me.LabelControl40)
        Me.GroupControl1.Controls.Add(Me.TextEditDescpE2)
        Me.GroupControl1.Controls.Add(Me.TextEditRateE3)
        Me.GroupControl1.Controls.Add(Me.TextEditRateE2)
        Me.GroupControl1.Controls.Add(Me.TextEditDescpE3)
        Me.GroupControl1.Controls.Add(Me.LabelControl41)
        Me.GroupControl1.Location = New System.Drawing.Point(4, 4)
        Me.GroupControl1.LookAndFeel.SkinName = "iMaginary"
        Me.GroupControl1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(418, 472)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Earnings"
        '
        'PictureEdit10
        '
        Me.PictureEdit10.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit10.EditValue = CType(resources.GetObject("PictureEdit10.EditValue"), Object)
        Me.PictureEdit10.Location = New System.Drawing.Point(323, 285)
        Me.PictureEdit10.Name = "PictureEdit10"
        Me.PictureEdit10.Properties.AllowFocused = False
        Me.PictureEdit10.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit10.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit10.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit10.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit10.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit10.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit10.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit10.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit10.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit10.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit10.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit10.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit10.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit10.TabIndex = 110
        '
        'PictureEdit9
        '
        Me.PictureEdit9.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit9.EditValue = CType(resources.GetObject("PictureEdit9.EditValue"), Object)
        Me.PictureEdit9.Location = New System.Drawing.Point(323, 259)
        Me.PictureEdit9.Name = "PictureEdit9"
        Me.PictureEdit9.Properties.AllowFocused = False
        Me.PictureEdit9.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit9.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit9.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit9.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit9.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit9.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit9.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit9.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit9.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit9.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit9.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit9.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit9.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit9.TabIndex = 109
        '
        'PictureEdit8
        '
        Me.PictureEdit8.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit8.EditValue = CType(resources.GetObject("PictureEdit8.EditValue"), Object)
        Me.PictureEdit8.Location = New System.Drawing.Point(323, 233)
        Me.PictureEdit8.Name = "PictureEdit8"
        Me.PictureEdit8.Properties.AllowFocused = False
        Me.PictureEdit8.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit8.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit8.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit8.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit8.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit8.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit8.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit8.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit8.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit8.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit8.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit8.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit8.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit8.TabIndex = 108
        '
        'PictureEdit7
        '
        Me.PictureEdit7.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit7.EditValue = CType(resources.GetObject("PictureEdit7.EditValue"), Object)
        Me.PictureEdit7.Location = New System.Drawing.Point(323, 207)
        Me.PictureEdit7.Name = "PictureEdit7"
        Me.PictureEdit7.Properties.AllowFocused = False
        Me.PictureEdit7.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit7.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit7.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit7.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit7.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit7.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit7.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit7.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit7.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit7.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit7.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit7.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit7.TabIndex = 107
        '
        'PictureEdit6
        '
        Me.PictureEdit6.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit6.EditValue = CType(resources.GetObject("PictureEdit6.EditValue"), Object)
        Me.PictureEdit6.Location = New System.Drawing.Point(323, 181)
        Me.PictureEdit6.Name = "PictureEdit6"
        Me.PictureEdit6.Properties.AllowFocused = False
        Me.PictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit6.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit6.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit6.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit6.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit6.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit6.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit6.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit6.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit6.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit6.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit6.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit6.TabIndex = 106
        '
        'PictureEdit5
        '
        Me.PictureEdit5.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit5.EditValue = CType(resources.GetObject("PictureEdit5.EditValue"), Object)
        Me.PictureEdit5.Location = New System.Drawing.Point(323, 154)
        Me.PictureEdit5.Name = "PictureEdit5"
        Me.PictureEdit5.Properties.AllowFocused = False
        Me.PictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit5.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit5.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit5.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit5.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit5.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit5.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit5.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit5.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit5.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit5.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit5.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit5.TabIndex = 105
        '
        'PictureEdit4
        '
        Me.PictureEdit4.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit4.EditValue = CType(resources.GetObject("PictureEdit4.EditValue"), Object)
        Me.PictureEdit4.Location = New System.Drawing.Point(323, 128)
        Me.PictureEdit4.Name = "PictureEdit4"
        Me.PictureEdit4.Properties.AllowFocused = False
        Me.PictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit4.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit4.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit4.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit4.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit4.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit4.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit4.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit4.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit4.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit4.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit4.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit4.TabIndex = 104
        '
        'PictureEdit3
        '
        Me.PictureEdit3.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit3.EditValue = CType(resources.GetObject("PictureEdit3.EditValue"), Object)
        Me.PictureEdit3.Location = New System.Drawing.Point(323, 102)
        Me.PictureEdit3.Name = "PictureEdit3"
        Me.PictureEdit3.Properties.AllowFocused = False
        Me.PictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit3.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit3.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit3.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit3.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit3.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit3.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit3.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit3.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit3.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit3.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit3.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit3.TabIndex = 103
        '
        'PictureEdit2
        '
        Me.PictureEdit2.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit2.EditValue = CType(resources.GetObject("PictureEdit2.EditValue"), Object)
        Me.PictureEdit2.Location = New System.Drawing.Point(323, 76)
        Me.PictureEdit2.Name = "PictureEdit2"
        Me.PictureEdit2.Properties.AllowFocused = False
        Me.PictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit2.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit2.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit2.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit2.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit2.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit2.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit2.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit2.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit2.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit2.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit2.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit2.TabIndex = 102
        '
        'PictureEdit1
        '
        Me.PictureEdit1.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit1.EditValue = CType(resources.GetObject("PictureEdit1.EditValue"), Object)
        Me.PictureEdit1.Location = New System.Drawing.Point(323, 50)
        Me.PictureEdit1.Name = "PictureEdit1"
        Me.PictureEdit1.Properties.AllowFocused = False
        Me.PictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit1.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit1.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit1.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit1.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit1.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit1.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit1.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit1.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit1.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit1.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit1.TabIndex = 101
        '
        'CheckEditOT
        '
        Me.CheckEditOT.Location = New System.Drawing.Point(159, 396)
        Me.CheckEditOT.Name = "CheckEditOT"
        Me.CheckEditOT.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditOT.Properties.Appearance.Options.UseFont = True
        Me.CheckEditOT.Properties.Caption = "OT Amount  Round"
        Me.CheckEditOT.Properties.DisplayValueChecked = "Y"
        Me.CheckEditOT.Properties.DisplayValueGrayed = "N"
        Me.CheckEditOT.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditOT.Properties.ValueChecked = "Y"
        Me.CheckEditOT.Properties.ValueGrayed = "N"
        Me.CheckEditOT.Properties.ValueUnchecked = "N"
        Me.CheckEditOT.Size = New System.Drawing.Size(139, 19)
        Me.CheckEditOT.TabIndex = 45
        '
        'CheckEditMR
        '
        Me.CheckEditMR.Location = New System.Drawing.Point(159, 371)
        Me.CheckEditMR.Name = "CheckEditMR"
        Me.CheckEditMR.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditMR.Properties.Appearance.Options.UseFont = True
        Me.CheckEditMR.Properties.Caption = "Medical  Round"
        Me.CheckEditMR.Properties.DisplayValueChecked = "Y"
        Me.CheckEditMR.Properties.DisplayValueGrayed = "N"
        Me.CheckEditMR.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditMR.Properties.ValueChecked = "Y"
        Me.CheckEditMR.Properties.ValueGrayed = "N"
        Me.CheckEditMR.Properties.ValueUnchecked = "N"
        Me.CheckEditMR.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditMR.TabIndex = 44
        '
        'CheckEditCR
        '
        Me.CheckEditCR.Location = New System.Drawing.Point(159, 346)
        Me.CheckEditCR.Name = "CheckEditCR"
        Me.CheckEditCR.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditCR.Properties.Appearance.Options.UseFont = True
        Me.CheckEditCR.Properties.Caption = "Conveyance Round"
        Me.CheckEditCR.Properties.DisplayValueChecked = "Y"
        Me.CheckEditCR.Properties.DisplayValueGrayed = "N"
        Me.CheckEditCR.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditCR.Properties.ValueChecked = "Y"
        Me.CheckEditCR.Properties.ValueGrayed = "N"
        Me.CheckEditCR.Properties.ValueUnchecked = "N"
        Me.CheckEditCR.Size = New System.Drawing.Size(139, 19)
        Me.CheckEditCR.TabIndex = 44
        '
        'CheckEditDA
        '
        Me.CheckEditDA.Location = New System.Drawing.Point(19, 396)
        Me.CheckEditDA.Name = "CheckEditDA"
        Me.CheckEditDA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditDA.Properties.Appearance.Options.UseFont = True
        Me.CheckEditDA.Properties.Caption = "DA Round"
        Me.CheckEditDA.Properties.DisplayValueChecked = "Y"
        Me.CheckEditDA.Properties.DisplayValueGrayed = "N"
        Me.CheckEditDA.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditDA.Properties.ValueChecked = "Y"
        Me.CheckEditDA.Properties.ValueGrayed = "N"
        Me.CheckEditDA.Properties.ValueUnchecked = "N"
        Me.CheckEditDA.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditDA.TabIndex = 43
        '
        'CheckEditHRA
        '
        Me.CheckEditHRA.Location = New System.Drawing.Point(19, 371)
        Me.CheckEditHRA.Name = "CheckEditHRA"
        Me.CheckEditHRA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditHRA.Properties.Appearance.Options.UseFont = True
        Me.CheckEditHRA.Properties.Caption = "HRA Round"
        Me.CheckEditHRA.Properties.DisplayValueChecked = "Y"
        Me.CheckEditHRA.Properties.DisplayValueGrayed = "N"
        Me.CheckEditHRA.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditHRA.Properties.ValueChecked = "Y"
        Me.CheckEditHRA.Properties.ValueGrayed = "N"
        Me.CheckEditHRA.Properties.ValueUnchecked = "N"
        Me.CheckEditHRA.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditHRA.TabIndex = 42
        '
        'CheckEditBR
        '
        Me.CheckEditBR.Location = New System.Drawing.Point(19, 346)
        Me.CheckEditBR.Name = "CheckEditBR"
        Me.CheckEditBR.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditBR.Properties.Appearance.Options.UseFont = True
        Me.CheckEditBR.Properties.Caption = "Basic Round"
        Me.CheckEditBR.Properties.DisplayValueChecked = "Y"
        Me.CheckEditBR.Properties.DisplayValueGrayed = "N"
        Me.CheckEditBR.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditBR.Properties.ValueChecked = "Y"
        Me.CheckEditBR.Properties.ValueGrayed = "N"
        Me.CheckEditBR.Properties.ValueUnchecked = "N"
        Me.CheckEditBR.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditBR.TabIndex = 41
        '
        'CheckEditRndE10
        '
        Me.CheckEditRndE10.Location = New System.Drawing.Point(356, 285)
        Me.CheckEditRndE10.Name = "CheckEditRndE10"
        Me.CheckEditRndE10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndE10.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndE10.Properties.Caption = ""
        Me.CheckEditRndE10.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndE10.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndE10.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndE10.Properties.ValueChecked = "Y"
        Me.CheckEditRndE10.Properties.ValueGrayed = "N"
        Me.CheckEditRndE10.Properties.ValueUnchecked = "N"
        Me.CheckEditRndE10.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndE10.TabIndex = 40
        '
        'CheckEditRndE9
        '
        Me.CheckEditRndE9.Location = New System.Drawing.Point(356, 259)
        Me.CheckEditRndE9.Name = "CheckEditRndE9"
        Me.CheckEditRndE9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndE9.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndE9.Properties.Caption = ""
        Me.CheckEditRndE9.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndE9.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndE9.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndE9.Properties.ValueChecked = "Y"
        Me.CheckEditRndE9.Properties.ValueGrayed = "N"
        Me.CheckEditRndE9.Properties.ValueUnchecked = "N"
        Me.CheckEditRndE9.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndE9.TabIndex = 36
        '
        'CheckEditRndE8
        '
        Me.CheckEditRndE8.Location = New System.Drawing.Point(356, 233)
        Me.CheckEditRndE8.Name = "CheckEditRndE8"
        Me.CheckEditRndE8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndE8.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndE8.Properties.Caption = ""
        Me.CheckEditRndE8.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndE8.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndE8.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndE8.Properties.ValueChecked = "Y"
        Me.CheckEditRndE8.Properties.ValueGrayed = "N"
        Me.CheckEditRndE8.Properties.ValueUnchecked = "N"
        Me.CheckEditRndE8.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndE8.TabIndex = 32
        '
        'CheckEditRndE7
        '
        Me.CheckEditRndE7.Location = New System.Drawing.Point(356, 207)
        Me.CheckEditRndE7.Name = "CheckEditRndE7"
        Me.CheckEditRndE7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndE7.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndE7.Properties.Caption = ""
        Me.CheckEditRndE7.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndE7.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndE7.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndE7.Properties.ValueChecked = "Y"
        Me.CheckEditRndE7.Properties.ValueGrayed = "N"
        Me.CheckEditRndE7.Properties.ValueUnchecked = "N"
        Me.CheckEditRndE7.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndE7.TabIndex = 28
        '
        'CheckEditRndE6
        '
        Me.CheckEditRndE6.Location = New System.Drawing.Point(356, 181)
        Me.CheckEditRndE6.Name = "CheckEditRndE6"
        Me.CheckEditRndE6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndE6.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndE6.Properties.Caption = ""
        Me.CheckEditRndE6.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndE6.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndE6.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndE6.Properties.ValueChecked = "Y"
        Me.CheckEditRndE6.Properties.ValueGrayed = "N"
        Me.CheckEditRndE6.Properties.ValueUnchecked = "N"
        Me.CheckEditRndE6.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndE6.TabIndex = 24
        '
        'CheckEditRndE5
        '
        Me.CheckEditRndE5.Location = New System.Drawing.Point(356, 155)
        Me.CheckEditRndE5.Name = "CheckEditRndE5"
        Me.CheckEditRndE5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndE5.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndE5.Properties.Caption = ""
        Me.CheckEditRndE5.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndE5.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndE5.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndE5.Properties.ValueChecked = "Y"
        Me.CheckEditRndE5.Properties.ValueGrayed = "N"
        Me.CheckEditRndE5.Properties.ValueUnchecked = "N"
        Me.CheckEditRndE5.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndE5.TabIndex = 20
        '
        'CheckEditRndE4
        '
        Me.CheckEditRndE4.Location = New System.Drawing.Point(356, 129)
        Me.CheckEditRndE4.Name = "CheckEditRndE4"
        Me.CheckEditRndE4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndE4.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndE4.Properties.Caption = ""
        Me.CheckEditRndE4.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndE4.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndE4.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndE4.Properties.ValueChecked = "Y"
        Me.CheckEditRndE4.Properties.ValueGrayed = "N"
        Me.CheckEditRndE4.Properties.ValueUnchecked = "N"
        Me.CheckEditRndE4.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndE4.TabIndex = 16
        '
        'CheckEditRndE3
        '
        Me.CheckEditRndE3.Location = New System.Drawing.Point(356, 103)
        Me.CheckEditRndE3.Name = "CheckEditRndE3"
        Me.CheckEditRndE3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndE3.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndE3.Properties.Caption = ""
        Me.CheckEditRndE3.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndE3.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndE3.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndE3.Properties.ValueChecked = "Y"
        Me.CheckEditRndE3.Properties.ValueGrayed = "N"
        Me.CheckEditRndE3.Properties.ValueUnchecked = "N"
        Me.CheckEditRndE3.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndE3.TabIndex = 12
        '
        'CheckEditRndE2
        '
        Me.CheckEditRndE2.Location = New System.Drawing.Point(356, 75)
        Me.CheckEditRndE2.Name = "CheckEditRndE2"
        Me.CheckEditRndE2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndE2.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndE2.Properties.Caption = ""
        Me.CheckEditRndE2.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndE2.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndE2.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndE2.Properties.ValueChecked = "Y"
        Me.CheckEditRndE2.Properties.ValueGrayed = "N"
        Me.CheckEditRndE2.Properties.ValueUnchecked = "N"
        Me.CheckEditRndE2.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndE2.TabIndex = 8
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(349, 30)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(35, 14)
        Me.LabelControl15.TabIndex = 100
        Me.LabelControl15.Text = "Round"
        '
        'CheckEditRndE1
        '
        Me.CheckEditRndE1.Location = New System.Drawing.Point(356, 48)
        Me.CheckEditRndE1.Name = "CheckEditRndE1"
        Me.CheckEditRndE1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditRndE1.Properties.Appearance.Options.UseFont = True
        Me.CheckEditRndE1.Properties.Caption = ""
        Me.CheckEditRndE1.Properties.DisplayValueChecked = "Y"
        Me.CheckEditRndE1.Properties.DisplayValueGrayed = "N"
        Me.CheckEditRndE1.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditRndE1.Properties.ValueChecked = "Y"
        Me.CheckEditRndE1.Properties.ValueGrayed = "N"
        Me.CheckEditRndE1.Properties.ValueUnchecked = "N"
        Me.CheckEditRndE1.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditRndE1.TabIndex = 4
        '
        'GridLookUpEditFormulaE10
        '
        Me.GridLookUpEditFormulaE10.Location = New System.Drawing.Point(240, 284)
        Me.GridLookUpEditFormulaE10.Name = "GridLookUpEditFormulaE10"
        Me.GridLookUpEditFormulaE10.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE10.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE10.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE10.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE10.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE10.Properties.NullText = ""
        Me.GridLookUpEditFormulaE10.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE10.Properties.View = Me.GridView10
        Me.GridLookUpEditFormulaE10.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE10.TabIndex = 39
        '
        'GridView10
        '
        Me.GridView10.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn19, Me.GridColumn20})
        Me.GridView10.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView10.Name = "GridView10"
        Me.GridView10.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView10.OptionsView.ShowGroupPanel = False
        '
        'GridColumn19
        '
        Me.GridColumn19.Caption = "Code"
        Me.GridColumn19.FieldName = "CODE"
        Me.GridColumn19.Name = "GridColumn19"
        Me.GridColumn19.Visible = True
        Me.GridColumn19.VisibleIndex = 0
        '
        'GridColumn20
        '
        Me.GridColumn20.Caption = "Formula"
        Me.GridColumn20.FieldName = "FORM"
        Me.GridColumn20.Name = "GridColumn20"
        Me.GridColumn20.Visible = True
        Me.GridColumn20.VisibleIndex = 1
        '
        'GridLookUpEditFormulaE9
        '
        Me.GridLookUpEditFormulaE9.Location = New System.Drawing.Point(240, 258)
        Me.GridLookUpEditFormulaE9.Name = "GridLookUpEditFormulaE9"
        Me.GridLookUpEditFormulaE9.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE9.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE9.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE9.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE9.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE9.Properties.NullText = ""
        Me.GridLookUpEditFormulaE9.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE9.Properties.View = Me.GridView9
        Me.GridLookUpEditFormulaE9.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE9.TabIndex = 35
        '
        'GridView9
        '
        Me.GridView9.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn17, Me.GridColumn18})
        Me.GridView9.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView9.Name = "GridView9"
        Me.GridView9.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView9.OptionsView.ShowGroupPanel = False
        '
        'GridColumn17
        '
        Me.GridColumn17.Caption = "Code"
        Me.GridColumn17.FieldName = "CODE"
        Me.GridColumn17.Name = "GridColumn17"
        Me.GridColumn17.Visible = True
        Me.GridColumn17.VisibleIndex = 0
        '
        'GridColumn18
        '
        Me.GridColumn18.Caption = "Formula"
        Me.GridColumn18.FieldName = "FORM"
        Me.GridColumn18.Name = "GridColumn18"
        Me.GridColumn18.Visible = True
        Me.GridColumn18.VisibleIndex = 1
        '
        'GridLookUpEditFormulaE8
        '
        Me.GridLookUpEditFormulaE8.Location = New System.Drawing.Point(240, 232)
        Me.GridLookUpEditFormulaE8.Name = "GridLookUpEditFormulaE8"
        Me.GridLookUpEditFormulaE8.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE8.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE8.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE8.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE8.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE8.Properties.NullText = ""
        Me.GridLookUpEditFormulaE8.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE8.Properties.View = Me.GridView8
        Me.GridLookUpEditFormulaE8.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE8.TabIndex = 31
        '
        'GridView8
        '
        Me.GridView8.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn15, Me.GridColumn16})
        Me.GridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView8.Name = "GridView8"
        Me.GridView8.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView8.OptionsView.ShowGroupPanel = False
        '
        'GridColumn15
        '
        Me.GridColumn15.Caption = "Code"
        Me.GridColumn15.FieldName = "CODE"
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.Visible = True
        Me.GridColumn15.VisibleIndex = 0
        '
        'GridColumn16
        '
        Me.GridColumn16.Caption = "Formula"
        Me.GridColumn16.FieldName = "FORM"
        Me.GridColumn16.Name = "GridColumn16"
        Me.GridColumn16.Visible = True
        Me.GridColumn16.VisibleIndex = 1
        '
        'GridLookUpEditFormulaE7
        '
        Me.GridLookUpEditFormulaE7.Location = New System.Drawing.Point(240, 206)
        Me.GridLookUpEditFormulaE7.Name = "GridLookUpEditFormulaE7"
        Me.GridLookUpEditFormulaE7.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE7.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE7.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE7.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE7.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE7.Properties.NullText = ""
        Me.GridLookUpEditFormulaE7.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE7.Properties.View = Me.GridView7
        Me.GridLookUpEditFormulaE7.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE7.TabIndex = 27
        '
        'GridView7
        '
        Me.GridView7.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn13, Me.GridColumn14})
        Me.GridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView7.Name = "GridView7"
        Me.GridView7.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView7.OptionsView.ShowGroupPanel = False
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "Code"
        Me.GridColumn13.FieldName = "CODE"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 0
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "Formula"
        Me.GridColumn14.FieldName = "FORM"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = True
        Me.GridColumn14.VisibleIndex = 1
        '
        'GridLookUpEditFormulaE6
        '
        Me.GridLookUpEditFormulaE6.Location = New System.Drawing.Point(240, 180)
        Me.GridLookUpEditFormulaE6.Name = "GridLookUpEditFormulaE6"
        Me.GridLookUpEditFormulaE6.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE6.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE6.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE6.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE6.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE6.Properties.NullText = ""
        Me.GridLookUpEditFormulaE6.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE6.Properties.View = Me.GridView6
        Me.GridLookUpEditFormulaE6.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE6.TabIndex = 23
        '
        'GridView6
        '
        Me.GridView6.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn11, Me.GridColumn12})
        Me.GridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView6.Name = "GridView6"
        Me.GridView6.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView6.OptionsView.ShowGroupPanel = False
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Code"
        Me.GridColumn11.FieldName = "CODE"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 0
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Formula"
        Me.GridColumn12.FieldName = "FORM"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 1
        '
        'GridLookUpEditFormulaE5
        '
        Me.GridLookUpEditFormulaE5.Location = New System.Drawing.Point(240, 154)
        Me.GridLookUpEditFormulaE5.Name = "GridLookUpEditFormulaE5"
        Me.GridLookUpEditFormulaE5.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE5.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE5.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE5.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE5.Properties.NullText = ""
        Me.GridLookUpEditFormulaE5.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE5.Properties.View = Me.GridView5
        Me.GridLookUpEditFormulaE5.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE5.TabIndex = 19
        '
        'GridView5
        '
        Me.GridView5.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn9, Me.GridColumn10})
        Me.GridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView5.Name = "GridView5"
        Me.GridView5.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView5.OptionsView.ShowGroupPanel = False
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Code"
        Me.GridColumn9.FieldName = "CODE"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 0
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Formula"
        Me.GridColumn10.FieldName = "FORM"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 1
        '
        'GridLookUpEditFormulaE4
        '
        Me.GridLookUpEditFormulaE4.Location = New System.Drawing.Point(240, 128)
        Me.GridLookUpEditFormulaE4.Name = "GridLookUpEditFormulaE4"
        Me.GridLookUpEditFormulaE4.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE4.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE4.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE4.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE4.Properties.NullText = ""
        Me.GridLookUpEditFormulaE4.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE4.Properties.View = Me.GridView4
        Me.GridLookUpEditFormulaE4.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE4.TabIndex = 15
        '
        'GridView4
        '
        Me.GridView4.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn7, Me.GridColumn8})
        Me.GridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView4.OptionsView.ShowGroupPanel = False
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Code"
        Me.GridColumn7.FieldName = "CODE"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 0
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Fromula"
        Me.GridColumn8.FieldName = "FORM"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 1
        '
        'GridLookUpEditFormulaE3
        '
        Me.GridLookUpEditFormulaE3.Location = New System.Drawing.Point(240, 102)
        Me.GridLookUpEditFormulaE3.Name = "GridLookUpEditFormulaE3"
        Me.GridLookUpEditFormulaE3.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE3.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE3.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE3.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE3.Properties.NullText = ""
        Me.GridLookUpEditFormulaE3.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE3.Properties.View = Me.GridView3
        Me.GridLookUpEditFormulaE3.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE3.TabIndex = 11
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn5, Me.GridColumn6})
        Me.GridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Code"
        Me.GridColumn5.FieldName = "CODE"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 0
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Formula"
        Me.GridColumn6.FieldName = "FORM"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 1
        '
        'GridLookUpEditFormulaE2
        '
        Me.GridLookUpEditFormulaE2.Location = New System.Drawing.Point(240, 76)
        Me.GridLookUpEditFormulaE2.Name = "GridLookUpEditFormulaE2"
        Me.GridLookUpEditFormulaE2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE2.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE2.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE2.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE2.Properties.NullText = ""
        Me.GridLookUpEditFormulaE2.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE2.Properties.View = Me.GridView2
        Me.GridLookUpEditFormulaE2.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE2.TabIndex = 7
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn3, Me.GridColumn4})
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Code"
        Me.GridColumn3.FieldName = "CODE"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 0
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Formula"
        Me.GridColumn4.FieldName = "FORM"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 1
        '
        'GridLookUpEditFormulaE1
        '
        Me.GridLookUpEditFormulaE1.Location = New System.Drawing.Point(240, 50)
        Me.GridLookUpEditFormulaE1.Name = "GridLookUpEditFormulaE1"
        Me.GridLookUpEditFormulaE1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE1.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE1.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE1.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE1.Properties.NullText = ""
        Me.GridLookUpEditFormulaE1.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE1.Properties.View = Me.GridView1
        Me.GridLookUpEditFormulaE1.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE1.TabIndex = 3
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Code"
        Me.GridColumn1.FieldName = "CODE"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Formula"
        Me.GridColumn2.FieldName = "FORM"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        '
        'LabelControl46
        '
        Me.LabelControl46.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl46.Appearance.Options.UseFont = True
        Me.LabelControl46.Location = New System.Drawing.Point(8, 287)
        Me.LabelControl46.Name = "LabelControl46"
        Me.LabelControl46.Size = New System.Drawing.Size(14, 14)
        Me.LabelControl46.TabIndex = 98
        Me.LabelControl46.Text = "10"
        '
        'TextEditRateE10
        '
        Me.TextEditRateE10.Location = New System.Drawing.Point(134, 284)
        Me.TextEditRateE10.Name = "TextEditRateE10"
        Me.TextEditRateE10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE10.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE10.Properties.Mask.EditMask = "######.##"
        Me.TextEditRateE10.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRateE10.Properties.MaxLength = 9
        Me.TextEditRateE10.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE10.TabIndex = 38
        '
        'TextEditDescpE10
        '
        Me.TextEditDescpE10.Location = New System.Drawing.Point(28, 284)
        Me.TextEditDescpE10.Name = "TextEditDescpE10"
        Me.TextEditDescpE10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE10.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE10.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE10.TabIndex = 37
        '
        'LabelControl47
        '
        Me.LabelControl47.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl47.Appearance.Options.UseFont = True
        Me.LabelControl47.Location = New System.Drawing.Point(8, 261)
        Me.LabelControl47.Name = "LabelControl47"
        Me.LabelControl47.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl47.TabIndex = 95
        Me.LabelControl47.Text = "9"
        '
        'TextEditRateE9
        '
        Me.TextEditRateE9.Location = New System.Drawing.Point(134, 258)
        Me.TextEditRateE9.Name = "TextEditRateE9"
        Me.TextEditRateE9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE9.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE9.Properties.Mask.EditMask = "######.##"
        Me.TextEditRateE9.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRateE9.Properties.MaxLength = 9
        Me.TextEditRateE9.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE9.TabIndex = 34
        '
        'TextEditDescpE9
        '
        Me.TextEditDescpE9.Location = New System.Drawing.Point(28, 258)
        Me.TextEditDescpE9.Name = "TextEditDescpE9"
        Me.TextEditDescpE9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE9.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE9.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE9.TabIndex = 33
        '
        'LabelControl48
        '
        Me.LabelControl48.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl48.Appearance.Options.UseFont = True
        Me.LabelControl48.Location = New System.Drawing.Point(8, 235)
        Me.LabelControl48.Name = "LabelControl48"
        Me.LabelControl48.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl48.TabIndex = 92
        Me.LabelControl48.Text = "8"
        '
        'TextEditRateE8
        '
        Me.TextEditRateE8.Location = New System.Drawing.Point(134, 232)
        Me.TextEditRateE8.Name = "TextEditRateE8"
        Me.TextEditRateE8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE8.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE8.Properties.Mask.EditMask = "######.##"
        Me.TextEditRateE8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRateE8.Properties.MaxLength = 9
        Me.TextEditRateE8.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE8.TabIndex = 30
        '
        'TextEditDescpE8
        '
        Me.TextEditDescpE8.Location = New System.Drawing.Point(28, 232)
        Me.TextEditDescpE8.Name = "TextEditDescpE8"
        Me.TextEditDescpE8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE8.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE8.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE8.TabIndex = 29
        '
        'LabelControl49
        '
        Me.LabelControl49.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl49.Appearance.Options.UseFont = True
        Me.LabelControl49.Location = New System.Drawing.Point(8, 209)
        Me.LabelControl49.Name = "LabelControl49"
        Me.LabelControl49.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl49.TabIndex = 89
        Me.LabelControl49.Text = "7"
        '
        'TextEditRateE7
        '
        Me.TextEditRateE7.Location = New System.Drawing.Point(134, 206)
        Me.TextEditRateE7.Name = "TextEditRateE7"
        Me.TextEditRateE7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE7.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE7.Properties.Mask.EditMask = "######.##"
        Me.TextEditRateE7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRateE7.Properties.MaxLength = 9
        Me.TextEditRateE7.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE7.TabIndex = 26
        '
        'TextEditDescpE7
        '
        Me.TextEditDescpE7.Location = New System.Drawing.Point(28, 206)
        Me.TextEditDescpE7.Name = "TextEditDescpE7"
        Me.TextEditDescpE7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE7.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE7.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE7.TabIndex = 25
        '
        'LabelControl50
        '
        Me.LabelControl50.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl50.Appearance.Options.UseFont = True
        Me.LabelControl50.Location = New System.Drawing.Point(8, 183)
        Me.LabelControl50.Name = "LabelControl50"
        Me.LabelControl50.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl50.TabIndex = 86
        Me.LabelControl50.Text = "6"
        '
        'TextEditRateE6
        '
        Me.TextEditRateE6.Location = New System.Drawing.Point(134, 180)
        Me.TextEditRateE6.Name = "TextEditRateE6"
        Me.TextEditRateE6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE6.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE6.Properties.Mask.EditMask = "######.##"
        Me.TextEditRateE6.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRateE6.Properties.MaxLength = 9
        Me.TextEditRateE6.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE6.TabIndex = 22
        '
        'TextEditDescpE6
        '
        Me.TextEditDescpE6.Location = New System.Drawing.Point(28, 180)
        Me.TextEditDescpE6.Name = "TextEditDescpE6"
        Me.TextEditDescpE6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE6.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE6.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE6.TabIndex = 21
        '
        'TextEditRateE5
        '
        Me.TextEditRateE5.Location = New System.Drawing.Point(134, 154)
        Me.TextEditRateE5.Name = "TextEditRateE5"
        Me.TextEditRateE5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE5.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE5.Properties.Mask.EditMask = "######.##"
        Me.TextEditRateE5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRateE5.Properties.MaxLength = 9
        Me.TextEditRateE5.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE5.TabIndex = 18
        '
        'LabelControl38
        '
        Me.LabelControl38.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl38.Appearance.Options.UseFont = True
        Me.LabelControl38.Location = New System.Drawing.Point(8, 157)
        Me.LabelControl38.Name = "LabelControl38"
        Me.LabelControl38.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl38.TabIndex = 78
        Me.LabelControl38.Text = "5"
        '
        'LabelControl45
        '
        Me.LabelControl45.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl45.Appearance.Options.UseFont = True
        Me.LabelControl45.Location = New System.Drawing.Point(47, 30)
        Me.LabelControl45.Name = "LabelControl45"
        Me.LabelControl45.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl45.TabIndex = 61
        Me.LabelControl45.Text = "Description"
        '
        'TextEditDescpE1
        '
        Me.TextEditDescpE1.Location = New System.Drawing.Point(28, 50)
        Me.TextEditDescpE1.Name = "TextEditDescpE1"
        Me.TextEditDescpE1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE1.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE1.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE1.TabIndex = 1
        '
        'TextEditDescpE5
        '
        Me.TextEditDescpE5.Location = New System.Drawing.Point(28, 154)
        Me.TextEditDescpE5.Name = "TextEditDescpE5"
        Me.TextEditDescpE5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE5.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE5.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE5.TabIndex = 17
        '
        'TextEditRateE1
        '
        Me.TextEditRateE1.Location = New System.Drawing.Point(134, 50)
        Me.TextEditRateE1.Name = "TextEditRateE1"
        Me.TextEditRateE1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE1.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE1.Properties.Mask.EditMask = "######.##"
        Me.TextEditRateE1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRateE1.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditRateE1.Properties.MaxLength = 9
        Me.TextEditRateE1.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE1.TabIndex = 2
        '
        'LabelControl39
        '
        Me.LabelControl39.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl39.Appearance.Options.UseFont = True
        Me.LabelControl39.Location = New System.Drawing.Point(8, 131)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl39.TabIndex = 75
        Me.LabelControl39.Text = "4"
        '
        'LabelControl44
        '
        Me.LabelControl44.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl44.Appearance.Options.UseFont = True
        Me.LabelControl44.Location = New System.Drawing.Point(150, 30)
        Me.LabelControl44.Name = "LabelControl44"
        Me.LabelControl44.Size = New System.Drawing.Size(61, 14)
        Me.LabelControl44.TabIndex = 64
        Me.LabelControl44.Text = "Rate / Amt"
        '
        'TextEditRateE4
        '
        Me.TextEditRateE4.Location = New System.Drawing.Point(134, 128)
        Me.TextEditRateE4.Name = "TextEditRateE4"
        Me.TextEditRateE4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE4.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE4.Properties.Mask.EditMask = "######.##"
        Me.TextEditRateE4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRateE4.Properties.MaxLength = 9
        Me.TextEditRateE4.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE4.TabIndex = 14
        '
        'LabelControl43
        '
        Me.LabelControl43.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl43.Appearance.Options.UseFont = True
        Me.LabelControl43.Location = New System.Drawing.Point(256, 30)
        Me.LabelControl43.Name = "LabelControl43"
        Me.LabelControl43.Size = New System.Drawing.Size(42, 14)
        Me.LabelControl43.TabIndex = 65
        Me.LabelControl43.Text = "Formula"
        '
        'TextEditDescpE4
        '
        Me.TextEditDescpE4.Location = New System.Drawing.Point(28, 128)
        Me.TextEditDescpE4.Name = "TextEditDescpE4"
        Me.TextEditDescpE4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE4.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE4.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE4.TabIndex = 13
        '
        'LabelControl42
        '
        Me.LabelControl42.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl42.Appearance.Options.UseFont = True
        Me.LabelControl42.Location = New System.Drawing.Point(8, 53)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl42.TabIndex = 66
        Me.LabelControl42.Text = "1"
        '
        'LabelControl40
        '
        Me.LabelControl40.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl40.Appearance.Options.UseFont = True
        Me.LabelControl40.Location = New System.Drawing.Point(8, 105)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl40.TabIndex = 72
        Me.LabelControl40.Text = "3"
        '
        'TextEditDescpE2
        '
        Me.TextEditDescpE2.Location = New System.Drawing.Point(28, 76)
        Me.TextEditDescpE2.Name = "TextEditDescpE2"
        Me.TextEditDescpE2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE2.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE2.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE2.TabIndex = 5
        '
        'TextEditRateE3
        '
        Me.TextEditRateE3.Location = New System.Drawing.Point(134, 102)
        Me.TextEditRateE3.Name = "TextEditRateE3"
        Me.TextEditRateE3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE3.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE3.Properties.Mask.EditMask = "######.##"
        Me.TextEditRateE3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRateE3.Properties.MaxLength = 9
        Me.TextEditRateE3.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE3.TabIndex = 10
        '
        'TextEditRateE2
        '
        Me.TextEditRateE2.Location = New System.Drawing.Point(134, 76)
        Me.TextEditRateE2.Name = "TextEditRateE2"
        Me.TextEditRateE2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE2.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE2.Properties.Mask.EditMask = "######.##"
        Me.TextEditRateE2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditRateE2.Properties.MaxLength = 9
        Me.TextEditRateE2.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE2.TabIndex = 6
        '
        'TextEditDescpE3
        '
        Me.TextEditDescpE3.Location = New System.Drawing.Point(28, 102)
        Me.TextEditDescpE3.Name = "TextEditDescpE3"
        Me.TextEditDescpE3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE3.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE3.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE3.TabIndex = 9
        '
        'LabelControl41
        '
        Me.LabelControl41.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl41.Appearance.Options.UseFont = True
        Me.LabelControl41.Location = New System.Drawing.Point(8, 79)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl41.TabIndex = 69
        Me.LabelControl41.Text = "2"
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.CheckEditPFE10)
        Me.XtraTabPage2.Controls.Add(Me.CheckEditPFE8)
        Me.XtraTabPage2.Controls.Add(Me.CheckEditPFE6)
        Me.XtraTabPage2.Controls.Add(Me.CheckEditPFE4)
        Me.XtraTabPage2.Controls.Add(Me.CheckEditPFE2)
        Me.XtraTabPage2.Controls.Add(Me.CheckEditPFE9)
        Me.XtraTabPage2.Controls.Add(Me.CheckEditPFE7)
        Me.XtraTabPage2.Controls.Add(Me.CheckEditPFE5)
        Me.XtraTabPage2.Controls.Add(Me.CheckEditPFE3)
        Me.XtraTabPage2.Controls.Add(Me.CheckEditPFE1)
        Me.XtraTabPage2.Controls.Add(Me.CheckEditPFMedical)
        Me.XtraTabPage2.Controls.Add(Me.CheckEditPFConv)
        Me.XtraTabPage2.Controls.Add(Me.CheckEditPFDA)
        Me.XtraTabPage2.Controls.Add(Me.CheckEditPFHRA)
        Me.XtraTabPage2.Controls.Add(Me.CheckEditPFBasic)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl59)
        Me.XtraTabPage2.Controls.Add(Me.TextEditPF22)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl58)
        Me.XtraTabPage2.Controls.Add(Me.TextEditPF21)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl57)
        Me.XtraTabPage2.Controls.Add(Me.TextEditPF02)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl56)
        Me.XtraTabPage2.Controls.Add(Me.TextEditPFRound)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl55)
        Me.XtraTabPage2.Controls.Add(Me.TextEditPFVPFDec)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl54)
        Me.XtraTabPage2.Controls.Add(Me.TextEditPFPFDec)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl53)
        Me.XtraTabPage2.Controls.Add(Me.TextEditPFFPRDec)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl52)
        Me.XtraTabPage2.Controls.Add(Me.TextEditPFEPRDec)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl51)
        Me.XtraTabPage2.Controls.Add(Me.TextEditPFPRErDec)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl37)
        Me.XtraTabPage2.Controls.Add(Me.TextEditPFLimite)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl36)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.PageVisible = False
        Me.XtraTabPage2.Size = New System.Drawing.Size(1028, 504)
        Me.XtraTabPage2.Text = "PF Setup"
        '
        'CheckEditPFE10
        '
        Me.CheckEditPFE10.Location = New System.Drawing.Point(623, 257)
        Me.CheckEditPFE10.Name = "CheckEditPFE10"
        Me.CheckEditPFE10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPFE10.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPFE10.Properties.Caption = "Earning-10"
        Me.CheckEditPFE10.Properties.DisplayValueChecked = "1"
        Me.CheckEditPFE10.Properties.DisplayValueGrayed = "0"
        Me.CheckEditPFE10.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditPFE10.Properties.ValueChecked = "1"
        Me.CheckEditPFE10.Properties.ValueGrayed = "0"
        Me.CheckEditPFE10.Properties.ValueUnchecked = "0"
        Me.CheckEditPFE10.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditPFE10.TabIndex = 97
        '
        'CheckEditPFE8
        '
        Me.CheckEditPFE8.Location = New System.Drawing.Point(623, 232)
        Me.CheckEditPFE8.Name = "CheckEditPFE8"
        Me.CheckEditPFE8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPFE8.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPFE8.Properties.Caption = "Earning-08"
        Me.CheckEditPFE8.Properties.DisplayValueChecked = "1"
        Me.CheckEditPFE8.Properties.DisplayValueGrayed = "0"
        Me.CheckEditPFE8.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditPFE8.Properties.ValueChecked = "1"
        Me.CheckEditPFE8.Properties.ValueGrayed = "0"
        Me.CheckEditPFE8.Properties.ValueUnchecked = "0"
        Me.CheckEditPFE8.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditPFE8.TabIndex = 95
        '
        'CheckEditPFE6
        '
        Me.CheckEditPFE6.Location = New System.Drawing.Point(623, 207)
        Me.CheckEditPFE6.Name = "CheckEditPFE6"
        Me.CheckEditPFE6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPFE6.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPFE6.Properties.Caption = "Earning-06"
        Me.CheckEditPFE6.Properties.DisplayValueChecked = "1"
        Me.CheckEditPFE6.Properties.DisplayValueGrayed = "0"
        Me.CheckEditPFE6.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditPFE6.Properties.ValueChecked = "1"
        Me.CheckEditPFE6.Properties.ValueGrayed = "0"
        Me.CheckEditPFE6.Properties.ValueUnchecked = "0"
        Me.CheckEditPFE6.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditPFE6.TabIndex = 93
        '
        'CheckEditPFE4
        '
        Me.CheckEditPFE4.Location = New System.Drawing.Point(623, 182)
        Me.CheckEditPFE4.Name = "CheckEditPFE4"
        Me.CheckEditPFE4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPFE4.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPFE4.Properties.Caption = "Earning-04"
        Me.CheckEditPFE4.Properties.DisplayValueChecked = "1"
        Me.CheckEditPFE4.Properties.DisplayValueGrayed = "0"
        Me.CheckEditPFE4.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditPFE4.Properties.ValueChecked = "1"
        Me.CheckEditPFE4.Properties.ValueGrayed = "0"
        Me.CheckEditPFE4.Properties.ValueUnchecked = "0"
        Me.CheckEditPFE4.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditPFE4.TabIndex = 91
        '
        'CheckEditPFE2
        '
        Me.CheckEditPFE2.Location = New System.Drawing.Point(623, 157)
        Me.CheckEditPFE2.Name = "CheckEditPFE2"
        Me.CheckEditPFE2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPFE2.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPFE2.Properties.Caption = "Earning-02"
        Me.CheckEditPFE2.Properties.DisplayValueChecked = "1"
        Me.CheckEditPFE2.Properties.DisplayValueGrayed = "0"
        Me.CheckEditPFE2.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditPFE2.Properties.ValueChecked = "1"
        Me.CheckEditPFE2.Properties.ValueGrayed = "0"
        Me.CheckEditPFE2.Properties.ValueUnchecked = "0"
        Me.CheckEditPFE2.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditPFE2.TabIndex = 89
        '
        'CheckEditPFE9
        '
        Me.CheckEditPFE9.Location = New System.Drawing.Point(508, 257)
        Me.CheckEditPFE9.Name = "CheckEditPFE9"
        Me.CheckEditPFE9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPFE9.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPFE9.Properties.Caption = "Earning-09"
        Me.CheckEditPFE9.Properties.DisplayValueChecked = "1"
        Me.CheckEditPFE9.Properties.DisplayValueGrayed = "0"
        Me.CheckEditPFE9.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditPFE9.Properties.ValueChecked = "1"
        Me.CheckEditPFE9.Properties.ValueGrayed = "0"
        Me.CheckEditPFE9.Properties.ValueUnchecked = "0"
        Me.CheckEditPFE9.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditPFE9.TabIndex = 96
        '
        'CheckEditPFE7
        '
        Me.CheckEditPFE7.Location = New System.Drawing.Point(508, 232)
        Me.CheckEditPFE7.Name = "CheckEditPFE7"
        Me.CheckEditPFE7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPFE7.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPFE7.Properties.Caption = "Earning-07"
        Me.CheckEditPFE7.Properties.DisplayValueChecked = "1"
        Me.CheckEditPFE7.Properties.DisplayValueGrayed = "0"
        Me.CheckEditPFE7.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditPFE7.Properties.ValueChecked = "1"
        Me.CheckEditPFE7.Properties.ValueGrayed = "0"
        Me.CheckEditPFE7.Properties.ValueUnchecked = "0"
        Me.CheckEditPFE7.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditPFE7.TabIndex = 94
        '
        'CheckEditPFE5
        '
        Me.CheckEditPFE5.Location = New System.Drawing.Point(508, 207)
        Me.CheckEditPFE5.Name = "CheckEditPFE5"
        Me.CheckEditPFE5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPFE5.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPFE5.Properties.Caption = "Earning-05"
        Me.CheckEditPFE5.Properties.DisplayValueChecked = "1"
        Me.CheckEditPFE5.Properties.DisplayValueGrayed = "0"
        Me.CheckEditPFE5.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditPFE5.Properties.ValueChecked = "1"
        Me.CheckEditPFE5.Properties.ValueGrayed = "0"
        Me.CheckEditPFE5.Properties.ValueUnchecked = "0"
        Me.CheckEditPFE5.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditPFE5.TabIndex = 92
        '
        'CheckEditPFE3
        '
        Me.CheckEditPFE3.Location = New System.Drawing.Point(508, 182)
        Me.CheckEditPFE3.Name = "CheckEditPFE3"
        Me.CheckEditPFE3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPFE3.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPFE3.Properties.Caption = "Earning-03"
        Me.CheckEditPFE3.Properties.DisplayValueChecked = "1"
        Me.CheckEditPFE3.Properties.DisplayValueGrayed = "0"
        Me.CheckEditPFE3.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditPFE3.Properties.ValueChecked = "1"
        Me.CheckEditPFE3.Properties.ValueGrayed = "0"
        Me.CheckEditPFE3.Properties.ValueUnchecked = "0"
        Me.CheckEditPFE3.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditPFE3.TabIndex = 90
        '
        'CheckEditPFE1
        '
        Me.CheckEditPFE1.Location = New System.Drawing.Point(508, 157)
        Me.CheckEditPFE1.Name = "CheckEditPFE1"
        Me.CheckEditPFE1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPFE1.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPFE1.Properties.Caption = "Earning-01"
        Me.CheckEditPFE1.Properties.DisplayValueChecked = "1"
        Me.CheckEditPFE1.Properties.DisplayValueGrayed = "0"
        Me.CheckEditPFE1.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditPFE1.Properties.ValueChecked = "1"
        Me.CheckEditPFE1.Properties.ValueGrayed = "0"
        Me.CheckEditPFE1.Properties.ValueUnchecked = "0"
        Me.CheckEditPFE1.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditPFE1.TabIndex = 88
        '
        'CheckEditPFMedical
        '
        Me.CheckEditPFMedical.Location = New System.Drawing.Point(508, 132)
        Me.CheckEditPFMedical.Name = "CheckEditPFMedical"
        Me.CheckEditPFMedical.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPFMedical.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPFMedical.Properties.Caption = "Medical"
        Me.CheckEditPFMedical.Properties.DisplayValueChecked = "1"
        Me.CheckEditPFMedical.Properties.DisplayValueGrayed = "0"
        Me.CheckEditPFMedical.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditPFMedical.Properties.ValueChecked = "1"
        Me.CheckEditPFMedical.Properties.ValueGrayed = "0"
        Me.CheckEditPFMedical.Properties.ValueUnchecked = "0"
        Me.CheckEditPFMedical.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditPFMedical.TabIndex = 87
        '
        'CheckEditPFConv
        '
        Me.CheckEditPFConv.Location = New System.Drawing.Point(508, 107)
        Me.CheckEditPFConv.Name = "CheckEditPFConv"
        Me.CheckEditPFConv.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPFConv.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPFConv.Properties.Caption = "Conveyance"
        Me.CheckEditPFConv.Properties.DisplayValueChecked = "1"
        Me.CheckEditPFConv.Properties.DisplayValueGrayed = "0"
        Me.CheckEditPFConv.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditPFConv.Properties.ValueChecked = "1"
        Me.CheckEditPFConv.Properties.ValueGrayed = "0"
        Me.CheckEditPFConv.Properties.ValueUnchecked = "0"
        Me.CheckEditPFConv.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditPFConv.TabIndex = 86
        '
        'CheckEditPFDA
        '
        Me.CheckEditPFDA.Location = New System.Drawing.Point(508, 82)
        Me.CheckEditPFDA.Name = "CheckEditPFDA"
        Me.CheckEditPFDA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPFDA.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPFDA.Properties.Caption = "DA"
        Me.CheckEditPFDA.Properties.DisplayValueChecked = "1"
        Me.CheckEditPFDA.Properties.DisplayValueGrayed = "0"
        Me.CheckEditPFDA.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditPFDA.Properties.ValueChecked = "1"
        Me.CheckEditPFDA.Properties.ValueGrayed = "0"
        Me.CheckEditPFDA.Properties.ValueUnchecked = "0"
        Me.CheckEditPFDA.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditPFDA.TabIndex = 85
        '
        'CheckEditPFHRA
        '
        Me.CheckEditPFHRA.Location = New System.Drawing.Point(508, 57)
        Me.CheckEditPFHRA.Name = "CheckEditPFHRA"
        Me.CheckEditPFHRA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPFHRA.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPFHRA.Properties.Caption = "HRA"
        Me.CheckEditPFHRA.Properties.DisplayValueChecked = "1"
        Me.CheckEditPFHRA.Properties.DisplayValueGrayed = "0"
        Me.CheckEditPFHRA.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditPFHRA.Properties.ValueChecked = "1"
        Me.CheckEditPFHRA.Properties.ValueGrayed = "0"
        Me.CheckEditPFHRA.Properties.ValueUnchecked = "0"
        Me.CheckEditPFHRA.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditPFHRA.TabIndex = 84
        '
        'CheckEditPFBasic
        '
        Me.CheckEditPFBasic.Location = New System.Drawing.Point(508, 32)
        Me.CheckEditPFBasic.Name = "CheckEditPFBasic"
        Me.CheckEditPFBasic.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPFBasic.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPFBasic.Properties.Caption = "Basic"
        Me.CheckEditPFBasic.Properties.DisplayValueChecked = "1"
        Me.CheckEditPFBasic.Properties.DisplayValueGrayed = "0"
        Me.CheckEditPFBasic.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditPFBasic.Properties.ValueChecked = "1"
        Me.CheckEditPFBasic.Properties.ValueGrayed = "0"
        Me.CheckEditPFBasic.Properties.ValueUnchecked = "0"
        Me.CheckEditPFBasic.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditPFBasic.TabIndex = 83
        '
        'LabelControl59
        '
        Me.LabelControl59.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Underline)
        Me.LabelControl59.Appearance.Options.UseFont = True
        Me.LabelControl59.Location = New System.Drawing.Point(508, 12)
        Me.LabelControl59.Name = "LabelControl59"
        Me.LabelControl59.Size = New System.Drawing.Size(210, 14)
        Me.LabelControl59.TabIndex = 82
        Me.LabelControl59.Text = "Amount on which PF will be deducted"
        '
        'TextEditPF22
        '
        Me.TextEditPF22.Location = New System.Drawing.Point(206, 265)
        Me.TextEditPF22.Name = "TextEditPF22"
        Me.TextEditPF22.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPF22.Properties.Appearance.Options.UseFont = True
        Me.TextEditPF22.Properties.Mask.EditMask = "##.##"
        Me.TextEditPF22.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditPF22.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditPF22.Properties.MaxLength = 5
        Me.TextEditPF22.Size = New System.Drawing.Size(109, 20)
        Me.TextEditPF22.TabIndex = 81
        '
        'LabelControl58
        '
        Me.LabelControl58.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl58.Appearance.Options.UseFont = True
        Me.LabelControl58.Location = New System.Drawing.Point(17, 268)
        Me.LabelControl58.Name = "LabelControl58"
        Me.LabelControl58.Size = New System.Drawing.Size(55, 14)
        Me.LabelControl58.TabIndex = 80
        Me.LabelControl58.Text = "PF A/C 22"
        '
        'TextEditPF21
        '
        Me.TextEditPF21.Location = New System.Drawing.Point(206, 239)
        Me.TextEditPF21.Name = "TextEditPF21"
        Me.TextEditPF21.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPF21.Properties.Appearance.Options.UseFont = True
        Me.TextEditPF21.Properties.Mask.EditMask = "##.##"
        Me.TextEditPF21.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditPF21.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditPF21.Properties.MaxLength = 5
        Me.TextEditPF21.Size = New System.Drawing.Size(109, 20)
        Me.TextEditPF21.TabIndex = 79
        '
        'LabelControl57
        '
        Me.LabelControl57.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl57.Appearance.Options.UseFont = True
        Me.LabelControl57.Location = New System.Drawing.Point(17, 242)
        Me.LabelControl57.Name = "LabelControl57"
        Me.LabelControl57.Size = New System.Drawing.Size(55, 14)
        Me.LabelControl57.TabIndex = 78
        Me.LabelControl57.Text = "PF A/C 21"
        '
        'TextEditPF02
        '
        Me.TextEditPF02.Location = New System.Drawing.Point(206, 213)
        Me.TextEditPF02.Name = "TextEditPF02"
        Me.TextEditPF02.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPF02.Properties.Appearance.Options.UseFont = True
        Me.TextEditPF02.Properties.Mask.EditMask = "##.##"
        Me.TextEditPF02.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditPF02.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditPF02.Properties.MaxLength = 5
        Me.TextEditPF02.Size = New System.Drawing.Size(109, 20)
        Me.TextEditPF02.TabIndex = 77
        '
        'LabelControl56
        '
        Me.LabelControl56.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl56.Appearance.Options.UseFont = True
        Me.LabelControl56.Location = New System.Drawing.Point(17, 216)
        Me.LabelControl56.Name = "LabelControl56"
        Me.LabelControl56.Size = New System.Drawing.Size(55, 14)
        Me.LabelControl56.TabIndex = 76
        Me.LabelControl56.Text = "PF A/C 02"
        '
        'TextEditPFRound
        '
        Me.TextEditPFRound.Location = New System.Drawing.Point(206, 187)
        Me.TextEditPFRound.Name = "TextEditPFRound"
        Me.TextEditPFRound.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPFRound.Properties.Appearance.Options.UseFont = True
        Me.TextEditPFRound.Properties.Mask.EditMask = "##.##"
        Me.TextEditPFRound.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditPFRound.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditPFRound.Properties.MaxLength = 1
        Me.TextEditPFRound.Size = New System.Drawing.Size(109, 20)
        Me.TextEditPFRound.TabIndex = 75
        '
        'LabelControl55
        '
        Me.LabelControl55.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl55.Appearance.Options.UseFont = True
        Me.LabelControl55.Location = New System.Drawing.Point(17, 190)
        Me.LabelControl55.Name = "LabelControl55"
        Me.LabelControl55.Size = New System.Drawing.Size(171, 14)
        Me.LabelControl55.TabIndex = 74
        Me.LabelControl55.Text = "Rounding with decimal place of"
        '
        'TextEditPFVPFDec
        '
        Me.TextEditPFVPFDec.Location = New System.Drawing.Point(206, 161)
        Me.TextEditPFVPFDec.Name = "TextEditPFVPFDec"
        Me.TextEditPFVPFDec.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPFVPFDec.Properties.Appearance.Options.UseFont = True
        Me.TextEditPFVPFDec.Properties.Mask.EditMask = "##.##"
        Me.TextEditPFVPFDec.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditPFVPFDec.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditPFVPFDec.Properties.MaxLength = 5
        Me.TextEditPFVPFDec.Size = New System.Drawing.Size(109, 20)
        Me.TextEditPFVPFDec.TabIndex = 73
        '
        'LabelControl54
        '
        Me.LabelControl54.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl54.Appearance.Options.UseFont = True
        Me.LabelControl54.Location = New System.Drawing.Point(17, 164)
        Me.LabelControl54.Name = "LabelControl54"
        Me.LabelControl54.Size = New System.Drawing.Size(146, 14)
        Me.LabelControl54.TabIndex = 72
        Me.LabelControl54.Text = "Employees VPF Decuction "
        '
        'TextEditPFPFDec
        '
        Me.TextEditPFPFDec.Location = New System.Drawing.Point(206, 135)
        Me.TextEditPFPFDec.Name = "TextEditPFPFDec"
        Me.TextEditPFPFDec.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPFPFDec.Properties.Appearance.Options.UseFont = True
        Me.TextEditPFPFDec.Properties.Mask.EditMask = "##.##"
        Me.TextEditPFPFDec.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditPFPFDec.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditPFPFDec.Properties.MaxLength = 5
        Me.TextEditPFPFDec.Size = New System.Drawing.Size(109, 20)
        Me.TextEditPFPFDec.TabIndex = 71
        '
        'LabelControl53
        '
        Me.LabelControl53.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl53.Appearance.Options.UseFont = True
        Me.LabelControl53.Location = New System.Drawing.Point(17, 138)
        Me.LabelControl53.Name = "LabelControl53"
        Me.LabelControl53.Size = New System.Drawing.Size(138, 14)
        Me.LabelControl53.TabIndex = 70
        Me.LabelControl53.Text = "Employees PF Decuction "
        '
        'TextEditPFFPRDec
        '
        Me.TextEditPFFPRDec.Location = New System.Drawing.Point(206, 109)
        Me.TextEditPFFPRDec.Name = "TextEditPFFPRDec"
        Me.TextEditPFFPRDec.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPFFPRDec.Properties.Appearance.Options.UseFont = True
        Me.TextEditPFFPRDec.Properties.Mask.EditMask = "##.##"
        Me.TextEditPFFPRDec.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditPFFPRDec.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditPFFPRDec.Properties.MaxLength = 5
        Me.TextEditPFFPRDec.Size = New System.Drawing.Size(109, 20)
        Me.TextEditPFFPRDec.TabIndex = 69
        '
        'LabelControl52
        '
        Me.LabelControl52.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl52.Appearance.Options.UseFont = True
        Me.LabelControl52.Location = New System.Drawing.Point(17, 112)
        Me.LabelControl52.Name = "LabelControl52"
        Me.LabelControl52.Size = New System.Drawing.Size(151, 14)
        Me.LabelControl52.TabIndex = 68
        Me.LabelControl52.Text = "Employeer's FPF Decuction "
        '
        'TextEditPFEPRDec
        '
        Me.TextEditPFEPRDec.Location = New System.Drawing.Point(206, 83)
        Me.TextEditPFEPRDec.Name = "TextEditPFEPRDec"
        Me.TextEditPFEPRDec.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPFEPRDec.Properties.Appearance.Options.UseFont = True
        Me.TextEditPFEPRDec.Properties.Mask.EditMask = "##.##"
        Me.TextEditPFEPRDec.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditPFEPRDec.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditPFEPRDec.Properties.MaxLength = 5
        Me.TextEditPFEPRDec.Size = New System.Drawing.Size(109, 20)
        Me.TextEditPFEPRDec.TabIndex = 67
        '
        'LabelControl51
        '
        Me.LabelControl51.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl51.Appearance.Options.UseFont = True
        Me.LabelControl51.Location = New System.Drawing.Point(17, 86)
        Me.LabelControl51.Name = "LabelControl51"
        Me.LabelControl51.Size = New System.Drawing.Size(152, 14)
        Me.LabelControl51.TabIndex = 66
        Me.LabelControl51.Text = "Employeer's EPF Decuction "
        '
        'TextEditPFPRErDec
        '
        Me.TextEditPFPRErDec.Location = New System.Drawing.Point(206, 57)
        Me.TextEditPFPRErDec.Name = "TextEditPFPRErDec"
        Me.TextEditPFPRErDec.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPFPRErDec.Properties.Appearance.Options.UseFont = True
        Me.TextEditPFPRErDec.Properties.Mask.EditMask = "##.##"
        Me.TextEditPFPRErDec.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditPFPRErDec.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditPFPRErDec.Properties.MaxLength = 5
        Me.TextEditPFPRErDec.Size = New System.Drawing.Size(109, 20)
        Me.TextEditPFPRErDec.TabIndex = 65
        '
        'LabelControl37
        '
        Me.LabelControl37.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl37.Appearance.Options.UseFont = True
        Me.LabelControl37.Location = New System.Drawing.Point(17, 60)
        Me.LabelControl37.Name = "LabelControl37"
        Me.LabelControl37.Size = New System.Drawing.Size(145, 14)
        Me.LabelControl37.TabIndex = 64
        Me.LabelControl37.Text = "Employeer's PF Decuction "
        '
        'TextEditPFLimite
        '
        Me.TextEditPFLimite.Location = New System.Drawing.Point(206, 31)
        Me.TextEditPFLimite.Name = "TextEditPFLimite"
        Me.TextEditPFLimite.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPFLimite.Properties.Appearance.Options.UseFont = True
        Me.TextEditPFLimite.Properties.Mask.EditMask = "######.##"
        Me.TextEditPFLimite.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditPFLimite.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditPFLimite.Properties.MaxLength = 9
        Me.TextEditPFLimite.Size = New System.Drawing.Size(109, 20)
        Me.TextEditPFLimite.TabIndex = 63
        '
        'LabelControl36
        '
        Me.LabelControl36.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl36.Appearance.Options.UseFont = True
        Me.LabelControl36.Location = New System.Drawing.Point(17, 34)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(123, 14)
        Me.LabelControl36.TabIndex = 62
        Me.LabelControl36.Text = "Limite of PF deduction"
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.CheckEditESIEOT)
        Me.XtraTabPage3.Controls.Add(Me.CheckEditESIE10)
        Me.XtraTabPage3.Controls.Add(Me.CheckEditESIE8)
        Me.XtraTabPage3.Controls.Add(Me.CheckEditESIE6)
        Me.XtraTabPage3.Controls.Add(Me.CheckEditESIE4)
        Me.XtraTabPage3.Controls.Add(Me.CheckEditESIE2)
        Me.XtraTabPage3.Controls.Add(Me.CheckEditESIE9)
        Me.XtraTabPage3.Controls.Add(Me.CheckEditESIE7)
        Me.XtraTabPage3.Controls.Add(Me.CheckEditESIE5)
        Me.XtraTabPage3.Controls.Add(Me.CheckEditESIE3)
        Me.XtraTabPage3.Controls.Add(Me.CheckEditESIE1)
        Me.XtraTabPage3.Controls.Add(Me.CheckEditESIMedical)
        Me.XtraTabPage3.Controls.Add(Me.CheckEditESIConv)
        Me.XtraTabPage3.Controls.Add(Me.CheckEditESIDA)
        Me.XtraTabPage3.Controls.Add(Me.CheckEditESIHRA)
        Me.XtraTabPage3.Controls.Add(Me.CheckEditESIBasic)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl60)
        Me.XtraTabPage3.Controls.Add(Me.TextEditESIRound)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl61)
        Me.XtraTabPage3.Controls.Add(Me.TextEditESIEpDec)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl62)
        Me.XtraTabPage3.Controls.Add(Me.TextEditESIErDec)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl63)
        Me.XtraTabPage3.Controls.Add(Me.TextEditESILimite)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl64)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.PageVisible = False
        Me.XtraTabPage3.Size = New System.Drawing.Size(1028, 504)
        Me.XtraTabPage3.Text = "ESI Setup"
        '
        'CheckEditESIEOT
        '
        Me.CheckEditESIEOT.Location = New System.Drawing.Point(508, 282)
        Me.CheckEditESIEOT.Name = "CheckEditESIEOT"
        Me.CheckEditESIEOT.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESIEOT.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESIEOT.Properties.Caption = "OT"
        Me.CheckEditESIEOT.Properties.DisplayValueChecked = "1"
        Me.CheckEditESIEOT.Properties.DisplayValueGrayed = "0"
        Me.CheckEditESIEOT.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditESIEOT.Properties.ValueChecked = "1"
        Me.CheckEditESIEOT.Properties.ValueGrayed = "0"
        Me.CheckEditESIEOT.Properties.ValueUnchecked = "0"
        Me.CheckEditESIEOT.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditESIEOT.TabIndex = 122
        '
        'CheckEditESIE10
        '
        Me.CheckEditESIE10.Location = New System.Drawing.Point(623, 257)
        Me.CheckEditESIE10.Name = "CheckEditESIE10"
        Me.CheckEditESIE10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESIE10.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESIE10.Properties.Caption = "Earning-10"
        Me.CheckEditESIE10.Properties.DisplayValueChecked = "1"
        Me.CheckEditESIE10.Properties.DisplayValueGrayed = "0"
        Me.CheckEditESIE10.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditESIE10.Properties.ValueChecked = "1"
        Me.CheckEditESIE10.Properties.ValueGrayed = "0"
        Me.CheckEditESIE10.Properties.ValueUnchecked = "0"
        Me.CheckEditESIE10.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditESIE10.TabIndex = 121
        '
        'CheckEditESIE8
        '
        Me.CheckEditESIE8.Location = New System.Drawing.Point(623, 232)
        Me.CheckEditESIE8.Name = "CheckEditESIE8"
        Me.CheckEditESIE8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESIE8.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESIE8.Properties.Caption = "Earning-08"
        Me.CheckEditESIE8.Properties.DisplayValueChecked = "1"
        Me.CheckEditESIE8.Properties.DisplayValueGrayed = "0"
        Me.CheckEditESIE8.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditESIE8.Properties.ValueChecked = "1"
        Me.CheckEditESIE8.Properties.ValueGrayed = "0"
        Me.CheckEditESIE8.Properties.ValueUnchecked = "0"
        Me.CheckEditESIE8.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditESIE8.TabIndex = 119
        '
        'CheckEditESIE6
        '
        Me.CheckEditESIE6.Location = New System.Drawing.Point(623, 207)
        Me.CheckEditESIE6.Name = "CheckEditESIE6"
        Me.CheckEditESIE6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESIE6.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESIE6.Properties.Caption = "Earning-06"
        Me.CheckEditESIE6.Properties.DisplayValueChecked = "1"
        Me.CheckEditESIE6.Properties.DisplayValueGrayed = "0"
        Me.CheckEditESIE6.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditESIE6.Properties.ValueChecked = "1"
        Me.CheckEditESIE6.Properties.ValueGrayed = "0"
        Me.CheckEditESIE6.Properties.ValueUnchecked = "0"
        Me.CheckEditESIE6.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditESIE6.TabIndex = 117
        '
        'CheckEditESIE4
        '
        Me.CheckEditESIE4.Location = New System.Drawing.Point(623, 182)
        Me.CheckEditESIE4.Name = "CheckEditESIE4"
        Me.CheckEditESIE4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESIE4.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESIE4.Properties.Caption = "Earning-04"
        Me.CheckEditESIE4.Properties.DisplayValueChecked = "1"
        Me.CheckEditESIE4.Properties.DisplayValueGrayed = "0"
        Me.CheckEditESIE4.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditESIE4.Properties.ValueChecked = "1"
        Me.CheckEditESIE4.Properties.ValueGrayed = "0"
        Me.CheckEditESIE4.Properties.ValueUnchecked = "0"
        Me.CheckEditESIE4.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditESIE4.TabIndex = 115
        '
        'CheckEditESIE2
        '
        Me.CheckEditESIE2.Location = New System.Drawing.Point(623, 157)
        Me.CheckEditESIE2.Name = "CheckEditESIE2"
        Me.CheckEditESIE2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESIE2.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESIE2.Properties.Caption = "Earning-02"
        Me.CheckEditESIE2.Properties.DisplayValueChecked = "1"
        Me.CheckEditESIE2.Properties.DisplayValueGrayed = "0"
        Me.CheckEditESIE2.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditESIE2.Properties.ValueChecked = "1"
        Me.CheckEditESIE2.Properties.ValueGrayed = "0"
        Me.CheckEditESIE2.Properties.ValueUnchecked = "0"
        Me.CheckEditESIE2.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditESIE2.TabIndex = 113
        '
        'CheckEditESIE9
        '
        Me.CheckEditESIE9.Location = New System.Drawing.Point(508, 257)
        Me.CheckEditESIE9.Name = "CheckEditESIE9"
        Me.CheckEditESIE9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESIE9.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESIE9.Properties.Caption = "Earning-09"
        Me.CheckEditESIE9.Properties.DisplayValueChecked = "1"
        Me.CheckEditESIE9.Properties.DisplayValueGrayed = "0"
        Me.CheckEditESIE9.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditESIE9.Properties.ValueChecked = "1"
        Me.CheckEditESIE9.Properties.ValueGrayed = "0"
        Me.CheckEditESIE9.Properties.ValueUnchecked = "0"
        Me.CheckEditESIE9.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditESIE9.TabIndex = 120
        '
        'CheckEditESIE7
        '
        Me.CheckEditESIE7.Location = New System.Drawing.Point(508, 232)
        Me.CheckEditESIE7.Name = "CheckEditESIE7"
        Me.CheckEditESIE7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESIE7.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESIE7.Properties.Caption = "Earning-07"
        Me.CheckEditESIE7.Properties.DisplayValueChecked = "1"
        Me.CheckEditESIE7.Properties.DisplayValueGrayed = "0"
        Me.CheckEditESIE7.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditESIE7.Properties.ValueChecked = "1"
        Me.CheckEditESIE7.Properties.ValueGrayed = "0"
        Me.CheckEditESIE7.Properties.ValueUnchecked = "0"
        Me.CheckEditESIE7.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditESIE7.TabIndex = 118
        '
        'CheckEditESIE5
        '
        Me.CheckEditESIE5.Location = New System.Drawing.Point(508, 207)
        Me.CheckEditESIE5.Name = "CheckEditESIE5"
        Me.CheckEditESIE5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESIE5.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESIE5.Properties.Caption = "Earning-05"
        Me.CheckEditESIE5.Properties.DisplayValueChecked = "1"
        Me.CheckEditESIE5.Properties.DisplayValueGrayed = "0"
        Me.CheckEditESIE5.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditESIE5.Properties.ValueChecked = "1"
        Me.CheckEditESIE5.Properties.ValueGrayed = "0"
        Me.CheckEditESIE5.Properties.ValueUnchecked = "0"
        Me.CheckEditESIE5.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditESIE5.TabIndex = 116
        '
        'CheckEditESIE3
        '
        Me.CheckEditESIE3.Location = New System.Drawing.Point(508, 182)
        Me.CheckEditESIE3.Name = "CheckEditESIE3"
        Me.CheckEditESIE3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESIE3.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESIE3.Properties.Caption = "Earning-03"
        Me.CheckEditESIE3.Properties.DisplayValueChecked = "1"
        Me.CheckEditESIE3.Properties.DisplayValueGrayed = "0"
        Me.CheckEditESIE3.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditESIE3.Properties.ValueChecked = "1"
        Me.CheckEditESIE3.Properties.ValueGrayed = "0"
        Me.CheckEditESIE3.Properties.ValueUnchecked = "0"
        Me.CheckEditESIE3.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditESIE3.TabIndex = 114
        '
        'CheckEditESIE1
        '
        Me.CheckEditESIE1.Location = New System.Drawing.Point(508, 157)
        Me.CheckEditESIE1.Name = "CheckEditESIE1"
        Me.CheckEditESIE1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESIE1.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESIE1.Properties.Caption = "Earning-01"
        Me.CheckEditESIE1.Properties.DisplayValueChecked = "1"
        Me.CheckEditESIE1.Properties.DisplayValueGrayed = "0"
        Me.CheckEditESIE1.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditESIE1.Properties.ValueChecked = "1"
        Me.CheckEditESIE1.Properties.ValueGrayed = "0"
        Me.CheckEditESIE1.Properties.ValueUnchecked = "0"
        Me.CheckEditESIE1.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditESIE1.TabIndex = 112
        '
        'CheckEditESIMedical
        '
        Me.CheckEditESIMedical.Location = New System.Drawing.Point(508, 132)
        Me.CheckEditESIMedical.Name = "CheckEditESIMedical"
        Me.CheckEditESIMedical.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESIMedical.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESIMedical.Properties.Caption = "Medical"
        Me.CheckEditESIMedical.Properties.DisplayValueChecked = "1"
        Me.CheckEditESIMedical.Properties.DisplayValueGrayed = "0"
        Me.CheckEditESIMedical.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditESIMedical.Properties.ValueChecked = "1"
        Me.CheckEditESIMedical.Properties.ValueGrayed = "0"
        Me.CheckEditESIMedical.Properties.ValueUnchecked = "0"
        Me.CheckEditESIMedical.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditESIMedical.TabIndex = 111
        '
        'CheckEditESIConv
        '
        Me.CheckEditESIConv.Location = New System.Drawing.Point(508, 107)
        Me.CheckEditESIConv.Name = "CheckEditESIConv"
        Me.CheckEditESIConv.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESIConv.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESIConv.Properties.Caption = "Conveyance"
        Me.CheckEditESIConv.Properties.DisplayValueChecked = "1"
        Me.CheckEditESIConv.Properties.DisplayValueGrayed = "0"
        Me.CheckEditESIConv.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditESIConv.Properties.ValueChecked = "1"
        Me.CheckEditESIConv.Properties.ValueGrayed = "0"
        Me.CheckEditESIConv.Properties.ValueUnchecked = "0"
        Me.CheckEditESIConv.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditESIConv.TabIndex = 110
        '
        'CheckEditESIDA
        '
        Me.CheckEditESIDA.Location = New System.Drawing.Point(508, 82)
        Me.CheckEditESIDA.Name = "CheckEditESIDA"
        Me.CheckEditESIDA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESIDA.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESIDA.Properties.Caption = "DA"
        Me.CheckEditESIDA.Properties.DisplayValueChecked = "1"
        Me.CheckEditESIDA.Properties.DisplayValueGrayed = "0"
        Me.CheckEditESIDA.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditESIDA.Properties.ValueChecked = "1"
        Me.CheckEditESIDA.Properties.ValueGrayed = "0"
        Me.CheckEditESIDA.Properties.ValueUnchecked = "0"
        Me.CheckEditESIDA.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditESIDA.TabIndex = 109
        '
        'CheckEditESIHRA
        '
        Me.CheckEditESIHRA.Location = New System.Drawing.Point(508, 57)
        Me.CheckEditESIHRA.Name = "CheckEditESIHRA"
        Me.CheckEditESIHRA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESIHRA.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESIHRA.Properties.Caption = "HRA"
        Me.CheckEditESIHRA.Properties.DisplayValueChecked = "1"
        Me.CheckEditESIHRA.Properties.DisplayValueGrayed = "0"
        Me.CheckEditESIHRA.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditESIHRA.Properties.ValueChecked = "1"
        Me.CheckEditESIHRA.Properties.ValueGrayed = "0"
        Me.CheckEditESIHRA.Properties.ValueUnchecked = "0"
        Me.CheckEditESIHRA.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditESIHRA.TabIndex = 108
        '
        'CheckEditESIBasic
        '
        Me.CheckEditESIBasic.Location = New System.Drawing.Point(508, 32)
        Me.CheckEditESIBasic.Name = "CheckEditESIBasic"
        Me.CheckEditESIBasic.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESIBasic.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESIBasic.Properties.Caption = "Basic"
        Me.CheckEditESIBasic.Properties.DisplayValueChecked = "1"
        Me.CheckEditESIBasic.Properties.DisplayValueGrayed = "0"
        Me.CheckEditESIBasic.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditESIBasic.Properties.ValueChecked = "1"
        Me.CheckEditESIBasic.Properties.ValueGrayed = "0"
        Me.CheckEditESIBasic.Properties.ValueUnchecked = "0"
        Me.CheckEditESIBasic.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditESIBasic.TabIndex = 107
        '
        'LabelControl60
        '
        Me.LabelControl60.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Underline)
        Me.LabelControl60.Appearance.Options.UseFont = True
        Me.LabelControl60.Location = New System.Drawing.Point(508, 12)
        Me.LabelControl60.Name = "LabelControl60"
        Me.LabelControl60.Size = New System.Drawing.Size(210, 14)
        Me.LabelControl60.TabIndex = 106
        Me.LabelControl60.Text = "Amount on which PF will be deducted"
        '
        'TextEditESIRound
        '
        Me.TextEditESIRound.Location = New System.Drawing.Point(206, 109)
        Me.TextEditESIRound.Name = "TextEditESIRound"
        Me.TextEditESIRound.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditESIRound.Properties.Appearance.Options.UseFont = True
        Me.TextEditESIRound.Properties.Mask.EditMask = "##.##"
        Me.TextEditESIRound.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditESIRound.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditESIRound.Properties.MaxLength = 1
        Me.TextEditESIRound.Size = New System.Drawing.Size(109, 20)
        Me.TextEditESIRound.TabIndex = 105
        '
        'LabelControl61
        '
        Me.LabelControl61.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl61.Appearance.Options.UseFont = True
        Me.LabelControl61.Location = New System.Drawing.Point(17, 112)
        Me.LabelControl61.Name = "LabelControl61"
        Me.LabelControl61.Size = New System.Drawing.Size(171, 14)
        Me.LabelControl61.TabIndex = 104
        Me.LabelControl61.Text = "Rounding with decimal place of"
        '
        'TextEditESIEpDec
        '
        Me.TextEditESIEpDec.Location = New System.Drawing.Point(206, 83)
        Me.TextEditESIEpDec.Name = "TextEditESIEpDec"
        Me.TextEditESIEpDec.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditESIEpDec.Properties.Appearance.Options.UseFont = True
        Me.TextEditESIEpDec.Properties.Mask.EditMask = "##.##"
        Me.TextEditESIEpDec.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditESIEpDec.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditESIEpDec.Properties.MaxLength = 5
        Me.TextEditESIEpDec.Size = New System.Drawing.Size(109, 20)
        Me.TextEditESIEpDec.TabIndex = 103
        '
        'LabelControl62
        '
        Me.LabelControl62.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl62.Appearance.Options.UseFont = True
        Me.LabelControl62.Location = New System.Drawing.Point(17, 86)
        Me.LabelControl62.Name = "LabelControl62"
        Me.LabelControl62.Size = New System.Drawing.Size(143, 14)
        Me.LabelControl62.TabIndex = 102
        Me.LabelControl62.Text = "Employees ESI Decuction "
        '
        'TextEditESIErDec
        '
        Me.TextEditESIErDec.Location = New System.Drawing.Point(206, 57)
        Me.TextEditESIErDec.Name = "TextEditESIErDec"
        Me.TextEditESIErDec.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditESIErDec.Properties.Appearance.Options.UseFont = True
        Me.TextEditESIErDec.Properties.Mask.EditMask = "##.##"
        Me.TextEditESIErDec.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditESIErDec.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditESIErDec.Properties.MaxLength = 5
        Me.TextEditESIErDec.Size = New System.Drawing.Size(109, 20)
        Me.TextEditESIErDec.TabIndex = 101
        '
        'LabelControl63
        '
        Me.LabelControl63.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl63.Appearance.Options.UseFont = True
        Me.LabelControl63.Location = New System.Drawing.Point(17, 60)
        Me.LabelControl63.Name = "LabelControl63"
        Me.LabelControl63.Size = New System.Drawing.Size(150, 14)
        Me.LabelControl63.TabIndex = 100
        Me.LabelControl63.Text = "Employeer's ESI Decuction "
        '
        'TextEditESILimite
        '
        Me.TextEditESILimite.Location = New System.Drawing.Point(206, 31)
        Me.TextEditESILimite.Name = "TextEditESILimite"
        Me.TextEditESILimite.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditESILimite.Properties.Appearance.Options.UseFont = True
        Me.TextEditESILimite.Properties.Mask.EditMask = "######.##"
        Me.TextEditESILimite.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditESILimite.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditESILimite.Properties.MaxLength = 9
        Me.TextEditESILimite.Size = New System.Drawing.Size(109, 20)
        Me.TextEditESILimite.TabIndex = 99
        '
        'LabelControl64
        '
        Me.LabelControl64.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl64.Appearance.Options.UseFont = True
        Me.LabelControl64.Location = New System.Drawing.Point(17, 34)
        Me.LabelControl64.Name = "LabelControl64"
        Me.LabelControl64.Size = New System.Drawing.Size(128, 14)
        Me.LabelControl64.TabIndex = 98
        Me.LabelControl64.Text = "Limite of ESI deduction"
        '
        'XtraTabPage4
        '
        Me.XtraTabPage4.Controls.Add(Me.LabelControl71)
        Me.XtraTabPage4.Controls.Add(Me.TextEditGratuityFormula)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl70)
        Me.XtraTabPage4.Controls.Add(Me.TextEdit1)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl69)
        Me.XtraTabPage4.Controls.Add(Me.TextEditBonusRate)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl68)
        Me.XtraTabPage4.Controls.Add(Me.CheckEditBonusAllOnArr)
        Me.XtraTabPage4.Controls.Add(Me.TextEditBonusAmtLimit)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl67)
        Me.XtraTabPage4.Controls.Add(Me.TextEditBonusWageLimit)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl66)
        Me.XtraTabPage4.Controls.Add(Me.TextEditBonusMinWorking)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl65)
        Me.XtraTabPage4.Name = "XtraTabPage4"
        Me.XtraTabPage4.PageVisible = False
        Me.XtraTabPage4.Size = New System.Drawing.Size(1028, 504)
        Me.XtraTabPage4.Text = "Bonus/Gratuity Setup"
        '
        'LabelControl71
        '
        Me.LabelControl71.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl71.Appearance.Options.UseFont = True
        Me.LabelControl71.Location = New System.Drawing.Point(17, 190)
        Me.LabelControl71.Name = "LabelControl71"
        Me.LabelControl71.Size = New System.Drawing.Size(89, 14)
        Me.LabelControl71.TabIndex = 130
        Me.LabelControl71.Text = "Gratuity Formula"
        '
        'TextEditGratuityFormula
        '
        Me.TextEditGratuityFormula.Location = New System.Drawing.Point(234, 187)
        Me.TextEditGratuityFormula.Name = "TextEditGratuityFormula"
        Me.TextEditGratuityFormula.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditGratuityFormula.Properties.Appearance.Options.UseFont = True
        Me.TextEditGratuityFormula.Size = New System.Drawing.Size(509, 20)
        Me.TextEditGratuityFormula.TabIndex = 129
        '
        'LabelControl70
        '
        Me.LabelControl70.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl70.Appearance.Options.UseFont = True
        Me.LabelControl70.Location = New System.Drawing.Point(17, 166)
        Me.LabelControl70.Name = "LabelControl70"
        Me.LabelControl70.Size = New System.Drawing.Size(150, 14)
        Me.LabelControl70.TabIndex = 128
        Me.LabelControl70.Text = "Maximum Years of  Gratuity"
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(234, 160)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.Mask.EditMask = "#.##"
        Me.TextEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit1.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEdit1.Properties.MaxLength = 4
        Me.TextEdit1.Size = New System.Drawing.Size(109, 20)
        Me.TextEdit1.TabIndex = 127
        '
        'LabelControl69
        '
        Me.LabelControl69.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl69.Appearance.Options.UseFont = True
        Me.LabelControl69.Location = New System.Drawing.Point(17, 140)
        Me.LabelControl69.Name = "LabelControl69"
        Me.LabelControl69.Size = New System.Drawing.Size(62, 14)
        Me.LabelControl69.TabIndex = 126
        Me.LabelControl69.Text = "Bonus Rate"
        '
        'TextEditBonusRate
        '
        Me.TextEditBonusRate.Location = New System.Drawing.Point(234, 134)
        Me.TextEditBonusRate.Name = "TextEditBonusRate"
        Me.TextEditBonusRate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditBonusRate.Properties.Appearance.Options.UseFont = True
        Me.TextEditBonusRate.Properties.Mask.EditMask = "###.##"
        Me.TextEditBonusRate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditBonusRate.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditBonusRate.Properties.MaxLength = 6
        Me.TextEditBonusRate.Size = New System.Drawing.Size(109, 20)
        Me.TextEditBonusRate.TabIndex = 125
        '
        'LabelControl68
        '
        Me.LabelControl68.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl68.Appearance.Options.UseFont = True
        Me.LabelControl68.Location = New System.Drawing.Point(17, 114)
        Me.LabelControl68.Name = "LabelControl68"
        Me.LabelControl68.Size = New System.Drawing.Size(161, 14)
        Me.LabelControl68.TabIndex = 124
        Me.LabelControl68.Text = "Bonus Allowed on Arrear Also"
        '
        'CheckEditBonusAllOnArr
        '
        Me.CheckEditBonusAllOnArr.EditValue = "N"
        Me.CheckEditBonusAllOnArr.Location = New System.Drawing.Point(234, 109)
        Me.CheckEditBonusAllOnArr.Name = "CheckEditBonusAllOnArr"
        Me.CheckEditBonusAllOnArr.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditBonusAllOnArr.Properties.Appearance.Options.UseFont = True
        Me.CheckEditBonusAllOnArr.Properties.Caption = ""
        Me.CheckEditBonusAllOnArr.Properties.DisplayValueChecked = "Y"
        Me.CheckEditBonusAllOnArr.Properties.DisplayValueGrayed = "N"
        Me.CheckEditBonusAllOnArr.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditBonusAllOnArr.Properties.ValueChecked = "Y"
        Me.CheckEditBonusAllOnArr.Properties.ValueGrayed = "N"
        Me.CheckEditBonusAllOnArr.Properties.ValueUnchecked = "N"
        Me.CheckEditBonusAllOnArr.Size = New System.Drawing.Size(18, 19)
        Me.CheckEditBonusAllOnArr.TabIndex = 123
        '
        'TextEditBonusAmtLimit
        '
        Me.TextEditBonusAmtLimit.Location = New System.Drawing.Point(234, 83)
        Me.TextEditBonusAmtLimit.Name = "TextEditBonusAmtLimit"
        Me.TextEditBonusAmtLimit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditBonusAmtLimit.Properties.Appearance.Options.UseFont = True
        Me.TextEditBonusAmtLimit.Properties.Mask.EditMask = "######.##"
        Me.TextEditBonusAmtLimit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditBonusAmtLimit.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditBonusAmtLimit.Properties.MaxLength = 6
        Me.TextEditBonusAmtLimit.Size = New System.Drawing.Size(109, 20)
        Me.TextEditBonusAmtLimit.TabIndex = 105
        '
        'LabelControl67
        '
        Me.LabelControl67.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl67.Appearance.Options.UseFont = True
        Me.LabelControl67.Location = New System.Drawing.Point(17, 86)
        Me.LabelControl67.Name = "LabelControl67"
        Me.LabelControl67.Size = New System.Drawing.Size(117, 14)
        Me.LabelControl67.TabIndex = 104
        Me.LabelControl67.Text = "Bonus Amount Limite"
        '
        'TextEditBonusWageLimit
        '
        Me.TextEditBonusWageLimit.Location = New System.Drawing.Point(234, 57)
        Me.TextEditBonusWageLimit.Name = "TextEditBonusWageLimit"
        Me.TextEditBonusWageLimit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditBonusWageLimit.Properties.Appearance.Options.UseFont = True
        Me.TextEditBonusWageLimit.Properties.Mask.EditMask = "######.##"
        Me.TextEditBonusWageLimit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditBonusWageLimit.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditBonusWageLimit.Properties.MaxLength = 6
        Me.TextEditBonusWageLimit.Size = New System.Drawing.Size(109, 20)
        Me.TextEditBonusWageLimit.TabIndex = 103
        '
        'LabelControl66
        '
        Me.LabelControl66.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl66.Appearance.Options.UseFont = True
        Me.LabelControl66.Location = New System.Drawing.Point(17, 60)
        Me.LabelControl66.Name = "LabelControl66"
        Me.LabelControl66.Size = New System.Drawing.Size(105, 14)
        Me.LabelControl66.TabIndex = 102
        Me.LabelControl66.Text = "Bonus Wage Limite"
        '
        'TextEditBonusMinWorking
        '
        Me.TextEditBonusMinWorking.Location = New System.Drawing.Point(234, 31)
        Me.TextEditBonusMinWorking.Name = "TextEditBonusMinWorking"
        Me.TextEditBonusMinWorking.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditBonusMinWorking.Properties.Appearance.Options.UseFont = True
        Me.TextEditBonusMinWorking.Properties.Mask.EditMask = "###.##"
        Me.TextEditBonusMinWorking.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditBonusMinWorking.Properties.Mask.PlaceHolder = Global.Microsoft.VisualBasic.ChrW(48)
        Me.TextEditBonusMinWorking.Properties.MaxLength = 6
        Me.TextEditBonusMinWorking.Size = New System.Drawing.Size(109, 20)
        Me.TextEditBonusMinWorking.TabIndex = 101
        '
        'LabelControl65
        '
        Me.LabelControl65.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl65.Appearance.Options.UseFont = True
        Me.LabelControl65.Location = New System.Drawing.Point(17, 34)
        Me.LabelControl65.Name = "LabelControl65"
        Me.LabelControl65.Size = New System.Drawing.Size(181, 14)
        Me.LabelControl65.TabIndex = 100
        Me.LabelControl65.Text = "Minimum Working Days for Bonus"
        '
        'XtraTabPage5
        '
        Me.XtraTabPage5.Controls.Add(Me.GridControlLeave)
        Me.XtraTabPage5.Controls.Add(Me.GroupControl5)
        Me.XtraTabPage5.Controls.Add(Me.GroupControl4)
        Me.XtraTabPage5.Controls.Add(Me.GroupControl3)
        Me.XtraTabPage5.Name = "XtraTabPage5"
        Me.XtraTabPage5.PageVisible = False
        Me.XtraTabPage5.Size = New System.Drawing.Size(1028, 504)
        Me.XtraTabPage5.Text = "Allowed"
        '
        'GridControlLeave
        '
        Me.GridControlLeave.DataSource = Me.TblLeaveMasterBindingSource
        GridLevelNode2.RelationName = "Level1"
        Me.GridControlLeave.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode2})
        Me.GridControlLeave.Location = New System.Drawing.Point(618, 3)
        Me.GridControlLeave.MainView = Me.GridViewLeave
        Me.GridControlLeave.Name = "GridControlLeave"
        Me.GridControlLeave.Size = New System.Drawing.Size(340, 498)
        Me.GridControlLeave.TabIndex = 3
        Me.GridControlLeave.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewLeave})
        '
        'TblLeaveMasterBindingSource
        '
        Me.TblLeaveMasterBindingSource.DataMember = "tblLeaveMaster"
        Me.TblLeaveMasterBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewLeave
        '
        Me.GridViewLeave.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colLEAVEFIELD, Me.colLEAVECODE})
        Me.GridViewLeave.GridControl = Me.GridControlLeave
        Me.GridViewLeave.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridViewLeave.Name = "GridViewLeave"
        Me.GridViewLeave.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewLeave.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewLeave.OptionsBehavior.Editable = False
        Me.GridViewLeave.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewLeave.OptionsSelection.MultiSelect = True
        Me.GridViewLeave.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        '
        'colLEAVEFIELD
        '
        Me.colLEAVEFIELD.FieldName = "LEAVEFIELD"
        Me.colLEAVEFIELD.Name = "colLEAVEFIELD"
        Me.colLEAVEFIELD.Visible = True
        Me.colLEAVEFIELD.VisibleIndex = 1
        '
        'colLEAVECODE
        '
        Me.colLEAVECODE.FieldName = "LEAVECODE"
        Me.colLEAVECODE.Name = "colLEAVECODE"
        Me.colLEAVECODE.Visible = True
        Me.colLEAVECODE.VisibleIndex = 2
        '
        'GroupControl5
        '
        Me.GroupControl5.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GroupControl5.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl5.AppearanceCaption.Options.UseFont = True
        Me.GroupControl5.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl5.Controls.Add(Me.CheckEditchklvPF)
        Me.GroupControl5.Controls.Add(Me.CheckEditAllowLeaveMedical)
        Me.GroupControl5.Controls.Add(Me.CheckEditAllowLeaveConveyance)
        Me.GroupControl5.Controls.Add(Me.CheckEditAllowLeaveDA)
        Me.GroupControl5.Controls.Add(Me.CheckEditAllowLeaveHRA)
        Me.GroupControl5.Controls.Add(Me.CheckEditAllowLeaveE10)
        Me.GroupControl5.Controls.Add(Me.CheckEditAllowLeaveE8)
        Me.GroupControl5.Controls.Add(Me.CheckEditAllowLeaveE6)
        Me.GroupControl5.Controls.Add(Me.CheckEditAllowLeaveE4)
        Me.GroupControl5.Controls.Add(Me.CheckEditAllowLeaveE2)
        Me.GroupControl5.Controls.Add(Me.CheckEditAllowLeaveE9)
        Me.GroupControl5.Controls.Add(Me.CheckEditAllowLeaveE7)
        Me.GroupControl5.Controls.Add(Me.CheckEditAllowLeaveE5)
        Me.GroupControl5.Controls.Add(Me.CheckEditAllowLeaveE3)
        Me.GroupControl5.Controls.Add(Me.CheckEditAllowLeaveE1)
        Me.GroupControl5.Controls.Add(Me.CheckEditAllowLeaveBasic)
        Me.GroupControl5.Location = New System.Drawing.Point(386, 3)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(226, 498)
        Me.GroupControl5.TabIndex = 2
        Me.GroupControl5.Text = "Leave Encash On"
        '
        'CheckEditchklvPF
        '
        Me.CheckEditchklvPF.EditValue = "N"
        Me.CheckEditchklvPF.Location = New System.Drawing.Point(18, 412)
        Me.CheckEditchklvPF.Name = "CheckEditchklvPF"
        Me.CheckEditchklvPF.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditchklvPF.Properties.Appearance.Options.UseFont = True
        Me.CheckEditchklvPF.Properties.Caption = "PF Allowed on Leave Encash"
        Me.CheckEditchklvPF.Properties.DisplayValueChecked = "Y"
        Me.CheckEditchklvPF.Properties.DisplayValueGrayed = "N"
        Me.CheckEditchklvPF.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditchklvPF.Properties.ValueChecked = "Y"
        Me.CheckEditchklvPF.Properties.ValueGrayed = "N"
        Me.CheckEditchklvPF.Properties.ValueUnchecked = "N"
        Me.CheckEditchklvPF.Size = New System.Drawing.Size(190, 19)
        Me.CheckEditchklvPF.TabIndex = 112
        '
        'CheckEditAllowLeaveMedical
        '
        Me.CheckEditAllowLeaveMedical.Location = New System.Drawing.Point(18, 137)
        Me.CheckEditAllowLeaveMedical.Name = "CheckEditAllowLeaveMedical"
        Me.CheckEditAllowLeaveMedical.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowLeaveMedical.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowLeaveMedical.Properties.Caption = "Medical"
        Me.CheckEditAllowLeaveMedical.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowLeaveMedical.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowLeaveMedical.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowLeaveMedical.Properties.ValueChecked = "1"
        Me.CheckEditAllowLeaveMedical.Properties.ValueGrayed = "0"
        Me.CheckEditAllowLeaveMedical.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowLeaveMedical.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowLeaveMedical.TabIndex = 111
        '
        'CheckEditAllowLeaveConveyance
        '
        Me.CheckEditAllowLeaveConveyance.Location = New System.Drawing.Point(18, 112)
        Me.CheckEditAllowLeaveConveyance.Name = "CheckEditAllowLeaveConveyance"
        Me.CheckEditAllowLeaveConveyance.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowLeaveConveyance.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowLeaveConveyance.Properties.Caption = "Conveyance"
        Me.CheckEditAllowLeaveConveyance.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowLeaveConveyance.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowLeaveConveyance.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowLeaveConveyance.Properties.ValueChecked = "1"
        Me.CheckEditAllowLeaveConveyance.Properties.ValueGrayed = "0"
        Me.CheckEditAllowLeaveConveyance.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowLeaveConveyance.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowLeaveConveyance.TabIndex = 110
        '
        'CheckEditAllowLeaveDA
        '
        Me.CheckEditAllowLeaveDA.Location = New System.Drawing.Point(18, 87)
        Me.CheckEditAllowLeaveDA.Name = "CheckEditAllowLeaveDA"
        Me.CheckEditAllowLeaveDA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowLeaveDA.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowLeaveDA.Properties.Caption = "DA"
        Me.CheckEditAllowLeaveDA.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowLeaveDA.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowLeaveDA.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowLeaveDA.Properties.ValueChecked = "1"
        Me.CheckEditAllowLeaveDA.Properties.ValueGrayed = "0"
        Me.CheckEditAllowLeaveDA.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowLeaveDA.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowLeaveDA.TabIndex = 109
        '
        'CheckEditAllowLeaveHRA
        '
        Me.CheckEditAllowLeaveHRA.Location = New System.Drawing.Point(18, 62)
        Me.CheckEditAllowLeaveHRA.Name = "CheckEditAllowLeaveHRA"
        Me.CheckEditAllowLeaveHRA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowLeaveHRA.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowLeaveHRA.Properties.Caption = "HRA"
        Me.CheckEditAllowLeaveHRA.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowLeaveHRA.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowLeaveHRA.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowLeaveHRA.Properties.ValueChecked = "1"
        Me.CheckEditAllowLeaveHRA.Properties.ValueGrayed = "0"
        Me.CheckEditAllowLeaveHRA.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowLeaveHRA.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowLeaveHRA.TabIndex = 108
        '
        'CheckEditAllowLeaveE10
        '
        Me.CheckEditAllowLeaveE10.Location = New System.Drawing.Point(18, 387)
        Me.CheckEditAllowLeaveE10.Name = "CheckEditAllowLeaveE10"
        Me.CheckEditAllowLeaveE10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowLeaveE10.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowLeaveE10.Properties.Caption = "Earning-10"
        Me.CheckEditAllowLeaveE10.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowLeaveE10.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowLeaveE10.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowLeaveE10.Properties.ValueChecked = "1"
        Me.CheckEditAllowLeaveE10.Properties.ValueGrayed = "0"
        Me.CheckEditAllowLeaveE10.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowLeaveE10.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowLeaveE10.TabIndex = 107
        '
        'CheckEditAllowLeaveE8
        '
        Me.CheckEditAllowLeaveE8.Location = New System.Drawing.Point(18, 337)
        Me.CheckEditAllowLeaveE8.Name = "CheckEditAllowLeaveE8"
        Me.CheckEditAllowLeaveE8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowLeaveE8.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowLeaveE8.Properties.Caption = "Earning-08"
        Me.CheckEditAllowLeaveE8.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowLeaveE8.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowLeaveE8.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowLeaveE8.Properties.ValueChecked = "1"
        Me.CheckEditAllowLeaveE8.Properties.ValueGrayed = "0"
        Me.CheckEditAllowLeaveE8.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowLeaveE8.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowLeaveE8.TabIndex = 105
        '
        'CheckEditAllowLeaveE6
        '
        Me.CheckEditAllowLeaveE6.Location = New System.Drawing.Point(18, 287)
        Me.CheckEditAllowLeaveE6.Name = "CheckEditAllowLeaveE6"
        Me.CheckEditAllowLeaveE6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowLeaveE6.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowLeaveE6.Properties.Caption = "Earning-06"
        Me.CheckEditAllowLeaveE6.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowLeaveE6.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowLeaveE6.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowLeaveE6.Properties.ValueChecked = "1"
        Me.CheckEditAllowLeaveE6.Properties.ValueGrayed = "0"
        Me.CheckEditAllowLeaveE6.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowLeaveE6.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowLeaveE6.TabIndex = 103
        '
        'CheckEditAllowLeaveE4
        '
        Me.CheckEditAllowLeaveE4.Location = New System.Drawing.Point(18, 237)
        Me.CheckEditAllowLeaveE4.Name = "CheckEditAllowLeaveE4"
        Me.CheckEditAllowLeaveE4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowLeaveE4.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowLeaveE4.Properties.Caption = "Earning-04"
        Me.CheckEditAllowLeaveE4.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowLeaveE4.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowLeaveE4.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowLeaveE4.Properties.ValueChecked = "1"
        Me.CheckEditAllowLeaveE4.Properties.ValueGrayed = "0"
        Me.CheckEditAllowLeaveE4.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowLeaveE4.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowLeaveE4.TabIndex = 101
        '
        'CheckEditAllowLeaveE2
        '
        Me.CheckEditAllowLeaveE2.Location = New System.Drawing.Point(18, 187)
        Me.CheckEditAllowLeaveE2.Name = "CheckEditAllowLeaveE2"
        Me.CheckEditAllowLeaveE2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowLeaveE2.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowLeaveE2.Properties.Caption = "Earning-02"
        Me.CheckEditAllowLeaveE2.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowLeaveE2.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowLeaveE2.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowLeaveE2.Properties.ValueChecked = "1"
        Me.CheckEditAllowLeaveE2.Properties.ValueGrayed = "0"
        Me.CheckEditAllowLeaveE2.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowLeaveE2.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowLeaveE2.TabIndex = 99
        '
        'CheckEditAllowLeaveE9
        '
        Me.CheckEditAllowLeaveE9.Location = New System.Drawing.Point(18, 362)
        Me.CheckEditAllowLeaveE9.Name = "CheckEditAllowLeaveE9"
        Me.CheckEditAllowLeaveE9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowLeaveE9.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowLeaveE9.Properties.Caption = "Earning-09"
        Me.CheckEditAllowLeaveE9.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowLeaveE9.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowLeaveE9.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowLeaveE9.Properties.ValueChecked = "1"
        Me.CheckEditAllowLeaveE9.Properties.ValueGrayed = "0"
        Me.CheckEditAllowLeaveE9.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowLeaveE9.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowLeaveE9.TabIndex = 106
        '
        'CheckEditAllowLeaveE7
        '
        Me.CheckEditAllowLeaveE7.Location = New System.Drawing.Point(18, 312)
        Me.CheckEditAllowLeaveE7.Name = "CheckEditAllowLeaveE7"
        Me.CheckEditAllowLeaveE7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowLeaveE7.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowLeaveE7.Properties.Caption = "Earning-07"
        Me.CheckEditAllowLeaveE7.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowLeaveE7.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowLeaveE7.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowLeaveE7.Properties.ValueChecked = "1"
        Me.CheckEditAllowLeaveE7.Properties.ValueGrayed = "0"
        Me.CheckEditAllowLeaveE7.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowLeaveE7.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowLeaveE7.TabIndex = 104
        '
        'CheckEditAllowLeaveE5
        '
        Me.CheckEditAllowLeaveE5.Location = New System.Drawing.Point(18, 262)
        Me.CheckEditAllowLeaveE5.Name = "CheckEditAllowLeaveE5"
        Me.CheckEditAllowLeaveE5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowLeaveE5.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowLeaveE5.Properties.Caption = "Earning-05"
        Me.CheckEditAllowLeaveE5.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowLeaveE5.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowLeaveE5.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowLeaveE5.Properties.ValueChecked = "1"
        Me.CheckEditAllowLeaveE5.Properties.ValueGrayed = "0"
        Me.CheckEditAllowLeaveE5.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowLeaveE5.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowLeaveE5.TabIndex = 102
        '
        'CheckEditAllowLeaveE3
        '
        Me.CheckEditAllowLeaveE3.Location = New System.Drawing.Point(18, 212)
        Me.CheckEditAllowLeaveE3.Name = "CheckEditAllowLeaveE3"
        Me.CheckEditAllowLeaveE3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowLeaveE3.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowLeaveE3.Properties.Caption = "Earning-03"
        Me.CheckEditAllowLeaveE3.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowLeaveE3.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowLeaveE3.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowLeaveE3.Properties.ValueChecked = "1"
        Me.CheckEditAllowLeaveE3.Properties.ValueGrayed = "0"
        Me.CheckEditAllowLeaveE3.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowLeaveE3.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowLeaveE3.TabIndex = 100
        '
        'CheckEditAllowLeaveE1
        '
        Me.CheckEditAllowLeaveE1.Location = New System.Drawing.Point(18, 162)
        Me.CheckEditAllowLeaveE1.Name = "CheckEditAllowLeaveE1"
        Me.CheckEditAllowLeaveE1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowLeaveE1.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowLeaveE1.Properties.Caption = "Earning-01"
        Me.CheckEditAllowLeaveE1.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowLeaveE1.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowLeaveE1.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowLeaveE1.Properties.ValueChecked = "1"
        Me.CheckEditAllowLeaveE1.Properties.ValueGrayed = "0"
        Me.CheckEditAllowLeaveE1.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowLeaveE1.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowLeaveE1.TabIndex = 98
        '
        'CheckEditAllowLeaveBasic
        '
        Me.CheckEditAllowLeaveBasic.Location = New System.Drawing.Point(18, 37)
        Me.CheckEditAllowLeaveBasic.Name = "CheckEditAllowLeaveBasic"
        Me.CheckEditAllowLeaveBasic.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowLeaveBasic.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowLeaveBasic.Properties.Caption = "Basic"
        Me.CheckEditAllowLeaveBasic.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowLeaveBasic.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowLeaveBasic.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowLeaveBasic.Properties.ValueChecked = "1"
        Me.CheckEditAllowLeaveBasic.Properties.ValueGrayed = "0"
        Me.CheckEditAllowLeaveBasic.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowLeaveBasic.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowLeaveBasic.TabIndex = 85
        '
        'GroupControl4
        '
        Me.GroupControl4.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GroupControl4.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl4.AppearanceCaption.Options.UseFont = True
        Me.GroupControl4.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl4.Controls.Add(Me.CheckEditAllowReE10)
        Me.GroupControl4.Controls.Add(Me.CheckEditAllowReE8)
        Me.GroupControl4.Controls.Add(Me.CheckEditAllowReE6)
        Me.GroupControl4.Controls.Add(Me.CheckEditAllowReE4)
        Me.GroupControl4.Controls.Add(Me.CheckEditAllowReE2)
        Me.GroupControl4.Controls.Add(Me.CheckEditAllowReE9)
        Me.GroupControl4.Controls.Add(Me.CheckEditAllowReE7)
        Me.GroupControl4.Controls.Add(Me.CheckEditAllowReE5)
        Me.GroupControl4.Controls.Add(Me.CheckEditAllowReE3)
        Me.GroupControl4.Controls.Add(Me.CheckEditAllowReE1)
        Me.GroupControl4.Controls.Add(Me.CheckEditAllowReMedical)
        Me.GroupControl4.Controls.Add(Me.CheckEditAllowReConveyance)
        Me.GroupControl4.Location = New System.Drawing.Point(154, 3)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(226, 498)
        Me.GroupControl4.TabIndex = 1
        Me.GroupControl4.Text = "Reimbursement Allowed"
        '
        'CheckEditAllowReE10
        '
        Me.CheckEditAllowReE10.Location = New System.Drawing.Point(14, 312)
        Me.CheckEditAllowReE10.Name = "CheckEditAllowReE10"
        Me.CheckEditAllowReE10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowReE10.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowReE10.Properties.Caption = "Earning-10"
        Me.CheckEditAllowReE10.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowReE10.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowReE10.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowReE10.Properties.ValueChecked = "1"
        Me.CheckEditAllowReE10.Properties.ValueGrayed = "0"
        Me.CheckEditAllowReE10.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowReE10.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowReE10.TabIndex = 107
        '
        'CheckEditAllowReE8
        '
        Me.CheckEditAllowReE8.Location = New System.Drawing.Point(14, 262)
        Me.CheckEditAllowReE8.Name = "CheckEditAllowReE8"
        Me.CheckEditAllowReE8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowReE8.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowReE8.Properties.Caption = "Earning-08"
        Me.CheckEditAllowReE8.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowReE8.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowReE8.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowReE8.Properties.ValueChecked = "1"
        Me.CheckEditAllowReE8.Properties.ValueGrayed = "0"
        Me.CheckEditAllowReE8.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowReE8.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowReE8.TabIndex = 105
        '
        'CheckEditAllowReE6
        '
        Me.CheckEditAllowReE6.Location = New System.Drawing.Point(14, 212)
        Me.CheckEditAllowReE6.Name = "CheckEditAllowReE6"
        Me.CheckEditAllowReE6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowReE6.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowReE6.Properties.Caption = "Earning-06"
        Me.CheckEditAllowReE6.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowReE6.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowReE6.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowReE6.Properties.ValueChecked = "1"
        Me.CheckEditAllowReE6.Properties.ValueGrayed = "0"
        Me.CheckEditAllowReE6.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowReE6.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowReE6.TabIndex = 103
        '
        'CheckEditAllowReE4
        '
        Me.CheckEditAllowReE4.Location = New System.Drawing.Point(14, 162)
        Me.CheckEditAllowReE4.Name = "CheckEditAllowReE4"
        Me.CheckEditAllowReE4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowReE4.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowReE4.Properties.Caption = "Earning-04"
        Me.CheckEditAllowReE4.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowReE4.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowReE4.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowReE4.Properties.ValueChecked = "1"
        Me.CheckEditAllowReE4.Properties.ValueGrayed = "0"
        Me.CheckEditAllowReE4.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowReE4.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowReE4.TabIndex = 101
        '
        'CheckEditAllowReE2
        '
        Me.CheckEditAllowReE2.Location = New System.Drawing.Point(14, 112)
        Me.CheckEditAllowReE2.Name = "CheckEditAllowReE2"
        Me.CheckEditAllowReE2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowReE2.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowReE2.Properties.Caption = "Earning-02"
        Me.CheckEditAllowReE2.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowReE2.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowReE2.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowReE2.Properties.ValueChecked = "1"
        Me.CheckEditAllowReE2.Properties.ValueGrayed = "0"
        Me.CheckEditAllowReE2.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowReE2.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowReE2.TabIndex = 99
        '
        'CheckEditAllowReE9
        '
        Me.CheckEditAllowReE9.Location = New System.Drawing.Point(14, 287)
        Me.CheckEditAllowReE9.Name = "CheckEditAllowReE9"
        Me.CheckEditAllowReE9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowReE9.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowReE9.Properties.Caption = "Earning-09"
        Me.CheckEditAllowReE9.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowReE9.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowReE9.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowReE9.Properties.ValueChecked = "1"
        Me.CheckEditAllowReE9.Properties.ValueGrayed = "0"
        Me.CheckEditAllowReE9.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowReE9.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowReE9.TabIndex = 106
        '
        'CheckEditAllowReE7
        '
        Me.CheckEditAllowReE7.Location = New System.Drawing.Point(14, 237)
        Me.CheckEditAllowReE7.Name = "CheckEditAllowReE7"
        Me.CheckEditAllowReE7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowReE7.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowReE7.Properties.Caption = "Earning-07"
        Me.CheckEditAllowReE7.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowReE7.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowReE7.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowReE7.Properties.ValueChecked = "1"
        Me.CheckEditAllowReE7.Properties.ValueGrayed = "0"
        Me.CheckEditAllowReE7.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowReE7.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowReE7.TabIndex = 104
        '
        'CheckEditAllowReE5
        '
        Me.CheckEditAllowReE5.Location = New System.Drawing.Point(14, 187)
        Me.CheckEditAllowReE5.Name = "CheckEditAllowReE5"
        Me.CheckEditAllowReE5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowReE5.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowReE5.Properties.Caption = "Earning-05"
        Me.CheckEditAllowReE5.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowReE5.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowReE5.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowReE5.Properties.ValueChecked = "1"
        Me.CheckEditAllowReE5.Properties.ValueGrayed = "0"
        Me.CheckEditAllowReE5.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowReE5.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowReE5.TabIndex = 102
        '
        'CheckEditAllowReE3
        '
        Me.CheckEditAllowReE3.Location = New System.Drawing.Point(14, 137)
        Me.CheckEditAllowReE3.Name = "CheckEditAllowReE3"
        Me.CheckEditAllowReE3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowReE3.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowReE3.Properties.Caption = "Earning-03"
        Me.CheckEditAllowReE3.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowReE3.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowReE3.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowReE3.Properties.ValueChecked = "1"
        Me.CheckEditAllowReE3.Properties.ValueGrayed = "0"
        Me.CheckEditAllowReE3.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowReE3.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowReE3.TabIndex = 100
        '
        'CheckEditAllowReE1
        '
        Me.CheckEditAllowReE1.Location = New System.Drawing.Point(14, 87)
        Me.CheckEditAllowReE1.Name = "CheckEditAllowReE1"
        Me.CheckEditAllowReE1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowReE1.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowReE1.Properties.Caption = "Earning-01"
        Me.CheckEditAllowReE1.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowReE1.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowReE1.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowReE1.Properties.ValueChecked = "1"
        Me.CheckEditAllowReE1.Properties.ValueGrayed = "0"
        Me.CheckEditAllowReE1.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowReE1.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowReE1.TabIndex = 98
        '
        'CheckEditAllowReMedical
        '
        Me.CheckEditAllowReMedical.Location = New System.Drawing.Point(14, 62)
        Me.CheckEditAllowReMedical.Name = "CheckEditAllowReMedical"
        Me.CheckEditAllowReMedical.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowReMedical.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowReMedical.Properties.Caption = "Medical"
        Me.CheckEditAllowReMedical.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowReMedical.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowReMedical.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowReMedical.Properties.ValueChecked = "1"
        Me.CheckEditAllowReMedical.Properties.ValueGrayed = "0"
        Me.CheckEditAllowReMedical.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowReMedical.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowReMedical.TabIndex = 85
        '
        'CheckEditAllowReConveyance
        '
        Me.CheckEditAllowReConveyance.Location = New System.Drawing.Point(14, 37)
        Me.CheckEditAllowReConveyance.Name = "CheckEditAllowReConveyance"
        Me.CheckEditAllowReConveyance.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowReConveyance.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowReConveyance.Properties.Caption = "Conveyance"
        Me.CheckEditAllowReConveyance.Properties.DisplayValueChecked = "1"
        Me.CheckEditAllowReConveyance.Properties.DisplayValueGrayed = "0"
        Me.CheckEditAllowReConveyance.Properties.DisplayValueUnchecked = "0"
        Me.CheckEditAllowReConveyance.Properties.ValueChecked = "1"
        Me.CheckEditAllowReConveyance.Properties.ValueGrayed = "0"
        Me.CheckEditAllowReConveyance.Properties.ValueUnchecked = "0"
        Me.CheckEditAllowReConveyance.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowReConveyance.TabIndex = 84
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GroupControl3.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl3.Controls.Add(Me.CheckEditAllowGratuity)
        Me.GroupControl3.Controls.Add(Me.CheckEditAllowBonus)
        Me.GroupControl3.Controls.Add(Me.CheckEditAllowVPF)
        Me.GroupControl3.Controls.Add(Me.CheckEditAllowESI)
        Me.GroupControl3.Controls.Add(Me.CheckEditAllowPF)
        Me.GroupControl3.Controls.Add(Me.CheckEditAllowProfTax)
        Me.GroupControl3.Location = New System.Drawing.Point(3, 3)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(145, 498)
        Me.GroupControl3.TabIndex = 0
        Me.GroupControl3.Text = "General"
        '
        'CheckEditAllowGratuity
        '
        Me.CheckEditAllowGratuity.EditValue = "N"
        Me.CheckEditAllowGratuity.Location = New System.Drawing.Point(14, 162)
        Me.CheckEditAllowGratuity.Name = "CheckEditAllowGratuity"
        Me.CheckEditAllowGratuity.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowGratuity.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowGratuity.Properties.Caption = "Gratuity"
        Me.CheckEditAllowGratuity.Properties.DisplayValueChecked = "Y"
        Me.CheckEditAllowGratuity.Properties.DisplayValueGrayed = "N"
        Me.CheckEditAllowGratuity.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditAllowGratuity.Properties.ValueChecked = "Y"
        Me.CheckEditAllowGratuity.Properties.ValueGrayed = "N"
        Me.CheckEditAllowGratuity.Properties.ValueUnchecked = "N"
        Me.CheckEditAllowGratuity.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowGratuity.TabIndex = 89
        '
        'CheckEditAllowBonus
        '
        Me.CheckEditAllowBonus.EditValue = "N"
        Me.CheckEditAllowBonus.Location = New System.Drawing.Point(14, 137)
        Me.CheckEditAllowBonus.Name = "CheckEditAllowBonus"
        Me.CheckEditAllowBonus.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowBonus.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowBonus.Properties.Caption = "Bonus"
        Me.CheckEditAllowBonus.Properties.DisplayValueChecked = "Y"
        Me.CheckEditAllowBonus.Properties.DisplayValueGrayed = "N"
        Me.CheckEditAllowBonus.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditAllowBonus.Properties.ValueChecked = "Y"
        Me.CheckEditAllowBonus.Properties.ValueGrayed = "N"
        Me.CheckEditAllowBonus.Properties.ValueUnchecked = "N"
        Me.CheckEditAllowBonus.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowBonus.TabIndex = 88
        '
        'CheckEditAllowVPF
        '
        Me.CheckEditAllowVPF.EditValue = "N"
        Me.CheckEditAllowVPF.Location = New System.Drawing.Point(14, 112)
        Me.CheckEditAllowVPF.Name = "CheckEditAllowVPF"
        Me.CheckEditAllowVPF.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowVPF.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowVPF.Properties.Caption = "VPF"
        Me.CheckEditAllowVPF.Properties.DisplayValueChecked = "Y"
        Me.CheckEditAllowVPF.Properties.DisplayValueGrayed = "N"
        Me.CheckEditAllowVPF.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditAllowVPF.Properties.ValueChecked = "Y"
        Me.CheckEditAllowVPF.Properties.ValueGrayed = "N"
        Me.CheckEditAllowVPF.Properties.ValueUnchecked = "N"
        Me.CheckEditAllowVPF.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowVPF.TabIndex = 87
        '
        'CheckEditAllowESI
        '
        Me.CheckEditAllowESI.EditValue = "N"
        Me.CheckEditAllowESI.Location = New System.Drawing.Point(14, 87)
        Me.CheckEditAllowESI.Name = "CheckEditAllowESI"
        Me.CheckEditAllowESI.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowESI.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowESI.Properties.Caption = "ESI"
        Me.CheckEditAllowESI.Properties.DisplayValueChecked = "Y"
        Me.CheckEditAllowESI.Properties.DisplayValueGrayed = "N"
        Me.CheckEditAllowESI.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditAllowESI.Properties.ValueChecked = "Y"
        Me.CheckEditAllowESI.Properties.ValueGrayed = "N"
        Me.CheckEditAllowESI.Properties.ValueUnchecked = "N"
        Me.CheckEditAllowESI.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowESI.TabIndex = 86
        '
        'CheckEditAllowPF
        '
        Me.CheckEditAllowPF.EditValue = "N"
        Me.CheckEditAllowPF.Location = New System.Drawing.Point(14, 62)
        Me.CheckEditAllowPF.Name = "CheckEditAllowPF"
        Me.CheckEditAllowPF.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowPF.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowPF.Properties.Caption = "PF"
        Me.CheckEditAllowPF.Properties.DisplayValueChecked = "Y"
        Me.CheckEditAllowPF.Properties.DisplayValueGrayed = "N"
        Me.CheckEditAllowPF.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditAllowPF.Properties.ValueChecked = "Y"
        Me.CheckEditAllowPF.Properties.ValueGrayed = "N"
        Me.CheckEditAllowPF.Properties.ValueUnchecked = "N"
        Me.CheckEditAllowPF.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowPF.TabIndex = 85
        '
        'CheckEditAllowProfTax
        '
        Me.CheckEditAllowProfTax.EditValue = "N"
        Me.CheckEditAllowProfTax.Location = New System.Drawing.Point(14, 37)
        Me.CheckEditAllowProfTax.Name = "CheckEditAllowProfTax"
        Me.CheckEditAllowProfTax.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAllowProfTax.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAllowProfTax.Properties.Caption = "Prof. Tax"
        Me.CheckEditAllowProfTax.Properties.DisplayValueChecked = "Y"
        Me.CheckEditAllowProfTax.Properties.DisplayValueGrayed = "N"
        Me.CheckEditAllowProfTax.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditAllowProfTax.Properties.ValueChecked = "Y"
        Me.CheckEditAllowProfTax.Properties.ValueGrayed = "N"
        Me.CheckEditAllowProfTax.Properties.ValueUnchecked = "N"
        Me.CheckEditAllowProfTax.Size = New System.Drawing.Size(109, 19)
        Me.CheckEditAllowProfTax.TabIndex = 84
        '
        'XtraTabPage6
        '
        Me.XtraTabPage6.Controls.Add(Me.TextEditGrossEar10)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl34)
        Me.XtraTabPage6.Controls.Add(Me.TextEditGrossEar09)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl35)
        Me.XtraTabPage6.Controls.Add(Me.TextEditGrossEar08)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl32)
        Me.XtraTabPage6.Controls.Add(Me.TextEditGrossEar07)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl33)
        Me.XtraTabPage6.Controls.Add(Me.TextEditGrossEar06)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl31)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl24)
        Me.XtraTabPage6.Controls.Add(Me.TextEditGrossEar05)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl25)
        Me.XtraTabPage6.Controls.Add(Me.TextEditGrossEar04)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl26)
        Me.XtraTabPage6.Controls.Add(Me.TextEditGrossEar03)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl27)
        Me.XtraTabPage6.Controls.Add(Me.TextEditGrossEar02)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl28)
        Me.XtraTabPage6.Controls.Add(Me.TextEditGrossEar01)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl29)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl30)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl23)
        Me.XtraTabPage6.Controls.Add(Me.TextEditGrossMed)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl22)
        Me.XtraTabPage6.Controls.Add(Me.TextEditGrossConv)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl21)
        Me.XtraTabPage6.Controls.Add(Me.TextEditGrossHRA)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl20)
        Me.XtraTabPage6.Controls.Add(Me.TextEditGrossDA)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl19)
        Me.XtraTabPage6.Controls.Add(Me.TextEditGrossBasic)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl18)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl17)
        Me.XtraTabPage6.Name = "XtraTabPage6"
        Me.XtraTabPage6.Size = New System.Drawing.Size(1028, 504)
        Me.XtraTabPage6.Text = "Gross Setup"
        '
        'TextEditGrossEar10
        '
        Me.TextEditGrossEar10.Location = New System.Drawing.Point(414, 394)
        Me.TextEditGrossEar10.Name = "TextEditGrossEar10"
        Me.TextEditGrossEar10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditGrossEar10.Properties.Appearance.Options.UseFont = True
        Me.TextEditGrossEar10.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditGrossEar10.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditGrossEar10.Properties.MaxLength = 3
        Me.TextEditGrossEar10.Size = New System.Drawing.Size(72, 20)
        Me.TextEditGrossEar10.TabIndex = 33
        '
        'LabelControl34
        '
        Me.LabelControl34.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl34.Appearance.Options.UseFont = True
        Me.LabelControl34.Location = New System.Drawing.Point(322, 397)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl34.TabIndex = 32
        Me.LabelControl34.Text = "Earning-10"
        '
        'TextEditGrossEar09
        '
        Me.TextEditGrossEar09.Location = New System.Drawing.Point(414, 368)
        Me.TextEditGrossEar09.Name = "TextEditGrossEar09"
        Me.TextEditGrossEar09.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditGrossEar09.Properties.Appearance.Options.UseFont = True
        Me.TextEditGrossEar09.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditGrossEar09.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditGrossEar09.Properties.MaxLength = 3
        Me.TextEditGrossEar09.Size = New System.Drawing.Size(72, 20)
        Me.TextEditGrossEar09.TabIndex = 31
        '
        'LabelControl35
        '
        Me.LabelControl35.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl35.Appearance.Options.UseFont = True
        Me.LabelControl35.Location = New System.Drawing.Point(322, 371)
        Me.LabelControl35.Name = "LabelControl35"
        Me.LabelControl35.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl35.TabIndex = 30
        Me.LabelControl35.Text = "Earning-09"
        '
        'TextEditGrossEar08
        '
        Me.TextEditGrossEar08.Location = New System.Drawing.Point(414, 309)
        Me.TextEditGrossEar08.Name = "TextEditGrossEar08"
        Me.TextEditGrossEar08.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditGrossEar08.Properties.Appearance.Options.UseFont = True
        Me.TextEditGrossEar08.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditGrossEar08.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditGrossEar08.Properties.MaxLength = 3
        Me.TextEditGrossEar08.Size = New System.Drawing.Size(72, 20)
        Me.TextEditGrossEar08.TabIndex = 29
        '
        'LabelControl32
        '
        Me.LabelControl32.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl32.Appearance.Options.UseFont = True
        Me.LabelControl32.Location = New System.Drawing.Point(322, 312)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl32.TabIndex = 28
        Me.LabelControl32.Text = "Earning-08"
        '
        'TextEditGrossEar07
        '
        Me.TextEditGrossEar07.Location = New System.Drawing.Point(414, 283)
        Me.TextEditGrossEar07.Name = "TextEditGrossEar07"
        Me.TextEditGrossEar07.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditGrossEar07.Properties.Appearance.Options.UseFont = True
        Me.TextEditGrossEar07.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditGrossEar07.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditGrossEar07.Properties.MaxLength = 3
        Me.TextEditGrossEar07.Size = New System.Drawing.Size(72, 20)
        Me.TextEditGrossEar07.TabIndex = 27
        '
        'LabelControl33
        '
        Me.LabelControl33.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl33.Appearance.Options.UseFont = True
        Me.LabelControl33.Location = New System.Drawing.Point(322, 286)
        Me.LabelControl33.Name = "LabelControl33"
        Me.LabelControl33.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl33.TabIndex = 26
        Me.LabelControl33.Text = "Earning-07"
        '
        'TextEditGrossEar06
        '
        Me.TextEditGrossEar06.Location = New System.Drawing.Point(414, 231)
        Me.TextEditGrossEar06.Name = "TextEditGrossEar06"
        Me.TextEditGrossEar06.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditGrossEar06.Properties.Appearance.Options.UseFont = True
        Me.TextEditGrossEar06.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditGrossEar06.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditGrossEar06.Properties.MaxLength = 3
        Me.TextEditGrossEar06.Size = New System.Drawing.Size(72, 20)
        Me.TextEditGrossEar06.TabIndex = 25
        '
        'LabelControl31
        '
        Me.LabelControl31.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl31.Appearance.Options.UseFont = True
        Me.LabelControl31.Location = New System.Drawing.Point(322, 234)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl31.TabIndex = 24
        Me.LabelControl31.Text = "Earning-06"
        '
        'LabelControl24
        '
        Me.LabelControl24.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl24.Appearance.Options.UseFont = True
        Me.LabelControl24.Location = New System.Drawing.Point(414, 22)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(69, 14)
        Me.LabelControl24.TabIndex = 23
        Me.LabelControl24.Text = "Percentage"
        '
        'TextEditGrossEar05
        '
        Me.TextEditGrossEar05.Location = New System.Drawing.Point(414, 205)
        Me.TextEditGrossEar05.Name = "TextEditGrossEar05"
        Me.TextEditGrossEar05.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditGrossEar05.Properties.Appearance.Options.UseFont = True
        Me.TextEditGrossEar05.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditGrossEar05.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditGrossEar05.Properties.MaxLength = 3
        Me.TextEditGrossEar05.Size = New System.Drawing.Size(72, 20)
        Me.TextEditGrossEar05.TabIndex = 22
        '
        'LabelControl25
        '
        Me.LabelControl25.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl25.Appearance.Options.UseFont = True
        Me.LabelControl25.Location = New System.Drawing.Point(322, 208)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl25.TabIndex = 21
        Me.LabelControl25.Text = "Earning-05"
        '
        'TextEditGrossEar04
        '
        Me.TextEditGrossEar04.Location = New System.Drawing.Point(414, 156)
        Me.TextEditGrossEar04.Name = "TextEditGrossEar04"
        Me.TextEditGrossEar04.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditGrossEar04.Properties.Appearance.Options.UseFont = True
        Me.TextEditGrossEar04.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditGrossEar04.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditGrossEar04.Properties.MaxLength = 3
        Me.TextEditGrossEar04.Size = New System.Drawing.Size(72, 20)
        Me.TextEditGrossEar04.TabIndex = 20
        '
        'LabelControl26
        '
        Me.LabelControl26.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl26.Appearance.Options.UseFont = True
        Me.LabelControl26.Location = New System.Drawing.Point(322, 159)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl26.TabIndex = 19
        Me.LabelControl26.Text = "Earning-04"
        '
        'TextEditGrossEar03
        '
        Me.TextEditGrossEar03.Location = New System.Drawing.Point(414, 130)
        Me.TextEditGrossEar03.Name = "TextEditGrossEar03"
        Me.TextEditGrossEar03.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditGrossEar03.Properties.Appearance.Options.UseFont = True
        Me.TextEditGrossEar03.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditGrossEar03.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditGrossEar03.Properties.MaxLength = 3
        Me.TextEditGrossEar03.Size = New System.Drawing.Size(72, 20)
        Me.TextEditGrossEar03.TabIndex = 18
        '
        'LabelControl27
        '
        Me.LabelControl27.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl27.Appearance.Options.UseFont = True
        Me.LabelControl27.Location = New System.Drawing.Point(322, 133)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl27.TabIndex = 17
        Me.LabelControl27.Text = "Earning-03"
        '
        'TextEditGrossEar02
        '
        Me.TextEditGrossEar02.Location = New System.Drawing.Point(414, 78)
        Me.TextEditGrossEar02.Name = "TextEditGrossEar02"
        Me.TextEditGrossEar02.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditGrossEar02.Properties.Appearance.Options.UseFont = True
        Me.TextEditGrossEar02.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditGrossEar02.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditGrossEar02.Properties.MaxLength = 3
        Me.TextEditGrossEar02.Size = New System.Drawing.Size(72, 20)
        Me.TextEditGrossEar02.TabIndex = 16
        '
        'LabelControl28
        '
        Me.LabelControl28.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl28.Appearance.Options.UseFont = True
        Me.LabelControl28.Location = New System.Drawing.Point(322, 81)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl28.TabIndex = 15
        Me.LabelControl28.Text = "Earning-02"
        '
        'TextEditGrossEar01
        '
        Me.TextEditGrossEar01.Location = New System.Drawing.Point(414, 52)
        Me.TextEditGrossEar01.Name = "TextEditGrossEar01"
        Me.TextEditGrossEar01.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditGrossEar01.Properties.Appearance.Options.UseFont = True
        Me.TextEditGrossEar01.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditGrossEar01.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditGrossEar01.Properties.MaxLength = 3
        Me.TextEditGrossEar01.Size = New System.Drawing.Size(72, 20)
        Me.TextEditGrossEar01.TabIndex = 14
        '
        'LabelControl29
        '
        Me.LabelControl29.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl29.Appearance.Options.UseFont = True
        Me.LabelControl29.Location = New System.Drawing.Point(322, 55)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl29.TabIndex = 13
        Me.LabelControl29.Text = "Earning-01"
        '
        'LabelControl30
        '
        Me.LabelControl30.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl30.Appearance.Options.UseFont = True
        Me.LabelControl30.Location = New System.Drawing.Point(332, 22)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(31, 14)
        Me.LabelControl30.TabIndex = 12
        Me.LabelControl30.Text = "Head"
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl23.Appearance.Options.UseFont = True
        Me.LabelControl23.Location = New System.Drawing.Point(115, 22)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(69, 14)
        Me.LabelControl23.TabIndex = 11
        Me.LabelControl23.Text = "Percentage"
        '
        'TextEditGrossMed
        '
        Me.TextEditGrossMed.Location = New System.Drawing.Point(115, 156)
        Me.TextEditGrossMed.Name = "TextEditGrossMed"
        Me.TextEditGrossMed.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditGrossMed.Properties.Appearance.Options.UseFont = True
        Me.TextEditGrossMed.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditGrossMed.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditGrossMed.Properties.MaxLength = 3
        Me.TextEditGrossMed.Size = New System.Drawing.Size(72, 20)
        Me.TextEditGrossMed.TabIndex = 10
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl22.Appearance.Options.UseFont = True
        Me.LabelControl22.Location = New System.Drawing.Point(23, 159)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(39, 14)
        Me.LabelControl22.TabIndex = 9
        Me.LabelControl22.Text = "Medical"
        '
        'TextEditGrossConv
        '
        Me.TextEditGrossConv.Location = New System.Drawing.Point(115, 130)
        Me.TextEditGrossConv.Name = "TextEditGrossConv"
        Me.TextEditGrossConv.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditGrossConv.Properties.Appearance.Options.UseFont = True
        Me.TextEditGrossConv.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditGrossConv.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditGrossConv.Properties.MaxLength = 3
        Me.TextEditGrossConv.Size = New System.Drawing.Size(72, 20)
        Me.TextEditGrossConv.TabIndex = 8
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(23, 133)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl21.TabIndex = 7
        Me.LabelControl21.Text = "Conveyance"
        '
        'TextEditGrossHRA
        '
        Me.TextEditGrossHRA.Location = New System.Drawing.Point(115, 104)
        Me.TextEditGrossHRA.Name = "TextEditGrossHRA"
        Me.TextEditGrossHRA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditGrossHRA.Properties.Appearance.Options.UseFont = True
        Me.TextEditGrossHRA.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditGrossHRA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditGrossHRA.Properties.MaxLength = 3
        Me.TextEditGrossHRA.Size = New System.Drawing.Size(72, 20)
        Me.TextEditGrossHRA.TabIndex = 6
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl20.Appearance.Options.UseFont = True
        Me.LabelControl20.Location = New System.Drawing.Point(23, 107)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(23, 14)
        Me.LabelControl20.TabIndex = 5
        Me.LabelControl20.Text = "HRA"
        '
        'TextEditGrossDA
        '
        Me.TextEditGrossDA.Location = New System.Drawing.Point(115, 78)
        Me.TextEditGrossDA.Name = "TextEditGrossDA"
        Me.TextEditGrossDA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditGrossDA.Properties.Appearance.Options.UseFont = True
        Me.TextEditGrossDA.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditGrossDA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditGrossDA.Properties.MaxLength = 3
        Me.TextEditGrossDA.Size = New System.Drawing.Size(72, 20)
        Me.TextEditGrossDA.TabIndex = 4
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Location = New System.Drawing.Point(23, 81)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(16, 14)
        Me.LabelControl19.TabIndex = 3
        Me.LabelControl19.Text = "DA"
        '
        'TextEditGrossBasic
        '
        Me.TextEditGrossBasic.Location = New System.Drawing.Point(115, 52)
        Me.TextEditGrossBasic.Name = "TextEditGrossBasic"
        Me.TextEditGrossBasic.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditGrossBasic.Properties.Appearance.Options.UseFont = True
        Me.TextEditGrossBasic.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditGrossBasic.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditGrossBasic.Properties.MaxLength = 3
        Me.TextEditGrossBasic.Size = New System.Drawing.Size(72, 20)
        Me.TextEditGrossBasic.TabIndex = 2
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl18.Appearance.Options.UseFont = True
        Me.LabelControl18.Location = New System.Drawing.Point(23, 55)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(26, 14)
        Me.LabelControl18.TabIndex = 1
        Me.LabelControl18.Text = "Basic"
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(23, 22)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(31, 14)
        Me.LabelControl17.TabIndex = 0
        Me.LabelControl17.Text = "Head"
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.SimpleButtonSave)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel1.Location = New System.Drawing.Point(0, 534)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1036, 34)
        Me.SidePanel1.TabIndex = 1
        Me.SidePanel1.Text = "SidePanel1"
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(6, 6)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 0
        Me.SimpleButtonSave.Text = "Save"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(101, 568)
        Me.MemoEdit1.TabIndex = 1
        '
        'PAY_FORMULATableAdapter
        '
        Me.PAY_FORMULATableAdapter.ClearBeforeFill = True
        '
        'PaY_FORMULA1TableAdapter1
        '
        Me.PaY_FORMULA1TableAdapter1.ClearBeforeFill = True
        '
        'TblLeaveMasterTableAdapter
        '
        Me.TblLeaveMasterTableAdapter.ClearBeforeFill = True
        '
        'TblLeaveMaster1TableAdapter1
        '
        Me.TblLeaveMaster1TableAdapter1.ClearBeforeFill = True
        '
        'XtraPayrollSetup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraPayrollSetup"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.PictureEdit21.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit20.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit19.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit18.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit17.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit16.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit15.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndD11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit14.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndD10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndD9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndD8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndD7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndD6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndD5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndD4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndD3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndD2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndD1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditTDSE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PAYFORMULABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.PictureEdit10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditOT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditMR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditCR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditDA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditHRA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditBR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndE10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndE9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndE8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndE7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndE6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndE5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndE4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndE3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndE2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditRndE1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        Me.XtraTabPage2.PerformLayout()
        CType(Me.CheckEditPFE10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPFE8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPFE6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPFE4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPFE2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPFE9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPFE7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPFE5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPFE3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPFE1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPFMedical.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPFConv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPFDA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPFHRA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPFBasic.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPF22.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPF21.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPF02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPFRound.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPFVPFDec.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPFPFDec.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPFFPRDec.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPFEPRDec.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPFPRErDec.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPFLimite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage3.ResumeLayout(False)
        Me.XtraTabPage3.PerformLayout()
        CType(Me.CheckEditESIEOT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESIE10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESIE8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESIE6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESIE4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESIE2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESIE9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESIE7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESIE5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESIE3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESIE1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESIMedical.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESIConv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESIDA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESIHRA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESIBasic.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditESIRound.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditESIEpDec.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditESIErDec.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditESILimite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage4.ResumeLayout(False)
        Me.XtraTabPage4.PerformLayout()
        CType(Me.TextEditGratuityFormula.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditBonusRate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditBonusAllOnArr.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditBonusAmtLimit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditBonusWageLimit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditBonusMinWorking.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage5.ResumeLayout(False)
        CType(Me.GridControlLeave, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblLeaveMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewLeave, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        CType(Me.CheckEditchklvPF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowLeaveMedical.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowLeaveConveyance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowLeaveDA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowLeaveHRA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowLeaveE10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowLeaveE8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowLeaveE6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowLeaveE4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowLeaveE2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowLeaveE9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowLeaveE7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowLeaveE5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowLeaveE3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowLeaveE1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowLeaveBasic.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.CheckEditAllowReE10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowReE8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowReE6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowReE4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowReE2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowReE9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowReE7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowReE5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowReE3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowReE1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowReMedical.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowReConveyance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.CheckEditAllowGratuity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowBonus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowVPF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowESI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowPF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAllowProfTax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage6.ResumeLayout(False)
        Me.XtraTabPage6.PerformLayout()
        CType(Me.TextEditGrossEar10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditGrossEar09.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditGrossEar08.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditGrossEar07.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditGrossEar06.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditGrossEar05.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditGrossEar04.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditGrossEar03.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditGrossEar02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditGrossEar01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditGrossMed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditGrossConv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditGrossHRA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditGrossDA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditGrossBasic.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents SSSDBDataSet As ULtra.SSSDBDataSet
    Friend WithEvents PAYFORMULABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PAY_FORMULATableAdapter As ULtra.SSSDBDataSetTableAdapters.PAY_FORMULATableAdapter
    Friend WithEvents PaY_FORMULA1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.PAY_FORMULA1TableAdapter
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEditRndD11 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditRndD10 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditRndD9 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditRndD8 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditRndD7 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditRndD6 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditRndD5 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditRndD4 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditRndD3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditRndD2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEditRndD1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditTDSE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridLookUpEditFormulaD11 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView21 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn41 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn42 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TextEditRate10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditRate9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditRate8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditRate7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditRate6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditRate5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditRate4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditRate3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditRate2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditRate1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridLookUpEditFormulaD10 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView11 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn21 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn22 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD9 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView12 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn23 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn24 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD8 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView13 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn25 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn26 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD7 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView14 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn27 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn28 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD6 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView15 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn29 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn30 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD5 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView16 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn31 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn32 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD4 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView17 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn33 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn34 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD3 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView18 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn35 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn36 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD2 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView19 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn37 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn38 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD1 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView20 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn39 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn40 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEditRndE10 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditRndE9 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditRndE8 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditRndE7 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditRndE6 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditRndE5 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditRndE4 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditRndE3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditRndE2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEditRndE1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GridLookUpEditFormulaE10 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView10 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn19 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn20 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE9 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView9 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn17 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn18 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE8 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView8 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE7 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView7 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE6 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView6 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE5 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView5 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE4 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE3 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE2 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE1 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl46 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRateE10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescpE10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl47 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRateE9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescpE9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl48 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRateE8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescpE8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl49 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRateE7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescpE7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl50 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRateE6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescpE6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditRateE5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl45 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditDescpE1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescpE5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditRateE1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl44 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRateE4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl43 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditDescpE4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditDescpE2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditRateE3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditRateE2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescpE3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage4 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage5 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage6 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents CheckEditBR As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditHRA As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditDA As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditOT As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditMR As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditCR As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditGrossBasic As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditGrossDA As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditGrossMed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditGrossConv As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditGrossHRA As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditGrossEar05 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditGrossEar04 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditGrossEar03 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditGrossEar02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditGrossEar01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditGrossEar06 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditGrossEar08 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditGrossEar07 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditGrossEar10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditGrossEar09 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditPFPRErDec As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl37 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditPFLimite As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditPFEPRDec As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl51 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditPFFPRDec As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl52 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditPFVPFDec As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl54 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditPFPFDec As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl53 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditPFRound As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl55 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditPF21 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl57 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditPF02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl56 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditPF22 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl58 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl59 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEditPFE9 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPFE7 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPFE5 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPFE3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPFE1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPFMedical As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPFConv As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPFDA As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPFHRA As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPFBasic As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPFE10 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPFE8 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPFE6 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPFE4 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPFE2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESIE10 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESIE8 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESIE6 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESIE4 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESIE2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESIE9 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESIE7 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESIE5 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESIE3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESIE1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESIMedical As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESIConv As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESIDA As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESIHRA As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESIBasic As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl60 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditESIRound As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl61 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditESIEpDec As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl62 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditESIErDec As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl63 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditESILimite As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl64 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEditESIEOT As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEditBonusMinWorking As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl65 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditBonusWageLimit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl66 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditBonusAmtLimit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl67 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEditBonusAllOnArr As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl68 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl69 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditBonusRate As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl70 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditGratuityFormula As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl71 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEditAllowGratuity As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowBonus As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowVPF As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowESI As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowPF As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowProfTax As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEditAllowReMedical As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowReConveyance As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowReE10 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowReE8 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowReE6 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowReE4 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowReE2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowReE9 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowReE7 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowReE5 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowReE3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowReE1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEditAllowLeaveMedical As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowLeaveConveyance As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowLeaveDA As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowLeaveHRA As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowLeaveE10 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowLeaveE8 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowLeaveE6 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowLeaveE4 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowLeaveE2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowLeaveE9 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowLeaveE7 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowLeaveE5 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowLeaveE3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowLeaveE1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAllowLeaveBasic As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GridControlLeave As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewLeave As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TblLeaveMasterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents colLEAVEFIELD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLEAVECODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblLeaveMasterTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblLeaveMasterTableAdapter
    Friend WithEvents TblLeaveMaster1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblLeaveMaster1TableAdapter
    Friend WithEvents CheckEditchklvPF As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PictureEdit21 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit20 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit19 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit18 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit17 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit16 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit15 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit14 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit13 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit12 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit11 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit10 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit9 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit8 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit7 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit6 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit5 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit4 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit3 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit2 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit1 As DevExpress.XtraEditors.PictureEdit

End Class
