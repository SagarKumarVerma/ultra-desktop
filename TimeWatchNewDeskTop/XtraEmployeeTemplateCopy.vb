﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraEditors.Repository
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports DevExpress.XtraGrid
Imports System.Drawing.Imaging


Imports libzkfpcsharp
Imports System.Runtime.InteropServices
Imports System.Threading
Imports Sample
Imports System.Text
Imports DevExpress.XtraLayout.Customization
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports Sample


Public Class XtraEmployeeTemplateCopy
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    'for finger print
    Dim IsRegister As Boolean = False
    Dim RegisterCount As Integer = 0
    Dim cbRegTmp As Integer = 0
    Dim mDBHandle As IntPtr = IntPtr.Zero
    Dim mDevHandle As IntPtr = IntPtr.Zero
    Dim FormHandle As IntPtr = IntPtr.Zero
    Dim iFid As Integer = 1
    Dim RegTmps()() As Byte = New Byte(3 - 1)() {}
    Dim mfpWidth As Integer = 0
    Dim mfpHeight As Integer = 0
    Dim FPBuffer() As Byte
    Dim bIsTimeToDie As Boolean = False
    Dim cbCapTmp As Integer = 2048
    Dim CapTmp() As Byte = New Byte(2048 - 1) {}
    Dim RegTmp() As Byte = New Byte(2048 - 1) {}
    Dim REGISTER_FINGER_COUNT As Integer = 3
    Dim bIdentify As Boolean = True

    Dim MESSAGE_CAPTURED_OK As Integer = (1024 + 6)
    Dim strBase64 As String

    Public Shared Vimage As Image
    Public Shared VName As String
    Public Shared VCom As String
    Public Shared VToMeet As String
    Public Shared VDept As String
    Public Shared VDate As String
    Public Shared VTime As String
    Public Shared VSrNo As String

    Dim captureThread As Thread

    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
           Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        Else
            TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
        End If
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
    End Sub
    Private Sub XtraEmployeeTemplateCopy_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If Common.servername = "Access" Then
            Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine1
        Else
            Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine
        End If
        GridControl1.DataSource = Common.MachineNonAdmin

        SimpleButtonInit.Enabled = True
        SimpleButtonStop.Enabled = False
        picFPImg.Image = Nothing
        FormHandle = Me.Handle  'used for finger print
        setDeviceGrid()
        Try
            'zkfp2.Terminate()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub setDeviceGrid()
        Dim gridtblregisterselet As String
        gridtblregisterselet = "select * from tblMachine where DeviceType = 'ZK(TFT)' or DeviceType='Bio-1Pro/ATF305Pro/ATF686Pro' "
        Dim WTDataTable As DataTable
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridtblregisterselet, Common.con1)
            WTDataTable = New DataTable("tblMachine")
            dataAdapter.Fill(WTDataTable)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridtblregisterselet, Common.con)
            WTDataTable = New DataTable("tblMachine")
            dataAdapter.Fill(WTDataTable)
        End If
        GridControl1.DataSource = WTDataTable
    End Sub
    Private Sub SimpleButtonInit_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonInit.Click
        'Dim ret As Integer '= zkfperrdef.ZKFP_ERR_OK
        'ret = zkfp2.Init()
        ''If (ret = zkfp2.Init()) = zkfperrdef.ZKFP_ERR_OK Then
        'If ret = zkfperrdef.ZKFP_ERR_OK Then
        '    Dim nCount As Integer = zkfp2.GetDeviceCount()
        '    If nCount = 0 Then
        '        zkfp2.Terminate()
        '        XtraMessageBox.Show(ulf, "<size=10>No device connected!</size>", "<size=9>Error</size>")
        '    Else
        '        bnOpen()
        '        bnEnroll()
        '        SimpleButtonInit.Enabled = False
        '        SimpleButtonStop.Enabled = True
        '    End If
        'Else
        '    XtraMessageBox.Show(ulf, "<size=10>No Device Found!</size>", "<size=9>Error</size>")
        '    'XtraMessageBox.Show(ulf, "<size=10>Initialize fail, ret=" & ret & " !</size>", "<size=9>Error</size>")
        'End If
    End Sub
    '<DllImport("user32.dll", EntryPoint:="SendMessageA")>
    'Public Shared Function SendMessage(ByVal hwnd As IntPtr, ByVal wMsg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As Integer
    'End Function
    'Private Sub bnOpen()
    '    Dim ret As Integer = zkfp.ZKFP_ERR_OK
    '    mDevHandle = zkfp2.OpenDevice(0)
    '    If (IntPtr.Zero = mDevHandle) Then '0 is the index of device connected, we have only one device so 0
    '        XtraMessageBox.Show(ulf, "<size=10>OpenDevice fail</size>", "<size=9>Error</size>")
    '        'MessageBox.Show("OpenDevice fail")
    '        Return
    '    End If
    '    mDBHandle = zkfp2.DBInit()
    '    If (IntPtr.Zero = mDBHandle) Then
    '        XtraMessageBox.Show(ulf, "<size=10Init DB fail</size>", "<size=9>Error</size>")
    '        'MessageBox.Show("Init DB fail")
    '        zkfp2.CloseDevice(mDevHandle)
    '        mDevHandle = IntPtr.Zero
    '        Return
    '    End If

    '    RegisterCount = 0
    '    cbRegTmp = 0
    '    iFid = 1

    '    'Do While (i < 3)
    '    '    RegTmps(i) = New Byte(2048 - 1) {}
    '    '    i = (i + 1)
    '    'Loop
    '    For i = 0 To 2
    '        RegTmps(i) = New Byte(2048 - 1) {}
    '    Next
    '    Dim paramValue() As Byte = New Byte(4 - 1) {}
    '    Dim size As Integer = 4
    '    zkfp2.GetParameters(mDevHandle, 1, paramValue, size)
    '    zkfp2.ByteArray2Int(paramValue, mfpWidth)
    '    size = 4
    '    zkfp2.GetParameters(mDevHandle, 2, paramValue, size)
    '    zkfp2.ByteArray2Int(paramValue, mfpHeight)
    '    FPBuffer = New Byte((mfpWidth * mfpHeight) - 1) {}
    '    captureThread = New Thread(New ThreadStart(AddressOf DoCapture))
    '    captureThread.IsBackground = True
    '    captureThread.Start()
    '    bIsTimeToDie = False
    '    textRes.Text = "Open succ"
    'End Sub
    'Private Sub DoCapture()
    '    While Not bIsTimeToDie
    '        cbCapTmp = 2048
    '        Try
    '            Dim ret As Integer = zkfp2.AcquireFingerprint(mDevHandle, FPBuffer, CapTmp, cbCapTmp)
    '            If (ret = zkfp.ZKFP_ERR_OK) Then
    '                SendMessage(FormHandle, MESSAGE_CAPTURED_OK, IntPtr.Zero, IntPtr.Zero)
    '            End If
    '        Catch ex As Exception

    '        End Try
    '        Thread.Sleep(200)
    '    End While

    'End Sub
    'Private Sub bnEnroll()
    '    If Not IsRegister Then
    '        IsRegister = True
    '        RegisterCount = 0
    '        cbRegTmp = 0
    '        textRes.Text = "Please press your finger 3 times!"
    '    End If
    'End Sub
    'Protected Overrides Sub DefWndProc(ByRef m As System.Windows.Forms.Message)
    '    Select Case (m.Msg)
    '        Case MESSAGE_CAPTURED_OK
    '            Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream
    '            Sample.BitmapFormat.GetBitmap(FPBuffer, mfpWidth, mfpHeight, ms)
    '            Dim bmp As Bitmap = New Bitmap(ms)
    '            Me.picFPImg.Image = bmp
    '            'Me.PictureBox1.Image = bmp
    '            If IsRegister Then
    '                Dim ret As Integer = zkfp.ZKFP_ERR_OK
    '                Dim score As Integer = 0
    '                Dim fid As Integer = 0
    '                ret = zkfp2.DBIdentify(mDBHandle, CapTmp, fid, score)
    '                If (zkfp.ZKFP_ERR_OK = ret) Then
    '                    textRes.Text = ("This finger was already register by " & (fid & "!"))
    '                    Return
    '                End If

    '                'If ((RegisterCount > 0) And (zkfp2.DBMatch(mDBHandle, CapTmp, RegTmps((RegisterCount - 1))) <= 0)) Then
    '                If (RegisterCount > 0) Then
    '                    If zkfp2.DBMatch(mDBHandle, CapTmp, RegTmps(RegisterCount - 1)) <= 0 Then
    '                        textRes.Text = "Please press the same finger 3 times for the enrollment"
    '                        Return
    '                    End If
    '                End If

    '                Array.Copy(CapTmp, RegTmps(RegisterCount), cbCapTmp)
    '                strBase64 = zkfp2.BlobToBase64(CapTmp, cbCapTmp)
    '                Dim blob() As Byte = zkfp2.Base64ToBlob(strBase64)
    '                RegisterCount = (RegisterCount + 1)
    '                If (RegisterCount >= REGISTER_FINGER_COUNT) Then
    '                    RegisterCount = 0
    '                    If ((zkfp.ZKFP_ERR_OK = zkfp2.DBMerge(mDBHandle, RegTmps(0), RegTmps(1), RegTmps(2), RegTmp, cbRegTmp)) _
    '                                AndAlso (zkfp.ZKFP_ERR_OK = zkfp2.DBAdd(mDBHandle, iFid, RegTmp))) Then
    '                        iFid = (iFid + 1)
    '                        textRes.Text = "enroll succ"
    '                        'SimpleButtonFPCopy.Enabled = True
    '                    Else
    '                        textRes.Text = ("enroll fail, error code=" & ret)
    '                    End If

    '                    IsRegister = False
    '                    Return
    '                Else
    '                    textRes.Text = ("You need to press the " & ((REGISTER_FINGER_COUNT - RegisterCount) & " times fingerprint"))
    '                End If
    '            Else
    '                If (cbRegTmp <= 0) Then
    '                    textRes.Text = "Please register your finger first!"
    '                    Return
    '                End If

    '                If bIdentify Then
    '                    Dim ret As Integer = zkfp.ZKFP_ERR_OK
    '                    Dim score As Integer = 0
    '                    Dim fid As Integer = 0
    '                    ret = zkfp2.DBIdentify(mDBHandle, CapTmp, fid, score)
    '                    If (zkfp.ZKFP_ERR_OK = ret) Then
    '                        textRes.Text = ("Identify succ, fid= " & fid & ",score=" & score & "!")
    '                        Return
    '                    Else
    '                        textRes.Text = ("Identify fail, ret= " & ret)
    '                        Return
    '                    End If

    '                Else
    '                    Dim ret As Integer = zkfp2.DBMatch(mDBHandle, CapTmp, RegTmp)
    '                    If (0 < ret) Then
    '                        textRes.Text = ("Match finger succ, score=" & (ret & "!"))
    '                        Return
    '                    Else
    '                        textRes.Text = ("Match finger fail, ret= " & ret)
    '                        Return
    '                    End If
    '                End If
    '            End If
    '            'Me.picFPImg.EditValue = RegTmps(RegisterCount) 'CapTmp   'nitin
    '        Case Else
    '            MyBase.DefWndProc(m)
    '    End Select
    'End Sub
    'Private Sub SimpleButtonStop_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonStop.Click
    '    'zkfp2.Terminate()
    '    SimpleButtonInit.Enabled = True
    '    SimpleButtonStop.Enabled = False
    'End Sub
    'Private Sub FPCopy() 'Handles SimpleButtonFPCopy.Click
    '    If TextFingerNo.Text.Trim = "" Then
    '        XtraMessageBox.Show(ulf, "<size=10>Finger No cannot be Empty!</size>", "<size=9>Error</size>")
    '        Exit Sub
    '    End If
    '    If strBase64 = "" Then
    '        XtraMessageBox.Show(ulf, "<size=10>Finger Template cannot be Empty!</size>", "<size=9>Error</size>")
    '        Exit Sub
    '    End If
    '    Dim selectedRows As Integer() = GridView1.GetSelectedRows()
    '    Dim result As Object() = New Object(selectedRows.Length - 1) {}
    '    Dim LstMachineId As String
    '    For i = 0 To selectedRows.Length - 1
    '        Dim rowHandle As Integer = selectedRows(i)
    '        If Not GridView1.IsGroupRow(rowHandle) Then
    '            LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
    '            Dim sdwEnrollNumber As String
    '            Dim sName As String = ""
    '            Dim sPassword As String = ""
    '            Dim iPrivilege As Integer
    '            Dim idwFingerIndex As Integer
    '            Dim sTmpData As String = ""
    '            Dim sEnabled As String = ""
    '            Dim bEnabled As Boolean = False
    '            Dim iflag As Integer

    '            Dim lpszIPAddress As String = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
    '            Dim vpszIPAddress As String = Trim(lpszIPAddress)

    '            Dim bIsConnected = False
    '            Dim iMachineNumber As Integer = result(i)
    '            Dim idwErrorCode As Integer
    '            Dim com As Common = New Common
    '            Dim axCZKEM1 As New zkemkeeper.CZKEM
    '            bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
    '            If bIsConnected = True Then
    '                axCZKEM1.EnableDevice(iMachineNumber, False)
    '                XtraMasterTest.LabelControlStatus.Text = "Uploading Template " & XtraEmployeeEdit.UserNoForTemplate
    '                Application.DoEvents()

    '                sdwEnrollNumber = XtraEmployeeEdit.UserNoForTemplate ' TextEditPhone.Text.Trim 'Convert.ToInt32(RsFp.Tables(0).Rows(0)("enrollnumber").ToString())
    '                If IsNumeric(sdwEnrollNumber) = True Then
    '                    sdwEnrollNumber = Convert.ToDouble(XtraEmployeeEdit.UserNoForTemplate)
    '                End If
    '                sName = XtraEmployeeEdit.NameForTemplate 'TextEditName.Text.Trim ' dstmp.Tables(0).Rows(0)("EMPNAME").ToString().Trim()
    '                idwFingerIndex = TextFingerNo.Text '6 ' Convert.ToInt32(RsFp.Tables(0).Rows(0)("FingerNumber").ToString().Trim())
    '                sTmpData = strBase64 'Encoding.UTF8.GetString(dsRecord.Tables(0).Rows(0).Item("vFingerImage"), 0, dsRecord.Tables(0).Rows(0).Item("vFingerImage").Length) 'dsRecord.Tables(0).Rows(0).Item("vFingerImage")
    '                'Dim sTmpDataByte() As Byte = dsRecord.Tables(0).Rows(0).Item("vFingerImage")
    '                iPrivilege = 0 'Convert.ToInt32(RsFp.Tables(0).Rows(0)("Privilege").ToString().Trim()) 'told by ajitesh(used for admin)
    '                sPassword = "" 'RsFp.Tables(0).Rows(0)("Password").ToString().Trim()
    '                sEnabled = "true" 'RsFp.Tables(0).Rows(0)(6).ToString().Trim()
    '                iflag = 0 'Convert.ToInt32(RsFp.Tables(0).Rows(0)(7).ToString())' as per old s/w
    '                'Dim icard As String = "" 'RsFp.Tables(0).Rows(0)("CardNumber").ToString().Trim()
    '                If sEnabled.ToString().ToLower() = "true" Then
    '                    bEnabled = True
    '                Else
    '                    bEnabled = False
    '                End If
    '                'Dim iBackupNumber As String
    '                Dim rs As Integer
    '                If axCZKEM1.SSR_SetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled) Then 'upload user information to the device
    '                    rs = axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iflag, sTmpData) 'upload templates information to the device
    '                    'axCZKEM1.SetUserValidDate(iMachineNumber, sdwEnrollNumber, 1, 0, DateEditStart.DateTime.ToString("yyyy-MM-dd HH:mm:00"), DateEditEnd.DateTime.ToString("yyyy-MM-dd HH:mm:59"))  'add valdity
    '                Else
    '                    axCZKEM1.GetLastError(idwErrorCode)
    '                    XtraMessageBox.Show(ulf, "<size=10>Operation failed,ErrorCode=" & idwErrorCode.ToString() & "</size>", "<size=9>Error<size=9>")
    '                    axCZKEM1.EnableDevice(iMachineNumber, True)
    '                    'Cursor = Cursors.Default
    '                    Return
    '                End If
    '                axCZKEM1.EnableDevice(iMachineNumber, True)
    '            Else
    '                axCZKEM1.GetLastError(idwErrorCode)
    '                XtraMessageBox.Show(ulf, "<size=10>Operation failed, Please Try Again </size>", "<size=9>Error<size=9>")
    '                Continue For
    '            End If
    '        End If
    '    Next
    '    XtraMasterTest.LabelControlStatus.Text = ""
    '    Application.DoEvents()
    '    Me.Close()
    'End Sub
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        'FPCopy()
    End Sub
    Private Sub XtraEmployeeTemplateCopy_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Try
            'zkfp2.Terminate()
            captureThread.Abort()
        Catch ex As Exception

        End Try
    End Sub
End Class