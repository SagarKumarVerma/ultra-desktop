﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraLeaveReport
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraLeaveReport))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.PopupContainerControlEmp = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlEmp = New DevExpress.XtraGrid.GridControl()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New ULtra.SSSDBDataSet()
        Me.GridViewEmp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPAYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlDept = New DevExpress.XtraEditors.PopupContainerControl()
        Me.PopupContainerControlShift = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlShift = New DevExpress.XtraGrid.GridControl()
        Me.TblShiftMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewShift = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colSHIFT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSTARTTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.colENDTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit5 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.GridControlDept = New DevExpress.XtraGrid.GridControl()
        Me.TblDepartmentBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewDept = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colDEPARTMENTCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEPARTMENTNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlComp = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlComp = New DevExpress.XtraGrid.GridControl()
        Me.TblCompanyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewComp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCOMPANYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlCat = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlCat = New DevExpress.XtraGrid.GridControl()
        Me.TblCatagoryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewCat = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCATAGORYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit4 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlGrade = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlGrade = New DevExpress.XtraGrid.GridControl()
        Me.TblGradeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewGrade = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit6 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlBranch = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlBranch = New DevExpress.XtraGrid.GridControl()
        Me.GridViewBranch = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBRANCHCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRANCHNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit7 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.SidePanelSelection = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditEmp = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerEditGrade = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.PopupContainerEditLocation = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerEditComp = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditShift = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerEditDept = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditCat = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.GridLookUpEdit1 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblLeaveMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colLEAVEFIELD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLEAVECODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLEAVEDESCRIPTION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckDeptSkip = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckLeaveRegister = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckBalanceLeaves = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckConsumedLeaves = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckAccruedLeaves = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckLeaveCard = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckSanctionedLeaves = New DevExpress.XtraEditors.CheckEdit()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.ComboNepaliYearTo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.ComboNEpaliMonthTo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.CheckExcel = New DevExpress.XtraEditors.CheckEdit()
        Me.ComboNepaliDateTo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.CheckText = New DevExpress.XtraEditors.CheckEdit()
        Me.ComboNepaliYearFrm = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.ComboNEpaliMonthFrm = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.DateEdit2 = New DevExpress.XtraEditors.DateEdit()
        Me.ComboNepaliDateFrm = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblLeaveMasterTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblLeaveMasterTableAdapter()
        Me.TblLeaveMaster1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblLeaveMaster1TableAdapter()
        Me.Tblbranch1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblbranch1TableAdapter()
        Me.TblbranchTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblbranchTableAdapter()
        Me.TblDepartmentTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter()
        Me.TblEmployee1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        Me.TblEmployeeTableAdapter = New ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblCompany1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblCompany1TableAdapter()
        Me.TblDepartment1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter()
        Me.TblShiftMasterTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblShiftMasterTableAdapter()
        Me.TblCatagory1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblCatagory1TableAdapter()
        Me.TblCatagoryTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblCatagoryTableAdapter()
        Me.TblGradeTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblGradeTableAdapter()
        Me.TblCompanyTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblCompanyTableAdapter()
        Me.TblShiftMaster1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblShiftMaster1TableAdapter()
        Me.TblGrade1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblGrade1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlEmp.SuspendLayout()
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlDept.SuspendLayout()
        CType(Me.PopupContainerControlShift, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlShift.SuspendLayout()
        CType(Me.GridControlShift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblShiftMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewShift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlComp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlComp.SuspendLayout()
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlCat, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlCat.SuspendLayout()
        CType(Me.GridControlCat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCatagoryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewCat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlGrade.SuspendLayout()
        CType(Me.GridControlGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblGradeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlBranch.SuspendLayout()
        CType(Me.GridControlBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanelSelection.SuspendLayout()
        CType(Me.PopupContainerEditEmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditGrade.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditLocation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditComp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditShift.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditDept.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditCat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.GridLookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblLeaveMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckDeptSkip.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckLeaveRegister.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckBalanceLeaves.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckConsumedLeaves.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckAccruedLeaves.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckLeaveCard.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckSanctionedLeaves.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.ComboNepaliYearTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonthTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckExcel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliDateTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliYearFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonthFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliDateFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlEmp)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlDept)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlComp)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlCat)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlGrade)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlBranch)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanelSelection)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PanelControl1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1250, 568)
        Me.SplitContainerControl1.SplitterPosition = 1165
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'PopupContainerControlEmp
        '
        Me.PopupContainerControlEmp.Controls.Add(Me.GridControlEmp)
        Me.PopupContainerControlEmp.Location = New System.Drawing.Point(938, 239)
        Me.PopupContainerControlEmp.Name = "PopupContainerControlEmp"
        Me.PopupContainerControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlEmp.TabIndex = 32
        '
        'GridControlEmp
        '
        Me.GridControlEmp.DataSource = Me.TblEmployeeBindingSource
        Me.GridControlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlEmp.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlEmp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlEmp.MainView = Me.GridViewEmp
        Me.GridControlEmp.Name = "GridControlEmp"
        Me.GridControlEmp.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit1})
        Me.GridControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlEmp.TabIndex = 6
        Me.GridControlEmp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewEmp})
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridViewEmp
        '
        Me.GridViewEmp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPAYCODE, Me.colEMPNAME})
        Me.GridViewEmp.GridControl = Me.GridControlEmp
        Me.GridViewEmp.Name = "GridViewEmp"
        Me.GridViewEmp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.Editable = False
        Me.GridViewEmp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewEmp.OptionsSelection.MultiSelect = True
        Me.GridViewEmp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewEmp.OptionsView.ColumnAutoWidth = False
        Me.GridViewEmp.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colPAYCODE, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colPAYCODE
        '
        Me.colPAYCODE.Caption = "Paycode"
        Me.colPAYCODE.FieldName = "PAYCODE"
        Me.colPAYCODE.Name = "colPAYCODE"
        Me.colPAYCODE.Visible = True
        Me.colPAYCODE.VisibleIndex = 1
        '
        'colEMPNAME
        '
        Me.colEMPNAME.Caption = "Name"
        Me.colEMPNAME.FieldName = "EMPNAME"
        Me.colEMPNAME.Name = "colEMPNAME"
        Me.colEMPNAME.Visible = True
        Me.colEMPNAME.VisibleIndex = 2
        Me.colEMPNAME.Width = 125
        '
        'RepositoryItemTimeEdit1
        '
        Me.RepositoryItemTimeEdit1.AutoHeight = False
        Me.RepositoryItemTimeEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit1.Name = "RepositoryItemTimeEdit1"
        '
        'PopupContainerControlDept
        '
        Me.PopupContainerControlDept.Controls.Add(Me.PopupContainerControlShift)
        Me.PopupContainerControlDept.Controls.Add(Me.GridControlDept)
        Me.PopupContainerControlDept.Location = New System.Drawing.Point(632, 239)
        Me.PopupContainerControlDept.Name = "PopupContainerControlDept"
        Me.PopupContainerControlDept.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlDept.TabIndex = 31
        '
        'PopupContainerControlShift
        '
        Me.PopupContainerControlShift.Controls.Add(Me.GridControlShift)
        Me.PopupContainerControlShift.Location = New System.Drawing.Point(56, 104)
        Me.PopupContainerControlShift.Name = "PopupContainerControlShift"
        Me.PopupContainerControlShift.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlShift.TabIndex = 17
        '
        'GridControlShift
        '
        Me.GridControlShift.DataSource = Me.TblShiftMasterBindingSource
        Me.GridControlShift.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlShift.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlShift.Location = New System.Drawing.Point(0, 0)
        Me.GridControlShift.MainView = Me.GridViewShift
        Me.GridControlShift.Name = "GridControlShift"
        Me.GridControlShift.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit5, Me.RepositoryItemDateEdit1})
        Me.GridControlShift.Size = New System.Drawing.Size(300, 300)
        Me.GridControlShift.TabIndex = 6
        Me.GridControlShift.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewShift})
        '
        'TblShiftMasterBindingSource
        '
        Me.TblShiftMasterBindingSource.DataMember = "tblShiftMaster"
        Me.TblShiftMasterBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewShift
        '
        Me.GridViewShift.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSHIFT, Me.colSTARTTIME, Me.colENDTIME})
        Me.GridViewShift.GridControl = Me.GridControlShift
        Me.GridViewShift.Name = "GridViewShift"
        Me.GridViewShift.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewShift.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewShift.OptionsBehavior.Editable = False
        Me.GridViewShift.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewShift.OptionsSelection.MultiSelect = True
        Me.GridViewShift.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewShift.OptionsView.ColumnAutoWidth = False
        Me.GridViewShift.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSHIFT, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colSHIFT
        '
        Me.colSHIFT.Caption = "Shift"
        Me.colSHIFT.FieldName = "SHIFT"
        Me.colSHIFT.Name = "colSHIFT"
        Me.colSHIFT.Visible = True
        Me.colSHIFT.VisibleIndex = 1
        '
        'colSTARTTIME
        '
        Me.colSTARTTIME.Caption = "Start Time"
        Me.colSTARTTIME.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colSTARTTIME.FieldName = "STARTTIME"
        Me.colSTARTTIME.Name = "colSTARTTIME"
        Me.colSTARTTIME.Visible = True
        Me.colSTARTTIME.VisibleIndex = 2
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'colENDTIME
        '
        Me.colENDTIME.Caption = "End Time"
        Me.colENDTIME.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colENDTIME.FieldName = "ENDTIME"
        Me.colENDTIME.Name = "colENDTIME"
        Me.colENDTIME.Visible = True
        Me.colENDTIME.VisibleIndex = 3
        '
        'RepositoryItemTimeEdit5
        '
        Me.RepositoryItemTimeEdit5.AutoHeight = False
        Me.RepositoryItemTimeEdit5.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit5.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit5.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit5.Name = "RepositoryItemTimeEdit5"
        '
        'GridControlDept
        '
        Me.GridControlDept.DataSource = Me.TblDepartmentBindingSource
        Me.GridControlDept.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlDept.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlDept.Location = New System.Drawing.Point(0, 0)
        Me.GridControlDept.MainView = Me.GridViewDept
        Me.GridControlDept.Name = "GridControlDept"
        Me.GridControlDept.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit3})
        Me.GridControlDept.Size = New System.Drawing.Size(300, 300)
        Me.GridControlDept.TabIndex = 6
        Me.GridControlDept.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewDept})
        '
        'TblDepartmentBindingSource
        '
        Me.TblDepartmentBindingSource.DataMember = "tblDepartment"
        Me.TblDepartmentBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewDept
        '
        Me.GridViewDept.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colDEPARTMENTCODE, Me.colDEPARTMENTNAME})
        Me.GridViewDept.GridControl = Me.GridControlDept
        Me.GridViewDept.Name = "GridViewDept"
        Me.GridViewDept.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.Editable = False
        Me.GridViewDept.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewDept.OptionsSelection.MultiSelect = True
        Me.GridViewDept.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewDept.OptionsView.ColumnAutoWidth = False
        Me.GridViewDept.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colDEPARTMENTCODE, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colDEPARTMENTCODE
        '
        Me.colDEPARTMENTCODE.Caption = "Depatment Code"
        Me.colDEPARTMENTCODE.FieldName = "DEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Name = "colDEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Visible = True
        Me.colDEPARTMENTCODE.VisibleIndex = 1
        Me.colDEPARTMENTCODE.Width = 100
        '
        'colDEPARTMENTNAME
        '
        Me.colDEPARTMENTNAME.Caption = "Name"
        Me.colDEPARTMENTNAME.FieldName = "DEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Name = "colDEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Visible = True
        Me.colDEPARTMENTNAME.VisibleIndex = 2
        Me.colDEPARTMENTNAME.Width = 120
        '
        'RepositoryItemTimeEdit3
        '
        Me.RepositoryItemTimeEdit3.AutoHeight = False
        Me.RepositoryItemTimeEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit3.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit3.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit3.Name = "RepositoryItemTimeEdit3"
        '
        'PopupContainerControlComp
        '
        Me.PopupContainerControlComp.Controls.Add(Me.GridControlComp)
        Me.PopupContainerControlComp.Location = New System.Drawing.Point(929, 510)
        Me.PopupContainerControlComp.Name = "PopupContainerControlComp"
        Me.PopupContainerControlComp.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlComp.TabIndex = 30
        '
        'GridControlComp
        '
        Me.GridControlComp.DataSource = Me.TblCompanyBindingSource
        Me.GridControlComp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlComp.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlComp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlComp.MainView = Me.GridViewComp
        Me.GridControlComp.Name = "GridControlComp"
        Me.GridControlComp.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit2})
        Me.GridControlComp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlComp.TabIndex = 6
        Me.GridControlComp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewComp})
        '
        'TblCompanyBindingSource
        '
        Me.TblCompanyBindingSource.DataMember = "tblCompany"
        Me.TblCompanyBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewComp
        '
        Me.GridViewComp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCOMPANYCODE, Me.colCOMPANYNAME})
        Me.GridViewComp.GridControl = Me.GridControlComp
        Me.GridViewComp.Name = "GridViewComp"
        Me.GridViewComp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.Editable = False
        Me.GridViewComp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewComp.OptionsSelection.MultiSelect = True
        Me.GridViewComp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewComp.OptionsView.ColumnAutoWidth = False
        Me.GridViewComp.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colCOMPANYCODE, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colCOMPANYCODE
        '
        Me.colCOMPANYCODE.Caption = "Company Code"
        Me.colCOMPANYCODE.FieldName = "COMPANYCODE"
        Me.colCOMPANYCODE.Name = "colCOMPANYCODE"
        Me.colCOMPANYCODE.Visible = True
        Me.colCOMPANYCODE.VisibleIndex = 1
        Me.colCOMPANYCODE.Width = 100
        '
        'colCOMPANYNAME
        '
        Me.colCOMPANYNAME.Caption = "Name"
        Me.colCOMPANYNAME.FieldName = "COMPANYNAME"
        Me.colCOMPANYNAME.Name = "colCOMPANYNAME"
        Me.colCOMPANYNAME.Visible = True
        Me.colCOMPANYNAME.VisibleIndex = 2
        Me.colCOMPANYNAME.Width = 120
        '
        'RepositoryItemTimeEdit2
        '
        Me.RepositoryItemTimeEdit2.AutoHeight = False
        Me.RepositoryItemTimeEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit2.Name = "RepositoryItemTimeEdit2"
        '
        'PopupContainerControlCat
        '
        Me.PopupContainerControlCat.Controls.Add(Me.GridControlCat)
        Me.PopupContainerControlCat.Location = New System.Drawing.Point(623, 510)
        Me.PopupContainerControlCat.Name = "PopupContainerControlCat"
        Me.PopupContainerControlCat.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlCat.TabIndex = 29
        '
        'GridControlCat
        '
        Me.GridControlCat.DataSource = Me.TblCatagoryBindingSource
        Me.GridControlCat.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlCat.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlCat.Location = New System.Drawing.Point(0, 0)
        Me.GridControlCat.MainView = Me.GridViewCat
        Me.GridControlCat.Name = "GridControlCat"
        Me.GridControlCat.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit4})
        Me.GridControlCat.Size = New System.Drawing.Size(300, 300)
        Me.GridControlCat.TabIndex = 6
        Me.GridControlCat.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewCat})
        '
        'TblCatagoryBindingSource
        '
        Me.TblCatagoryBindingSource.DataMember = "tblCatagory"
        Me.TblCatagoryBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewCat
        '
        Me.GridViewCat.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCAT, Me.colCATAGORYNAME})
        Me.GridViewCat.GridControl = Me.GridControlCat
        Me.GridViewCat.Name = "GridViewCat"
        Me.GridViewCat.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewCat.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewCat.OptionsBehavior.Editable = False
        Me.GridViewCat.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewCat.OptionsSelection.MultiSelect = True
        Me.GridViewCat.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewCat.OptionsView.ColumnAutoWidth = False
        Me.GridViewCat.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colCAT, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colCAT
        '
        Me.colCAT.Caption = "Catagory Code"
        Me.colCAT.FieldName = "CAT"
        Me.colCAT.Name = "colCAT"
        Me.colCAT.Visible = True
        Me.colCAT.VisibleIndex = 1
        Me.colCAT.Width = 100
        '
        'colCATAGORYNAME
        '
        Me.colCATAGORYNAME.Caption = "Name"
        Me.colCATAGORYNAME.FieldName = "CATAGORYNAME"
        Me.colCATAGORYNAME.Name = "colCATAGORYNAME"
        Me.colCATAGORYNAME.Visible = True
        Me.colCATAGORYNAME.VisibleIndex = 2
        Me.colCATAGORYNAME.Width = 120
        '
        'RepositoryItemTimeEdit4
        '
        Me.RepositoryItemTimeEdit4.AutoHeight = False
        Me.RepositoryItemTimeEdit4.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit4.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit4.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit4.Name = "RepositoryItemTimeEdit4"
        '
        'PopupContainerControlGrade
        '
        Me.PopupContainerControlGrade.Controls.Add(Me.GridControlGrade)
        Me.PopupContainerControlGrade.Location = New System.Drawing.Point(317, 510)
        Me.PopupContainerControlGrade.Name = "PopupContainerControlGrade"
        Me.PopupContainerControlGrade.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlGrade.TabIndex = 28
        '
        'GridControlGrade
        '
        Me.GridControlGrade.DataSource = Me.TblGradeBindingSource
        Me.GridControlGrade.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlGrade.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlGrade.Location = New System.Drawing.Point(0, 0)
        Me.GridControlGrade.MainView = Me.GridViewGrade
        Me.GridControlGrade.Name = "GridControlGrade"
        Me.GridControlGrade.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit6})
        Me.GridControlGrade.Size = New System.Drawing.Size(300, 300)
        Me.GridControlGrade.TabIndex = 6
        Me.GridControlGrade.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewGrade})
        '
        'TblGradeBindingSource
        '
        Me.TblGradeBindingSource.DataMember = "tblGrade"
        Me.TblGradeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewGrade
        '
        Me.GridViewGrade.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridViewGrade.GridControl = Me.GridControlGrade
        Me.GridViewGrade.Name = "GridViewGrade"
        Me.GridViewGrade.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewGrade.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewGrade.OptionsBehavior.Editable = False
        Me.GridViewGrade.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewGrade.OptionsSelection.MultiSelect = True
        Me.GridViewGrade.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewGrade.OptionsView.ColumnAutoWidth = False
        Me.GridViewGrade.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn1, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Grade Code"
        Me.GridColumn1.FieldName = "GradeCode"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 1
        Me.GridColumn1.Width = 100
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Name"
        Me.GridColumn2.FieldName = "GradeName"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 2
        Me.GridColumn2.Width = 120
        '
        'RepositoryItemTimeEdit6
        '
        Me.RepositoryItemTimeEdit6.AutoHeight = False
        Me.RepositoryItemTimeEdit6.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit6.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit6.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit6.Name = "RepositoryItemTimeEdit6"
        '
        'PopupContainerControlBranch
        '
        Me.PopupContainerControlBranch.Controls.Add(Me.GridControlBranch)
        Me.PopupContainerControlBranch.Location = New System.Drawing.Point(11, 510)
        Me.PopupContainerControlBranch.Name = "PopupContainerControlBranch"
        Me.PopupContainerControlBranch.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlBranch.TabIndex = 27
        '
        'GridControlBranch
        '
        Me.GridControlBranch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlBranch.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlBranch.Location = New System.Drawing.Point(0, 0)
        Me.GridControlBranch.MainView = Me.GridViewBranch
        Me.GridControlBranch.Name = "GridControlBranch"
        Me.GridControlBranch.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit7, Me.RepositoryItemDateEdit2})
        Me.GridControlBranch.Size = New System.Drawing.Size(300, 300)
        Me.GridControlBranch.TabIndex = 6
        Me.GridControlBranch.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewBranch})
        '
        'GridViewBranch
        '
        Me.GridViewBranch.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBRANCHCODE, Me.colBRANCHNAME})
        Me.GridViewBranch.GridControl = Me.GridControlBranch
        Me.GridViewBranch.Name = "GridViewBranch"
        Me.GridViewBranch.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewBranch.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewBranch.OptionsBehavior.Editable = False
        Me.GridViewBranch.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewBranch.OptionsSelection.MultiSelect = True
        Me.GridViewBranch.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewBranch.OptionsView.ColumnAutoWidth = False
        Me.GridViewBranch.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colBRANCHCODE, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colBRANCHCODE
        '
        Me.colBRANCHCODE.Caption = "Location Code"
        Me.colBRANCHCODE.FieldName = "BRANCHCODE"
        Me.colBRANCHCODE.Name = "colBRANCHCODE"
        Me.colBRANCHCODE.Visible = True
        Me.colBRANCHCODE.VisibleIndex = 1
        Me.colBRANCHCODE.Width = 100
        '
        'colBRANCHNAME
        '
        Me.colBRANCHNAME.Caption = "Name"
        Me.colBRANCHNAME.FieldName = "BRANCHNAME"
        Me.colBRANCHNAME.Name = "colBRANCHNAME"
        Me.colBRANCHNAME.Visible = True
        Me.colBRANCHNAME.VisibleIndex = 2
        Me.colBRANCHNAME.Width = 120
        '
        'RepositoryItemTimeEdit7
        '
        Me.RepositoryItemTimeEdit7.AutoHeight = False
        Me.RepositoryItemTimeEdit7.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit7.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit7.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit7.Name = "RepositoryItemTimeEdit7"
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'SidePanelSelection
        '
        Me.SidePanelSelection.Controls.Add(Me.LabelControl4)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl9)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditEmp)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditGrade)
        Me.SidePanelSelection.Controls.Add(Me.CheckEdit1)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl8)
        Me.SidePanelSelection.Controls.Add(Me.CheckEdit2)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditLocation)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditComp)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl7)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl5)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditShift)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditDept)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl6)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl11)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditCat)
        Me.SidePanelSelection.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanelSelection.Location = New System.Drawing.Point(0, 60)
        Me.SidePanelSelection.Name = "SidePanelSelection"
        Me.SidePanelSelection.Size = New System.Drawing.Size(1165, 157)
        Me.SidePanelSelection.TabIndex = 26
        Me.SidePanelSelection.Text = "SidePanel4"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(16, 18)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl4.TabIndex = 5
        Me.LabelControl4.Text = "Select Employee"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(366, 65)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl9.TabIndex = 20
        Me.LabelControl9.Text = "Select Grade"
        '
        'PopupContainerEditEmp
        '
        Me.PopupContainerEditEmp.Location = New System.Drawing.Point(113, 16)
        Me.PopupContainerEditEmp.Name = "PopupContainerEditEmp"
        Me.PopupContainerEditEmp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditEmp.Properties.PopupControl = Me.PopupContainerControlEmp
        Me.PopupContainerEditEmp.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditEmp.TabIndex = 6
        Me.PopupContainerEditEmp.ToolTip = "Leave blank if want for all Employees"
        '
        'PopupContainerEditGrade
        '
        Me.PopupContainerEditGrade.Location = New System.Drawing.Point(501, 63)
        Me.PopupContainerEditGrade.Name = "PopupContainerEditGrade"
        Me.PopupContainerEditGrade.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditGrade.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditGrade.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditGrade.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditGrade.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditGrade.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditGrade.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditGrade.Properties.PopupControl = Me.PopupContainerControlGrade
        Me.PopupContainerEditGrade.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditGrade.TabIndex = 19
        Me.PopupContainerEditGrade.ToolTip = "Leave blank if want for all Employees"
        '
        'CheckEdit1
        '
        Me.CheckEdit1.EditValue = True
        Me.CheckEdit1.Location = New System.Drawing.Point(19, 115)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit1.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit1.Properties.Caption = "Active Employee"
        Me.CheckEdit1.Size = New System.Drawing.Size(155, 19)
        Me.CheckEdit1.TabIndex = 7
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(16, 91)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(84, 14)
        Me.LabelControl8.TabIndex = 18
        Me.LabelControl8.Text = "Select Location"
        '
        'CheckEdit2
        '
        Me.CheckEdit2.EditValue = True
        Me.CheckEdit2.Location = New System.Drawing.Point(247, 115)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit2.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit2.Properties.Caption = "Inactive Employee"
        Me.CheckEdit2.Size = New System.Drawing.Size(155, 19)
        Me.CheckEdit2.TabIndex = 8
        '
        'PopupContainerEditLocation
        '
        Me.PopupContainerEditLocation.Location = New System.Drawing.Point(113, 89)
        Me.PopupContainerEditLocation.Name = "PopupContainerEditLocation"
        Me.PopupContainerEditLocation.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditLocation.Properties.PopupControl = Me.PopupContainerControlBranch
        Me.PopupContainerEditLocation.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditLocation.TabIndex = 17
        Me.PopupContainerEditLocation.ToolTip = "Leave blank if want for all Employees"
        '
        'PopupContainerEditComp
        '
        Me.PopupContainerEditComp.Location = New System.Drawing.Point(113, 42)
        Me.PopupContainerEditComp.Name = "PopupContainerEditComp"
        Me.PopupContainerEditComp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditComp.Properties.PopupControl = Me.PopupContainerControlComp
        Me.PopupContainerEditComp.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditComp.TabIndex = 9
        Me.PopupContainerEditComp.ToolTip = "Leave blank if want for all Employees"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(366, 42)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(63, 14)
        Me.LabelControl7.TabIndex = 16
        Me.LabelControl7.Text = "Select Shift"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(16, 43)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(88, 14)
        Me.LabelControl5.TabIndex = 10
        Me.LabelControl5.Text = "Select Company"
        '
        'PopupContainerEditShift
        '
        Me.PopupContainerEditShift.Location = New System.Drawing.Point(501, 40)
        Me.PopupContainerEditShift.Name = "PopupContainerEditShift"
        Me.PopupContainerEditShift.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditShift.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditShift.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditShift.Properties.AppearanceDisabled.Options.UseFont = True
        Me.PopupContainerEditShift.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditShift.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditShift.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditShift.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditShift.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditShift.Properties.PopupControl = Me.PopupContainerControlShift
        Me.PopupContainerEditShift.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditShift.TabIndex = 15
        Me.PopupContainerEditShift.ToolTip = "Leave blank if want for all Employees"
        '
        'PopupContainerEditDept
        '
        Me.PopupContainerEditDept.Location = New System.Drawing.Point(501, 15)
        Me.PopupContainerEditDept.Name = "PopupContainerEditDept"
        Me.PopupContainerEditDept.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditDept.Properties.PopupControl = Me.PopupContainerControlDept
        Me.PopupContainerEditDept.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditDept.TabIndex = 11
        Me.PopupContainerEditDept.ToolTip = "Leave blank if want for all Employees"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(16, 67)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl6.TabIndex = 14
        Me.LabelControl6.Text = "Select Catagory"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(366, 17)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(104, 14)
        Me.LabelControl11.TabIndex = 12
        Me.LabelControl11.Text = "Select Department"
        '
        'PopupContainerEditCat
        '
        Me.PopupContainerEditCat.Location = New System.Drawing.Point(113, 66)
        Me.PopupContainerEditCat.Name = "PopupContainerEditCat"
        Me.PopupContainerEditCat.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditCat.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditCat.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditCat.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditCat.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditCat.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditCat.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditCat.Properties.PopupControl = Me.PopupContainerControlCat
        Me.PopupContainerEditCat.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditCat.TabIndex = 13
        Me.PopupContainerEditCat.ToolTip = "Leave blank if want for all Employees"
        '
        'PanelControl1
        '
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.GridLookUpEdit1)
        Me.PanelControl1.Controls.Add(Me.TextEdit1)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.CheckDeptSkip)
        Me.PanelControl1.Controls.Add(Me.CheckLeaveRegister)
        Me.PanelControl1.Controls.Add(Me.CheckBalanceLeaves)
        Me.PanelControl1.Controls.Add(Me.CheckConsumedLeaves)
        Me.PanelControl1.Controls.Add(Me.CheckAccruedLeaves)
        Me.PanelControl1.Controls.Add(Me.CheckLeaveCard)
        Me.PanelControl1.Controls.Add(Me.CheckSanctionedLeaves)
        Me.PanelControl1.Location = New System.Drawing.Point(11, 239)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(615, 256)
        Me.PanelControl1.TabIndex = 25
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(327, 19)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(64, 14)
        Me.LabelControl1.TabIndex = 39
        Me.LabelControl1.Text = "Leave Type"
        '
        'GridLookUpEdit1
        '
        Me.GridLookUpEdit1.Location = New System.Drawing.Point(420, 16)
        Me.GridLookUpEdit1.Name = "GridLookUpEdit1"
        Me.GridLookUpEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit1.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEdit1.Properties.DataSource = Me.TblLeaveMasterBindingSource
        Me.GridLookUpEdit1.Properties.DisplayMember = "LEAVECODE"
        Me.GridLookUpEdit1.Properties.NullText = ""
        Me.GridLookUpEdit1.Properties.ValueMember = "LEAVECODE"
        Me.GridLookUpEdit1.Properties.View = Me.GridLookUpEdit1View
        Me.GridLookUpEdit1.Size = New System.Drawing.Size(166, 20)
        Me.GridLookUpEdit1.TabIndex = 38
        '
        'TblLeaveMasterBindingSource
        '
        Me.TblLeaveMasterBindingSource.DataMember = "tblLeaveMaster"
        Me.TblLeaveMasterBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridLookUpEdit1View
        '
        Me.GridLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colLEAVEFIELD, Me.colLEAVECODE, Me.colLEAVEDESCRIPTION})
        Me.GridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit1View.Name = "GridLookUpEdit1View"
        Me.GridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colLEAVEFIELD
        '
        Me.colLEAVEFIELD.FieldName = "LEAVEFIELD"
        Me.colLEAVEFIELD.Name = "colLEAVEFIELD"
        '
        'colLEAVECODE
        '
        Me.colLEAVECODE.Caption = "Leave Code"
        Me.colLEAVECODE.FieldName = "LEAVECODE"
        Me.colLEAVECODE.Name = "colLEAVECODE"
        Me.colLEAVECODE.Visible = True
        Me.colLEAVECODE.VisibleIndex = 0
        '
        'colLEAVEDESCRIPTION
        '
        Me.colLEAVEDESCRIPTION.Caption = "Leave Description"
        Me.colLEAVEDESCRIPTION.FieldName = "LEAVEDESCRIPTION"
        Me.colLEAVEDESCRIPTION.Name = "colLEAVEDESCRIPTION"
        Me.colLEAVEDESCRIPTION.Visible = True
        Me.colLEAVEDESCRIPTION.VisibleIndex = 1
        '
        'TextEdit1
        '
        Me.TextEdit1.EditValue = "58"
        Me.TextEdit1.Location = New System.Drawing.Point(143, 211)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEdit1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit1.Properties.MaxLength = 2
        Me.TextEdit1.Size = New System.Drawing.Size(55, 20)
        Me.TextEdit1.TabIndex = 37
        Me.TextEdit1.Visible = False
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(48, 214)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(80, 14)
        Me.LabelControl10.TabIndex = 36
        Me.LabelControl10.Text = "Lines Per Page"
        Me.LabelControl10.Visible = False
        '
        'CheckDeptSkip
        '
        Me.CheckDeptSkip.Location = New System.Drawing.Point(297, 212)
        Me.CheckDeptSkip.Name = "CheckDeptSkip"
        Me.CheckDeptSkip.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckDeptSkip.Properties.Appearance.Options.UseFont = True
        Me.CheckDeptSkip.Properties.Caption = "Department Wise Skip"
        Me.CheckDeptSkip.Size = New System.Drawing.Size(186, 19)
        Me.CheckDeptSkip.TabIndex = 35
        Me.CheckDeptSkip.Visible = False
        '
        'CheckLeaveRegister
        '
        Me.CheckLeaveRegister.Location = New System.Drawing.Point(15, 142)
        Me.CheckLeaveRegister.Name = "CheckLeaveRegister"
        Me.CheckLeaveRegister.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckLeaveRegister.Properties.Appearance.Options.UseFont = True
        Me.CheckLeaveRegister.Properties.Caption = "Leave Register"
        Me.CheckLeaveRegister.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckLeaveRegister.Properties.RadioGroupIndex = 0
        Me.CheckLeaveRegister.Size = New System.Drawing.Size(168, 19)
        Me.CheckLeaveRegister.TabIndex = 16
        Me.CheckLeaveRegister.TabStop = False
        Me.CheckLeaveRegister.Visible = False
        '
        'CheckBalanceLeaves
        '
        Me.CheckBalanceLeaves.Location = New System.Drawing.Point(15, 117)
        Me.CheckBalanceLeaves.Name = "CheckBalanceLeaves"
        Me.CheckBalanceLeaves.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckBalanceLeaves.Properties.Appearance.Options.UseFont = True
        Me.CheckBalanceLeaves.Properties.Caption = "Balance Leaves"
        Me.CheckBalanceLeaves.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckBalanceLeaves.Properties.RadioGroupIndex = 0
        Me.CheckBalanceLeaves.Size = New System.Drawing.Size(168, 19)
        Me.CheckBalanceLeaves.TabIndex = 15
        Me.CheckBalanceLeaves.TabStop = False
        '
        'CheckConsumedLeaves
        '
        Me.CheckConsumedLeaves.Location = New System.Drawing.Point(15, 92)
        Me.CheckConsumedLeaves.Name = "CheckConsumedLeaves"
        Me.CheckConsumedLeaves.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckConsumedLeaves.Properties.Appearance.Options.UseFont = True
        Me.CheckConsumedLeaves.Properties.Caption = "Consumed Leaves"
        Me.CheckConsumedLeaves.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckConsumedLeaves.Properties.RadioGroupIndex = 0
        Me.CheckConsumedLeaves.Size = New System.Drawing.Size(168, 19)
        Me.CheckConsumedLeaves.TabIndex = 14
        Me.CheckConsumedLeaves.TabStop = False
        '
        'CheckAccruedLeaves
        '
        Me.CheckAccruedLeaves.Location = New System.Drawing.Point(15, 67)
        Me.CheckAccruedLeaves.Name = "CheckAccruedLeaves"
        Me.CheckAccruedLeaves.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckAccruedLeaves.Properties.Appearance.Options.UseFont = True
        Me.CheckAccruedLeaves.Properties.Caption = "Accrued Leaves"
        Me.CheckAccruedLeaves.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckAccruedLeaves.Properties.RadioGroupIndex = 0
        Me.CheckAccruedLeaves.Size = New System.Drawing.Size(168, 19)
        Me.CheckAccruedLeaves.TabIndex = 13
        Me.CheckAccruedLeaves.TabStop = False
        '
        'CheckLeaveCard
        '
        Me.CheckLeaveCard.Location = New System.Drawing.Point(15, 42)
        Me.CheckLeaveCard.Name = "CheckLeaveCard"
        Me.CheckLeaveCard.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckLeaveCard.Properties.Appearance.Options.UseFont = True
        Me.CheckLeaveCard.Properties.Caption = "Leave Card"
        Me.CheckLeaveCard.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckLeaveCard.Properties.RadioGroupIndex = 0
        Me.CheckLeaveCard.Size = New System.Drawing.Size(168, 19)
        Me.CheckLeaveCard.TabIndex = 12
        Me.CheckLeaveCard.TabStop = False
        '
        'CheckSanctionedLeaves
        '
        Me.CheckSanctionedLeaves.EditValue = True
        Me.CheckSanctionedLeaves.Location = New System.Drawing.Point(15, 17)
        Me.CheckSanctionedLeaves.Name = "CheckSanctionedLeaves"
        Me.CheckSanctionedLeaves.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckSanctionedLeaves.Properties.Appearance.Options.UseFont = True
        Me.CheckSanctionedLeaves.Properties.Caption = "Sanctioned Leaves"
        Me.CheckSanctionedLeaves.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckSanctionedLeaves.Properties.RadioGroupIndex = 0
        Me.CheckSanctionedLeaves.Size = New System.Drawing.Size(168, 19)
        Me.CheckSanctionedLeaves.TabIndex = 11
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.ComboNepaliYearTo)
        Me.SidePanel1.Controls.Add(Me.TextEdit2)
        Me.SidePanel1.Controls.Add(Me.ComboNEpaliMonthTo)
        Me.SidePanel1.Controls.Add(Me.CheckExcel)
        Me.SidePanel1.Controls.Add(Me.ComboNepaliDateTo)
        Me.SidePanel1.Controls.Add(Me.CheckText)
        Me.SidePanel1.Controls.Add(Me.ComboNepaliYearFrm)
        Me.SidePanel1.Controls.Add(Me.SimpleButton1)
        Me.SidePanel1.Controls.Add(Me.ComboNEpaliMonthFrm)
        Me.SidePanel1.Controls.Add(Me.DateEdit2)
        Me.SidePanel1.Controls.Add(Me.ComboNepaliDateFrm)
        Me.SidePanel1.Controls.Add(Me.LabelControl3)
        Me.SidePanel1.Controls.Add(Me.DateEdit1)
        Me.SidePanel1.Controls.Add(Me.LabelControl2)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanel1.Location = New System.Drawing.Point(0, 0)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1165, 60)
        Me.SidePanel1.TabIndex = 0
        Me.SidePanel1.Text = "SidePanel1"
        '
        'ComboNepaliYearTo
        '
        Me.ComboNepaliYearTo.Location = New System.Drawing.Point(477, 19)
        Me.ComboNepaliYearTo.Name = "ComboNepaliYearTo"
        Me.ComboNepaliYearTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearTo.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYearTo.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearTo.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYearTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYearTo.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYearTo.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliYearTo.TabIndex = 39
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(96, 19)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit2.Properties.Appearance.Options.UseFont = True
        Me.TextEdit2.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEdit2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEdit2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit2.Properties.MaxLength = 4
        Me.TextEdit2.Size = New System.Drawing.Size(83, 20)
        Me.TextEdit2.TabIndex = 31
        Me.TextEdit2.Visible = False
        '
        'ComboNEpaliMonthTo
        '
        Me.ComboNEpaliMonthTo.Location = New System.Drawing.Point(391, 19)
        Me.ComboNEpaliMonthTo.Name = "ComboNEpaliMonthTo"
        Me.ComboNEpaliMonthTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthTo.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonthTo.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthTo.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonthTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonthTo.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonthTo.Size = New System.Drawing.Size(80, 20)
        Me.ComboNEpaliMonthTo.TabIndex = 38
        '
        'CheckExcel
        '
        Me.CheckExcel.Location = New System.Drawing.Point(770, 16)
        Me.CheckExcel.Name = "CheckExcel"
        Me.CheckExcel.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckExcel.Properties.Appearance.Options.UseFont = True
        Me.CheckExcel.Properties.Caption = "Excel"
        Me.CheckExcel.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckExcel.Properties.RadioGroupIndex = 0
        Me.CheckExcel.Size = New System.Drawing.Size(63, 19)
        Me.CheckExcel.TabIndex = 30
        Me.CheckExcel.TabStop = False
        '
        'ComboNepaliDateTo
        '
        Me.ComboNepaliDateTo.Location = New System.Drawing.Point(343, 19)
        Me.ComboNepaliDateTo.Name = "ComboNepaliDateTo"
        Me.ComboNepaliDateTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateTo.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliDateTo.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateTo.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliDateTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliDateTo.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNepaliDateTo.Size = New System.Drawing.Size(42, 20)
        Me.ComboNepaliDateTo.TabIndex = 37
        '
        'CheckText
        '
        Me.CheckText.EditValue = True
        Me.CheckText.Location = New System.Drawing.Point(665, 17)
        Me.CheckText.Name = "CheckText"
        Me.CheckText.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckText.Properties.Appearance.Options.UseFont = True
        Me.CheckText.Properties.Caption = "Text"
        Me.CheckText.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckText.Properties.RadioGroupIndex = 0
        Me.CheckText.Size = New System.Drawing.Size(63, 19)
        Me.CheckText.TabIndex = 29
        '
        'ComboNepaliYearFrm
        '
        Me.ComboNepaliYearFrm.Location = New System.Drawing.Point(208, 19)
        Me.ComboNepaliYearFrm.Name = "ComboNepaliYearFrm"
        Me.ComboNepaliYearFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearFrm.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYearFrm.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearFrm.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYearFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYearFrm.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYearFrm.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliYearFrm.TabIndex = 36
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(555, 18)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 28
        Me.SimpleButton1.Text = "Generate"
        '
        'ComboNEpaliMonthFrm
        '
        Me.ComboNEpaliMonthFrm.Location = New System.Drawing.Point(122, 19)
        Me.ComboNEpaliMonthFrm.Name = "ComboNEpaliMonthFrm"
        Me.ComboNEpaliMonthFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthFrm.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonthFrm.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthFrm.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonthFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonthFrm.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonthFrm.Size = New System.Drawing.Size(80, 20)
        Me.ComboNEpaliMonthFrm.TabIndex = 35
        '
        'DateEdit2
        '
        Me.DateEdit2.EditValue = Nothing
        Me.DateEdit2.Location = New System.Drawing.Point(368, 19)
        Me.DateEdit2.Name = "DateEdit2"
        Me.DateEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit2.Properties.Appearance.Options.UseFont = True
        Me.DateEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit2.TabIndex = 27
        '
        'ComboNepaliDateFrm
        '
        Me.ComboNepaliDateFrm.Location = New System.Drawing.Point(74, 19)
        Me.ComboNepaliDateFrm.Name = "ComboNepaliDateFrm"
        Me.ComboNepaliDateFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateFrm.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliDateFrm.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateFrm.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliDateFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliDateFrm.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNepaliDateFrm.Size = New System.Drawing.Size(42, 20)
        Me.ComboNepaliDateFrm.TabIndex = 34
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(284, 22)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(45, 14)
        Me.LabelControl3.TabIndex = 26
        Me.LabelControl3.Text = "To Date"
        '
        'DateEdit1
        '
        Me.DateEdit1.EditValue = Nothing
        Me.DateEdit1.Location = New System.Drawing.Point(96, 19)
        Me.DateEdit1.Name = "DateEdit1"
        Me.DateEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit1.Properties.Appearance.Options.UseFont = True
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit1.TabIndex = 25
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(11, 22)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(57, 14)
        Me.LabelControl2.TabIndex = 24
        Me.LabelControl2.Text = "From Date"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(77, 568)
        Me.MemoEdit1.TabIndex = 3
        '
        'TblLeaveMasterTableAdapter
        '
        Me.TblLeaveMasterTableAdapter.ClearBeforeFill = True
        '
        'TblLeaveMaster1TableAdapter1
        '
        Me.TblLeaveMaster1TableAdapter1.ClearBeforeFill = True
        '
        'Tblbranch1TableAdapter1
        '
        Me.Tblbranch1TableAdapter1.ClearBeforeFill = True
        '
        'TblbranchTableAdapter
        '
        Me.TblbranchTableAdapter.ClearBeforeFill = True
        '
        'TblDepartmentTableAdapter
        '
        Me.TblDepartmentTableAdapter.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblCompany1TableAdapter1
        '
        Me.TblCompany1TableAdapter1.ClearBeforeFill = True
        '
        'TblDepartment1TableAdapter1
        '
        Me.TblDepartment1TableAdapter1.ClearBeforeFill = True
        '
        'TblShiftMasterTableAdapter
        '
        Me.TblShiftMasterTableAdapter.ClearBeforeFill = True
        '
        'TblCatagory1TableAdapter1
        '
        Me.TblCatagory1TableAdapter1.ClearBeforeFill = True
        '
        'TblCatagoryTableAdapter
        '
        Me.TblCatagoryTableAdapter.ClearBeforeFill = True
        '
        'TblGradeTableAdapter
        '
        Me.TblGradeTableAdapter.ClearBeforeFill = True
        '
        'TblCompanyTableAdapter
        '
        Me.TblCompanyTableAdapter.ClearBeforeFill = True
        '
        'TblShiftMaster1TableAdapter1
        '
        Me.TblShiftMaster1TableAdapter1.ClearBeforeFill = True
        '
        'TblGrade1TableAdapter1
        '
        Me.TblGrade1TableAdapter1.ClearBeforeFill = True
        '
        'XtraLeaveReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraLeaveReport"
        Me.Size = New System.Drawing.Size(1250, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlEmp.ResumeLayout(False)
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlDept.ResumeLayout(False)
        CType(Me.PopupContainerControlShift, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlShift.ResumeLayout(False)
        CType(Me.GridControlShift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblShiftMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewShift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlComp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlComp.ResumeLayout(False)
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlCat, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlCat.ResumeLayout(False)
        CType(Me.GridControlCat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCatagoryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewCat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlGrade, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlGrade.ResumeLayout(False)
        CType(Me.GridControlGrade, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblGradeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewGrade, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlBranch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlBranch.ResumeLayout(False)
        CType(Me.GridControlBranch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewBranch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanelSelection.ResumeLayout(False)
        Me.SidePanelSelection.PerformLayout()
        CType(Me.PopupContainerEditEmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditGrade.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditLocation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditComp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditShift.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditDept.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditCat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.GridLookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblLeaveMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckDeptSkip.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckLeaveRegister.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckBalanceLeaves.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckConsumedLeaves.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckAccruedLeaves.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckLeaveCard.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckSanctionedLeaves.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        Me.SidePanel1.PerformLayout()
        CType(Me.ComboNepaliYearTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonthTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckExcel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliDateTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliYearFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonthFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliDateFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents CheckExcel As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckText As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit2 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckDeptSkip As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckLeaveRegister As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckBalanceLeaves As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckConsumedLeaves As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckAccruedLeaves As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckLeaveCard As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckSanctionedLeaves As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GridLookUpEdit1 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TblLeaveMasterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As ULtra.SSSDBDataSet
    Friend WithEvents TblLeaveMasterTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblLeaveMasterTableAdapter
    Friend WithEvents TblLeaveMaster1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblLeaveMaster1TableAdapter
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents colLEAVEFIELD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLEAVECODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLEAVEDESCRIPTION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SidePanelSelection As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditEmp As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents PopupContainerEditGrade As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PopupContainerEditLocation As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents PopupContainerEditComp As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditShift As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents PopupContainerEditDept As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditCat As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents Tblbranch1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblbranch1TableAdapter
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblDepartmentBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblbranchTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblbranchTableAdapter
    Friend WithEvents TblCompanyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblGradeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblShiftMasterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblCatagoryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblDepartmentTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter
    Friend WithEvents TblEmployee1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents TblEmployeeTableAdapter As ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents TblCompany1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblCompany1TableAdapter
    Friend WithEvents TblDepartment1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter
    Friend WithEvents TblShiftMasterTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblShiftMasterTableAdapter
    Friend WithEvents TblCatagory1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblCatagory1TableAdapter
    Friend WithEvents TblCatagoryTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblCatagoryTableAdapter
    Friend WithEvents TblGradeTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblGradeTableAdapter
    Friend WithEvents TblCompanyTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblCompanyTableAdapter
    Friend WithEvents TblShiftMaster1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblShiftMaster1TableAdapter
    Friend WithEvents TblGrade1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblGrade1TableAdapter
    Friend WithEvents PopupContainerControlBranch As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlBranch As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewBranch As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colBRANCHCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRANCHNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit7 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents PopupContainerControlGrade As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlGrade As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewGrade As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit6 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlCat As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlCat As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewCat As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCATAGORYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit4 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlComp As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlComp As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewComp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCOMPANYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlDept As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlDept As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewDept As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colDEPARTMENTCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEPARTMENTNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlEmp As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlEmp As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewEmp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colPAYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlShift As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlShift As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewShift As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colSHIFT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTARTTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents colENDTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit5 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents ComboNepaliYearTo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonthTo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliDateTo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliYearFrm As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonthFrm As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliDateFrm As DevExpress.XtraEditors.ComboBoxEdit

End Class
