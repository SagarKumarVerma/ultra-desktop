﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HikvisionTest
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.listViewEvent = New System.Windows.Forms.ListView()
        Me.columnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader13 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader14 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader15 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader16 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader17 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader18 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader19 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader20 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader21 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader22 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader23 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader24 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader25 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader26 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader27 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader28 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader29 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader30 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader31 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader32 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader33 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader34 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader35 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.columnHeader36 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(34, 12)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'listViewEvent
        '
        Me.listViewEvent.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.columnHeader1, Me.columnHeader2, Me.columnHeader3, Me.columnHeader4, Me.columnHeader5, Me.columnHeader6, Me.columnHeader7, Me.columnHeader8, Me.columnHeader9, Me.columnHeader10, Me.columnHeader11, Me.columnHeader12, Me.columnHeader13, Me.columnHeader14, Me.columnHeader15, Me.columnHeader16, Me.columnHeader17, Me.columnHeader18, Me.columnHeader19, Me.columnHeader20, Me.columnHeader21, Me.columnHeader22, Me.columnHeader23, Me.columnHeader24, Me.columnHeader25, Me.columnHeader26, Me.columnHeader27, Me.columnHeader28, Me.columnHeader29, Me.columnHeader30, Me.columnHeader31, Me.columnHeader32, Me.columnHeader33, Me.columnHeader34, Me.columnHeader35, Me.columnHeader36})
        Me.listViewEvent.GridLines = True
        Me.listViewEvent.LabelEdit = True
        Me.listViewEvent.Location = New System.Drawing.Point(34, 72)
        Me.listViewEvent.Name = "listViewEvent"
        Me.listViewEvent.Size = New System.Drawing.Size(754, 284)
        Me.listViewEvent.TabIndex = 30
        Me.listViewEvent.UseCompatibleStateImageBehavior = False
        Me.listViewEvent.View = System.Windows.Forms.View.Details
        '
        'columnHeader1
        '
        Me.columnHeader1.Text = "No"
        '
        'columnHeader2
        '
        Me.columnHeader2.Text = "Log Time"
        '
        'columnHeader3
        '
        Me.columnHeader3.Text = "Major Type"
        Me.columnHeader3.Width = 80
        '
        'columnHeader4
        '
        Me.columnHeader4.Text = "Minor Type"
        Me.columnHeader4.Width = 120
        '
        'columnHeader5
        '
        Me.columnHeader5.Text = "Card No"
        '
        'columnHeader6
        '
        Me.columnHeader6.Text = "Card Type"
        Me.columnHeader6.Width = 80
        '
        'columnHeader7
        '
        Me.columnHeader7.Text = "White List No"
        Me.columnHeader7.Width = 100
        '
        'columnHeader8
        '
        Me.columnHeader8.Text = "Report Channel"
        Me.columnHeader8.Width = 90
        '
        'columnHeader9
        '
        Me.columnHeader9.Text = "Card Reader Kind"
        Me.columnHeader9.Width = 100
        '
        'columnHeader10
        '
        Me.columnHeader10.Text = "Card Reader No"
        Me.columnHeader10.Width = 100
        '
        'columnHeader11
        '
        Me.columnHeader11.Text = "Door No"
        '
        'columnHeader12
        '
        Me.columnHeader12.Text = "Verify No"
        Me.columnHeader12.Width = 80
        '
        'columnHeader13
        '
        Me.columnHeader13.Text = "Alarm In No"
        Me.columnHeader13.Width = 100
        '
        'columnHeader14
        '
        Me.columnHeader14.Text = "Alarm Out No"
        Me.columnHeader14.Width = 100
        '
        'columnHeader15
        '
        Me.columnHeader15.Text = "Case Sensor No"
        Me.columnHeader15.Width = 100
        '
        'columnHeader16
        '
        Me.columnHeader16.Text = "RS485 No"
        Me.columnHeader16.Width = 80
        '
        'columnHeader17
        '
        Me.columnHeader17.Text = "Multi Card Group No"
        Me.columnHeader17.Width = 110
        '
        'columnHeader18
        '
        Me.columnHeader18.Text = "Access Channel"
        Me.columnHeader18.Width = 80
        '
        'columnHeader19
        '
        Me.columnHeader19.Text = "Device No"
        Me.columnHeader19.Width = 80
        '
        'columnHeader20
        '
        Me.columnHeader20.Text = "Employee No"
        Me.columnHeader20.Width = 80
        '
        'columnHeader21
        '
        Me.columnHeader21.Text = "Distract Control No"
        Me.columnHeader21.Width = 100
        '
        'columnHeader22
        '
        Me.columnHeader22.Text = "Local Controller ID"
        Me.columnHeader22.Width = 100
        '
        'columnHeader23
        '
        Me.columnHeader23.Text = "InterNet Access"
        Me.columnHeader23.Width = 80
        '
        'columnHeader24
        '
        Me.columnHeader24.Text = "Type"
        '
        'columnHeader25
        '
        Me.columnHeader25.Text = "Mac Addr"
        Me.columnHeader25.Width = 80
        '
        'columnHeader26
        '
        Me.columnHeader26.Text = "Swipe Card Type"
        Me.columnHeader26.Width = 100
        '
        'columnHeader27
        '
        Me.columnHeader27.Text = "Serial No"
        Me.columnHeader27.Width = 80
        '
        'columnHeader28
        '
        Me.columnHeader28.Text = "Channel Controller ID"
        Me.columnHeader28.Width = 100
        '
        'columnHeader29
        '
        Me.columnHeader29.Text = "Channel Controller Lamp ID"
        Me.columnHeader29.Width = 100
        '
        'columnHeader30
        '
        Me.columnHeader30.Text = "Channel Controller IR Adaptor ID"
        Me.columnHeader30.Width = 100
        '
        'columnHeader31
        '
        Me.columnHeader31.Text = "Channel Controller IR Emitter ID"
        Me.columnHeader31.Width = 100
        '
        'columnHeader32
        '
        Me.columnHeader32.Text = "Inductive Event Type"
        Me.columnHeader32.Width = 100
        '
        'columnHeader33
        '
        Me.columnHeader33.Text = "Record Channel"
        Me.columnHeader33.Width = 80
        '
        'columnHeader34
        '
        Me.columnHeader34.Text = "User Type"
        '
        'columnHeader35
        '
        Me.columnHeader35.Text = "Current Verify Mode"
        Me.columnHeader35.Width = 80
        '
        'columnHeader36
        '
        Me.columnHeader36.Text = "Employee No String"
        Me.columnHeader36.Width = 100
        '
        'HikvisionTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(889, 368)
        Me.Controls.Add(Me.listViewEvent)
        Me.Controls.Add(Me.Button1)
        Me.Name = "HikvisionTest"
        Me.Text = "HikvisionTest"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Private WithEvents listViewEvent As System.Windows.Forms.ListView
    Private WithEvents columnHeader1 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader2 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader3 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader4 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader5 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader6 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader7 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader8 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader9 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader10 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader11 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader12 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader13 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader14 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader15 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader16 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader17 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader18 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader19 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader20 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader21 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader22 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader23 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader24 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader25 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader26 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader27 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader28 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader29 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader30 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader31 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader32 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader33 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader34 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader35 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader36 As System.Windows.Forms.ColumnHeader
End Class
