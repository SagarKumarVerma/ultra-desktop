﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraAppInfo
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraAppInfo))
        Me.lblUpdDate = New DevExpress.XtraEditors.LabelControl()
        Me.lblInstDate = New DevExpress.XtraEditors.LabelControl()
        Me.lblType = New DevExpress.XtraEditors.LabelControl()
        Me.lblKey = New DevExpress.XtraEditors.LabelControl()
        Me.lblVersion = New DevExpress.XtraEditors.LabelControl()
        Me.lblName = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'lblUpdDate
        '
        Me.lblUpdDate.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblUpdDate.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblUpdDate.Appearance.Options.UseFont = True
        Me.lblUpdDate.Appearance.Options.UseForeColor = True
        Me.lblUpdDate.Location = New System.Drawing.Point(124, 151)
        Me.lblUpdDate.Name = "lblUpdDate"
        Me.lblUpdDate.Size = New System.Drawing.Size(20, 14)
        Me.lblUpdDate.TabIndex = 36
        Me.lblUpdDate.Text = "     "
        '
        'lblInstDate
        '
        Me.lblInstDate.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblInstDate.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblInstDate.Appearance.Options.UseFont = True
        Me.lblInstDate.Appearance.Options.UseForeColor = True
        Me.lblInstDate.Location = New System.Drawing.Point(124, 122)
        Me.lblInstDate.Name = "lblInstDate"
        Me.lblInstDate.Size = New System.Drawing.Size(24, 14)
        Me.lblInstDate.TabIndex = 35
        Me.lblInstDate.Text = "      "
        '
        'lblType
        '
        Me.lblType.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblType.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblType.Appearance.Options.UseFont = True
        Me.lblType.Appearance.Options.UseForeColor = True
        Me.lblType.Location = New System.Drawing.Point(124, 94)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(20, 14)
        Me.lblType.TabIndex = 34
        Me.lblType.Text = "     "
        '
        'lblKey
        '
        Me.lblKey.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblKey.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblKey.Appearance.Options.UseFont = True
        Me.lblKey.Appearance.Options.UseForeColor = True
        Me.lblKey.Location = New System.Drawing.Point(124, 65)
        Me.lblKey.Name = "lblKey"
        Me.lblKey.Size = New System.Drawing.Size(20, 14)
        Me.lblKey.TabIndex = 33
        Me.lblKey.Text = "     "
        '
        'lblVersion
        '
        Me.lblVersion.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblVersion.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblVersion.Appearance.Options.UseFont = True
        Me.lblVersion.Appearance.Options.UseForeColor = True
        Me.lblVersion.Location = New System.Drawing.Point(124, 38)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(24, 14)
        Me.lblVersion.TabIndex = 32
        Me.lblVersion.Text = "      "
        '
        'lblName
        '
        Me.lblName.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblName.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblName.Appearance.Options.UseFont = True
        Me.lblName.Appearance.Options.UseForeColor = True
        Me.lblName.Location = New System.Drawing.Point(124, 12)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(201, 14)
        Me.lblName.TabIndex = 31
        Me.lblName.Text = "Integrated Attendance System Ultra"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(12, 151)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(77, 14)
        Me.LabelControl10.TabIndex = 30
        Me.LabelControl10.Text = "Updated Date"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(12, 122)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(75, 14)
        Me.LabelControl9.TabIndex = 29
        Me.LabelControl9.Text = "Installed Date"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(12, 94)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(72, 14)
        Me.LabelControl8.TabIndex = 28
        Me.LabelControl8.Text = "License Type"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(12, 65)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(64, 14)
        Me.LabelControl7.TabIndex = 27
        Me.LabelControl7.Text = "License Key"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(12, 38)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(40, 14)
        Me.LabelControl6.TabIndex = 26
        Me.LabelControl6.Text = "Version"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(31, 14)
        Me.LabelControl5.TabIndex = 25
        Me.LabelControl5.Text = "Name"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(253, 147)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(107, 23)
        Me.SimpleButton1.TabIndex = 37
        Me.SimpleButton1.Text = "Check Update"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(124, 197)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 38
        Me.SimpleButton2.Text = "Close"
        '
        'XtraAppInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(387, 261)
        Me.ControlBox = False
        Me.Controls.Add(Me.SimpleButton2)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.lblUpdDate)
        Me.Controls.Add(Me.lblInstDate)
        Me.Controls.Add(Me.lblType)
        Me.Controls.Add(Me.lblKey)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.LabelControl10)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.LabelControl5)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraAppInfo"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ULtra Info"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblUpdDate As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblInstDate As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblType As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblKey As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblVersion As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
End Class
