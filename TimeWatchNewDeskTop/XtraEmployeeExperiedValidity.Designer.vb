﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class XtraEmployeeExperiedValidity
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Me.SSSDBDataSet = New ULtra.SSSDBDataSet()
        Me.TblMachineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblMachineTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblMachineTableAdapter()
        Me.TblMachine1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblMachine1TableAdapter()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colACTIVE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPAYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGUARDIANNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateOFBIRTH = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateOFJOIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPRESENTCARDNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEPARTMENTCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSEX = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISMARRIED = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBUS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colQUALIFICATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEXPERIENCE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDESIGNATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colADDRESS1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPINCODE1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTELEPHONE1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colE_MAIL1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colADDRESS2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPINCODE2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTELEPHONE2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBLOODGROUP = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPPHOTO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPSIGNATURE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDivisionCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGradeCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLeavingdate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLeavingReason = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVehicleNo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPFNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPF_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colESINO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAUTHORISEDMACHINE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPTYPE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBankAcc = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbankCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colReporting1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colReporting2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRANCHCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDESPANSARYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colValidityStartDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colValidityEndDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEmployeeGroupId = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedBy = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMachineCard = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TblEmployeeTableAdapter = New ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblEmployee1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel2 = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanel3 = New DevExpress.XtraEditors.SidePanel()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        Me.SidePanel2.SuspendLayout()
        Me.SidePanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblMachineBindingSource
        '
        Me.TblMachineBindingSource.DataMember = "tblMachine"
        Me.TblMachineBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblMachineTableAdapter
        '
        Me.TblMachineTableAdapter.ClearBeforeFill = True
        '
        'TblMachine1TableAdapter1
        '
        Me.TblMachine1TableAdapter1.ClearBeforeFill = True
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.GridControl2)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(865, 476)
        Me.GroupControl2.TabIndex = 19
        '
        'GridControl2
        '
        Me.GridControl2.DataSource = Me.TblEmployeeBindingSource
        Me.GridControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl2.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.Remove.Visible = False
        GridLevelNode1.RelationName = "Level1"
        Me.GridControl2.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridControl2.Location = New System.Drawing.Point(2, 23)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(861, 451)
        Me.GridControl2.TabIndex = 1
        Me.GridControl2.UseEmbeddedNavigator = True
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colACTIVE, Me.colPAYCODE, Me.colEMPNAME, Me.colGUARDIANNAME, Me.colDateOFBIRTH, Me.colDateOFJOIN, Me.colPRESENTCARDNO, Me.colCOMPANYCODE, Me.colDEPARTMENTCODE, Me.colCAT, Me.colSEX, Me.colISMARRIED, Me.colBUS, Me.colQUALIFICATION, Me.colEXPERIENCE, Me.colDESIGNATION, Me.colADDRESS1, Me.colPINCODE1, Me.colTELEPHONE1, Me.colE_MAIL1, Me.colADDRESS2, Me.colPINCODE2, Me.colTELEPHONE2, Me.colBLOODGROUP, Me.colEMPPHOTO, Me.colEMPSIGNATURE, Me.colDivisionCode, Me.colGradeCode, Me.colLeavingdate, Me.colLeavingReason, Me.colVehicleNo, Me.colPFNO, Me.colPF_NO, Me.colESINO, Me.colAUTHORISEDMACHINE, Me.colEMPTYPE, Me.colBankAcc, Me.colbankCODE, Me.colReporting1, Me.colReporting2, Me.colBRANCHCODE, Me.colDESPANSARYCODE, Me.colValidityStartDate, Me.colValidityEndDate, Me.colEmployeeGroupId, Me.colLastModifiedBy, Me.colLastModifiedDate, Me.colMachineCard})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView2.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView2.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colPRESENTCARDNO, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colACTIVE
        '
        Me.colACTIVE.FieldName = "ACTIVE"
        Me.colACTIVE.Name = "colACTIVE"
        '
        'colPAYCODE
        '
        Me.colPAYCODE.FieldName = "PAYCODE"
        Me.colPAYCODE.Name = "colPAYCODE"
        Me.colPAYCODE.Visible = True
        Me.colPAYCODE.VisibleIndex = 0
        '
        'colEMPNAME
        '
        Me.colEMPNAME.FieldName = "EMPNAME"
        Me.colEMPNAME.Name = "colEMPNAME"
        Me.colEMPNAME.Visible = True
        Me.colEMPNAME.VisibleIndex = 1
        '
        'colGUARDIANNAME
        '
        Me.colGUARDIANNAME.FieldName = "GUARDIANNAME"
        Me.colGUARDIANNAME.Name = "colGUARDIANNAME"
        '
        'colDateOFBIRTH
        '
        Me.colDateOFBIRTH.FieldName = "DateOFBIRTH"
        Me.colDateOFBIRTH.Name = "colDateOFBIRTH"
        '
        'colDateOFJOIN
        '
        Me.colDateOFJOIN.FieldName = "DateOFJOIN"
        Me.colDateOFJOIN.Name = "colDateOFJOIN"
        '
        'colPRESENTCARDNO
        '
        Me.colPRESENTCARDNO.FieldName = "PRESENTCARDNO"
        Me.colPRESENTCARDNO.Name = "colPRESENTCARDNO"
        Me.colPRESENTCARDNO.Visible = True
        Me.colPRESENTCARDNO.VisibleIndex = 2
        '
        'colCOMPANYCODE
        '
        Me.colCOMPANYCODE.FieldName = "COMPANYCODE"
        Me.colCOMPANYCODE.Name = "colCOMPANYCODE"
        '
        'colDEPARTMENTCODE
        '
        Me.colDEPARTMENTCODE.FieldName = "DEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Name = "colDEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Visible = True
        Me.colDEPARTMENTCODE.VisibleIndex = 3
        '
        'colCAT
        '
        Me.colCAT.FieldName = "CAT"
        Me.colCAT.Name = "colCAT"
        '
        'colSEX
        '
        Me.colSEX.FieldName = "SEX"
        Me.colSEX.Name = "colSEX"
        '
        'colISMARRIED
        '
        Me.colISMARRIED.FieldName = "ISMARRIED"
        Me.colISMARRIED.Name = "colISMARRIED"
        '
        'colBUS
        '
        Me.colBUS.FieldName = "BUS"
        Me.colBUS.Name = "colBUS"
        '
        'colQUALIFICATION
        '
        Me.colQUALIFICATION.FieldName = "QUALIFICATION"
        Me.colQUALIFICATION.Name = "colQUALIFICATION"
        '
        'colEXPERIENCE
        '
        Me.colEXPERIENCE.FieldName = "EXPERIENCE"
        Me.colEXPERIENCE.Name = "colEXPERIENCE"
        '
        'colDESIGNATION
        '
        Me.colDESIGNATION.FieldName = "DESIGNATION"
        Me.colDESIGNATION.Name = "colDESIGNATION"
        '
        'colADDRESS1
        '
        Me.colADDRESS1.FieldName = "ADDRESS1"
        Me.colADDRESS1.Name = "colADDRESS1"
        '
        'colPINCODE1
        '
        Me.colPINCODE1.FieldName = "PINCODE1"
        Me.colPINCODE1.Name = "colPINCODE1"
        '
        'colTELEPHONE1
        '
        Me.colTELEPHONE1.FieldName = "TELEPHONE1"
        Me.colTELEPHONE1.Name = "colTELEPHONE1"
        Me.colTELEPHONE1.Visible = True
        Me.colTELEPHONE1.VisibleIndex = 4
        '
        'colE_MAIL1
        '
        Me.colE_MAIL1.FieldName = "E_MAIL1"
        Me.colE_MAIL1.Name = "colE_MAIL1"
        '
        'colADDRESS2
        '
        Me.colADDRESS2.FieldName = "ADDRESS2"
        Me.colADDRESS2.Name = "colADDRESS2"
        '
        'colPINCODE2
        '
        Me.colPINCODE2.FieldName = "PINCODE2"
        Me.colPINCODE2.Name = "colPINCODE2"
        '
        'colTELEPHONE2
        '
        Me.colTELEPHONE2.FieldName = "TELEPHONE2"
        Me.colTELEPHONE2.Name = "colTELEPHONE2"
        '
        'colBLOODGROUP
        '
        Me.colBLOODGROUP.FieldName = "BLOODGROUP"
        Me.colBLOODGROUP.Name = "colBLOODGROUP"
        '
        'colEMPPHOTO
        '
        Me.colEMPPHOTO.FieldName = "EMPPHOTO"
        Me.colEMPPHOTO.Name = "colEMPPHOTO"
        '
        'colEMPSIGNATURE
        '
        Me.colEMPSIGNATURE.FieldName = "EMPSIGNATURE"
        Me.colEMPSIGNATURE.Name = "colEMPSIGNATURE"
        '
        'colDivisionCode
        '
        Me.colDivisionCode.FieldName = "DivisionCode"
        Me.colDivisionCode.Name = "colDivisionCode"
        '
        'colGradeCode
        '
        Me.colGradeCode.FieldName = "GradeCode"
        Me.colGradeCode.Name = "colGradeCode"
        '
        'colLeavingdate
        '
        Me.colLeavingdate.FieldName = "Leavingdate"
        Me.colLeavingdate.Name = "colLeavingdate"
        '
        'colLeavingReason
        '
        Me.colLeavingReason.FieldName = "LeavingReason"
        Me.colLeavingReason.Name = "colLeavingReason"
        '
        'colVehicleNo
        '
        Me.colVehicleNo.FieldName = "VehicleNo"
        Me.colVehicleNo.Name = "colVehicleNo"
        '
        'colPFNO
        '
        Me.colPFNO.FieldName = "PFNO"
        Me.colPFNO.Name = "colPFNO"
        '
        'colPF_NO
        '
        Me.colPF_NO.FieldName = "PF_NO"
        Me.colPF_NO.Name = "colPF_NO"
        '
        'colESINO
        '
        Me.colESINO.FieldName = "ESINO"
        Me.colESINO.Name = "colESINO"
        '
        'colAUTHORISEDMACHINE
        '
        Me.colAUTHORISEDMACHINE.FieldName = "AUTHORISEDMACHINE"
        Me.colAUTHORISEDMACHINE.Name = "colAUTHORISEDMACHINE"
        '
        'colEMPTYPE
        '
        Me.colEMPTYPE.FieldName = "EMPTYPE"
        Me.colEMPTYPE.Name = "colEMPTYPE"
        '
        'colBankAcc
        '
        Me.colBankAcc.FieldName = "BankAcc"
        Me.colBankAcc.Name = "colBankAcc"
        '
        'colbankCODE
        '
        Me.colbankCODE.FieldName = "bankCODE"
        Me.colbankCODE.Name = "colbankCODE"
        '
        'colReporting1
        '
        Me.colReporting1.FieldName = "Reporting1"
        Me.colReporting1.Name = "colReporting1"
        '
        'colReporting2
        '
        Me.colReporting2.FieldName = "Reporting2"
        Me.colReporting2.Name = "colReporting2"
        '
        'colBRANCHCODE
        '
        Me.colBRANCHCODE.FieldName = "BRANCHCODE"
        Me.colBRANCHCODE.Name = "colBRANCHCODE"
        '
        'colDESPANSARYCODE
        '
        Me.colDESPANSARYCODE.FieldName = "DESPANSARYCODE"
        Me.colDESPANSARYCODE.Name = "colDESPANSARYCODE"
        '
        'colValidityStartDate
        '
        Me.colValidityStartDate.FieldName = "ValidityStartDate"
        Me.colValidityStartDate.Name = "colValidityStartDate"
        Me.colValidityStartDate.Visible = True
        Me.colValidityStartDate.VisibleIndex = 5
        '
        'colValidityEndDate
        '
        Me.colValidityEndDate.FieldName = "ValidityEndDate"
        Me.colValidityEndDate.Name = "colValidityEndDate"
        '
        'colEmployeeGroupId
        '
        Me.colEmployeeGroupId.FieldName = "EmployeeGroupId"
        Me.colEmployeeGroupId.Name = "colEmployeeGroupId"
        '
        'colLastModifiedBy
        '
        Me.colLastModifiedBy.FieldName = "LastModifiedBy"
        Me.colLastModifiedBy.Name = "colLastModifiedBy"
        '
        'colLastModifiedDate
        '
        Me.colLastModifiedDate.FieldName = "LastModifiedDate"
        Me.colLastModifiedDate.Name = "colLastModifiedDate"
        '
        'colMachineCard
        '
        Me.colMachineCard.Caption = "MachineCard"
        Me.colMachineCard.FieldName = "MachineCard"
        Me.colMachineCard.Name = "colMachineCard"
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(785, 4)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(68, 23)
        Me.SimpleButton1.TabIndex = 33
        Me.SimpleButton1.Text = "Close"
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.LabelControl1)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanel1.Location = New System.Drawing.Point(0, 0)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(865, 30)
        Me.SidePanel1.TabIndex = 35
        Me.SidePanel1.Text = "SidePanel1"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl1.Appearance.ForeColor = System.Drawing.Color.SteelBlue
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Appearance.Options.UseForeColor = True
        Me.LabelControl1.Location = New System.Drawing.Point(13, 4)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(98, 19)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "LabelControl1"
        '
        'SidePanel2
        '
        Me.SidePanel2.Controls.Add(Me.SimpleButton1)
        Me.SidePanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel2.Location = New System.Drawing.Point(0, 506)
        Me.SidePanel2.Name = "SidePanel2"
        Me.SidePanel2.Size = New System.Drawing.Size(865, 30)
        Me.SidePanel2.TabIndex = 36
        Me.SidePanel2.Text = "SidePanel2"
        '
        'SidePanel3
        '
        Me.SidePanel3.Controls.Add(Me.GroupControl2)
        Me.SidePanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanel3.Location = New System.Drawing.Point(0, 30)
        Me.SidePanel3.Name = "SidePanel3"
        Me.SidePanel3.Size = New System.Drawing.Size(865, 476)
        Me.SidePanel3.TabIndex = 37
        Me.SidePanel3.Text = "SidePanel3"
        '
        'XtraEmployeeExperiedValidity
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(865, 536)
        Me.ControlBox = False
        Me.Controls.Add(Me.SidePanel3)
        Me.Controls.Add(Me.SidePanel2)
        Me.Controls.Add(Me.SidePanel1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraEmployeeExperiedValidity"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "  "
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        Me.SidePanel1.PerformLayout()
        Me.SidePanel2.ResumeLayout(False)
        Me.SidePanel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SSSDBDataSet As ULtra.SSSDBDataSet
    Friend WithEvents TblMachineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblMachineTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblMachineTableAdapter
    Friend WithEvents TblMachine1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblMachine1TableAdapter
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblEmployeeTableAdapter As ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents colACTIVE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPAYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGUARDIANNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateOFBIRTH As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateOFJOIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPRESENTCARDNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEPARTMENTCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSEX As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISMARRIED As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBUS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colQUALIFICATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEXPERIENCE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDESIGNATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colADDRESS1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPINCODE1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTELEPHONE1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colE_MAIL1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colADDRESS2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPINCODE2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTELEPHONE2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBLOODGROUP As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPPHOTO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPSIGNATURE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDivisionCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGradeCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLeavingdate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLeavingReason As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVehicleNo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPFNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPF_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colESINO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAUTHORISEDMACHINE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPTYPE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBankAcc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbankCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colReporting1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colReporting2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRANCHCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDESPANSARYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colValidityStartDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colValidityEndDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEmployeeGroupId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblEmployee1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colMachineCard As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SidePanel2 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanel3 As DevExpress.XtraEditors.SidePanel
End Class
