﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraEditors.Repository
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports DevExpress.XtraGrid

Public Class XtraTimeOfficePolicy
    Dim ulf As UserLookAndFeel
    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraTimeOfficePolicy_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        'Me.Width = My.Computer.Screen.WorkingArea.Width
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.Width = SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = (SplitContainerControl1.Parent.Width) * 85 / 100

        'not to do in all pages
        Common.splitforMasterMenuWidth = SplitContainerControl1.Width
        Common.SplitterPosition = SplitContainerControl1.SplitterPosition

        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\HTDB.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            Me.TblSetup1TableAdapter1.Fill(Me.SSSDBDataSet.tblSetup1)
            GridControl1.DataSource = SSSDBDataSet.tblSetup1
        Else
            'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            TblSetupTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.TblSetupTableAdapter.Fill(Me.SSSDBDataSet.tblSetup)
            GridControl1.DataSource = SSSDBDataSet.tblSetup
        End If
    End Sub
    Private Sub GridView1_RowDeleted(sender As System.Object, e As DevExpress.Data.RowDeletedEventArgs) Handles GridView1.RowDeleted
        Me.TblSetupTableAdapter.Update(Me.SSSDBDataSet.tblSetup)
        Me.TblSetup1TableAdapter1.Update(Me.SSSDBDataSet.tblSetup1)
        XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
    End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
            Else
                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet = New DataSet
                Dim sSql As String = "select * from tblSetup"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count = 1 Then
                    Me.Validate()
                    e.Handled = True
                    XtraMessageBox.Show(ulf, "<size=10>Minimum one record is necessary.</size>", "<size=9>Error</size>")
                End If
                Common.Load_Corporate_PolicySql()
            End If
        End If
    End Sub
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        e.Allow = False
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            XtraTimeOfficePolicyEdit.ShowDialog()
            If Common.servername = "Access" Then   'to refresh the grid
                Me.TblSetup1TableAdapter1.Fill(Me.SSSDBDataSet.tblSetup1)
            Else
                Me.TblSetupTableAdapter.Fill(Me.SSSDBDataSet.tblSetup)
            End If
        End If
    End Sub

    Private Sub GridControl1_ProcessGridKey(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles GridControl1.ProcessGridKey
        Dim grid = CType(sender, GridControl)
         Dim view = CType(grid.FocusedView, GridView)
        If (e.KeyData = Keys.Delete) Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                view.DeleteSelectedRows()
                e.Handled = True
            End If
        End If
    End Sub
End Class
