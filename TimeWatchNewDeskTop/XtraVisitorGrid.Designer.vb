﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraVisitorGrid
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.TblVisitorTransactionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New ULtra.SSSDBDataSet()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colVISIT_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVisitor_Name = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVisit_Date = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colINTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOUTTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAPPTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAPPOINTMENT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFV_No = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVCompanyName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVCompanyaDDRESS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPLOYEECODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEPARTMENTcode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVEHICLENO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colContactNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCO_VISITOR1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCO_VISITOR2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTOOLS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUSER_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colpurpose = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVERIFIED = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colENTRYTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colvisitor_image = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTIMESPEND = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBlackListed = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colvFinger = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colvFingerImage = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.Bar2 = New DevExpress.XtraBars.Bar()
        Me.BarManager2 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarDockControl1 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl2 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl3 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl4 = New DevExpress.XtraBars.BarDockControl()
        Me.TblVisitorTransactionTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblVisitorTransactionTableAdapter()
        Me.TblVisitorTransaction1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblVisitorTransaction1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblVisitorTransactionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 31)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GridControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1250, 537)
        Me.SplitContainerControl1.SplitterPosition = 1165
        Me.SplitContainerControl1.TabIndex = 1
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblVisitorTransactionBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Remove.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(1165, 537)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'TblVisitorTransactionBindingSource
        '
        Me.TblVisitorTransactionBindingSource.DataMember = "tblVisitorTransaction"
        Me.TblVisitorTransactionBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Appearance.TopNewRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GridView1.Appearance.TopNewRow.ForeColor = System.Drawing.Color.Blue
        Me.GridView1.Appearance.TopNewRow.Options.UseFont = True
        Me.GridView1.Appearance.TopNewRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colVISIT_NO, Me.colVisitor_Name, Me.colVisit_Date, Me.colINTIME, Me.colOUTTIME, Me.colAPPTIME, Me.colAPPOINTMENT, Me.colFV_No, Me.colVCompanyName, Me.colVCompanyaDDRESS, Me.colEMPLOYEECODE, Me.colDEPARTMENTcode, Me.colVEHICLENO, Me.colContactNO, Me.colCO_VISITOR1, Me.colCO_VISITOR2, Me.colTOOLS, Me.colUSER_R, Me.colpurpose, Me.colVERIFIED, Me.colENTRYTIME, Me.colvisitor_image, Me.colTIMESPEND, Me.colBlackListed, Me.colvFinger, Me.colvFingerImage})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colVisit_Date, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'colVISIT_NO
        '
        Me.colVISIT_NO.Caption = "Visit No"
        Me.colVISIT_NO.FieldName = "VISIT_NO"
        Me.colVISIT_NO.Name = "colVISIT_NO"
        Me.colVISIT_NO.Visible = True
        Me.colVISIT_NO.VisibleIndex = 0
        Me.colVISIT_NO.Width = 120
        '
        'colVisitor_Name
        '
        Me.colVisitor_Name.Caption = "Name"
        Me.colVisitor_Name.FieldName = "Visitor_Name"
        Me.colVisitor_Name.Name = "colVisitor_Name"
        Me.colVisitor_Name.Visible = True
        Me.colVisitor_Name.VisibleIndex = 1
        Me.colVisitor_Name.Width = 120
        '
        'colVisit_Date
        '
        Me.colVisit_Date.Caption = "Date"
        Me.colVisit_Date.FieldName = "Visit_Date"
        Me.colVisit_Date.Name = "colVisit_Date"
        Me.colVisit_Date.Visible = True
        Me.colVisit_Date.VisibleIndex = 2
        Me.colVisit_Date.Width = 100
        '
        'colINTIME
        '
        Me.colINTIME.Caption = "In Time"
        Me.colINTIME.FieldName = "INTIME"
        Me.colINTIME.Name = "colINTIME"
        Me.colINTIME.Visible = True
        Me.colINTIME.VisibleIndex = 3
        '
        'colOUTTIME
        '
        Me.colOUTTIME.Caption = "Out Time"
        Me.colOUTTIME.FieldName = "OUTTIME"
        Me.colOUTTIME.Name = "colOUTTIME"
        Me.colOUTTIME.Visible = True
        Me.colOUTTIME.VisibleIndex = 4
        '
        'colAPPTIME
        '
        Me.colAPPTIME.Caption = "App Time"
        Me.colAPPTIME.FieldName = "APPTIME"
        Me.colAPPTIME.Name = "colAPPTIME"
        Me.colAPPTIME.Visible = True
        Me.colAPPTIME.VisibleIndex = 5
        '
        'colAPPOINTMENT
        '
        Me.colAPPOINTMENT.Caption = "Appoinment Time"
        Me.colAPPOINTMENT.FieldName = "APPOINTMENT"
        Me.colAPPOINTMENT.Name = "colAPPOINTMENT"
        Me.colAPPOINTMENT.Width = 100
        '
        'colFV_No
        '
        Me.colFV_No.Caption = "Phone No."
        Me.colFV_No.FieldName = "FV_No"
        Me.colFV_No.Name = "colFV_No"
        Me.colFV_No.Visible = True
        Me.colFV_No.VisibleIndex = 6
        Me.colFV_No.Width = 100
        '
        'colVCompanyName
        '
        Me.colVCompanyName.Caption = "Company"
        Me.colVCompanyName.FieldName = "VCompanyName"
        Me.colVCompanyName.Name = "colVCompanyName"
        Me.colVCompanyName.Visible = True
        Me.colVCompanyName.VisibleIndex = 7
        Me.colVCompanyName.Width = 120
        '
        'colVCompanyaDDRESS
        '
        Me.colVCompanyaDDRESS.Caption = "Company Address"
        Me.colVCompanyaDDRESS.FieldName = "VCompanyaDDRESS"
        Me.colVCompanyaDDRESS.Name = "colVCompanyaDDRESS"
        Me.colVCompanyaDDRESS.Visible = True
        Me.colVCompanyaDDRESS.VisibleIndex = 8
        Me.colVCompanyaDDRESS.Width = 120
        '
        'colEMPLOYEECODE
        '
        Me.colEMPLOYEECODE.Caption = "To Meet"
        Me.colEMPLOYEECODE.FieldName = "EMPLOYEECODE"
        Me.colEMPLOYEECODE.Name = "colEMPLOYEECODE"
        Me.colEMPLOYEECODE.Visible = True
        Me.colEMPLOYEECODE.VisibleIndex = 9
        Me.colEMPLOYEECODE.Width = 100
        '
        'colDEPARTMENTcode
        '
        Me.colDEPARTMENTcode.Caption = "Department"
        Me.colDEPARTMENTcode.FieldName = "DEPARTMENTcode"
        Me.colDEPARTMENTcode.Name = "colDEPARTMENTcode"
        Me.colDEPARTMENTcode.Visible = True
        Me.colDEPARTMENTcode.VisibleIndex = 10
        Me.colDEPARTMENTcode.Width = 100
        '
        'colVEHICLENO
        '
        Me.colVEHICLENO.Caption = "Vehicle No"
        Me.colVEHICLENO.FieldName = "VEHICLENO"
        Me.colVEHICLENO.Name = "colVEHICLENO"
        Me.colVEHICLENO.Visible = True
        Me.colVEHICLENO.VisibleIndex = 11
        '
        'colContactNO
        '
        Me.colContactNO.Caption = "Contact No"
        Me.colContactNO.FieldName = "ContactNO"
        Me.colContactNO.Name = "colContactNO"
        Me.colContactNO.Visible = True
        Me.colContactNO.VisibleIndex = 12
        Me.colContactNO.Width = 100
        '
        'colCO_VISITOR1
        '
        Me.colCO_VISITOR1.Caption = "Co Visitor 1"
        Me.colCO_VISITOR1.FieldName = "CO_VISITOR1"
        Me.colCO_VISITOR1.Name = "colCO_VISITOR1"
        Me.colCO_VISITOR1.Visible = True
        Me.colCO_VISITOR1.VisibleIndex = 13
        Me.colCO_VISITOR1.Width = 120
        '
        'colCO_VISITOR2
        '
        Me.colCO_VISITOR2.Caption = "Co Visitor 2"
        Me.colCO_VISITOR2.FieldName = "CO_VISITOR2"
        Me.colCO_VISITOR2.Name = "colCO_VISITOR2"
        Me.colCO_VISITOR2.Visible = True
        Me.colCO_VISITOR2.VisibleIndex = 14
        Me.colCO_VISITOR2.Width = 120
        '
        'colTOOLS
        '
        Me.colTOOLS.Caption = "Tools"
        Me.colTOOLS.FieldName = "TOOLS"
        Me.colTOOLS.Name = "colTOOLS"
        Me.colTOOLS.Visible = True
        Me.colTOOLS.VisibleIndex = 15
        Me.colTOOLS.Width = 120
        '
        'colUSER_R
        '
        Me.colUSER_R.Caption = "User"
        Me.colUSER_R.FieldName = "USER_R"
        Me.colUSER_R.Name = "colUSER_R"
        Me.colUSER_R.Visible = True
        Me.colUSER_R.VisibleIndex = 16
        '
        'colpurpose
        '
        Me.colpurpose.FieldName = "purpose"
        Me.colpurpose.Name = "colpurpose"
        '
        'colVERIFIED
        '
        Me.colVERIFIED.FieldName = "VERIFIED"
        Me.colVERIFIED.Name = "colVERIFIED"
        '
        'colENTRYTIME
        '
        Me.colENTRYTIME.FieldName = "ENTRYTIME"
        Me.colENTRYTIME.Name = "colENTRYTIME"
        Me.colENTRYTIME.Visible = True
        Me.colENTRYTIME.VisibleIndex = 17
        '
        'colvisitor_image
        '
        Me.colvisitor_image.FieldName = "visitor_image"
        Me.colvisitor_image.Name = "colvisitor_image"
        '
        'colTIMESPEND
        '
        Me.colTIMESPEND.Caption = "Time Spend"
        Me.colTIMESPEND.FieldName = "TIMESPEND"
        Me.colTIMESPEND.Name = "colTIMESPEND"
        Me.colTIMESPEND.Visible = True
        Me.colTIMESPEND.VisibleIndex = 18
        '
        'colBlackListed
        '
        Me.colBlackListed.FieldName = "BlackListed"
        Me.colBlackListed.Name = "colBlackListed"
        Me.colBlackListed.Visible = True
        Me.colBlackListed.VisibleIndex = 19
        '
        'colvFinger
        '
        Me.colvFinger.FieldName = "vFinger"
        Me.colvFinger.Name = "colvFinger"
        '
        'colvFingerImage
        '
        Me.colvFingerImage.FieldName = "vFingerImage"
        Me.colvFingerImage.Name = "colvFingerImage"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = "Here we can get the information of all visitor visited." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "By default data is arr" & _
    "anged in descending order of time of visitor visited." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Here we can also get th" & _
    "e data in excel sheet."
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(77, 537)
        Me.MemoEdit1.TabIndex = 3
        '
        'Bar2
        '
        Me.Bar2.BarName = "Main menu"
        Me.Bar2.DockCol = 0
        Me.Bar2.DockRow = 0
        Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar2.OptionsBar.MultiLine = True
        Me.Bar2.OptionsBar.UseWholeRow = True
        Me.Bar2.Text = "Main menu"
        '
        'BarManager2
        '
        Me.BarManager2.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager2.DockControls.Add(Me.BarDockControl1)
        Me.BarManager2.DockControls.Add(Me.BarDockControl2)
        Me.BarManager2.DockControls.Add(Me.BarDockControl3)
        Me.BarManager2.DockControls.Add(Me.BarDockControl4)
        Me.BarManager2.Form = Me
        Me.BarManager2.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItem3, Me.BarButtonItem4})
        Me.BarManager2.MainMenu = Me.Bar1
        Me.BarManager2.MaxItemId = 2
        '
        'Bar1
        '
        Me.Bar1.BarAppearance.Hovered.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.Bar1.BarAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.Bar1.BarAppearance.Hovered.Options.UseFont = True
        Me.Bar1.BarAppearance.Hovered.Options.UseForeColor = True
        Me.Bar1.BarAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Bar1.BarAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.Bar1.BarAppearance.Normal.Options.UseFont = True
        Me.Bar1.BarAppearance.Normal.Options.UseForeColor = True
        Me.Bar1.BarAppearance.Pressed.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Bar1.BarAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.Bar1.BarAppearance.Pressed.Options.UseFont = True
        Me.Bar1.BarAppearance.Pressed.Options.UseForeColor = True
        Me.Bar1.BarName = "Main menu"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem3), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem4)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.MultiLine = True
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Main menu"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "Export"
        Me.BarButtonItem3.Id = 0
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "Import"
        Me.BarButtonItem4.Id = 1
        Me.BarButtonItem4.Name = "BarButtonItem4"
        Me.BarButtonItem4.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BarDockControl1
        '
        Me.BarDockControl1.CausesValidation = False
        Me.BarDockControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.BarDockControl1.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl1.Manager = Me.BarManager2
        Me.BarDockControl1.Size = New System.Drawing.Size(1250, 31)
        '
        'BarDockControl2
        '
        Me.BarDockControl2.CausesValidation = False
        Me.BarDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BarDockControl2.Location = New System.Drawing.Point(0, 568)
        Me.BarDockControl2.Manager = Me.BarManager2
        Me.BarDockControl2.Size = New System.Drawing.Size(1250, 0)
        '
        'BarDockControl3
        '
        Me.BarDockControl3.CausesValidation = False
        Me.BarDockControl3.Dock = System.Windows.Forms.DockStyle.Left
        Me.BarDockControl3.Location = New System.Drawing.Point(0, 31)
        Me.BarDockControl3.Manager = Me.BarManager2
        Me.BarDockControl3.Size = New System.Drawing.Size(0, 537)
        '
        'BarDockControl4
        '
        Me.BarDockControl4.CausesValidation = False
        Me.BarDockControl4.Dock = System.Windows.Forms.DockStyle.Right
        Me.BarDockControl4.Location = New System.Drawing.Point(1250, 31)
        Me.BarDockControl4.Manager = Me.BarManager2
        Me.BarDockControl4.Size = New System.Drawing.Size(0, 537)
        '
        'TblVisitorTransactionTableAdapter
        '
        Me.TblVisitorTransactionTableAdapter.ClearBeforeFill = True
        '
        'TblVisitorTransaction1TableAdapter1
        '
        Me.TblVisitorTransaction1TableAdapter1.ClearBeforeFill = True
        '
        'XtraVisitorGrid
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Controls.Add(Me.BarDockControl3)
        Me.Controls.Add(Me.BarDockControl4)
        Me.Controls.Add(Me.BarDockControl2)
        Me.Controls.Add(Me.BarDockControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraVisitorGrid"
        Me.Size = New System.Drawing.Size(1250, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblVisitorTransactionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
    Friend WithEvents BarManager2 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarDockControl1 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl2 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl3 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl4 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TblVisitorTransactionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As ULtra.SSSDBDataSet
    Friend WithEvents colVISIT_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVisit_Date As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVisitor_Name As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVCompanyName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVCompanyaDDRESS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVEHICLENO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colContactNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFV_No As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEPARTMENTcode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPLOYEECODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCO_VISITOR1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCO_VISITOR2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colUSER_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colINTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOUTTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colpurpose As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAPPOINTMENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVERIFIED As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAPPTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colENTRYTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colvisitor_image As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTOOLS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTIMESPEND As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBlackListed As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colvFinger As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colvFingerImage As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblVisitorTransactionTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblVisitorTransactionTableAdapter
    Friend WithEvents TblVisitorTransaction1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblVisitorTransaction1TableAdapter

End Class
