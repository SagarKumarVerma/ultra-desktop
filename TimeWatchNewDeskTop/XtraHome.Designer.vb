﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraHome
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TileItemElement12 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraHome))
        Dim TileItemElement13 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement14 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement15 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement16 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemFrame4 As DevExpress.XtraEditors.TileItemFrame = New DevExpress.XtraEditors.TileItemFrame()
        Dim TileItemElement17 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemFrame5 As DevExpress.XtraEditors.TileItemFrame = New DevExpress.XtraEditors.TileItemFrame()
        Dim TileItemElement18 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemFrame6 As DevExpress.XtraEditors.TileItemFrame = New DevExpress.XtraEditors.TileItemFrame()
        Dim TileItemElement19 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement20 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement21 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement22 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Me.NavigationFrame1 = New DevExpress.XtraBars.Navigation.NavigationFrame()
        Me.NavigationPage1 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.TileControl3 = New DevExpress.XtraEditors.TileControl()
        Me.TileGroup7 = New DevExpress.XtraEditors.TileGroup()
        Me.TileItem7 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem8 = New DevExpress.XtraEditors.TileItem()
        Me.TileGroup3 = New DevExpress.XtraEditors.TileGroup()
        Me.TileItem9 = New DevExpress.XtraEditors.TileItem()
        Me.TileGroup4 = New DevExpress.XtraEditors.TileGroup()
        Me.TileItem13 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem10 = New DevExpress.XtraEditors.TileItem()
        Me.TileGroup6 = New DevExpress.XtraEditors.TileGroup()
        Me.TileItem12 = New DevExpress.XtraEditors.TileItem()
        Me.TileControl1 = New DevExpress.XtraEditors.TileControl()
        Me.TileControl2 = New DevExpress.XtraEditors.TileControl()
        Me.LabelControlHld = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControlEmp = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.SidePanel2 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControlPresent = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.SidePanel3 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControlAbsent = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.SidePanel4 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControlWO = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl5 = New DevExpress.XtraEditors.PanelControl()
        Me.SidePanel5 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControlLeave = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl6 = New DevExpress.XtraEditors.PanelControl()
        Me.SidePanel6 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControlLate = New DevExpress.XtraEditors.LabelControl()
        Me.TileItem11 = New DevExpress.XtraEditors.TileItem()
        Me.TileGroup5 = New DevExpress.XtraEditors.TileGroup()
        Me.TileItem1 = New DevExpress.XtraEditors.TileItem()
        CType(Me.NavigationFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        Me.SidePanel1.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        Me.SidePanel2.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        Me.SidePanel3.SuspendLayout()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        Me.SidePanel4.SuspendLayout()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl5.SuspendLayout()
        Me.SidePanel5.SuspendLayout()
        CType(Me.PanelControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl6.SuspendLayout()
        Me.SidePanel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavigationFrame1
        '
        Me.NavigationFrame1.Location = New System.Drawing.Point(0, 0)
        Me.NavigationFrame1.Name = "NavigationFrame1"
        Me.NavigationFrame1.SelectedPage = Nothing
        Me.NavigationFrame1.Size = New System.Drawing.Size(0, 0)
        Me.NavigationFrame1.TabIndex = 0
        '
        'NavigationPage1
        '
        Me.NavigationPage1.Caption = "NavigationPage1"
        Me.NavigationPage1.Name = "NavigationPage1"
        Me.NavigationPage1.Size = New System.Drawing.Size(200, 100)
        '
        'TileControl3
        '
        Me.TileControl3.AppearanceItem.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TileControl3.AppearanceItem.Normal.Options.UseFont = True
        Me.TileControl3.BackColor = System.Drawing.Color.SteelBlue
        Me.TileControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TileControl3.Groups.Add(Me.TileGroup7)
        Me.TileControl3.Groups.Add(Me.TileGroup3)
        Me.TileControl3.Groups.Add(Me.TileGroup4)
        Me.TileControl3.Groups.Add(Me.TileGroup6)
        Me.TileControl3.Location = New System.Drawing.Point(0, 0)
        Me.TileControl3.MaxId = 14
        Me.TileControl3.Name = "TileControl3"
        Me.TileControl3.Size = New System.Drawing.Size(1432, 523)
        Me.TileControl3.TabIndex = 2
        Me.TileControl3.Text = "TileControl3"
        '
        'TileGroup7
        '
        Me.TileGroup7.Items.Add(Me.TileItem7)
        Me.TileGroup7.Items.Add(Me.TileItem8)
        Me.TileGroup7.Name = "TileGroup7"
        Me.TileGroup7.Visible = False
        '
        'TileItem7
        '
        TileItemElement12.Image = CType(resources.GetObject("TileItemElement12.Image"), System.Drawing.Image)
        TileItemElement12.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter
        TileItemElement12.Text = "Device"
        TileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter
        Me.TileItem7.Elements.Add(TileItemElement12)
        Me.TileItem7.Id = 2
        Me.TileItem7.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium
        Me.TileItem7.Name = "TileItem7"
        '
        'TileItem8
        '
        TileItemElement13.Image = CType(resources.GetObject("TileItemElement13.Image"), System.Drawing.Image)
        TileItemElement13.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter
        TileItemElement13.Text = "Communication"
        TileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter
        Me.TileItem8.Elements.Add(TileItemElement13)
        Me.TileItem8.Id = 3
        Me.TileItem8.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium
        Me.TileItem8.Name = "TileItem8"
        '
        'TileGroup3
        '
        Me.TileGroup3.Items.Add(Me.TileItem9)
        Me.TileGroup3.Items.Add(Me.TileItem1)
        Me.TileGroup3.Name = "TileGroup3"
        '
        'TileItem9
        '
        TileItemElement14.Appearance.Normal.Font = New System.Drawing.Font("Tahoma", 25.0!)
        TileItemElement14.Appearance.Normal.ForeColor = System.Drawing.Color.Lime
        TileItemElement14.Appearance.Normal.Options.UseFont = True
        TileItemElement14.Appearance.Normal.Options.UseForeColor = True
        TileItemElement14.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter
        TileItemElement14.Text = "458"
        TileItemElement14.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleRight
        Me.TileItem9.Elements.Add(TileItemElement14)
        Me.TileItem9.Id = 4
        Me.TileItem9.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem9.Name = "TileItem9"
        '
        'TileGroup4
        '
        Me.TileGroup4.Items.Add(Me.TileItem13)
        Me.TileGroup4.Items.Add(Me.TileItem10)
        Me.TileGroup4.Name = "TileGroup4"
        '
        'TileItem13
        '
        TileItemElement15.Text = "TileItem13"
        Me.TileItem13.Elements.Add(TileItemElement15)
        Me.TileItem13.Id = 12
        Me.TileItem13.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem13.Name = "TileItem13"
        '
        'TileItem10
        '
        TileItemElement16.Image = CType(resources.GetObject("TileItemElement16.Image"), System.Drawing.Image)
        TileItemElement16.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter
        TileItemElement16.Text = "Reports"
        TileItemElement16.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter
        Me.TileItem10.Elements.Add(TileItemElement16)
        TileItemFrame4.Animation = DevExpress.XtraEditors.TileItemContentAnimationType.ScrollTop
        TileItemElement17.Image = CType(resources.GetObject("TileItemElement17.Image"), System.Drawing.Image)
        TileItemElement17.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter
        TileItemElement17.Text = "Reports"
        TileItemElement17.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter
        TileItemFrame4.Elements.Add(TileItemElement17)
        TileItemFrame4.Image = CType(resources.GetObject("TileItemFrame4.Image"), System.Drawing.Image)
        TileItemFrame4.Interval = 100
        TileItemFrame5.Animation = DevExpress.XtraEditors.TileItemContentAnimationType.ScrollDown
        TileItemElement18.Image = CType(resources.GetObject("TileItemElement18.Image"), System.Drawing.Image)
        TileItemElement18.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter
        TileItemElement18.Text = "Reports"
        TileItemElement18.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter
        TileItemFrame5.Elements.Add(TileItemElement18)
        TileItemFrame5.Image = CType(resources.GetObject("TileItemFrame5.Image"), System.Drawing.Image)
        TileItemFrame5.Interval = 100
        TileItemElement19.Image = CType(resources.GetObject("TileItemElement19.Image"), System.Drawing.Image)
        TileItemElement19.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter
        TileItemElement19.Text = "Reports"
        TileItemElement19.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter
        TileItemFrame6.Elements.Add(TileItemElement19)
        TileItemFrame6.Image = CType(resources.GetObject("TileItemFrame6.Image"), System.Drawing.Image)
        Me.TileItem10.Frames.Add(TileItemFrame4)
        Me.TileItem10.Frames.Add(TileItemFrame5)
        Me.TileItem10.Frames.Add(TileItemFrame6)
        Me.TileItem10.Id = 6
        Me.TileItem10.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem10.Name = "TileItem10"
        '
        'TileGroup6
        '
        Me.TileGroup6.Items.Add(Me.TileItem12)
        Me.TileGroup6.Name = "TileGroup6"
        '
        'TileItem12
        '
        TileItemElement20.Image = CType(resources.GetObject("TileItemElement20.Image"), System.Drawing.Image)
        TileItemElement20.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter
        TileItemElement20.Text = "Master"
        TileItemElement20.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter
        Me.TileItem12.Elements.Add(TileItemElement20)
        Me.TileItem12.Id = 8
        Me.TileItem12.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium
        Me.TileItem12.Name = "TileItem12"
        '
        'TileControl1
        '
        Me.TileControl1.AppearanceItem.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TileControl1.AppearanceItem.Normal.Options.UseFont = True
        Me.TileControl1.Location = New System.Drawing.Point(0, 0)
        Me.TileControl1.Name = "TileControl1"
        Me.TileControl1.Size = New System.Drawing.Size(240, 150)
        Me.TileControl1.TabIndex = 0
        '
        'TileControl2
        '
        Me.TileControl2.AppearanceItem.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TileControl2.AppearanceItem.Normal.Options.UseFont = True
        Me.TileControl2.Location = New System.Drawing.Point(0, 0)
        Me.TileControl2.Name = "TileControl2"
        Me.TileControl2.Size = New System.Drawing.Size(240, 150)
        Me.TileControl2.TabIndex = 0
        '
        'LabelControlHld
        '
        Me.LabelControlHld.Appearance.BackColor = System.Drawing.Color.SteelBlue
        Me.LabelControlHld.Appearance.BackColor2 = System.Drawing.Color.SteelBlue
        Me.LabelControlHld.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControlHld.Appearance.ForeColor = System.Drawing.Color.White
        Me.LabelControlHld.Appearance.Options.UseBackColor = True
        Me.LabelControlHld.Appearance.Options.UseFont = True
        Me.LabelControlHld.Appearance.Options.UseForeColor = True
        Me.LabelControlHld.Location = New System.Drawing.Point(1229, 123)
        Me.LabelControlHld.Name = "LabelControlHld"
        Me.LabelControlHld.Size = New System.Drawing.Size(47, 16)
        Me.LabelControlHld.TabIndex = 14
        Me.LabelControlHld.Text = "Holiday"
        Me.LabelControlHld.Visible = False
        '
        'PanelControl1
        '
        Me.PanelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.PanelControl1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.PanelControl1.Appearance.Options.UseBackColor = True
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl1.Controls.Add(Me.SidePanel1)
        Me.PanelControl1.Controls.Add(Me.LabelControlEmp)
        Me.PanelControl1.Location = New System.Drawing.Point(219, 39)
        Me.PanelControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.PanelControl1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(200, 78)
        Me.PanelControl1.TabIndex = 15
        Me.PanelControl1.Visible = False
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.LabelControl1)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel1.Location = New System.Drawing.Point(0, 55)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(200, 23)
        Me.SidePanel1.TabIndex = 6
        Me.SidePanel1.Text = "SidePanel1"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.LabelControl1.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LabelControl1.Appearance.Options.UseBackColor = True
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Appearance.Options.UseForeColor = True
        Me.LabelControl1.Location = New System.Drawing.Point(3, 4)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(104, 16)
        Me.LabelControl1.TabIndex = 4
        Me.LabelControl1.Text = "Total Employees"
        '
        'LabelControlEmp
        '
        Me.LabelControlEmp.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LabelControlEmp.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LabelControlEmp.Appearance.Font = New System.Drawing.Font("Tahoma", 15.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControlEmp.Appearance.ForeColor = System.Drawing.Color.White
        Me.LabelControlEmp.Appearance.Options.UseBackColor = True
        Me.LabelControlEmp.Appearance.Options.UseFont = True
        Me.LabelControlEmp.Appearance.Options.UseForeColor = True
        Me.LabelControlEmp.Location = New System.Drawing.Point(129, 19)
        Me.LabelControlEmp.Name = "LabelControlEmp"
        Me.LabelControlEmp.Size = New System.Drawing.Size(50, 24)
        Me.LabelControlEmp.TabIndex = 5
        Me.LabelControlEmp.Text = "Total"
        '
        'PanelControl2
        '
        Me.PanelControl2.Appearance.BackColor = System.Drawing.Color.LimeGreen
        Me.PanelControl2.Appearance.BackColor2 = System.Drawing.Color.LimeGreen
        Me.PanelControl2.Appearance.Options.UseBackColor = True
        Me.PanelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl2.Controls.Add(Me.SidePanel2)
        Me.PanelControl2.Controls.Add(Me.LabelControlPresent)
        Me.PanelControl2.Location = New System.Drawing.Point(465, 39)
        Me.PanelControl2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.PanelControl2.LookAndFeel.UseDefaultLookAndFeel = False
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(200, 78)
        Me.PanelControl2.TabIndex = 16
        Me.PanelControl2.Visible = False
        '
        'SidePanel2
        '
        Me.SidePanel2.Controls.Add(Me.LabelControl7)
        Me.SidePanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel2.Location = New System.Drawing.Point(0, 55)
        Me.SidePanel2.Name = "SidePanel2"
        Me.SidePanel2.Size = New System.Drawing.Size(200, 23)
        Me.SidePanel2.TabIndex = 6
        Me.SidePanel2.Text = "SidePanel2"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.LabelControl7.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl7.Appearance.ForeColor = System.Drawing.Color.LimeGreen
        Me.LabelControl7.Appearance.Options.UseBackColor = True
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Appearance.Options.UseForeColor = True
        Me.LabelControl7.Location = New System.Drawing.Point(3, 4)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(51, 16)
        Me.LabelControl7.TabIndex = 4
        Me.LabelControl7.Text = "Present"
        '
        'LabelControlPresent
        '
        Me.LabelControlPresent.Appearance.BackColor = System.Drawing.Color.LimeGreen
        Me.LabelControlPresent.Appearance.BackColor2 = System.Drawing.Color.LimeGreen
        Me.LabelControlPresent.Appearance.Font = New System.Drawing.Font("Tahoma", 15.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControlPresent.Appearance.ForeColor = System.Drawing.Color.White
        Me.LabelControlPresent.Appearance.Options.UseBackColor = True
        Me.LabelControlPresent.Appearance.Options.UseFont = True
        Me.LabelControlPresent.Appearance.Options.UseForeColor = True
        Me.LabelControlPresent.Location = New System.Drawing.Point(129, 19)
        Me.LabelControlPresent.Name = "LabelControlPresent"
        Me.LabelControlPresent.Size = New System.Drawing.Size(50, 24)
        Me.LabelControlPresent.TabIndex = 5
        Me.LabelControlPresent.Text = "Total"
        '
        'PanelControl3
        '
        Me.PanelControl3.Appearance.BackColor = System.Drawing.Color.Red
        Me.PanelControl3.Appearance.BackColor2 = System.Drawing.Color.Red
        Me.PanelControl3.Appearance.Options.UseBackColor = True
        Me.PanelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl3.Controls.Add(Me.SidePanel3)
        Me.PanelControl3.Controls.Add(Me.LabelControlAbsent)
        Me.PanelControl3.Location = New System.Drawing.Point(713, 39)
        Me.PanelControl3.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.PanelControl3.LookAndFeel.UseDefaultLookAndFeel = False
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(200, 78)
        Me.PanelControl3.TabIndex = 17
        Me.PanelControl3.Visible = False
        '
        'SidePanel3
        '
        Me.SidePanel3.Controls.Add(Me.LabelControl2)
        Me.SidePanel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel3.Location = New System.Drawing.Point(0, 55)
        Me.SidePanel3.Name = "SidePanel3"
        Me.SidePanel3.Size = New System.Drawing.Size(200, 23)
        Me.SidePanel3.TabIndex = 6
        Me.SidePanel3.Text = "SidePanel3"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.LabelControl2.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl2.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl2.Appearance.Options.UseBackColor = True
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Appearance.Options.UseForeColor = True
        Me.LabelControl2.Location = New System.Drawing.Point(3, 4)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(47, 16)
        Me.LabelControl2.TabIndex = 4
        Me.LabelControl2.Text = "Absent"
        '
        'LabelControlAbsent
        '
        Me.LabelControlAbsent.Appearance.BackColor = System.Drawing.Color.Red
        Me.LabelControlAbsent.Appearance.BackColor2 = System.Drawing.Color.Red
        Me.LabelControlAbsent.Appearance.Font = New System.Drawing.Font("Tahoma", 15.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControlAbsent.Appearance.ForeColor = System.Drawing.Color.White
        Me.LabelControlAbsent.Appearance.Options.UseBackColor = True
        Me.LabelControlAbsent.Appearance.Options.UseFont = True
        Me.LabelControlAbsent.Appearance.Options.UseForeColor = True
        Me.LabelControlAbsent.Location = New System.Drawing.Point(129, 19)
        Me.LabelControlAbsent.Name = "LabelControlAbsent"
        Me.LabelControlAbsent.Size = New System.Drawing.Size(50, 24)
        Me.LabelControlAbsent.TabIndex = 5
        Me.LabelControlAbsent.Text = "Total"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LabelControl8.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 15.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl8.Appearance.ForeColor = System.Drawing.Color.White
        Me.LabelControl8.Appearance.Options.UseBackColor = True
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Appearance.Options.UseForeColor = True
        Me.LabelControl8.Location = New System.Drawing.Point(129, 19)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(50, 24)
        Me.LabelControl8.TabIndex = 5
        Me.LabelControl8.Text = "Total"
        '
        'PanelControl4
        '
        Me.PanelControl4.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PanelControl4.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PanelControl4.Appearance.Options.UseBackColor = True
        Me.PanelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl4.Controls.Add(Me.SidePanel4)
        Me.PanelControl4.Controls.Add(Me.LabelControlWO)
        Me.PanelControl4.Location = New System.Drawing.Point(13, 36)
        Me.PanelControl4.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.PanelControl4.LookAndFeel.UseDefaultLookAndFeel = False
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(200, 78)
        Me.PanelControl4.TabIndex = 18
        Me.PanelControl4.Visible = False
        '
        'SidePanel4
        '
        Me.SidePanel4.Controls.Add(Me.LabelControl9)
        Me.SidePanel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel4.Location = New System.Drawing.Point(0, 55)
        Me.SidePanel4.Name = "SidePanel4"
        Me.SidePanel4.Size = New System.Drawing.Size(200, 23)
        Me.SidePanel4.TabIndex = 6
        Me.SidePanel4.Text = "SidePanel4"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.LabelControl9.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl9.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl9.Appearance.Options.UseBackColor = True
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Appearance.Options.UseForeColor = True
        Me.LabelControl9.Location = New System.Drawing.Point(3, 4)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(60, 16)
        Me.LabelControl9.TabIndex = 4
        Me.LabelControl9.Text = "Week Off"
        '
        'LabelControlWO
        '
        Me.LabelControlWO.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControlWO.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControlWO.Appearance.Font = New System.Drawing.Font("Tahoma", 15.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControlWO.Appearance.ForeColor = System.Drawing.Color.White
        Me.LabelControlWO.Appearance.Options.UseBackColor = True
        Me.LabelControlWO.Appearance.Options.UseFont = True
        Me.LabelControlWO.Appearance.Options.UseForeColor = True
        Me.LabelControlWO.Location = New System.Drawing.Point(129, 19)
        Me.LabelControlWO.Name = "LabelControlWO"
        Me.LabelControlWO.Size = New System.Drawing.Size(50, 24)
        Me.LabelControlWO.TabIndex = 5
        Me.LabelControlWO.Text = "Total"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LabelControl10.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 15.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl10.Appearance.ForeColor = System.Drawing.Color.White
        Me.LabelControl10.Appearance.Options.UseBackColor = True
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Appearance.Options.UseForeColor = True
        Me.LabelControl10.Location = New System.Drawing.Point(129, 19)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(50, 24)
        Me.LabelControl10.TabIndex = 5
        Me.LabelControl10.Text = "Total"
        '
        'PanelControl5
        '
        Me.PanelControl5.Appearance.BackColor = System.Drawing.Color.Khaki
        Me.PanelControl5.Appearance.BackColor2 = System.Drawing.Color.Khaki
        Me.PanelControl5.Appearance.Options.UseBackColor = True
        Me.PanelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl5.Controls.Add(Me.SidePanel5)
        Me.PanelControl5.Controls.Add(Me.LabelControlLeave)
        Me.PanelControl5.Location = New System.Drawing.Point(1125, 36)
        Me.PanelControl5.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.PanelControl5.LookAndFeel.UseDefaultLookAndFeel = False
        Me.PanelControl5.Name = "PanelControl5"
        Me.PanelControl5.Size = New System.Drawing.Size(200, 78)
        Me.PanelControl5.TabIndex = 19
        Me.PanelControl5.Visible = False
        '
        'SidePanel5
        '
        Me.SidePanel5.Controls.Add(Me.LabelControl11)
        Me.SidePanel5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel5.Location = New System.Drawing.Point(0, 55)
        Me.SidePanel5.Name = "SidePanel5"
        Me.SidePanel5.Size = New System.Drawing.Size(200, 23)
        Me.SidePanel5.TabIndex = 6
        Me.SidePanel5.Text = "SidePanel5"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.LabelControl11.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl11.Appearance.ForeColor = System.Drawing.Color.Khaki
        Me.LabelControl11.Appearance.Options.UseBackColor = True
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Appearance.Options.UseForeColor = True
        Me.LabelControl11.Location = New System.Drawing.Point(3, 4)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl11.TabIndex = 4
        Me.LabelControl11.Text = "Leave"
        '
        'LabelControlLeave
        '
        Me.LabelControlLeave.Appearance.BackColor = System.Drawing.Color.Khaki
        Me.LabelControlLeave.Appearance.BackColor2 = System.Drawing.Color.Khaki
        Me.LabelControlLeave.Appearance.Font = New System.Drawing.Font("Tahoma", 15.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControlLeave.Appearance.ForeColor = System.Drawing.Color.White
        Me.LabelControlLeave.Appearance.Options.UseBackColor = True
        Me.LabelControlLeave.Appearance.Options.UseFont = True
        Me.LabelControlLeave.Appearance.Options.UseForeColor = True
        Me.LabelControlLeave.Location = New System.Drawing.Point(129, 19)
        Me.LabelControlLeave.Name = "LabelControlLeave"
        Me.LabelControlLeave.Size = New System.Drawing.Size(50, 24)
        Me.LabelControlLeave.TabIndex = 5
        Me.LabelControlLeave.Text = "Total"
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl12.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 15.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl12.Appearance.ForeColor = System.Drawing.Color.White
        Me.LabelControl12.Appearance.Options.UseBackColor = True
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Appearance.Options.UseForeColor = True
        Me.LabelControl12.Location = New System.Drawing.Point(129, 19)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(50, 24)
        Me.LabelControl12.TabIndex = 5
        Me.LabelControl12.Text = "Total"
        '
        'PanelControl6
        '
        Me.PanelControl6.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.PanelControl6.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.PanelControl6.Appearance.Options.UseBackColor = True
        Me.PanelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl6.Controls.Add(Me.SidePanel6)
        Me.PanelControl6.Controls.Add(Me.LabelControlLate)
        Me.PanelControl6.Location = New System.Drawing.Point(919, 39)
        Me.PanelControl6.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.PanelControl6.LookAndFeel.UseDefaultLookAndFeel = False
        Me.PanelControl6.Name = "PanelControl6"
        Me.PanelControl6.Size = New System.Drawing.Size(200, 78)
        Me.PanelControl6.TabIndex = 20
        Me.PanelControl6.Visible = False
        '
        'SidePanel6
        '
        Me.SidePanel6.Controls.Add(Me.LabelControl13)
        Me.SidePanel6.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel6.Location = New System.Drawing.Point(0, 55)
        Me.SidePanel6.Name = "SidePanel6"
        Me.SidePanel6.Size = New System.Drawing.Size(200, 23)
        Me.SidePanel6.TabIndex = 6
        Me.SidePanel6.Text = "SidePanel6"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.LabelControl13.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl13.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.LabelControl13.Appearance.Options.UseBackColor = True
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Appearance.Options.UseForeColor = True
        Me.LabelControl13.Location = New System.Drawing.Point(3, 4)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(77, 16)
        Me.LabelControl13.TabIndex = 4
        Me.LabelControl13.Text = "Late Arrival"
        '
        'LabelControlLate
        '
        Me.LabelControlLate.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.LabelControlLate.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.LabelControlLate.Appearance.Font = New System.Drawing.Font("Tahoma", 15.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControlLate.Appearance.ForeColor = System.Drawing.Color.White
        Me.LabelControlLate.Appearance.Options.UseBackColor = True
        Me.LabelControlLate.Appearance.Options.UseFont = True
        Me.LabelControlLate.Appearance.Options.UseForeColor = True
        Me.LabelControlLate.Location = New System.Drawing.Point(129, 19)
        Me.LabelControlLate.Name = "LabelControlLate"
        Me.LabelControlLate.Size = New System.Drawing.Size(50, 24)
        Me.LabelControlLate.TabIndex = 5
        Me.LabelControlLate.Text = "Total"
        '
        'TileItem11
        '
        TileItemElement21.Image = CType(resources.GetObject("TileItemElement21.Image"), System.Drawing.Image)
        TileItemElement21.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter
        TileItemElement21.Text = "Employee Management"
        TileItemElement21.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter
        Me.TileItem11.Elements.Add(TileItemElement21)
        Me.TileItem11.Id = 7
        Me.TileItem11.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium
        Me.TileItem11.Name = "TileItem11"
        '
        'TileGroup5
        '
        Me.TileGroup5.Items.Add(Me.TileItem11)
        Me.TileGroup5.Name = "TileGroup5"
        '
        'TileItem1
        '
        TileItemElement22.Text = "TileItem1"
        Me.TileItem1.Elements.Add(TileItemElement22)
        Me.TileItem1.Id = 13
        Me.TileItem1.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem1.Name = "TileItem1"
        '
        'XtraHome
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.PanelControl6)
        Me.Controls.Add(Me.PanelControl5)
        Me.Controls.Add(Me.PanelControl4)
        Me.Controls.Add(Me.PanelControl3)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.LabelControlHld)
        Me.Controls.Add(Me.TileControl3)
        Me.Name = "XtraHome"
        Me.Size = New System.Drawing.Size(1432, 523)
        CType(Me.NavigationFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        Me.SidePanel1.ResumeLayout(False)
        Me.SidePanel1.PerformLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        Me.SidePanel2.ResumeLayout(False)
        Me.SidePanel2.PerformLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        Me.SidePanel3.ResumeLayout(False)
        Me.SidePanel3.PerformLayout()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        Me.PanelControl4.PerformLayout()
        Me.SidePanel4.ResumeLayout(False)
        Me.SidePanel4.PerformLayout()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl5.ResumeLayout(False)
        Me.PanelControl5.PerformLayout()
        Me.SidePanel5.ResumeLayout(False)
        Me.SidePanel5.PerformLayout()
        CType(Me.PanelControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl6.ResumeLayout(False)
        Me.PanelControl6.PerformLayout()
        Me.SidePanel6.ResumeLayout(False)
        Me.SidePanel6.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NavigationFrame1 As DevExpress.XtraBars.Navigation.NavigationFrame
    Friend WithEvents NavigationPage1 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents TileControl1 As DevExpress.XtraEditors.TileControl
    Friend WithEvents TileControl2 As DevExpress.XtraEditors.TileControl
    Friend WithEvents TileControl3 As DevExpress.XtraEditors.TileControl
    Friend WithEvents TileGroup3 As DevExpress.XtraEditors.TileGroup
    Friend WithEvents TileItem7 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem8 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem9 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileGroup4 As DevExpress.XtraEditors.TileGroup
    Friend WithEvents TileItem10 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem12 As DevExpress.XtraEditors.TileItem
    Friend WithEvents LabelControlHld As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControlEmp As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SidePanel2 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlPresent As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SidePanel3 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlAbsent As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SidePanel4 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlWO As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl5 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SidePanel5 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlLeave As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl6 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SidePanel6 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlLate As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TileGroup7 As DevExpress.XtraEditors.TileGroup
    Friend WithEvents TileItem13 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileGroup6 As DevExpress.XtraEditors.TileGroup
    Friend WithEvents TileItem11 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileGroup5 As DevExpress.XtraEditors.TileGroup
    Friend WithEvents TileItem1 As DevExpress.XtraEditors.TileItem

End Class
