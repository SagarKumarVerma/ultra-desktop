﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class XtraTest
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Me.TabPane1 = New DevExpress.XtraBars.Navigation.TabPane()
        Me.TabNavigationPage1 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.TabNavigationPage2 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.textRes = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControlZKDevice = New DevExpress.XtraGrid.GridControl()
        Me.GridViewZKDevice = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.TabPane1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPane1.SuspendLayout()
        Me.TabNavigationPage2.SuspendLayout()
        CType(Me.textRes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.GridControlZKDevice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewZKDevice, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabPane1
        '
        Me.TabPane1.AllowTransitionAnimation = DevExpress.Utils.DefaultBoolean.[True]
        Me.TabPane1.Appearance.FontStyleDelta = System.Drawing.FontStyle.Italic
        Me.TabPane1.Appearance.Options.UseFont = True
        Me.TabPane1.AppearanceButton.Hovered.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Underline)
        Me.TabPane1.AppearanceButton.Hovered.Options.UseFont = True
        Me.TabPane1.AppearanceButton.Normal.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TabPane1.AppearanceButton.Normal.Options.UseFont = True
        Me.TabPane1.AppearanceButton.Pressed.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TabPane1.AppearanceButton.Pressed.Options.UseFont = True
        Me.TabPane1.Controls.Add(Me.TabNavigationPage1)
        Me.TabPane1.Controls.Add(Me.TabNavigationPage2)
        Me.TabPane1.Location = New System.Drawing.Point(12, 12)
        Me.TabPane1.LookAndFeel.SkinName = "iMaginary"
        Me.TabPane1.Name = "TabPane1"
        Me.TabPane1.Pages.AddRange(New DevExpress.XtraBars.Navigation.NavigationPageBase() {Me.TabNavigationPage1, Me.TabNavigationPage2})
        Me.TabPane1.RegularSize = New System.Drawing.Size(231, 235)
        Me.TabPane1.SelectedPage = Me.TabNavigationPage1
        Me.TabPane1.Size = New System.Drawing.Size(231, 235)
        Me.TabPane1.TabIndex = 105
        Me.TabPane1.Text = "Template"
        Me.TabPane1.TransitionType = DevExpress.Utils.Animation.Transitions.Cover
        '
        'TabNavigationPage1
        '
        Me.TabNavigationPage1.Caption = "Office Details"
        Me.TabNavigationPage1.Name = "TabNavigationPage1"
        Me.TabNavigationPage1.Size = New System.Drawing.Size(213, 184)
        '
        'TabNavigationPage2
        '
        Me.TabNavigationPage2.Caption = "Template"
        Me.TabNavigationPage2.Controls.Add(Me.textRes)
        Me.TabNavigationPage2.Controls.Add(Me.GroupControl5)
        Me.TabNavigationPage2.Name = "TabNavigationPage2"
        Me.TabNavigationPage2.Size = New System.Drawing.Size(741, 413)
        '
        'textRes
        '
        Me.textRes.Location = New System.Drawing.Point(10, 454)
        Me.textRes.Name = "textRes"
        Me.textRes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.textRes.Properties.Appearance.Options.UseFont = True
        Me.textRes.Properties.ReadOnly = True
        Me.textRes.Size = New System.Drawing.Size(190, 20)
        Me.textRes.TabIndex = 62
        Me.textRes.Visible = False
        '
        'GroupControl5
        '
        Me.GroupControl5.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl5.AppearanceCaption.Options.UseFont = True
        Me.GroupControl5.Controls.Add(Me.GridControlZKDevice)
        Me.GroupControl5.Location = New System.Drawing.Point(151, 361)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(149, 120)
        Me.GroupControl5.TabIndex = 47
        Me.GroupControl5.Text = "Device List"
        Me.GroupControl5.Visible = False
        '
        'GridControlZKDevice
        '
        Me.GridControlZKDevice.Dock = System.Windows.Forms.DockStyle.Fill
        GridLevelNode1.RelationName = "Level1"
        Me.GridControlZKDevice.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridControlZKDevice.Location = New System.Drawing.Point(2, 23)
        Me.GridControlZKDevice.MainView = Me.GridViewZKDevice
        Me.GridControlZKDevice.Name = "GridControlZKDevice"
        Me.GridControlZKDevice.Size = New System.Drawing.Size(145, 95)
        Me.GridControlZKDevice.TabIndex = 1
        Me.GridControlZKDevice.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewZKDevice})
        '
        'GridViewZKDevice
        '
        Me.GridViewZKDevice.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8})
        Me.GridViewZKDevice.GridControl = Me.GridControlZKDevice
        Me.GridViewZKDevice.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridViewZKDevice.Name = "GridViewZKDevice"
        Me.GridViewZKDevice.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewZKDevice.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewZKDevice.OptionsBehavior.Editable = False
        Me.GridViewZKDevice.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewZKDevice.OptionsSelection.MultiSelect = True
        Me.GridViewZKDevice.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewZKDevice.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn4, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Controller Id"
        Me.GridColumn4.FieldName = "ID_NO"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 1
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Device IP"
        Me.GridColumn5.FieldName = "LOCATION"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 2
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Location"
        Me.GridColumn6.FieldName = "branch"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 3
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Device Type"
        Me.GridColumn7.FieldName = "DeviceType"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 4
        '
        'GridColumn8
        '
        Me.GridColumn8.FieldName = "A_R"
        Me.GridColumn8.Name = "GridColumn8"
        '
        'XtraTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(759, 464)
        Me.Controls.Add(Me.TabPane1)
        Me.Name = "XtraTest"
        Me.Text = "XtraTest"
        CType(Me.TabPane1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPane1.ResumeLayout(False)
        Me.TabNavigationPage2.ResumeLayout(False)
        CType(Me.textRes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        CType(Me.GridControlZKDevice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewZKDevice, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabPane1 As DevExpress.XtraBars.Navigation.TabPane
    Friend WithEvents TabNavigationPage1 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents TabNavigationPage2 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents textRes As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControlZKDevice As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewZKDevice As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
End Class
