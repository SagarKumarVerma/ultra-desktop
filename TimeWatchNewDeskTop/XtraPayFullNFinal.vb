﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb

Public Class XtraPayFullNFinal
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim leaveFlage As Boolean = True
    Dim rsPaysetup As DataSet
    Dim bCapture As Boolean
    Dim bProcess As Boolean
    Dim netSal As Double
    Dim gratuityAmt As Double, MYEAR As Double
    Dim strLVcash As String
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        If Common.servername = "Access" Then
            Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        Else
            TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        End If
    End Sub
    Private Sub XtraPayMaintenance_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100
        GroupControl2.Width = GroupControl2.Parent.Width / 2
        LookUpEdit1.Properties.DataSource = Common.EmpNonAdmin
        setDefault()
    End Sub
    Private Sub LookUpEdit1_Leave(sender As System.Object, e As System.EventArgs) Handles LookUpEdit1.Leave
        If Not leaveFlage Then Return


        leaveFlage = False
        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim ds1 As DataSet
        Dim sSql As String = "select PAYCODE from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
        If LookUpEdit1.EditValue.ToString.Trim <> "" Then
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                If LookUpEdit1.EditValue.ToString.Trim <> "" Then
                    XtraMessageBox.Show(ulf, "<size=10>No such Employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    LookUpEdit1.Select()
                    setDefault()
                    leaveFlage = True
                    Exit Sub
                End If
            Else


                sSql = "select * from pay_fullfinal where paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
                Dim rsChk As DataSet = New DataSet 'Cn.Execute(sSql)
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsChk)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsChk)
                End If
                If rsChk.Tables(0).Rows.Count > 0 Then
                    'WorkMessage("Already Exist, are you sure to delete this record ? ", "Confirm", True)
                    If XtraMessageBox.Show(ulf, "<size=10>Already Exist, are you sure to delete this record ? </size>", "<size=9>Confirmation</size>", _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                        sSql = "update tblemployee set active='Y',leavingdate=null,leavingreason=null where paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If

                        sSql = "UPDATE tblLeaveledger set L01_Cash=L01_cash-" & rsChk.Tables(0).Rows(0)("L01_Cash") & ",L02_Cash=L02_cash-" & rsChk.Tables(0).Rows(0)("L02_Cash") & ",L03_Cash=L03_cash-" & rsChk.Tables(0).Rows(0)("L03_Cash") & ",L04_Cash=L04_cash-" & rsChk.Tables(0).Rows(0)("L04_Cash") & ",L05_Cash=L05_cash-" & rsChk.Tables(0).Rows(0)("L05_Cash") & ",L06_Cash=L06_cash-" & rsChk.Tables(0).Rows(0)("L06_Cash") & ",L07_Cash=L07_cash-" & rsChk.Tables(0).Rows(0)("L07_Cash") & ",L08_Cash=L08_cash-" & rsChk.Tables(0).Rows(0)("L08_Cash") & ",L09_Cash=L09_cash-" & rsChk.Tables(0).Rows(0)("L09_Cash") & ",L10_Cash=L10_cash-" & rsChk.Tables(0).Rows(0)("L10_Cash") & ",L11_Cash=L11_cash-" & rsChk.Tables(0).Rows(0)("L11_Cash") & ",L12_Cash=L12_cash-" & rsChk.Tables(0).Rows(0)("L12_Cash") & ",L13_Cash=L13_cash-" & rsChk.Tables(0).Rows(0)("L13_Cash") & ",L14_Cash=L14_cash-" & rsChk.Tables(0).Rows(0)("L14_Cash") & ",L15_Cash=L15_cash-" & rsChk.Tables(0).Rows(0)("L15_Cash") & ",L16_Cash=L16_cash-" & rsChk.Tables(0).Rows(0)("L16_Cash") & ",L17_Cash=L17_cash-" & rsChk.Tables(0).Rows(0)("L17_Cash") & ",L18_Cash=L18_cash-" & rsChk.Tables(0).Rows(0)("L18_Cash") & ",L19_Cash=L19_cash-" & rsChk.Tables(0).Rows(0)("L19_Cash") & ",L20_Cash=L20_cash-" & rsChk.Tables(0).Rows(0)("L20_Cash") & _
                                    " Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "' and lyear=" & Year(rsChk.Tables(0).Rows(0)("mon_year"))
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                        sSql = "UPDATE PAY_LEAVEENCASHMENT  Set L01_Cash=L01_cash-" & rsChk.Tables(0).Rows(0)("L01_Cash") & ",L02_Cash=L02_cash-" & rsChk.Tables(0).Rows(0)("L02_Cash") & ",L03_Cash=L03_cash-" & rsChk.Tables(0).Rows(0)("L03_Cash") & ",L04_Cash=L04_cash-" & rsChk.Tables(0).Rows(0)("L04_Cash") & ",L05_Cash=L05_cash-" & rsChk.Tables(0).Rows(0)("L05_Cash") & ",L06_Cash=L06_cash-" & rsChk.Tables(0).Rows(0)("L06_Cash") & ",L07_Cash=L07_cash-" & rsChk.Tables(0).Rows(0)("L07_Cash") & ",L08_Cash=L08_cash-" & rsChk.Tables(0).Rows(0)("L08_Cash") & ",L09_Cash=L09_cash-" & rsChk.Tables(0).Rows(0)("L09_Cash") & ",L10_Cash=L10_cash-" & rsChk.Tables(0).Rows(0)("L10_Cash") & ",L11_Cash=L11_cash-" & rsChk.Tables(0).Rows(0)("L11_Cash") & ",L12_Cash=L12_cash-" & rsChk.Tables(0).Rows(0)("L12_Cash") & ",L13_Cash=L13_cash-" & rsChk.Tables(0).Rows(0)("L13_Cash") & ",L14_Cash=L14_cash-" & rsChk.Tables(0).Rows(0)("L14_Cash") & ",L15_Cash=L15_cash-" & rsChk.Tables(0).Rows(0)("L15_Cash") & ",L16_Cash=L16_cash-" & rsChk.Tables(0).Rows(0)("L16_Cash") & ",L17_Cash=L17_cash-" & rsChk.Tables(0).Rows(0)("L17_Cash") & ",L18_Cash=L18_cash-" & rsChk.Tables(0).Rows(0)("L18_Cash") & ",L19_Cash=L19_cash-" & rsChk.Tables(0).Rows(0)("L19_Cash") & ",L20_Cash=L20_cash-" & rsChk.Tables(0).Rows(0)("L20_Cash") & _
                                "Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "' AND left(ACC_YEAR,4)='" & Format(rsChk.Tables(0).Rows(0)("mon_year"), "yyyy") & "'"
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                        sSql = "delete from PAY_GRATUITY  Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                        sSql = "delete from pay_fullfinal where paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                    Else
                        setDefault()
                    End If
                End If




                Dim sSql1 As String = "select EMPNAME, PRESENTCARDNO, (select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId) as EmpGrp, (select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName, (select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT) as CATName, DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
                sSql1 = "select EMPNAME, PRESENTCARDNO, " & _
                       "(SELECT COMPANYNAME from tblCompany WHERE tblCompany.COMPANYCODE = TblEmployee.COMPANYCODE) as CompName, " & _
                       "(select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId) as EmpGrp," & _
                       "(select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName, " & _
                       "(select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT) as CATName, " & _
                       "(select GradeName from tblGrade WHERE tblGrade.GradeCode = TblEmployee.GradeCode) as GrdName, DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
                If Common.servername = "Access" Then
                    sSql1 = "select T.EMPNAME, T.PRESENTCARDNO, C.COMPANYNAME  as CompName,  T.DESIGNATION, E.GroupName as EmpGrp, D.DEPARTMENTNAME as DEPName, cat.CATAGORYNAME as CATName, grd.GradeName as GrdName  from TblEmployee T,  tblCompany C, EmployeeGroup E, tblDepartment D, tblCatagory cat, tblGrade grd where C.COMPANYCODE = T.COMPANYCODE  and E.GroupId = T.EmployeeGroupId and D.DEPARTMENTCODE = T.DEPARTMENTCODE and cat.CAT = T.CAT and grd.GradeCode = T.GradeCode and T.Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
                    adapA1 = New OleDbDataAdapter(sSql1, Common.con1)
                    ds1 = New DataSet
                    adapA1.Fill(ds1)
                Else
                    adap1 = New SqlDataAdapter(sSql1, Common.con)
                    ds1 = New DataSet
                    adap1.Fill(ds1)
                End If
                lblName.Text = ds1.Tables(0).Rows(0).Item("EMPNAME").ToString
                lblCardNum.Text = ds1.Tables(0).Rows(0).Item("PRESENTCARDNO").ToString
                lblDept.Text = ds1.Tables(0).Rows(0).Item("DEPName").ToString
            End If
        End If
        leaveFlage = True
        LoadLeaveQuota(LookUpEdit1.EditValue.ToString.Trim)
        btnApply.Enabled = True
    End Sub
    Private Sub setDefault()
        txtapplicablefrom.DateTime = Now
        LookUpEdit1.EditValue = ""
        lblName.Text = ""
        lblCardNum.Text = ""
        lblDept.Text = ""
        cmdOk.Enabled = False
        txtPayableMonth.DateTime = Now
        PopupContainerEditLeave.EditValue = ""
        GridViewLeave.ClearSelection()
        If Common.IsNepali = "Y" Then
            ComboNepaliYear.Visible = True
            ComboNEpaliMonth.Visible = True
            txtPayableMonth.Visible = False
            Dim DC As New DateConverter()
            Dim doj As String = DC.ToBS(New Date(txtPayableMonth.DateTime.Year, txtPayableMonth.DateTime.Month, 1))
            Dim dojTmp() As String = doj.Split("-")
            ComboNepaliYear.EditValue = dojTmp(0)
            ComboNEpaliMonth.SelectedIndex = dojTmp(1) - 1

        Else
            ComboNepaliYear.Visible = False
            ComboNEpaliMonth.Visible = False
            txtPayableMonth.Visible = True
        End If

        Dim sSql As String = "SELECT * FROM PAY_SETUP"
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        rsPaysetup = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsPaysetup)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsPaysetup)
        End If

        For n = 1 To Len(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")))
            If n = 1 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    Label3.Visible = True
                    txtEarningConv.Visible = True
                Else
                    Label3.Visible = False
                    txtEarningConv.Visible = False
                End If
            End If
            If n = 2 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    Label7.Visible = True
                    txtEarningMed.Visible = True
                Else
                    Label7.Visible = False
                    txtEarningMed.Visible = False
                End If
            End If
            If n = 3 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    lbl1.Visible = True
                    lbl1.Text = IIf(rsPaysetup.Tables(0).Rows(0)("VIES_1").ToString.Trim = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_1"))
                Else
                    lbl1.Text = ""
                    lbl1.Visible = False
                End If
            End If
            If n = 4 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    lbl2.Visible = True
                    lbl2.Text = IIf(rsPaysetup.Tables(0).Rows(0)("VIES_2").ToString.Trim = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_2"))
                Else
                    lbl2.Visible = False
                    lbl2.Text = ""
                End If
            End If
            If n = 5 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    lbl3.Visible = True
                    lbl3.Text = IIf(rsPaysetup.Tables(0).Rows(0)("VIES_3").ToString.Trim = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_3"))
                Else
                    lbl3.Visible = False
                    lbl3.Text = ""
                End If
            End If
            If n = 6 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    lbl4.Visible = True
                    lbl4.Text = IIf(rsPaysetup.Tables(0).Rows(0)("VIES_4").ToString.Trim = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_4"))
                Else
                    lbl4.Visible = False
                    lbl4.Text = ""
                End If
            End If
            If n = 7 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    lbl5.Visible = True
                    lbl5.Text = IIf(rsPaysetup.Tables(0).Rows(0)("VIES_5").ToString.Trim = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_5"))
                Else
                    lbl5.Visible = False
                    lbl5.Text = ""
                End If
            End If
            If n = 8 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    lbl6.Visible = True
                    lbl6.Text = IIf(rsPaysetup.Tables(0).Rows(0)("VIES_6").ToString.Trim = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_6"))
                Else
                    lbl6.Visible = False
                    lbl6.Text = ""
                End If
            End If
            If n = 9 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    lbl7.Visible = True
                    lbl7.Text = IIf(rsPaysetup.Tables(0).Rows(0)("VIES_7").ToString.Trim = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_7"))
                Else
                    lbl7.Visible = False
                    lbl7.Text = ""
                End If
            End If
            If n = 10 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    lbl8.Visible = True
                    lbl8.Text = IIf(rsPaysetup.Tables(0).Rows(0)("VIES_8").ToString.Trim = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_8"))
                Else
                    lbl8.Visible = False
                    lbl8.Text = ""
                End If
            End If
            If n = 11 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    lbl9.Visible = True
                    lbl9.Text = IIf(rsPaysetup.Tables(0).Rows(0)("VIES_9").ToString.Trim = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_9"))
                Else
                    lbl9.Visible = False
                    lbl9.Text = ""
                End If
            End If
            If n = 12 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    lbl10.Visible = True
                    lbl10.Text = IIf(rsPaysetup.Tables(0).Rows(0)("VIES_10").ToString.Trim = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_10"))
                Else
                    lbl10.Visible = False
                    lbl10.Text = ""
                End If
            End If
        Next n
        If Trim(lbl1.Text) = "" Then
            txtEarningEarn1.Visible = False
        End If
        If Trim(lbl2.Text) = "" Then
            txtEarningEarn2.Visible = False
        End If
        If Trim(lbl3.Text) = "" Then
            txtEarningEarn3.Visible = False
        End If
        If Trim(lbl4.Text) = "" Then
            txtEarningEarn4.Visible = False
        End If
        If Trim(lbl5.Text) = "" Then
            txtEarningEarn5.Visible = False
        End If
        If Trim(lbl6.Text) = "" Then
            txtEarningEarn6.Visible = False
        End If
        If Trim(lbl7.Text) = "" Then
            txtEarningEarn7.Visible = False
        End If
        If Trim(lbl8.Text) = "" Then
            txtEarningEarn8.Visible = False
        End If
        If Trim(lbl9.Text) = "" Then
            txtEarningEarn9.Visible = False
        End If
        If Trim(lbl10.Text) = "" Then
            txtEarningEarn10.Visible = False
        End If

        txtGratuity.Text = "000000.00"
        txtLeaveInCash.Text = "000000.00"
        txtNoticePay.Text = "000000.00"
        txtAdvance.Text = "000000.00"
        txtLoan.Text = "000000.00"
        txtEarningConv.Text = "000000.00"
        txtEarningMed.Text = "000000.00"
        txtEarningEarn1.Text = "000000.00"
        txtEarningEarn2.Text = "000000.00"
        txtEarningEarn3.Text = "000000.00"
        txtEarningEarn4.Text = "000000.00"
        txtEarningEarn5.Text = "000000.00"
        txtEarningEarn6.Text = "000000.00"
        txtEarningEarn7.Text = "000000.00"
        txtEarningEarn8.Text = "000000.00"
        txtEarningEarn9.Text = "000000.00"
        txtEarningEarn10.Text = "000000.00"
        txtNetPay.Text = "000000.00"
        txtLeave.Text = "00000.00"
        txtNoticeMonth.Text = "000"

    End Sub        
    Private Sub DateEditFrom_Leave(sender As System.Object, e As System.EventArgs) Handles txtPayableMonth.Leave
        'setValues()
    End Sub
    Private Sub ComboNepaliYear_Leave(sender As System.Object, e As System.EventArgs) Handles ComboNepaliYear.Leave
        'setValues()
    End Sub
    Private Sub PopupContainerEditLeave_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditLeave.QueryResultValue
        Dim selectedRows() As Integer = GridViewLeave.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewLeave.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("LEAVEFIELD"))
        Next
        e.Value = sb.ToString
    End Sub    
    Private Sub LoadLeaveQuota(paycode As String)
        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim sSql As String
        Dim ds, ds1 As DataSet

        sSql = "select EmployeeGroup.Id from TblEmployee, EmployeeGroup where TblEmployee.EmployeeGroupId=EmployeeGroup.GroupId and PAYCODE='" & paycode & "'"
        ds1 = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds1)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds1)
        End If
        If ds1.Tables(0).Rows.Count > 0 Then
            If Common.EmpGrpArr(ds1.Tables(0).Rows(0).Item(0)).ISCOMPOFF = "Y" Then
                sSql = "select LEAVEFIELD, LEAVECODE, LEAVEDESCRIPTION, isCompOffType from tblLeaveMaster  Order By LeaveField"
            Else
                sSql = "select LEAVEFIELD, LEAVECODE, LEAVEDESCRIPTION, isCompOffType from tblLeaveMaster where  leavetype='L' Order By LeaveField"
            End If
            sSql = "select LEAVEFIELD, LEAVECODE, LEAVEDESCRIPTION, isCompOffType from tblLeaveMaster where  leavetype='L' " & _
                    " and LEAVEFIELD in ('" & rsPaysetup.Tables(0).Rows(0)("LVFIELD").ToString.Trim.Replace(".", "','") & "')" & _
                    " Order By LeaveField"

            ds = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            Dim isCompOffType As String '= ds.Tables(0).Rows(0).Item("isCompOffType").ToString.Trim
            Dim s As String = ""
            Dim s1 As String = ""
            Dim dt As DataTable = New DataTable
            dt.Columns.Add("LEAVEFIELD")
            dt.Columns.Add("LeaveCode")
            dt.Columns.Add("LeaveLeft")
            Dim TotallftLEave As Double = 0
            For i As Integer = 1 To ds.Tables(0).Rows.Count
                isCompOffType = ds.Tables(0).Rows(i - 1).Item("isCompOffType").ToString.Trim
                s = "L" & i.ToString("00")
                s1 = "L" & i.ToString("00") & "_ADD "
                Dim sS As String = "select " & s & " , " & s1 & " from tblLeaveLedger where PAYCODE = '" & paycode & "' and LYEAR = " & txtapplicablefrom.DateTime.Year & ""
                'MsgBox(sS)
                If Common.servername = "Access" Then
                    adapA1 = New OleDbDataAdapter(sS, Common.con1)
                    ds1 = New DataSet
                    adapA1.Fill(ds1)
                Else
                    adap1 = New SqlDataAdapter(sS, Common.con)
                    ds1 = New DataSet
                    adap1.Fill(ds1)
                End If
                Dim lftLEave As String
                If ds1.Tables(0).Rows.Count = 0 Then
                    ''Try
                    'sSql = "select EmployeeGroupId from TblEmployee where PAYCODE ='" & paycode & "'"
                    'Dim adapX As SqlDataAdapter
                    'Dim adapAX As OleDbDataAdapter
                    'Dim dsX As DataSet = New DataSet
                    'Dim dsX1 As DataSet = New DataSet
                    'If Common.servername = "Access" Then
                    '    adapAX = New OleDbDataAdapter(sSql, Common.con1)
                    '    adapAX.Fill(dsX1)
                    'Else
                    '    adapX = New SqlDataAdapter(sSql, Common.con)
                    '    adapX.Fill(dsX1)
                    'End If
                    ''sSql = "SELECT * from EmployeeGroupLeaveLedger where GroupId = '" & dsX1.Tables(0).Rows(0).Item("EmployeeGroupId").ToString.Trim & "' and LYEAR=" & Now.Year & " "
                    'sSql = "SELECT * from EmployeeGroupLeaveLedger where GroupId = '" & dsX1.Tables(0).Rows(0).Item("EmployeeGroupId").ToString.Trim & "' and LYEAR=" & txtapplicablefrom.DateTime.Year & " "
                    'dsX = New DataSet
                    'If Common.servername = "Access" Then
                    '    adapAX = New OleDbDataAdapter(sSql, Common.con1)
                    '    adapAX.Fill(dsX)
                    'Else
                    '    adapX = New SqlDataAdapter(sSql, Common.con)
                    '    adapX.Fill(dsX)
                    'End If
                    'If dsX.Tables(0).Rows.Count = 0 Then
                    '    XtraMessageBox.Show(ulf, "<size=10>Leave Accrual Not Prsent. Please Create Leave Accrual.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    '    setDefault()
                    '    Exit Sub
                    'Else
                    '    Dim sSql3 As String = "insert into tblLeaveLedger (PAYCODE, L01_ADD, L02_ADD, L03_ADD, L04_ADD, L05_ADD, L06_ADD, L07_ADD, L08_ADD, L09_ADD, L10_ADD, L11_ADD, L12_ADD, L13_ADD, L14_ADD, L15_ADD, L16_ADD, L17_ADD, L18_ADD, L19_ADD, L20_ADD, ACC_FLAG, LYEAR, L01, L02, L03, L04, L05, L06, L07, L08, L09, L10, L11, L12, L13, L14, L15, L16, L17, L18, L19, L20) VALUES " & _
                    '                          "('" & paycode & "', " & dsX.Tables(0).Rows(0).Item("L01_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L02_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L03_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L04_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L05_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L06_ADD").ToString.Trim & ", " & _
                    '                         "" & dsX.Tables(0).Rows(0).Item("L07_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L08_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L09_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L10_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L11_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L12_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L13_ADD").ToString.Trim & ", " & _
                    '                         "" & dsX.Tables(0).Rows(0).Item("L14_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L15_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L16_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L17_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L18_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L19_ADD").ToString.Trim & "," & dsX.Tables(0).Rows(0).Item("L20_ADD").ToString.Trim & ",'N','" & txtapplicablefrom.DateTime.Year & "', " & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & ") "
                    '    If Common.servername = "Access" Then
                    '        If Common.con1.State <> ConnectionState.Open Then
                    '            Common.con1.Open()
                    '        End If
                    '        cmd1 = New OleDbCommand(sSql3, Common.con1)
                    '        cmd1.ExecuteNonQuery()
                    '        If Common.con1.State <> ConnectionState.Closed Then
                    '            Common.con1.Close()
                    '        End If
                    '    Else
                    '        If Common.con.State <> ConnectionState.Open Then
                    '            Common.con.Open()
                    '        End If
                    '        cmd = New SqlCommand(sSql3, Common.con)
                    '        cmd.ExecuteNonQuery()
                    '        If Common.con.State <> ConnectionState.Closed Then
                    '            Common.con.Close()
                    '        End If
                    '    End If
                    'End If
                    'lftLEave = "0"
                Else
                    lftLEave = ds1.Tables(0).Rows(0).Item(1).ToString - ds1.Tables(0).Rows(0).Item(0).ToString
                End If
                If isCompOffType = "Y" Then
                    lftLEave = "0"
                End If
                TotallftLEave = TotallftLEave + lftLEave
                dt.Rows.Add(ds.Tables(0).Rows(i - 1).Item("LEAVEFIELD").ToString.Trim, ds.Tables(0).Rows(i - 1).Item("LEAVECODE").ToString.Trim, lftLEave)
            Next
            Dim datase As DataSet = New DataSet()
            datase.Tables.Add(dt)
            GridControlLeave.DataSource = dt
            GridViewLeave.SelectAll()
            Dim selectedRows() As Integer = GridViewLeave.GetSelectedRows
            Dim sb As StringBuilder = New StringBuilder
            For Each selectionRow As Integer In selectedRows
                Dim a As System.Data.DataRowView = GridViewLeave.GetRow(selectionRow)
                If (sb.ToString.Length > 0) Then
                    sb.Append(", ")
                End If
                sb.Append(a.Item("LEAVEFIELD"))
            Next

            PopupContainerEditLeave.EditValue = sb.ToString
            txtLeave.Text = TotallftLEave
        End If
    End Sub
    Private Sub btnApply_Click(sender As System.Object, e As System.EventArgs) Handles btnApply.Click
        'Dim esiamt As Double, mothers As Double
        Dim datePrv As Date
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Me.Cursor = Cursors.WaitCursor
        Dim sSql As String = "select max(mon_year)as month from pay_result"
        Dim rspaymaster As DataSet = New DataSet 'Cn.Execute(sSql)
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rspaymaster)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rspaymaster)
        End If
        If rspaymaster.Tables(0).Rows.Count > 0 Then
            If rspaymaster.Tables(0).Rows(0)("month").ToString.Trim <> "" Then
                datePrv = rspaymaster.Tables(0).Rows(0)("month")
                'If Format(rspaymaster.Tables(0).Rows(0)("month"), "yyyyMM") > Format(txtapplicablefrom.DateTime, "yyyyMM") Then
                If Convert.ToDateTime(rspaymaster.Tables(0).Rows(0)("month")).ToString("yyyyMM") > txtapplicablefrom.DateTime.ToString("yyyyMM") Then
                    'MsgBox("Processing Month Already Closed", vbInformation)
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Processing Month Already Closed</size>", "<size=9>Information</size>")
                    Exit Sub
                    'ElseIf txtapplicablefrom.DateTime.ToString("yyyyMM") > Format(DateAdd("M", 1, datePrv), "yyyyMM") Then
                ElseIf txtapplicablefrom.DateTime.ToString("yyyyMM") > datePrv.AddMonths(1).ToString("yyyyMM") Then
                    'MsgBox("Previous Month Processing has not been done", vbInformation)
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Previous Month Processing has not been done</size>", "<size=9>Information</size>")
                    Exit Sub
                End If
            End If
        End If
        datePrv = txtapplicablefrom.DateTime.AddMonths(-1) '.ToString("yyyy-MM-dd") ' DateAdd("M", -1, txtapplicablefrom)
        cmdOk.Enabled = True
        sSql = "select paycode from pay_master where paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
        rspaymaster = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rspaymaster)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rspaymaster)
        End If
        'Dim pProcess As XtraPayrollProcess = New XtraPayrollProcess  'test
        If rspaymaster.Tables(0).Rows.Count > 0 Then
            bCapture = DATACAPTURE()
            sSql = "select A.*,B.GRADUTY_ALLOWED,c.dateofjoin,c.Leavingdate from pay_result A,PAY_MASTER B,tblemployee c where A.PAYCODE=B.PAYCODE and b.paycode=c.paycode AND A.PAYCODE Between '" & LookUpEdit1.EditValue.ToString.Trim & "' And '" & LookUpEdit1.EditValue.ToString.Trim & "' And DatePart(mm,A.Mon_Year)=" & Format(txtapplicablefrom.DateTime, "MM") & " And DatePart(yyyy,A.Mon_Year)=" & Format(txtapplicablefrom.DateTime, "yyyy")
            bProcess = DoProcess(sSql)
            'bProcess = pProcess.DoProcess(sSql)  'test
            sSql = "select A.* from pay_result A,PAY_MASTER B where A.PAYCODE=B.PAYCODE AND A.PAYCODE Between '" & LookUpEdit1.EditValue.ToString.Trim & "' And '" & LookUpEdit1.EditValue.ToString.Trim & "' And DatePart(mm,A.Mon_Year)=" & Format(txtapplicablefrom.DateTime, "MM") & " And DatePart(yyyy,A.Mon_Year)=" & Format(txtapplicablefrom.DateTime, "yyyy")
            Dim rsMonthTable As DataSet = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(rsMonthTable)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(rsMonthTable)
            End If
            If rsMonthTable.Tables(0).Rows.Count > 0 Then
                txtGratuity.Text = Format(Math.Round(gratuityAmt, 0), "000000.00")
                txtLeaveInCash.Text = "000000.00"
                For n = 1 To Len(Trim(rsPaysetup.Tables(0).Rows(0)("LEAVEREL")))
                    If n = 1 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("LEAVEREL")), n, 1) = "1" Then
                        txtLeaveInCash.Text = Format(Val(txtLeaveInCash.Text) + rsMonthTable.Tables(0).Rows(0)("vbasic"), "000000.00")
                    End If
                    If n = 2 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("LEAVEREL")), n, 1) = "1" Then
                        txtLeaveInCash.Text = Format(Val(txtLeaveInCash.Text) + rsMonthTable.Tables(0).Rows(0)("VHRA_RATE"), "000000.00")
                    End If
                    If n = 3 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("LEAVEREL")), n, 1) = "1" Then
                        txtLeaveInCash.Text = Format(Val(txtLeaveInCash.Text) + rsMonthTable.Tables(0).Rows(0)("VDA_RATE"), "000000.00")
                    End If
                    If n = 4 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("LEAVEREL")), n, 1) = "1" Then
                        txtLeaveInCash.Text = Format(Val(txtLeaveInCash.Text) + rsMonthTable.Tables(0).Rows(0)("VCONV_RATE"), "000000.00")
                    End If
                    If n = 5 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("LEAVEREL")), n, 1) = "1" Then
                        txtLeaveInCash.Text = Format(Val(txtLeaveInCash.Text) + rsMonthTable.Tables(0).Rows(0)("VMED_RATE"), "000000.00")
                    End If
                    If n = 6 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("LEAVEREL")), n, 1) = "1" Then
                        txtLeaveInCash.Text = Format(Val(txtLeaveInCash.Text) + rsMonthTable.Tables(0).Rows(0)("VI_1"), "000000.00")
                    End If
                    If n = 7 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("LEAVEREL")), n, 1) = "1" Then
                        txtLeaveInCash.Text = Format(Val(txtLeaveInCash.Text) + rsMonthTable.Tables(0).Rows(0)("VI_2"), "000000.00")
                    End If
                    If n = 8 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("LEAVEREL")), n, 1) = "1" Then
                        txtLeaveInCash.Text = Format(Val(txtLeaveInCash.Text) + rsMonthTable.Tables(0).Rows(0)("VI_3"), "000000.00")
                    End If
                    If n = 9 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("LEAVEREL")), n, 1) = "1" Then
                        txtLeaveInCash.Text = Format(Val(txtLeaveInCash.Text) + rsMonthTable.Tables(0).Rows(0)("VI_4"), "000000.00")
                    End If
                    If n = 10 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("LEAVEREL")), n, 1) = "1" Then
                        txtLeaveInCash.Text = Format(Val(txtLeaveInCash.Text) + rsMonthTable.Tables(0).Rows(0)("VI_5"), "000000.00")
                    End If
                    If n = 11 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("LEAVEREL")), n, 1) = "1" Then
                        txtLeaveInCash.Text = Format(Val(txtLeaveInCash.Text) + rsMonthTable.Tables(0).Rows(0)("VI_6"), "000000.00")
                    End If
                    If n = 12 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("LEAVEREL")), n, 1) = "1" Then
                        txtLeaveInCash.Text = Format(Val(txtLeaveInCash.Text) + rsMonthTable.Tables(0).Rows(0)("VI_7"), "000000.00")
                    End If
                    If n = 13 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("LEAVEREL")), n, 1) = "1" Then
                        txtLeaveInCash.Text = Format(Val(txtLeaveInCash.Text) + rsMonthTable.Tables(0).Rows(0)("VI_8"), "000000.00")
                    End If
                    If n = 14 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("LEAVEREL")), n, 1) = "1" Then
                        txtLeaveInCash.Text = Format(Val(txtLeaveInCash.Text) + rsMonthTable.Tables(0).Rows(0)("VI_9"), "000000.00")
                    End If
                    If n = 15 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("LEAVEREL")), n, 1) = "1" Then
                        txtLeaveInCash.Text = Format(Val(txtLeaveInCash.Text) + rsMonthTable.Tables(0).Rows(0)("VI_10"), "000000.00")
                    End If
                Next n
                txtLeaveInCash.Text = Format(Math.Round(((Val(txtLeaveInCash.Text) / 30) * txtLeave.Text.Trim), 0), "000000.00")
                txtNoticePay.Text = Format(Math.Round(((rsMonthTable.Tables(0).Rows(0)("vbasic") / 30) * txtNoticeMonth.Text.Trim), 0), "000000.00")
                sSql = "Select Sum(Inst_Amt) as InstAmt From TBLADVANCEDATA " & _
                    "Where Paycode='" & Trim(rsMonthTable.Tables(0).Rows(0)("Paycode")) & _
                    "' And mon_year>'" & Format(txtapplicablefrom.DateTime, "yyyy-MM-dd") & "'" & _
                    " And A_L='" & "A'"
                Dim rsAdvance As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsAdvance)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsAdvance)
                End If
                txtAdvance.Text = IIf(rsAdvance.Tables(0).Rows(0)("InstAmt").ToString.Trim = "", "000000.00", rsAdvance.Tables(0).Rows(0)("InstAmt"))
                sSql = "Select Sum(Inst_Amt) AS InstAmt From TBLADVANCEDATA " & _
                    "Where Paycode='" & Trim(rsMonthTable.Tables(0).Rows(0)("Paycode")) & _
                    "' And mon_year>'" & Format(txtapplicablefrom.DateTime, "yyyy-MM-dd") & "'" & _
                    " And A_L='" & "L'"
                Dim rsFine As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsFine)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsFine)
                End If
                txtLoan.Text = IIf(rsFine.Tables(0).Rows(0)("InstAmt").ToString.Trim = "", "000000.00", rsFine.Tables(0).Rows(0)("InstAmt"))
                sSql = "select * from  PAY_REIMURSH where paid<>'Y' and paycode='" & Trim(rsMonthTable.Tables(0).Rows(0)("Paycode")) & "'"
                rsFine = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsFine)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsFine)
                End If
                If rsFine.Tables(0).Rows.Count > 0 Then
                    'rsFine.MoveFirst()
                    txtEarningConv.Text = "000000.00"
                    txtEarningMed.Text = "000000.00"
                    txtEarningEarn1.Text = "000000.00"
                    txtEarningEarn2.Text = "000000.00"
                    txtEarningEarn3.Text = "000000.00"
                    txtEarningEarn4.Text = "000000.00"
                    txtEarningEarn5.Text = "000000.00"
                    txtEarningEarn6.Text = "000000.00"
                    txtEarningEarn7.Text = "000000.00"
                    txtEarningEarn8.Text = "000000.00"
                    txtEarningEarn9.Text = "000000.00"
                    txtEarningEarn10.Text = "000000.00"
                    For i As Integer = 0 To rsFine.Tables(0).Rows.Count - 1 ' Do While Not rsFine.EOF
                        txtEarningConv.Text = Format(Val(txtEarningConv.Text) + rsFine.Tables(0).Rows(i)("VMED_AMT"), "000000.00")
                        txtEarningMed.Text = Format(Val(txtEarningMed.Text) + rsFine.Tables(0).Rows(i)("VCONV_AMT"), "000000.00")
                        txtEarningEarn1.Text = Format(Val(txtEarningEarn1.Text) + rsFine.Tables(0).Rows(i)("VI_1_AMT"), "000000.00")
                        txtEarningEarn2.Text = Format(Val(txtEarningEarn2.Text) + rsFine.Tables(0).Rows(i)("VI_2_AMT"), "000000.00")
                        txtEarningEarn3.Text = Format(Val(txtEarningEarn3.Text) + rsFine.Tables(0).Rows(i)("VI_3_AMT"), "000000.00")
                        txtEarningEarn4.Text = Format(Val(txtEarningEarn4.Text) + rsFine.Tables(0).Rows(i)("VI_4_AMT"), "000000.00")
                        txtEarningEarn5.Text = Format(Val(txtEarningEarn5.Text) + rsFine.Tables(0).Rows(i)("VI_5_AMT"), "000000.00")
                        txtEarningEarn6.Text = Format(Val(txtEarningEarn6.Text) + rsFine.Tables(0).Rows(i)("VI_6_AMT"), "000000.00")
                        txtEarningEarn7.Text = Format(Val(txtEarningEarn7.Text) + rsFine.Tables(0).Rows(i)("VI_7_AMT"), "000000.00")
                        txtEarningEarn8.Text = Format(Val(txtEarningEarn8.Text) + rsFine.Tables(0).Rows(i)("VI_8_AMT"), "000000.00")
                        txtEarningEarn9.Text = Format(Val(txtEarningEarn9.Text) + rsFine.Tables(0).Rows(i)("VI_9_AMT"), "000000.00")
                        txtEarningEarn10.Text = Format(Val(txtEarningEarn10.Text) + rsFine.Tables(0).Rows(i)("VI_10_AMT"), "000000.00")
                        'rsFine.MoveNext()
                        'If rsFine.EOF Then
                        '    Exit Do
                        'End If
                    Next
                End If
                If prvMonth.Checked Then
                    sSql = "select * from pay_result WHERE PAYCODE ='" & LookUpEdit1.EditValue.ToString.Trim & "'  And DatePart(mm,Mon_Year)=" & Format(datePrv, "MM") & " And DatePart(yyyy,Mon_Year)=" & Format(datePrv, "yyyy")
                    rsMonthTable = New DataSet
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(rsMonthTable)
                    Else
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(rsMonthTable)
                    End If
                    If rsMonthTable.Tables(0).Rows.Count > 0 Then
                        Dim tmp = rsMonthTable.Tables(0).Rows(0)("vsalary") + rsMonthTable.Tables(0).Rows(0)("vda_amt") + rsMonthTable.Tables(0).Rows(0)("vhra_amt") + rsMonthTable.Tables(0).Rows(0)("vot_amt") + rsMonthTable.Tables(0).Rows(0)("vi_1_amt") + rsMonthTable.Tables(0).Rows(0)("vi_2_amt") + rsMonthTable.Tables(0).Rows(0)("vi_3_amt") + rsMonthTable.Tables(0).Rows(0)("vi_4_amt") + rsMonthTable.Tables(0).Rows(0)("vi_5_amt") + rsMonthTable.Tables(0).Rows(0)("vi_6_amt") + rsMonthTable.Tables(0).Rows(0)("vi_7_amt") + rsMonthTable.Tables(0).Rows(0)("vi_8_amt") + rsMonthTable.Tables(0).Rows(0)("vi_9_amt") + rsMonthTable.Tables(0).Rows(0)("vi_10_amt") + rsMonthTable.Tables(0).Rows(0)("vconv_amt") + rsMonthTable.Tables(0).Rows(0)("vmed_amt")
                        netSal = netSal + tmp
                        Dim tmp1 = (rsMonthTable.Tables(0).Rows(0)("vd_1_amt") + rsMonthTable.Tables(0).Rows(0)("vd_2_amt") + rsMonthTable.Tables(0).Rows(0)("vd_3_amt") + rsMonthTable.Tables(0).Rows(0)("vd_4_amt") + rsMonthTable.Tables(0).Rows(0)("vd_5_amt") + rsMonthTable.Tables(0).Rows(0)("vd_6_amt") + rsMonthTable.Tables(0).Rows(0)("vd_7_amt") + rsMonthTable.Tables(0).Rows(0)("vd_8_amt") + rsMonthTable.Tables(0).Rows(0)("vd_9_amt") + rsMonthTable.Tables(0).Rows(0)("vd_10_amt") + rsMonthTable.Tables(0).Rows(0)("VFINE") + rsMonthTable.Tables(0).Rows(0)("Vadvance") + rsMonthTable.Tables(0).Rows(0)("VESI_AMT") + rsMonthTable.Tables(0).Rows(0)("VPF_AMT") + rsMonthTable.Tables(0).Rows(0)("VTDS_AMT") + rsMonthTable.Tables(0).Rows(0)("PROF_TAX_AMT") + +rsMonthTable.Tables(0).Rows(0)("VVPF_AMT"))
                        netSal = netSal - tmp1
                    End If
                End If
                If netSal < 0 Then
                    txtNetPay.Text = "000000.00"
                Else
                    txtNetPay.Text = Format(netSal, "000000.00")
                End If
            End If
        End If
        Me.Cursor = Cursors.Default
        'Screen.MousePointer = vbNormal
    End Sub
    'Private Function DoProcess(sSql As String) As Boolean
    '    Dim PP As XtraPayrollProcess = New XtraPayrollProcess
    '    Dim adap As SqlDataAdapter
    '    Dim adapA As OleDbDataAdapter
    '    Dim sAdvFn As String
    '    Dim rsProcess As DataSet
    '    Dim rsProf As DataSet
    '    Dim sWhichFormula As String '* 355
    '    Dim sPaycode As String '* 10, 
    '    Dim decpos As Integer
    '    Dim iPFAMT As Double, iFPFAMT As Double, iOtAmt As Double, iHRAAMT As Double, iESIAMT As Double
    '    Dim iDedAmt1 As Double, iDedAmt2 As Double, iDedAmt3 As Double, iDedAmt4 As Double, iDedAmt5 As Double, iDedAmt6 As Double, iDedAmt7 As Double, iDedAmt8 As Double, iDedAmt9 As Double, iDedAmt10 As Double
    '    Dim iErnAmt1 As Double, iErnAmt2 As Double, iErnAmt3 As Double, iErnAmt4 As Double, iErnAmt5 As Double, iErnAmt6 As Double, iErnAmt7 As Double, iErnAmt8 As Double, iErnAmt9 As Double, iErnAmt10 As Double
    '    Dim iSalary As Double, iNetSal As Double, iGrossSal As Double
    '    Dim BASIC As Double, DA As Double, CONV As Double, MED As Double, PF As Double, FPF As Double, OTRate As Double
    '    Dim HRA As Double, ESI As Double, Fine As Double, Advance As Double
    '    Dim PRE As Double, ABS1 As Double, HLD As Double, LATE As Double
    '    Dim EARLY As Double, OT As Double, CL As Double, SL As Double, PL_EL As Double
    '    Dim OTHER_LV As Double, LEAVE As Double, TDAYS As Double, T_LATE As Double, T_EARLY As Double
    '    Dim D_1TOD_10 As Double, E_1TOE_10 As Double, OT_RATE As Double, MON_DAY As Double
    '    Dim ided1 As Double, ided2 As Double, ided3 As Double, ided4 As Double, ided5 As Double, ided6 As Double, ided7 As Double, ided8 As Double, ided9 As Double, ided10 As Double
    '    Dim iern1 As Double, iern2 As Double, iern3 As Double, iern4 As Double, iern5 As Double, iern6 As Double, iern7 As Double, iern8 As Double, iern9 As Double, iern10 As Double
    '    Dim iRESULT As Double
    '    Dim iWOValue As Integer
    '    Dim test As Double, mArrearSalary As Double
    '    Dim rsarrear As DataSet, rsarrearpayday As DataSet
    '    Dim marrear As Double, marresiamt As Double, marrpfamt As Double, marrearesi As Double, marrearpf As Double, marrearfpf As Double, marrearpfonamt As Double, marrearvpf As Double
    '    Dim uptodate As Date, arrearpayday As Double, arrearmonday As Double
    '    Dim amt1_amt As Double, amt2_amt As Double, amt3_amt As Double, amt4_amt As Double, amt5_amt As Double, n As Integer
    '    Dim conv_amt As Double, medical_amt As Double, tds_amt As Double, prof_tax_amt As Double, amt_on_pf As Double, amt_on_esi As Double, iVPFAMT As Double, iEPFAMT As Double, TDS As Double, PROF_TAX As Double

    '    Dim Reimbconv_amt As Double, Reimmedical_amt As Double, ReimErnAmt1 As Double, ReimErnAmt2 As Double, ReimErnAmt3 As Double, ReimErnAmt4 As Double, ReimErnAmt5 As Double, ReimErnAmt6 As Double, ReimErnAmt7 As Double, ReimErnAmt8 As Double, ReimErnAmt9 As Double, ReimErnAmt10 As Double
    '    'On Error GoTo ErrHand
    '    rsProcess = New DataSet 'Cn.Execute(sSql)
    '    If Common.servername = "Access" Then
    '        adapA = New OleDbDataAdapter(sSql, Common.con1)
    '        adapA.Fill(rsProcess)
    '    Else
    '        adap = New SqlDataAdapter(sSql, Common.con)
    '        adap.Fill(rsProcess)
    '    End If
    '    For i As Integer = 0 To rsProcess.Tables(0).Rows.Count - 1 ' Do While Not rsProcess.EOF
    '        Reimbconv_amt = 0 : Reimmedical_amt = 0 : ReimErnAmt1 = 0 : ReimErnAmt2 = 0 : ReimErnAmt3 = 0 : ReimErnAmt4 = 0 : ReimErnAmt5 = 0 : ReimErnAmt6 = 0 : ReimErnAmt7 = 0 : ReimErnAmt8 = 0 : ReimErnAmt9 = 0 : ReimErnAmt10 = 0
    '        iPFAMT = 0 : iFPFAMT = 0 : iOtAmt = 0 : iHRAAMT = 0 : Advance = 0 : Fine = 0 : iDedAmt1 = 0 : iDedAmt2 = 0 : iDedAmt3 = 0 : iDedAmt4 = 0 : iDedAmt5 = 0 : iDedAmt6 = 0 : iDedAmt7 = 0 : iDedAmt8 = 0 : iDedAmt9 = 0 : iDedAmt10 = 0
    '        iErnAmt1 = 0 : iErnAmt2 = 0 : iErnAmt3 = 0 : iErnAmt4 = 0 : iErnAmt5 = 0 : iErnAmt6 = 0 : iErnAmt7 = 0 : iErnAmt8 = 0 : iErnAmt9 = 0 : iErnAmt10 = 0 : iESIAMT = 0
    '        conv_amt = 0 : medical_amt = 0 : tds_amt = 0 : prof_tax_amt = 0 : amt_on_pf = 0 : amt_on_esi = 0 : iVPFAMT = 0 : iEPFAMT = 0
    '        sPaycode = rsProcess.Tables(0).Rows(i)("PAYCODE") : BASIC = rsProcess.Tables(0).Rows(i)("VBASIC")
    '        DA = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VDA_RATE")), 0, rsProcess.Tables(0).Rows(i)("VDA_RATE")) : HRA = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VHRA_RATE")), 0, rsProcess.Tables(0).Rows(i)("VHRA_RATE"))
    '        MED = IIf(IsNull(rsProcess.Tables(0).Rows(i)("Vmed_rate")), 0, rsProcess.Tables(0).Rows(i)("Vmed_rate")) : CONV = IIf(IsNull(rsProcess.Tables(0).Rows(i)("Vconv_rate")), 0, rsProcess.Tables(0).Rows(i)("Vconv_rate"))
    '        PRE = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VPRE")), 0, rsProcess.Tables(0).Rows(i)("VPRE"))
    '        ABS1 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VABS")), 0, rsProcess.Tables(0).Rows(i)("VABS")) : HLD = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VHLD")), 0, rsProcess.Tables(0).Rows(i)("VHLD"))
    '        LATE = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VLATE")), 0, rsProcess.Tables(0).Rows(i)("VLATE")) : EARLY = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VEARLY")), 0, rsProcess.Tables(0).Rows(i)("VEARLY"))
    '        'OT = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VOT")), 0, rsProcess.Tables(0).Rows(i)("VOT")) : OT = Hr2Min(OT) : CL = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VCL")), 0, rsProcess.Tables(0).Rows(i)("VCL"))
    '        OT = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VOT")), 0, rsProcess.Tables(0).Rows(i)("VOT")) : OT = OT * 60 : CL = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VCL")), 0, rsProcess.Tables(0).Rows(i)("VCL"))
    '        SL = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VSL")), 0, rsProcess.Tables(0).Rows(i)("VSL")) : PL_EL = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VPL_EL")), 0, rsProcess.Tables(0).Rows(i)("VPL_EL"))
    '        OTHER_LV = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VOTHER_LV")), 0, rsProcess.Tables(0).Rows(i)("VOTHER_LV")) : LEAVE = CL + SL + PL_EL + OTHER_LV
    '        iWOValue = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VWO")), 0, rsProcess.Tables(0).Rows(i)("VWO"))
    '        TDAYS = PRE + HLD + LEAVE + iWOValue : T_LATE = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VLATE")), 0, rsProcess.Tables(0).Rows(i)("VLATE"))
    '        T_EARLY = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VEARLY")), 0, rsProcess.Tables(0).Rows(i)("VEARLY"))
    '        TDS = IIf(IsNull(rsProcess.Tables(0).Rows(i)("Vtds_rate")), 0, rsProcess.Tables(0).Rows(i)("Vtds_rate"))
    '        ided1 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VD_1")), 0, rsProcess.Tables(0).Rows(i)("VD_1"))
    '        ided2 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VD_2")), 0, rsProcess.Tables(0).Rows(i)("VD_2"))
    '        ided3 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VD_3")), 0, rsProcess.Tables(0).Rows(i)("VD_3"))
    '        ided4 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VD_4")), 0, rsProcess.Tables(0).Rows(i)("VD_4"))
    '        ided5 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VD_5")), 0, rsProcess.Tables(0).Rows(i)("VD_5"))
    '        ided6 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VD_6")), 0, rsProcess.Tables(0).Rows(i)("VD_6"))
    '        ided7 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VD_7")), 0, rsProcess.Tables(0).Rows(i)("VD_7"))
    '        ided8 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VD_8")), 0, rsProcess.Tables(0).Rows(i)("VD_8"))
    '        ided9 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VD_9")), 0, rsProcess.Tables(0).Rows(i)("VD_9"))
    '        ided10 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VD_10")), 0, rsProcess.Tables(0).Rows(i)("VD_10"))

    '        iern1 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VI_1")), 0, rsProcess.Tables(0).Rows(i)("VI_1"))
    '        iern2 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VI_2")), 0, rsProcess.Tables(0).Rows(i)("VI_2"))
    '        iern3 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VI_3")), 0, rsProcess.Tables(0).Rows(i)("VI_3"))
    '        iern4 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VI_4")), 0, rsProcess.Tables(0).Rows(i)("VI_4"))
    '        iern5 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VI_5")), 0, rsProcess.Tables(0).Rows(i)("VI_5"))
    '        iern6 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VI_6")), 0, rsProcess.Tables(0).Rows(i)("VI_6"))
    '        iern7 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VI_7")), 0, rsProcess.Tables(0).Rows(i)("VI_7"))
    '        iern8 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VI_8")), 0, rsProcess.Tables(0).Rows(i)("VI_8"))
    '        iern9 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VI_9")), 0, rsProcess.Tables(0).Rows(i)("VI_9"))
    '        iern10 = IIf(IsNull(rsProcess.Tables(0).Rows(i)("VI_10")), 0, rsProcess.Tables(0).Rows(i)("VI_10"))

    '        OT_RATE = rsProcess.Tables(0).Rows(i)("VOT_RATE")
    '        MON_DAY = Date.DaysInMonth(txtapplicablefrom.DateTime.Year, txtapplicablefrom.DateTime.Month) ' NoOfDay(txtapplicablefrom.Value)
    '        '******-FOR BASIC SALARY-*************
    '        iSalary = (BASIC / MON_DAY) * TDAYS
    '        If rsPaysetup.Tables(0).Rows(0)("BASIC_RND") = "Y" Then
    '            iSalary = Math.Round(iSalary, 0)
    '        Else
    '            iSalary = Math.Round(iSalary, 2)
    '        End If
    '        '******-FOR DA*************
    '        If rsProcess.Tables(0).Rows(i)("Vda_f") = "F" Then
    '            DA = rsProcess.Tables(0).Rows(i)("vda_RATE")
    '        Else
    '            DA = (rsProcess.Tables(0).Rows(i)("vda_RATE") / MON_DAY) * TDAYS
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("DA_RND") = "Y" Then
    '            DA = Math.Round(DA, 0)
    '        Else
    '            DA = Math.Round(DA, 2)
    '        End If
    '        '******-FOR HRA -*************
    '        If rsProcess.Tables(0).Rows(i)("VHRA_F") = "F" Then
    '            iHRAAMT = rsProcess.Tables(0).Rows(i)("VHRA_RATE")
    '        Else
    '            iHRAAMT = (rsProcess.Tables(0).Rows(i)("vHRA_RATE") / MON_DAY) * TDAYS
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("HRA_RND") = "Y" Then
    '            iHRAAMT = Math.Round(iHRAAMT, 0)
    '        Else
    '            iHRAAMT = Math.Round(iHRAAMT, 2)
    '        End If
    '        '******-FOR CONVEYANCE*************
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 1, 1) = "0" Then
    '            If rsProcess.Tables(0).Rows(i)("Vconv_f") = "F" Then
    '                conv_amt = rsProcess.Tables(0).Rows(i)("Vconv_rate")
    '            Else
    '                conv_amt = (rsProcess.Tables(0).Rows(i)("Vconv_rate") / MON_DAY) * TDAYS
    '            End If
    '        Else
    '            conv_amt = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("CONV_RND") = "Y" Then
    '            conv_amt = Math.Round(conv_amt, 0)
    '        Else
    '            conv_amt = Math.Round(conv_amt, 2)
    '        End If
    '        '******-FOR MEDICAL*************
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 2, 1) = "0" Then
    '            If rsProcess.Tables(0).Rows(i)("Vmed_f") = "F" Then
    '                medical_amt = rsProcess.Tables(0).Rows(i)("Vmed_rate")
    '            Else
    '                medical_amt = (rsProcess.Tables(0).Rows(i)("Vmed_rate") / MON_DAY) * TDAYS
    '            End If
    '        Else
    '            medical_amt = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("MEDICAL_RND") = "Y" Then
    '            medical_amt = Math.Round(medical_amt, 0)
    '        Else
    '            medical_amt = Math.Round(medical_amt, 2)
    '        End If
    '        '******-FOR Earning   Formula -**********
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 3, 1) = "0" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_1")) And rsProcess.Tables(0).Rows(i)("VIT_1").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_1").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_1"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    iErnAmt1 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                iErnAmt1 = iern1
    '            End If
    '        Else
    '            iErnAmt1 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN1_RND") = "Y" Then
    '            iErnAmt1 = Math.Round(iErnAmt1, 0)
    '        Else
    '            iErnAmt1 = Math.Round(iErnAmt1, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 4, 1) = "0" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_2")) And rsProcess.Tables(0).Rows(i)("VIT_2").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_2").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_2"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    iErnAmt2 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                iErnAmt2 = iern2
    '            End If
    '        Else
    '            iErnAmt2 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN2_RND") = "Y" Then
    '            iErnAmt2 = Math.Round(iErnAmt2, 0)
    '        Else
    '            iErnAmt2 = Math.Round(iErnAmt2, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 5, 1) = "0" Then
    '            If (Not IsNull(Trim(rsProcess.Tables(0).Rows(i)("VIT_3"))) And rsProcess.Tables(0).Rows(i)("VIT_3").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_3").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_3"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    iErnAmt3 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                iErnAmt3 = iern3
    '            End If
    '        Else
    '            iErnAmt3 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN3_RND") = "Y" Then
    '            iErnAmt3 = Math.Round(iErnAmt3, 0)
    '        Else
    '            iErnAmt3 = Math.Round(iErnAmt3, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 6, 1) = "0" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_4").ToString) And rsProcess.Tables(0).Rows(i)("VIT_4").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_4").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_4"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    iErnAmt4 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                iErnAmt4 = iern4
    '            End If
    '        Else
    '            iErnAmt4 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN4_RND") = "Y" Then
    '            iErnAmt4 = Math.Round(iErnAmt4, 0)
    '        Else
    '            iErnAmt4 = Math.Round(iErnAmt4, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 7, 1) = "0" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_5").ToString) And rsProcess.Tables(0).Rows(i)("VIT_5").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_5").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_5"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    iErnAmt5 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                iErnAmt5 = iern5
    '            End If
    '        Else
    '            iErnAmt5 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN5_RND") = "Y" Then
    '            iErnAmt5 = Math.Round(iErnAmt5, 0)
    '        Else
    '            iErnAmt5 = Math.Round(iErnAmt5, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 8, 1) = "0" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_6").ToString) And rsProcess.Tables(0).Rows(i)("VIT_6").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_6").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_6"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    iErnAmt6 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                iErnAmt6 = iern6
    '            End If
    '        Else
    '            iErnAmt6 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN6_RND") = "Y" Then
    '            iErnAmt6 = Math.Round(iErnAmt6, 0)
    '        Else
    '            iErnAmt6 = Math.Round(iErnAmt6, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 9, 1) = "0" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_7").ToString) And rsProcess.Tables(0).Rows(i)("VIT_7").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_7").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_7"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    iErnAmt7 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                iErnAmt7 = iern7
    '            End If
    '        Else
    '            iErnAmt7 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN7_RND") = "Y" Then
    '            iErnAmt7 = Math.Round(iErnAmt7, 0)
    '        Else
    '            iErnAmt7 = Math.Round(iErnAmt7, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 10, 1) = "0" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_8").ToString) And rsProcess.Tables(0).Rows(i)("VIT_8").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_8").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_8"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    iErnAmt8 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                iErnAmt8 = iern8
    '            End If
    '        Else
    '            iErnAmt8 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN8_RND") = "Y" Then
    '            iErnAmt8 = Math.Round(iErnAmt8, 0)
    '        Else
    '            iErnAmt8 = Math.Round(iErnAmt8, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 11, 1) = "0" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_9").ToString) And rsProcess.Tables(0).Rows(i)("VIT_9").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_9").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_9"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    iErnAmt9 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                iErnAmt9 = iern9
    '            End If
    '        Else
    '            iErnAmt9 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN9_RND") = "Y" Then
    '            iErnAmt9 = Math.Round(iErnAmt9, 0)
    '        Else
    '            iErnAmt9 = Math.Round(iErnAmt9, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 12, 1) = "0" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_10").ToString) And rsProcess.Tables(0).Rows(i)("VIT_10").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_10").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_10"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    iErnAmt10 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                iErnAmt10 = iern10
    '            End If
    '        Else
    '            iErnAmt10 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN10_RND") = "Y" Then
    '            iErnAmt10 = Math.Round(iErnAmt10, 0)
    '        Else
    '            iErnAmt10 = Math.Round(iErnAmt10, 2)
    '        End If
    '        '******-FOR OT  AMT FORMULA-*************
    '        iOtAmt = 0
    '        If Not IsNull(rsProcess.Tables(0).Rows(i)("VOT_FLAG")) And rsProcess.Tables(0).Rows(i)("VOT_FLAG").ToString.Trim <> "" Then
    '            OT = OT
    '            sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VOT_FLAG"))
    '            If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                iOtAmt = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '            End If
    '        Else
    '            iOtAmt = OT_RATE
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("OTAMT_RND") = "Y" Then
    '            iOtAmt = Math.Round(iOtAmt, 0)
    '        Else
    '            iOtAmt = Math.Round(iOtAmt, 2)
    '        End If
    '        '******-FOR TDS*************
    '        If Not IsNull(rsProcess.Tables(0).Rows(i)("VTDS_F")) And rsProcess.Tables(0).Rows(i)("VTDS_F").ToString.Trim <> "" Then
    '            sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VTDS_F"))
    '            If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                tds_amt = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '            End If
    '        Else
    '            tds_amt = rsProcess.Tables(0).Rows(i)("VTDS_RATE")
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("TDS_RND") = "Y" Then
    '            tds_amt = Math.Round(tds_amt, 0)
    '        Else
    '            tds_amt = Math.Round(tds_amt, 2)
    '        End If
    '        '******-FOR Deduction Formula -**********
    '        D_1TOD_10 = 0
    '        If (Not IsNull(rsProcess.Tables(0).Rows(i)("VDT_1").ToString) And rsProcess.Tables(0).Rows(i)("VDT_1").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VDT_1").ToString.Trim <> "0") Then
    '            sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_1"))
    '            If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                iDedAmt1 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '            End If
    '        Else
    '            iDedAmt1 = ided1
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("DED1_RND") = "Y" Then
    '            iDedAmt1 = Math.Round(iDedAmt1, 0)
    '        Else
    '            iDedAmt1 = Math.Round(iDedAmt1, 2)
    '        End If
    '        If (Not IsNull(rsProcess.Tables(0).Rows(i)("VDT_2").ToString) And rsProcess.Tables(0).Rows(i)("VDT_2").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VDT_2").ToString.Trim <> "0") Then
    '            sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_2"))
    '            If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                iDedAmt2 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '            End If
    '        Else
    '            iDedAmt2 = ided2
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("DED2_RND") = "Y" Then
    '            iDedAmt2 = Math.Round(iDedAmt2, 0)
    '        Else
    '            iDedAmt2 = Math.Round(iDedAmt2, 2)
    '        End If
    '        If (Not IsNull(rsProcess.Tables(0).Rows(i)("VDT_3").ToString) And rsProcess.Tables(0).Rows(i)("VDT_3").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VDT_3").ToString.Trim <> "0") Then
    '            sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_3"))
    '            If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                iDedAmt3 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '            End If
    '        Else
    '            iDedAmt3 = ided3
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("DED3_RND") = "Y" Then
    '            iDedAmt3 = Math.Round(iDedAmt3, 0)
    '        Else
    '            iDedAmt3 = Math.Round(iDedAmt3, 2)
    '        End If
    '        If (Not IsNull(rsProcess.Tables(0).Rows(i)("VDT_4")) And rsProcess.Tables(0).Rows(i)("VDT_4").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VDT_4").ToString.Trim <> "0") Then
    '            sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_4"))
    '            If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                iDedAmt4 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '            End If
    '        Else
    '            iDedAmt4 = ided4
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("DED4_RND") = "Y" Then
    '            iDedAmt4 = Math.Round(iDedAmt4, 0)
    '        Else
    '            iDedAmt4 = Math.Round(iDedAmt4, 2)
    '        End If
    '        If (Not IsNull(rsProcess.Tables(0).Rows(i)("VDT_5")) And rsProcess.Tables(0).Rows(i)("VDT_5").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VDT_5").ToString.Trim <> "0") Then
    '            sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_5"))
    '            If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                iDedAmt5 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '            End If
    '        Else
    '            iDedAmt5 = ided5
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("DED5_RND") = "Y" Then
    '            iDedAmt5 = Math.Round(iDedAmt5, 0)
    '        Else
    '            iDedAmt5 = Math.Round(iDedAmt5, 2)
    '        End If
    '        If (Not IsNull(rsProcess.Tables(0).Rows(i)("VDT_6")) And rsProcess.Tables(0).Rows(i)("VDT_6").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VDT_6").ToString.Trim <> "0") Then
    '            sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_6"))
    '            If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                iDedAmt6 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '            End If
    '        Else
    '            iDedAmt6 = ided6
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("DED6_RND") = "Y" Then
    '            iDedAmt6 = Math.Round(iDedAmt6, 0)
    '        Else
    '            iDedAmt6 = Math.Round(iDedAmt6, 2)
    '        End If
    '        If (Not IsNull(rsProcess.Tables(0).Rows(i)("VDT_7")) And rsProcess.Tables(0).Rows(i)("VDT_7").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VDT_7").ToString.Trim <> "0") Then
    '            sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_7"))
    '            If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                iDedAmt7 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '            End If
    '        Else
    '            iDedAmt7 = ided7
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("DED7_RND") = "Y" Then
    '            iDedAmt7 = Math.Round(iDedAmt7, 0)
    '        Else
    '            iDedAmt7 = Math.Round(iDedAmt7, 2)
    '        End If
    '        If (Not IsNull(rsProcess.Tables(0).Rows(i)("VDT_8")) And rsProcess.Tables(0).Rows(i)("VDT_8").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VDT_8").ToString.Trim <> "0") Then
    '            sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_8"))
    '            If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                iDedAmt8 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '            End If
    '        Else
    '            iDedAmt8 = ided8
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("DED8_RND") = "Y" Then
    '            iDedAmt8 = Math.Round(iDedAmt8, 0)
    '        Else
    '            iDedAmt8 = Math.Round(iDedAmt8, 2)
    '        End If
    '        If (Not IsNull(rsProcess.Tables(0).Rows(i)("VDT_9")) And rsProcess.Tables(0).Rows(i)("VDT_9").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VDT_9").ToString.Trim <> "0") Then
    '            sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_9"))
    '            If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                iDedAmt9 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '            End If
    '        Else
    '            iDedAmt9 = ided9
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("DED9_RND") = "Y" Then
    '            iDedAmt9 = Math.Round(iDedAmt9, 0)
    '        Else
    '            iDedAmt9 = Math.Round(iDedAmt9, 2)
    '        End If
    '        If (Not IsNull(rsProcess.Tables(0).Rows(i)("VDT_10")) And rsProcess.Tables(0).Rows(i)("VDT_10").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VDT_10").ToString.Trim <> "0") Then
    '            sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_10"))
    '            If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                iDedAmt10 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '            End If
    '        Else
    '            iDedAmt10 = ided10
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("DED10_RND") = "Y" Then
    '            iDedAmt10 = Math.Round(iDedAmt10, 0)
    '        Else
    '            iDedAmt10 = Math.Round(iDedAmt10, 2)
    '        End If
    '        '******-FOR PF AMT FORMULA-**************
    '        If rsProcess.Tables(0).Rows(i)("PF_ALLOWED") = "Y" Then
    '            For n = 1 To Len(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")))
    '                If n = 1 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
    '                    amt_on_pf = amt_on_pf + iSalary
    '                End If
    '                If n = 2 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
    '                    amt_on_pf = amt_on_pf + iHRAAMT
    '                End If
    '                If n = 3 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
    '                    amt_on_pf = amt_on_pf + DA
    '                End If
    '                If n = 4 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
    '                    amt_on_pf = amt_on_pf + conv_amt
    '                End If
    '                If n = 5 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
    '                    amt_on_pf = amt_on_pf + medical_amt
    '                End If
    '                If n = 6 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
    '                    amt_on_pf = amt_on_pf + iErnAmt1
    '                End If
    '                If n = 7 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
    '                    amt_on_pf = amt_on_pf + iErnAmt2
    '                End If
    '                If n = 8 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
    '                    amt_on_pf = amt_on_pf + iErnAmt3
    '                End If
    '                If n = 9 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
    '                    amt_on_pf = amt_on_pf + iErnAmt4
    '                End If
    '                If n = 10 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
    '                    amt_on_pf = amt_on_pf + iErnAmt5
    '                End If
    '                If n = 11 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
    '                    amt_on_pf = amt_on_pf + iErnAmt6
    '                End If
    '                If n = 12 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
    '                    amt_on_pf = amt_on_pf + iErnAmt7
    '                End If
    '                If n = 13 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
    '                    amt_on_pf = amt_on_pf + iErnAmt8
    '                End If
    '                If n = 14 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
    '                    amt_on_pf = amt_on_pf + iErnAmt9
    '                End If
    '                If n = 15 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
    '                    amt_on_pf = amt_on_pf + iErnAmt10
    '                End If
    '            Next n
    '            'iPFAMT = Math.Round((IIf(rsPaysetup.Tables(0).Rows(0)("PFFIXED") = "Y", IIf(amt_on_pf > rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), amt_on_pf), amt_on_pf) * rsPaysetup.Tables(0).Rows(0)("PF")) / 100, rsPaysetup.Tables(0).Rows(0)("PFRND"))
    '            iPFAMT = Math.Round((IIf(amt_on_pf > rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), amt_on_pf) * rsPaysetup.Tables(0).Rows(0)("PF")) / 100)
    '            'iEPFAMT = Math.Round((IIf(amt_on_pf > rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), amt_on_pf) * rsPaysetup.Tables(0).Rows(0)("EPF")) / 100, rsPaysetup.Tables(0).Rows(0)("PFRND"))
    '            iEPFAMT = Math.Round((IIf(amt_on_pf > rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), amt_on_pf) * rsPaysetup.Tables(0).Rows(0)("EPF")) / 100)
    '            iFPFAMT = iPFAMT - iEPFAMT
    '            If rsProcess.Tables(0).Rows(i)("VPF_ALLOWED") = "Y" Then
    '                'iVPFAMT = Math.Round((amt_on_pf * rsPaysetup.Tables(0).Rows(0)("VPF")) / 100, rsPaysetup.Tables(0).Rows(0)("PFRND"))
    '                iVPFAMT = Math.Round((amt_on_pf * rsPaysetup.Tables(0).Rows(0)("VPF")) / 100)
    '                iFPFAMT = iFPFAMT + iVPFAMT
    '            End If
    '            amt_on_pf = IIf(amt_on_pf > rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), amt_on_pf)
    '        End If
    '        '******-FOR ESI AMT FORMULA-*************
    '        If rsProcess.Tables(0).Rows(i)("ESI_ALLOWED") = "Y" Then
    '            For n = 1 To Len(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")))
    '                If n = 1 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
    '                    amt_on_esi = amt_on_esi + iSalary
    '                End If
    '                If n = 2 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
    '                    amt_on_esi = amt_on_esi + iHRAAMT
    '                End If
    '                If n = 3 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
    '                    amt_on_esi = amt_on_esi + DA
    '                End If
    '                If n = 4 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
    '                    amt_on_esi = amt_on_esi + conv_amt
    '                End If
    '                If n = 5 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
    '                    amt_on_esi = amt_on_esi + medical_amt
    '                End If
    '                If n = 6 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
    '                    amt_on_esi = amt_on_esi + iErnAmt1
    '                End If
    '                If n = 7 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
    '                    amt_on_esi = amt_on_esi + iErnAmt2
    '                End If
    '                If n = 8 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
    '                    amt_on_esi = amt_on_esi + iErnAmt3
    '                End If
    '                If n = 9 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
    '                    amt_on_esi = amt_on_esi + iErnAmt4
    '                End If
    '                If n = 10 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
    '                    amt_on_esi = amt_on_esi + iErnAmt5
    '                End If
    '                If n = 11 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
    '                    amt_on_esi = amt_on_esi + iErnAmt6
    '                End If
    '                If n = 12 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
    '                    amt_on_esi = amt_on_esi + iErnAmt7
    '                End If
    '                If n = 13 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
    '                    amt_on_esi = amt_on_esi + iErnAmt8
    '                End If
    '                If n = 14 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
    '                    amt_on_esi = amt_on_esi + iErnAmt9
    '                End If
    '                If n = 15 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
    '                    amt_on_esi = amt_on_esi + iErnAmt10
    '                End If
    '            Next n
    '            If (Convert.ToDateTime(rsProcess.Tables(0).Rows(i)("MON_YEAR")).ToString("MM") = "04" Or Convert.ToDateTime(rsProcess.Tables(0).Rows(i)("MON_YEAR")).ToString("MM") = "10") And amt_on_esi > rsPaysetup.Tables(0).Rows(0)("ESILIMIT") Then
    '                amt_on_esi = 0
    '                iESIAMT = 0
    '            Else
    '                iESIAMT = IIf(amt_on_esi > rsPaysetup.Tables(0).Rows(0)("ESILIMIT"), 0, (amt_on_esi * rsPaysetup.Tables(0).Rows(0)("ESIE")) / 100)
    '                iESIAMT = Math.Round(iESIAMT, 0) + IIf(iESIAMT - Math.Round(iESIAMT, 0) > 0, 1, 0)
    '                'iESIAMT = IIf(amt_on_esi > rsPaysetup.Tables(0).Rows(0)("ESILIMIT"), (rsPaysetup.Tables(0).Rows(0)("ESILIMIT") * rsPaysetup.Tables(0).Rows(0)("ESIE")) / 100, (amt_on_esi * rsPaysetup.Tables(0).Rows(0)("ESIE")) / 100) + ((iOtAmt * rsPaysetup.Tables(0).Rows(0)("ESIE")) / 100)
    '                'iESIAMT = math.Round(iESIAMT, rsPaysetup.Tables(0).Rows(0)("ESIRND")) - math.Round((iOtAmt * rsPaysetup.Tables(0).Rows(0)("ESIE")) / 100, rsPaysetup.Tables(0).Rows(0)("ESIRND"))
    '            End If
    '        End If
    '        '**Advance**
    '        sAdvFn = "Select Sum(Inst_Amt) as InstAmt From TBLADVANCEDATA " & _
    '                "Where Paycode='" & Trim(sPaycode) & _
    '                "' And DatePart(mm,Mon_Year) = " & txtapplicablefrom.DateTime.ToString("MM") & _
    '                " And DatePart(yyyy,Mon_Year) = " & txtapplicablefrom.DateTime.ToString("yyyy") & _
    '                " And A_L='" & "A'"

    '        Dim rsAdvance As DataSet = New DataSet 'Cn.Execute(sSql)
    '        If Common.servername = "Access" Then
    '            adapA = New OleDbDataAdapter(sAdvFn, Common.con1)
    '            adapA.Fill(rsAdvance)
    '        Else
    '            adap = New SqlDataAdapter(sAdvFn, Common.con)
    '            adap.Fill(rsAdvance)
    '        End If
    '        Advance = IIf(IsNull(rsAdvance.Tables(0).Rows(0)("InstAmt").ToString), 0, rsAdvance.Tables(0).Rows(0)("InstAmt"))
    '        '**Fine**
    '        sAdvFn = "Select Sum(Inst_Amt) AS InstAmt From TBLADVANCEDATA " & _
    '             "Where Paycode='" & sPaycode & _
    '             "' And DatePart(mm,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("MM") & _
    '             " And DatePart(yyyy,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("yyyy") & " And A_L='" & "L'"

    '        Dim rsFine As DataSet = New DataSet 'Cn.Execute(sSql)
    '        If Common.servername = "Access" Then
    '            adapA = New OleDbDataAdapter(sAdvFn, Common.con1)
    '            adapA.Fill(rsFine)
    '        Else
    '            adap = New SqlDataAdapter(sAdvFn, Common.con)
    '            adap.Fill(rsFine)
    '        End If

    '        Fine = IIf(IsNull(rsFine.Tables(0).Rows(0)("InstAmt")), 0, rsFine.Tables(0).Rows(0)("InstAmt"))
    '        '****************************************
    '        D_1TOD_10 = iDedAmt1 + iDedAmt2 + iDedAmt3 + iDedAmt4 + iDedAmt5 + iDedAmt6 + iDedAmt7 + iDedAmt8 + iDedAmt9 + iDedAmt10
    '        E_1TOE_10 = iErnAmt1 + iErnAmt2 + iErnAmt3 + iErnAmt4 + iErnAmt5 + iErnAmt6 + iErnAmt7 + iErnAmt8 + iErnAmt9 + iErnAmt10
    '        iGrossSal = iSalary + DA + iHRAAMT + iOtAmt + E_1TOE_10 + conv_amt + medical_amt
    '        '******-FOR PROFESSIONAL TAX*************
    '        If rsProcess.Tables(0).Rows(i)("PROF_TAX_ALLOWED") = "Y" Then
    '            sSql = "select * from professionaltax where lowerlimit<=" & iGrossSal & " and upperlimit>=" & iGrossSal
    '            rsProf = New DataSet 'Cn.Execute(sSql)
    '            If Common.servername = "Access" Then
    '                adapA = New OleDbDataAdapter(sSql, Common.con1)
    '                adapA.Fill(rsProf)
    '            Else
    '                adap = New SqlDataAdapter(sSql, Common.con)
    '                adap.Fill(rsProf)
    '            End If
    '            If rsProf.Tables(0).Rows.Count > 0 Then
    '                prof_tax_amt = rsProf.Tables(0).Rows(0)("taxamount")
    '            End If
    '            If rsPaysetup.Tables(0).Rows(0)("PROF_TAX_RND") = "Y" Then
    '                prof_tax_amt = Math.Round(prof_tax_amt, 0)
    '            Else
    '                prof_tax_amt = Math.Round(prof_tax_amt, 2)
    '            End If
    '        End If
    '        iNetSal = iGrossSal - (D_1TOD_10 + Fine + Advance + iESIAMT + iPFAMT + iFPFAMT + tds_amt + prof_tax_amt)
    '        netSal = iNetSal
    '        sSql = "Update pay_result Set AMTONPF=" & amt_on_pf & ",VPF_AMT=" & iPFAMT & ",VEPF_AMT=" & iEPFAMT & ",VFPF_AMT=" & iFPFAMT & ",VVPF_AMT=" & iVPFAMT & _
    '             ",VTDS_AMT=" & tds_amt & ",PROF_TAX_AMT=" & prof_tax_amt & ",VHRA_AMT=" & iHRAAMT & ",VCONV_AMT=" & conv_amt & ",VMED_AMT=" & medical_amt & ",AMTONESI=" & amt_on_esi & ",ESIONOT=" & IIf(iESIAMT > 0, Math.Round((iOtAmt * rsPaysetup.Tables(0).Rows(0)("ESIE")) / 100, 0) + IIf((iOtAmt * rsPaysetup.Tables(0).Rows(0)("ESIE")) / 100 - Math.Round((iOtAmt * rsPaysetup.Tables(0).Rows(0)("ESIE")) / 100, 0) > 0, 1, 0), 0) & ",VESI_AMT=" & iESIAMT & ",vot_amt=" & iOtAmt & _
    '             ",VFINE=" & Fine & ",VADVANCE=" & Advance & ",VDA_AMT=" & DA & _
    '             ",VD_1_AMT=" & iDedAmt1 & ",VD_2_AMT=" & iDedAmt2 & _
    '             ",VD_3_AMT=" & iDedAmt3 & ",VD_4_AMT=" & iDedAmt4 & _
    '             ",VD_5_AMT=" & iDedAmt5 & ",VD_6_AMT=" & iDedAmt6 & _
    '             ",VD_7_AMT=" & iDedAmt7 & ",VD_8_AMT=" & iDedAmt8 & _
    '             ",VD_9_AMT=" & iDedAmt9 & ",VD_10_AMT=" & iDedAmt10 & _
    '             ",VI_1_AMT=" & iErnAmt1 & ",VI_2_AMT=" & iErnAmt2 & _
    '             ",VI_3_AMT=" & iErnAmt3 & ",VI_4_AMT=" & iErnAmt4 & _
    '             ",VI_5_AMT=" & iErnAmt5 & ",VI_6_AMT=" & iErnAmt6 & _
    '             ",VI_7_AMT=" & iErnAmt7 & ",VI_8_AMT=" & iErnAmt8 & _
    '             ",VI_9_AMT=" & iErnAmt9 & ",VI_10_AMT=" & iErnAmt10 & _
    '             ",VTOTAL_DAY=" & MON_DAY & ",VLEAVE=" & LEAVE & _
    '             ",VSALARY=" & iSalary & ",VTDAYS=" & TDAYS & " Where Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("yyyy")
    '        'Cn.Execute(sSql)
    '        If Common.servername = "Access" Then
    '            If Common.con1.State <> ConnectionState.Open Then
    '                Common.con1.Open()
    '            End If
    '            cmd1 = New OleDbCommand(sSql, Common.con1)
    '            cmd1.ExecuteNonQuery()
    '            If Common.con1.State <> ConnectionState.Closed Then
    '                Common.con1.Close()
    '            End If
    '        Else
    '            If Common.con.State <> ConnectionState.Open Then
    '                Common.con.Open()
    '            End If
    '            cmd = New SqlCommand(sSql, Common.con)
    '            cmd.ExecuteNonQuery()
    '            If Common.con.State <> ConnectionState.Closed Then
    '                Common.con.Close()
    '            End If
    '        End If
    '        ''            REIMBURSHMENT CALCULATION
    '        '******-FOR CONVEYANCE*************
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 1, 1) = "1" Then
    '            If rsProcess.Tables(0).Rows(i)("Vconv_f") = "F" Then
    '                Reimbconv_amt = rsProcess.Tables(0).Rows(i)("Vconv_rate")
    '            Else
    '                Reimbconv_amt = (rsProcess.Tables(0).Rows(i)("Vconv_rate") / MON_DAY) * TDAYS
    '            End If
    '        Else
    '            Reimbconv_amt = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("CONV_RND") = "Y" Then
    '            Reimbconv_amt = Math.Round(Reimbconv_amt, 0)
    '        Else
    '            Reimbconv_amt = Math.Round(Reimbconv_amt, 2)
    '        End If
    '        '******-FOR MEDICAL*************
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 2, 1) = "1" Then
    '            If rsProcess.Tables(0).Rows(i)("Vmed_f") = "F" Then
    '                Reimmedical_amt = rsProcess.Tables(0).Rows(i)("Vmed_rate")
    '            Else
    '                Reimmedical_amt = (rsProcess.Tables(0).Rows(i)("Vmed_rate") / MON_DAY) * TDAYS
    '            End If
    '        Else
    '            Reimmedical_amt = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("MEDICAL_RND") = "Y" Then
    '            Reimmedical_amt = Math.Round(Reimmedical_amt, 0)
    '        Else
    '            Reimmedical_amt = Math.Round(Reimmedical_amt, 2)
    '        End If
    '        '******-FOR Earning   Formula -**********
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 3, 1) = "1" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_1")) And rsProcess.Tables(0).Rows(i)("VIT_1").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_1").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_1"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    ReimErnAmt1 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                ReimErnAmt1 = iern1
    '            End If
    '        Else
    '            ReimErnAmt1 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN1_RND") = "Y" Then
    '            ReimErnAmt1 = Math.Round(ReimErnAmt1, 0)
    '        Else
    '            ReimErnAmt1 = Math.Round(ReimErnAmt1, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 4, 1) = "1" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_2")) And rsProcess.Tables(0).Rows(i)("VIT_2").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_2").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_2"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    ReimErnAmt2 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                ReimErnAmt2 = iern2
    '            End If
    '        Else
    '            ReimErnAmt2 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN2_RND") = "Y" Then
    '            ReimErnAmt2 = Math.Round(ReimErnAmt2, 0)
    '        Else
    '            ReimErnAmt2 = Math.Round(ReimErnAmt2, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 5, 1) = "1" Then
    '            If (Not IsNull(Trim(rsProcess.Tables(0).Rows(i)("VIT_3"))) And rsProcess.Tables(0).Rows(i)("VIT_3").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_3").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_3"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    ReimErnAmt3 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                ReimErnAmt3 = iern3
    '            End If
    '        Else
    '            ReimErnAmt3 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN3_RND") = "Y" Then
    '            ReimErnAmt3 = Math.Round(ReimErnAmt3, 0)
    '        Else
    '            ReimErnAmt3 = Math.Round(ReimErnAmt3, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 6, 1) = "1" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_4")) And rsProcess.Tables(0).Rows(i)("VIT_4").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_4").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_4"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    ReimErnAmt4 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                ReimErnAmt4 = iern4
    '            End If
    '        Else
    '            ReimErnAmt4 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN4_RND") = "Y" Then
    '            ReimErnAmt4 = Math.Round(ReimErnAmt4, 0)
    '        Else
    '            ReimErnAmt4 = Math.Round(ReimErnAmt4, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 7, 1) = "1" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_5")) And rsProcess.Tables(0).Rows(i)("VIT_5").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_5").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_5"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    ReimErnAmt5 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                ReimErnAmt5 = iern5
    '            End If
    '        Else
    '            ReimErnAmt5 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN5_RND") = "Y" Then
    '            ReimErnAmt5 = Math.Round(ReimErnAmt5, 0)
    '        Else
    '            ReimErnAmt5 = Math.Round(ReimErnAmt5, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 8, 1) = "1" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_6")) And rsProcess.Tables(0).Rows(i)("VIT_6").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_6").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_6"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    ReimErnAmt6 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                ReimErnAmt6 = iern6
    '            End If
    '        Else
    '            ReimErnAmt6 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN6_RND") = "Y" Then
    '            ReimErnAmt6 = Math.Round(ReimErnAmt6, 0)
    '        Else
    '            ReimErnAmt6 = Math.Round(ReimErnAmt6, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 9, 1) = "1" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_7")) And rsProcess.Tables(0).Rows(i)("VIT_7").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_7").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_7"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    ReimErnAmt7 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                ReimErnAmt7 = iern7
    '            End If
    '        Else
    '            ReimErnAmt7 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN7_RND") = "Y" Then
    '            ReimErnAmt7 = Math.Round(ReimErnAmt7, 0)
    '        Else
    '            ReimErnAmt7 = Math.Round(ReimErnAmt7, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 10, 1) = "1" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_8")) And rsProcess.Tables(0).Rows(i)("VIT_8").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_8").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_8"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    ReimErnAmt8 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                ReimErnAmt8 = iern8
    '            End If
    '        Else
    '            ReimErnAmt8 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN8_RND") = "Y" Then
    '            ReimErnAmt8 = Math.Round(ReimErnAmt8, 0)
    '        Else
    '            ReimErnAmt8 = Math.Round(ReimErnAmt8, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 11, 1) = "1" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_9")) And rsProcess.Tables(0).Rows(i)("VIT_9").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_9").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_9"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    ReimErnAmt9 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                ReimErnAmt9 = iern9
    '            End If
    '        Else
    '            ReimErnAmt9 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN9_RND") = "Y" Then
    '            ReimErnAmt9 = Math.Round(ReimErnAmt9, 0)
    '        Else
    '            ReimErnAmt9 = Math.Round(ReimErnAmt9, 2)
    '        End If
    '        If Mid(rsPaysetup.Tables(0).Rows(0)("REIMBREL"), 12, 1) = "1" Then
    '            If (Not IsNull(rsProcess.Tables(0).Rows(i)("VIT_10")) And rsProcess.Tables(0).Rows(i)("VIT_10").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i)("VIT_10").ToString.Trim <> "0") Then
    '                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_10"))
    '                If sWhichFormula <> "" And sWhichFormula <> " " Then
    '                    ReimErnAmt10 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
    '                End If
    '            Else
    '                ReimErnAmt10 = iern10
    '            End If
    '        Else
    '            ReimErnAmt10 = 0
    '        End If
    '        If rsPaysetup.Tables(0).Rows(0)("EARN10_RND") = "Y" Then
    '            ReimErnAmt10 = Math.Round(ReimErnAmt10, 0)
    '        Else
    '            ReimErnAmt10 = Math.Round(ReimErnAmt10, 2)
    '        End If
    '        sSql = "Update PAY_REIMURSH Set VCONV_AMT=" & Reimbconv_amt & ",VMED_AMT=" & Reimmedical_amt & _
    '                 ",VI_1_AMT=" & ReimErnAmt1 & ",VI_2_AMT=" & ReimErnAmt2 & _
    '                 ",VI_3_AMT=" & ReimErnAmt3 & ",VI_4_AMT=" & ReimErnAmt4 & _
    '                 ",VI_5_AMT=" & ReimErnAmt5 & ",VI_6_AMT=" & ReimErnAmt6 & _
    '                 ",VI_7_AMT=" & ReimErnAmt7 & ",VI_8_AMT=" & ReimErnAmt8 & _
    '                 ",VI_9_AMT=" & ReimErnAmt9 & ",VI_10_AMT=" & ReimErnAmt10 & _
    '                 " Where Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("yyyy")
    '        'Cn.Execute(sSql)
    '        If Common.servername = "Access" Then
    '            If Common.con1.State <> ConnectionState.Open Then
    '                Common.con1.Open()
    '            End If
    '            cmd1 = New OleDbCommand(sSql, Common.con1)
    '            cmd1.ExecuteNonQuery()
    '            If Common.con1.State <> ConnectionState.Closed Then
    '                Common.con1.Close()
    '            End If
    '        Else
    '            If Common.con.State <> ConnectionState.Open Then
    '                Common.con.Open()
    '            End If
    '            cmd = New SqlCommand(sSql, Common.con)
    '            cmd.ExecuteNonQuery()
    '            If Common.con.State <> ConnectionState.Closed Then
    '                Common.con.Close()
    '            End If
    '        End If

    '        gratuityAmt = 0
    '        If rsProcess.Tables(0).Rows(i)("GRADUTY_ALLOWED") = "Y" Then
    '            If IsNull(rsProcess.Tables(0).Rows(i)("dateofjoin")) Then
    '                MsgBox("Date of Joining not feeded in Employee Maser, Cannot Process Gratuity of :" & rsProcess.Tables(0).Rows(i)("empname"), vbInformation + vbExclamation, "TimeWatch Message")
    '            Else
    '                gratuityAmt = 0
    '                sWhichFormula = rsPaysetup.Tables(0).Rows(0)("GRADUTY_FORMULA")
    '                If IsNull(rsProcess.Tables(0).Rows(i)("Leavingdate")) Then
    '                    MYEAR = Format((DateDiff("d", rsProcess.Tables(0).Rows(i)("dateofjoin"), txtapplicablefrom.DateTime) + 1) / 365, "00.00")
    '                Else
    '                    MYEAR = Format((DateDiff("d", rsProcess.Tables(0).Rows(i)("dateofjoin"), rsProcess.Tables(0).Rows(i)("Leavingdate")) + 1) / 365, "00.00")
    '                End If
    '                If MYEAR >= rsPaysetup.Tables(0).Rows(0)("graduty_yrlimit") Then
    '                    MYEAR = Math.Round(MYEAR, 0)
    '                    If InStr(1, UCase(sWhichFormula), " YEAR ") <> 0 Then
    '                        sWhichFormula = Replace(sWhichFormula, " YEAR ", Format(MYEAR, "00.00"))
    '                    End If
    '                    gratuityAmt = Math.Round(PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED), 0)
    '                End If
    '            End If
    '        End If
    '        'rsProcess.MoveNext()
    '    Next ' Loop
    '    sSql = "delete from pay_result where mon_year='" & txtapplicablefrom.DateTime.ToString("yyyy-MM-01") & "' and paycode not in (select paycode from Pay_master)"
    '    If Common.servername = "Access" Then
    '        If Common.con1.State <> ConnectionState.Open Then
    '            Common.con1.Open()
    '        End If
    '        cmd1 = New OleDbCommand(sSql, Common.con1)
    '        cmd1.ExecuteNonQuery()
    '        If Common.con1.State <> ConnectionState.Closed Then
    '            Common.con1.Close()
    '        End If
    '    Else
    '        If Common.con.State <> ConnectionState.Open Then
    '            Common.con.Open()
    '        End If
    '        cmd = New SqlCommand(sSql, Common.con)
    '        cmd.ExecuteNonQuery()
    '        If Common.con.State <> ConnectionState.Closed Then
    '            Common.con.Close()
    '        End If
    '    End If
    '    sSql = "delete from PAY_REIMURSH where mon_year='" & txtapplicablefrom.DateTime.ToString("yyyy-MM-01") & "' and paycode not in (select paycode from Pay_master)"
    '    If Common.servername = "Access" Then
    '        If Common.con1.State <> ConnectionState.Open Then
    '            Common.con1.Open()
    '        End If
    '        cmd1 = New OleDbCommand(sSql, Common.con1)
    '        cmd1.ExecuteNonQuery()
    '        If Common.con1.State <> ConnectionState.Closed Then
    '            Common.con1.Close()
    '        End If
    '    Else
    '        If Common.con.State <> ConnectionState.Open Then
    '            Common.con.Open()
    '        End If
    '        cmd = New SqlCommand(sSql, Common.con)
    '        cmd.ExecuteNonQuery()
    '        If Common.con.State <> ConnectionState.Closed Then
    '            Common.con.Close()
    '        End If
    '    End If
    '    rsProcess = Nothing
    '    'rsAdvance = Nothing
    '    'rsFine = Nothing

    '    DoProcess = True       
    'End Function
    Private Function DoProcess(sSql As String) As Boolean
        Dim PP As XtraPayrollProcess = New XtraPayrollProcess
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim sAdvFn As String
        Dim rsProcess As DataSet
        Dim rsProf As DataSet
        Dim sWhichFormula As String '* 355
        Dim sPaycode As String '* 10, 
        Dim decpos As Integer
        Dim iPFAMT As Double, iFPFAMT As Double, iOtAmt As Double, iHRAAMT As Double, iESIAMT As Double
        Dim iDedAmt1 As Double, iDedAmt2 As Double, iDedAmt3 As Double, iDedAmt4 As Double, iDedAmt5 As Double, iDedAmt6 As Double, iDedAmt7 As Double, iDedAmt8 As Double, iDedAmt9 As Double, iDedAmt10 As Double
        Dim iErnAmt1 As Double, iErnAmt2 As Double, iErnAmt3 As Double, iErnAmt4 As Double, iErnAmt5 As Double, iErnAmt6 As Double, iErnAmt7 As Double, iErnAmt8 As Double, iErnAmt9 As Double, iErnAmt10 As Double
        Dim iSalary As Double, iNetSal As Double, iGrossSal As Double
        Dim BASIC As Double, DA As Double, CONV As Double, MED As Double, PF As Double, FPF As Double, OTRate As Double
        Dim HRA As Double, ESI As Double, Fine As Double, Advance As Double
        Dim PRE As Double, ABS1 As Double, HLD As Double, LATE As Double
        Dim EARLY As Double, OT As Double, CL As Double, SL As Double, PL_EL As Double
        Dim OTHER_LV As Double, LEAVE As Double, TDAYS As Double, T_LATE As Double, T_EARLY As Double
        Dim D_1TOD_10 As Double, E_1TOE_10 As Double, OT_RATE As Double, MON_DAY As Double
        Dim ided1 As Double, ided2 As Double, ided3 As Double, ided4 As Double, ided5 As Double, ided6 As Double, ided7 As Double, ided8 As Double, ided9 As Double, ided10 As Double
        Dim iern1 As Double, iern2 As Double, iern3 As Double, iern4 As Double, iern5 As Double, iern6 As Double, iern7 As Double, iern8 As Double, iern9 As Double, iern10 As Double
        Dim iRESULT As Double
        Dim iWOValue As Integer
        Dim test As Double, mArrearSalary As Double
        Dim rsarrear As DataSet, rsarrearpayday As DataSet
        Dim marrear As Double, marresiamt As Double, marrpfamt As Double, marrearesi As Double, marrearpf As Double, marrearfpf As Double, marrearpfonamt As Double, marrearvpf As Double
        Dim uptodate As Date, arrearpayday As Double, arrearmonday As Double
        Dim amt1_amt As Double, amt2_amt As Double, amt3_amt As Double, amt4_amt As Double, amt5_amt As Double, n As Integer
        Dim conv_amt As Double, medical_amt As Double, tds_amt As Double, prof_tax_amt As Double, amt_on_pf As Double, amt_on_esi As Double, iVPFAMT As Double, iEPFAMT As Double, TDS As Double, PROF_TAX As Double

        Dim Reimbconv_amt As Double, Reimmedical_amt As Double, ReimErnAmt1 As Double, ReimErnAmt2 As Double, ReimErnAmt3 As Double, ReimErnAmt4 As Double, ReimErnAmt5 As Double, ReimErnAmt6 As Double, ReimErnAmt7 As Double, ReimErnAmt8 As Double, ReimErnAmt9 As Double, ReimErnAmt10 As Double
        'On Error GoTo ErrHand
        rsProcess = New DataSet 'Cn.Execute(sSql)
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsProcess)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsProcess)
        End If
        For i As Integer = 0 To rsProcess.Tables(0).Rows.Count - 1 ' Do While Not rsProcess.EOF
            Reimbconv_amt = 0 : Reimmedical_amt = 0 : ReimErnAmt1 = 0 : ReimErnAmt2 = 0 : ReimErnAmt3 = 0 : ReimErnAmt4 = 0 : ReimErnAmt5 = 0 : ReimErnAmt6 = 0 : ReimErnAmt7 = 0 : ReimErnAmt8 = 0 : ReimErnAmt9 = 0 : ReimErnAmt10 = 0
            iPFAMT = 0 : iFPFAMT = 0 : iOtAmt = 0 : iHRAAMT = 0 : Advance = 0 : Fine = 0 : iDedAmt1 = 0 : iDedAmt2 = 0 : iDedAmt3 = 0 : iDedAmt4 = 0 : iDedAmt5 = 0 : iDedAmt6 = 0 : iDedAmt7 = 0 : iDedAmt8 = 0 : iDedAmt9 = 0 : iDedAmt10 = 0
            iErnAmt1 = 0 : iErnAmt2 = 0 : iErnAmt3 = 0 : iErnAmt4 = 0 : iErnAmt5 = 0 : iErnAmt6 = 0 : iErnAmt7 = 0 : iErnAmt8 = 0 : iErnAmt9 = 0 : iErnAmt10 = 0 : iESIAMT = 0
            conv_amt = 0 : medical_amt = 0 : tds_amt = 0 : prof_tax_amt = 0 : amt_on_pf = 0 : amt_on_esi = 0 : iVPFAMT = 0 : iEPFAMT = 0
            sPaycode = rsProcess.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim : BASIC = rsProcess.Tables(0).Rows(i).Item("VBASIC").ToString.Trim
            If rsProcess.Tables(0).Rows(i).Item("VDA_RATE").ToString.Trim = "" Then : DA = 0 : Else : DA = rsProcess.Tables(0).Rows(i).Item("VDA_RATE").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VHRA_RATE").ToString.Trim = "" Then : HRA = 0 : Else : HRA = rsProcess.Tables(0).Rows(i).Item("VHRA_RATE").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("Vmed_rate").ToString.Trim = "" Then : MED = 0 : Else : MED = rsProcess.Tables(0).Rows(i).Item("Vmed_rate").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("Vconv_rate").ToString.Trim = "" Then : CONV = 0 : Else : CONV = rsProcess.Tables(0).Rows(i).Item("Vconv_rate").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VPRE").ToString.Trim = "" Then : PRE = 0 : Else : PRE = rsProcess.Tables(0).Rows(i).Item("VPRE").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VABS").ToString.Trim = "" Then : ABS1 = 0 : Else : ABS1 = rsProcess.Tables(0).Rows(i).Item("VABS").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VHLD").ToString.Trim = "" Then : HLD = 0 : Else : HLD = rsProcess.Tables(0).Rows(i).Item("VHLD") : End If
            If rsProcess.Tables(0).Rows(i).Item("VLATE").ToString.Trim = "" Then : LATE = 0 : Else : LATE = rsProcess.Tables(0).Rows(i).Item("VLATE").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VEARLY").ToString.Trim = "" Then : EARLY = 0 : Else : EARLY = rsProcess.Tables(0).Rows(i).Item("VEARLY").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VOT").ToString.Trim = "" Then : OT = 0 : Else : OT = rsProcess.Tables(0).Rows(i).Item("VOT").ToString.Trim : End If
            ' OT = HrMin.Hr2Min(OT) 'OT * 60 ' Hr2Min(OT)

            Dim values As String() = OT.ToString.Split(".")
            Dim h As Integer = Convert.ToInt32(values(0))
            Dim m As Integer
            Try
                m = Convert.ToInt32(values(1))
            Catch ex As Exception
                m = 0
            End Try
            OT = h * 60 + m

            If rsProcess.Tables(0).Rows(i).Item("VCL").ToString.Trim = "" Then : CL = 0 : Else : CL = rsProcess.Tables(0).Rows(i).Item("VCL").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VSL").ToString.Trim = "" Then : SL = 0 : Else : SL = rsProcess.Tables(0).Rows(i).Item("VSL").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VPL_EL").ToString.Trim = "" Then : PL_EL = 0 : Else : PL_EL = rsProcess.Tables(0).Rows(i).Item("VPL_EL").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VOTHER_LV").ToString.Trim = "" Then : OTHER_LV = 0 : Else : OTHER_LV = rsProcess.Tables(0).Rows(i).Item("VOTHER_LV").ToString.Trim : End If
            LEAVE = CL + SL + PL_EL + OTHER_LV
            iWOValue = IIf(rsProcess.Tables(0).Rows(i).Item("VWO").ToString.Trim = "", 0, rsProcess.Tables(0).Rows(i).Item("VWO").ToString.Trim)
            TDAYS = PRE + HLD + LEAVE + iWOValue
            If rsProcess.Tables(0).Rows(i).Item("VLATE").ToString.Trim = "" Then : T_LATE = 0 : Else : T_LATE = rsProcess.Tables(0).Rows(i).Item("VLATE").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VEARLY").ToString.Trim = "" Then : T_EARLY = 0 : Else : T_EARLY = rsProcess.Tables(0).Rows(i).Item("VEARLY").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("Vtds_rate").ToString.Trim = "" Then : TDS = 0 : Else : TDS = rsProcess.Tables(0).Rows(i).Item("Vtds_rate").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_1").ToString.Trim = "" Then : ided1 = 0 : Else : ided1 = rsProcess.Tables(0).Rows(i).Item("VD_1").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_2").ToString.Trim = "" Then : ided2 = 0 : Else : ided2 = rsProcess.Tables(0).Rows(i).Item("VD_2").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_3").ToString.Trim = "" Then : ided3 = 0 : Else : ided3 = rsProcess.Tables(0).Rows(i).Item("VD_3").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_4").ToString.Trim = "" Then : ided4 = 0 : Else : ided4 = rsProcess.Tables(0).Rows(i).Item("VD_4").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_5").ToString.Trim = "" Then : ided5 = 0 : Else : ided5 = rsProcess.Tables(0).Rows(i).Item("VD_5").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_6").ToString.Trim = "" Then : ided6 = 0 : Else : ided6 = rsProcess.Tables(0).Rows(i).Item("VD_6").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_7").ToString.Trim = "" Then : ided7 = 0 : Else : ided7 = rsProcess.Tables(0).Rows(i).Item("VD_7").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_8").ToString.Trim = "" Then : ided8 = 0 : Else : ided8 = rsProcess.Tables(0).Rows(i).Item("VD_8").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_9").ToString.Trim = "" Then : ided9 = 0 : Else : ided9 = rsProcess.Tables(0).Rows(i).Item("VD_9").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_10").ToString.Trim = "" Then : ided10 = 0 : Else : ided10 = rsProcess.Tables(0).Rows(i).Item("VD_10").ToString.Trim : End If

            If rsProcess.Tables(0).Rows(i).Item("VI_1").ToString.Trim = "" Then : iern1 = 0 : Else : iern1 = rsProcess.Tables(0).Rows(i).Item("VI_1").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_2").ToString.Trim = "" Then : iern2 = 0 : Else : iern2 = rsProcess.Tables(0).Rows(i).Item("VI_2").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_3").ToString.Trim = "" Then : iern3 = 0 : Else : iern3 = rsProcess.Tables(0).Rows(i).Item("VI_3").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_4").ToString.Trim = "" Then : iern4 = 0 : Else : iern4 = rsProcess.Tables(0).Rows(i).Item("VI_4").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_5").ToString.Trim = "" Then : iern5 = 0 : Else : iern5 = rsProcess.Tables(0).Rows(i).Item("VI_5").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_6").ToString.Trim = "" Then : iern6 = 0 : Else : iern6 = rsProcess.Tables(0).Rows(i).Item("VI_6").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_7").ToString.Trim = "" Then : iern7 = 0 : Else : iern7 = rsProcess.Tables(0).Rows(i).Item("VI_7").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_8").ToString.Trim = "" Then : iern8 = 0 : Else : iern8 = rsProcess.Tables(0).Rows(i).Item("VI_8").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_9").ToString.Trim = "" Then : iern9 = 0 : Else : iern9 = rsProcess.Tables(0).Rows(i).Item("VI_9").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_10").ToString.Trim = "" Then : iern10 = 0 : Else : iern10 = rsProcess.Tables(0).Rows(i).Item("VI_10").ToString.Trim : End If

            OT_RATE = rsProcess.Tables(0).Rows(i).Item("VOT_RATE").ToString.Trim
            MON_DAY = Date.DaysInMonth(txtapplicablefrom.DateTime.Year, txtapplicablefrom.DateTime.Month) ' NoOfDay(txtapplicablefrom.Value)
            '******-FOR BASIC SALARY-*************
            iSalary = (BASIC / MON_DAY) * TDAYS
            If rsPaysetup.Tables(0).Rows(0).Item("BASIC_RND").ToString.Trim = "Y" Then
                iSalary = Math.Round(iSalary, 0)
            Else
                iSalary = Math.Round(iSalary, 2)
            End If
            '******-FOR DA*************
            If rsProcess.Tables(0).Rows(i).Item("Vda_f").ToString.Trim = "F" Then
                DA = rsProcess.Tables(0).Rows(i).Item("vda_RATE")
            Else
                DA = (rsProcess.Tables(0).Rows(i).Item("vda_RATE") / MON_DAY) * TDAYS
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DA_RND") = "Y" Then
                DA = Math.Round(DA, 0)
            Else
                DA = Math.Round(DA, 2)
            End If
            '******-FOR HRA -*************
            If rsProcess.Tables(0).Rows(i).Item("VHRA_F").ToString.Trim = "F" Then
                iHRAAMT = rsProcess.Tables(0).Rows(i).Item("VHRA_RATE")
            Else
                iHRAAMT = (rsProcess.Tables(0).Rows(i).Item("vHRA_RATE") / MON_DAY) * TDAYS
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("HRA_RND").ToString.Trim = "Y" Then
                iHRAAMT = Math.Round(iHRAAMT, 0)
            Else
                iHRAAMT = Math.Round(iHRAAMT, 2)
            End If
            '******-FOR CONVEYANCE*************
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 1, 1).Trim.Trim = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("Vconv_f").ToString.Trim = "F" Then
                    conv_amt = rsProcess.Tables(0).Rows(i).Item("Vconv_rate")
                Else
                    conv_amt = (rsProcess.Tables(0).Rows(i).Item("Vconv_rate") / MON_DAY) * TDAYS
                End If
            Else
                conv_amt = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("CONV_RND").ToString.Trim = "Y" Then
                conv_amt = Math.Round(conv_amt, 0)
            Else
                conv_amt = Math.Round(conv_amt, 2)
            End If
            '******-FOR MEDICAL*************
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 2, 1) = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("Vmed_f") = "F" Then
                    medical_amt = rsProcess.Tables(0).Rows(i).Item("Vmed_rate")
                Else
                    medical_amt = (rsProcess.Tables(0).Rows(i).Item("Vmed_rate") / MON_DAY) * TDAYS
                End If
            Else
                medical_amt = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("MEDICAL_RND").ToString.Trim = "Y" Then
                medical_amt = Math.Round(medical_amt, 0)
            Else
                medical_amt = Math.Round(medical_amt, 2)
            End If
            '******-FOR Earning   Formula -**********
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 3, 1) = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_1").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_1").ToString.Trim <> "0" Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_1"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt1 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt1 = iern1
                End If
            Else
                iErnAmt1 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0)("EARN1_RND") = "Y" Then
                iErnAmt1 = Math.Round(iErnAmt1, 0)
            Else
                iErnAmt1 = Math.Round(iErnAmt1, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 4, 1) = "0" Then
                If (rsProcess.Tables(0).Rows(i).Item("VIT_2").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VIT_2").ToString.Trim <> "0" Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_2"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt2 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt2 = iern2
                End If
            Else
                iErnAmt2 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN2_RND").ToString.Trim = "Y" Then
                iErnAmt2 = Math.Round(iErnAmt2, 0)
            Else
                iErnAmt2 = Math.Round(iErnAmt2, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 5, 1) = "0" Then
                If Trim(rsProcess.Tables(0).Rows(i).Item("VIT_3").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_3").ToString.Trim <> "0") Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_3"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt3 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt3 = iern3
                End If
            Else
                iErnAmt3 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN3_RND").ToString.Trim = "Y" Then
                iErnAmt3 = Math.Round(iErnAmt3, 0)
            Else
                iErnAmt3 = Math.Round(iErnAmt3, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 6, 1) = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_4") <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_4").ToString.Trim <> "0" Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_4"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt4 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt4 = iern4
                End If
            Else
                iErnAmt4 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN4_RND").ToString.Trim = "Y" Then
                iErnAmt4 = Math.Round(iErnAmt4, 0)
            Else
                iErnAmt4 = Math.Round(iErnAmt4, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 7, 1) = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_5").ToString.Trim = "" And rsProcess.Tables(0).Rows(i).Item("VIT_5").ToString.Trim <> "0" Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_5"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt5 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt5 = iern5
                End If
            Else
                iErnAmt5 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN5_RND").ToString.Trim = "Y" Then
                iErnAmt5 = Math.Round(iErnAmt5, 0)
            Else
                iErnAmt5 = Math.Round(iErnAmt5, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 8, 1) = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_6").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_6").ToString.Trim <> "0" Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_6"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt6 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt6 = iern6
                End If
            Else
                iErnAmt6 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN6_RND").ToString.Trim = "Y" Then
                iErnAmt6 = Math.Round(iErnAmt6, 0)
            Else
                iErnAmt6 = Math.Round(iErnAmt6, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL"), 9, 1).ToString.Trim = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_7").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_7").ToString.Trim <> "0" Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_7"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt7 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt7 = iern7
                End If
            Else
                iErnAmt7 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN7_RND").ToString.Trim = "Y" Then
                iErnAmt7 = Math.Round(iErnAmt7, 0)
            Else
                iErnAmt7 = Math.Round(iErnAmt7, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL"), 10, 1).ToString.Trim = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_8").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_8").ToString.Trim <> "0" Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_8"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt8 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt8 = iern8
                End If
            Else
                iErnAmt8 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN8_RND").ToString.Trim = "Y" Then
                iErnAmt8 = Math.Round(iErnAmt8, 0)
            Else
                iErnAmt8 = Math.Round(iErnAmt8, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 11, 1) = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_9").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_9").ToString.Trim <> "0" Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_9"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt9 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt9 = iern9
                End If
            Else
                iErnAmt9 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN9_RND").ToString.Trim = "Y" Then
                iErnAmt9 = Math.Round(iErnAmt9, 0)
            Else
                iErnAmt9 = Math.Round(iErnAmt9, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 12, 1) = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_10").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_10").ToString.Trim <> "0" Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_10"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt10 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt10 = iern10
                End If
            Else
                iErnAmt10 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN10_RND").ToString.Trim = "Y" Then
                iErnAmt10 = Math.Round(iErnAmt10, 0)
            Else
                iErnAmt10 = Math.Round(iErnAmt10, 2)
            End If
            '******-FOR OT  AMT FORMULA-*************
            iOtAmt = 0
            If rsProcess.Tables(0).Rows(i).Item("VOT_FLAG").ToString.Trim <> "" Then
                OT = OT
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VOT_FLAG"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iOtAmt = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iOtAmt = OT_RATE
            End If
            If rsPaysetup.Tables(0).Rows(0)("OTAMT_RND") = "Y" Then
                iOtAmt = Math.Round(iOtAmt, 0)
            Else
                iOtAmt = Math.Round(iOtAmt, 2)
            End If
            '******-FOR TDS*************
            If rsProcess.Tables(0).Rows(i).Item("VTDS_F").ToString.Trim <> "" Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VTDS_F"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    tds_amt = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                tds_amt = rsProcess.Tables(0).Rows(i).Item("VTDS_RATE")
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("TDS_RND").ToString.Trim = "Y" Then
                tds_amt = Math.Round(tds_amt, 0)
            Else
                tds_amt = Math.Round(tds_amt, 2)
            End If
            '******-FOR Deduction Formula -**********
            D_1TOD_10 = 0
            If rsProcess.Tables(0).Rows(i).Item("VDT_1").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_1").ToString.Trim <> "0" Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_1"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt1 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt1 = ided1
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED1_RND").ToString.Trim = "Y" Then
                iDedAmt1 = Math.Round(iDedAmt1, 0)
            Else
                iDedAmt1 = Math.Round(iDedAmt1, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_2").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_2").ToString.Trim <> "0" Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_2"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt2 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt2 = ided2
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED2_RND").ToString.Trim = "Y" Then
                iDedAmt2 = Math.Round(iDedAmt2, 0)
            Else
                iDedAmt2 = Math.Round(iDedAmt2, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_3").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_3").ToString.Trim <> "0" Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_3"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt3 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt3 = ided3
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED3_RND").ToString.Trim = "Y" Then
                iDedAmt3 = Math.Round(iDedAmt3, 0)
            Else
                iDedAmt3 = Math.Round(iDedAmt3, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_4").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_4").ToString.Trim <> "0" Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_4"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt4 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt4 = ided4
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED4_RND").ToString.Trim = "Y" Then
                iDedAmt4 = Math.Round(iDedAmt4, 0)
            Else
                iDedAmt4 = Math.Round(iDedAmt4, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_5").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_5").ToString.Trim <> "0" Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_5"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt5 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt5 = ided5
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED5_RND").ToString.Trim = "Y" Then
                iDedAmt5 = Math.Round(iDedAmt5, 0)
            Else
                iDedAmt5 = Math.Round(iDedAmt5, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_6").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_6").ToString.Trim <> "0" Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_6"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt6 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt6 = ided6
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED6_RND").ToString.Trim = "Y" Then
                iDedAmt6 = Math.Round(iDedAmt6, 0)
            Else
                iDedAmt6 = Math.Round(iDedAmt6, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_7").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_7").ToString.Trim <> "0" Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_7"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt7 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt7 = ided7
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED7_RND").ToString.Trim = "Y" Then
                iDedAmt7 = Math.Round(iDedAmt7, 0)
            Else
                iDedAmt7 = Math.Round(iDedAmt7, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_8").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_8").ToString.Trim.ToString.Trim <> "0" Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_8"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt8 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt8 = ided8
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED8_RND").ToString.Trim = "Y" Then
                iDedAmt8 = Math.Round(iDedAmt8, 0)
            Else
                iDedAmt8 = Math.Round(iDedAmt8, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_9").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_9").ToString.Trim <> "0" Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_9"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt9 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt9 = ided9
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED9_RND").ToString.Trim = "Y" Then
                iDedAmt9 = Math.Round(iDedAmt9, 0)
            Else
                iDedAmt9 = Math.Round(iDedAmt9, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_10").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_10").ToString.Trim <> "0" Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VDT_10"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt10 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt10 = ided10
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED10_RND").ToString.Trim = "Y" Then
                iDedAmt10 = Math.Round(iDedAmt10, 0)
            Else
                iDedAmt10 = Math.Round(iDedAmt10, 2)
            End If
            '******-FOR PF AMT FORMULA-**************
            If rsProcess.Tables(0).Rows(i).Item("PF_ALLOWED").ToString.Trim = "Y" Then
                For n = 1 To Len(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim))
                    If n = 1 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iSalary
                    End If
                    If n = 2 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iHRAAMT
                    End If
                    If n = 3 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + DA
                    End If
                    If n = 4 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + conv_amt
                    End If
                    If n = 5 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + medical_amt
                    End If
                    If n = 6 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt1
                    End If
                    If n = 7 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt2
                    End If
                    If n = 8 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt3
                    End If
                    If n = 9 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt4
                    End If
                    If n = 10 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt5
                    End If
                    If n = 11 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt6
                    End If
                    If n = 12 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt7
                    End If
                    If n = 13 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt8
                    End If
                    If n = 14 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt9
                    End If
                    If n = 15 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt10
                    End If
                Next n
                'iPFAMT = Math.Round((IIf(rsPaysetup.Tables(0).Rows(0)("PFFIXED") = "Y", IIf(amt_on_pf > rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), amt_on_pf), amt_on_pf) * rsPaysetup.Tables(0).Rows(0)("PF")) / 100, rsPaysetup.Tables(0).Rows(0)("PFRND"))
                iPFAMT = Math.Round((amt_on_pf * rsPaysetup.Tables(0).Rows(0).Item("PF")) / 100, Convert.ToInt16(rsPaysetup.Tables(0).Rows(0).Item("PFRND").ToString.Trim))
                iEPFAMT = Math.Round((IIf(amt_on_pf > rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT"), rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT"), amt_on_pf) * rsPaysetup.Tables(0).Rows(0).Item("EPF")) / 100, Convert.ToInt16(rsPaysetup.Tables(0).Rows(0).Item("PFRND").ToString.Trim))
                iFPFAMT = iPFAMT - iEPFAMT
                If rsProcess.Tables(0).Rows(i).Item("VPF_ALLOWED") = "Y" Then
                    iVPFAMT = Math.Round((amt_on_pf * rsPaysetup.Tables(0).Rows(0).Item("VPF")) / 100, Convert.ToInt16(rsPaysetup.Tables(0).Rows(0).Item("PFRND").ToString.Trim))
                    iFPFAMT = iFPFAMT + iVPFAMT
                End If
                amt_on_pf = IIf(amt_on_pf > rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), amt_on_pf)
            End If
            '******-FOR ESI AMT FORMULA-*************
            If rsProcess.Tables(0).Rows(i).Item("ESI_ALLOWED").ToString.Trim = "Y" Then
                For n = 1 To Len(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim))
                    If n = 1 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iSalary
                    End If
                    If n = 2 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iHRAAMT
                    End If
                    If n = 3 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + DA
                    End If
                    If n = 4 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + conv_amt
                    End If
                    If n = 5 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + medical_amt
                    End If
                    If n = 6 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt1
                    End If
                    If n = 7 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt2
                    End If
                    If n = 8 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt3
                    End If
                    If n = 9 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt4
                    End If
                    If n = 10 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt5
                    End If
                    If n = 11 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt6
                    End If
                    If n = 12 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt7
                    End If
                    If n = 13 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt8
                    End If
                    If n = 14 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt9
                    End If
                    If n = 15 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt10
                    End If
                Next n
                If Convert.ToDateTime(rsProcess.Tables(0).Rows(i).Item("MON_YEAR").ToString.Trim).ToString("MM") = "04" Or Convert.ToDateTime(rsProcess.Tables(0).Rows(i).Item("MON_YEAR").ToString.Trim).ToString("MM") = "10" And amt_on_esi > rsPaysetup.Tables(0).Rows(0).Item("ESILIMIT").ToString.Trim Then
                    amt_on_esi = 0
                    iESIAMT = 0
                Else
                    iESIAMT = IIf(amt_on_esi > rsPaysetup.Tables(0).Rows(0).Item("ESILIMIT"), (rsPaysetup.Tables(0).Rows(0).Item("ESILIMIT") * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100, (amt_on_esi * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100)
                    'iESIAMT = IIf(amt_on_esi > rsPaysetup.Tables(0).Rows(0).Item("ESILIMIT"), (rsPaysetup.Tables(0).Rows(0).Item("ESILIMIT") * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100, (amt_on_esi * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100) + ((iOtAmt * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100)
                    iESIAMT = Math.Round(iESIAMT, 0) + IIf(iESIAMT - Math.Round(iESIAMT, 0) > 0, 1, 0)
                End If
            End If
            '**Advance**
            sAdvFn = "Select Sum(Inst_Amt) as InstAmt From TBLADVANCEDATA " & _
                    "Where Paycode='" & Trim(sPaycode) & _
                    "' And DatePart(mm,Mon_Year) = " & txtapplicablefrom.DateTime.ToString("MM") & _
                    " And DatePart(yyyy,Mon_Year) = " & txtapplicablefrom.DateTime.ToString("yyyy") & _
                    " And A_L='" & "A'"

            Dim rsAdvance As DataSet = New DataSet 'Cn.Execute(sSql)
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sAdvFn, Common.con1)
                adapA.Fill(rsAdvance)
            Else
                adap = New SqlDataAdapter(sAdvFn, Common.con)
                adap.Fill(rsAdvance)
            End If
            If rsAdvance.Tables(0).Rows(0).Item("InstAmt").ToString.Trim = "" Then : Advance = 0 : Else : Advance = rsAdvance.Tables(0).Rows(0).Item("InstAmt") : End If
            '**Fine**
            sAdvFn = "Select Sum(Inst_Amt) AS InstAmt From TBLADVANCEDATA " & _
                 "Where Paycode='" & sPaycode & _
                 "' And DatePart(mm,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("MM") & _
                 " And DatePart(yyyy,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("yyyy") & " And A_L='L'"

            Dim rsFine As DataSet = New DataSet 'Cn.Execute(sSql)
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sAdvFn, Common.con1)
                adapA.Fill(rsFine)
            Else
                adap = New SqlDataAdapter(sAdvFn, Common.con)
                adap.Fill(rsFine)
            End If

            Fine = IIf(IsNull(rsFine.Tables(0).Rows(0)("InstAmt")), 0, rsFine.Tables(0).Rows(0)("InstAmt"))
            If rsFine.Tables(0).Rows(0).Item("InstAmt").ToString.Trim = "" Then : Fine = 0 : Else : Fine = rsFine.Tables(0).Rows(0).Item("InstAmt").ToString.Trim : End If
            '****************************************
            D_1TOD_10 = iDedAmt1 + iDedAmt2 + iDedAmt3 + iDedAmt4 + iDedAmt5 + iDedAmt6 + iDedAmt7 + iDedAmt8 + iDedAmt9 + iDedAmt10
            E_1TOE_10 = iErnAmt1 + iErnAmt2 + iErnAmt3 + iErnAmt4 + iErnAmt5 + iErnAmt6 + iErnAmt7 + iErnAmt8 + iErnAmt9 + iErnAmt10
            iGrossSal = iSalary + DA + iHRAAMT + iOtAmt + E_1TOE_10 + conv_amt + medical_amt
            '******-FOR PROFESSIONAL TAX*************
            If rsProcess.Tables(0).Rows(i)("PROF_TAX_ALLOWED") = "Y" Then
                sSql = "select * from professionaltax where lowerlimit<=" & iGrossSal & " and upperlimit>=" & iGrossSal
                rsProf = New DataSet 'Cn.Execute(sSql)
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsProf)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsProf)
                End If
                If rsProf.Tables(0).Rows.Count > 0 Then
                    prof_tax_amt = rsProf.Tables(0).Rows(0).Item("taxamount")
                End If
                If rsPaysetup.Tables(0).Rows(0).Item("PROF_TAX_RND").ToString.Trim = "Y" Then
                    prof_tax_amt = Math.Round(prof_tax_amt, 0)
                Else
                    prof_tax_amt = Math.Round(prof_tax_amt, 2)
                End If
            End If
            iNetSal = iGrossSal - (D_1TOD_10 + Fine + Advance + iESIAMT + iPFAMT + iVPFAMT + tds_amt + prof_tax_amt)
            netSal = iNetSal
            sSql = "Update pay_result Set AMTONPF=" & amt_on_pf & ",VPF_AMT=" & iPFAMT & ",VEPF_AMT=" & iEPFAMT & ",VFPF_AMT=" & iFPFAMT & ",VVPF_AMT=" & iVPFAMT & _
                 ",VTDS_AMT=" & tds_amt & ",PROF_TAX_AMT=" & prof_tax_amt & ",VHRA_AMT=" & iHRAAMT & ",VCONV_AMT=" & conv_amt & ",VMED_AMT=" & medical_amt & ",AMTONESI=" & amt_on_esi & ",ESIONOT=" & IIf(iESIAMT > 0, Math.Round((iOtAmt * rsPaysetup.Tables(0).Rows(0)("ESIE")) / 100, 0) + IIf((iOtAmt * rsPaysetup.Tables(0).Rows(0)("ESIE")) / 100 - Math.Round((iOtAmt * rsPaysetup.Tables(0).Rows(0)("ESIE")) / 100, 0) > 0, 1, 0), 0) & ",VESI_AMT=" & iESIAMT & ",vot_amt=" & iOtAmt & _
                 ",VFINE=" & Fine & ",VADVANCE=" & Advance & ",VDA_AMT=" & DA & _
                 ",VD_1_AMT=" & iDedAmt1 & ",VD_2_AMT=" & iDedAmt2 & _
                 ",VD_3_AMT=" & iDedAmt3 & ",VD_4_AMT=" & iDedAmt4 & _
                 ",VD_5_AMT=" & iDedAmt5 & ",VD_6_AMT=" & iDedAmt6 & _
                 ",VD_7_AMT=" & iDedAmt7 & ",VD_8_AMT=" & iDedAmt8 & _
                 ",VD_9_AMT=" & iDedAmt9 & ",VD_10_AMT=" & iDedAmt10 & _
                 ",VI_1_AMT=" & iErnAmt1 & ",VI_2_AMT=" & iErnAmt2 & _
                 ",VI_3_AMT=" & iErnAmt3 & ",VI_4_AMT=" & iErnAmt4 & _
                 ",VI_5_AMT=" & iErnAmt5 & ",VI_6_AMT=" & iErnAmt6 & _
                 ",VI_7_AMT=" & iErnAmt7 & ",VI_8_AMT=" & iErnAmt8 & _
                 ",VI_9_AMT=" & iErnAmt9 & ",VI_10_AMT=" & iErnAmt10 & _
                 ",VTOTAL_DAY=" & MON_DAY & ",VLEAVE=" & LEAVE & _
                 ",VSALARY=" & iSalary & ",VTDAYS=" & TDAYS & " Where Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("yyyy")
            'Cn.Execute(sSql)
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            ''            REIMBURSHMENT CALCULATION
            '******-FOR CONVEYANCE*************
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL"), 1, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("Vconv_f") = "F" Then
                    Reimbconv_amt = rsProcess.Tables(0).Rows(i).Item("Vconv_rate")
                Else
                    Reimbconv_amt = (rsProcess.Tables(0).Rows(i).Item("Vconv_rate") / MON_DAY) * TDAYS
                End If
            Else
                Reimbconv_amt = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("CONV_RND").ToString.Trim = "Y" Then
                Reimbconv_amt = Math.Round(Reimbconv_amt, 0)
            Else
                Reimbconv_amt = Math.Round(Reimbconv_amt, 2)
            End If
            '******-FOR MEDICAL*************
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 2, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("Vmed_f") = "F" Then
                    Reimmedical_amt = rsProcess.Tables(0).Rows(i).Item("Vmed_rate")
                Else
                    Reimmedical_amt = (rsProcess.Tables(0).Rows(i).Item("Vmed_rate") / MON_DAY) * TDAYS
                End If
            Else
                Reimmedical_amt = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("MEDICAL_RND").ToString.Trim = "Y" Then
                Reimmedical_amt = Math.Round(Reimmedical_amt, 0)
            Else
                Reimmedical_amt = Math.Round(Reimmedical_amt, 2)
            End If
            '******-FOR Earning   Formula -**********
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 3, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_1").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_1").ToString.Trim <> "0" Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_1"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt1 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt1 = iern1
                End If
            Else
                ReimErnAmt1 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN1_RND").ToString.Trim = "Y" Then
                ReimErnAmt1 = Math.Round(ReimErnAmt1, 0)
            Else
                ReimErnAmt1 = Math.Round(ReimErnAmt1, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 4, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_2").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_2").ToString.Trim <> "0" Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_2"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt2 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt2 = iern2
                End If
            Else
                ReimErnAmt2 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN2_RND").ToString.Trim = "Y" Then
                ReimErnAmt2 = Math.Round(ReimErnAmt2, 0)
            Else
                ReimErnAmt2 = Math.Round(ReimErnAmt2, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 5, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_3").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_3").ToString.Trim <> "0" Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_3"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt3 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt3 = iern3
                End If
            Else
                ReimErnAmt3 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN3_RND").ToString.Trim = "Y" Then
                ReimErnAmt3 = Math.Round(ReimErnAmt3, 0)
            Else
                ReimErnAmt3 = Math.Round(ReimErnAmt3, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 6, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_4").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_4") <> 0 Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_4"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt4 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt4 = iern4
                End If
            Else
                ReimErnAmt4 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN4_RND").ToString.Trim = "Y" Then
                ReimErnAmt4 = Math.Round(ReimErnAmt4, 0)
            Else
                ReimErnAmt4 = Math.Round(ReimErnAmt4, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 7, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_5").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_5") <> 0 Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_5"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt5 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt5 = iern5
                End If
            Else
                ReimErnAmt5 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN5_RND").ToString.Trim = "Y" Then
                ReimErnAmt5 = Math.Round(ReimErnAmt5, 0)
            Else
                ReimErnAmt5 = Math.Round(ReimErnAmt5, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 8, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_6").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_6") <> 0 Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_6"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt6 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt6 = iern6
                End If
            Else
                ReimErnAmt6 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN6_RND").ToString.Trim = "Y" Then
                ReimErnAmt6 = Math.Round(ReimErnAmt6, 0)
            Else
                ReimErnAmt6 = Math.Round(ReimErnAmt6, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 9, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_7").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_7") <> 0 Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_7"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt7 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt7 = iern7
                End If
            Else
                ReimErnAmt7 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN7_RND").ToString.Trim = "Y" Then
                ReimErnAmt7 = Math.Round(ReimErnAmt7, 0)
            Else
                ReimErnAmt7 = Math.Round(ReimErnAmt7, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL"), 10, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_8").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_8") <> 0 Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_8"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt8 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt8 = iern8
                End If
            Else
                ReimErnAmt8 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN8_RND").ToString.Trim = "Y" Then
                ReimErnAmt8 = Math.Round(ReimErnAmt8, 0)
            Else
                ReimErnAmt8 = Math.Round(ReimErnAmt8, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 11, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_9").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_9") <> 0 Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_9"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt9 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt9 = iern9
                End If
            Else
                ReimErnAmt9 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN9_RND").ToString.Trim = "Y" Then
                ReimErnAmt9 = Math.Round(ReimErnAmt9, 0)
            Else
                ReimErnAmt9 = Math.Round(ReimErnAmt9, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 12, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_10").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_10") <> 0 Then
                    sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(i)("VIT_10"))
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt10 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt10 = iern10
                End If
            Else
                ReimErnAmt10 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN10_RND").ToString.Trim = "Y" Then
                ReimErnAmt10 = Math.Round(ReimErnAmt10, 0)
            Else
                ReimErnAmt10 = Math.Round(ReimErnAmt10, 2)
            End If
            sSql = "Update PAY_REIMURSH Set VCONV_AMT=" & Reimbconv_amt & ",VMED_AMT=" & Reimmedical_amt & _
                     ",VI_1_AMT=" & ReimErnAmt1 & ",VI_2_AMT=" & ReimErnAmt2 & _
                     ",VI_3_AMT=" & ReimErnAmt3 & ",VI_4_AMT=" & ReimErnAmt4 & _
                     ",VI_5_AMT=" & ReimErnAmt5 & ",VI_6_AMT=" & ReimErnAmt6 & _
                     ",VI_7_AMT=" & ReimErnAmt7 & ",VI_8_AMT=" & ReimErnAmt8 & _
                     ",VI_9_AMT=" & ReimErnAmt9 & ",VI_10_AMT=" & ReimErnAmt10 & _
                     " Where Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("yyyy")
            'Cn.Execute(sSql)
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If

            gratuityAmt = 0
            If rsProcess.Tables(0).Rows(i)("GRADUTY_ALLOWED") = "Y" Then
                If IsNull(rsProcess.Tables(0).Rows(i)("dateofjoin")) Then
                    MsgBox("Date of Joining not feeded in Employee Maser, Cannot Process Gratuity of :" & rsProcess.Tables(0).Rows(i)("empname"), vbInformation + vbExclamation, "TimeWatch Message")
                Else
                    gratuityAmt = 0
                    sWhichFormula = rsPaysetup.Tables(0).Rows(0)("GRADUTY_FORMULA")
                    If IsNull(rsProcess.Tables(0).Rows(i)("Leavingdate")) Then
                        MYEAR = Format((DateDiff("d", rsProcess.Tables(0).Rows(i)("dateofjoin"), txtapplicablefrom.DateTime) + 1) / 365, "00.00")
                    Else
                        MYEAR = Format((DateDiff("d", rsProcess.Tables(0).Rows(i)("dateofjoin"), rsProcess.Tables(0).Rows(i)("Leavingdate")) + 1) / 365, "00.00")
                    End If
                    If MYEAR >= rsPaysetup.Tables(0).Rows(0)("graduty_yrlimit") Then
                        MYEAR = Math.Round(MYEAR, 0)
                        If InStr(1, UCase(sWhichFormula), " YEAR ") <> 0 Then
                            sWhichFormula = Replace(sWhichFormula, " YEAR ", Format(MYEAR, "00.00"))
                        End If
                        gratuityAmt = Math.Round(PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED), 0)
                    End If
                End If
            End If
            'rsProcess.MoveNext()
        Next ' Loop
        sSql = "delete from pay_result where mon_year='" & txtapplicablefrom.DateTime.ToString("yyyy-MM-01") & "' and paycode not in (select paycode from Pay_master)"
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        sSql = "delete from PAY_REIMURSH where mon_year='" & txtapplicablefrom.DateTime.ToString("yyyy-MM-01") & "' and paycode not in (select paycode from Pay_master)"
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        rsProcess = Nothing
        'rsAdvance = Nothing
        'rsFine = Nothing

        DoProcess = True
    End Function
    Private Function IsNull(tmpStr As Object) As Boolean
        If tmpStr.ToString.Trim = "" Then
            Return True
        Else
            Return False
        End If
    End Function
    Function DATACAPTURE() As Boolean
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim sSql As String
        Dim rsCapture As DataSet
        'Dim rsCheckTable As DataSet
        Dim rspaymaster As DataSet
        Dim rsPiecedata As DataSet
        Dim sPaycode As String
        Dim iVTotHrsWorked As Integer, iVOTDuration As Integer
        Dim iVEarlyDeparture As Integer, iVLateArrival As Integer
        Dim iEarlyDays As Integer, iLateDays As Integer
        Dim iVLossHours As Integer, iPresentValue As Double
        Dim iAbsentValue As Double, iHolidayValue As Double
        Dim iWOValue As Double, sLeaveCode As String '* 3
        Dim iVCL As Double, iVSL As Double
        Dim iVPL_EL As Double, iVOther_LV As Double
        Dim iTotalLeave As Double, iLeaveAmount As Double
        'Dim sMonth As String '* 6,
        Dim iTotalCount As Long
        'Dim i As Integer
        'Dim mRsEmployee As DataSet
        Dim iTotalDays As Double, mLeaveType As String, mLeaveType1 As String, mLeaveType2 As String
        sSql = "Select a.PAYCODE,a.DATEOFFICE,a.HOURSWORKED,a.OTDURATION,a.EARLYDEPARTURE," & _
           "a.LATEARRIVAL,a.TOTALLOSSHRS,a.LEAVEVALUE,a.PRESENTVALUE,a.ABSENTVALUE," & _
           "a.HOLIDAY_VALUE,a.WO_VALUE,a.LEAVECODE,a.LEAVEAMOUNT,a.leavetype1,a.leavetype2,a.leavetype,a.firsthalfleavecode,a.SECONDHALFLEAVECODE,a.leaveamount1,a.leaveamount2,B.VEmployeeType From tblTimeRegister a,pay_master b " & _
           "Where a.paycode=b.paycode and a.PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "' " & _
           "And DATEOFFICE Between '" & txtapplicablefrom.DateTime.ToString("yyyy-MM-01") & "' " & _
           "And '" & txtapplicablefrom.DateTime.ToString("yyyy-MM-dd") & "' order by PAYCODE,DATEOFFICE"
        rsCapture = New DataSet 'Cn.Execute(sSql)
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsCapture)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsCapture)
        End If

        iTotalCount = rsCapture.Tables(0).Rows.Count
        If rsCapture.Tables(0).Rows.Count >= 1 Then
            'Screen.MousePointer = vbHourglass
            '*****for toDate table************************
            DATACAPTURE = True
            'rsCapture.Sort = "Paycode,DateOffice"
            For i As Integer = 0 To rsCapture.Tables(0).Rows.Count - 1 '  Do While Not rsCapture.EOF
                sPaycode = rsCapture.Tables(0).Rows(i)("PAYCODE")
                iVTotHrsWorked = 0 : iVOTDuration = 0 : iVEarlyDeparture = 0
                iVLateArrival = 0 : iVLossHours = 0 : iPresentValue = 0
                iAbsentValue = 0 : iHolidayValue = 0 : iWOValue = 0
                sLeaveCode = "" : iVCL = 0 : iVSL = 0
                iVPL_EL = 0 : iVOther_LV = 0 : iTotalLeave = 0
                iLeaveAmount = 0 : iEarlyDays = 0 : iLateDays = 0


                Do While Trim(sPaycode) = Trim(rsCapture.Tables(0).Rows(i)("PAYCODE"))
                    iVTotHrsWorked = iVTotHrsWorked + rsCapture.Tables(0).Rows(i)("HOURSWORKED")
                    iVOTDuration = iVOTDuration + rsCapture.Tables(0).Rows(i)("OTDURATION")
                    iVEarlyDeparture = iVEarlyDeparture + rsCapture.Tables(0).Rows(i)("EARLYDEPARTURE")
                    If rsCapture.Tables(0).Rows(i)("EARLYDEPARTURE") > 0 Then iEarlyDays = iEarlyDays + 1
                    iVLateArrival = iVLateArrival + rsCapture.Tables(0).Rows(i)("LATEARRIVAL")
                    If rsCapture.Tables(0).Rows(i)("LATEARRIVAL") > 0 Then iLateDays = iLateDays + 1
                    iVLossHours = iVLossHours + rsCapture.Tables(0).Rows(i)("TOTALLOSSHRS")
                    iPresentValue = iPresentValue + rsCapture.Tables(0).Rows(i)("PRESENTVALUE")
                    iAbsentValue = iAbsentValue + rsCapture.Tables(0).Rows(i)("ABSENTVALUE")
                    iHolidayValue = iHolidayValue + rsCapture.Tables(0).Rows(i)("HOLIDAY_VALUE")
                    iWOValue = iWOValue + rsCapture.Tables(0).Rows(i)("WO_VALUE")
                    mLeaveType = ""
                    mLeaveType1 = ""
                    mLeaveType2 = ""
                    If Not IsNull(rsCapture.Tables(0).Rows(i)("leavetype").ToString.Trim) Then
                        mLeaveType = rsCapture.Tables(0).Rows(i)("leavetype")
                    End If
                    If Not IsNull(rsCapture.Tables(0).Rows(i)("leavetype1").ToString.Trim) Then
                        mLeaveType1 = rsCapture.Tables(0).Rows(i)("leavetype1")
                    End If
                    If Not IsNull(rsCapture.Tables(0).Rows(i)("leavetype2").ToString.Trim) Then
                        mLeaveType2 = rsCapture.Tables(0).Rows(i)("leavetype2")
                    End If
                    If (UCase(Trim(rsCapture.Tables(0).Rows(i)("firsthalfleavecode").ToString.Trim)) <> "" And UCase(Trim(mLeaveType1)) = "L") Or (UCase(Trim(rsCapture.Tables(0).Rows(i)("SECONDHALFLEAVECODE").ToString)) <> "" And UCase(Trim(mLeaveType2)) = "L") Then
                        If UCase(Trim(rsCapture.Tables(0).Rows(i)("firsthalfleavecode").ToString)) = "CL" Then iVCL = iVCL + rsCapture.Tables(0).Rows(i)("leaveamount1")
                        If UCase(Trim(rsCapture.Tables(0).Rows(i)("firsthalfleavecode").ToString)) = "EL" Then iVPL_EL = iVPL_EL + rsCapture.Tables(0).Rows(i)("leaveamount1")
                        If UCase(Trim(rsCapture.Tables(0).Rows(i)("firsthalfleavecode").ToString)) <> "EL" And UCase(Trim(rsCapture.Tables(0).Rows(i)("firsthalfleavecode").ToString)) <> "CL" And UCase(Trim(rsCapture.Tables(0).Rows(i)("firsthalfleavecode").ToString)) <> "PL" And UCase(Trim(rsCapture.Tables(0).Rows(i)("firsthalfleavecode").ToString)) <> "SL" Then iVOther_LV = iVOther_LV + rsCapture.Tables(0).Rows(i)("leaveamount1")
                        If UCase(Trim(rsCapture.Tables(0).Rows(i)("firsthalfleavecode").ToString)) = "PL" Then iVPL_EL = iVPL_EL + rsCapture.Tables(0).Rows(i)("leaveamount1")
                        If UCase(Trim(rsCapture.Tables(0).Rows(i)("firsthalfleavecode").ToString)) = "SL" Then iVSL = iVSL + rsCapture.Tables(0).Rows(i)("leaveamount1")

                        If UCase(Trim(rsCapture.Tables(0).Rows(i)("SECONDHALFLEAVECODE").ToString)) = "CL" Then iVCL = iVCL + rsCapture.Tables(0).Rows(i)("leaveamount2")
                        If UCase(Trim(rsCapture.Tables(0).Rows(i)("SECONDHALFLEAVECODE").ToString)) = "EL" Then iVPL_EL = iVPL_EL + rsCapture.Tables(0).Rows(i)("leaveamount2")
                        If UCase(Trim(rsCapture.Tables(0).Rows(i)("SECONDHALFLEAVECODE").ToString)) <> "EL" And UCase(Trim(rsCapture.Tables(0).Rows(i)("SECONDHALFLEAVECODE"))) <> "CL" And UCase(Trim(rsCapture.Tables(0).Rows(i)("SECONDHALFLEAVECODE"))) <> "PL" And UCase(Trim(rsCapture.Tables(0).Rows(i)("SECONDHALFLEAVECODE"))) <> "SL" Then iVOther_LV = iVOther_LV + rsCapture.Tables(0).Rows(i)("leaveamount2")
                        If UCase(Trim(rsCapture.Tables(0).Rows(i)("SECONDHALFLEAVECODE").ToString)) = "PL" Then iVPL_EL = iVPL_EL + rsCapture.Tables(0).Rows(i)("leaveamount2")
                        If UCase(Trim(rsCapture.Tables(0).Rows(i)("SECONDHALFLEAVECODE").ToString)) = "SL" Then iVSL = iVSL + rsCapture.Tables(0).Rows(i)("leaveamount2")
                    Else
                        If UCase(Trim(mLeaveType)) = "L" Then
                            If UCase(Trim(rsCapture.Tables(0).Rows(i)("leavecode").ToString)) = "CL" Then iVCL = iVCL + rsCapture.Tables(0).Rows(i)("leaveamount")
                            If UCase(Trim(rsCapture.Tables(0).Rows(i)("leavecode").ToString)) = "EL" Then iVPL_EL = iVPL_EL + rsCapture.Tables(0).Rows(i)("leaveamount")
                            If UCase(Trim(rsCapture.Tables(0).Rows(i)("leavecode").ToString)) <> "EL" And UCase(Trim(rsCapture.Tables(0).Rows(i)("leavecode").ToString)) <> "CL" And UCase(Trim(rsCapture.Tables(0).Rows(i)("leavecode").ToString)) <> "PL" And UCase(Trim(rsCapture.Tables(0).Rows(i)("leavecode").ToString)) <> "SL" Then iVOther_LV = iVOther_LV + rsCapture.Tables(0).Rows(i)("leaveamount")
                            If UCase(Trim(rsCapture.Tables(0).Rows(i)("leavecode").ToString)) = "PL" Then iVPL_EL = iVPL_EL + rsCapture.Tables(0).Rows(i)("leaveamount")
                            If UCase(Trim(rsCapture.Tables(0).Rows(i)("leavecode").ToString)) = "SL" Then iVSL = iVSL + rsCapture.Tables(0).Rows(i)("leaveamount")
                        End If
                    End If
                    iLeaveAmount = iLeaveAmount + IIf(IsNull(rsCapture.Tables(0).Rows(i)("LEAVEVALUE").ToString), 0, rsCapture.Tables(0).Rows(i)("LEAVEVALUE"))
                    'rsCapture.MoveNext()
                    i = i + 1
                    If i > rsCapture.Tables(0).Rows.Count - 1 Then
                        GoTo tmp
                        Exit Do
                    End If
                    'If rsCapture.AbsolutePosition = adPosEOF Then
                    '    Exit Do
                    'End If
                    'If rsCapture.EOF Then
                    '    Exit Do
                    'End If
                Loop
                i = i - 1
tmp:            iTotalLeave = iVCL + iVSL + iVPL_EL + iVOther_LV
                iTotalDays = iPresentValue + iWOValue + iHolidayValue + iLeaveAmount

                sSql = "Select Paycode From Pay_result" & " Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "' And DatePart(mm,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("yyyy")
                Dim rsMonthTable As DataSet = New DataSet 'Cn.Execute(sSql)
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsMonthTable)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsMonthTable)
                End If
                sSql = "Select * From Pay_master" & " Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
                rspaymaster = New DataSet 'Cn.Execute(sSql)
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rspaymaster)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rspaymaster)
                End If
                If rsMonthTable.Tables(0).Rows.Count = 1 Then
                    If rspaymaster.Tables(0).Rows.Count = 1 Then
                        sSql = "Update Pay_Result Set VBASIC=" & rspaymaster.Tables(0).Rows(0)("VBASIC") & ",VDA_RATE=" & rspaymaster.Tables(0).Rows(0)("VDA_RATE") & ",VDA_F='" & rspaymaster.Tables(0).Rows(0)("VDA_F") & "'" & _
                            ",VCONV_RATE=" & rspaymaster.Tables(0).Rows(0)("VCONV_RATE") & ",VCONV_F='" & rspaymaster.Tables(0).Rows(0)("VCONV_F") & "'" & _
                            ",VMED_RATE=" & rspaymaster.Tables(0).Rows(0)("VMED_RATE") & ",VMED_F='" & rspaymaster.Tables(0).Rows(0)("VMED_F") & "'" & _
                            ",PF_ALLOWED='" & rspaymaster.Tables(0).Rows(0)("PF_ALLOWED") & "',VPF_ALLOWED='" & rspaymaster.Tables(0).Rows(0)("VPF_ALLOWED") & "'," & _
                            "VOT_RATE=" & rspaymaster.Tables(0).Rows(0)("VOT_RATE") & ",VOT_FLAG='" & rspaymaster.Tables(0).Rows(0)("VOT_FLAG") & "'," & _
                            "VHRA_RATE=" & rspaymaster.Tables(0).Rows(0)("VHRA_RATE") & ",VHRA_F='" & rspaymaster.Tables(0).Rows(0)("VHRA_F") & "',ESI_ALLOWED='" & rspaymaster.Tables(0).Rows(0)("ESI_ALLOWED") & "'," & _
                            "VDT_1='" & rspaymaster.Tables(0).Rows(0)("VDT_1") & "',VD_1=" & rspaymaster.Tables(0).Rows(0)("VD_1") & ",VDT_2='" & rspaymaster.Tables(0).Rows(0)("VDT_2") & "',VD_2=" & rspaymaster.Tables(0).Rows(0)("VD_2") & _
                            ",VDT_3='" & rspaymaster.Tables(0).Rows(0)("VDT_3") & "',VD_3=" & rspaymaster.Tables(0).Rows(0)("VD_3") & ",VDT_4='" & rspaymaster.Tables(0).Rows(0)("VDT_4") & "',VD_4=" & rspaymaster.Tables(0).Rows(0)("VD_4") & _
                            ",VDT_5='" & rspaymaster.Tables(0).Rows(0)("VDT_5") & "',VD_5=" & rspaymaster.Tables(0).Rows(0)("VD_5") & ",VDT_6='" & rspaymaster.Tables(0).Rows(0)("VDT_6") & "',VD_6=" & rspaymaster.Tables(0).Rows(0)("VD_6") & _
                            ",VDT_7='" & rspaymaster.Tables(0).Rows(0)("VDT_7") & "',VD_7=" & rspaymaster.Tables(0).Rows(0)("VD_7") & ",VDT_8='" & rspaymaster.Tables(0).Rows(0)("VDT_8") & "',VD_8=" & rspaymaster.Tables(0).Rows(0)("VD_8") & _
                            ",VDT_9='" & rspaymaster.Tables(0).Rows(0)("VDT_9") & "',VD_9=" & rspaymaster.Tables(0).Rows(0)("VD_9") & ",VDT_10='" & rspaymaster.Tables(0).Rows(0)("VDT_10") & "',VD_10=" & rspaymaster.Tables(0).Rows(0)("VD_10") & _
                            ",VIT_1='" & rspaymaster.Tables(0).Rows(0)("VIT_1") & "',VI_1=" & rspaymaster.Tables(0).Rows(0)("VI_1") & ",VIT_2='" & rspaymaster.Tables(0).Rows(0)("VIT_2") & "',VI_2=" & rspaymaster.Tables(0).Rows(0)("VI_2") & _
                            ",VIT_3='" & rspaymaster.Tables(0).Rows(0)("VIT_3") & "',VI_3=" & rspaymaster.Tables(0).Rows(0)("VI_3") & ",VIT_4='" & rspaymaster.Tables(0).Rows(0)("VIT_4") & "',VI_4=" & rspaymaster.Tables(0).Rows(0)("VI_4") & _
                            ",VIT_5='" & rspaymaster.Tables(0).Rows(0)("VIT_5") & "',VI_5=" & rspaymaster.Tables(0).Rows(0)("VI_5") & ",VIT_6='" & rspaymaster.Tables(0).Rows(0)("VIT_6") & "',VI_6=" & rspaymaster.Tables(0).Rows(0)("VI_6") & _
                            ",VIT_7='" & rspaymaster.Tables(0).Rows(0)("VIT_7") & "',VI_7=" & rspaymaster.Tables(0).Rows(0)("VI_7") & ",VIT_8='" & rspaymaster.Tables(0).Rows(0)("VIT_8") & "',VI_8=" & rspaymaster.Tables(0).Rows(0)("VI_8") & _
                            ",VIT_9='" & rspaymaster.Tables(0).Rows(0)("VIT_9") & "',VI_9=" & rspaymaster.Tables(0).Rows(0)("VI_9") & ",VIT_10='" & rspaymaster.Tables(0).Rows(0)("VIT_10") & "',VI_10=" & rspaymaster.Tables(0).Rows(0)("VI_10") & _
                            ",VTDS_RATE=" & rspaymaster.Tables(0).Rows(0)("VTDS_RATE") & ",VTDS_F='" & rspaymaster.Tables(0).Rows(0)("VTDS_F") & "'" & _
                            ",PROF_TAX_Allowed='" & rspaymaster.Tables(0).Rows(0)("PROF_TAX_AllOwed") & "'" & _
                            " where pay_Result.Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("yyyy")
                        'Cn.Execute(sSql)
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                        sSql = "Update Pay_Result Set VOT=" & Min2Hr(Trim(Str(iVOTDuration))) & _
                                ",VT_EARLY=" & iVEarlyDeparture & ",VT_LATE=" & iVLateArrival & _
                                ",VPRE=" & iPresentValue & ",VABS=" & iAbsentValue & _
                                ",VHLD=" & iHolidayValue & ",VCL=" & iVCL & ",VSL=" & iVSL & _
                                ",VPL_EL=" & iVPL_EL & ",VOTHER_LV=" & iVOther_LV & ",VLEAVE=" & iLeaveAmount & _
                                ",VLATE=" & iLateDays & ",VEARLY=" & iEarlyDays & ",VWO=" & iWOValue & ",VTDAYS=" & iTotalDays & _
                                " where pay_Result.Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("yyyy")
                        'Cn.Execute(sSql)
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                    End If
                Else

                    If rspaymaster.Tables(0).Rows.Count = 1 Then
                        sSql = "Insert Into Pay_Result(PAYCODE,mon_year) values('" & Trim(sPaycode) & "','" & txtapplicablefrom.DateTime.ToString("yyyy-MM-01") & "')"
                        'Cn.Execute(sSql)
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                        sSql = "Insert Into PAY_REIMURSH(PAYCODE,mon_year) values('" & Trim(sPaycode) & "','" & txtapplicablefrom.DateTime.ToString("yyyy-MM-01") & "')"
                        'Cn.Execute(sSql)
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                        sSql = "Update Pay_Result Set VBASIC=" & rspaymaster.Tables(0).Rows(0)("VBASIC") & ",VDA_RATE=" & rspaymaster.Tables(0).Rows(0)("VDA_RATE") & ",VDA_F='" & rspaymaster.Tables(0).Rows(0)("VDA_F") & "'" & _
                            ",VCONV_RATE=" & rspaymaster.Tables(0).Rows(0)("VCONV_RATE") & ",VCONV_F='" & rspaymaster.Tables(0).Rows(0)("VCONV_F") & "'" & _
                            ",VMED_RATE=" & rspaymaster.Tables(0).Rows(0)("VMED_RATE") & ",VMED_F='" & rspaymaster.Tables(0).Rows(0)("VMED_F") & "'" & _
                            ",PF_ALLOWED='" & rspaymaster.Tables(0).Rows(0)("PF_ALLOWED") & "',VPF_ALLOWED='" & rspaymaster.Tables(0).Rows(0)("VPF_ALLOWED") & "'," & _
                            "VOT_RATE=" & rspaymaster.Tables(0).Rows(0)("VOT_RATE") & ",VOT_FLAG='" & rspaymaster.Tables(0).Rows(0)("VOT_FLAG") & "'," & _
                            "VHRA_RATE=" & rspaymaster.Tables(0).Rows(0)("VHRA_RATE") & ",VHRA_F='" & rspaymaster.Tables(0).Rows(0)("VHRA_F") & "',ESI_ALLOWED='" & rspaymaster.Tables(0).Rows(0)("ESI_ALLOWED") & "'," & _
                            "VDT_1='" & rspaymaster.Tables(0).Rows(0)("VDT_1") & "',VD_1=" & rspaymaster.Tables(0).Rows(0)("VD_1") & ",VDT_2='" & rspaymaster.Tables(0).Rows(0)("VDT_2") & "',VD_2=" & rspaymaster.Tables(0).Rows(0)("VD_2") & _
                            ",VDT_3='" & rspaymaster.Tables(0).Rows(0)("VDT_3") & "',VD_3=" & rspaymaster.Tables(0).Rows(0)("VD_3") & ",VDT_4='" & rspaymaster.Tables(0).Rows(0)("VDT_4") & "',VD_4=" & rspaymaster.Tables(0).Rows(0)("VD_4") & _
                            ",VDT_5='" & rspaymaster.Tables(0).Rows(0)("VDT_5") & "',VD_5=" & rspaymaster.Tables(0).Rows(0)("VD_5") & ",VDT_6='" & rspaymaster.Tables(0).Rows(0)("VDT_6") & "',VD_6=" & rspaymaster.Tables(0).Rows(0)("VD_6") & _
                            ",VDT_7='" & rspaymaster.Tables(0).Rows(0)("VDT_7") & "',VD_7=" & rspaymaster.Tables(0).Rows(0)("VD_7") & ",VDT_8='" & rspaymaster.Tables(0).Rows(0)("VDT_8") & "',VD_8=" & rspaymaster.Tables(0).Rows(0)("VD_8") & _
                            ",VDT_9='" & rspaymaster.Tables(0).Rows(0)("VDT_9") & "',VD_9=" & rspaymaster.Tables(0).Rows(0)("VD_9") & ",VDT_10='" & rspaymaster.Tables(0).Rows(0)("VDT_10") & "',VD_10=" & rspaymaster.Tables(0).Rows(0)("VD_10") & _
                            ",VIT_1='" & rspaymaster.Tables(0).Rows(0)("VIT_1") & "',VI_1=" & rspaymaster.Tables(0).Rows(0)("VI_1") & ",VIT_2='" & rspaymaster.Tables(0).Rows(0)("VIT_2") & "',VI_2=" & rspaymaster.Tables(0).Rows(0)("VI_2") & _
                            ",VIT_3='" & rspaymaster.Tables(0).Rows(0)("VIT_3") & "',VI_3=" & rspaymaster.Tables(0).Rows(0)("VI_3") & ",VIT_4='" & rspaymaster.Tables(0).Rows(0)("VIT_4") & "',VI_4=" & rspaymaster.Tables(0).Rows(0)("VI_4") & _
                            ",VIT_5='" & rspaymaster.Tables(0).Rows(0)("VIT_5") & "',VI_5=" & rspaymaster.Tables(0).Rows(0)("VI_5") & ",VIT_6='" & rspaymaster.Tables(0).Rows(0)("VIT_6") & "',VI_6=" & rspaymaster.Tables(0).Rows(0)("VI_6") & _
                            ",VIT_7='" & rspaymaster.Tables(0).Rows(0)("VIT_7") & "',VI_7=" & rspaymaster.Tables(0).Rows(0)("VI_7") & ",VIT_8='" & rspaymaster.Tables(0).Rows(0)("VIT_8") & "',VI_8=" & rspaymaster.Tables(0).Rows(0)("VI_8") & _
                            ",VIT_9='" & rspaymaster.Tables(0).Rows(0)("VIT_9") & "',VI_9=" & rspaymaster.Tables(0).Rows(0)("VI_9") & ",VIT_10='" & rspaymaster.Tables(0).Rows(0)("VIT_10") & "',VI_10=" & rspaymaster.Tables(0).Rows(0)("VI_10") & _
                            ",VTDS_RATE=" & rspaymaster.Tables(0).Rows(0)("VTDS_RATE") & ",VTDS_F='" & rspaymaster.Tables(0).Rows(0)("VTDS_F") & "'" & _
                            ",PROF_TAX_Allowed='" & rspaymaster.Tables(0).Rows(0)("PROF_TAX_Allowed") & "'" & _
                            " where pay_Result.Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("yyyy")
                        'Cn.Execute(sSql)
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                        sSql = "Update Pay_Result Set VOT=" & Min2Hr(Trim(Str(iVOTDuration))) & _
                                ",VT_EARLY=" & iVEarlyDeparture & ",VT_LATE=" & iVLateArrival & _
                                ",VPRE=" & iPresentValue & ",VABS=" & iAbsentValue & _
                                ",VHLD=" & iHolidayValue & ",VCL=" & iVCL & ",VSL=" & iVSL & _
                                ",VPL_EL=" & iVPL_EL & ",VOTHER_LV=" & iVOther_LV & ",VLEAVE=" & iLeaveAmount & _
                                ",VLATE=" & iLateDays & ",VEARLY=" & iEarlyDays & ",VWO=" & iWOValue & ",VTDAYS=" & iTotalDays & _
                                " where pay_Result.Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("yyyy")
                        'Cn.Execute(sSql)
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                    End If
                End If
                'rsCapture.MovePrevious()
                i = i - 1
                If rsCapture.Tables(0).Rows(i)("VEmployeeType") = "P" Then
                    sSql = "Select sum(pamount) as amount from tblpiecedata Where PAYCODE='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("yyyy")
                    rsPiecedata = New DataSet 'Cn.Execute(sSql)
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(rsPiecedata)
                    Else
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(rsPiecedata)
                    End If
                    If rsPiecedata.Tables(0).Rows.Count = 1 Then
                        If Not IsNull(rsPiecedata.Tables(0).Rows(0)("amount")) Then
                            sSql = "Update Pay_Result Set VBASIC=" & rsPiecedata.Tables(0).Rows(0)("amount") & " where pay_Result.Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("yyyy")
                            'Cn.Execute(sSql)
                            If Common.servername = "Access" Then
                                If Common.con1.State <> ConnectionState.Open Then
                                    Common.con1.Open()
                                End If
                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                cmd1.ExecuteNonQuery()
                                If Common.con1.State <> ConnectionState.Closed Then
                                    Common.con1.Close()
                                End If
                            Else
                                If Common.con.State <> ConnectionState.Open Then
                                    Common.con.Open()
                                End If
                                cmd = New SqlCommand(sSql, Common.con)
                                cmd.ExecuteNonQuery()
                                If Common.con.State <> ConnectionState.Closed Then
                                    Common.con.Close()
                                End If
                            End If
                        End If
                    End If
                End If
                'rsCapture.MoveNext()
                'If rsCapture.AbsolutePosition = rsCapture.RecordCount Then
                '    rsCapture.MoveNext()
                'End If
                'rsMonthTable = Nothing
            Next ' Loop
        Else
            DATACAPTURE = False
            'Screen.MousePointer = vbNormal
        End If
        sSql = "delete from Pay_Result where paycode not in (select paycode from pay_master) And DatePart(mm,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & txtapplicablefrom.DateTime.ToString("yyyy")
        'Cn.Execute(sSql)
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        'rsCapture = Nothing
        'rsCheckTable = Nothing
        'rsMonthTable = Nothing        
    End Function
    Function Min2Hr(ByVal mMIN As String) As Double
        If Val(mMIN) >= 0 Then
            Min2Hr = (Int(Val(mMIN) / 60) + ((mMIN Mod 60) / 100))
        Else
            Min2Hr = -(Int(Val(Math.Abs(Convert.ToDouble(mMIN))) / 60) + ((Math.Abs(Convert.ToDouble(mMIN)) Mod 60) / 100))
        End If
    End Function
    Private Sub cmdOk_Click(sender As System.Object, e As System.EventArgs) Handles cmdOk.Click
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim rspaymaster As DataSet
        Dim rsrecordchk As DataSet
        Dim iPFAMT As Double, iEPFAMT As Double, iFPFAMT As Double

        '*** TBLEMPLOYEE
        Dim sSql As String = "update tblemployee set active='N',leavingdate='" & txtapplicablefrom.DateTime.ToString("yyyy-MM-dd") & "',leavingreason='FULL & FINAL ' where paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
        'Cn.Execute(sSql)
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        '*** Gratuity
        sSql = "Select Paycode From PAY_GRATUITY" & " Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
        rsrecordchk = New DataSet 'Cn.Execute(sSql)
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsrecordchk)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsrecordchk)
        End If
        If rsrecordchk.Tables(0).Rows.Count = 0 Then
            sSql = "Insert Into PAY_GRATUITY(PAYCODE) values('" & LookUpEdit1.EditValue.ToString.Trim & "')"
            'Cn.Execute(sSql)
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End If
        sSql = "Update PAY_GRATUITY Set ASONDATE='" & txtapplicablefrom.DateTime.ToString("yyyy-MM-dd") & "',GRATUITY_AMT=" & gratuityAmt & ",PERIOD=" & MYEAR & " Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
        'Cn.Execute(sSql)
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        '*** Leave Encashment
        'sSql = "Select Paycode From PAY_LEAVEENCASHMENT " & " Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "' AND ACC_YEAR='" & Format(txtapplicablefrom.Value, "yyyy") & Format(DateAdd("m", 12, txtapplicablefrom.Value), "yyyy") & "'"
        sSql = "Select Paycode From PAY_LEAVEENCASHMENT " & " Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "' AND ACC_YEAR='" & Format(txtapplicablefrom.DateTime, "yyyy") & Format(DateAdd("m", 12, txtapplicablefrom.DateTime), "yyyy") & "'"
        rsrecordchk = New DataSet 'Cn.Execute(sSql)
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsrecordchk)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsrecordchk)
        End If
        If rsrecordchk.Tables(0).Rows.Count = 0 Then
            sSql = "Insert Into PAY_LEAVEENCASHMENT(PAYCODE,acc_year) values('" & LookUpEdit1.EditValue.ToString.Trim & "','" & Format(txtapplicablefrom.DateTime, "yyyy") & Format(DateAdd("m", 12, txtapplicablefrom.DateTime), "yyyy") & "')"
            'Cn.Execute(sSql)
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End If
        iPFAMT = 0
        iEPFAMT = 0
        iFPFAMT = 0
        sSql = "select * from pay_master where paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
        rspaymaster = New DataSet 'Cn.Execute(sSql)
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rspaymaster)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rspaymaster)
        End If
        If rspaymaster.Tables(0).Rows.Count > 0 Then
            If rspaymaster.Tables(0).Rows(0)("PF_ALLOWED") = "Y" And rsPaysetup.Tables(0).Rows(0)("leavePF") = "Y" Then
                iPFAMT = Math.Round((IIf(rsPaysetup.Tables(0).Rows(0)("PFFIXED") = "Y", IIf(Val(txtLeaveInCash) > rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), Val(txtLeaveInCash)), Val(txtLeaveInCash)) * rsPaysetup.Tables(0).Rows(0)("PF")) / 100, rsPaysetup.Tables(0).Rows(0)("PFRND"))
                iEPFAMT = Math.Round((IIf(Val(txtLeaveInCash) > rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), Val(txtLeaveInCash)) * rsPaysetup.Tables(0).Rows(0)("EPF")) / 100, rsPaysetup.Tables(0).Rows(0)("PFRND"))
                iFPFAMT = iPFAMT - iEPFAMT
            End If
        End If
        sSql = "Update PAY_LEAVEENCASHMENT Set ENCASHMENT_AMT=" & txtLeaveInCash.Text.Trim & ",PAYED_MONTH='" & Format(txtPayableMonth.DateTime, "MM/yyyy") & "',VPF_AMT=" & iPFAMT & ",VEPF_AMT=" & iEPFAMT & ",VFPF_AMT=" & iFPFAMT & " Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "' AND ACC_YEAR='" & Format(txtapplicablefrom.DateTime, "yyyy") & Format(DateAdd("m", 12, txtapplicablefrom.DateTime), "yyyy") & "'"
        'Cn.Execute(sSql)
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        If Len(Trim(strLVcash)) > 0 Then
            sSql = "UPDATE tblLeaveledger set " & strLVcash & " Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "' and lyear=" & Year(txtapplicablefrom.DateTime)
            'Cn.Execute(sSql)
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            sSql = "UPDATE PAY_LEAVEENCASHMENT set " & strLVcash & " Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "' AND ACC_YEAR='" & Format(txtapplicablefrom.DateTime, "yyyy") & Format(DateAdd("m", 12, txtapplicablefrom.DateTime), "yyyy") & "'"
            'Cn.Execute(sSql)
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End If

        '*** Full and Final
        sSql = "Insert Into PAY_FULLFINAL(PAYCODE) values('" & LookUpEdit1.EditValue.ToString.Trim & "')"
        'Cn.Execute(sSql)
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        sSql = "Update PAY_FULLFINAL Set VCONV_AMT=" & txtEarningConv.Text.Trim & ",VMED_AMT=" & txtEarningMed.Text.Trim & _
                     ",VI_1_AMT=" & txtEarningEarn1.Text.Trim & ",VI_2_AMT=" & txtEarningEarn2.Text.Trim & _
                     ",VI_3_AMT=" & txtEarningEarn3.Text.Trim & ",VI_4_AMT=" & txtEarningEarn4.Text.Trim & _
                     ",VI_5_AMT=" & txtEarningEarn5.Text.Trim & ",VI_6_AMT=" & txtEarningEarn6.Text.Trim & _
                     ",VI_7_AMT=" & txtEarningEarn7.Text.Trim & ",VI_8_AMT=" & txtEarningEarn8.Text.Trim & _
                     ",VI_9_AMT=" & txtEarningEarn9.Text.Trim & ",VI_10_AMT=" & txtEarningEarn10.Text.Trim & _
                     ",VNETSALARY=" & txtNetPay.Text.Trim & ",LEAVING_DATE='" & txtapplicablefrom.DateTime.ToString("yyyy-MM-dd") & "'" & _
                     ",VLEAVE=" & txtLeave.Text.Trim & ",VLEAVE_INCASH=" & txtLeaveInCash.Text.Trim & _
                     ",VGRATUITY_DUR=" & MYEAR & ",VGRATUITY_AMT=" & gratuityAmt & _
                     ",VNOTICE_MONTH=" & txtNoticeMonth.Text.Trim & ",vNOTICE_AMT=" & txtNoticePay.Text.Trim & _
                     ",loan_AMT=" & txtLoan.Text.Trim & ",Advance_AMT=" & txtAdvance.Text.Trim & _
                     ",mon_year='" & txtapplicablefrom.DateTime.ToString("yyyy-MM-01") & "'" & _
                     " Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
        'Cn.Execute(sSql)
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        If strLVcash <> "" Then
            sSql = "UPDATE PAY_FULLFINAL set " & strLVcash & " Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
            'Cn.Execute(sSql)
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End If
        txtGratuity.Text = "000000.00"
        txtLeaveInCash.Text = "000000.00"
        txtNoticePay.Text = "000000.00"
        txtAdvance.Text = "000000.00"
        txtLoan.Text = "000000.00"
        txtEarningConv.Text = "000000.00"
        txtEarningMed.Text = "000000.00"
        txtEarningEarn1.Text = "000000.00"
        txtEarningEarn2.Text = "000000.00"
        txtEarningEarn3.Text = "000000.00"
        txtEarningEarn4.Text = "000000.00"
        txtEarningEarn5.Text = "000000.00"
        txtEarningEarn6.Text = "000000.00"
        txtEarningEarn7.Text = "000000.00"
        txtEarningEarn8.Text = "000000.00"
        txtEarningEarn9.Text = "000000.00"
        txtEarningEarn10.Text = "000000.00"
        txtNetPay.Text = "000000.00"
        txtLeave.Text = "00000.00"
        txtNoticeMonth.Text = "000"
        'txtPaycodeFrom.Text = ""
        btnApply.Enabled = False
        cmdOk.Enabled = False
        'txtPaycodeFrom.SetFocus()
        'rspaymaster.Close()
        'rsrecordchk.Close()
        setDefault()
    End Sub
    Private Sub PopupContainerEditLeave_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PopupContainerEditLeave.EditValueChanged
        Dim TotallftLEave As Double = 0
        Dim selectedRows As Integer() = GridViewLeave.GetSelectedRows()
        For i As Integer = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            TotallftLEave = TotallftLEave + GridViewLeave.GetRowCellValue(rowHandle, "LeaveLeft")
        Next
        txtLeave.Text = TotallftLEave
    End Sub
End Class
