﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Base
Imports ULtra.HSeriesSampleCSharp
Imports System.Runtime.InteropServices
Imports CMITech.UMXClient
Imports ULtra.AscDemo
Imports System.Threading
Imports Newtonsoft.Json

Public Class XtraEmployeeCopyToDevice

    Public Delegate Sub invokeDelegate()
    Dim serialNo As String = ""
    Dim lUserID As Integer = -1
    Dim m_lGetCardCfgHandle As Int32 = -1 'Hikvision
    Dim g_fGetGatewayCardCallback As CHCNetSDK.RemoteConfigCallback = Nothing 'Hikvision
    Dim m_lGetFingerPrintCfgHandle As Integer = -1
    Dim HCardNos As List(Of String) = Nothing
    Dim iMaxCardNum As Integer = 1000
    Dim m_struFingerPrintOne As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50
    Dim m_struDelFingerPrint As CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_CTRL_BYCARD_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_CTRL_BYCARD_V50
    Dim m_struRecordCardCfg() As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50((iMaxCardNum) - 1) {}
    Dim m_struCardInfo() As CHCNetSDK.NET_DVR_CARD_CFG_V50 = New CHCNetSDK.NET_DVR_CARD_CFG_V50((iMaxCardNum) - 1) {}
    Dim m_lUserID As Integer = -1
    Dim g_fSetFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fGetFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fDelFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fSetGatewayCardCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim m_lSetCardCfgHandle As Int32 = -1
    Dim m_bSendOne As Boolean = False
    Dim m_struSelSendCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50
    Dim m_struNowSendCard As CHCNetSDK.NET_DVR_CARD_CFG_V50
    Dim m_BSendSel As Boolean = False
    Dim m_dwCardNum As UInteger = 0
    Dim m_dwSendIndex As UInteger = 0
    Dim m_lSetFingerPrintCfgHandle As Integer = -1
    Dim m_iSendIndex As Integer = -1

    Dim Downloading As Boolean = False

    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand

    Dim DC As New DateConverter()

    Const PWD_DATA_SIZE = 40
    Const FP_DATA_SIZE = 1680
    Const FACE_DATA_SIZE = 20080
    Const VEIN_DATA_SIZE = 3080
    Private mbytEnrollData(FACE_DATA_SIZE - 1) As Byte
    Private mbytPhotoImage() As Byte

    Private mnCommHandleIndex As Long
    Private mbGetState As Boolean
    Private mlngPasswordData As Long

    Dim vpszIPAddress As String
    Dim vpszNetPort As Long
    Dim vpszNetPassword As Long
    Dim vnTimeOut As Long
    Dim vnProtocolType As Long
    Dim strDateTime As String
    Dim vnResult As Long
    Dim vnLicense As Long
    '//=============== Backup Number Constant ===============//
    'Public Const BACKUP_FP_0 = 0                  ' Finger 0
    'Public Const BACKUP_FP_1 = 1                  ' Finger 1
    'Public Const BACKUP_FP_2 = 2                  ' Finger 2
    'Public Const BACKUP_FP_3 = 3                  ' Finger 3
    'Public Const BACKUP_FP_4 = 4                  ' Finger 4
    'Public Const BACKUP_FP_5 = 5                  ' Finger 5
    'Public Const BACKUP_FP_6 = 6                  ' Finger 6
    'Public Const BACKUP_FP_7 = 7                  ' Finger 7
    'Public Const BACKUP_FP_8 = 8                  ' Finger 8
    'Public Const BACKUP_FP_9 = 9                  ' Finger 9
    'Public Const BACKUP_PSW = 10                  ' Password
    Public Const BACKUP_CARD = 11                 ' Card
    Public Const BACKUP_FACE = 12                 ' Face
    Public Const BACKUP_VEIN_0 = 20               ' Vein 0

    Dim m_lSetFaceCfgHandle As Integer = -1
    Dim m_lSetUserCfgHandle As Int32 = -1

    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
        Common.SetGridFont(GridView2, New Font("Tahoma", 10))
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub XtraEmployeeCopyToDevice_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'If Common.servername = "Access" Then
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine1
        '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        '    GridControl2.DataSource = SSSDBDataSet.TblEmployee1
        'Else
        '    TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine
        '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        '    GridControl2.DataSource = SSSDBDataSet.TblEmployee
        'End If
        GridControl1.DataSource = Common.MachineNonAdmin
        GridControl2.DataSource = Common.EmpNonAdmin

        DateEditStart.DateTime = "2018-01-01 00:00:00"
        DateEditEnd.DateTime = "2029-12-31 23:59:59"
        If Common.IsNepali = "Y" Then
            DateEditStart.Visible = False
            DateEditEnd.Visible = False
            ComboNepaliVStartDate.Visible = True
            ComboNepaliVStartMonth.Visible = True
            ComboNepaliVStartYear.Visible = True

            ComboNepaliVEndDate.Visible = True
            ComboNepaliVEndMonth.Visible = True
            ComboNepaliVEndYear.Visible = True

            Dim Vstart As String = DC.ToBS(New Date(DateEditStart.DateTime.Year, DateEditStart.DateTime.Month, DateEditStart.DateTime.Day))
            Dim dojTmp() As String = Vstart.Split("-")
            ComboNepaliVStartYear.EditValue = dojTmp(0)
            ComboNepaliVStartMonth.SelectedIndex = dojTmp(1) - 1
            ComboNepaliVStartDate.EditValue = dojTmp(2)


            Dim VEnd As String = DC.ToBS(New Date(DateEditEnd.DateTime.Year, DateEditEnd.DateTime.Month, DateEditEnd.DateTime.Day))
            dojTmp = VEnd.Split("-")
            ComboNepaliVEndYear.EditValue = dojTmp(0)
            ComboNepaliVEndMonth.SelectedIndex = dojTmp(1) - 1
            ComboNepaliVEndDate.EditValue = dojTmp(2)


            ComboNepaliVStartDate.Enabled = False
            ComboNepaliVStartMonth.Enabled = False
            ComboNepaliVStartYear.Enabled = False

            ComboNepaliVEndDate.Enabled = False
            ComboNepaliVEndMonth.Enabled = False
            ComboNepaliVEndYear.Enabled = False

        Else
            DateEditStart.Visible = True
            DateEditEnd.Visible = True
            ComboNepaliVStartDate.Visible = False
            ComboNepaliVStartMonth.Visible = False
            ComboNepaliVStartYear.Visible = False

            ComboNepaliVEndDate.Visible = False
            ComboNepaliVEndMonth.Visible = False
            ComboNepaliVEndYear.Visible = False
        End If
        'CheckCreate.Checked = True
        'CheckCreate.Enabled = False
        CheckEdit1.Checked = False
        CheckFrmDt.Checked = True
        GridView1.ClearSelection()
        GridView2.ClearSelection()
        CheckEditCopyCard.Checked = False
    End Sub
    Private Sub GridView2_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView2.CustomColumnDisplayText
        Try
            Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "FingerNumber" Then
                If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim >= 0 And _
                    view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim < 10 Then
                    e.DisplayText = "FP-" & view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim + 1
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 12 Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 50 Then
                    e.DisplayText = "FACE"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 11 Then
                    e.DisplayText = "CARD"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 10 Then
                    e.DisplayText = "PASSWORD"
                End If
                e.DisplayText = (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim \ 60).ToString("00") & ":" & (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim Mod 60).ToString("00")
            End If
        Catch
        End Try
        Try
            Dim adapA As OleDbDataAdapter
            Dim adap As SqlDataAdapter
            Dim ds As DataSet
            Dim sSql As String
            Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "UserName" Then
                sSql = "SELECT EMPNAME from TblEmployee where PRESENTCARDNO = '" & view.GetListSourceRowCellValue(e.ListSourceRowIndex, "EnrollNumber").ToString().Trim & "'"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    e.DisplayText = ds.Tables(0).Rows(0).Item(0).ToString
                Else
                    e.DisplayText = ""
                End If
            End If

            If e.Column.Caption = "Device Type" Then
                If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Template").ToString().Trim = "" Then
                    e.DisplayText = "Bio"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Template_Tw").ToString().Trim = "" Then
                    e.DisplayText = "ZK"
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
    Private Sub btnUploadFinger_Click(sender As System.Object, e As System.EventArgs) Handles btnUploadFinger.Click
        Dim m As Integer
        'Dim e As Integer
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim mKey As String
        Dim vnMachineNumber As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long
        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim strDateTime As String
        Dim vRet As Long
        Dim nCommHandleIndex As Long
        Dim vPrivilege As Long
        Dim failIP As New List(Of String)()
        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If GridView2.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Employee</size>", "<size=9>Error</size>")
            Exit Sub
        End If

        If Common.IsNepali = "Y" Then
            Try
                'DateEditStart.DateTime = DC.ToAD(New Date(ComboNepaliVStartYear.EditValue, ComboNepaliVStartMonth.SelectedIndex + 1, ComboNepaliVStartDate.EditValue))
                DateEditStart.DateTime = DC.ToAD(ComboNepaliVStartYear.EditValue & "-" & ComboNepaliVStartMonth.SelectedIndex + 1 & "-" & ComboNepaliVStartDate.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Validity Start Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliVStartDate.Select()
                Exit Sub
            End Try
            Try
                'DateEditEnd.DateTime = DC.ToAD(New Date(ComboNepaliVEndYear.EditValue, ComboNepaliVEndMonth.SelectedIndex + 1, ComboNepaliVEndDate.EditValue))
                DateEditEnd.DateTime = DC.ToAD(ComboNepaliVEndYear.EditValue & "-" & ComboNepaliVEndMonth.SelectedIndex + 1 & "-" & ComboNepaliVEndDate.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Validity End Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliVEndDate.Select()
                Exit Sub
            End Try
        End If
        Dim sSql As String = ""
        Me.Cursor = Cursors.WaitCursor
        Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim LstMachineId As String
        Dim commkey As Integer
        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridView1.IsGroupRow(rowHandle) Then
                LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                vpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim
                If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then

                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then

                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "HKSeries" Then
                    Dim cn As Common = New Common
                    Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                    Dim DeviceAdd As String = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim, userName As String = Trim(GridView1.GetRowCellValue(rowHandle, "HLogin").ToString.Trim), pwd As String = Trim(GridView1.GetRowCellValue(rowHandle, "HPassword").ToString.Trim)
                    Dim lUserID As Integer = -1
                    Dim failReason As String = ""
                    Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
                    If logistatus = False Then
                        If failReason <> "" Then
                            XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                            XtraMasterTest.LabelControlStatus.Text = ""
                            Application.DoEvents()
                            Continue For
                        End If
                    Else
                        If failReason <> "" Then
                            XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                            XtraMasterTest.LabelControlStatus.Text = ""
                            Application.DoEvents()
                            Me.Cursor = Cursors.Default
                            logistatus = cn.HikvisionLogOut(lUserID)
                            Continue For
                        End If

                        XtraMasterTest.LabelControlStatus.Text = ""
                        Application.DoEvents()

                        If (-1 <> m_lSetCardCfgHandle) Then
                            If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetCardCfgHandle) Then
                                m_lSetCardCfgHandle = -1
                            End If
                        End If
                        m_bSendOne = True
                        'loop for fingerprint
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        'If axCZKEM1.BeginBatchUpdate(iMachineNumber, 1) Then
                        For x As Integer = 0 To selectedRowsEmp.Length - 1  'for user
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim RsFp As DataSet = New DataSet
                                Dim mUser_ID As String = GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString.Trim()
                                Dim tmpUserId As String
                                If IsNumeric(mUser_ID) Then
                                    tmpUserId = Convert.ToDouble(mUser_ID).ToString("000000000000")
                                Else
                                    tmpUserId = mUser_ID
                                End If
                                XtraMasterTest.LabelControlStatus.Text = "Uploading User " & mUser_ID
                                Application.DoEvents()

                                'If GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim() = "11" Then
                                'for user copy only
                                Dim s As String = "select EMPNAME, ValidityStartdate, ValidityEnddate from TblEmployee where PRESENTCARDNO = '" & tmpUserId & "'"
                                Dim ValidityStartdate As DateTime
                                Dim ValidityEnddate As DateTime

                                Dim sName As String = GridView2.GetRowCellValue(rowHandleEmp, "EMPNAME").ToString().Trim()
                                Dim sdwEnrollNumber As String
                                If IsNumeric(mUser_ID) = True Then
                                    sdwEnrollNumber = Convert.ToDouble(mUser_ID)
                                Else
                                    sdwEnrollNumber = mUser_ID
                                End If
                                If CheckEdit1.Checked = True Then
                                    If CheckFrmDt.Checked = True Then
                                        ValidityStartdate = DateEditStart.DateTime ' Convert.ToDateTime("2018-01-01")
                                        ValidityEnddate = DateEditEnd.DateTime 'Convert.ToDateTime("2030-12-31")
                                    ElseIf CheckFrmEmp.Checked = True Then
                                        ValidityStartdate = Convert.ToDateTime(GridView2.GetRowCellValue(rowHandleEmp, "ValidityStartdate").ToString().Trim())
                                        ValidityEnddate = Convert.ToDateTime(GridView2.GetRowCellValue(rowHandleEmp, "ValidityEnddate").ToString().Trim())
                                    End If
                                Else
                                    ValidityStartdate = Convert.ToDateTime("2018-01-01")
                                    ValidityEnddate = Convert.ToDateTime("2030-12-31")
                                End If

                                If (m_lSetUserCfgHandle <> -1) Then
                                    If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetUserCfgHandle) Then
                                        m_lSetUserCfgHandle = -1
                                    End If
                                End If
                                Dim sURL As String = "PUT /ISAPI/AccessControl/UserInfo/SetUp?format=json"
                                Dim ptrURL As IntPtr = Marshal.StringToHGlobalAnsi(sURL)
                                m_lSetUserCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_JSON_CONFIG, ptrURL, sURL.Length, Nothing, IntPtr.Zero)
                                If (m_lSetUserCfgHandle < 0) Then
                                    'MessageBox.Show(("NET_DVR_StartRemoteConfig fail [url:PUT /ISAPI/AccessControl/UserInfo/SetUp?format=json] error:" + CHCNetSDK.NET_DVR_GetLastError))
                                    Marshal.FreeHGlobal(ptrURL)
                                    Continue For
                                Else
                                    SendUserInfo(sdwEnrollNumber, sName, ValidityStartdate, ValidityEnddate)
                                    Marshal.FreeHGlobal(ptrURL)
                                End If
                            End If
                        Next
                    End If
                End If
            End If
        Next i
        Me.Cursor = Cursors.Default
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()

        Dim failedIpArr() As String = failIP.ToArray
        Dim failIpStr As String = ""
        If failedIpArr.Length > 0 Then
            For i As Integer = 0 To failedIpArr.Length - 1
                failIpStr = failIpStr & "," & failedIpArr(i)
            Next
            failIpStr = failIpStr.TrimStart(",")
            XtraMessageBox.Show(ulf, "<size=10>" & failIpStr & " failed to download data</size>", "<size=9>Failed</size>")
        Else
            XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Success</size>")
        End If
        'XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
    End Sub
    Private Sub SendUserInfo(sdwEnrollNumber As String, sName As String, ValidityStartdate As DateTime, ValidityEnddate As DateTime)
        Dim JsonUserInfo As CUserInfoCfg = New CUserInfoCfg()
        JsonUserInfo.UserInfo = New CUserInfo()
        JsonUserInfo.UserInfo.employeeNo = sdwEnrollNumber
        JsonUserInfo.UserInfo.name = sName
        JsonUserInfo.UserInfo.userType = "normal"
        JsonUserInfo.UserInfo.Valid = New CValid()
        JsonUserInfo.UserInfo.Valid.enable = True
        JsonUserInfo.UserInfo.Valid.beginTime = ValidityStartdate.ToString("yyyy-MM-ddTHH:mm:ss") ' "2017-08-01T17:30:08"
        JsonUserInfo.UserInfo.Valid.endTime = ValidityEnddate.ToString("yyyy-MM-ddTHH:mm:ss") '"2020-08-01T17:30:08"
        JsonUserInfo.UserInfo.Valid.timeType = "local"
        JsonUserInfo.UserInfo.RightPlan = New List(Of CRightPlan)()
        Dim JsonRightPlan As CRightPlan = New CRightPlan()
        JsonRightPlan.doorNo = 1
        JsonRightPlan.planTemplateNo = 1 'textBoxRightPlan.Text
        JsonUserInfo.UserInfo.RightPlan.Add(JsonRightPlan)
        'JsonUserInfo.UserInfo.userVerifyMode = "card"
        JsonUserInfo.UserInfo.floorNumber = 0  'nitin
        JsonUserInfo.UserInfo.roomNumber = 0 'nitin
        'If Privilege = 1 Then
        '    JsonUserInfo.UserInfo.localUIRight = True  'admin rights
        'Else
        JsonUserInfo.UserInfo.localUIRight = False   'admin rights
        'End If

        JsonUserInfo.UserInfo.doorRight = "1" 'nitin
        Dim strJsonUserInfo As String = JsonConvert.SerializeObject(JsonUserInfo, Formatting.Indented, New JsonSerializerSettings With {
            .DefaultValueHandling = DefaultValueHandling.Ignore
        })
        Dim ptrJsonUserInfo As IntPtr = Marshal.StringToHGlobalAnsi(strJsonUserInfo)
        Dim ptrJsonData As IntPtr = Marshal.AllocHGlobal(1024)

        For i As Integer = 0 To 1024 - 1
            Marshal.WriteByte(ptrJsonData, i, 0)
        Next

        Dim dwState As Integer = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS)
        Dim dwReturned As UInteger = 0

        While True
            dwState = CHCNetSDK.NET_DVR_SendWithRecvRemoteConfig(m_lSetUserCfgHandle, ptrJsonUserInfo, CUInt(strJsonUserInfo.Length), ptrJsonData, 1024, dwReturned)
            Dim strJsonData As String = Marshal.PtrToStringAnsi(ptrJsonData)

            If dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_NEEDWAIT) Then
                Thread.Sleep(10)
                Continue While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FAILED) Then
                'MessageBox.Show("Set User Fail error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS) Then
                Dim JsonResponseStatus As CResponseStatus = New CResponseStatus()
                JsonResponseStatus = JsonConvert.DeserializeObject(Of CResponseStatus)(strJsonData)

                If JsonResponseStatus.statusCode = 1 Then
                    'MessageBox.Show("Set User Success")
                Else
                    'MessageBox.Show("Set User Fail, ResponseStatus.statusCode" & JsonResponseStatus.statusCode)
                End If

                Exit While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FINISH) Then
                'MessageBox.Show("Set User Finish")
                Exit While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_EXCEPTION) Then
                'MessageBox.Show("Set User Exception error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            Else
                'MessageBox.Show("unknown Status error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            End If
        End While

        If m_lSetUserCfgHandle <> -1 Then
            If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetUserCfgHandle) Then
                m_lSetUserCfgHandle = -1
            End If
        End If

        Marshal.FreeHGlobal(ptrJsonUserInfo)
        Marshal.FreeHGlobal(ptrJsonData)
    End Sub
    'Private Sub ProcessSetGatewayCardCallback(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
    '    If (pUserData = Nothing) Then
    '        Return
    '    End If

    '    Dim dwStatus As UInteger = CType(Marshal.ReadInt32(lpBuffer), UInteger)
    '    If (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_PROCESSING, UInteger)) Then
    '        ' just example need refinement
    '        Dim listItem As ListViewItem = New ListViewItem
    '        listItem.Text = "SUCC"
    '        Dim strTemp As String = Nothing
    '        If m_bSendOne Then
    '            'strTemp = String.Format("Send SUCC,CardNO:{0}", System.Text.Encoding.UTF8.GetString(m_struCardInfo(m_iSelectIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
    '            m_bSendOne = False
    '        Else
    '            'strTemp = String.Format("Send SUCC,CardNO:{0}", System.Text.Encoding.UTF8.GetString(m_struCardInfo(m_dwSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
    '        End If

    '        'listItem.SubItems.Add(strTemp)
    '        'Me.AddList(listViewMessage, listItem, True)
    '        'next
    '        CHCNetSDK.PostMessage(pUserData, 1002, 0, 0)
    '    ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_FAILED, UInteger)) Then
    '        Dim listItem As ListViewItem = New ListViewItem
    '        listItem.Text = "FAIL"
    '        Dim strTemp As String = Nothing
    '        Dim errorCode As UInteger = CType(Marshal.ReadInt32((lpBuffer + 4)), UInteger)
    '        Dim errorUserID As UInteger = CType(Marshal.ReadInt32((lpBuffer + 40)), UInteger)
    '        'strTemp = String.Format("Send FAILED,CardNO:{0},Error code{1},USER ID{2}", System.Text.Encoding.UTF8.GetString(m_struSelSendCardCfg.byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), errorCode, errorUserID)
    '        'listItem.SubItems.Add(strTemp)
    '        'Me.AddList(listViewMessage, listItem, True)
    '        'next
    '        Dim x As Integer = CHCNetSDK.NET_DVR_GetLastError
    '        Dim tmp As String = m_struSelSendCardCfg.dwEmployeeNo
    '        CHCNetSDK.PostMessage(pUserData, 1002, 0, 0)
    '    ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_SUCCESS, UInteger)) Then
    '        Dim listItem As ListViewItem = New ListViewItem
    '        listItem.Text = "SUCC"
    '        listItem.SubItems.Add("NET_DVR_SET_CARD_CFG_V50 Set finish")
    '        'Me.AddList(listViewMessage, listItem, True)
    '        CHCNetSDK.PostMessage(pUserData, 1001, 0, 0)
    '    ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_EXCEPTION, UInteger)) Then
    '        Dim listItem As ListViewItem = New ListViewItem
    '        listItem.Text = "FAIL"
    '        listItem.SubItems.Add("NET_DVR_SET_CARD_CFG_V50 Set FAIL")
    '        'Me.AddList(listViewMessage, listItem, True)
    '        CHCNetSDK.PostMessage(pUserData, 1001, 0, 0)
    '    End If

    '    Return
    'End Sub
    'Private Function GetSelItem(ByVal rowHandleEmp As Integer, ValidityStartdate As DateTime, ValidityEnddate As DateTime, sName As String, MachineCard As String) As CHCNetSDK.NET_DVR_CARD_CFG_V50
    '    Dim iPos As Integer = 0
    '    'If (rowHandleEmp >= 0) Then
    '    '    iPos = rowHandleEmp
    '    'End If

    '    Dim struCardInfo As CHCNetSDK.NET_DVR_CARD_CFG_V50 = New CHCNetSDK.NET_DVR_CARD_CFG_V50
    '    'If (iPos < 0) Then
    '    '    Return struCardInfo
    '    'End If

    '    struCardInfo.byCardNo = New Byte((32) - 1) {}
    '    struCardInfo.byName = New Byte((32) - 1) {}

    '    struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H80 'card password
    '    struCardInfo.byCardPassword = New Byte((8) - 1) {}

    '    struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H40 'Belongs to group parameters 
    '    struCardInfo.byBelongGroup = New Byte((128) - 1) {}

    '    struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H8  'DoorLimitParameter
    '    struCardInfo.byDoorRight = New Byte((256) - 1) {}
    '    struCardInfo.byLockCode = New Byte((8) - 1) {}
    '    struCardInfo.byRes2 = New Byte((3) - 1) {}
    '    struCardInfo.byRes3 = New Byte((83) - 1) {}
    '    struCardInfo.byRoomCode = New Byte((8) - 1) {}

    '    struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H100 'Card authority scheme
    '    struCardInfo.wCardRightPlan = New UShort((1024) - 1) {}
    '    struCardInfo.wCardRightPlan(0) = 1


    '    struCardInfo.struValid.byEnable = 1
    '    struCardInfo.struValid.byRes1 = New Byte((3) - 1) {}
    '    struCardInfo.struValid.byRes2 = New Byte((32) - 1) {}

    '    struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H2 'for validity
    '    struCardInfo.struValid.struBeginTime.wYear = ValidityStartdate.Year
    '    struCardInfo.struValid.struBeginTime.byMonth = ValidityStartdate.Month
    '    struCardInfo.struValid.struBeginTime.byDay = ValidityStartdate.Day
    '    struCardInfo.struValid.struBeginTime.byHour = ValidityStartdate.Hour
    '    struCardInfo.struValid.struBeginTime.byMinute = ValidityStartdate.Minute
    '    struCardInfo.struValid.struBeginTime.bySecond = ValidityStartdate.Second

    '    struCardInfo.struValid.struEndTime.wYear = ValidityEnddate.Year
    '    struCardInfo.struValid.struEndTime.byMonth = ValidityEnddate.Month
    '    struCardInfo.struValid.struEndTime.byDay = ValidityEnddate.Day
    '    struCardInfo.struValid.struEndTime.byHour = ValidityEnddate.Hour
    '    struCardInfo.struValid.struEndTime.byMinute = ValidityEnddate.Minute
    '    struCardInfo.struValid.struEndTime.bySecond = ValidityEnddate.Second


    '    struCardInfo.dwSize = CType(Marshal.SizeOf(struCardInfo), UInteger)
    '    Dim password As String = "" 'GridView2.GetRowCellValue(rowHandleEmp, "Password").ToString.Trim()
    '    Dim User_ID As String = GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString.Trim()
    '    Dim sdwEnrollNumber As String
    '    If IsNumeric(User_ID) = True Then
    '        sdwEnrollNumber = Convert.ToDouble(User_ID)
    '    Else
    '        sdwEnrollNumber = User_ID
    '    End If
    '    Dim byTemp() As Byte = System.Text.Encoding.UTF8.GetBytes(MachineCard)
    '    Dim y As Integer = 0
    '    Do While (y < byTemp.Length)
    '        struCardInfo.byCardNo(y) = byTemp(y)
    '        y = (y + 1)
    '    Loop

    '    byTemp = System.Text.Encoding.UTF8.GetBytes(sName)
    '    y = 0
    '    Do While (y < byTemp.Length)
    '        struCardInfo.byName(y) = byTemp(y)
    '        y = (y + 1)
    '    Loop

    '    byTemp = System.Text.Encoding.UTF8.GetBytes(password)
    '    y = 0
    '    Do While (y < byTemp.Length)
    '        struCardInfo.byCardPassword(y) = byTemp(y)
    '        y = (y + 1)
    '    Loop
    '    'struCardInfo.byCardType = 1
    '    struCardInfo.byCardValid = 1
    '    struCardInfo.byLeaderCard = 0
    '    struCardInfo.bySchedulePlanType = 2
    '    struCardInfo.dwEmployeeNo = sdwEnrollNumber
    '    struCardInfo.wDepartmentNo = 1
    '    struCardInfo.wSchedulePlanNo = 1
    '    struCardInfo.byDoorRight(0) = 1
    '    'new values hardcoded
    '    struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H4  'for card type
    '    struCardInfo.byCardType = CByte((1))

    '    Short.TryParse(0, struCardInfo.wFloorNumber)

    '    'struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H10 'FirstCardParameters
    '    'struCardInfo.byLeaderCard = 1

    '    'struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H20 'The maximum swiping card number
    '    'UInteger.TryParse(textBoxMaximumCreditCard.Text, m_struCardCfg.dwMaxSwipeTime)

    '    'struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H200  'Swiped number
    '    'UInteger.TryParse(textBoxreditCard.Text, m_struCardCfg.dwSwipeTime)

    '    m_struCardInfo(0) = struCardInfo
    '    Return m_struCardInfo(0)
    'End Function
    'Private Sub SendCardData(ByVal struCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50, Optional ByVal dwDiffTime As UInteger = 0)
    '    If (-1 = m_lSetCardCfgHandle) Then
    '        Return
    '    End If
    '    Dim dwSize As UInteger = CType(Marshal.SizeOf(struCardCfg), UInteger)
    '    Dim ptrStruCard As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Integer))
    '    Marshal.StructureToPtr(struCardCfg, ptrStruCard, False)
    '    If Not CHCNetSDK.NET_DVR_SendRemoteConfig(m_lSetCardCfgHandle, 3, ptrStruCard, dwSize) Then
    '        Marshal.FreeHGlobal(ptrStruCard)
    '        Return
    '    End If
    '    Marshal.FreeHGlobal(ptrStruCard)
    '    Return
    'End Sub
    Private Sub CheckEdit1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEdit1.CheckedChanged
        If CheckFrmDt.Checked = True Then
            PanelControl1.Enabled = True
            If Common.IsNepali = "Y" Then
                If CheckEdit1.Checked = True Then
                    ComboNepaliVStartDate.Enabled = True
                    ComboNepaliVStartMonth.Enabled = True
                    ComboNepaliVStartYear.Enabled = True

                    ComboNepaliVEndDate.Enabled = True
                    ComboNepaliVEndMonth.Enabled = True
                    ComboNepaliVEndYear.Enabled = True
                Else
                    ComboNepaliVStartDate.Enabled = False
                    ComboNepaliVStartMonth.Enabled = False
                    ComboNepaliVStartYear.Enabled = False

                    ComboNepaliVEndDate.Enabled = False
                    ComboNepaliVEndMonth.Enabled = False
                    ComboNepaliVEndYear.Enabled = False
                End If
            Else
                If CheckEdit1.Checked = True Then
                    DateEditStart.Enabled = True
                    DateEditEnd.Enabled = True
                    'CheckCreate.Enabled = True
                Else
                    DateEditStart.Enabled = False
                    DateEditEnd.Enabled = False
                    'CheckCreate.Enabled = False
                End If
            End If
        Else
            PanelControl1.Enabled = False
        End If
    End Sub
    Private Sub CheckFrmDt_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckFrmDt.CheckedChanged
        If CheckFrmDt.Checked = True Then
            PanelControl1.Enabled = True
        Else
            PanelControl1.Enabled = False
        End If
    End Sub

End Class