﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraReportNew
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraReportNew))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.SidePanel3 = New DevExpress.XtraEditors.SidePanel()
        Me.PopupContainerControlEmp = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlEmp = New DevExpress.XtraGrid.GridControl()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New ULtra.SSSDBDataSet()
        Me.GridViewEmp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPAYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlDept = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlDept = New DevExpress.XtraGrid.GridControl()
        Me.TblDepartmentBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewDept = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colDEPARTMENTCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEPARTMENTNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.SimpleButton5 = New DevExpress.XtraEditors.SimpleButton()
        Me.PopupContainerControlComp = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlComp = New DevExpress.XtraGrid.GridControl()
        Me.TblCompanyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewComp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCOMPANYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SidePanel2 = New DevExpress.XtraEditors.SidePanel()
        Me.PopupContainerControlGrade = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlGrade = New DevExpress.XtraGrid.GridControl()
        Me.TblGradeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewGrade = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit6 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlShift = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlShift = New DevExpress.XtraGrid.GridControl()
        Me.TblShiftMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewShift = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colSHIFT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSTARTTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.colENDTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit5 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlCat = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlCat = New DevExpress.XtraGrid.GridControl()
        Me.TblCatagoryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewCat = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCATAGORYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit4 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.CheckOverTimeSumm = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckShiftWiseAtt = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckDeptWiseAtt = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEmpWiseAtt = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEmpPerformance = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckShiftSchedule = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckOverStay = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckOverTime = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckAbsenteeism = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEarlyDeparture = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckLateArrival = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckPerformance = New DevExpress.XtraEditors.CheckEdit()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditGrade = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditSection = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditShift = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditCat = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditDept = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditComp = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.PopupContainerEditEmp = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit2 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblCompanyTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblCompanyTableAdapter()
        Me.TblCompany1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblCompany1TableAdapter()
        Me.TblEmployeeTableAdapter = New ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblEmployee1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        Me.TblDepartmentTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter()
        Me.TblDepartment1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter()
        Me.TblCatagoryTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblCatagoryTableAdapter()
        Me.TblCatagory1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblCatagory1TableAdapter()
        Me.TblShiftMasterTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblShiftMasterTableAdapter()
        Me.TblShiftMaster1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblShiftMaster1TableAdapter()
        Me.TblGradeTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblGradeTableAdapter()
        Me.TblGrade1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblGrade1TableAdapter()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckDeptSkip = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        Me.SidePanel3.SuspendLayout()
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlEmp.SuspendLayout()
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlDept.SuspendLayout()
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlComp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlComp.SuspendLayout()
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel2.SuspendLayout()
        CType(Me.PopupContainerControlGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlGrade.SuspendLayout()
        CType(Me.GridControlGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblGradeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlShift, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlShift.SuspendLayout()
        CType(Me.GridControlShift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblShiftMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewShift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlCat, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlCat.SuspendLayout()
        CType(Me.GridControlCat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCatagoryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewCat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.CheckOverTimeSumm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckShiftWiseAtt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckDeptWiseAtt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEmpWiseAtt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEmpPerformance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckShiftSchedule.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckOverStay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckOverTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckAbsenteeism.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEarlyDeparture.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckLateArrival.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckPerformance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.PopupContainerEditGrade.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditSection.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditShift.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditCat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditDept.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditComp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditEmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckDeptSkip.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel3)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1250, 583)
        Me.SplitContainerControl1.SplitterPosition = 1165
        Me.SplitContainerControl1.TabIndex = 1
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'SidePanel3
        '
        Me.SidePanel3.Controls.Add(Me.PopupContainerControlEmp)
        Me.SidePanel3.Controls.Add(Me.PopupContainerControlDept)
        Me.SidePanel3.Controls.Add(Me.SimpleButton5)
        Me.SidePanel3.Controls.Add(Me.PopupContainerControlComp)
        Me.SidePanel3.Controls.Add(Me.SimpleButton4)
        Me.SidePanel3.Controls.Add(Me.SimpleButton3)
        Me.SidePanel3.Controls.Add(Me.SimpleButton2)
        Me.SidePanel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel3.Location = New System.Drawing.Point(0, 529)
        Me.SidePanel3.Name = "SidePanel3"
        Me.SidePanel3.Size = New System.Drawing.Size(1165, 54)
        Me.SidePanel3.TabIndex = 4
        Me.SidePanel3.Text = "SidePanel3"
        '
        'PopupContainerControlEmp
        '
        Me.PopupContainerControlEmp.Controls.Add(Me.GridControlEmp)
        Me.PopupContainerControlEmp.Location = New System.Drawing.Point(793, 10)
        Me.PopupContainerControlEmp.Name = "PopupContainerControlEmp"
        Me.PopupContainerControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlEmp.TabIndex = 10
        '
        'GridControlEmp
        '
        Me.GridControlEmp.DataSource = Me.TblEmployeeBindingSource
        Me.GridControlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlEmp.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlEmp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlEmp.MainView = Me.GridViewEmp
        Me.GridControlEmp.Name = "GridControlEmp"
        Me.GridControlEmp.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit1})
        Me.GridControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlEmp.TabIndex = 6
        Me.GridControlEmp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewEmp})
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridViewEmp
        '
        Me.GridViewEmp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPAYCODE, Me.colEMPNAME})
        Me.GridViewEmp.GridControl = Me.GridControlEmp
        Me.GridViewEmp.Name = "GridViewEmp"
        Me.GridViewEmp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.Editable = False
        Me.GridViewEmp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewEmp.OptionsSelection.MultiSelect = True
        Me.GridViewEmp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewEmp.OptionsView.ColumnAutoWidth = False
        '
        'colPAYCODE
        '
        Me.colPAYCODE.Caption = "Paycode"
        Me.colPAYCODE.FieldName = "PAYCODE"
        Me.colPAYCODE.Name = "colPAYCODE"
        Me.colPAYCODE.Visible = True
        Me.colPAYCODE.VisibleIndex = 1
        '
        'colEMPNAME
        '
        Me.colEMPNAME.Caption = "Name"
        Me.colEMPNAME.FieldName = "EMPNAME"
        Me.colEMPNAME.Name = "colEMPNAME"
        Me.colEMPNAME.Visible = True
        Me.colEMPNAME.VisibleIndex = 2
        Me.colEMPNAME.Width = 125
        '
        'RepositoryItemTimeEdit1
        '
        Me.RepositoryItemTimeEdit1.AutoHeight = False
        Me.RepositoryItemTimeEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit1.Name = "RepositoryItemTimeEdit1"
        '
        'PopupContainerControlDept
        '
        Me.PopupContainerControlDept.Controls.Add(Me.GridControlDept)
        Me.PopupContainerControlDept.Location = New System.Drawing.Point(487, 9)
        Me.PopupContainerControlDept.Name = "PopupContainerControlDept"
        Me.PopupContainerControlDept.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlDept.TabIndex = 14
        '
        'GridControlDept
        '
        Me.GridControlDept.DataSource = Me.TblDepartmentBindingSource
        Me.GridControlDept.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlDept.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlDept.Location = New System.Drawing.Point(0, 0)
        Me.GridControlDept.MainView = Me.GridViewDept
        Me.GridControlDept.Name = "GridControlDept"
        Me.GridControlDept.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit3})
        Me.GridControlDept.Size = New System.Drawing.Size(300, 300)
        Me.GridControlDept.TabIndex = 6
        Me.GridControlDept.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewDept})
        '
        'TblDepartmentBindingSource
        '
        Me.TblDepartmentBindingSource.DataMember = "tblDepartment"
        Me.TblDepartmentBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewDept
        '
        Me.GridViewDept.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colDEPARTMENTCODE, Me.colDEPARTMENTNAME})
        Me.GridViewDept.GridControl = Me.GridControlDept
        Me.GridViewDept.Name = "GridViewDept"
        Me.GridViewDept.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.Editable = False
        Me.GridViewDept.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewDept.OptionsSelection.MultiSelect = True
        Me.GridViewDept.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewDept.OptionsView.ColumnAutoWidth = False
        '
        'colDEPARTMENTCODE
        '
        Me.colDEPARTMENTCODE.Caption = "Depatment Code"
        Me.colDEPARTMENTCODE.FieldName = "DEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Name = "colDEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Visible = True
        Me.colDEPARTMENTCODE.VisibleIndex = 1
        Me.colDEPARTMENTCODE.Width = 100
        '
        'colDEPARTMENTNAME
        '
        Me.colDEPARTMENTNAME.Caption = "Name"
        Me.colDEPARTMENTNAME.FieldName = "DEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Name = "colDEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Visible = True
        Me.colDEPARTMENTNAME.VisibleIndex = 2
        Me.colDEPARTMENTNAME.Width = 120
        '
        'RepositoryItemTimeEdit3
        '
        Me.RepositoryItemTimeEdit3.AutoHeight = False
        Me.RepositoryItemTimeEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit3.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit3.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit3.Name = "RepositoryItemTimeEdit3"
        '
        'SimpleButton5
        '
        Me.SimpleButton5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton5.Appearance.Options.UseFont = True
        Me.SimpleButton5.Location = New System.Drawing.Point(84, 10)
        Me.SimpleButton5.Name = "SimpleButton5"
        Me.SimpleButton5.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton5.TabIndex = 3
        Me.SimpleButton5.Text = "Excel"
        '
        'PopupContainerControlComp
        '
        Me.PopupContainerControlComp.Controls.Add(Me.GridControlComp)
        Me.PopupContainerControlComp.Location = New System.Drawing.Point(327, 10)
        Me.PopupContainerControlComp.Name = "PopupContainerControlComp"
        Me.PopupContainerControlComp.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlComp.TabIndex = 13
        '
        'GridControlComp
        '
        Me.GridControlComp.DataSource = Me.TblCompanyBindingSource
        Me.GridControlComp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlComp.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlComp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlComp.MainView = Me.GridViewComp
        Me.GridControlComp.Name = "GridControlComp"
        Me.GridControlComp.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit2})
        Me.GridControlComp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlComp.TabIndex = 6
        Me.GridControlComp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewComp})
        '
        'TblCompanyBindingSource
        '
        Me.TblCompanyBindingSource.DataMember = "tblCompany"
        Me.TblCompanyBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewComp
        '
        Me.GridViewComp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCOMPANYCODE, Me.colCOMPANYNAME})
        Me.GridViewComp.GridControl = Me.GridControlComp
        Me.GridViewComp.Name = "GridViewComp"
        Me.GridViewComp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.Editable = False
        Me.GridViewComp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewComp.OptionsSelection.MultiSelect = True
        Me.GridViewComp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewComp.OptionsView.ColumnAutoWidth = False
        '
        'colCOMPANYCODE
        '
        Me.colCOMPANYCODE.Caption = "Company Code"
        Me.colCOMPANYCODE.FieldName = "COMPANYCODE"
        Me.colCOMPANYCODE.Name = "colCOMPANYCODE"
        Me.colCOMPANYCODE.Visible = True
        Me.colCOMPANYCODE.VisibleIndex = 1
        Me.colCOMPANYCODE.Width = 100
        '
        'colCOMPANYNAME
        '
        Me.colCOMPANYNAME.Caption = "Name"
        Me.colCOMPANYNAME.FieldName = "COMPANYNAME"
        Me.colCOMPANYNAME.Name = "colCOMPANYNAME"
        Me.colCOMPANYNAME.Visible = True
        Me.colCOMPANYNAME.VisibleIndex = 2
        Me.colCOMPANYNAME.Width = 120
        '
        'RepositoryItemTimeEdit2
        '
        Me.RepositoryItemTimeEdit2.AutoHeight = False
        Me.RepositoryItemTimeEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit2.Name = "RepositoryItemTimeEdit2"
        '
        'SimpleButton4
        '
        Me.SimpleButton4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton4.Appearance.Options.UseFont = True
        Me.SimpleButton4.Location = New System.Drawing.Point(246, 9)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton4.TabIndex = 2
        Me.SimpleButton4.Text = "PDF"
        Me.SimpleButton4.Visible = False
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton3.Appearance.Options.UseFont = True
        Me.SimpleButton3.Location = New System.Drawing.Point(3, 9)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton3.TabIndex = 1
        Me.SimpleButton3.Text = "Text"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(165, 9)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 0
        Me.SimpleButton2.Text = "Excel"
        Me.SimpleButton2.Visible = False
        '
        'SidePanel2
        '
        Me.SidePanel2.Controls.Add(Me.PopupContainerControlGrade)
        Me.SidePanel2.Controls.Add(Me.PopupContainerControlShift)
        Me.SidePanel2.Controls.Add(Me.PopupContainerControlCat)
        Me.SidePanel2.Controls.Add(Me.PanelControl1)
        Me.SidePanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanel2.Location = New System.Drawing.Point(0, 153)
        Me.SidePanel2.Name = "SidePanel2"
        Me.SidePanel2.Size = New System.Drawing.Size(1165, 477)
        Me.SidePanel2.TabIndex = 3
        Me.SidePanel2.Text = "SidePanel2"
        '
        'PopupContainerControlGrade
        '
        Me.PopupContainerControlGrade.Controls.Add(Me.GridControlGrade)
        Me.PopupContainerControlGrade.Location = New System.Drawing.Point(16, 295)
        Me.PopupContainerControlGrade.Name = "PopupContainerControlGrade"
        Me.PopupContainerControlGrade.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlGrade.TabIndex = 23
        '
        'GridControlGrade
        '
        Me.GridControlGrade.DataSource = Me.TblGradeBindingSource
        Me.GridControlGrade.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlGrade.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlGrade.Location = New System.Drawing.Point(0, 0)
        Me.GridControlGrade.MainView = Me.GridViewGrade
        Me.GridControlGrade.Name = "GridControlGrade"
        Me.GridControlGrade.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit6})
        Me.GridControlGrade.Size = New System.Drawing.Size(300, 300)
        Me.GridControlGrade.TabIndex = 6
        Me.GridControlGrade.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewGrade})
        '
        'TblGradeBindingSource
        '
        Me.TblGradeBindingSource.DataMember = "tblGrade"
        Me.TblGradeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewGrade
        '
        Me.GridViewGrade.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridViewGrade.GridControl = Me.GridControlGrade
        Me.GridViewGrade.Name = "GridViewGrade"
        Me.GridViewGrade.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewGrade.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewGrade.OptionsBehavior.Editable = False
        Me.GridViewGrade.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewGrade.OptionsSelection.MultiSelect = True
        Me.GridViewGrade.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewGrade.OptionsView.ColumnAutoWidth = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Grade Code"
        Me.GridColumn1.FieldName = "GradeCode"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 1
        Me.GridColumn1.Width = 100
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Name"
        Me.GridColumn2.FieldName = "GradeName"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 2
        Me.GridColumn2.Width = 120
        '
        'RepositoryItemTimeEdit6
        '
        Me.RepositoryItemTimeEdit6.AutoHeight = False
        Me.RepositoryItemTimeEdit6.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit6.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit6.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit6.Name = "RepositoryItemTimeEdit6"
        '
        'PopupContainerControlShift
        '
        Me.PopupContainerControlShift.Controls.Add(Me.GridControlShift)
        Me.PopupContainerControlShift.Location = New System.Drawing.Point(633, 295)
        Me.PopupContainerControlShift.Name = "PopupContainerControlShift"
        Me.PopupContainerControlShift.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlShift.TabIndex = 16
        '
        'GridControlShift
        '
        Me.GridControlShift.DataSource = Me.TblShiftMasterBindingSource
        Me.GridControlShift.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlShift.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlShift.Location = New System.Drawing.Point(0, 0)
        Me.GridControlShift.MainView = Me.GridViewShift
        Me.GridControlShift.Name = "GridControlShift"
        Me.GridControlShift.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit5, Me.RepositoryItemDateEdit1})
        Me.GridControlShift.Size = New System.Drawing.Size(300, 300)
        Me.GridControlShift.TabIndex = 6
        Me.GridControlShift.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewShift})
        '
        'TblShiftMasterBindingSource
        '
        Me.TblShiftMasterBindingSource.DataMember = "tblShiftMaster"
        Me.TblShiftMasterBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewShift
        '
        Me.GridViewShift.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSHIFT, Me.colSTARTTIME, Me.colENDTIME})
        Me.GridViewShift.GridControl = Me.GridControlShift
        Me.GridViewShift.Name = "GridViewShift"
        Me.GridViewShift.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewShift.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewShift.OptionsBehavior.Editable = False
        Me.GridViewShift.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewShift.OptionsSelection.MultiSelect = True
        Me.GridViewShift.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewShift.OptionsView.ColumnAutoWidth = False
        '
        'colSHIFT
        '
        Me.colSHIFT.Caption = "Shift"
        Me.colSHIFT.FieldName = "SHIFT"
        Me.colSHIFT.Name = "colSHIFT"
        Me.colSHIFT.Visible = True
        Me.colSHIFT.VisibleIndex = 1
        '
        'colSTARTTIME
        '
        Me.colSTARTTIME.Caption = "Start Time"
        Me.colSTARTTIME.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colSTARTTIME.FieldName = "STARTTIME"
        Me.colSTARTTIME.Name = "colSTARTTIME"
        Me.colSTARTTIME.Visible = True
        Me.colSTARTTIME.VisibleIndex = 2
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'colENDTIME
        '
        Me.colENDTIME.Caption = "End Time"
        Me.colENDTIME.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colENDTIME.FieldName = "ENDTIME"
        Me.colENDTIME.Name = "colENDTIME"
        Me.colENDTIME.Visible = True
        Me.colENDTIME.VisibleIndex = 3
        '
        'RepositoryItemTimeEdit5
        '
        Me.RepositoryItemTimeEdit5.AutoHeight = False
        Me.RepositoryItemTimeEdit5.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit5.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit5.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit5.Name = "RepositoryItemTimeEdit5"
        '
        'PopupContainerControlCat
        '
        Me.PopupContainerControlCat.Controls.Add(Me.GridControlCat)
        Me.PopupContainerControlCat.Location = New System.Drawing.Point(324, 295)
        Me.PopupContainerControlCat.Name = "PopupContainerControlCat"
        Me.PopupContainerControlCat.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlCat.TabIndex = 15
        '
        'GridControlCat
        '
        Me.GridControlCat.DataSource = Me.TblCatagoryBindingSource
        Me.GridControlCat.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlCat.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlCat.Location = New System.Drawing.Point(0, 0)
        Me.GridControlCat.MainView = Me.GridViewCat
        Me.GridControlCat.Name = "GridControlCat"
        Me.GridControlCat.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit4})
        Me.GridControlCat.Size = New System.Drawing.Size(300, 300)
        Me.GridControlCat.TabIndex = 6
        Me.GridControlCat.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewCat})
        '
        'TblCatagoryBindingSource
        '
        Me.TblCatagoryBindingSource.DataMember = "tblCatagory"
        Me.TblCatagoryBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewCat
        '
        Me.GridViewCat.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCAT, Me.colCATAGORYNAME})
        Me.GridViewCat.GridControl = Me.GridControlCat
        Me.GridViewCat.Name = "GridViewCat"
        Me.GridViewCat.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewCat.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewCat.OptionsBehavior.Editable = False
        Me.GridViewCat.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewCat.OptionsSelection.MultiSelect = True
        Me.GridViewCat.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewCat.OptionsView.ColumnAutoWidth = False
        '
        'colCAT
        '
        Me.colCAT.Caption = "Catagory Code"
        Me.colCAT.FieldName = "CAT"
        Me.colCAT.Name = "colCAT"
        Me.colCAT.Visible = True
        Me.colCAT.VisibleIndex = 1
        Me.colCAT.Width = 100
        '
        'colCATAGORYNAME
        '
        Me.colCATAGORYNAME.Caption = "Name"
        Me.colCATAGORYNAME.FieldName = "CATAGORYNAME"
        Me.colCATAGORYNAME.Name = "colCATAGORYNAME"
        Me.colCATAGORYNAME.Visible = True
        Me.colCATAGORYNAME.VisibleIndex = 2
        Me.colCATAGORYNAME.Width = 120
        '
        'RepositoryItemTimeEdit4
        '
        Me.RepositoryItemTimeEdit4.AutoHeight = False
        Me.RepositoryItemTimeEdit4.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit4.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit4.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit4.Name = "RepositoryItemTimeEdit4"
        '
        'PanelControl1
        '
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl1.Controls.Add(Me.TextEdit1)
        Me.PanelControl1.Controls.Add(Me.CheckOverTimeSumm)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.CheckShiftWiseAtt)
        Me.PanelControl1.Controls.Add(Me.CheckDeptSkip)
        Me.PanelControl1.Controls.Add(Me.CheckDeptWiseAtt)
        Me.PanelControl1.Controls.Add(Me.CheckEmpWiseAtt)
        Me.PanelControl1.Controls.Add(Me.CheckEmpPerformance)
        Me.PanelControl1.Controls.Add(Me.CheckShiftSchedule)
        Me.PanelControl1.Controls.Add(Me.CheckOverStay)
        Me.PanelControl1.Controls.Add(Me.CheckOverTime)
        Me.PanelControl1.Controls.Add(Me.CheckAbsenteeism)
        Me.PanelControl1.Controls.Add(Me.CheckEarlyDeparture)
        Me.PanelControl1.Controls.Add(Me.CheckLateArrival)
        Me.PanelControl1.Controls.Add(Me.CheckPerformance)
        Me.PanelControl1.Location = New System.Drawing.Point(16, 16)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(500, 256)
        Me.PanelControl1.TabIndex = 12
        '
        'CheckOverTimeSumm
        '
        Me.CheckOverTimeSumm.Location = New System.Drawing.Point(230, 117)
        Me.CheckOverTimeSumm.Name = "CheckOverTimeSumm"
        Me.CheckOverTimeSumm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckOverTimeSumm.Properties.Appearance.Options.UseFont = True
        Me.CheckOverTimeSumm.Properties.Caption = "Over Time Summary"
        Me.CheckOverTimeSumm.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckOverTimeSumm.Properties.RadioGroupIndex = 0
        Me.CheckOverTimeSumm.Size = New System.Drawing.Size(185, 19)
        Me.CheckOverTimeSumm.TabIndex = 22
        Me.CheckOverTimeSumm.TabStop = False
        '
        'CheckShiftWiseAtt
        '
        Me.CheckShiftWiseAtt.Location = New System.Drawing.Point(230, 92)
        Me.CheckShiftWiseAtt.Name = "CheckShiftWiseAtt"
        Me.CheckShiftWiseAtt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckShiftWiseAtt.Properties.Appearance.Options.UseFont = True
        Me.CheckShiftWiseAtt.Properties.Caption = "Shift Wise Attendance"
        Me.CheckShiftWiseAtt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckShiftWiseAtt.Properties.RadioGroupIndex = 0
        Me.CheckShiftWiseAtt.Size = New System.Drawing.Size(185, 19)
        Me.CheckShiftWiseAtt.TabIndex = 21
        Me.CheckShiftWiseAtt.TabStop = False
        '
        'CheckDeptWiseAtt
        '
        Me.CheckDeptWiseAtt.Location = New System.Drawing.Point(230, 67)
        Me.CheckDeptWiseAtt.Name = "CheckDeptWiseAtt"
        Me.CheckDeptWiseAtt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckDeptWiseAtt.Properties.Appearance.Options.UseFont = True
        Me.CheckDeptWiseAtt.Properties.Caption = "Department Wise Attendance"
        Me.CheckDeptWiseAtt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckDeptWiseAtt.Properties.RadioGroupIndex = 0
        Me.CheckDeptWiseAtt.Size = New System.Drawing.Size(185, 19)
        Me.CheckDeptWiseAtt.TabIndex = 20
        Me.CheckDeptWiseAtt.TabStop = False
        '
        'CheckEmpWiseAtt
        '
        Me.CheckEmpWiseAtt.Location = New System.Drawing.Point(230, 42)
        Me.CheckEmpWiseAtt.Name = "CheckEmpWiseAtt"
        Me.CheckEmpWiseAtt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEmpWiseAtt.Properties.Appearance.Options.UseFont = True
        Me.CheckEmpWiseAtt.Properties.Caption = "Employee Wise Attendance"
        Me.CheckEmpWiseAtt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEmpWiseAtt.Properties.RadioGroupIndex = 0
        Me.CheckEmpWiseAtt.Size = New System.Drawing.Size(185, 19)
        Me.CheckEmpWiseAtt.TabIndex = 19
        Me.CheckEmpWiseAtt.TabStop = False
        '
        'CheckEmpPerformance
        '
        Me.CheckEmpPerformance.Location = New System.Drawing.Point(230, 17)
        Me.CheckEmpPerformance.Name = "CheckEmpPerformance"
        Me.CheckEmpPerformance.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEmpPerformance.Properties.Appearance.Options.UseFont = True
        Me.CheckEmpPerformance.Properties.Caption = "Employee wise Performance"
        Me.CheckEmpPerformance.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEmpPerformance.Properties.RadioGroupIndex = 0
        Me.CheckEmpPerformance.Size = New System.Drawing.Size(185, 19)
        Me.CheckEmpPerformance.TabIndex = 18
        Me.CheckEmpPerformance.TabStop = False
        '
        'CheckShiftSchedule
        '
        Me.CheckShiftSchedule.Location = New System.Drawing.Point(15, 167)
        Me.CheckShiftSchedule.Name = "CheckShiftSchedule"
        Me.CheckShiftSchedule.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckShiftSchedule.Properties.Appearance.Options.UseFont = True
        Me.CheckShiftSchedule.Properties.Caption = "Shift Schedule"
        Me.CheckShiftSchedule.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckShiftSchedule.Properties.RadioGroupIndex = 0
        Me.CheckShiftSchedule.Size = New System.Drawing.Size(168, 19)
        Me.CheckShiftSchedule.TabIndex = 17
        Me.CheckShiftSchedule.TabStop = False
        '
        'CheckOverStay
        '
        Me.CheckOverStay.Location = New System.Drawing.Point(15, 142)
        Me.CheckOverStay.Name = "CheckOverStay"
        Me.CheckOverStay.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckOverStay.Properties.Appearance.Options.UseFont = True
        Me.CheckOverStay.Properties.Caption = "Over Stay Register"
        Me.CheckOverStay.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckOverStay.Properties.RadioGroupIndex = 0
        Me.CheckOverStay.Size = New System.Drawing.Size(168, 19)
        Me.CheckOverStay.TabIndex = 16
        Me.CheckOverStay.TabStop = False
        '
        'CheckOverTime
        '
        Me.CheckOverTime.Location = New System.Drawing.Point(15, 117)
        Me.CheckOverTime.Name = "CheckOverTime"
        Me.CheckOverTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckOverTime.Properties.Appearance.Options.UseFont = True
        Me.CheckOverTime.Properties.Caption = "Over Time Register"
        Me.CheckOverTime.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckOverTime.Properties.RadioGroupIndex = 0
        Me.CheckOverTime.Size = New System.Drawing.Size(168, 19)
        Me.CheckOverTime.TabIndex = 15
        Me.CheckOverTime.TabStop = False
        '
        'CheckAbsenteeism
        '
        Me.CheckAbsenteeism.Location = New System.Drawing.Point(15, 92)
        Me.CheckAbsenteeism.Name = "CheckAbsenteeism"
        Me.CheckAbsenteeism.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckAbsenteeism.Properties.Appearance.Options.UseFont = True
        Me.CheckAbsenteeism.Properties.Caption = "Absenteeism Register"
        Me.CheckAbsenteeism.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckAbsenteeism.Properties.RadioGroupIndex = 0
        Me.CheckAbsenteeism.Size = New System.Drawing.Size(168, 19)
        Me.CheckAbsenteeism.TabIndex = 14
        Me.CheckAbsenteeism.TabStop = False
        '
        'CheckEarlyDeparture
        '
        Me.CheckEarlyDeparture.Location = New System.Drawing.Point(15, 67)
        Me.CheckEarlyDeparture.Name = "CheckEarlyDeparture"
        Me.CheckEarlyDeparture.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEarlyDeparture.Properties.Appearance.Options.UseFont = True
        Me.CheckEarlyDeparture.Properties.Caption = "Early Departure Register"
        Me.CheckEarlyDeparture.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEarlyDeparture.Properties.RadioGroupIndex = 0
        Me.CheckEarlyDeparture.Size = New System.Drawing.Size(168, 19)
        Me.CheckEarlyDeparture.TabIndex = 13
        Me.CheckEarlyDeparture.TabStop = False
        '
        'CheckLateArrival
        '
        Me.CheckLateArrival.Location = New System.Drawing.Point(15, 42)
        Me.CheckLateArrival.Name = "CheckLateArrival"
        Me.CheckLateArrival.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckLateArrival.Properties.Appearance.Options.UseFont = True
        Me.CheckLateArrival.Properties.Caption = "Late Arrival Register"
        Me.CheckLateArrival.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckLateArrival.Properties.RadioGroupIndex = 0
        Me.CheckLateArrival.Size = New System.Drawing.Size(168, 19)
        Me.CheckLateArrival.TabIndex = 12
        Me.CheckLateArrival.TabStop = False
        '
        'CheckPerformance
        '
        Me.CheckPerformance.Location = New System.Drawing.Point(15, 17)
        Me.CheckPerformance.Name = "CheckPerformance"
        Me.CheckPerformance.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckPerformance.Properties.Appearance.Options.UseFont = True
        Me.CheckPerformance.Properties.Caption = "Performance Register"
        Me.CheckPerformance.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckPerformance.Properties.RadioGroupIndex = 0
        Me.CheckPerformance.Size = New System.Drawing.Size(168, 19)
        Me.CheckPerformance.TabIndex = 11
        Me.CheckPerformance.TabStop = False
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.LabelControl9)
        Me.SidePanel1.Controls.Add(Me.PopupContainerEditGrade)
        Me.SidePanel1.Controls.Add(Me.LabelControl8)
        Me.SidePanel1.Controls.Add(Me.PopupContainerEditSection)
        Me.SidePanel1.Controls.Add(Me.LabelControl7)
        Me.SidePanel1.Controls.Add(Me.PopupContainerEditShift)
        Me.SidePanel1.Controls.Add(Me.LabelControl6)
        Me.SidePanel1.Controls.Add(Me.PopupContainerEditCat)
        Me.SidePanel1.Controls.Add(Me.LabelControl5)
        Me.SidePanel1.Controls.Add(Me.PopupContainerEditDept)
        Me.SidePanel1.Controls.Add(Me.LabelControl4)
        Me.SidePanel1.Controls.Add(Me.PopupContainerEditComp)
        Me.SidePanel1.Controls.Add(Me.CheckEdit2)
        Me.SidePanel1.Controls.Add(Me.CheckEdit1)
        Me.SidePanel1.Controls.Add(Me.PopupContainerEditEmp)
        Me.SidePanel1.Controls.Add(Me.LabelControl1)
        Me.SidePanel1.Controls.Add(Me.SimpleButton1)
        Me.SidePanel1.Controls.Add(Me.DateEdit2)
        Me.SidePanel1.Controls.Add(Me.LabelControl3)
        Me.SidePanel1.Controls.Add(Me.DateEdit1)
        Me.SidePanel1.Controls.Add(Me.LabelControl2)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanel1.Location = New System.Drawing.Point(0, 0)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1165, 153)
        Me.SidePanel1.TabIndex = 2
        Me.SidePanel1.Text = "SidePanel1"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(362, 89)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl9.TabIndex = 20
        Me.LabelControl9.Text = "Select Grade"
        '
        'PopupContainerEditGrade
        '
        Me.PopupContainerEditGrade.Location = New System.Drawing.Point(497, 86)
        Me.PopupContainerEditGrade.Name = "PopupContainerEditGrade"
        Me.PopupContainerEditGrade.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditGrade.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditGrade.TabIndex = 19
        Me.PopupContainerEditGrade.ToolTip = "Leave blank if want for all Employees"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(3, 89)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(79, 14)
        Me.LabelControl8.TabIndex = 18
        Me.LabelControl8.Text = "Select Section"
        Me.LabelControl8.Visible = False
        '
        'PopupContainerEditSection
        '
        Me.PopupContainerEditSection.Location = New System.Drawing.Point(100, 86)
        Me.PopupContainerEditSection.Name = "PopupContainerEditSection"
        Me.PopupContainerEditSection.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditSection.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditSection.TabIndex = 17
        Me.PopupContainerEditSection.ToolTip = "Leave blank if want for all Employees"
        Me.PopupContainerEditSection.Visible = False
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(362, 66)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(63, 14)
        Me.LabelControl7.TabIndex = 16
        Me.LabelControl7.Text = "Select Shift"
        '
        'PopupContainerEditShift
        '
        Me.PopupContainerEditShift.Location = New System.Drawing.Point(497, 63)
        Me.PopupContainerEditShift.Name = "PopupContainerEditShift"
        Me.PopupContainerEditShift.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditShift.Properties.PopupControl = Me.PopupContainerControlShift
        Me.PopupContainerEditShift.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditShift.TabIndex = 15
        Me.PopupContainerEditShift.ToolTip = "Leave blank if want for all Employees"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(3, 65)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl6.TabIndex = 14
        Me.LabelControl6.Text = "Select Catagory"
        '
        'PopupContainerEditCat
        '
        Me.PopupContainerEditCat.Location = New System.Drawing.Point(100, 63)
        Me.PopupContainerEditCat.Name = "PopupContainerEditCat"
        Me.PopupContainerEditCat.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditCat.Properties.PopupControl = Me.PopupContainerControlCat
        Me.PopupContainerEditCat.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditCat.TabIndex = 13
        Me.PopupContainerEditCat.ToolTip = "Leave blank if want for all Employees"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(362, 41)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(104, 14)
        Me.LabelControl5.TabIndex = 12
        Me.LabelControl5.Text = "Select Department"
        '
        'PopupContainerEditDept
        '
        Me.PopupContainerEditDept.Location = New System.Drawing.Point(497, 38)
        Me.PopupContainerEditDept.Name = "PopupContainerEditDept"
        Me.PopupContainerEditDept.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditDept.Properties.PopupControl = Me.PopupContainerControlDept
        Me.PopupContainerEditDept.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditDept.TabIndex = 11
        Me.PopupContainerEditDept.ToolTip = "Leave blank if want for all Employees"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(3, 41)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(88, 14)
        Me.LabelControl4.TabIndex = 10
        Me.LabelControl4.Text = "Select Company"
        '
        'PopupContainerEditComp
        '
        Me.PopupContainerEditComp.Location = New System.Drawing.Point(100, 39)
        Me.PopupContainerEditComp.Name = "PopupContainerEditComp"
        Me.PopupContainerEditComp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditComp.Properties.PopupControl = Me.PopupContainerControlComp
        Me.PopupContainerEditComp.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditComp.TabIndex = 9
        Me.PopupContainerEditComp.ToolTip = "Leave blank if want for all Employees"
        '
        'CheckEdit2
        '
        Me.CheckEdit2.Location = New System.Drawing.Point(234, 112)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit2.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit2.Properties.Caption = "Inactive Employee"
        Me.CheckEdit2.Size = New System.Drawing.Size(155, 19)
        Me.CheckEdit2.TabIndex = 8
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(6, 112)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit1.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit1.Properties.Caption = "Active Employee"
        Me.CheckEdit1.Size = New System.Drawing.Size(155, 19)
        Me.CheckEdit1.TabIndex = 7
        '
        'PopupContainerEditEmp
        '
        Me.PopupContainerEditEmp.Location = New System.Drawing.Point(100, 13)
        Me.PopupContainerEditEmp.Name = "PopupContainerEditEmp"
        Me.PopupContainerEditEmp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditEmp.Properties.PopupControl = Me.PopupContainerControlEmp
        Me.PopupContainerEditEmp.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditEmp.TabIndex = 6
        Me.PopupContainerEditEmp.ToolTip = "Leave blank if want for all Employees"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(3, 16)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl1.TabIndex = 5
        Me.LabelControl1.Text = "Select Employee"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(959, 16)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 4
        Me.SimpleButton1.Text = "Generate"
        '
        'DateEdit2
        '
        Me.DateEdit2.EditValue = Nothing
        Me.DateEdit2.Location = New System.Drawing.Point(769, 16)
        Me.DateEdit2.Name = "DateEdit2"
        Me.DateEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit2.Properties.Appearance.Options.UseFont = True
        Me.DateEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit2.TabIndex = 3
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(685, 19)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(45, 14)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "To Date"
        '
        'DateEdit1
        '
        Me.DateEdit1.EditValue = Nothing
        Me.DateEdit1.Location = New System.Drawing.Point(497, 16)
        Me.DateEdit1.Name = "DateEdit1"
        Me.DateEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit1.Properties.Appearance.Options.UseFont = True
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit1.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(412, 19)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(57, 14)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "From Date"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(77, 583)
        Me.MemoEdit1.TabIndex = 3
        '
        'TblCompanyTableAdapter
        '
        Me.TblCompanyTableAdapter.ClearBeforeFill = True
        '
        'TblCompany1TableAdapter1
        '
        Me.TblCompany1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'TblDepartmentTableAdapter
        '
        Me.TblDepartmentTableAdapter.ClearBeforeFill = True
        '
        'TblDepartment1TableAdapter1
        '
        Me.TblDepartment1TableAdapter1.ClearBeforeFill = True
        '
        'TblCatagoryTableAdapter
        '
        Me.TblCatagoryTableAdapter.ClearBeforeFill = True
        '
        'TblCatagory1TableAdapter1
        '
        Me.TblCatagory1TableAdapter1.ClearBeforeFill = True
        '
        'TblShiftMasterTableAdapter
        '
        Me.TblShiftMasterTableAdapter.ClearBeforeFill = True
        '
        'TblShiftMaster1TableAdapter1
        '
        Me.TblShiftMaster1TableAdapter1.ClearBeforeFill = True
        '
        'TblGradeTableAdapter
        '
        Me.TblGradeTableAdapter.ClearBeforeFill = True
        '
        'TblGrade1TableAdapter1
        '
        Me.TblGrade1TableAdapter1.ClearBeforeFill = True
        '
        'TextEdit1
        '
        Me.TextEdit1.EditValue = "58"
        Me.TextEdit1.Location = New System.Drawing.Point(143, 211)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEdit1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit1.Properties.MaxLength = 2
        Me.TextEdit1.Size = New System.Drawing.Size(55, 20)
        Me.TextEdit1.TabIndex = 37
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(48, 214)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(80, 14)
        Me.LabelControl10.TabIndex = 36
        Me.LabelControl10.Text = "Lines Per Page"
        '
        'CheckDeptSkip
        '
        Me.CheckDeptSkip.Location = New System.Drawing.Point(297, 212)
        Me.CheckDeptSkip.Name = "CheckDeptSkip"
        Me.CheckDeptSkip.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckDeptSkip.Properties.Appearance.Options.UseFont = True
        Me.CheckDeptSkip.Properties.Caption = "Department Wise Skip"
        Me.CheckDeptSkip.Size = New System.Drawing.Size(186, 19)
        Me.CheckDeptSkip.TabIndex = 35
        '
        'XtraReportNew
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraReportNew"
        Me.Size = New System.Drawing.Size(1250, 583)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        Me.SidePanel3.ResumeLayout(False)
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlEmp.ResumeLayout(False)
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlDept.ResumeLayout(False)
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlComp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlComp.ResumeLayout(False)
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel2.ResumeLayout(False)
        CType(Me.PopupContainerControlGrade, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlGrade.ResumeLayout(False)
        CType(Me.GridControlGrade, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblGradeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewGrade, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlShift, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlShift.ResumeLayout(False)
        CType(Me.GridControlShift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblShiftMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewShift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlCat, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlCat.ResumeLayout(False)
        CType(Me.GridControlCat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCatagoryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewCat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.CheckOverTimeSumm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckShiftWiseAtt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckDeptWiseAtt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEmpWiseAtt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEmpPerformance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckShiftSchedule.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckOverStay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckOverTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckAbsenteeism.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEarlyDeparture.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckLateArrival.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckPerformance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        Me.SidePanel1.PerformLayout()
        CType(Me.PopupContainerEditGrade.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditSection.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditShift.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditCat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditDept.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditComp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditEmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckDeptSkip.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents SidePanel3 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SimpleButton5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SidePanel2 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents PopupContainerControlEmp As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlEmp As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewEmp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colPAYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents PopupContainerEditEmp As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit2 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents CheckLateArrival As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckPerformance As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckAbsenteeism As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEarlyDeparture As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckShiftSchedule As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckOverStay As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckOverTime As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEmpPerformance As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEmpWiseAtt As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckShiftWiseAtt As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckDeptWiseAtt As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckOverTimeSumm As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PopupContainerEditComp As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditShift As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditCat As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditDept As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditSection As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditGrade As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents PopupContainerControlComp As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlComp As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblCompanyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As ULtra.SSSDBDataSet
    Friend WithEvents GridViewComp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCOMPANYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents TblCompanyTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblCompanyTableAdapter
    Friend WithEvents TblCompany1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblCompany1TableAdapter
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblEmployeeTableAdapter As ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents TblEmployee1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents PopupContainerControlDept As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlDept As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblDepartmentBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GridViewDept As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colDEPARTMENTCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEPARTMENTNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents TblDepartmentTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter
    Friend WithEvents TblDepartment1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter
    Friend WithEvents PopupContainerControlCat As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlCat As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblCatagoryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GridViewCat As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCATAGORYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit4 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents TblCatagoryTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblCatagoryTableAdapter
    Friend WithEvents PopupContainerControlShift As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlShift As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewShift As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemTimeEdit5 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents TblCatagory1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblCatagory1TableAdapter
    Friend WithEvents TblShiftMasterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents colSHIFT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTARTTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents colENDTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblShiftMasterTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblShiftMasterTableAdapter
    Friend WithEvents TblShiftMaster1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblShiftMaster1TableAdapter
    Friend WithEvents PopupContainerControlGrade As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlGrade As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewGrade As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit6 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents TblGradeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblGradeTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblGradeTableAdapter
    Friend WithEvents TblGrade1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblGrade1TableAdapter
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckDeptSkip As DevExpress.XtraEditors.CheckEdit

End Class
