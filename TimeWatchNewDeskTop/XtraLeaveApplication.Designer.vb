﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraLeaveApplication
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraLeaveApplication))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.ComboCmpAgn = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboNepaliYearApp = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.ComboNEpaliMonthApp = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliDateApp = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliYearTo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNEpaliMonthTo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliDateTo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliYearFrm = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNEpaliMonthFrm = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliDateFrm = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New ULtra.SSSDBDataSet()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPayCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOUCHER_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLeaveCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colForDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDays = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAppliedFor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHalfType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colReason = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colApprovedDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOFUsedAgainst = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckEdit6 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit5 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit4 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit3 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.DateEdit3 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.GridLookUpEditLeave = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.lblGrade = New DevExpress.XtraEditors.LabelControl()
        Me.lblEmpGrp = New DevExpress.XtraEditors.LabelControl()
        Me.lblCat = New DevExpress.XtraEditors.LabelControl()
        Me.lblDept = New DevExpress.XtraEditors.LabelControl()
        Me.lblComp = New DevExpress.XtraEditors.LabelControl()
        Me.lblDesi = New DevExpress.XtraEditors.LabelControl()
        Me.lblCardNum = New DevExpress.XtraEditors.LabelControl()
        Me.lblName = New DevExpress.XtraEditors.LabelControl()
        Me.lblVoucher = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit2 = New DevExpress.XtraEditors.DateEdit()
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblLeaveMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LeaveApplicationBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LeaveApplicationTableAdapter = New ULtra.SSSDBDataSetTableAdapters.LeaveApplicationTableAdapter()
        Me.LeaveApplication1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.LeaveApplication1TableAdapter()
        Me.TblLeaveMasterTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblLeaveMasterTableAdapter()
        Me.TblLeaveMaster1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblLeaveMaster1TableAdapter()
        Me.TblEmployeeTableAdapter = New ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblEmployee1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.ComboCmpAgn.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliYearApp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonthApp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliDateApp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliYearTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonthTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliDateTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliYearFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonthFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliDateFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.CheckEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit3.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditLeave.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblLeaveMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LeaveApplicationBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.LookAndFeel.SkinName = "iMaginary"
        Me.SplitContainerControl1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.ComboCmpAgn)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl13)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.ComboNepaliYearApp)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SimpleButton1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.ComboNEpaliMonthApp)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.ComboNepaliDateApp)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.ComboNepaliYearTo)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.ComboNEpaliMonthTo)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.ComboNepaliDateTo)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.ComboNepaliYearFrm)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.ComboNEpaliMonthFrm)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.ComboNepaliDateFrm)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LookUpEdit1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.DateEdit3)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl24)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.TextEdit1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl23)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GridLookUpEditLeave)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl22)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lblGrade)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lblEmpGrp)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lblCat)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lblDept)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lblComp)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lblDesi)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lblCardNum)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lblName)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lblVoucher)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl12)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl11)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl10)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl9)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl8)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl7)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl6)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl5)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl4)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.DateEdit2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.DateEdit1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl3)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.TextEdit2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 4
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'ComboCmpAgn
        '
        Me.ComboCmpAgn.Location = New System.Drawing.Point(672, 57)
        Me.ComboCmpAgn.Name = "ComboCmpAgn"
        Me.ComboCmpAgn.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboCmpAgn.Properties.Appearance.Options.UseFont = True
        Me.ComboCmpAgn.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboCmpAgn.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboCmpAgn.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboCmpAgn.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboCmpAgn.Size = New System.Drawing.Size(145, 20)
        Me.ComboCmpAgn.TabIndex = 34
        Me.ComboCmpAgn.Visible = False
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(589, 60)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(75, 14)
        Me.LabelControl13.TabIndex = 33
        Me.LabelControl13.Text = "Comp Against"
        Me.LabelControl13.Visible = False
        '
        'ComboNepaliYearApp
        '
        Me.ComboNepaliYearApp.Location = New System.Drawing.Point(503, 108)
        Me.ComboNepaliYearApp.Name = "ComboNepaliYearApp"
        Me.ComboNepaliYearApp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearApp.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYearApp.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearApp.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYearApp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYearApp.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYearApp.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliYearApp.TabIndex = 12
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Enabled = False
        Me.SimpleButton1.Location = New System.Drawing.Point(631, 295)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 14
        Me.SimpleButton1.Text = "Save"
        '
        'ComboNEpaliMonthApp
        '
        Me.ComboNEpaliMonthApp.Location = New System.Drawing.Point(431, 108)
        Me.ComboNEpaliMonthApp.Name = "ComboNEpaliMonthApp"
        Me.ComboNEpaliMonthApp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthApp.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonthApp.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthApp.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonthApp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonthApp.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonthApp.Size = New System.Drawing.Size(66, 20)
        Me.ComboNEpaliMonthApp.TabIndex = 11
        '
        'ComboNepaliDateApp
        '
        Me.ComboNepaliDateApp.Location = New System.Drawing.Point(383, 108)
        Me.ComboNepaliDateApp.Name = "ComboNepaliDateApp"
        Me.ComboNepaliDateApp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateApp.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliDateApp.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateApp.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliDateApp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliDateApp.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNepaliDateApp.Size = New System.Drawing.Size(42, 20)
        Me.ComboNepaliDateApp.TabIndex = 10
        '
        'ComboNepaliYearTo
        '
        Me.ComboNepaliYearTo.Location = New System.Drawing.Point(724, 13)
        Me.ComboNepaliYearTo.Name = "ComboNepaliYearTo"
        Me.ComboNepaliYearTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearTo.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYearTo.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearTo.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYearTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYearTo.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYearTo.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliYearTo.TabIndex = 7
        '
        'ComboNEpaliMonthTo
        '
        Me.ComboNEpaliMonthTo.Location = New System.Drawing.Point(652, 13)
        Me.ComboNEpaliMonthTo.Name = "ComboNEpaliMonthTo"
        Me.ComboNEpaliMonthTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthTo.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonthTo.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthTo.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonthTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonthTo.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonthTo.Size = New System.Drawing.Size(66, 20)
        Me.ComboNEpaliMonthTo.TabIndex = 6
        '
        'ComboNepaliDateTo
        '
        Me.ComboNepaliDateTo.Location = New System.Drawing.Point(604, 13)
        Me.ComboNepaliDateTo.Name = "ComboNepaliDateTo"
        Me.ComboNepaliDateTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateTo.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliDateTo.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateTo.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliDateTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliDateTo.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNepaliDateTo.Size = New System.Drawing.Size(42, 20)
        Me.ComboNepaliDateTo.TabIndex = 5
        '
        'ComboNepaliYearFrm
        '
        Me.ComboNepaliYearFrm.Location = New System.Drawing.Point(480, 13)
        Me.ComboNepaliYearFrm.Name = "ComboNepaliYearFrm"
        Me.ComboNepaliYearFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearFrm.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYearFrm.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearFrm.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYearFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYearFrm.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYearFrm.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliYearFrm.TabIndex = 4
        '
        'ComboNEpaliMonthFrm
        '
        Me.ComboNEpaliMonthFrm.Location = New System.Drawing.Point(408, 13)
        Me.ComboNEpaliMonthFrm.Name = "ComboNEpaliMonthFrm"
        Me.ComboNEpaliMonthFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthFrm.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonthFrm.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthFrm.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonthFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonthFrm.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonthFrm.Size = New System.Drawing.Size(66, 20)
        Me.ComboNEpaliMonthFrm.TabIndex = 3
        '
        'ComboNepaliDateFrm
        '
        Me.ComboNepaliDateFrm.Location = New System.Drawing.Point(360, 13)
        Me.ComboNepaliDateFrm.Name = "ComboNepaliDateFrm"
        Me.ComboNepaliDateFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateFrm.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliDateFrm.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateFrm.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliDateFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliDateFrm.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNepaliDateFrm.Size = New System.Drawing.Size(42, 20)
        Me.ComboNepaliDateFrm.TabIndex = 2
        '
        'LookUpEdit1
        '
        Me.LookUpEdit1.Location = New System.Drawing.Point(102, 14)
        Me.LookUpEdit1.Name = "LookUpEdit1"
        Me.LookUpEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.Appearance.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceFocused.Options.UseFont = True
        Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PAYCODE", "PAYCODE", 30, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EMPNAME", "Name")})
        Me.LookUpEdit1.Properties.DataSource = Me.TblEmployeeBindingSource
        Me.LookUpEdit1.Properties.DisplayMember = "PAYCODE"
        Me.LookUpEdit1.Properties.MaxLength = 12
        Me.LookUpEdit1.Properties.NullText = ""
        Me.LookUpEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.LookUpEdit1.Properties.ValueMember = "PAYCODE"
        Me.LookUpEdit1.Size = New System.Drawing.Size(129, 20)
        Me.LookUpEdit1.TabIndex = 1
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.GridControl1)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel1.Location = New System.Drawing.Point(0, 328)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1036, 240)
        Me.SidePanel1.TabIndex = 32
        Me.SidePanel1.Text = "SidePanel1"
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 1)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(1036, 239)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPayCode, Me.colVOUCHER_NO, Me.colLeaveCode, Me.colForDate, Me.colDays, Me.colAppliedFor, Me.colHalfType, Me.colReason, Me.colApprovedDate, Me.colCOFUsedAgainst})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        '
        'colPayCode
        '
        Me.colPayCode.FieldName = "PayCode"
        Me.colPayCode.Name = "colPayCode"
        '
        'colVOUCHER_NO
        '
        Me.colVOUCHER_NO.Caption = "Voucher No"
        Me.colVOUCHER_NO.FieldName = "VOUCHER_NO"
        Me.colVOUCHER_NO.Name = "colVOUCHER_NO"
        Me.colVOUCHER_NO.Visible = True
        Me.colVOUCHER_NO.VisibleIndex = 0
        Me.colVOUCHER_NO.Width = 100
        '
        'colLeaveCode
        '
        Me.colLeaveCode.FieldName = "LeaveCode"
        Me.colLeaveCode.Name = "colLeaveCode"
        Me.colLeaveCode.Visible = True
        Me.colLeaveCode.VisibleIndex = 1
        Me.colLeaveCode.Width = 100
        '
        'colForDate
        '
        Me.colForDate.FieldName = "ForDate"
        Me.colForDate.Name = "colForDate"
        Me.colForDate.Visible = True
        Me.colForDate.VisibleIndex = 2
        Me.colForDate.Width = 100
        '
        'colDays
        '
        Me.colDays.Caption = "Days"
        Me.colDays.FieldName = "Days"
        Me.colDays.Name = "colDays"
        Me.colDays.Visible = True
        Me.colDays.VisibleIndex = 3
        '
        'colAppliedFor
        '
        Me.colAppliedFor.FieldName = "AppliedFor"
        Me.colAppliedFor.Name = "colAppliedFor"
        Me.colAppliedFor.Visible = True
        Me.colAppliedFor.VisibleIndex = 4
        Me.colAppliedFor.Width = 100
        '
        'colHalfType
        '
        Me.colHalfType.FieldName = "HalfType"
        Me.colHalfType.Name = "colHalfType"
        Me.colHalfType.Visible = True
        Me.colHalfType.VisibleIndex = 5
        Me.colHalfType.Width = 100
        '
        'colReason
        '
        Me.colReason.FieldName = "Reason"
        Me.colReason.Name = "colReason"
        Me.colReason.Visible = True
        Me.colReason.VisibleIndex = 6
        Me.colReason.Width = 120
        '
        'colApprovedDate
        '
        Me.colApprovedDate.FieldName = "ApprovedDate"
        Me.colApprovedDate.Name = "colApprovedDate"
        Me.colApprovedDate.Visible = True
        Me.colApprovedDate.VisibleIndex = 7
        Me.colApprovedDate.Width = 120
        '
        'colCOFUsedAgainst
        '
        Me.colCOFUsedAgainst.Caption = "Comp Off Used Against"
        Me.colCOFUsedAgainst.FieldName = "COFUsedAgainst"
        Me.colCOFUsedAgainst.Name = "colCOFUsedAgainst"
        Me.colCOFUsedAgainst.Visible = True
        Me.colCOFUsedAgainst.VisibleIndex = 8
        Me.colCOFUsedAgainst.Width = 120
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.CheckEdit6)
        Me.GroupControl1.Controls.Add(Me.CheckEdit5)
        Me.GroupControl1.Controls.Add(Me.CheckEdit4)
        Me.GroupControl1.Controls.Add(Me.CheckEdit3)
        Me.GroupControl1.Controls.Add(Me.CheckEdit2)
        Me.GroupControl1.Controls.Add(Me.CheckEdit1)
        Me.GroupControl1.Location = New System.Drawing.Point(290, 138)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(335, 184)
        Me.GroupControl1.TabIndex = 13
        Me.GroupControl1.Text = "Leave Duration"
        '
        'CheckEdit6
        '
        Me.CheckEdit6.Location = New System.Drawing.Point(176, 84)
        Me.CheckEdit6.Name = "CheckEdit6"
        Me.CheckEdit6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit6.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit6.Properties.Caption = "Second Half"
        Me.CheckEdit6.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit6.Properties.RadioGroupIndex = 1
        Me.CheckEdit6.Size = New System.Drawing.Size(116, 19)
        Me.CheckEdit6.TabIndex = 4
        Me.CheckEdit6.TabStop = False
        Me.CheckEdit6.Visible = False
        '
        'CheckEdit5
        '
        Me.CheckEdit5.EditValue = True
        Me.CheckEdit5.Location = New System.Drawing.Point(176, 61)
        Me.CheckEdit5.Name = "CheckEdit5"
        Me.CheckEdit5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit5.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit5.Properties.Caption = "First Half"
        Me.CheckEdit5.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit5.Properties.RadioGroupIndex = 1
        Me.CheckEdit5.Size = New System.Drawing.Size(116, 19)
        Me.CheckEdit5.TabIndex = 3
        Me.CheckEdit5.Visible = False
        '
        'CheckEdit4
        '
        Me.CheckEdit4.EditValue = True
        Me.CheckEdit4.Location = New System.Drawing.Point(20, 109)
        Me.CheckEdit4.Name = "CheckEdit4"
        Me.CheckEdit4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit4.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit4.Properties.Caption = "Full Day"
        Me.CheckEdit4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit4.Properties.RadioGroupIndex = 0
        Me.CheckEdit4.Size = New System.Drawing.Size(116, 19)
        Me.CheckEdit4.TabIndex = 6
        '
        'CheckEdit3
        '
        Me.CheckEdit3.Location = New System.Drawing.Point(20, 84)
        Me.CheckEdit3.Name = "CheckEdit3"
        Me.CheckEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit3.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit3.Properties.Caption = "Three Forth"
        Me.CheckEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit3.Properties.RadioGroupIndex = 0
        Me.CheckEdit3.Size = New System.Drawing.Size(116, 19)
        Me.CheckEdit3.TabIndex = 5
        Me.CheckEdit3.TabStop = False
        '
        'CheckEdit2
        '
        Me.CheckEdit2.Location = New System.Drawing.Point(20, 61)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit2.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit2.Properties.Caption = "Half"
        Me.CheckEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit2.Properties.RadioGroupIndex = 0
        Me.CheckEdit2.Size = New System.Drawing.Size(116, 19)
        Me.CheckEdit2.TabIndex = 2
        Me.CheckEdit2.TabStop = False
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(20, 36)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit1.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit1.Properties.Caption = "Quarter"
        Me.CheckEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit1.Properties.RadioGroupIndex = 0
        Me.CheckEdit1.Size = New System.Drawing.Size(116, 19)
        Me.CheckEdit1.TabIndex = 1
        Me.CheckEdit1.TabStop = False
        '
        'DateEdit3
        '
        Me.DateEdit3.EditValue = Nothing
        Me.DateEdit3.Location = New System.Drawing.Point(383, 108)
        Me.DateEdit3.Name = "DateEdit3"
        Me.DateEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit3.Properties.Appearance.Options.UseFont = True
        Me.DateEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit3.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit3.Size = New System.Drawing.Size(112, 20)
        Me.DateEdit3.TabIndex = 10
        '
        'LabelControl24
        '
        Me.LabelControl24.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl24.Appearance.Options.UseFont = True
        Me.LabelControl24.Location = New System.Drawing.Point(293, 111)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(76, 14)
        Me.LabelControl24.TabIndex = 31
        Me.LabelControl24.Text = "Approve Date"
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(383, 83)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.MaxLength = 25
        Me.TextEdit1.Size = New System.Drawing.Size(166, 20)
        Me.TextEdit1.TabIndex = 9
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl23.Appearance.Options.UseFont = True
        Me.LabelControl23.Location = New System.Drawing.Point(293, 85)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(39, 14)
        Me.LabelControl23.TabIndex = 29
        Me.LabelControl23.Text = "Reason"
        '
        'GridLookUpEditLeave
        '
        Me.GridLookUpEditLeave.Location = New System.Drawing.Point(383, 57)
        Me.GridLookUpEditLeave.Name = "GridLookUpEditLeave"
        Me.GridLookUpEditLeave.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditLeave.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditLeave.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditLeave.Properties.AppearanceDisabled.Options.UseFont = True
        Me.GridLookUpEditLeave.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditLeave.Properties.AppearanceDropDown.Options.UseFont = True
        Me.GridLookUpEditLeave.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditLeave.Properties.AppearanceFocused.Options.UseFont = True
        Me.GridLookUpEditLeave.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditLeave.Properties.NullText = ""
        Me.GridLookUpEditLeave.Properties.View = Me.GridLookUpEdit1View
        Me.GridLookUpEditLeave.Size = New System.Drawing.Size(166, 20)
        Me.GridLookUpEditLeave.TabIndex = 8
        '
        'GridLookUpEdit1View
        '
        Me.GridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit1View.Name = "GridLookUpEdit1View"
        Me.GridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl22.Appearance.Options.UseFont = True
        Me.LabelControl22.Location = New System.Drawing.Point(293, 60)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(64, 14)
        Me.LabelControl22.TabIndex = 27
        Me.LabelControl22.Text = "Leave Code"
        '
        'lblGrade
        '
        Me.lblGrade.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblGrade.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblGrade.Appearance.Options.UseFont = True
        Me.lblGrade.Appearance.Options.UseForeColor = True
        Me.lblGrade.Location = New System.Drawing.Point(127, 282)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(20, 14)
        Me.lblGrade.TabIndex = 26
        Me.lblGrade.Text = "     "
        '
        'lblEmpGrp
        '
        Me.lblEmpGrp.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblEmpGrp.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblEmpGrp.Appearance.Options.UseFont = True
        Me.lblEmpGrp.Appearance.Options.UseForeColor = True
        Me.lblEmpGrp.Location = New System.Drawing.Point(127, 253)
        Me.lblEmpGrp.Name = "lblEmpGrp"
        Me.lblEmpGrp.Size = New System.Drawing.Size(20, 14)
        Me.lblEmpGrp.TabIndex = 25
        Me.lblEmpGrp.Text = "     "
        '
        'lblCat
        '
        Me.lblCat.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblCat.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblCat.Appearance.Options.UseFont = True
        Me.lblCat.Appearance.Options.UseForeColor = True
        Me.lblCat.Location = New System.Drawing.Point(127, 224)
        Me.lblCat.Name = "lblCat"
        Me.lblCat.Size = New System.Drawing.Size(20, 14)
        Me.lblCat.TabIndex = 24
        Me.lblCat.Text = "     "
        '
        'lblDept
        '
        Me.lblDept.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblDept.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblDept.Appearance.Options.UseFont = True
        Me.lblDept.Appearance.Options.UseForeColor = True
        Me.lblDept.Location = New System.Drawing.Point(127, 195)
        Me.lblDept.Name = "lblDept"
        Me.lblDept.Size = New System.Drawing.Size(24, 14)
        Me.lblDept.TabIndex = 23
        Me.lblDept.Text = "      "
        '
        'lblComp
        '
        Me.lblComp.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblComp.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblComp.Appearance.Options.UseFont = True
        Me.lblComp.Appearance.Options.UseForeColor = True
        Me.lblComp.Location = New System.Drawing.Point(127, 167)
        Me.lblComp.Name = "lblComp"
        Me.lblComp.Size = New System.Drawing.Size(20, 14)
        Me.lblComp.TabIndex = 22
        Me.lblComp.Text = "     "
        '
        'lblDesi
        '
        Me.lblDesi.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblDesi.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblDesi.Appearance.Options.UseFont = True
        Me.lblDesi.Appearance.Options.UseForeColor = True
        Me.lblDesi.Location = New System.Drawing.Point(127, 138)
        Me.lblDesi.Name = "lblDesi"
        Me.lblDesi.Size = New System.Drawing.Size(20, 14)
        Me.lblDesi.TabIndex = 21
        Me.lblDesi.Text = "     "
        '
        'lblCardNum
        '
        Me.lblCardNum.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblCardNum.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblCardNum.Appearance.Options.UseFont = True
        Me.lblCardNum.Appearance.Options.UseForeColor = True
        Me.lblCardNum.Location = New System.Drawing.Point(127, 111)
        Me.lblCardNum.Name = "lblCardNum"
        Me.lblCardNum.Size = New System.Drawing.Size(24, 14)
        Me.lblCardNum.TabIndex = 20
        Me.lblCardNum.Text = "      "
        '
        'lblName
        '
        Me.lblName.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblName.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblName.Appearance.Options.UseFont = True
        Me.lblName.Appearance.Options.UseForeColor = True
        Me.lblName.Location = New System.Drawing.Point(127, 85)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(20, 14)
        Me.lblName.TabIndex = 19
        Me.lblName.Text = "     "
        '
        'lblVoucher
        '
        Me.lblVoucher.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblVoucher.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblVoucher.Appearance.Options.UseFont = True
        Me.lblVoucher.Appearance.Options.UseForeColor = True
        Me.lblVoucher.Location = New System.Drawing.Point(127, 60)
        Me.lblVoucher.Name = "lblVoucher"
        Me.lblVoucher.Size = New System.Drawing.Size(20, 14)
        Me.lblVoucher.TabIndex = 18
        Me.lblVoucher.Text = "     "
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(15, 282)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(32, 14)
        Me.LabelControl12.TabIndex = 17
        Me.LabelControl12.Text = "Grade"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(15, 253)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(90, 14)
        Me.LabelControl11.TabIndex = 16
        Me.LabelControl11.Text = "Employee Group"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(15, 224)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(48, 14)
        Me.LabelControl10.TabIndex = 15
        Me.LabelControl10.Text = "Catagory"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(15, 195)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl9.TabIndex = 14
        Me.LabelControl9.Text = "Department"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(15, 167)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(50, 14)
        Me.LabelControl8.TabIndex = 13
        Me.LabelControl8.Text = "Company"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(15, 138)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(63, 14)
        Me.LabelControl7.TabIndex = 12
        Me.LabelControl7.Text = "Designation"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(15, 111)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(47, 14)
        Me.LabelControl6.TabIndex = 11
        Me.LabelControl6.Text = "Card No."
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(15, 85)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(31, 14)
        Me.LabelControl5.TabIndex = 10
        Me.LabelControl5.Text = "Name"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(15, 60)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(69, 14)
        Me.LabelControl4.TabIndex = 9
        Me.LabelControl4.Text = "Voucher No."
        '
        'DateEdit2
        '
        Me.DateEdit2.EditValue = Nothing
        Me.DateEdit2.Location = New System.Drawing.Point(622, 13)
        Me.DateEdit2.Name = "DateEdit2"
        Me.DateEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit2.Properties.Appearance.Options.UseFont = True
        Me.DateEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Size = New System.Drawing.Size(112, 20)
        Me.DateEdit2.TabIndex = 3
        '
        'DateEdit1
        '
        Me.DateEdit1.EditValue = Nothing
        Me.DateEdit1.Location = New System.Drawing.Point(360, 13)
        Me.DateEdit1.Name = "DateEdit1"
        Me.DateEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit1.Properties.Appearance.Options.UseFont = True
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Size = New System.Drawing.Size(112, 20)
        Me.DateEdit1.TabIndex = 2
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(553, 16)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(45, 14)
        Me.LabelControl3.TabIndex = 6
        Me.LabelControl3.Text = "To Date"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(288, 16)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(57, 14)
        Me.LabelControl2.TabIndex = 5
        Me.LabelControl2.Text = "From Date"
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(102, 40)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit2.Properties.Appearance.Options.UseFont = True
        Me.TextEdit2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit2.Properties.MaxLength = 12
        Me.TextEdit2.Size = New System.Drawing.Size(129, 20)
        Me.TextEdit2.TabIndex = 1
        Me.TextEdit2.Visible = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(15, 16)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(46, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Paycode"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(101, 568)
        Me.MemoEdit1.TabIndex = 1
        '
        'TblLeaveMasterBindingSource
        '
        Me.TblLeaveMasterBindingSource.DataMember = "tblLeaveMaster"
        Me.TblLeaveMasterBindingSource.DataSource = Me.SSSDBDataSet
        '
        'LeaveApplicationBindingSource
        '
        Me.LeaveApplicationBindingSource.DataMember = "LeaveApplication"
        Me.LeaveApplicationBindingSource.DataSource = Me.SSSDBDataSet
        '
        'LeaveApplicationTableAdapter
        '
        Me.LeaveApplicationTableAdapter.ClearBeforeFill = True
        '
        'LeaveApplication1TableAdapter1
        '
        Me.LeaveApplication1TableAdapter1.ClearBeforeFill = True
        '
        'TblLeaveMasterTableAdapter
        '
        Me.TblLeaveMasterTableAdapter.ClearBeforeFill = True
        '
        'TblLeaveMaster1TableAdapter1
        '
        Me.TblLeaveMaster1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'XtraLeaveApplication
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "XtraLeaveApplication"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.ComboCmpAgn.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliYearApp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonthApp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliDateApp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliYearTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonthTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliDateTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliYearFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonthFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliDateFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.CheckEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit3.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditLeave.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblLeaveMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LeaveApplicationBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblGrade As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEmpGrp As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCat As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblDept As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblComp As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblDesi As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCardNum As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblVoucher As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit2 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridLookUpEditLeave As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DateEdit3 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEdit4 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit6 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit5 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LeaveApplicationBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As ULtra.SSSDBDataSet
    Friend WithEvents LeaveApplicationTableAdapter As ULtra.SSSDBDataSetTableAdapters.LeaveApplicationTableAdapter
    Friend WithEvents LeaveApplication1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.LeaveApplication1TableAdapter
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colPayCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLeaveCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colForDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAppliedFor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHalfType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colReason As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colApprovedDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblLeaveMasterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblLeaveMasterTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblLeaveMasterTableAdapter
    Friend WithEvents TblLeaveMaster1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblLeaveMaster1TableAdapter
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colDays As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOUCHER_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblEmployeeTableAdapter As ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents TblEmployee1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents ComboNepaliYearTo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonthTo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliDateTo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliYearFrm As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonthFrm As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliDateFrm As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliYearApp As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonthApp As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliDateApp As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboCmpAgn As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents colCOFUsedAgainst As DevExpress.XtraGrid.Columns.GridColumn

End Class
