﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraUserMgmt
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraUserMgmt))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.TblUserBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New ULtra.SSSDBDataSet()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colUSER_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUSERDESCRIPRION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPASSWORD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAutoProcess = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDataProcess = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMain = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colV_Transaction = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAdmin = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPayroll = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colReports = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLeave = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCompany = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDepartment = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSection = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGrade = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCategory = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colShift = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEmployee = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVisitor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colReason_Card = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colManual_Attendance = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOstoOt = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colShiftChange = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHoliDay = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLeaveMaster = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLeaveApplication = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLeaveAccural = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLeaveAccuralAuto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTimeOfficeSetup = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUserPrevilege = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVerification = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colInstallationSetup = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEmployeeSetup = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colArearEntry = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAdvance_Loan = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colForm16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colForm16Return = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colpayrollFormula = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPayrollSetup = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLoanAdjustment = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTimeOfficeReport = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVisitorReport = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPayrollReport = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colRegisterCreation = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colRegisterUpdation = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBackDateProcess = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colReProcess = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTCAL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colauth_comp = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAuth_dept = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUSERTYPE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colpaycode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCompAdd = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCompModi = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCompDel = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDeptAdd = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDeptModi = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDeptDel = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCatAdd = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCatModi = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCatDel = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSecAdd = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSecModi = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSecDel = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGrdAdd = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGrdModi = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGrdDel = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSftAdd = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSftModi = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSftDel = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEmpAdd = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEmpModi = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEmpDel = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDataMaintenance = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedBy = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemTimeEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.RepositoryItemTextEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemTimeSpanEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeSpanEdit()
        Me.RepositoryItemTimeEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.RepositoryItemTextEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblUserTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblUserTableAdapter()
        Me.TblUser1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.tblUser1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblUserBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeSpanEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GridControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblUserBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEdit1, Me.RepositoryItemTimeEdit1, Me.RepositoryItemTextEdit2, Me.RepositoryItemTimeSpanEdit1, Me.RepositoryItemTimeEdit2, Me.RepositoryItemComboBox1, Me.RepositoryItemTextEdit3})
        Me.GridControl1.Size = New System.Drawing.Size(1036, 568)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'TblUserBindingSource
        '
        Me.TblUserBindingSource.DataMember = "tblUser"
        Me.TblUserBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Appearance.TopNewRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GridView1.Appearance.TopNewRow.ForeColor = System.Drawing.Color.Blue
        Me.GridView1.Appearance.TopNewRow.Options.UseFont = True
        Me.GridView1.Appearance.TopNewRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colUSER_R, Me.colUSERDESCRIPRION, Me.colPASSWORD, Me.colAutoProcess, Me.colDataProcess, Me.colMain, Me.colV_Transaction, Me.colAdmin, Me.colPayroll, Me.colReports, Me.colLeave, Me.colCompany, Me.colDepartment, Me.colSection, Me.colGrade, Me.colCategory, Me.colShift, Me.colEmployee, Me.colVisitor, Me.colReason_Card, Me.colManual_Attendance, Me.colOstoOt, Me.colShiftChange, Me.colHoliDay, Me.colLeaveMaster, Me.colLeaveApplication, Me.colLeaveAccural, Me.colLeaveAccuralAuto, Me.colTimeOfficeSetup, Me.colUserPrevilege, Me.colVerification, Me.colInstallationSetup, Me.colEmployeeSetup, Me.colArearEntry, Me.colAdvance_Loan, Me.colForm16, Me.colForm16Return, Me.colpayrollFormula, Me.colPayrollSetup, Me.colLoanAdjustment, Me.colTimeOfficeReport, Me.colVisitorReport, Me.colPayrollReport, Me.colRegisterCreation, Me.colRegisterUpdation, Me.colBackDateProcess, Me.colReProcess, Me.colOTCAL, Me.colauth_comp, Me.colAuth_dept, Me.colUSERTYPE, Me.colpaycode, Me.colCompAdd, Me.colCompModi, Me.colCompDel, Me.colDeptAdd, Me.colDeptModi, Me.colDeptDel, Me.colCatAdd, Me.colCatModi, Me.colCatDel, Me.colSecAdd, Me.colSecModi, Me.colSecDel, Me.colGrdAdd, Me.colGrdModi, Me.colGrdDel, Me.colSftAdd, Me.colSftModi, Me.colSftDel, Me.colEmpAdd, Me.colEmpModi, Me.colEmpDel, Me.colDataMaintenance, Me.colLastModifiedBy, Me.colLastModifiedDate})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.NewItemRowText = "Click here to add new User"
        Me.GridView1.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm
        Me.GridView1.OptionsEditForm.EditFormColumnCount = 1
        Me.GridView1.OptionsEditForm.PopupEditFormWidth = 400
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.GridView1.OptionsView.ShowFooter = True
        '
        'colUSER_R
        '
        Me.colUSER_R.Caption = "User"
        Me.colUSER_R.FieldName = "USER_R"
        Me.colUSER_R.Name = "colUSER_R"
        Me.colUSER_R.Visible = True
        Me.colUSER_R.VisibleIndex = 0
        '
        'colUSERDESCRIPRION
        '
        Me.colUSERDESCRIPRION.Caption = "User Description"
        Me.colUSERDESCRIPRION.FieldName = "USERDESCRIPRION"
        Me.colUSERDESCRIPRION.Name = "colUSERDESCRIPRION"
        Me.colUSERDESCRIPRION.Visible = True
        Me.colUSERDESCRIPRION.VisibleIndex = 1
        '
        'colPASSWORD
        '
        Me.colPASSWORD.Caption = "Password"
        Me.colPASSWORD.FieldName = "PASSWORD"
        Me.colPASSWORD.Name = "colPASSWORD"
        '
        'colAutoProcess
        '
        Me.colAutoProcess.Caption = "Auto Process"
        Me.colAutoProcess.FieldName = "AutoProcess"
        Me.colAutoProcess.Name = "colAutoProcess"
        Me.colAutoProcess.Visible = True
        Me.colAutoProcess.VisibleIndex = 2
        '
        'colDataProcess
        '
        Me.colDataProcess.Caption = "Data Process"
        Me.colDataProcess.FieldName = "DataProcess"
        Me.colDataProcess.Name = "colDataProcess"
        Me.colDataProcess.Visible = True
        Me.colDataProcess.VisibleIndex = 3
        '
        'colMain
        '
        Me.colMain.Caption = "Master"
        Me.colMain.FieldName = "Main"
        Me.colMain.Name = "colMain"
        Me.colMain.Visible = True
        Me.colMain.VisibleIndex = 4
        '
        'colV_Transaction
        '
        Me.colV_Transaction.Caption = "Transaction"
        Me.colV_Transaction.FieldName = "V_Transaction"
        Me.colV_Transaction.Name = "colV_Transaction"
        Me.colV_Transaction.Visible = True
        Me.colV_Transaction.VisibleIndex = 5
        '
        'colAdmin
        '
        Me.colAdmin.Caption = "Admin"
        Me.colAdmin.FieldName = "Admin"
        Me.colAdmin.Name = "colAdmin"
        Me.colAdmin.Visible = True
        Me.colAdmin.VisibleIndex = 6
        '
        'colPayroll
        '
        Me.colPayroll.Caption = "Payroll"
        Me.colPayroll.FieldName = "Payroll"
        Me.colPayroll.Name = "colPayroll"
        Me.colPayroll.Visible = True
        Me.colPayroll.VisibleIndex = 7
        '
        'colReports
        '
        Me.colReports.Caption = "Reports"
        Me.colReports.FieldName = "Reports"
        Me.colReports.Name = "colReports"
        Me.colReports.Visible = True
        Me.colReports.VisibleIndex = 8
        '
        'colLeave
        '
        Me.colLeave.Caption = "Leave"
        Me.colLeave.FieldName = "Leave"
        Me.colLeave.Name = "colLeave"
        Me.colLeave.Visible = True
        Me.colLeave.VisibleIndex = 9
        '
        'colCompany
        '
        Me.colCompany.Caption = "Company"
        Me.colCompany.FieldName = "Company"
        Me.colCompany.Name = "colCompany"
        Me.colCompany.Visible = True
        Me.colCompany.VisibleIndex = 10
        '
        'colDepartment
        '
        Me.colDepartment.Caption = "Departent"
        Me.colDepartment.FieldName = "Department"
        Me.colDepartment.Name = "colDepartment"
        Me.colDepartment.Visible = True
        Me.colDepartment.VisibleIndex = 11
        '
        'colSection
        '
        Me.colSection.Caption = "Employee Group"
        Me.colSection.FieldName = "Section"
        Me.colSection.Name = "colSection"
        Me.colSection.Visible = True
        Me.colSection.VisibleIndex = 12
        '
        'colGrade
        '
        Me.colGrade.Caption = "Grade"
        Me.colGrade.FieldName = "Grade"
        Me.colGrade.Name = "colGrade"
        Me.colGrade.Visible = True
        Me.colGrade.VisibleIndex = 13
        '
        'colCategory
        '
        Me.colCategory.Caption = "Category"
        Me.colCategory.FieldName = "Category"
        Me.colCategory.Name = "colCategory"
        Me.colCategory.Visible = True
        Me.colCategory.VisibleIndex = 14
        '
        'colShift
        '
        Me.colShift.FieldName = "Shift"
        Me.colShift.Name = "colShift"
        Me.colShift.Visible = True
        Me.colShift.VisibleIndex = 15
        '
        'colEmployee
        '
        Me.colEmployee.FieldName = "Employee"
        Me.colEmployee.Name = "colEmployee"
        Me.colEmployee.Visible = True
        Me.colEmployee.VisibleIndex = 16
        '
        'colVisitor
        '
        Me.colVisitor.FieldName = "Visitor"
        Me.colVisitor.Name = "colVisitor"
        Me.colVisitor.Visible = True
        Me.colVisitor.VisibleIndex = 17
        '
        'colReason_Card
        '
        Me.colReason_Card.FieldName = "Reason_Card"
        Me.colReason_Card.Name = "colReason_Card"
        '
        'colManual_Attendance
        '
        Me.colManual_Attendance.FieldName = "Manual_Attendance"
        Me.colManual_Attendance.Name = "colManual_Attendance"
        Me.colManual_Attendance.Visible = True
        Me.colManual_Attendance.VisibleIndex = 18
        '
        'colOstoOt
        '
        Me.colOstoOt.FieldName = "OstoOt"
        Me.colOstoOt.Name = "colOstoOt"
        '
        'colShiftChange
        '
        Me.colShiftChange.FieldName = "ShiftChange"
        Me.colShiftChange.Name = "colShiftChange"
        '
        'colHoliDay
        '
        Me.colHoliDay.FieldName = "HoliDay"
        Me.colHoliDay.Name = "colHoliDay"
        '
        'colLeaveMaster
        '
        Me.colLeaveMaster.FieldName = "LeaveMaster"
        Me.colLeaveMaster.Name = "colLeaveMaster"
        '
        'colLeaveApplication
        '
        Me.colLeaveApplication.FieldName = "LeaveApplication"
        Me.colLeaveApplication.Name = "colLeaveApplication"
        '
        'colLeaveAccural
        '
        Me.colLeaveAccural.FieldName = "LeaveAccural"
        Me.colLeaveAccural.Name = "colLeaveAccural"
        '
        'colLeaveAccuralAuto
        '
        Me.colLeaveAccuralAuto.FieldName = "LeaveAccuralAuto"
        Me.colLeaveAccuralAuto.Name = "colLeaveAccuralAuto"
        '
        'colTimeOfficeSetup
        '
        Me.colTimeOfficeSetup.FieldName = "TimeOfficeSetup"
        Me.colTimeOfficeSetup.Name = "colTimeOfficeSetup"
        '
        'colUserPrevilege
        '
        Me.colUserPrevilege.FieldName = "UserPrevilege"
        Me.colUserPrevilege.Name = "colUserPrevilege"
        '
        'colVerification
        '
        Me.colVerification.FieldName = "Verification"
        Me.colVerification.Name = "colVerification"
        '
        'colInstallationSetup
        '
        Me.colInstallationSetup.FieldName = "InstallationSetup"
        Me.colInstallationSetup.Name = "colInstallationSetup"
        '
        'colEmployeeSetup
        '
        Me.colEmployeeSetup.FieldName = "EmployeeSetup"
        Me.colEmployeeSetup.Name = "colEmployeeSetup"
        '
        'colArearEntry
        '
        Me.colArearEntry.FieldName = "ArearEntry"
        Me.colArearEntry.Name = "colArearEntry"
        '
        'colAdvance_Loan
        '
        Me.colAdvance_Loan.FieldName = "Advance_Loan"
        Me.colAdvance_Loan.Name = "colAdvance_Loan"
        '
        'colForm16
        '
        Me.colForm16.FieldName = "Form16"
        Me.colForm16.Name = "colForm16"
        '
        'colForm16Return
        '
        Me.colForm16Return.FieldName = "Form16Return"
        Me.colForm16Return.Name = "colForm16Return"
        '
        'colpayrollFormula
        '
        Me.colpayrollFormula.FieldName = "payrollFormula"
        Me.colpayrollFormula.Name = "colpayrollFormula"
        '
        'colPayrollSetup
        '
        Me.colPayrollSetup.FieldName = "PayrollSetup"
        Me.colPayrollSetup.Name = "colPayrollSetup"
        '
        'colLoanAdjustment
        '
        Me.colLoanAdjustment.FieldName = "LoanAdjustment"
        Me.colLoanAdjustment.Name = "colLoanAdjustment"
        '
        'colTimeOfficeReport
        '
        Me.colTimeOfficeReport.FieldName = "TimeOfficeReport"
        Me.colTimeOfficeReport.Name = "colTimeOfficeReport"
        '
        'colVisitorReport
        '
        Me.colVisitorReport.FieldName = "VisitorReport"
        Me.colVisitorReport.Name = "colVisitorReport"
        '
        'colPayrollReport
        '
        Me.colPayrollReport.FieldName = "PayrollReport"
        Me.colPayrollReport.Name = "colPayrollReport"
        '
        'colRegisterCreation
        '
        Me.colRegisterCreation.FieldName = "RegisterCreation"
        Me.colRegisterCreation.Name = "colRegisterCreation"
        '
        'colRegisterUpdation
        '
        Me.colRegisterUpdation.FieldName = "RegisterUpdation"
        Me.colRegisterUpdation.Name = "colRegisterUpdation"
        '
        'colBackDateProcess
        '
        Me.colBackDateProcess.FieldName = "BackDateProcess"
        Me.colBackDateProcess.Name = "colBackDateProcess"
        '
        'colReProcess
        '
        Me.colReProcess.FieldName = "ReProcess"
        Me.colReProcess.Name = "colReProcess"
        '
        'colOTCAL
        '
        Me.colOTCAL.FieldName = "OTCAL"
        Me.colOTCAL.Name = "colOTCAL"
        '
        'colauth_comp
        '
        Me.colauth_comp.FieldName = "auth_comp"
        Me.colauth_comp.Name = "colauth_comp"
        '
        'colAuth_dept
        '
        Me.colAuth_dept.FieldName = "Auth_dept"
        Me.colAuth_dept.Name = "colAuth_dept"
        '
        'colUSERTYPE
        '
        Me.colUSERTYPE.FieldName = "USERTYPE"
        Me.colUSERTYPE.Name = "colUSERTYPE"
        '
        'colpaycode
        '
        Me.colpaycode.FieldName = "paycode"
        Me.colpaycode.Name = "colpaycode"
        '
        'colCompAdd
        '
        Me.colCompAdd.FieldName = "CompAdd"
        Me.colCompAdd.Name = "colCompAdd"
        '
        'colCompModi
        '
        Me.colCompModi.FieldName = "CompModi"
        Me.colCompModi.Name = "colCompModi"
        '
        'colCompDel
        '
        Me.colCompDel.FieldName = "CompDel"
        Me.colCompDel.Name = "colCompDel"
        '
        'colDeptAdd
        '
        Me.colDeptAdd.FieldName = "DeptAdd"
        Me.colDeptAdd.Name = "colDeptAdd"
        '
        'colDeptModi
        '
        Me.colDeptModi.FieldName = "DeptModi"
        Me.colDeptModi.Name = "colDeptModi"
        '
        'colDeptDel
        '
        Me.colDeptDel.FieldName = "DeptDel"
        Me.colDeptDel.Name = "colDeptDel"
        '
        'colCatAdd
        '
        Me.colCatAdd.FieldName = "CatAdd"
        Me.colCatAdd.Name = "colCatAdd"
        '
        'colCatModi
        '
        Me.colCatModi.FieldName = "CatModi"
        Me.colCatModi.Name = "colCatModi"
        '
        'colCatDel
        '
        Me.colCatDel.FieldName = "CatDel"
        Me.colCatDel.Name = "colCatDel"
        '
        'colSecAdd
        '
        Me.colSecAdd.FieldName = "SecAdd"
        Me.colSecAdd.Name = "colSecAdd"
        '
        'colSecModi
        '
        Me.colSecModi.FieldName = "SecModi"
        Me.colSecModi.Name = "colSecModi"
        '
        'colSecDel
        '
        Me.colSecDel.FieldName = "SecDel"
        Me.colSecDel.Name = "colSecDel"
        '
        'colGrdAdd
        '
        Me.colGrdAdd.FieldName = "GrdAdd"
        Me.colGrdAdd.Name = "colGrdAdd"
        '
        'colGrdModi
        '
        Me.colGrdModi.FieldName = "GrdModi"
        Me.colGrdModi.Name = "colGrdModi"
        '
        'colGrdDel
        '
        Me.colGrdDel.FieldName = "GrdDel"
        Me.colGrdDel.Name = "colGrdDel"
        '
        'colSftAdd
        '
        Me.colSftAdd.FieldName = "SftAdd"
        Me.colSftAdd.Name = "colSftAdd"
        '
        'colSftModi
        '
        Me.colSftModi.FieldName = "SftModi"
        Me.colSftModi.Name = "colSftModi"
        '
        'colSftDel
        '
        Me.colSftDel.FieldName = "SftDel"
        Me.colSftDel.Name = "colSftDel"
        '
        'colEmpAdd
        '
        Me.colEmpAdd.FieldName = "EmpAdd"
        Me.colEmpAdd.Name = "colEmpAdd"
        '
        'colEmpModi
        '
        Me.colEmpModi.FieldName = "EmpModi"
        Me.colEmpModi.Name = "colEmpModi"
        '
        'colEmpDel
        '
        Me.colEmpDel.FieldName = "EmpDel"
        Me.colEmpDel.Name = "colEmpDel"
        '
        'colDataMaintenance
        '
        Me.colDataMaintenance.FieldName = "DataMaintenance"
        Me.colDataMaintenance.Name = "colDataMaintenance"
        '
        'colLastModifiedBy
        '
        Me.colLastModifiedBy.FieldName = "LastModifiedBy"
        Me.colLastModifiedBy.Name = "colLastModifiedBy"
        '
        'colLastModifiedDate
        '
        Me.colLastModifiedDate.FieldName = "LastModifiedDate"
        Me.colLastModifiedDate.Name = "colLastModifiedDate"
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.RepositoryItemTextEdit1.MaxLength = 3
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'RepositoryItemTimeEdit1
        '
        Me.RepositoryItemTimeEdit1.AutoHeight = False
        Me.RepositoryItemTimeEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit1.EditFormat.FormatString = "HH:mm"
        Me.RepositoryItemTimeEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.RepositoryItemTimeEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit1.MaxLength = 5
        Me.RepositoryItemTimeEdit1.Name = "RepositoryItemTimeEdit1"
        '
        'RepositoryItemTextEdit2
        '
        Me.RepositoryItemTextEdit2.AutoHeight = False
        Me.RepositoryItemTextEdit2.DisplayFormat.FormatString = "HH:mm"
        Me.RepositoryItemTextEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemTextEdit2.EditFormat.FormatString = "HH:mm"
        Me.RepositoryItemTextEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemTextEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.RepositoryItemTextEdit2.Name = "RepositoryItemTextEdit2"
        '
        'RepositoryItemTimeSpanEdit1
        '
        Me.RepositoryItemTimeSpanEdit1.AutoHeight = False
        Me.RepositoryItemTimeSpanEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeSpanEdit1.DisplayFormat.FormatString = "HH:mm"
        Me.RepositoryItemTimeSpanEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.RepositoryItemTimeSpanEdit1.EditFormat.FormatString = "HH:mm"
        Me.RepositoryItemTimeSpanEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.RepositoryItemTimeSpanEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeSpanEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Custom
        Me.RepositoryItemTimeSpanEdit1.Name = "RepositoryItemTimeSpanEdit1"
        Me.RepositoryItemTimeSpanEdit1.TimeEditStyle = DevExpress.XtraEditors.Repository.TimeEditStyle.SpinButtons
        '
        'RepositoryItemTimeEdit2
        '
        Me.RepositoryItemTimeEdit2.AutoHeight = False
        Me.RepositoryItemTimeEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit2.MaxLength = 5
        Me.RepositoryItemTimeEdit2.Name = "RepositoryItemTimeEdit2"
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Items.AddRange(New Object() {"DAY", "NIGHT", "HALFDAY"})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'RepositoryItemTextEdit3
        '
        Me.RepositoryItemTextEdit3.AutoHeight = False
        Me.RepositoryItemTextEdit3.Mask.EditMask = "([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]"
        Me.RepositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.RepositoryItemTextEdit3.Name = "RepositoryItemTextEdit3"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(101, 568)
        Me.MemoEdit1.TabIndex = 1
        '
        'TblUserTableAdapter
        '
        Me.TblUserTableAdapter.ClearBeforeFill = True
        '
        'TblUser1TableAdapter1
        '
        Me.TblUser1TableAdapter1.ClearBeforeFill = True
        '
        'XtraUserMgmt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraUserMgmt"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblUserBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeSpanEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTimeEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents RepositoryItemTextEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents RepositoryItemTextEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTimeSpanEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTimeSpanEdit
    Friend WithEvents RepositoryItemTimeEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TblUserBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As ULtra.SSSDBDataSet
    Friend WithEvents colUSER_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colUSERDESCRIPRION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPASSWORD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAutoProcess As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDataProcess As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMain As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colV_Transaction As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAdmin As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPayroll As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colReports As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLeave As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCompany As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDepartment As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSection As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGrade As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCategory As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colShift As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEmployee As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVisitor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colReason_Card As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colManual_Attendance As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOstoOt As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colShiftChange As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHoliDay As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLeaveMaster As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLeaveApplication As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLeaveAccural As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLeaveAccuralAuto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTimeOfficeSetup As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colUserPrevilege As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVerification As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colInstallationSetup As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEmployeeSetup As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colArearEntry As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAdvance_Loan As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colForm16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colForm16Return As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colpayrollFormula As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPayrollSetup As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLoanAdjustment As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTimeOfficeReport As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVisitorReport As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPayrollReport As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colRegisterCreation As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colRegisterUpdation As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBackDateProcess As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colReProcess As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTCAL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colauth_comp As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAuth_dept As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colUSERTYPE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colpaycode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCompAdd As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCompModi As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCompDel As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDeptAdd As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDeptModi As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDeptDel As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCatAdd As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCatModi As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCatDel As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSecAdd As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSecModi As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSecDel As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGrdAdd As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGrdModi As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGrdDel As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSftAdd As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSftModi As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSftDel As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEmpAdd As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEmpModi As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEmpDel As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDataMaintenance As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblUserTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblUserTableAdapter
    Friend WithEvents TblUser1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.tblUser1TableAdapter

End Class
