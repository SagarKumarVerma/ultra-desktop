﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraEmpGroupEditDemo
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.TblShiftMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New ULtra.SSSDBDataSet()
        Me.TblShiftMasterTableAdapter = New ULtra.SSSDBDataSetTableAdapters.tblShiftMasterTableAdapter()
        Me.GridLookUpEdit1 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colSHIFT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSTARTTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colENDTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHDURATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHENDTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colORDERINF12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTSTARTAFTER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTDEDUCTHRS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHDEDUCTION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFTPOSITION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFTDURATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTDEDUCTAFTER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedBy = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.CheckedComboBoxEdit1 = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.LookUpEdit1 = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colSHIFT1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSTARTTIME1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colENDTIME1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHTIME1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHDURATION1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHENDTIME1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colORDERINF121 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTSTARTAFTER1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTDEDUCTHRS1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHDEDUCTION1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFTPOSITION1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFTDURATION1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTDEDUCTAFTER1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedBy1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedDate1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.bar2 = New DevExpress.XtraBars.Bar()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.PopupContainerEdit1 = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerControl1 = New DevExpress.XtraEditors.PopupContainerControl()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblShiftMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckedComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(49, 51)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Size = New System.Drawing.Size(161, 20)
        Me.TextEdit1.TabIndex = 0
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(49, 78)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Size = New System.Drawing.Size(161, 20)
        Me.TextEdit2.TabIndex = 1
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(49, 105)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 2
        Me.SimpleButton1.Text = "SimpleButton1"
        '
        'TblShiftMasterBindingSource
        '
        Me.TblShiftMasterBindingSource.DataMember = "tblShiftMaster"
        Me.TblShiftMasterBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblShiftMasterTableAdapter
        '
        Me.TblShiftMasterTableAdapter.ClearBeforeFill = True
        '
        'GridLookUpEdit1
        '
        Me.GridLookUpEdit1.Location = New System.Drawing.Point(49, 203)
        Me.GridLookUpEdit1.Name = "GridLookUpEdit1"
        Me.GridLookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEdit1.Properties.DataSource = Me.TblShiftMasterBindingSource
        Me.GridLookUpEdit1.Properties.DisplayMember = "STARTTIME"
        Me.GridLookUpEdit1.Properties.ValueMember = "SHIFT"
        Me.GridLookUpEdit1.Properties.View = Me.GridLookUpEdit1View
        Me.GridLookUpEdit1.Size = New System.Drawing.Size(217, 20)
        Me.GridLookUpEdit1.TabIndex = 4
        '
        'GridLookUpEdit1View
        '
        Me.GridLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSHIFT, Me.colSTARTTIME, Me.colENDTIME, Me.colLUNCHTIME, Me.colLUNCHDURATION, Me.colLUNCHENDTIME, Me.colORDERINF12, Me.colOTSTARTAFTER, Me.colOTDEDUCTHRS, Me.colLUNCHDEDUCTION, Me.colSHIFTPOSITION, Me.colSHIFTDURATION, Me.colOTDEDUCTAFTER, Me.colLastModifiedBy, Me.colLastModifiedDate})
        Me.GridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit1View.Name = "GridLookUpEdit1View"
        Me.GridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colSHIFT
        '
        Me.colSHIFT.FieldName = "SHIFT"
        Me.colSHIFT.Name = "colSHIFT"
        Me.colSHIFT.Visible = True
        Me.colSHIFT.VisibleIndex = 0
        '
        'colSTARTTIME
        '
        Me.colSTARTTIME.FieldName = "STARTTIME"
        Me.colSTARTTIME.Name = "colSTARTTIME"
        Me.colSTARTTIME.Visible = True
        Me.colSTARTTIME.VisibleIndex = 1
        '
        'colENDTIME
        '
        Me.colENDTIME.FieldName = "ENDTIME"
        Me.colENDTIME.Name = "colENDTIME"
        Me.colENDTIME.Visible = True
        Me.colENDTIME.VisibleIndex = 2
        '
        'colLUNCHTIME
        '
        Me.colLUNCHTIME.FieldName = "LUNCHTIME"
        Me.colLUNCHTIME.Name = "colLUNCHTIME"
        '
        'colLUNCHDURATION
        '
        Me.colLUNCHDURATION.FieldName = "LUNCHDURATION"
        Me.colLUNCHDURATION.Name = "colLUNCHDURATION"
        '
        'colLUNCHENDTIME
        '
        Me.colLUNCHENDTIME.FieldName = "LUNCHENDTIME"
        Me.colLUNCHENDTIME.Name = "colLUNCHENDTIME"
        '
        'colORDERINF12
        '
        Me.colORDERINF12.FieldName = "ORDERINF12"
        Me.colORDERINF12.Name = "colORDERINF12"
        '
        'colOTSTARTAFTER
        '
        Me.colOTSTARTAFTER.FieldName = "OTSTARTAFTER"
        Me.colOTSTARTAFTER.Name = "colOTSTARTAFTER"
        '
        'colOTDEDUCTHRS
        '
        Me.colOTDEDUCTHRS.FieldName = "OTDEDUCTHRS"
        Me.colOTDEDUCTHRS.Name = "colOTDEDUCTHRS"
        '
        'colLUNCHDEDUCTION
        '
        Me.colLUNCHDEDUCTION.FieldName = "LUNCHDEDUCTION"
        Me.colLUNCHDEDUCTION.Name = "colLUNCHDEDUCTION"
        '
        'colSHIFTPOSITION
        '
        Me.colSHIFTPOSITION.FieldName = "SHIFTPOSITION"
        Me.colSHIFTPOSITION.Name = "colSHIFTPOSITION"
        '
        'colSHIFTDURATION
        '
        Me.colSHIFTDURATION.FieldName = "SHIFTDURATION"
        Me.colSHIFTDURATION.Name = "colSHIFTDURATION"
        '
        'colOTDEDUCTAFTER
        '
        Me.colOTDEDUCTAFTER.FieldName = "OTDEDUCTAFTER"
        Me.colOTDEDUCTAFTER.Name = "colOTDEDUCTAFTER"
        '
        'colLastModifiedBy
        '
        Me.colLastModifiedBy.FieldName = "LastModifiedBy"
        Me.colLastModifiedBy.Name = "colLastModifiedBy"
        '
        'colLastModifiedDate
        '
        Me.colLastModifiedDate.FieldName = "LastModifiedDate"
        Me.colLastModifiedDate.Name = "colLastModifiedDate"
        '
        'CheckedComboBoxEdit1
        '
        Me.CheckedComboBoxEdit1.EditValue = ""
        Me.CheckedComboBoxEdit1.Location = New System.Drawing.Point(357, 170)
        Me.CheckedComboBoxEdit1.Name = "CheckedComboBoxEdit1"
        Me.CheckedComboBoxEdit1.Properties.AllowMultiSelect = True
        Me.CheckedComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.CheckedComboBoxEdit1.Properties.DataSource = Me.TblShiftMasterBindingSource
        Me.CheckedComboBoxEdit1.Properties.DisplayMember = "STARTTIME"
        Me.CheckedComboBoxEdit1.Properties.ValueMember = "SHIFT"
        Me.CheckedComboBoxEdit1.Size = New System.Drawing.Size(244, 20)
        Me.CheckedComboBoxEdit1.TabIndex = 5
        '
        'LookUpEdit1
        '
        Me.LookUpEdit1.Location = New System.Drawing.Point(49, 167)
        Me.LookUpEdit1.Name = "LookUpEdit1"
        Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEdit1.Properties.NullText = "[EditValue is null]"
        Me.LookUpEdit1.Size = New System.Drawing.Size(217, 20)
        Me.LookUpEdit1.TabIndex = 3
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblShiftMasterBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.MenuManager = Me.barManager1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(200, 127)
        Me.GridControl1.TabIndex = 6
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSHIFT1, Me.colSTARTTIME1, Me.colENDTIME1, Me.colLUNCHTIME1, Me.colLUNCHDURATION1, Me.colLUNCHENDTIME1, Me.colORDERINF121, Me.colOTSTARTAFTER1, Me.colOTDEDUCTHRS1, Me.colLUNCHDEDUCTION1, Me.colSHIFTPOSITION1, Me.colSHIFTDURATION1, Me.colOTDEDUCTAFTER1, Me.colLastModifiedBy1, Me.colLastModifiedDate1})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        '
        'colSHIFT1
        '
        Me.colSHIFT1.FieldName = "SHIFT"
        Me.colSHIFT1.Name = "colSHIFT1"
        Me.colSHIFT1.Visible = True
        Me.colSHIFT1.VisibleIndex = 1
        '
        'colSTARTTIME1
        '
        Me.colSTARTTIME1.FieldName = "STARTTIME"
        Me.colSTARTTIME1.Name = "colSTARTTIME1"
        Me.colSTARTTIME1.Visible = True
        Me.colSTARTTIME1.VisibleIndex = 2
        '
        'colENDTIME1
        '
        Me.colENDTIME1.FieldName = "ENDTIME"
        Me.colENDTIME1.Name = "colENDTIME1"
        Me.colENDTIME1.Visible = True
        Me.colENDTIME1.VisibleIndex = 3
        '
        'colLUNCHTIME1
        '
        Me.colLUNCHTIME1.FieldName = "LUNCHTIME"
        Me.colLUNCHTIME1.Name = "colLUNCHTIME1"
        '
        'colLUNCHDURATION1
        '
        Me.colLUNCHDURATION1.FieldName = "LUNCHDURATION"
        Me.colLUNCHDURATION1.Name = "colLUNCHDURATION1"
        '
        'colLUNCHENDTIME1
        '
        Me.colLUNCHENDTIME1.FieldName = "LUNCHENDTIME"
        Me.colLUNCHENDTIME1.Name = "colLUNCHENDTIME1"
        '
        'colORDERINF121
        '
        Me.colORDERINF121.FieldName = "ORDERINF12"
        Me.colORDERINF121.Name = "colORDERINF121"
        '
        'colOTSTARTAFTER1
        '
        Me.colOTSTARTAFTER1.FieldName = "OTSTARTAFTER"
        Me.colOTSTARTAFTER1.Name = "colOTSTARTAFTER1"
        '
        'colOTDEDUCTHRS1
        '
        Me.colOTDEDUCTHRS1.FieldName = "OTDEDUCTHRS"
        Me.colOTDEDUCTHRS1.Name = "colOTDEDUCTHRS1"
        '
        'colLUNCHDEDUCTION1
        '
        Me.colLUNCHDEDUCTION1.FieldName = "LUNCHDEDUCTION"
        Me.colLUNCHDEDUCTION1.Name = "colLUNCHDEDUCTION1"
        '
        'colSHIFTPOSITION1
        '
        Me.colSHIFTPOSITION1.FieldName = "SHIFTPOSITION"
        Me.colSHIFTPOSITION1.Name = "colSHIFTPOSITION1"
        '
        'colSHIFTDURATION1
        '
        Me.colSHIFTDURATION1.FieldName = "SHIFTDURATION"
        Me.colSHIFTDURATION1.Name = "colSHIFTDURATION1"
        '
        'colOTDEDUCTAFTER1
        '
        Me.colOTDEDUCTAFTER1.FieldName = "OTDEDUCTAFTER"
        Me.colOTDEDUCTAFTER1.Name = "colOTDEDUCTAFTER1"
        '
        'colLastModifiedBy1
        '
        Me.colLastModifiedBy1.FieldName = "LastModifiedBy"
        Me.colLastModifiedBy1.Name = "colLastModifiedBy1"
        '
        'colLastModifiedDate1
        '
        Me.colLastModifiedDate1.FieldName = "LastModifiedDate"
        Me.colLastModifiedDate1.Name = "colLastModifiedDate1"
        '
        'barManager1
        '
        Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.bar2})
        Me.barManager1.DockControls.Add(Me.barDockControlTop)
        Me.barManager1.DockControls.Add(Me.barDockControlBottom)
        Me.barManager1.DockControls.Add(Me.barDockControlLeft)
        Me.barManager1.DockControls.Add(Me.barDockControlRight)
        Me.barManager1.Form = Me
        Me.barManager1.MainMenu = Me.bar2
        Me.barManager1.MaxItemId = 1
        '
        'bar2
        '
        Me.bar2.BarName = "Main menu"
        Me.bar2.DockCol = 0
        Me.bar2.DockRow = 0
        Me.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bar2.OptionsBar.MultiLine = True
        Me.bar2.OptionsBar.UseWholeRow = True
        Me.bar2.Text = "Main menu"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Manager = Me.barManager1
        Me.barDockControlTop.Size = New System.Drawing.Size(674, 20)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 311)
        Me.barDockControlBottom.Manager = Me.barManager1
        Me.barDockControlBottom.Size = New System.Drawing.Size(674, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 20)
        Me.barDockControlLeft.Manager = Me.barManager1
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 291)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(674, 20)
        Me.barDockControlRight.Manager = Me.barManager1
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 291)
        '
        'PopupContainerEdit1
        '
        Me.PopupContainerEdit1.Location = New System.Drawing.Point(49, 135)
        Me.PopupContainerEdit1.Name = "PopupContainerEdit1"
        Me.PopupContainerEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEdit1.Properties.PopupControl = Me.PopupContainerControl1
        Me.PopupContainerEdit1.Size = New System.Drawing.Size(215, 20)
        Me.PopupContainerEdit1.TabIndex = 7
        '
        'PopupContainerControl1
        '
        Me.PopupContainerControl1.Controls.Add(Me.GridControl1)
        Me.PopupContainerControl1.Location = New System.Drawing.Point(357, 28)
        Me.PopupContainerControl1.Name = "PopupContainerControl1"
        Me.PopupContainerControl1.Size = New System.Drawing.Size(200, 127)
        Me.PopupContainerControl1.TabIndex = 8
        '
        'XtraEmpGroupEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.PopupContainerControl1)
        Me.Controls.Add(Me.PopupContainerEdit1)
        Me.Controls.Add(Me.CheckedComboBoxEdit1)
        Me.Controls.Add(Me.GridLookUpEdit1)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.TextEdit2)
        Me.Controls.Add(Me.TextEdit1)
        Me.Controls.Add(Me.LookUpEdit1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "XtraEmpGroupEdit"
        Me.Size = New System.Drawing.Size(674, 311)
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblShiftMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckedComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControl1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TblShiftMasterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As ULtra.SSSDBDataSet
    Friend WithEvents TblShiftMasterTableAdapter As ULtra.SSSDBDataSetTableAdapters.tblShiftMasterTableAdapter
    Friend WithEvents GridLookUpEdit1 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colSHIFT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTARTTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colENDTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHDURATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHENDTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colORDERINF12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTSTARTAFTER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTDEDUCTHRS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHDEDUCTION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFTPOSITION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFTDURATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTDEDUCTAFTER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents CheckedComboBoxEdit1 As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colSHIFT1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTARTTIME1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colENDTIME1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHTIME1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHDURATION1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHENDTIME1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colORDERINF121 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTSTARTAFTER1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTDEDUCTHRS1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHDEDUCTION1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFTPOSITION1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFTDURATION1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTDEDUCTAFTER1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedBy1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedDate1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents PopupContainerEdit1 As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents PopupContainerControl1 As DevExpress.XtraEditors.PopupContainerControl
    Private WithEvents barManager1 As DevExpress.XtraBars.BarManager
    Private WithEvents bar2 As DevExpress.XtraBars.Bar
    Private WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Private WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Private WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Private WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl

End Class
