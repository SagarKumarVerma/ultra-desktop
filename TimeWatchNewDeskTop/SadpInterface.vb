﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Threading.Tasks

Namespace SADP_Ability_Tool

	Public Class SadpInterface
		Public Structure SADP_DEVICE_INFO
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 12, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public szSeries() As Byte
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 48, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public szSerialNO() As Byte
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 20, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public szMAC() As Byte
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 16, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public szIPv4Address() As Byte
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 16, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public szIPv4SubnetMask() As Byte
			Public dwDeviceType As UInteger
			Public dwPort As UInteger
			Public dwNumberOfEncoders As UInteger
			Public dwNumberOfHardDisk As UInteger
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 48, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public szDeviceSoftwareVersion() As Byte
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 48, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public szDSPVersion() As Byte
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 48, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public szBootTime() As Byte
			Public iResult As Integer
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 24, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public szDevDesc() As Byte '设备类型描述
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 24, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public szOEMinfo() As Byte 'OEM产商信息
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 16, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public szIPv4Gateway() As Byte 'IPv4网关
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 46, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public szIPv6Address() As Byte 'IPv6地址
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 46, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public szIPv6Gateway() As Byte 'IPv6网关
			Public byIPv6MaskLen As Byte 'IPv6子网前缀长度
			Public bySupport As Byte '按位表示,对应为为1表示支持,0x01:是否支持Ipv6,0x02:是否支持修改Ipv6参数,0x04:是否支持Dhcp    0x08: 是否支持udp多播 0x10:是否含加密节点,0x20:是否支持恢复密码,0x40:是否支持重置密码,0x80:是否支持同步IPC密码
			Public byDhcpEnabled As Byte 'Dhcp状态, 0 不启用 1 启用
			Public byDeviceAbility As Byte '0：设备不支持"‘设备类型描述’ 'OEM厂商' 'IPv4网关' ‘IPv6地址’ 'IPv6网关' ‘IPv6子网前缀’‘DHCP’"  1：支持上诉功能
			Public wHttpPort As UShort ' Http 端口
			Public wDigitalChannelNum As UShort
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 16, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public szCmsIPv4() As Byte
			Public wCmsPort As UShort
			Public byOEMCode As Byte '0-基线设备 1-OEM设备
			Public byActivated As Byte '设备是否激活;0-激活，1-未激活（老的设备都是已激活状态）
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 24, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public szBaseDesc() As Byte '基线短型号，不随定制而修改的型号，用于萤石平台进行型号对比
			Public bySupport1 As Byte '按位表示,  1表示支持，0表示不支持
			'0x01:是否支持重置密码方式2 
			'0x02;是否支持设备锁定功能
			'0x04:是否支持导入GUID重置密码
			'0x08:是否支持安全问题重置密码
			'0x10:是否支持OEM更换Logo
			'0x20:是否支持绑定操作
			'0x40:是否支持恢复未激活
			'0x80:是否支持wifi信号增强模式
			Public byHCPlatform As Byte '是否支持HCPlatform 0-保留， 1-支持， 2-不支持
			Public byEnableHCPlatform As Byte '是否启用HCPlatform  0 -保留 1- 启用， 2- 不启用
			Public byEZVIZCode As Byte '0- 基线设备 1- 萤石设备
			Public dwDetailOEMCode As UInteger '详细OEMCode信息:oemcode由客户序号（可变位,从1开始，1~429496)+菜单风格（2位）+区域号（2位）三部分构成。
			'规则说明：oemcode最大值为4294967295，最多是十位数。
			Public byModifyVerificationCode As Byte '是否修改验证码 0-保留， 1-修改验证码， 2- 不修改验证码
			Public byMaxBindNum As Byte '支持绑定的最大个数（目前只有NVR支持该字段）
			Public wOEMCommandPort As UShort ' OEM命令 端口
			Public bySupportWifiRegion As Byte '设备支持的wifi区域列表，按位表示，1表示支持，0表示不支持
			'0x01:是否支持default（默认功率和北美一致）
			'0x02:是否支持china
			'0x04:是否支持nothAmerica
			'0x08:是否支持japan
			'0x10:是否支持europe
			'0x20:是否支持world
			Public byEnableWifiEnhancement As Byte '是否启用wifi增强模式,0-不启用，1-启用
			Public byWifiRegion As Byte '设备当前区域，0-default，1-china，2-nothAmerica，3-japan，4-europe,5-world
			Public bySupport2 As Byte '按位表示,  1表示支持，0表示不支持
			'0x01:是否通道默认密码配置 
			'0x02:是否支持邮箱重置密码
			'0x04:是否支持未激活配置SSID和Password
		End Structure

		Public Structure SADP_DEVICE_INFO_V40
			Public struSadpDeviceInfo As SADP_DEVICE_INFO
			Public byLicensed As Byte '设备是否授权：0-保留,1-设备未授权，2-设备已授权
			Public bySystemMode As Byte '系统模式 0-保留,1-单控，2-双控，3-单机集群，4-双控集群
			Public byControllerType As Byte '控制器类型 0-保留，1-A控，2-B控
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 16, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public szEhmoeVersion() As Byte 'Ehmoe版本号
			Public bySpecificDeviceType As Byte '设备类型，1-中性设备  2-海康设备
			Public dwSDKOverTLSPort As UInteger ' 私有协议中 SDK Over TLS 命令端口
			Public bySecurityMode As Byte '设备安全模式：0-standard,1-high-A,2-high-B,3-custom
			Public bySDKServerStatus As Byte '设备SDK服务状态, 0-开启，1-关闭
			Public bySDKOverTLSServerStatus As Byte '设备SDKOverTLS服务状态, 0-关闭，1-开启
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 33, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public szUserName() As Byte '管理员用户的用户名
			<System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst := 452, ArraySubType := System.Runtime.InteropServices.UnmanagedType.I1)>
			Public byRes() As Byte
		End Structure


		Public Delegate Sub PDEVICE_FIND_CALLBACK_LINK(ByVal lpDeviceInfo As IntPtr, ByVal pUserData As IntPtr)
		Public Delegate Sub PDEVICE_FIND_CALLBACK_MULTICAST(ByVal lpDeviceInfo As IntPtr, ByVal pUserData As IntPtr)

		<DllImport("./Sadp.dll", EntryPoint := "SADP_Start_V40")>
		Public Shared Function SADP_Start_V40(ByVal pDeviceFindCallBackLink As PDEVICE_FIND_CALLBACK_LINK, ByVal pDeviceFindCallBackMulticast As PDEVICE_FIND_CALLBACK_MULTICAST, ByVal bInstallNPF As Integer, ByVal pUserData As IntPtr) As Integer
		End Function
	End Class
End Namespace
