﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraPayMaintenance
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraPayMaintenance))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.txtHolidays = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.txtEarlyDays = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.txtLateDays = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.txtOtherLeave = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.txtPLEL = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.txtSL = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.txtCL = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.txtWO = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.txtEarly = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.txtLate = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.txtOT = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.txtAbsent = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.txtDaysWorked = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.ComboNepaliYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNEpaliMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEditFrom = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.lblName = New DevExpress.XtraEditors.LabelControl()
        Me.lblDept = New DevExpress.XtraEditors.LabelControl()
        Me.lblCardNum = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.SSSDBDataSet = New ULtra.SSSDBDataSet()
        Me.TblEmployeeTableAdapter = New ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblEmployee1TableAdapter1 = New ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.txtHolidays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarlyDays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLateDays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOtherLeave.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPLEL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtWO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEarly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAbsent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDaysWorked.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.ComboNepaliYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PanelControl1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1250, 568)
        Me.SplitContainerControl1.SplitterPosition = 1165
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.SimpleButtonSave)
        Me.PanelControl1.Controls.Add(Me.txtHolidays)
        Me.PanelControl1.Controls.Add(Me.LabelControl18)
        Me.PanelControl1.Controls.Add(Me.txtEarlyDays)
        Me.PanelControl1.Controls.Add(Me.LabelControl17)
        Me.PanelControl1.Controls.Add(Me.txtLateDays)
        Me.PanelControl1.Controls.Add(Me.LabelControl16)
        Me.PanelControl1.Controls.Add(Me.txtOtherLeave)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.txtPLEL)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.txtSL)
        Me.PanelControl1.Controls.Add(Me.LabelControl13)
        Me.PanelControl1.Controls.Add(Me.txtCL)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.txtWO)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Controls.Add(Me.txtEarly)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.txtLate)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.txtOT)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.txtAbsent)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.txtDaysWorked)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 113)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(1165, 455)
        Me.PanelControl1.TabIndex = 40
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(511, 263)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 16
        Me.SimpleButtonSave.Text = "Save"
        '
        'txtHolidays
        '
        Me.txtHolidays.Location = New System.Drawing.Point(486, 179)
        Me.txtHolidays.Name = "txtHolidays"
        Me.txtHolidays.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtHolidays.Properties.Appearance.Options.UseFont = True
        Me.txtHolidays.Properties.Mask.EditMask = "##.##"
        Me.txtHolidays.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtHolidays.Properties.MaxLength = 5
        Me.txtHolidays.Size = New System.Drawing.Size(100, 20)
        Me.txtHolidays.TabIndex = 15
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl18.Appearance.Options.UseFont = True
        Me.LabelControl18.Location = New System.Drawing.Point(380, 181)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(43, 14)
        Me.LabelControl18.TabIndex = 46
        Me.LabelControl18.Text = "Holidays"
        '
        'txtEarlyDays
        '
        Me.txtEarlyDays.Location = New System.Drawing.Point(486, 153)
        Me.txtEarlyDays.Name = "txtEarlyDays"
        Me.txtEarlyDays.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarlyDays.Properties.Appearance.Options.UseFont = True
        Me.txtEarlyDays.Properties.Mask.EditMask = "##.##"
        Me.txtEarlyDays.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarlyDays.Properties.MaxLength = 5
        Me.txtEarlyDays.Size = New System.Drawing.Size(100, 20)
        Me.txtEarlyDays.TabIndex = 14
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(380, 155)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(54, 14)
        Me.LabelControl17.TabIndex = 44
        Me.LabelControl17.Text = "Early Days"
        '
        'txtLateDays
        '
        Me.txtLateDays.Location = New System.Drawing.Point(486, 127)
        Me.txtLateDays.Name = "txtLateDays"
        Me.txtLateDays.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtLateDays.Properties.Appearance.Options.UseFont = True
        Me.txtLateDays.Properties.Mask.EditMask = "##.##"
        Me.txtLateDays.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtLateDays.Properties.MaxLength = 5
        Me.txtLateDays.Size = New System.Drawing.Size(100, 20)
        Me.txtLateDays.TabIndex = 13
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(380, 129)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(53, 14)
        Me.LabelControl16.TabIndex = 42
        Me.LabelControl16.Text = "Late Days"
        '
        'txtOtherLeave
        '
        Me.txtOtherLeave.Location = New System.Drawing.Point(486, 101)
        Me.txtOtherLeave.Name = "txtOtherLeave"
        Me.txtOtherLeave.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtOtherLeave.Properties.Appearance.Options.UseFont = True
        Me.txtOtherLeave.Properties.Mask.EditMask = "##.##"
        Me.txtOtherLeave.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtOtherLeave.Properties.MaxLength = 5
        Me.txtOtherLeave.Size = New System.Drawing.Size(100, 20)
        Me.txtOtherLeave.TabIndex = 12
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(380, 103)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(68, 14)
        Me.LabelControl15.TabIndex = 40
        Me.LabelControl15.Text = "Other Leave"
        '
        'txtPLEL
        '
        Me.txtPLEL.Location = New System.Drawing.Point(486, 75)
        Me.txtPLEL.Name = "txtPLEL"
        Me.txtPLEL.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtPLEL.Properties.Appearance.Options.UseFont = True
        Me.txtPLEL.Properties.Mask.EditMask = "##.##"
        Me.txtPLEL.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtPLEL.Properties.MaxLength = 5
        Me.txtPLEL.Size = New System.Drawing.Size(100, 20)
        Me.txtPLEL.TabIndex = 11
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(380, 77)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(31, 14)
        Me.LabelControl14.TabIndex = 38
        Me.LabelControl14.Text = "PL/EL"
        '
        'txtSL
        '
        Me.txtSL.Location = New System.Drawing.Point(486, 49)
        Me.txtSL.Name = "txtSL"
        Me.txtSL.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtSL.Properties.Appearance.Options.UseFont = True
        Me.txtSL.Properties.Mask.EditMask = "##.##"
        Me.txtSL.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtSL.Properties.MaxLength = 5
        Me.txtSL.Size = New System.Drawing.Size(100, 20)
        Me.txtSL.TabIndex = 10
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(380, 51)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(13, 14)
        Me.LabelControl13.TabIndex = 36
        Me.LabelControl13.Text = "SL"
        '
        'txtCL
        '
        Me.txtCL.Location = New System.Drawing.Point(486, 23)
        Me.txtCL.Name = "txtCL"
        Me.txtCL.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtCL.Properties.Appearance.Options.UseFont = True
        Me.txtCL.Properties.Mask.EditMask = "##.##"
        Me.txtCL.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtCL.Properties.MaxLength = 5
        Me.txtCL.Size = New System.Drawing.Size(100, 20)
        Me.txtCL.TabIndex = 9
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(380, 25)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(13, 14)
        Me.LabelControl12.TabIndex = 34
        Me.LabelControl12.Text = "CL"
        '
        'txtWO
        '
        Me.txtWO.Location = New System.Drawing.Point(127, 178)
        Me.txtWO.Name = "txtWO"
        Me.txtWO.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtWO.Properties.Appearance.Options.UseFont = True
        Me.txtWO.Properties.Mask.EditMask = "##.##"
        Me.txtWO.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtWO.Properties.MaxLength = 5
        Me.txtWO.Size = New System.Drawing.Size(100, 20)
        Me.txtWO.TabIndex = 8
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(21, 180)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(21, 14)
        Me.LabelControl11.TabIndex = 32
        Me.LabelControl11.Text = "WO"
        '
        'txtEarly
        '
        Me.txtEarly.Location = New System.Drawing.Point(127, 152)
        Me.txtEarly.Name = "txtEarly"
        Me.txtEarly.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtEarly.Properties.Appearance.Options.UseFont = True
        Me.txtEarly.Properties.Mask.EditMask = "###.##"
        Me.txtEarly.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtEarly.Properties.MaxLength = 6
        Me.txtEarly.Size = New System.Drawing.Size(100, 20)
        Me.txtEarly.TabIndex = 7
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(21, 154)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl10.TabIndex = 30
        Me.LabelControl10.Text = "Early Hours"
        '
        'txtLate
        '
        Me.txtLate.Location = New System.Drawing.Point(127, 126)
        Me.txtLate.Name = "txtLate"
        Me.txtLate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtLate.Properties.Appearance.Options.UseFont = True
        Me.txtLate.Properties.Mask.EditMask = "###.##"
        Me.txtLate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtLate.Properties.MaxLength = 6
        Me.txtLate.Size = New System.Drawing.Size(100, 20)
        Me.txtLate.TabIndex = 6
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(21, 128)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(59, 14)
        Me.LabelControl8.TabIndex = 28
        Me.LabelControl8.Text = "Late Hours"
        '
        'txtOT
        '
        Me.txtOT.Location = New System.Drawing.Point(127, 75)
        Me.txtOT.Name = "txtOT"
        Me.txtOT.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtOT.Properties.Appearance.Options.UseFont = True
        Me.txtOT.Properties.Mask.EditMask = "##.##"
        Me.txtOT.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtOT.Properties.MaxLength = 5
        Me.txtOT.Size = New System.Drawing.Size(100, 20)
        Me.txtOT.TabIndex = 5
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(21, 77)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl7.TabIndex = 26
        Me.LabelControl7.Text = "O.T. Hours"
        '
        'txtAbsent
        '
        Me.txtAbsent.Location = New System.Drawing.Point(127, 49)
        Me.txtAbsent.Name = "txtAbsent"
        Me.txtAbsent.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtAbsent.Properties.Appearance.Options.UseFont = True
        Me.txtAbsent.Properties.Mask.EditMask = "##.##"
        Me.txtAbsent.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtAbsent.Properties.MaxLength = 5
        Me.txtAbsent.Size = New System.Drawing.Size(100, 20)
        Me.txtAbsent.TabIndex = 4
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(21, 51)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(77, 14)
        Me.LabelControl4.TabIndex = 24
        Me.LabelControl4.Text = "Absent / LWP"
        '
        'txtDaysWorked
        '
        Me.txtDaysWorked.Location = New System.Drawing.Point(127, 23)
        Me.txtDaysWorked.Name = "txtDaysWorked"
        Me.txtDaysWorked.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtDaysWorked.Properties.Appearance.Options.UseFont = True
        Me.txtDaysWorked.Properties.Mask.EditMask = "##.##"
        Me.txtDaysWorked.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtDaysWorked.Properties.MaxLength = 5
        Me.txtDaysWorked.Size = New System.Drawing.Size(100, 20)
        Me.txtDaysWorked.TabIndex = 3
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(21, 25)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(72, 14)
        Me.LabelControl3.TabIndex = 22
        Me.LabelControl3.Text = "Days Worked"
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.GroupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl1.Controls.Add(Me.ComboNepaliYear)
        Me.GroupControl1.Controls.Add(Me.ComboNEpaliMonth)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.LookUpEdit1)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.DateEditFrom)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.lblName)
        Me.GroupControl1.Controls.Add(Me.lblDept)
        Me.GroupControl1.Controls.Add(Me.lblCardNum)
        Me.GroupControl1.Controls.Add(Me.LabelControl9)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(1165, 113)
        Me.GroupControl1.TabIndex = 39
        Me.GroupControl1.Text = "Employee"
        '
        'ComboNepaliYear
        '
        Me.ComboNepaliYear.Location = New System.Drawing.Point(600, 39)
        Me.ComboNepaliYear.Name = "ComboNepaliYear"
        Me.ComboNepaliYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYear.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYear.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliYear.TabIndex = 39
        '
        'ComboNEpaliMonth
        '
        Me.ComboNEpaliMonth.Location = New System.Drawing.Point(528, 39)
        Me.ComboNEpaliMonth.Name = "ComboNEpaliMonth"
        Me.ComboNEpaliMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonth.Size = New System.Drawing.Size(66, 20)
        Me.ComboNEpaliMonth.TabIndex = 38
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(21, 42)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(84, 14)
        Me.LabelControl5.TabIndex = 21
        Me.LabelControl5.Text = "Select Paycode"
        '
        'LookUpEdit1
        '
        Me.LookUpEdit1.Location = New System.Drawing.Point(127, 40)
        Me.LookUpEdit1.Name = "LookUpEdit1"
        Me.LookUpEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.Appearance.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceFocused.Options.UseFont = True
        Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PAYCODE", "PAYCODE", 30, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EMPNAME", "Name")})
        Me.LookUpEdit1.Properties.DisplayMember = "PAYCODE"
        Me.LookUpEdit1.Properties.MaxLength = 12
        Me.LookUpEdit1.Properties.NullText = ""
        Me.LookUpEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.LookUpEdit1.Properties.ValueMember = "PAYCODE"
        Me.LookUpEdit1.Size = New System.Drawing.Size(129, 20)
        Me.LookUpEdit1.TabIndex = 1
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(431, 42)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(35, 14)
        Me.LabelControl6.TabIndex = 16
        Me.LabelControl6.Text = "Month"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(21, 74)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(31, 14)
        Me.LabelControl1.TabIndex = 32
        Me.LabelControl1.Text = "Name"
        '
        'DateEditFrom
        '
        Me.DateEditFrom.EditValue = New Date(2018, 1, 1, 16, 13, 14, 0)
        Me.DateEditFrom.Location = New System.Drawing.Point(528, 39)
        Me.DateEditFrom.Name = "DateEditFrom"
        Me.DateEditFrom.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditFrom.Properties.Appearance.Options.UseFont = True
        Me.DateEditFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditFrom.Properties.Mask.EditMask = "MM/yyyy"
        Me.DateEditFrom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditFrom.Size = New System.Drawing.Size(135, 20)
        Me.DateEditFrom.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(431, 74)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(47, 14)
        Me.LabelControl2.TabIndex = 33
        Me.LabelControl2.Text = "Card No."
        '
        'lblName
        '
        Me.lblName.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblName.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblName.Appearance.Options.UseFont = True
        Me.lblName.Appearance.Options.UseForeColor = True
        Me.lblName.Location = New System.Drawing.Point(127, 74)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(20, 14)
        Me.lblName.TabIndex = 35
        Me.lblName.Text = "     "
        '
        'lblDept
        '
        Me.lblDept.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblDept.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblDept.Appearance.Options.UseFont = True
        Me.lblDept.Appearance.Options.UseForeColor = True
        Me.lblDept.Location = New System.Drawing.Point(811, 74)
        Me.lblDept.Name = "lblDept"
        Me.lblDept.Size = New System.Drawing.Size(24, 14)
        Me.lblDept.TabIndex = 37
        Me.lblDept.Text = "      "
        '
        'lblCardNum
        '
        Me.lblCardNum.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblCardNum.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblCardNum.Appearance.Options.UseFont = True
        Me.lblCardNum.Appearance.Options.UseForeColor = True
        Me.lblCardNum.Location = New System.Drawing.Point(543, 74)
        Me.lblCardNum.Name = "lblCardNum"
        Me.lblCardNum.Size = New System.Drawing.Size(24, 14)
        Me.lblCardNum.TabIndex = 36
        Me.lblCardNum.Text = "      "
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(699, 74)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl9.TabIndex = 34
        Me.LabelControl9.Text = "Department"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(77, 568)
        Me.MemoEdit1.TabIndex = 4
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'XtraPayMaintenance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraPayMaintenance"
        Me.Size = New System.Drawing.Size(1250, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.txtHolidays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarlyDays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLateDays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOtherLeave.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPLEL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtWO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEarly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAbsent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDaysWorked.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.ComboNepaliYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents DateEditFrom As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents SSSDBDataSet As ULtra.SSSDBDataSet
    Friend WithEvents TblEmployeeTableAdapter As ULtra.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblEmployee1TableAdapter1 As ULtra.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblDept As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCardNum As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents txtHolidays As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtEarlyDays As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtLateDays As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtOtherLeave As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtPLEL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtSL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtCL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtWO As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtEarly As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtLate As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtOT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtAbsent As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtDaysWorked As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ComboNepaliYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonth As DevExpress.XtraEditors.ComboBoxEdit

End Class
