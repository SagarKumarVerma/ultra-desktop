﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Base
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Threading
Imports ULtra.AscDemo
Imports System.Runtime.InteropServices
Imports ULtra.AcsDemo

'Imports DevExpress.XtraRichEdit.Forms.SearchHelper

Public Class XtraAutoDownloadLogs
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Delegate Sub invokeDelegate()
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\HTDB.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine1
        Else
            TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine
        End If
    End Sub
    Private Sub XtraAutoDownloadLogs_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'If Common.servername = "Access" Then
        '    'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\HTDB.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine1
        'Else
        '    'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
        '    TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine
        'End If
        Common.loadDevice()
        GridControl1.DataSource = Common.MachineNonAdmin

        LabelControl1.Text = ""
        Timer1.Enabled = True
    End Sub
    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        Timer1.Enabled = False
        If Common.LogDownLoadCounter = 1 Then  'if it is from login page
            'Dim downloadLogsThread = New Thread(AddressOf Me.downloadLogsThread)
            downloadLogsHikvision()
            Common.LogDownLoadCounter = 0
            Me.Close()
        End If
    End Sub
    Private Sub downloadLogsHikvision()
        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType As String
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim dsM As DataSet = New DataSet

        Dim sSql As String = "select top 1 * from machinerawpunchAll order by OFFICEPUNCH desc "
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(dsM)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(dsM)
        End If
        Dim StartDate As DateTime
        Dim EndDate As DateTime = Now
        If dsM.Tables(0).Rows.Count = 0 Then
            StartDate = Convert.ToDateTime("2020-01-01 00:00:00")
        Else
            StartDate = Convert.ToDateTime(dsM.Tables(0).Rows(0).Item("OFFICEPUNCH"))
        End If

        dsM = New DataSet
        sSql = "select * from tblMachine"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(dsM)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(dsM)
        End If
        If dsM.Tables(0).Rows.Count = 0 Then
            Return
        End If
        Dim datafile As String = System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"
        For k As Integer = 0 To dsM.Tables(0).Rows.Count - 1
            IP = dsM.Tables(0).Rows(k).Item("LOCATION").ToString.Trim
            ID_NO = dsM.Tables(0).Rows(k).Item("ID_NO").ToString.Trim
            DeviceType = dsM.Tables(0).Rows(k).Item("DeviceType").ToString.Trim
            Dim IN_OUT As String = dsM.Tables(0).Rows(k).Item("IN_OUT").ToString.Trim
            LabelControl1.Text = "Downloading data from " & IP & "  ..."
            Application.DoEvents()
            If DeviceType.ToString.Trim = "HKSeries" Then
                Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                Dim DeviceAdd As String = IP
                Dim userName As String = dsM.Tables(0).Rows(k).Item("HLogin").ToString.Trim
                Dim pwd As String = dsM.Tables(0).Rows(k).Item("HPassword").ToString.Trim
                Dim lUserID As Integer = -1
                Dim failReason As String = ""
                Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
                If logistatus = False Then
                    failIP.Add(IP)
                    Continue For
                Else
                    If failReason <> "" Then
                        XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                        XtraMasterTest.LabelControlStatus.Text = ""
                        Application.DoEvents()
                        logistatus = cn.HikvisionLogOut(lUserID)
                        Continue For
                    End If
                    'serialNo = System.Text.Encoding.UTF8.GetString(struDeviceInfoV40.struDeviceV30.sSerialNumber).TrimEnd(Microsoft.VisualBasic.ChrW(92))
                    datafile = System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"
                    Dim m_lLogNum = 0
                    Dim struCond As CHCNetSDK.NET_DVR_ACS_EVENT_COND = New CHCNetSDK.NET_DVR_ACS_EVENT_COND()
                    struCond.Init()
                    struCond.dwSize = CUInt(Marshal.SizeOf(struCond))
                    Dim MajorType As String = "Event" 'comboBoxMainType.SelectedItem.ToString()
                    struCond.dwMajor = GetAcsEventType.ReturnMajorTypeValue(MajorType)
                    Dim MinorType As String = "FINGERPRINT_COMPARE_PASS" 'comboBoxSecondType.SelectedItem.ToString()
                    struCond.dwMinor = GetAcsEventType.ReturnMinorTypeValue(MinorType)

                    struCond.struStartTime.dwYear = StartDate.Year
                    struCond.struStartTime.dwMonth = StartDate.Month
                    struCond.struStartTime.dwDay = StartDate.Day
                    struCond.struStartTime.dwHour = StartDate.Hour
                    struCond.struStartTime.dwMinute = StartDate.Minute
                    struCond.struStartTime.dwSecond = StartDate.Second
                    struCond.struEndTime.dwYear = EndDate.Year
                    struCond.struEndTime.dwMonth = EndDate.Month
                    struCond.struEndTime.dwDay = EndDate.Day
                    struCond.struEndTime.dwHour = EndDate.Hour
                    struCond.struEndTime.dwMinute = EndDate.Minute
                    struCond.struEndTime.dwSecond = EndDate.Second
                    struCond.byPicEnable = 0
                    struCond.szMonitorID = ""
                    struCond.wInductiveEventType = 65535
                    Dim dwSize As UInteger = struCond.dwSize
                    Dim ptrCond As IntPtr = Marshal.AllocHGlobal(CInt(dwSize))
                    Marshal.StructureToPtr(struCond, ptrCond, False)
                    Dim m_lGetAcsEventHandle As Integer = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_GET_ACS_EVENT, ptrCond, CInt(dwSize), Nothing, IntPtr.Zero)

                    If -1 = m_lGetAcsEventHandle Then
                        failIP.Add(IP)
                        Marshal.FreeHGlobal(ptrCond)
                        Continue For
                        'MessageBox.Show("NET_DVR_StartRemoteConfig FAIL, ERROR CODE" & CHCNetSDK.NET_DVR_GetLastError().ToString(), "Error", MessageBoxButtons.OK)
                        'Return
                    End If

                    Dim dwStatus As Integer = 0
                    Dim Flag As Boolean = True
                    Dim struCFG As CHCNetSDK.NET_DVR_ACS_EVENT_CFG = New CHCNetSDK.NET_DVR_ACS_EVENT_CFG()
                    struCFG.dwSize = CUInt(Marshal.SizeOf(struCFG))
                    Dim dwOutBuffSize As Integer = CInt(struCFG.dwSize)
                    struCFG.init()

                    Dim ptr As IntPtr = Marshal.AllocHGlobal(1024)
                    For n As Integer = 0 To 1024 - 1
                        Marshal.WriteByte(ptr, n, 0)
                    Next

                    While Flag
                        dwStatus = CHCNetSDK.NET_DVR_GetNextRemoteConfig(m_lGetAcsEventHandle, ptr, dwOutBuffSize)

                        Dim StructData As CHCNetSDK.NET_DVR_ACS_EVENT_CFG = New CHCNetSDK.NET_DVR_ACS_EVENT_CFG()
                        'Marshal.PtrToStructure(ptr, StructData)
                        '(CHCNetSDK.NET_DVR_ACS_EVENT_CFG)Marshal.PtrToStructure(ptr, typeof(CHCNetSDK.NET_DVR_ACS_EVENT_CFG))
                        StructData = Marshal.PtrToStructure(ptr, GetType(CHCNetSDK.NET_DVR_ACS_EVENT_CFG))
                        Select Case dwStatus
                            Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_SUCCESS
                                Try
                                    AddAcsEventToList(StructData, IN_OUT, ID_NO, datafile)
                                Catch ex As Exception
                                    Flag = False
                                End Try
                            Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_NEED_WAIT
                                Thread.Sleep(200)
                            Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_FAILED
                                CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetAcsEventHandle)
                                'MessageBox.Show("NET_SDK_GET_NEXT_STATUS_FAILED" & CHCNetSDK.NET_DVR_GetLastError().ToString(), "Error", MessageBoxButtons.OK)
                                Flag = False
                            Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_FINISH
                                CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetAcsEventHandle)
                                Flag = False
                            Case Else
                                'MessageBox.Show("NET_SDK_GET_NEXT_STATUS_UNKOWN" & CHCNetSDK.NET_DVR_GetLastError().ToString(), "Error", MessageBoxButtons.OK)
                                Flag = False
                                CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetAcsEventHandle)
                        End Select
                    End While

                    Marshal.FreeHGlobal(ptrCond)
                    logistatus = cn.HikvisionLogOut(lUserID)
                End If
            End If
        Next
    End Sub
    Private Sub AddAcsEventToList(ByRef struEventCfg As CHCNetSDK.NET_DVR_ACS_EVENT_CFG, MachineIN_OUT As String, ID_NO As String, datafile As String)
        'If InvokeRequired = True Then
        '    Dim d As SetTextCallback = New SetTextCallback(AddressOf Me.AddAcsEventToList)
        '    Me.Invoke(d, New Object() {struEventCfg})
        '    'Dim tmp = IN_OUT
        'Else
        'Threading.Thread.MemoryBarrier()
        'Thread.Sleep(20)
        'MsgBox(struEventCfg.struAcsEventInfo.dwSerialNo.ToString)
        Dim cn As Common = New Common
        Dim con1 As OleDbConnection
        Dim con As SqlConnection
        If Common.servername = "Access" Then
            con1 = New OleDbConnection(Common.ConnectionString)
        Else
            con = New SqlConnection(Common.ConnectionString)
        End If

        Dim CARDNO As String = System.Text.Encoding.UTF8.GetString(struEventCfg.struAcsEventInfo.byCardNo).Trim
        Dim EMPNO As String = System.Text.Encoding.UTF8.GetString(struEventCfg.struAcsEventInfo.byEmployeeNo).Trim
        If CARDNO = "" And EMPNO.Trim = "" Then
            Exit Sub
        ElseIf CARDNO = "" And EMPNO <> "" Then
            CARDNO = EMPNO
        End If

        If IsNumeric(CARDNO) Then
            CARDNO = Convert.ToInt64(CARDNO).ToString("000000000000")
        End If


        Dim OFFICEPUNCH As String = Convert.ToDateTime(cn.GetStrLogTime(struEventCfg.struTime)).ToString("yyyy-MM-dd HH:mm:ss")
        XtraMasterTest.LabelControlStatus.Text = "Downloading " & CARDNO & " " & OFFICEPUNCH
        Application.DoEvents()

        'Dim IP As String = struEventCfg.struRemoteHostAddr.sIpV4

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String

        'insert in text file
        Dim fs As FileStream = New FileStream(datafile, FileMode.OpenOrCreate, FileAccess.Write)
        Dim sw As StreamWriter = New StreamWriter(fs)
        'find the end of the underlying filestream
        sw.BaseStream.Seek(0, SeekOrigin.End)
        sw.WriteLine(CARDNO & " " & OFFICEPUNCH & " " & ID_NO)
        sw.Flush()
        sw.Close()
        'end insert in text file


        'sSql = "Select Paycode  from tblEmployee where presentcardno='" & CARDNO & "'"
        'sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.presentcardno = '" & CARDNO & "'"
        sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & CARDNO & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"

        ds = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            Dim paycode As String = ds.Tables(0).Rows(0)("Paycode").ToString.Trim
            Dim ISROUNDTHECLOCKWORK As String = ds.Tables(0).Rows(0)("ISROUNDTHECLOCKWORK").ToString.Trim
            'MsgBox(serialNo)
            'Dim sSql2 As String = "select * from tblMachine where MAC_ADDRESS ='" & serialNo.Trim & "'"
            'Dim adap1 As SqlDataAdapter
            'Dim adapA1 As OleDbDataAdapter
            'Dim ds3 As DataSet = New DataSet()
            'If Common.servername = "Access" Then
            '    adapA1 = New OleDbDataAdapter(sSql2, con1)
            '    adapA1.Fill(ds3)
            'Else
            '    adap1 = New SqlDataAdapter(sSql2, con)
            '    adap1.Fill(ds3)
            'End If
            Dim IN_OUT As String
            If MachineIN_OUT = "B" Then
                Dim ds2 As DataSet = New DataSet
                Dim sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & CARDNO & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd") & "'"

                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql1, con1)
                    adapA.Fill(ds2)
                Else
                    sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & CARDNO & "' and convert(varchar, OFFICEPUNCH, 23) = '" & Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd") & "'"
                    adap = New SqlDataAdapter(sSql1, con)
                    adap.Fill(ds2)
                End If

                If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                    IN_OUT = "I"
                Else
                    If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                        IN_OUT = "I"
                    Else
                        IN_OUT = "O"
                    End If
                End If
            Else
                IN_OUT = MachineIN_OUT
            End If
            Try

                Dim del As invokeDelegate = Function()
                                                XtraMasterTest.LabelControlStatus.Text = "Downloading " & CARDNO & " " & OFFICEPUNCH
                                            End Function
                Invoke(del)
                Thread.Sleep(200)
                If Common.servername = "Access" Then
                    con1.Open()
                    cmd1 = New OleDbCommand("insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & paycode & "' ) ", con1)
                    cmd1.ExecuteNonQuery()
                    cmd1 = New OleDbCommand("insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & paycode & "' ) ", con1)
                    cmd1.ExecuteNonQuery()
                    con1.Close()
                Else
                    con.Open()
                    cmd = New SqlCommand("insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & paycode & "' ) ", con)
                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand("insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & paycode & "' ) ", con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                End If
                Dim pDate As DateTime = Convert.ToDateTime(OFFICEPUNCH)
                cn.Remove_Duplicate_Punches(pDate, paycode, ds.Tables(0).Rows(0).Item("Id"))
                If ISROUNDTHECLOCKWORK = "Y" Then
                    If Common.PrcessMode = "M" Then
                        cn.Process_AllnonRTCINOUT(pDate.AddDays(-1), Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"), ISROUNDTHECLOCKWORK)
                    Else
                        cn.Process_AllRTC(pDate.AddDays(-1), Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"))
                    End If
                    'cn.Process_AllRTC(pDate.AddDays(-1), Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"))
                Else
                    If Common.PrcessMode = "M" Then
                        cn.Process_AllnonRTCINOUT(pDate, Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"), ISROUNDTHECLOCKWORK)
                    Else
                        cn.Process_AllnonRTC(pDate, Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"))
                    End If
                    'cn.Process_AllnonRTC(pDate, Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"))
                End If
                'sms
                If Common.g_SMSApplicable = "" Then
                    Common.Load_SMS_Policy()
                End If
                If Common.g_SMSApplicable = "Y" Then
                    If Common.g_isAllSMS = "Y" Then
                        Try
                            If ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim <> "" Then
                                Dim SmsContent As String = Common.g_AllContent1 & " " & OFFICEPUNCH & " " & Common.g_AllContent2
                                cn.sendSMS(ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim, SmsContent)
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                End If
                'sms end
            Catch ex As Exception
            End Try
        End If
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
    End Sub
End Class